OBJECT Table 2000000075 User Metadata
{
  OBJECT-PROPERTIES
  {
    Date=13/04/17;
    Time=14:36:26;
    Version List=NAVW110.0;
  }
  PROPERTIES
  {
    DataPerCompany=No;
    CaptionML=[ENU=User Metadata;
               PTG=Metadados Utilizador];
  }
  FIELDS
  {
    { 3   ;   ;User SID            ;GUID          ;TableRelation=User."User Security ID";
                                                   TestTableRelation=No;
                                                   CaptionML=[ENU=User SID;
                                                              PTG=SID Utilizador] }
    { 6   ;   ;User ID             ;Code50        ;FieldClass=FlowField;
                                                   CalcFormula=Lookup(User."User Name" WHERE (User Security ID=FIELD(User SID)));
                                                   CaptionML=[ENU=User ID;
                                                              PTG=ID Utilizador] }
    { 9   ;   ;Page ID             ;Integer       ;TableRelation=AllObjWithCaption."Object ID" WHERE (Object Type=CONST(Page));
                                                   CaptionML=[ENU=Page ID;
                                                              PTG=ID Page] }
    { 12  ;   ;Description         ;Text250       ;FieldClass=FlowField;
                                                   CalcFormula=Lookup(AllObjWithCaption."Object Caption" WHERE (Object Type=CONST(Page),
                                                                                                                Object ID=FIELD(Page ID)));
                                                   CaptionML=[ENU=Description;
                                                              PTG=Descri��o] }
    { 15  ;   ;Date                ;Date          ;CaptionML=[ENU=Date;
                                                              PTG=Data] }
    { 18  ;   ;Time                ;Time          ;CaptionML=[ENU=Time;
                                                              PTG=Tempo] }
    { 21  ;   ;Personalization ID  ;Code40        ;CaptionML=[ENU=Personalization ID;
                                                              PTG=ID Personaliza��o] }
    { 24  ;   ;Page Metadata Delta ;BLOB          ;CaptionML=[ENU=Page Metadata Delta;
                                                              PTG=Delta Metadados Page] }
  }
  KEYS
  {
    {    ;User SID,Page ID,Personalization ID     ;Clustered=Yes }
    {    ;Date                                     }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    END.
  }
}

