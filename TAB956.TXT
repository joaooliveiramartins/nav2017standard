OBJECT Table 956 Time Sheet Detail Archive
{
  OBJECT-PROPERTIES
  {
    Date=07/09/12;
    Time=12:00:00;
    Version List=NAVW17.10;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Time Sheet Detail Archive;
               PTG=Hist. Detalhe Folha Horas];
  }
  FIELDS
  {
    { 1   ;   ;Time Sheet No.      ;Code20        ;TableRelation="Time Sheet Header Archive";
                                                   CaptionML=[ENU=Time Sheet No.;
                                                              PTG=N� Folha Horas] }
    { 2   ;   ;Time Sheet Line No. ;Integer       ;CaptionML=[ENU=Time Sheet Line No.;
                                                              PTG=N� Linha Folha Horas] }
    { 3   ;   ;Date                ;Date          ;CaptionML=[ENU=Date;
                                                              PTG=Data] }
    { 4   ;   ;Type                ;Option        ;CaptionML=[ENU=Type;
                                                              PTG=Tipo];
                                                   OptionCaptionML=[ENU=" ,Resource,Job,Service,Absence,Assembly Order";
                                                                    PTG=" ,Recurso,Projeto,Servi�o,Aus�ncia,Enc. Montagem"];
                                                   OptionString=[ ,Resource,Job,Service,Absence,Assembly Order] }
    { 5   ;   ;Resource No.        ;Code20        ;TableRelation=Resource;
                                                   CaptionML=[ENU=Resource No.;
                                                              PTG=N� Recurso] }
    { 6   ;   ;Job No.             ;Code20        ;TableRelation=Job;
                                                   CaptionML=[ENU=Job No.;
                                                              PTG=N� Projeto] }
    { 7   ;   ;Job Task No.        ;Code20        ;TableRelation="Job Task"."Job Task No." WHERE (Job No.=FIELD(Job No.));
                                                   CaptionML=[ENU=Job Task No.;
                                                              PTG=N� Tarefa Projeto] }
    { 9   ;   ;Cause of Absence Code;Code10       ;TableRelation="Cause of Absence";
                                                   CaptionML=[ENU=Cause of Absence Code;
                                                              PTG=C�d. motivo aus�ncia] }
    { 13  ;   ;Service Order No.   ;Code20        ;TableRelation=IF (Posted=CONST(No)) "Service Header".No. WHERE (Document Type=CONST(Order));
                                                   CaptionML=[ENU=Service Order No.;
                                                              PTG=N� Ordem Servi�o] }
    { 14  ;   ;Service Order Line No.;Integer     ;CaptionML=[ENU=Service Order Line No.;
                                                              PTG=N� Linha Ordem Servi�o] }
    { 15  ;   ;Quantity            ;Decimal       ;CaptionML=[ENU=Quantity;
                                                              PTG=Quantidade];
                                                   Editable=No }
    { 16  ;   ;Posted Quantity     ;Decimal       ;CaptionML=[ENU=Posted Quantity;
                                                              PTG=Quantidade Registada] }
    { 18  ;   ;Assembly Order No.  ;Code20        ;TableRelation=IF (Posted=CONST(No)) "Assembly Header".No. WHERE (Document Type=CONST(Order));
                                                   CaptionML=[ENU=Assembly Order No.;
                                                              PTG=N� Enc. Montagem] }
    { 19  ;   ;Assembly Order Line No.;Integer    ;CaptionML=[ENU=Assembly Order Line No.;
                                                              PTG=N� Linha Enc. Montagem] }
    { 20  ;   ;Status              ;Option        ;CaptionML=[ENU=Status;
                                                              PTG=Estado];
                                                   OptionCaptionML=[ENU=Open,Submitted,Rejected,Approved;
                                                                    PTG=Pendente,Submetido,Rejeitado,Aprovado];
                                                   OptionString=Open,Submitted,Rejected,Approved }
    { 23  ;   ;Posted              ;Boolean       ;CaptionML=[ENU=Posted;
                                                              PTG=Registado] }
    { 24  ;   ;Document No.        ;Code20        ;CaptionML=[ENU=Document No.;
                                                              PTG=N� Documento] }
    { 25  ;   ;Posting Date        ;Date          ;CaptionML=[ENU=Posting Date;
                                                              PTG=Data Registo] }
  }
  KEYS
  {
    {    ;Time Sheet No.,Time Sheet Line No.,Date ;Clustered=Yes }
    {    ;Type,Job No.,Job Task No.,Status,Posted ;SumIndexFields=Quantity }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    END.
  }
}

