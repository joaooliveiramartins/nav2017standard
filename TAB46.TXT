OBJECT Table 46 Item Register
{
  OBJECT-PROPERTIES
  {
    Date=07/09/12;
    Time=12:00:00;
    Version List=NAVW17.10;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Item Register;
               PTG=Reg. Movs. Produto];
    LookupPageID=Page117;
  }
  FIELDS
  {
    { 1   ;   ;No.                 ;Integer       ;CaptionML=[ENU=No.;
                                                              PTG=N�] }
    { 2   ;   ;From Entry No.      ;Integer       ;TableRelation="Item Ledger Entry";
                                                   CaptionML=[ENU=From Entry No.;
                                                              PTG=De N� Mov.] }
    { 3   ;   ;To Entry No.        ;Integer       ;TableRelation="Item Ledger Entry";
                                                   CaptionML=[ENU=To Entry No.;
                                                              PTG=At� N� Mov.] }
    { 4   ;   ;Creation Date       ;Date          ;CaptionML=[ENU=Creation Date;
                                                              PTG=Data Cria��o] }
    { 5   ;   ;Source Code         ;Code10        ;TableRelation="Source Code";
                                                   CaptionML=[ENU=Source Code;
                                                              PTG=C�d. Origem] }
    { 6   ;   ;User ID             ;Code50        ;TableRelation=User."User Name";
                                                   OnLookup=VAR
                                                              UserMgt@1000 : Codeunit 418;
                                                            BEGIN
                                                              UserMgt.LookupUserID("User ID");
                                                            END;

                                                   TestTableRelation=No;
                                                   CaptionML=[ENU=User ID;
                                                              PTG=ID Utilizador] }
    { 7   ;   ;Journal Batch Name  ;Code10        ;TableRelation="Item Journal Batch".Name;
                                                   TestTableRelation=No;
                                                   CaptionML=[ENU=Journal Batch Name;
                                                              PTG=Nome Sec��o Di�rio] }
    { 10  ;   ;From Phys. Inventory Entry No.;Integer;
                                                   TableRelation="Phys. Inventory Ledger Entry";
                                                   CaptionML=[ENU=From Phys. Inventory Entry No.;
                                                              PTG=De N� Mov. Inv. F�sico] }
    { 11  ;   ;To Phys. Inventory Entry No.;Integer;
                                                   TableRelation="Phys. Inventory Ledger Entry";
                                                   CaptionML=[ENU=To Phys. Inventory Entry No.;
                                                              PTG=At� N� Mov. Inv. F�sico] }
    { 5800;   ;From Value Entry No.;Integer       ;TableRelation="Value Entry";
                                                   CaptionML=[ENU=From Value Entry No.;
                                                              PTG=De N� Mov. Valor] }
    { 5801;   ;To Value Entry No.  ;Integer       ;TableRelation="Value Entry";
                                                   CaptionML=[ENU=To Value Entry No.;
                                                              PTG=At� N� Mov. Valor] }
    { 5831;   ;From Capacity Entry No.;Integer    ;TableRelation="Capacity Ledger Entry";
                                                   CaptionML=[ENU=From Capacity Entry No.;
                                                              PTG=De N� Mov. Capacidade] }
    { 5832;   ;To Capacity Entry No.;Integer      ;TableRelation="Capacity Ledger Entry";
                                                   CaptionML=[ENU=To Capacity Entry No.;
                                                              PTG=At� N� Mov. Capacidade] }
  }
  KEYS
  {
    {    ;No.                                     ;Clustered=Yes }
    {    ;Creation Date                            }
    {    ;Source Code,Journal Batch Name,Creation Date }
  }
  FIELDGROUPS
  {
    { 1   ;DropDown            ;No.,From Entry No.,To Entry No.,Creation Date,Source Code }
  }
  CODE
  {

    BEGIN
    END.
  }
}

