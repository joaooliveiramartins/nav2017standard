OBJECT Page 5785 Warehouse Activity Lines
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0;
  }
  PROPERTIES
  {
    Editable=No;
    CaptionML=[ENU=Warehouse Activity Lines;
               PTG=Linhas Atividade Armaz�m];
    SourceTable=Table5767;
    PageType=List;
    OnAfterGetCurrRecord=BEGIN
                           CurrPage.CAPTION := FormCaption;
                         END;

    ActionList=ACTIONS
    {
      { 1900000003;0 ;ActionContainer;
                      ActionContainerType=RelatedInformation }
      { 77      ;1   ;ActionGroup;
                      CaptionML=[ENU=&Line;
                                 PTG=&Linha];
                      Image=Line }
      { 24      ;2   ;Action    ;
                      Name=Card;
                      ShortCutKey=Shift+F7;
                      CaptionML=[ENU=Card;
                                 PTG=Ficha];
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=EditLines;
                      PromotedCategory=Process;
                      OnAction=BEGIN
                                 ShowActivityDoc;
                               END;
                                }
      { 78      ;2   ;Action    ;
                      CaptionML=[ENU=Show &Whse. Document;
                                 PTG=Mostrar Documento Arma&z�m];
                      Promoted=Yes;
                      Image=ViewOrder;
                      PromotedCategory=Process;
                      OnAction=BEGIN
                                 ShowWhseDoc;
                               END;
                                }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 58  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the action type for the warehouse activity line.;
                           PTG=""];
                SourceExpr="Action Type";
                Visible=FALSE }

    { 2   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the type of warehouse activity for the line.;
                           PTG=""];
                SourceExpr="Activity Type";
                Visible=FALSE }

    { 4   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the number of the warehouse activity.;
                           PTG=""];
                SourceExpr="No." }

    { 6   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the number of the warehouse activity line.;
                           PTG=""];
                SourceExpr="Line No." }

    { 8   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the type of source document to which the warehouse activity line relates, such as sales, purchase, and production.;
                           PTG=""];
                SourceExpr="Source Type";
                Visible=FALSE }

    { 10  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the source subtype of the document related to the warehouse request.;
                           PTG=""];
                SourceExpr="Source Subtype";
                Visible=FALSE }

    { 12  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the source number of the document from which the entry originates.;
                           PTG=""];
                SourceExpr="Source No." }

    { 14  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the number in the Line No. field on the warehouse receipt line related to this cross-dock opportunity.;
                           PTG=""];
                SourceExpr="Source Line No." }

    { 16  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the source subline number.;
                           PTG=""];
                SourceExpr="Source Subline No.";
                Visible=FALSE }

    { 18  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the type of document that the line relates to, such as a sales order.;
                           PTG=""];
                SourceExpr="Source Document" }

    { 20  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the code for the location where the activity occurs.;
                           PTG=""];
                SourceExpr="Location Code" }

    { 42  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the zone code where the bin on this line is located.;
                           PTG=""];
                SourceExpr="Zone Code";
                Visible=FALSE }

    { 44  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the bin where items on the line are handled.;
                           PTG=""];
                SourceExpr="Bin Code";
                Visible=FALSE }

    { 22  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the shelf number of the item for informational use.;
                           PTG=""];
                SourceExpr="Shelf No." }

    { 26  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the item number of the item to be handled, such as picked or put away.;
                           PTG=""];
                SourceExpr="Item No." }

    { 28  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the variant code of the item to be handled.;
                           PTG=""];
                SourceExpr="Variant Code";
                Visible=FALSE }

    { 30  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the unit of measure code of the item on the line.;
                           PTG=""];
                SourceExpr="Unit of Measure Code" }

    { 32  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the quantity per unit of measure of the item on the line.;
                           PTG=""];
                SourceExpr="Qty. per Unit of Measure" }

    { 34  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies a description of the item on the line.;
                           PTG=""];
                SourceExpr=Description }

    { 36  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies a description of the item on the line.;
                           PTG=""];
                SourceExpr="Description 2";
                Visible=FALSE }

    { 38  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the quantity of the item to be handled, such as received, put-away, or assigned.;
                           PTG=""];
                SourceExpr=Quantity }

    { 40  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the quantity of the item to be handled, in the base unit of measure.;
                           PTG=""];
                SourceExpr="Qty. (Base)" }

    { 46  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the number of items that have not yet been handled for this warehouse activity line.;
                           PTG=""];
                SourceExpr="Qty. Outstanding" }

    { 48  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the number of items, expressed in the base unit of measure, that have not yet been handled for this warehouse activity line.;
                           PTG=""];
                SourceExpr="Qty. Outstanding (Base)" }

    { 50  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies how many units to handle in this warehouse activity.;
                           PTG=""];
                SourceExpr="Qty. to Handle" }

    { 52  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the quantity of items to be handled in this warehouse activity.;
                           PTG=""];
                SourceExpr="Qty. to Handle (Base)" }

    { 54  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the number of items on the line that have been handled in this warehouse activity.;
                           PTG=""];
                SourceExpr="Qty. Handled" }

    { 56  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the number of items on the line that have been handled in this warehouse activity.;
                           PTG=""];
                SourceExpr="Qty. Handled (Base)" }

    { 72  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the code of the equipment required when you perform the action on the line.;
                           PTG=""];
                SourceExpr="Special Equipment Code";
                Visible=FALSE }

    { 60  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the shipping advice, informing whether partial deliveries are acceptable, copied from the source document header.;
                           PTG=""];
                SourceExpr="Shipping Advice" }

    { 66  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the date when the warehouse activity must be completed.;
                           PTG=""];
                SourceExpr="Due Date" }

    { 62  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the type of warehouse document from which the line originated.;
                           PTG=""];
                SourceExpr="Whse. Document Type";
                Visible=FALSE }

    { 64  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the number of the warehouse document that is the basis for the action on the line.;
                           PTG=""];
                SourceExpr="Whse. Document No.";
                Visible=FALSE }

    { 68  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the number of the line in the warehouse document that is the basis for the action on the line.;
                           PTG=""];
                SourceExpr="Whse. Document Line No.";
                Visible=FALSE }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {
    VAR
      Text000@1001 : TextConst 'ENU=Warehouse Put-away Lines;PTG=Linhas Arrumar Armaz�m';
      Text001@1000 : TextConst 'ENU=Warehouse Pick Lines;PTG=Linhas Recolha Armaz�m';
      Text002@1002 : TextConst 'ENU=Warehouse Movement Lines;PTG=Linhas Movimentos Armaz�m';
      Text003@1003 : TextConst 'ENU=Warehouse Activity Lines;PTG=Linhas Atividade Armaz�m';
      Text004@1004 : TextConst 'ENU=Inventory Put-away Lines;PTG=Linhas Arrumar Invent�rio';
      Text005@1005 : TextConst 'ENU=Inventory Pick Lines;PTG=Linhas Recolha Invent�rio';

    LOCAL PROCEDURE FormCaption@1() : Text[250];
    BEGIN
      CASE "Activity Type" OF
        "Activity Type"::"Put-away":
          EXIT(Text000);
        "Activity Type"::Pick:
          EXIT(Text001);
        "Activity Type"::Movement:
          EXIT(Text002);
        "Activity Type"::"Invt. Put-away":
          EXIT(Text004);
        "Activity Type"::"Invt. Pick":
          EXIT(Text005);
        ELSE
          EXIT(Text003);
      END;
    END;

    BEGIN
    END.
  }
}

