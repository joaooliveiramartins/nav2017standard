OBJECT Page 1120 Cost Type Balance/Budget
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Cost Type Balance/Budget;
               PTG=Saldo/Or�amento Tipo Custo];
    SaveValues=Yes;
    InsertAllowed=No;
    DeleteAllowed=No;
    SourceTable=Table1103;
    PageType=Worksheet;
    RefreshOnActivate=Yes;
    OnOpenPage=BEGIN
                 BudgetFilter := GETFILTER("Budget Filter");
                 FindPeriod('');
               END;

    OnAfterGetRecord=BEGIN
                       NameIndent := 0;
                       CalcFormFields;
                       NameIndent := Indentation;
                       Emphasize := Type <> Type::"Cost Type";
                     END;

    ActionList=ACTIONS
    {
      { 1900000003;0 ;ActionContainer;
                      ActionContainerType=RelatedInformation }
      { 23      ;1   ;ActionGroup;
                      CaptionML=[ENU=A&ccount;
                                 PTG=&Conta];
                      Image=ChartOfAccounts }
      { 28      ;2   ;Action    ;
                      ShortCutKey=Shift+F7;
                      CaptionML=[ENU=Card;
                                 PTG=Ficha];
                      RunObject=Page 1101;
                      RunPageLink=No.=FIELD(No.),
                                  Date Filter=FIELD(Date Filter),
                                  Cost Center Filter=FIELD(Cost Center Filter),
                                  Cost Object Filter=FIELD(Cost Object Filter),
                                  Budget Filter=FIELD(Budget Filter);
                      Image=EditLines }
      { 29      ;2   ;Action    ;
                      ShortCutKey=Ctrl+F7;
                      CaptionML=[ENU=Cost E&ntries;
                                 PTG=Movime&ntos Custo];
                      RunObject=Page 1103;
                      RunPageView=SORTING(Cost Type No.,Posting Date);
                      RunPageLink=Cost Type No.=FIELD(No.),
                                  Posting Date=FIELD(Date Filter);
                      Promoted=No;
                      Image=CostEntries;
                      PromotedCategory=Process }
      { 1       ;0   ;ActionContainer;
                      ActionContainerType=ActionItems }
      { 2       ;1   ;Action    ;
                      Name=PreviousPeriod;
                      CaptionML=[ENU=Previous Period;
                                 PTG=Per�odo Anterior];
                      ToolTipML=[ENU=Previous Period;
                                 PTG=Per�odo Anterior];
                      Promoted=Yes;
                      Image=PreviousRecord;
                      PromotedCategory=Process;
                      OnAction=BEGIN
                                 FindPeriod('<=');
                               END;
                                }
      { 3       ;1   ;Action    ;
                      Name=NextPeriod;
                      CaptionML=[ENU=Next Period;
                                 PTG=Per. Seguinte];
                      ToolTipML=[ENU=Next Period;
                                 PTG=Per. Seguinte];
                      Promoted=Yes;
                      Image=NextRecord;
                      PromotedCategory=Process;
                      OnAction=BEGIN
                                 FindPeriod('>=');
                               END;
                                }
      { 24      ;1   ;ActionGroup;
                      CaptionML=[ENU=F&unctions;
                                 PTG=F&un��es];
                      Image=Action }
      { 47      ;2   ;Action    ;
                      Ellipsis=Yes;
                      CaptionML=[ENU=Copy Budget;
                                 PTG=Copiar Or�amento];
                      RunObject=Report 96;
                      Image=CopyBudget }
    }
  }
  CONTROLS
  {
    { 4   ;0   ;Container ;
                ContainerType=ContentArea }

    { 5   ;1   ;Group     ;
                CaptionML=[ENU=Options;
                           PTG=Op��es] }

    { 7   ;2   ;Field     ;
                CaptionML=[ENU=Budget Filter;
                           PTG=Filtro Or�amento];
                SourceExpr=BudgetFilter;
                TableRelation="Cost Budget Name".Name;
                LookupPageID=Cost Budget Names;
                OnValidate=BEGIN
                             CurrPage.UPDATE(FALSE);
                           END;
                            }

    { 8   ;2   ;Field     ;
                CaptionML=[ENU=Cost Center Filter;
                           PTG=Filtro Centro Custo];
                SourceExpr=CostCenterFilter;
                OnValidate=BEGIN
                             CurrPage.UPDATE(FALSE);
                           END;

                OnLookup=VAR
                           CostCenter@1000 : Record 1112;
                         BEGIN
                           EXIT(CostCenter.LookupCostCenterFilter(Text));
                         END;
                          }

    { 9   ;2   ;Field     ;
                CaptionML=[ENU=Cost Object Filter;
                           PTG=Filtro Objeto Custo];
                SourceExpr=CostObjectFilter;
                OnValidate=BEGIN
                             CurrPage.UPDATE(FALSE);
                           END;

                OnLookup=VAR
                           CostObject@1000 : Record 1113;
                         BEGIN
                           EXIT(CostObject.LookupCostObjectFilter(Text));
                         END;
                          }

    { 10  ;2   ;Field     ;
                CaptionML=[ENU=View by;
                           PTG=Ver por];
                ToolTipML=[ENU=Specifies by which period amounts are displayed.;
                           PTG=Especifica por que per�odos s�o apresentados os valores.];
                OptionCaptionML=[ENU=Day,Week,Month,Quarter,Year,Accounting Period;
                                 PTG=Dia,Semana,M�s,Trimestre,Ano,Per�odo Contabil�stico];
                SourceExpr=PeriodType;
                OnValidate=BEGIN
                             FindPeriod('');
                           END;
                            }

    { 11  ;2   ;Field     ;
                CaptionML=[ENU=View as;
                           PTG=Ver como];
                ToolTipML=[ENU=Specifies how amounts are displayed. Net Change: The net change in the balance for the selected period. Balance at Date: The balance as of the last day in the selected period.;
                           PTG=Especifica como s�o apresentados os valores. Saldo Per�odo: Saldo no per�odo selecionado. Saldo � Data: Saldo do �ltimo dia do per�odo selecionado.];
                OptionCaptionML=[ENU=Net Change,Balance at Date;
                                 PTG=Saldo Per�odo,Saldo � Data];
                SourceExpr=AmountType;
                OnValidate=BEGIN
                             IF (AmountType = AmountType::"Balance at Date") OR (AmountType = AmountType::"Net Change") THEN
                               FindPeriod('');
                           END;
                            }

    { 12  ;1   ;Group     ;
                IndentationColumnName=NameIndent;
                IndentationControls=Name;
                GroupType=Repeater }

    { 13  ;2   ;Field     ;
                Name=Number;
                SourceExpr="No.";
                Style=Strong;
                StyleExpr=Emphasize }

    { 14  ;2   ;Field     ;
                Name=Name;
                SourceExpr=Name;
                Style=Strong;
                StyleExpr=Emphasize }

    { 6   ;2   ;Field     ;
                BlankZero=Yes;
                SourceExpr="Net Change";
                Style=Strong;
                StyleExpr=Emphasize }

    { 15  ;2   ;Field     ;
                BlankNumbers=BlankNegAndZero;
                SourceExpr="Debit Amount";
                Visible=FALSE;
                Style=Strong;
                StyleExpr=Emphasize }

    { 16  ;2   ;Field     ;
                BlankNumbers=BlankNegAndZero;
                SourceExpr="Credit Amount";
                Visible=FALSE;
                Style=Strong;
                StyleExpr=Emphasize }

    { 18  ;2   ;Field     ;
                BlankZero=Yes;
                SourceExpr="Budget Amount";
                Style=Strong;
                StyleExpr=Emphasize;
                OnValidate=BEGIN
                             CalcFormFields;
                           END;
                            }

    { 19  ;2   ;Field     ;
                CaptionML=[ENU=Balance/Budget (%);
                           PTG=Saldo/Or�amento (%)];
                DecimalPlaces=1:1;
                BlankZero=Yes;
                SourceExpr=BudgetPct;
                Editable=FALSE }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {
    VAR
      PeriodType@1000 : 'Day,Week,Month,Quarter,Year,Accounting Period';
      AmountType@1001 : 'Net Change,Balance at Date';
      BudgetPct@1003 : Decimal;
      Emphasize@1004 : Boolean INDATASET;
      NameIndent@1006 : Integer INDATASET;
      BudgetFilter@1002 : Code[10];
      CostCenterFilter@1008 : Text[1024];
      CostObjectFilter@1007 : Text[1024];

    LOCAL PROCEDURE FindPeriod@1(SearchText@1000 : Code[3]);
    VAR
      Calendar@1001 : Record 2000000007;
      PeriodFormMgt@1003 : Codeunit 359;
    BEGIN
      IF GETFILTER("Date Filter") <> '' THEN BEGIN
        Calendar.SETFILTER("Period Start",GETFILTER("Date Filter"));
        IF NOT PeriodFormMgt.FindDate('+',Calendar,PeriodType) THEN
          PeriodFormMgt.FindDate('+',Calendar,PeriodType::Day);
        Calendar.SETRANGE("Period Start");
      END;
      PeriodFormMgt.FindDate(SearchText,Calendar,PeriodType);
      IF AmountType = AmountType::"Net Change" THEN
        IF Calendar."Period Start" = Calendar."Period End" THEN
          SETRANGE("Date Filter",Calendar."Period Start")
        ELSE
          SETRANGE("Date Filter",Calendar."Period Start",Calendar."Period End")
      ELSE
        SETRANGE("Date Filter",0D,Calendar."Period End");

      CurrPage.UPDATE(FALSE);
    END;

    LOCAL PROCEDURE CalcFormFields@2();
    BEGIN
      SETFILTER("Budget Filter",BudgetFilter);
      SETFILTER("Cost Center Filter",CostCenterFilter);
      SETFILTER("Cost Object Filter",CostObjectFilter);

      CALCFIELDS("Net Change","Budget Amount");
      IF "Budget Amount" = 0 THEN
        BudgetPct := 0
      ELSE
        BudgetPct := ROUND("Net Change" / "Budget Amount" * 100);
    END;

    BEGIN
    END.
  }
}

