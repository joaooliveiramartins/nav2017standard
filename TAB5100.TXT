OBJECT Table 5100 Communication Method
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Communication Method;
               PTG=M�todo Comunica��o];
  }
  FIELDS
  {
    { 1   ;   ;Key                 ;Integer       ;CaptionML=[ENU=Key;
                                                              PTG=Chave] }
    { 2   ;   ;Description         ;Text50        ;CaptionML=[ENU=Description;
                                                              PTG=Descri��o] }
    { 3   ;   ;Number              ;Text30        ;CaptionML=[ENU=Number;
                                                              PTG=N�mero] }
    { 4   ;   ;Contact No.         ;Code20        ;TableRelation=Contact;
                                                   CaptionML=[ENU=Contact No.;
                                                              PTG=N� Contacto] }
    { 5   ;   ;Name                ;Text50        ;CaptionML=[ENU=Name;
                                                              PTG=Nome] }
    { 6   ;   ;Type                ;Option        ;CaptionML=[ENU=Type;
                                                              PTG=Tipo];
                                                   OptionCaptionML=[ENU=Company,Person;
                                                                    PTG=Empresa,Pessoa];
                                                   OptionString=Company,Person }
    { 7   ;   ;E-Mail              ;Text80        ;ExtendedDatatype=E-Mail;
                                                   CaptionML=[ENU=Email;
                                                              PTG=E-mail] }
  }
  KEYS
  {
    {    ;Key                                     ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    END.
  }
}

