OBJECT Table 5927 Repair Status
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0;
  }
  PROPERTIES
  {
    OnInsert=BEGIN
               VALIDATE("Service Order Status");
             END;

    OnDelete=VAR
               ServiceItemLine@1000 : Record 5901;
             BEGIN
               ServiceItemLine.SETRANGE("Repair Status Code",Code);
               IF NOT ServiceItemLine.ISEMPTY THEN
                 ERROR(Text002,TABLECAPTION,Code,ServiceItemLine.TABLECAPTION);
             END;

    CaptionML=[ENU=Repair Status;
               PTG=Estado Repara��o];
    LookupPageID=Page5942;
    DrillDownPageID=Page5942;
  }
  FIELDS
  {
    { 1   ;   ;Code                ;Code10        ;CaptionML=[ENU=Code;
                                                              PTG=C�digo];
                                                   NotBlank=Yes }
    { 2   ;   ;Description         ;Text50        ;CaptionML=[ENU=Description;
                                                              PTG=Descri��o] }
    { 3   ;   ;Service Order Status;Option        ;OnValidate=BEGIN
                                                                IF NOT ServStatusPrioritySetup.GET("Service Order Status") THEN
                                                                  ERROR(
                                                                    Text000,
                                                                    FIELDCAPTION("Service Order Status"),"Service Order Status",ServStatusPrioritySetup.TABLECAPTION);

                                                                Priority := ServStatusPrioritySetup.Priority;
                                                              END;

                                                   CaptionML=[ENU=Service Order Status;
                                                              PTG=Estado Ordem Servi�o];
                                                   OptionCaptionML=[ENU=Pending,In Process,Finished,On Hold;
                                                                    PTG=Pendente,Em Execu��o,Terminado,Retido];
                                                   OptionString=Pending,In Process,Finished,On Hold }
    { 4   ;   ;Priority            ;Option        ;CaptionML=[ENU=Priority;
                                                              PTG=Prioridade];
                                                   OptionCaptionML=[ENU=High,Medium High,Medium Low,Low;
                                                                    PTG=Alto,M�dio Alto,M�dio Baixo,Baixo];
                                                   OptionString=High,Medium High,Medium Low,Low;
                                                   Editable=No }
    { 5   ;   ;Initial             ;Boolean       ;OnValidate=VAR
                                                                RepairStatus@1000 : Record 5927;
                                                              BEGIN
                                                                IF Initial THEN BEGIN
                                                                  RepairStatus.SETFILTER(Code,'<>%1',Code);
                                                                  RepairStatus.SETRANGE(Initial,TRUE);
                                                                  IF NOT RepairStatus.ISEMPTY THEN
                                                                    ERROR(Text001,TABLECAPTION,FIELDCAPTION(Initial));
                                                                END;
                                                              END;

                                                   CaptionML=[ENU=Initial;
                                                              PTG=Inicial] }
    { 6   ;   ;Partly Serviced     ;Boolean       ;OnValidate=VAR
                                                                RepairStatus@1000 : Record 5927;
                                                              BEGIN
                                                                IF "Partly Serviced" THEN BEGIN
                                                                  RepairStatus.SETFILTER(Code,'<>%1',Code);
                                                                  RepairStatus.SETRANGE("Partly Serviced",TRUE);
                                                                  IF NOT RepairStatus.ISEMPTY THEN
                                                                    ERROR(Text001,TABLECAPTION,FIELDCAPTION("Partly Serviced"));
                                                                END;
                                                              END;

                                                   CaptionML=[ENU=Partly Serviced;
                                                              PTG=Parcialmente Executado] }
    { 7   ;   ;In Process          ;Boolean       ;OnValidate=VAR
                                                                RepairStatus@1000 : Record 5927;
                                                              BEGIN
                                                                IF "In Process" THEN BEGIN
                                                                  RepairStatus.SETFILTER(Code,'<>%1',Code);
                                                                  RepairStatus.SETRANGE("In Process",TRUE);
                                                                  IF NOT RepairStatus.ISEMPTY THEN
                                                                    ERROR(Text001,TABLECAPTION,FIELDCAPTION("In Process"));
                                                                END;
                                                              END;

                                                   CaptionML=[ENU=In Process;
                                                              PTG=Em Execu��o] }
    { 8   ;   ;Finished            ;Boolean       ;OnValidate=VAR
                                                                RepairStatus@1000 : Record 5927;
                                                              BEGIN
                                                                IF Finished THEN BEGIN
                                                                  RepairStatus.SETFILTER(Code,'<>%1',Code);
                                                                  RepairStatus.SETRANGE(Finished,TRUE);
                                                                  IF NOT RepairStatus.ISEMPTY THEN
                                                                    ERROR(Text001,TABLECAPTION,FIELDCAPTION(Finished));
                                                                END;
                                                              END;

                                                   CaptionML=[ENU=Finished;
                                                              PTG=Terminada] }
    { 9   ;   ;Referred            ;Boolean       ;OnValidate=VAR
                                                                RepairStatus@1000 : Record 5927;
                                                              BEGIN
                                                                IF Referred THEN BEGIN
                                                                  RepairStatus.SETFILTER(Code,'<>%1',Code);
                                                                  RepairStatus.SETRANGE(Referred,TRUE);
                                                                  IF NOT RepairStatus.ISEMPTY THEN
                                                                    ERROR(Text001,TABLECAPTION,FIELDCAPTION(Referred));
                                                                END;
                                                              END;

                                                   CaptionML=[ENU=Referred;
                                                              PTG=Atribu�do] }
    { 10  ;   ;Spare Part Ordered  ;Boolean       ;OnValidate=VAR
                                                                RepairStatus@1000 : Record 5927;
                                                              BEGIN
                                                                IF "Spare Part Ordered" THEN BEGIN
                                                                  RepairStatus.SETFILTER(Code,'<>%1',Code);
                                                                  RepairStatus.SETRANGE("Spare Part Ordered",TRUE);
                                                                  IF NOT RepairStatus.ISEMPTY THEN
                                                                    ERROR(Text001,TABLECAPTION,FIELDCAPTION("Spare Part Ordered"));
                                                                END;
                                                              END;

                                                   CaptionML=[ENU=Spare Part Ordered;
                                                              PTG=Pe�as Encomendadas] }
    { 11  ;   ;Spare Part Received ;Boolean       ;OnValidate=VAR
                                                                RepairStatus@1000 : Record 5927;
                                                              BEGIN
                                                                IF "Spare Part Received" THEN BEGIN
                                                                  RepairStatus.SETFILTER(Code,'<>%1',Code);
                                                                  RepairStatus.SETRANGE("Spare Part Received",TRUE);
                                                                  IF NOT RepairStatus.ISEMPTY THEN
                                                                    ERROR(Text001,TABLECAPTION,FIELDCAPTION("Spare Part Received"));
                                                                END;
                                                              END;

                                                   CaptionML=[ENU=Spare Part Received;
                                                              PTG=Pe�as Recebidas] }
    { 12  ;   ;Waiting for Customer;Boolean       ;OnValidate=VAR
                                                                RepairStatus@1000 : Record 5927;
                                                              BEGIN
                                                                IF "Waiting for Customer" THEN BEGIN
                                                                  RepairStatus.SETFILTER(Code,'<>%1',Code);
                                                                  RepairStatus.SETRANGE("Waiting for Customer",TRUE);
                                                                  IF NOT RepairStatus.ISEMPTY THEN
                                                                    ERROR(Text001,TABLECAPTION,FIELDCAPTION("Waiting for Customer"));
                                                                END;
                                                              END;

                                                   CaptionML=[ENU=Waiting for Customer;
                                                              PTG=� Espera do Cliente] }
    { 13  ;   ;Quote Finished      ;Boolean       ;OnValidate=VAR
                                                                RepairStatus@1000 : Record 5927;
                                                              BEGIN
                                                                IF "Quote Finished" THEN BEGIN
                                                                  RepairStatus.SETFILTER(Code,'<>%1',Code);
                                                                  RepairStatus.SETRANGE("Quote Finished",TRUE);
                                                                  IF NOT RepairStatus.ISEMPTY THEN
                                                                    ERROR(Text001,TABLECAPTION,FIELDCAPTION("Quote Finished"));
                                                                END;
                                                              END;

                                                   CaptionML=[ENU=Quote Finished;
                                                              PTG=Proposta Terminada] }
    { 20  ;   ;Posting Allowed     ;Boolean       ;CaptionML=[ENU=Posting Allowed;
                                                              PTG=Registo Permitido] }
    { 21  ;   ;Pending Status Allowed;Boolean     ;CaptionML=[ENU=Pending Status Allowed;
                                                              PTG=Estado Pendente Permitido] }
    { 22  ;   ;In Process Status Allowed;Boolean  ;CaptionML=[ENU=In Process Status Allowed;
                                                              PTG=Estado Em Execu��o Permitido] }
    { 23  ;   ;Finished Status Allowed;Boolean    ;CaptionML=[ENU=Finished Status Allowed;
                                                              PTG=Estado Terminado Permitido] }
    { 24  ;   ;On Hold Status Allowed;Boolean     ;CaptionML=[ENU=On Hold Status Allowed;
                                                              PTG=Estado Retido Permitido] }
  }
  KEYS
  {
    {    ;Code                                    ;Clustered=Yes }
    {    ;Priority                                 }
    {    ;Description                              }
  }
  FIELDGROUPS
  {
    { 1   ;DropDown            ;Code,Description,Service Order Status    }
  }
  CODE
  {
    VAR
      Text000@1000 : TextConst 'ENU=The program cannot find the %1 %2 in the %3 table.;PTG=O programa n�o consegue encontrar o %1 %2 na tabela %3.';
      Text001@1001 : TextConst 'ENU=Only one %1 can be marked as %2.;PTG=Apenas um %1 pode ser marcado como %2.';
      ServStatusPrioritySetup@1003 : Record 5928;
      Text002@1004 : TextConst 'ENU=You cannot delete the %1 %2 because there is at least one %3 that has this %1.;PTG=N�o � poss�vel eliminar %1 %2, porque existe, pelo menos, um %3 com %1.';

    PROCEDURE ReturnStatusCode@2(RepairStatus2@1000 : Record 5927) : Code[10];
    VAR
      RepairStatus@1001 : Record 5927;
    BEGIN
      CASE TRUE OF
        RepairStatus2.Initial:
          RepairStatus.SETRANGE(Initial,TRUE);
        RepairStatus2."Partly Serviced":
          RepairStatus.SETRANGE("Partly Serviced",TRUE);
        RepairStatus2."In Process":
          RepairStatus.SETRANGE("In Process",TRUE);
        RepairStatus2.Finished:
          RepairStatus.SETRANGE(Finished,TRUE);
        RepairStatus2.Referred:
          RepairStatus.SETRANGE(Referred,TRUE);
        RepairStatus2."Spare Part Ordered":
          RepairStatus.SETRANGE("Spare Part Ordered",TRUE);
        RepairStatus2."Spare Part Received":
          RepairStatus.SETRANGE("Spare Part Received",TRUE);
        RepairStatus2."Waiting for Customer":
          RepairStatus.SETRANGE("Waiting for Customer",TRUE);
        RepairStatus2."Quote Finished":
          RepairStatus.SETRANGE("Quote Finished",TRUE);
        ELSE
          EXIT('');
      END;
      IF RepairStatus.FINDFIRST THEN
        EXIT(RepairStatus.Code);

      EXIT('');
    END;

    BEGIN
    END.
  }
}

