OBJECT Report 1200 Create Direct Debit Collection
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0.00.15052;
  }
  PROPERTIES
  {
    TransactionType=Update;
    CaptionML=[ENU=Create Direct Debit Collection;
               PTG=Criar Recebimentos D�bito Direto];
    ProcessingOnly=Yes;
    OnPreReport=BEGIN
                  BankAccount.GET(BankAccount."No.");
                  GLSetup.GET;
                END;

    OnPostReport=BEGIN
                   IF NoOfEntries = 0 THEN
                     ERROR(NoEntriesCreatedErr);
                   MESSAGE(EntriesCreatedMsg,NoOfEntries);
                 END;

  }
  DATASET
  {
    { 2   ;    ;DataItem;                    ;
               DataItemTable=Table18;
               OnPreDataItem=BEGIN
                               DirectDebitCollection.CreateNew(BankAccount.GetDirectDebitMessageNo,BankAccount."No.",PartnerType);
                               SETRANGE("Partner Type",PartnerType);
                             END;

               ReqFilterFields=Currency Code,Country/Region Code }

    { 1   ;1   ;DataItem;                    ;
               DataItemTable=Table21;
               DataItemTableView=SORTING(Open,Due Date)
                                 WHERE(Open=CONST(Yes),
                                       Document Type=CONST(Invoice));
               OnPreDataItem=BEGIN
                               SETAUTOCALCFIELDS("Remaining Amount");
                               SETRANGE("Due Date",FromDate,ToDate);
                               IF OnlyInvoicesWithMandate THEN
                                 SETFILTER("Direct Debit Mandate ID",'<>%1','');
                               SETRANGE("Currency Code",GLSetup.GetCurrencyCode('EUR'))
                             END;

               OnAfterGetRecord=BEGIN
                                  IF OnlyCustomersWithMandate THEN
                                    IF NOT Customer.HasValidDDMandate("Due Date") THEN
                                      CurrReport.SKIP;
                                  IF OnlyInvoicesWithMandate THEN BEGIN
                                    SEPADirectDebitMandate.GET("Direct Debit Mandate ID");
                                    IF NOT SEPADirectDebitMandate.IsMandateActive("Due Date") THEN
                                      CurrReport.SKIP;
                                  END;

                                  IF NOT EntryFullyCollected("Entry No.") THEN BEGIN
                                    DirectDebitCollectionEntry.CreateNew(DirectDebitCollection."No.","Cust. Ledger Entry");
                                    NoOfEntries += 1;
                                  END;
                                END;

               DataItemLink=Customer No.=FIELD(No.) }

  }
  REQUESTPAGE
  {
    PROPERTIES
    {
      SaveValues=Yes;
    }
    CONTROLS
    {
      { 1   ;    ;Container ;
                  CaptionML=[ENU=Options;
                             PTG=Op��es];
                  ContainerType=ContentArea }

      { 2   ;1   ;Field     ;
                  Name=FromDueDate;
                  CaptionML=[ENU=From Due Date;
                             PTG=Desde Data Vencimento];
                  ToolTipML=ENU=Specifies the earliest payment due date on sales invoices that you want to create a direct-debit collection for.;
                  ApplicationArea=#Suite;
                  SourceExpr=FromDate }

      { 3   ;1   ;Field     ;
                  Name=ToDueDate;
                  CaptionML=[ENU=To Due Date;
                             PTG=Para Data Vencimento];
                  ToolTipML=ENU=Specifies the latest payment due date on sales invoices that you want to create a direct-debit collection for.;
                  ApplicationArea=#Suite;
                  SourceExpr=ToDate;
                  OnValidate=BEGIN
                               IF (ToDate <> 0D) AND (FromDate > ToDate) THEN
                                 ERROR(WrongDateErr);
                             END;
                              }

      { 8   ;1   ;Field     ;
                  Name=PartnerType;
                  CaptionML=[ENU=Partner Type;
                             PTG=Tipo Parceiro];
                  ToolTipML=ENU=Specifies if the direct-debit collection is made for customers of type Company or Person.;
                  OptionCaptionML=[ENU=,Company,Person;
                                   PTG=,Empresa,Pessoa];
                  ApplicationArea=#Suite;
                  NotBlank=Yes;
                  BlankZero=Yes;
                  SourceExpr=PartnerType }

      { 4   ;1   ;Field     ;
                  Name=OnlyCustomerValidMandate;
                  CaptionML=[ENU=Only Customers With Valid Mandate;
                             PTG=Apenas Clientes com Autoriza��o V�lida];
                  ToolTipML=ENU=Specifies if a direct-debit collection is created for customers who have a valid direct-debit mandate. A direct-debit collection is created even if the Direct Debit Mandate ID field is not filled on the sales invoice.;
                  ApplicationArea=#Suite;
                  SourceExpr=OnlyCustomersWithMandate }

      { 7   ;1   ;Field     ;
                  Name=OnlyInvoiceValidMandate;
                  CaptionML=[ENU=Only Invoices With Valid Mandate;
                             PTG=Apenas Faturas com Autoriza��o V�lida];
                  ToolTipML=ENU=Specifies if a direct-debit collection is only created for sales invoices if a valid direct-debit mandate is selected in the Direct Debit Mandate ID field on the sales invoice.;
                  ApplicationArea=#Suite;
                  SourceExpr=OnlyInvoicesWithMandate }

      { 5   ;1   ;Field     ;
                  Name=BankAccNo;
                  CaptionML=[ENU=Bank Account No.;
                             PTG=N� Conta Banc�ria];
                  ToolTipML=ENU=Specifies which of your company's bank accounts the collected payment will be transferred to from the customer's bank account.;
                  ApplicationArea=#Suite;
                  SourceExpr=BankAccount."No.";
                  TableRelation="Bank Account";
                  OnValidate=BEGIN
                               IF BankAccount."No." = '' THEN
                                 EXIT;
                               BankAccount.GET(BankAccount."No.");
                               BankAccount.TESTFIELD("Direct Debit Msg. Nos.");
                             END;
                              }

      { 6   ;1   ;Field     ;
                  Name=BankAccName;
                  CaptionML=[ENU=Bank Account Name;
                             PTG=Nome Conta Banc�ria];
                  ToolTipML=ENU=Specifies the name of the bank account that you select in the Bank Account No. field. This field is filled automatically.;
                  ApplicationArea=#Suite;
                  SourceExpr=BankAccount.Name;
                  Enabled=FALSE;
                  Editable=FALSE }

    }
  }
  LABELS
  {
  }
  CODE
  {
    VAR
      BankAccount@1004 : Record 270;
      DirectDebitCollection@1005 : Record 1207;
      DirectDebitCollectionEntry@1006 : Record 1208;
      SEPADirectDebitMandate@1008 : Record 1230;
      GLSetup@1013 : Record 98;
      FromDate@1000 : Date;
      ToDate@1001 : Date;
      OnlyCustomersWithMandate@1002 : Boolean;
      WrongDateErr@1003 : TextConst 'ENU=To Date must be equal to or greater than From Date.;PTG=O campo At� Data Vencimento deve ser maior ou igual que Desde Data Vencimento.';
      OnlyInvoicesWithMandate@1007 : Boolean;
      NoOfEntries@1009 : Integer;
      NoEntriesCreatedErr@1010 : TextConst '@@@="%1=Field;%2=Table;%3=Field;Table";ENU=No entries have been created.;PTG=N�o foram criados movimentos.';
      EntriesCreatedMsg@1011 : TextConst '@@@="%1 = an integer number, e.g. 7.";ENU=%1 entries have been created.;PTG=Foram criados %1 registos.';
      PartnerType@1012 : ',Company,Person';

    LOCAL PROCEDURE EntryFullyCollected@1(EntryNo@1000 : Integer) : Boolean;
    VAR
      DirectDebitCollectionEntry@1001 : Record 1208;
    BEGIN
      DirectDebitCollectionEntry.SETRANGE("Applies-to Entry No.",EntryNo);
      IF DirectDebitCollectionEntry.ISEMPTY THEN
        EXIT(FALSE);

      DirectDebitCollectionEntry.SETFILTER(
        Status,'%1|%2',DirectDebitCollectionEntry.Status::New,DirectDebitCollectionEntry.Status::"File Created");
      DirectDebitCollectionEntry.CALCSUMS("Transfer Amount");
      EXIT(DirectDebitCollectionEntry."Transfer Amount" >= "Cust. Ledger Entry"."Remaining Amount");
    END;

    BEGIN
    END.
  }
  RDLDATA
  {
  }
}

