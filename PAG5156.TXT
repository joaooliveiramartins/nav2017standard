OBJECT Page 5156 Customer Template List
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0;
  }
  PROPERTIES
  {
    Editable=No;
    CaptionML=[ENU=Customer Template List;
               PTG=Lista Modelo Clientes];
    SourceTable=Table5105;
    PageType=List;
    CardPageID=Customer Template Card;
    ActionList=ACTIONS
    {
      { 1900000003;0 ;ActionContainer;
                      ActionContainerType=RelatedInformation }
      { 15      ;1   ;ActionGroup;
                      CaptionML=[ENU=&Customer Template;
                                 PTG=Modelo &Cliente];
                      Image=Template }
      { 18      ;2   ;ActionGroup;
                      CaptionML=[ENU=Dimensions;
                                 PTG=Dimens�es];
                      Image=Dimensions }
      { 19      ;3   ;Action    ;
                      ShortCutKey=Shift+Ctrl+D;
                      CaptionML=[ENU=Dimensions-Single;
                                 PTG=Dimens�es-Singular];
                      ToolTipML=[ENU=View or edit the single set of dimensions that are set up for the selected record.;
                                 PTG=""];
                      RunObject=Page 540;
                      RunPageLink=Table ID=CONST(5105),
                                  No.=FIELD(Code);
                      Image=Dimensions }
      { 20      ;3   ;Action    ;
                      AccessByPermission=TableData 348=R;
                      CaptionML=[ENU=Dimensions-&Multiple;
                                 PTG=Dimens�es-&M�ltipla];
                      ToolTipML=[ENU=View or edit dimensions for a group of records. You can assign dimension codes to transactions to distribute costs and analyze historical information.;
                                 PTG=""];
                      Image=DimensionSets;
                      OnAction=VAR
                                 CustTemplate@1001 : Record 5105;
                                 DefaultDimMultiple@1000 : Page 542;
                               BEGIN
                                 CurrPage.SETSELECTIONFILTER(CustTemplate);
                                 DefaultDimMultiple.SetMultiCustTemplate(CustTemplate);
                                 DefaultDimMultiple.RUNMODAL;
                               END;
                                }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 2   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the code for the customer template. You can set up as many codes as you want. The code must be unique. You cannot have the same code twice in one table.;
                           PTG=""];
                ApplicationArea=#All;
                SourceExpr=Code }

    { 4   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the description of the customer template.;
                           PTG=""];
                ApplicationArea=#All;
                SourceExpr=Description }

    { 6   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the country/region code for the customer template.;
                           PTG=""];
                SourceExpr="Country/Region Code" }

    { 8   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the territory code for the customer template.;
                           PTG=""];
                SourceExpr="Territory Code" }

    { 10  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the currency code for the customer template.;
                           PTG=""];
                SourceExpr="Currency Code" }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {

    BEGIN
    END.
  }
}

