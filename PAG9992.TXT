OBJECT Page 9992 Code Coverage Object
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW19.00;
  }
  PROPERTIES
  {
    Editable=No;
    CaptionML=[ENU=Objects;
               PTG=Objetos];
    SourceTable=Table2000000001;
    SourceTableView=WHERE(Type=FILTER(<>TableData));
    PageType=List;
    ActionList=ACTIONS
    {
      { 10      ;    ;ActionContainer;
                      Name=Actions;
                      CaptionML=[ENU=Action;
                                 PTG=A��o];
                      ActionContainerType=ActionItems }
      { 11      ;1   ;Action    ;
                      Name=Load;
                      CaptionML=[ENU=Load;
                                 PTG=Importar];
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=AddContacts;
                      PromotedCategory=Process;
                      OnAction=VAR
                                 Object@1000 : Record 2000000001;
                                 CodeCoverageMgt@1001 : Codeunit 9990;
                               BEGIN
                                 Object.COPYFILTERS(Rec);
                                 CodeCoverageMgt.Include(Object);
                               END;
                                }
    }
  }
  CONTROLS
  {
    { 1   ;0   ;Container ;
                ContainerType=ContentArea }

    { 2   ;1   ;Group     ;
                CaptionML=[ENU=Group;
                           PTG=Grupo];
                GroupType=Repeater }

    { 3   ;2   ;Field     ;
                CaptionML=[ENU=Type;
                           PTG=Tipo];
                SourceExpr=Type }

    { 4   ;2   ;Field     ;
                CaptionML=[ENU=ID;
                           PTG=ID];
                SourceExpr=ID }

    { 5   ;2   ;Field     ;
                CaptionML=[ENU=Name;
                           PTG=Nome];
                SourceExpr=Name }

    { 6   ;2   ;Field     ;
                CaptionML=[ENU=Modified;
                           PTG=Modificado];
                SourceExpr=Modified }

    { 7   ;2   ;Field     ;
                CaptionML=[ENU=Compiled;
                           PTG=Compilado];
                SourceExpr=Compiled }

    { 8   ;2   ;Field     ;
                CaptionML=[ENU=Date;
                           PTG=Data];
                SourceExpr=Date }

    { 9   ;2   ;Field     ;
                CaptionML=[ENU=Version List;
                           PTG=Lista Vers�o];
                SourceExpr="Version List" }

  }
  CODE
  {

    BEGIN
    END.
  }
}

