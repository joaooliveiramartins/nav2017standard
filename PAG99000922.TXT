OBJECT Page 99000922 Production Forecast Entries
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Production Forecast Entries;
               PTG=Movs. Previs�o Produ��o];
    InsertAllowed=No;
    SourceTable=Table99000852;
    DelayedInsert=Yes;
    PageType=List;
    OnNewRecord=BEGIN
                  "Production Forecast Name" := xRec."Production Forecast Name";
                  "Item No." := xRec."Item No.";
                  "Forecast Date" := xRec."Forecast Date";
                END;

  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 2   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the name of the production forecast to which the entry belongs.;
                           PTG=Especifica o nome da previs�o de produ��o � qual o movimento pertence.];
                SourceExpr="Production Forecast Name";
                Editable=FALSE }

    { 4   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the item identification number of the entry.;
                           PTG=Especifica o n�mero de identifica��o do produto do movimento.];
                SourceExpr="Item No.";
                Editable=FALSE }

    { 29  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies a brief description of your forecast.;
                           PTG=Especifica uma breve descri ��o da previs�o.];
                SourceExpr=Description }

    { 14  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the quantity of the entry stated, in base units of measure.;
                           PTG=Especifica a quantidade do movimento, em unidades de medida base.];
                SourceExpr="Forecast Quantity (Base)";
                Editable=FALSE }

    { 6   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the date of the production forecast to which the entry belongs.;
                           PTG=Especifica a data da previs�o de produ��o � qual o movimento pertence.];
                SourceExpr="Forecast Date";
                Editable=FALSE }

    { 8   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the quantities you have entered in the production forecast within the selected time interval.;
                           PTG=Especifica as quantidades que definiu na previs�o de produ��o, dentro do intervalo de tempo selecionado.];
                SourceExpr="Forecast Quantity";
                Editable=FALSE }

    { 10  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the unit of measure code that is valid for the production forecast entry.;
                           PTG="Especifica o c�digo de unidade de medida que � v lido para o movimento de previs�o de produ��o. "];
                SourceExpr="Unit of Measure Code";
                Editable=FALSE }

    { 12  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the valid number of units that the unit of measure code represents for the production forecast entry.;
                           PTG=Especifica o n�mero v alido de unidades que o c�digo de unidade de medida representa para o movimento de previs�o de produ��o.];
                SourceExpr="Qty. per Unit of Measure";
                Editable=FALSE }

    { 18  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the code for the location that is linked to the entry.;
                           PTG=Especifica o c�digo para a localiza��o que esta associada e este movimento.];
                SourceExpr="Location Code";
                Editable=FALSE }

    { 22  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies that the forecast entry is for a component item.;
                           PTG=Especifica que o movimento de previs�o � para um produto componente.];
                SourceExpr="Component Forecast";
                Editable=FALSE }

    { 24  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the entry number of the production forecast line.;
                           PTG=Especifica o n�mero de movimento da linha de previs�o de produ��o.];
                SourceExpr="Entry No.";
                Visible=FALSE;
                Editable=FALSE }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {

    BEGIN
    END.
  }
}

