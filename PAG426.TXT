OBJECT Page 426 Vendor Bank Account List
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0;
  }
  PROPERTIES
  {
    Editable=No;
    CaptionML=[ENU=Vendor Bank Account List;
               PTG=Lista Conta Banc ria Fornecedor];
    SourceTable=Table288;
    DataCaptionFields=Vendor No.;
    PageType=List;
    CardPageID=Vendor Bank Account Card;
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 2   ;2   ;Field     ;
                ToolTipML=ENU=Specifies a code to identify this vendor bank account.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr=Code }

    { 4   ;2   ;Field     ;
                ToolTipML=ENU=Specifies the name of the bank where the vendor has this bank account.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr=Name }

    { 87  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the postal code of the address.;
                SourceExpr="Post Code";
                Visible=FALSE }

    { 89  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the country/region code of the address.;
                SourceExpr="Country/Region Code";
                Visible=FALSE }

    { 91  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the telephone number of the bank where the vendor has the bank account.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Phone No." }

    { 93  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the fax number associated with the address.;
                SourceExpr="Fax No.";
                Visible=FALSE }

    { 23  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the name of the bank employee regularly contacted in connection with this bank account.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr=Contact }

    { 105 ;2   ;Field     ;
                ToolTipML=ENU=Specifies the number used by the bank for the bank account.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Bank Account No.";
                Visible=FALSE }

    { 6   ;2   ;Field     ;
                ToolTipML=ENU=Specifies the SWIFT code (international bank identifier code) of the bank where the vendor has the account.;
                SourceExpr="SWIFT Code";
                Visible=FALSE }

    { 10  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the bank account's international bank account number.;
                SourceExpr=IBAN;
                Visible=FALSE }

    { 8   ;2   ;Field     ;
                ToolTipML=ENU=Specifies the relevant currency code for the bank account.;
                ApplicationArea=#Suite;
                SourceExpr="Currency Code";
                Visible=FALSE }

    { 103 ;2   ;Field     ;
                ToolTipML=ENU=Specifies a code that determines the language associated with this bank account.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Language Code";
                Visible=FALSE }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {

    BEGIN
    END.
  }
}

