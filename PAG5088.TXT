OBJECT Page 5088 Campaign Statistics
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0;
  }
  PROPERTIES
  {
    Editable=No;
    CaptionML=[ENU=Campaign Statistics;
               PTG=Estat�sticas Campanha];
    LinksAllowed=No;
    SourceTable=Table5071;
    PageType=Card;
    OnAfterGetRecord=BEGIN
                       IF "Target Contacts Contacted" = 0 THEN
                         ResponseRate := 0
                       ELSE
                         ResponseRate := ROUND("Contacts Responded" / "Target Contacts Contacted" * 100,0.1);

                       IF "Contacts Responded" = 0 THEN BEGIN
                         AvgCostPerResp := 0;
                         AvgDurationPerResp := 0;
                       END ELSE BEGIN
                         AvgCostPerResp := ROUND("Cost (LCY)" / "Contacts Responded");
                         AvgDurationPerResp := ROUND("Duration (Min.)" / "Contacts Responded",0.01);
                       END;
                     END;

  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                CaptionML=[ENU=General;
                           PTG=Geral] }

    { 2   ;2   ;Field     ;
                ToolTipML=ENU=Specifies the number of target contacts that you have contacted with this campaign. The field is not editable.;
                SourceExpr="Target Contacts Contacted" }

    { 4   ;2   ;Field     ;
                ToolTipML=ENU=Specifies the number of contacts who have responded to the campaign. This field is not editable.;
                SourceExpr="Contacts Responded" }

    { 25  ;2   ;Field     ;
                CaptionML=[ENU=Response Rate %;
                           PTG=% Resposta];
                ToolTipML=ENU=Specifies how many participated in the campaign, represented as a percentage of the number of target contacts contacted.;
                DecimalPlaces=1:1;
                SourceExpr=ResponseRate }

    { 8   ;2   ;Field     ;
                ToolTipML=ENU=Specifies the total cost of all the interactions created as part of the campaign. This field is not editable.;
                SourceExpr="Cost (LCY)" }

    { 21  ;2   ;Field     ;
                CaptionML=[ENU=Avg. Cost per Response;
                           PTG=Custo M�dio p/ Resposta];
                ToolTipML=ENU=Specifies the cost of the campaign per response.;
                SourceExpr=AvgCostPerResp;
                AutoFormatType=1 }

    { 6   ;2   ;Field     ;
                ToolTipML=ENU=Specifies the total time it took to complete all the interactions linked to the campaign. This field is not editable.;
                DecimalPlaces=0:0;
                SourceExpr="Duration (Min.)" }

    { 17  ;2   ;Field     ;
                CaptionML=[ENU=Avg. Duration per Response;
                           PTG=Dura��o M�dia p/ Resposta];
                ToolTipML=ENU=Specifies how long the campaign took per response.;
                DecimalPlaces=0:0;
                SourceExpr=AvgDurationPerResp }

    { 1903771501;1;Group  ;
                CaptionML=[ENU=Opportunities;
                           PTG=Oportunidades] }

    { 10  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the number of opportunities created as part of this campaign. This field is not editable.;
                SourceExpr="No. of Opportunities" }

    { 12  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the estimated value of the opportunities created as part of this campaign. This field is not editable.;
                SourceExpr="Estimated Value (LCY)" }

    { 14  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the calculated current value of the opportunities linked to the campaign. This field is not editable.;
                SourceExpr="Calcd. Current Value (LCY)" }

  }
  CODE
  {
    VAR
      ResponseRate@1000 : Decimal;
      AvgCostPerResp@1001 : Decimal;
      AvgDurationPerResp@1002 : Decimal;

    BEGIN
    END.
  }
}

