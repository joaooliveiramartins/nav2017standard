OBJECT Page 1254 Text-to-Account Mapping Wksh.
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0.00.15052;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Text-to-Account Mapping;
               PTG=Mapeamento Texto para Conta];
    SourceTable=Table1251;
    DelayedInsert=Yes;
    DataCaptionFields=Mapping Text;
    PageType=ListPlus;
    AutoSplitKey=Yes;
    OnInit=BEGIN
             UpdateDefaultGLAccounts
           END;

    OnNewRecord=VAR
                  VendorFilter@1001 : Text;
                BEGIN
                  VendorFilter := GETFILTER("Vendor No.");
                  IF VendorFilter <> '' THEN
                    "Vendor No." := COPYSTR(VendorFilter,1,STRLEN(VendorFilter));
                END;

    OnQueryClosePage=BEGIN
                       EXIT(CheckEntriesAreConsistent);
                     END;

    ActionList=ACTIONS
    {
      { 11      ;0   ;ActionContainer;
                      ActionContainerType=ActionItems }
      { 9       ;1   ;Action    ;
                      Name=SetUpDefaultGLAccounts;
                      CaptionML=[ENU=Set Up Default Accounts;
                                 PTG=Configurar Contas Padr�o];
                      ToolTipML=ENU=Specifies how to manage certain aspects of purchases and payables, such as how to calculate and post discounts and whether to round invoices.;
                      ApplicationArea=#Basic,#Suite;
                      RunObject=Page 460;
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=Setup;
                      PromotedCategory=Process }
    }
  }
  CONTROLS
  {
    { 1   ;0   ;Container ;
                ContainerType=ContentArea }

    { 10  ;1   ;Group     ;
                GroupType=Group }

    { 8   ;2   ;Field     ;
                Name=DefaultDebitAccName;
                CaptionML=[ENU=Default Debit Account for Non-Item Lines;
                           PTG=Conta D�bito Padr�o para Linhas dif. Produtos];
                ToolTipML=ENU=Specifies the debit account that is automatically inserted on purchase lines that are created from electronic documents when the incoming document line does not contain an identifiable item. Any incoming document line that does not have a GTIN or the vendor's item number will be converted to a purchase line of type G/L Account, and the No. field on the purchase line will contain the account that you select in the Debit Acc. for Non-Item Lines field.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr=DefaultDebitAccName;
                Editable=FALSE;
                OnDrillDown=VAR
                              AccType@1000 : 'Debit,Credit';
                            BEGIN
                              SetUpDefaultGLAccounts(DefaultDebitAccNo,AccType::Debit);
                            END;
                             }

    { 12  ;2   ;Field     ;
                Name=DefaultCreditAccName;
                CaptionML=[ENU=Default Credit Account for Non-Item Lines;
                           PTG=Conta Cr�dito Padr�o para Linhas dif. Produtos];
                ToolTipML=ENU=Specifies the credit account that is automatically inserted on purchase credit memo lines that are created from electronic documents when the incoming document line does not contain an identifiable item. Any incoming document line that does not have a GTIN or the vendor's item number will be converted to a purchase line of type G/L Account, and the No. field on the purchase line will contain the account that you select in the Credit Acc. for Non-Item Lines field. For more information, see Debit Acc. for Non-Item Lines.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr=DefaultCreditAccName;
                Editable=FALSE;
                OnDrillDown=VAR
                              AccType@1000 : 'Debit,Credit';
                            BEGIN
                              SetUpDefaultGLAccounts(DefaultCreditAccNo,AccType::Credit);
                            END;
                             }

    { 2   ;1   ;Group     ;
                Name=Mapping Rules;
                GroupType=Repeater }

    { 3   ;2   ;Field     ;
                ToolTipML=ENU=Specifies the text on the payment that is used to map the payment to a customer, vendor, or general ledger account when you choose the Apply Automatically function in the Payment Reconciliation Journal window.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Mapping Text" }

    { 13  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the number of the vendor that incoming documents containing the mapping text will be created for, or that payments will be posted to.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Vendor No." }

    { 6   ;2   ;Field     ;
                ToolTipML=ENU=Specifies the debit account that payments with this text-to-account mapping are matched with when you choose the Apply Automatically function in the Payment Reconciliation Journal window.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Debit Acc. No." }

    { 7   ;2   ;Field     ;
                ToolTipML=ENU=Specifies the credit account that payments with this text-to-account mapping are applied to when you choose the Apply Automatically function in the Payment Reconciliation Journal window.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Credit Acc. No." }

    { 4   ;2   ;Field     ;
                ToolTipML=ENU=Specifies the type of balancing account that payments or incoming document records with this text-to-account mapping are created for. The Bank Account option is used for incoming documents only.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Bal. Source Type" }

    { 5   ;2   ;Field     ;
                ToolTipML=ENU=Specifies the balancing account in the general ledger or on bank accounts that payments or incoming document records with this text-to-account mapping are created for.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Bal. Source No." }

  }
  CODE
  {
    VAR
      PurchasesPayablesSetup@1003 : Record 312;
      DefaultDebitAccNo@1001 : Code[20];
      DefaultDebitAccName@1002 : Text[50];
      DefaultCreditAccNo@1004 : Code[20];
      DefaultCreditAccName@1000 : Text[50];

    LOCAL PROCEDURE UpdateDefaultGLAccounts@1();
    VAR
      GLAccount@1000 : Record 15;
    BEGIN
      PurchasesPayablesSetup.GET;
      DefaultDebitAccNo := PurchasesPayablesSetup."Debit Acc. for Non-Item Lines";
      DefaultDebitAccName := '';
      DefaultCreditAccNo := PurchasesPayablesSetup."Credit Acc. for Non-Item Lines";
      DefaultCreditAccName := '';
      IF GLAccount.GET(DefaultDebitAccNo) THEN
        DefaultDebitAccName := STRSUBSTNO('%1 - %2',DefaultDebitAccNo,GLAccount.Name);
      IF GLAccount.GET(DefaultCreditAccNo) THEN
        DefaultCreditAccName := STRSUBSTNO('%1 - %2',DefaultCreditAccNo,GLAccount.Name);
      CurrPage.UPDATE;
    END;

    LOCAL PROCEDURE SetUpDefaultGLAccounts@2(Account@1001 : Code[20];Type@1002 : 'Debit,Credit');
    VAR
      GLAccount@1000 : Record 15;
    BEGIN
      GLAccount."No." := Account;
      GLAccount.SETRANGE("Direct Posting",TRUE);
      IF PAGE.RUNMODAL(PAGE::"G/L Account List",GLAccount) = ACTION::LookupOK THEN
        IF Account <> GLAccount."No." THEN BEGIN
          PurchasesPayablesSetup.GET;
          CASE Type OF
            Type::Debit:
              PurchasesPayablesSetup."Debit Acc. for Non-Item Lines" := GLAccount."No.";
            Type::Credit:
              PurchasesPayablesSetup."Credit Acc. for Non-Item Lines" := GLAccount."No.";
          END;
          PurchasesPayablesSetup.MODIFY;
          UpdateDefaultGLAccounts;
        END;
    END;

    BEGIN
    END.
  }
}

