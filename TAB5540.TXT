OBJECT Table 5540 Timeline Event
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Timeline Event;
               PTG=Calend�rio Evento];
  }
  FIELDS
  {
    { 1   ;   ;Transaction Type    ;Option        ;CaptionML=[ENU=Transaction Type;
                                                              PTG=Natureza Transa��o];
                                                   OptionCaptionML=[ENU=None,Initial,Fixed Supply,Adjustable Supply,New Supply,Fixed Demand,Expected Demand;
                                                                    PTG=Nenhuma,Inicial,Fornecimento Fixo,Fornecimento Ajust�vel,Novo Fornecimento,Requisi��o Fixa,Requisi��o Esperada];
                                                   OptionString=None,Initial,Fixed Supply,Adjustable Supply,New Supply,Fixed Demand,Expected Demand }
    { 3   ;   ;Description         ;Text250       ;CaptionML=[ENU=Description;
                                                              PTG=Descri��o] }
    { 4   ;   ;Original Date       ;Date          ;CaptionML=[ENU=Original Date;
                                                              PTG=Data Original] }
    { 5   ;   ;New Date            ;Date          ;CaptionML=[ENU=New Date;
                                                              PTG=Nova Data] }
    { 6   ;   ;ChangeRefNo         ;Text250       ;CaptionML=[ENU=ChangeRefNo;
                                                              PTG=AlterarN�Ref] }
    { 9   ;   ;Source Line ID      ;RecordID      ;CaptionML=[ENU=Source Line ID;
                                                              PTG=ID Linha Origem];
                                                   Editable=No }
    { 10  ;   ;Source Document ID  ;RecordID      ;CaptionML=[ENU=Source Document ID;
                                                              PTG=ID Documento Origem];
                                                   Editable=No }
    { 20  ;   ;Original Quantity   ;Decimal       ;CaptionML=[ENU=Original Quantity;
                                                              PTG=Quantidade Original];
                                                   DecimalPlaces=0:5 }
    { 21  ;   ;New Quantity        ;Decimal       ;CaptionML=[ENU=New Quantity;
                                                              PTG=Nova Quantidade];
                                                   DecimalPlaces=0:5 }
    { 1000;   ;ID                  ;Integer       ;AutoIncrement=No;
                                                   CaptionML=[ENU=ID;
                                                              PTG=ID];
                                                   MinValue=0;
                                                   NotBlank=Yes }
  }
  KEYS
  {
    {    ;ID                                      ;Clustered=Yes }
    {    ;New Date,ID                              }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    PROCEDURE TransferToTransactionTable@19(VAR TimelineEvent@1003 : Record 5540;VAR transactionTable@1002 : DotNet "'Microsoft.Dynamics.Framework.UI.WinForms.DataVisualization.Timeline, Version=10.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35'.Microsoft.Dynamics.Framework.UI.WinForms.DataVisualization.TimelineVisualization.DataModel+TransactionDataTable");
    VAR
      transactionRow@1001 : DotNet "'Microsoft.Dynamics.Framework.UI.WinForms.DataVisualization.Timeline, Version=10.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35'.Microsoft.Dynamics.Framework.UI.WinForms.DataVisualization.TimelineVisualization.DataModel+TransactionRow";
    BEGIN
      transactionTable := transactionTable.TransactionDataTable;
      TimelineEvent.RESET;
      IF TimelineEvent.FIND('-') THEN
        REPEAT
          transactionRow := transactionTable.NewRow;
          transactionRow.RefNo := FORMAT(TimelineEvent.ID);
          transactionRow.ChangeRefNo := TimelineEvent.ChangeRefNo;
          transactionRow.TransactionType := TimelineEvent."Transaction Type";
          transactionRow.Description := TimelineEvent.Description;
          transactionRow.OriginalDate := CREATEDATETIME(TimelineEvent."Original Date",DefaultTime);
          transactionRow.NewDate := CREATEDATETIME(TimelineEvent."New Date",DefaultTime);
          transactionRow.OriginalQuantity := TimelineEvent."Original Quantity";
          transactionRow.NewQuantity := TimelineEvent."New Quantity";
          transactionTable.Rows.Add(transactionRow);
        UNTIL (TimelineEvent.NEXT = 0);
    END;

    PROCEDURE DefaultTime@1() : Time;
    BEGIN
      EXIT(0T);
    END;

    BEGIN
    END.
  }
}

