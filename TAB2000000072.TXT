OBJECT Table 2000000072 Profile
{
  OBJECT-PROPERTIES
  {
    Date=13/04/17;
    Time=14:36:26;
    Version List=NAVW110.0;
  }
  PROPERTIES
  {
    DataPerCompany=No;
    CaptionML=[ENU=Profile;
               PTG=Perfil];
  }
  FIELDS
  {
    { 3   ;   ;Profile ID          ;Code30        ;CaptionML=[ENU=Profile ID;
                                                              PTG=ID Perfil] }
    { 12  ;   ;Description         ;Text250       ;CaptionML=[ENU=Description;
                                                              PTG=Descri��o] }
    { 15  ;   ;Role Center ID      ;Integer       ;TableRelation=AllObjWithCaption."Object ID" WHERE (Object Type=CONST(Page));
                                                   CaptionML=[ENU=Role Center ID;
                                                              PTG=ID Centro Perfil] }
    { 18  ;   ;Default Role Center ;Boolean       ;CaptionML=[ENU=Default Role Center;
                                                              PTG=Centro Perfil Pr�-Definido] }
    { 21  ;   ;Use Comments        ;Boolean       ;CaptionML=[ENU=Use Comments;
                                                              PTG=Usar Coment�rios] }
    { 24  ;   ;Use Notes           ;Boolean       ;CaptionML=[ENU=Use Notes;
                                                              PTG=Usar Notas] }
    { 27  ;   ;Use Record Notes    ;Boolean       ;CaptionML=[ENU=Use Record Notes;
                                                              PTG=Usar Notas Tabela] }
    { 30  ;   ;Record Notebook     ;Text250       ;CaptionML=[ENU=Record Notebook;
                                                              PTG=Bloco de Notas Tabela] }
    { 33  ;   ;Use Page Notes      ;Boolean       ;CaptionML=[ENU=Use Page Notes;
                                                              PTG=Usar Notas P�gina] }
    { 36  ;   ;Page Notebook       ;Text250       ;CaptionML=[ENU=Page Notebook;
                                                              PTG=Bloco de Notas P�gina] }
    { 39  ;   ;Disable Personalization;Boolean    ;CaptionML=[ENU=Disable Personalization;
                                                              PTG=Desativar Personaliza��o] }
  }
  KEYS
  {
    {    ;Profile ID                              ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    END.
  }
}

