OBJECT Page 5116 Salesperson/Purchaser Card
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Salesperson/Purchaser Card;
               PTG=Ficha Vendedor/Comprador];
    SourceTable=Table13;
    PageType=Card;
    OnOpenPage=VAR
                 CRMIntegrationManagement@1000 : Codeunit 5330;
               BEGIN
                 CRMIntegrationEnabled := CRMIntegrationManagement.IsCRMIntegrationEnabled;
               END;

    OnInsertRecord=BEGIN
                     IF xRec.Code = '' THEN
                       RESET;
                   END;

    OnAfterGetCurrRecord=VAR
                           CRMCouplingManagement@1001 : Codeunit 5331;
                         BEGIN
                           IF CRMIntegrationEnabled THEN
                             CRMIsCoupledToRecord := CRMCouplingManagement.IsRecordCoupledToCRM(RECORDID);
                         END;

    ActionList=ACTIONS
    {
      { 1900000003;0 ;ActionContainer;
                      ActionContainerType=RelatedInformation }
      { 19      ;1   ;ActionGroup;
                      CaptionML=[ENU=&Salesperson;
                                 PTG=Ve&ndedor];
                      Image=SalesPerson }
      { 28      ;2   ;Action    ;
                      CaptionML=[ENU=Tea&ms;
                                 PTG=E&quipas];
                      RunObject=Page 5107;
                      RunPageView=SORTING(Salesperson Code);
                      RunPageLink=Salesperson Code=FIELD(Code);
                      Image=TeamSales }
      { 25      ;2   ;Action    ;
                      CaptionML=[ENU=Con&tacts;
                                 PTG=Con&tactos];
                      ToolTipML=ENU=View a list of contacts that are associated with the salesperson/purchaser.;
                      ApplicationArea=#RelationshipMgmt;
                      RunObject=Page 5052;
                      RunPageView=SORTING(Salesperson Code);
                      RunPageLink=Salesperson Code=FIELD(Code);
                      Image=CustomerContact }
      { 23      ;2   ;Action    ;
                      ShortCutKey=Shift+Ctrl+D;
                      CaptionML=[ENU=Dimensions;
                                 PTG=Dimens�es];
                      ToolTipML=ENU=View or edits dimensions, such as area, project, or department, that you can assign to sales and purchase documents to distribute costs and analyze transaction history.;
                      ApplicationArea=#Suite;
                      RunObject=Page 540;
                      RunPageLink=Table ID=CONST(13),
                                  No.=FIELD(Code);
                      Image=Dimensions }
      { 21      ;2   ;Action    ;
                      ShortCutKey=F7;
                      CaptionML=[ENU=Statistics;
                                 PTG=Estat�sticas];
                      ToolTipML=ENU=View statistical information, such as the value of posted entries, for the record.;
                      ApplicationArea=#RelationshipMgmt;
                      RunObject=Page 5117;
                      RunPageLink=Code=FIELD(Code);
                      Promoted=Yes;
                      Image=Statistics;
                      PromotedCategory=Process }
      { 26      ;2   ;Action    ;
                      CaptionML=[ENU=C&ampaigns;
                                 PTG=C&ampanhas];
                      RunObject=Page 5087;
                      RunPageView=SORTING(Salesperson Code);
                      RunPageLink=Salesperson Code=FIELD(Code);
                      Image=Campaign }
      { 27      ;2   ;Action    ;
                      CaptionML=[ENU=S&egments;
                                 PTG=S&egmentos];
                      ToolTipML=ENU=View a list of all segments.;
                      ApplicationArea=#RelationshipMgmt;
                      RunObject=Page 5093;
                      RunPageView=SORTING(Salesperson Code);
                      RunPageLink=Salesperson Code=FIELD(Code);
                      Image=Segment }
      { 33      ;2   ;Separator ;
                      CaptionML=[ENU="";
                                 PTG=""] }
      { 32      ;2   ;Action    ;
                      ShortCutKey=Ctrl+F7;
                      CaptionML=[ENU=Interaction Log E&ntries;
                                 PTG=Movs. I&ntera��o];
                      ToolTipML=ENU=View interaction log entries for the salesperson/purchaser.;
                      ApplicationArea=#RelationshipMgmt;
                      RunObject=Page 5076;
                      RunPageView=SORTING(Salesperson Code);
                      RunPageLink=Salesperson Code=FIELD(Code);
                      Image=InteractionLog }
      { 76      ;2   ;Action    ;
                      CaptionML=[ENU=Postponed &Interactions;
                                 PTG=&Intera��es Adiadas];
                      ToolTipML=ENU=View postponed interactions for the salesperson/purchaser.;
                      ApplicationArea=#RelationshipMgmt;
                      RunObject=Page 5082;
                      RunPageView=SORTING(Salesperson Code);
                      RunPageLink=Salesperson Code=FIELD(Code);
                      Image=PostponedInteractions }
      { 35      ;2   ;Action    ;
                      CaptionML=[ENU=T&o-dos;
                                 PTG=A��es a E&fetuar];
                      RunObject=Page 5096;
                      RunPageView=SORTING(Salesperson Code);
                      RunPageLink=Salesperson Code=FIELD(Code),
                                  System To-do Type=FILTER(Organizer|Salesperson Attendee);
                      Image=TaskList }
      { 78      ;2   ;ActionGroup;
                      CaptionML=[ENU=Oppo&rtunities;
                                 PTG=Opo&rtunidades];
                      Image=OpportunityList }
      { 34      ;3   ;Action    ;
                      CaptionML=[ENU=List;
                                 PTG=Lista];
                      ToolTipML=ENU=View a list of all salespeople/purchasers.;
                      ApplicationArea=#RelationshipMgmt;
                      RunObject=Page 5123;
                      RunPageView=SORTING(Salesperson Code);
                      RunPageLink=Salesperson Code=FIELD(Code);
                      Image=OpportunitiesList }
      { 13      ;1   ;ActionGroup;
                      Name=ActionGroupCRM;
                      CaptionML=[ENU=Dynamics CRM;
                                 PTG=Dynamics CRM];
                      Visible=CRMIntegrationEnabled }
      { 11      ;2   ;Action    ;
                      Name=CRMGotoSystemUser;
                      CaptionML=[ENU=User;
                                 PTG=Utilizador];
                      ToolTipML=[ENU=Open the coupled Microsoft Dynamics CRM system user.;
                                 PTG=Abrir utilizador de sistema do Microsoft Dynamics CRM, acoplado.];
                      ApplicationArea=#Suite,#RelationshipMgmt;
                      Image=CoupledUser;
                      OnAction=VAR
                                 CRMIntegrationManagement@1000 : Codeunit 5330;
                               BEGIN
                                 CRMIntegrationManagement.ShowCRMEntityFromRecordID(RECORDID);
                               END;
                                }
      { 20      ;2   ;Action    ;
                      Name=CRMSynchronizeNow;
                      AccessByPermission=TableData 5331=IM;
                      CaptionML=[ENU=Synchronize Now;
                                 PTG=Sincronizar Agora];
                      ToolTipML=[ENU=Send or get updated data to or from Microsoft Dynamics CRM.;
                                 PTG=Enviar ou obter dados atualizados, para ou do Microsoft Dynamics CRM.];
                      Image=Refresh;
                      OnAction=VAR
                                 CRMIntegrationManagement@1001 : Codeunit 5330;
                               BEGIN
                                 CRMIntegrationManagement.UpdateOneNow(RECORDID);
                               END;
                                }
      { 18      ;2   ;ActionGroup;
                      Name=Coupling;
                      CaptionML=[@@@=Coupling is a noun;
                                 ENU=Coupling;
                                 PTG=Acoplamento];
                      ToolTipML=[ENU=Create, change, or delete a coupling between the Microsoft Dynamics NAV record and a Microsoft Dynamics CRM record.;
                                 PTG=Criar, modificar, ou eliminar um acoplamento entre o registo do Microsoft Dynamcis NAV e um registo do Microsoft Dynamics CRM.];
                      Image=LinkAccount }
      { 9       ;3   ;Action    ;
                      Name=ManageCRMCoupling;
                      AccessByPermission=TableData 5331=IM;
                      CaptionML=[ENU=Set Up Coupling;
                                 PTG=Configurar Acoplamento];
                      ToolTipML=[ENU=Create or modify the coupling to a Microsoft Dynamics CRM user.;
                                 PTG=Criar ou modificar o acoplamento a um utilizador do Microsoft Dynamics CRM.];
                      ApplicationArea=#Suite,#RelationshipMgmt;
                      Image=LinkAccount;
                      OnAction=VAR
                                 CRMIntegrationManagement@1000 : Codeunit 5330;
                               BEGIN
                                 CRMIntegrationManagement.DefineCoupling(RECORDID);
                               END;
                                }
      { 7       ;3   ;Action    ;
                      Name=DeleteCRMCoupling;
                      AccessByPermission=TableData 5331=IM;
                      CaptionML=[ENU=Delete Coupling;
                                 PTG=Eliminar Acoplamento];
                      ToolTipML=[ENU=Delete the coupling to a Microsoft Dynamics CRM user.;
                                 PTG=Eliminar o acoplamento a um utilizador do Microsoft Dynamics CRM, acoplado.];
                      ApplicationArea=#Suite,#RelationshipMgmt;
                      Enabled=CRMIsCoupledToRecord;
                      Image=UnLinkAccount;
                      OnAction=VAR
                                 CRMCouplingManagement@1000 : Codeunit 5331;
                               BEGIN
                                 CRMCouplingManagement.RemoveCoupling(RECORDID);
                               END;
                                }
      { 1900000004;0 ;ActionContainer;
                      ActionContainerType=ActionItems }
      { 36      ;1   ;Action    ;
                      AccessByPermission=TableData 5062=R;
                      CaptionML=[ENU=Create &Interact;
                                 PTG=Criar &Intera��o];
                      ToolTipML=ENU=Create an interaction with a specified contact.;
                      ApplicationArea=#RelationshipMgmt;
                      Promoted=Yes;
                      Image=CreateInteraction;
                      PromotedCategory=Process;
                      OnAction=BEGIN
                                 CreateInteraction;
                               END;
                                }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                CaptionML=[ENU=General;
                           PTG=Geral] }

    { 2   ;2   ;Field     ;
                ToolTipML=ENU=Specifies a code for the salesperson or purchaser.;
                ApplicationArea=#All;
                SourceExpr=Code }

    { 4   ;2   ;Field     ;
                ToolTipML=ENU=Specifies the name of the salesperson or purchaser.;
                ApplicationArea=#All;
                SourceExpr=Name }

    { 22  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the salesperson's job title.;
                ApplicationArea=#RelationshipMgmt;
                SourceExpr="Job Title" }

    { 6   ;2   ;Field     ;
                ToolTipML=ENU=Specifies the percentage to use to calculate the salesperson's commission.;
                ApplicationArea=#RelationshipMgmt;
                SourceExpr="Commission %" }

    { 10  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the salesperson's telephone number.;
                ApplicationArea=#RelationshipMgmt;
                SourceExpr="Phone No." }

    { 8   ;2   ;Field     ;
                ToolTipML=ENU=Specifies the salesperson's email address.;
                ApplicationArea=#RelationshipMgmt;
                SourceExpr="E-Mail" }

    { 12  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the date of the next to-do assigned to the salesperson.;
                SourceExpr="Next To-do Date" }

    { 1905885101;1;Group  ;
                CaptionML=[ENU=Invoicing;
                           PTG=Fatura��o] }

    { 14  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the dimension value code for the global dimension 1 you have assigned to the campaign.;
                ApplicationArea=#Suite;
                SourceExpr="Global Dimension 1 Code" }

    { 16  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the dimension value code for the global dimension 2 you have assigned to the campaign.;
                ApplicationArea=#Suite;
                SourceExpr="Global Dimension 2 Code" }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 3   ;1   ;Part      ;
                SubPageLink=Code=FIELD(Code);
                PagePartID=Page5108;
                PartType=Page }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {
    VAR
      CRMIntegrationEnabled@1001 : Boolean;
      CRMIsCoupledToRecord@1000 : Boolean;

    BEGIN
    END.
  }
}

