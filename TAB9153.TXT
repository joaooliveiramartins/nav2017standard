OBJECT Table 9153 My Account
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0;
  }
  PROPERTIES
  {
    CaptionML=[ENU=My Account;
               PTG=Minha Conta];
  }
  FIELDS
  {
    { 1   ;   ;User ID             ;Code50        ;TableRelation=User."User Name";
                                                   ValidateTableRelation=No;
                                                   TestTableRelation=No;
                                                   CaptionML=[ENU=User ID;
                                                              PTG=ID Utilizador] }
    { 2   ;   ;Account No.         ;Code20        ;TableRelation="G/L Account";
                                                   OnValidate=BEGIN
                                                                SetAccountFields;
                                                              END;

                                                   CaptionML=[ENU=Account No.;
                                                              PTG=N� Conta];
                                                   NotBlank=Yes }
    { 3   ;   ;Name                ;Text50        ;CaptionML=[ENU=Name;
                                                              PTG=Nome];
                                                   Editable=No }
    { 4   ;   ;Balance             ;Decimal       ;FieldClass=FlowField;
                                                   CalcFormula=Sum("G/L Entry".Amount WHERE (G/L Account No.=FIELD(Account No.)));
                                                   CaptionML=[ENU=Balance;
                                                              PTG=Balan�o];
                                                   Editable=No }
  }
  KEYS
  {
    {    ;User ID,Account No.                     ;Clustered=Yes }
    {    ;Name                                     }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    LOCAL PROCEDURE SetAccountFields@1();
    VAR
      GLAccount@1000 : Record 15;
    BEGIN
      GLAccount.GET("Account No.");
      Name := GLAccount.Name;
    END;

    BEGIN
    END.
  }
}

