OBJECT Table 99000765 Manufacturing Setup
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Manufacturing Setup;
               PTG=Configura��o Produ��o];
    LookupPageID=Page99000768;
  }
  FIELDS
  {
    { 1   ;   ;Primary Key         ;Code10        ;CaptionML=[ENU=Primary Key;
                                                              PTG=Chave Prim�ria];
                                                   Editable=No }
    { 7   ;   ;Normal Starting Time;Time          ;CaptionML=[ENU=Normal Starting Time;
                                                              PTG=Hora In�cio Normal] }
    { 8   ;   ;Normal Ending Time  ;Time          ;CaptionML=[ENU=Normal Ending Time;
                                                              PTG=Hora Final Normal] }
    { 9   ;   ;Doc. No. Is Prod. Order No.;Boolean;CaptionML=[ENU=Doc. No. Is Prod. Order No.;
                                                              PTG=N� Doc. � N� Ordem Produ��o] }
    { 11  ;   ;Cost Incl. Setup    ;Boolean       ;CaptionML=[ENU=Cost Incl. Setup;
                                                              PTG=Custo Incluindo Prepara��o] }
    { 12  ;   ;Dynamic Low-Level Code;Boolean     ;CaptionML=[ENU=Dynamic Low-Level Code;
                                                              PTG=C�d. N�vel Abaixo Din�mico] }
    { 18  ;   ;Planning Warning    ;Boolean       ;CaptionML=[ENU=Planning Warning;
                                                              PTG=Aviso de Planeamento] }
    { 20  ;   ;Simulated Order Nos.;Code10        ;TableRelation="No. Series";
                                                   CaptionML=[ENU=Simulated Order Nos.;
                                                              PTG=N� S�ries Ordens Simuladas] }
    { 21  ;   ;Planned Order Nos.  ;Code10        ;TableRelation="No. Series";
                                                   CaptionML=[ENU=Planned Order Nos.;
                                                              PTG=N� S�ries Ordens Planeadas] }
    { 22  ;   ;Firm Planned Order Nos.;Code10     ;TableRelation="No. Series";
                                                   CaptionML=[ENU=Firm Planned Order Nos.;
                                                              PTG=N� S�ries Ordens Planeadas Firmes] }
    { 23  ;   ;Released Order Nos. ;Code10        ;TableRelation="No. Series";
                                                   CaptionML=[ENU=Released Order Nos.;
                                                              PTG=N� S�ries Ordens Libertas] }
    { 29  ;   ;Work Center Nos.    ;Code10        ;TableRelation="No. Series";
                                                   AccessByPermission=TableData 99000758=R;
                                                   CaptionML=[ENU=Work Center Nos.;
                                                              PTG=N� S�ries Centro Trabalho] }
    { 30  ;   ;Machine Center Nos. ;Code10        ;TableRelation="No. Series";
                                                   AccessByPermission=TableData 99000758=R;
                                                   CaptionML=[ENU=Machine Center Nos.;
                                                              PTG=N� S�ries Centro M�quina] }
    { 31  ;   ;Production BOM Nos. ;Code10        ;TableRelation="No. Series";
                                                   AccessByPermission=TableData 99000771=R;
                                                   CaptionML=[ENU=Production BOM Nos.;
                                                              PTG=N� S�ries L.M. Produ��o] }
    { 32  ;   ;Routing Nos.        ;Code10        ;TableRelation="No. Series";
                                                   AccessByPermission=TableData 99000760=R;
                                                   CaptionML=[ENU=Routing Nos.;
                                                              PTG=N� S�ries Gamas Operat�rias] }
    { 35  ;   ;Current Production Forecast;Code10 ;TableRelation="Production Forecast Name".Name;
                                                   CaptionML=[ENU=Current Production Forecast;
                                                              PTG=Previs�o de Produ��o Atual] }
    { 37  ;   ;Use Forecast on Locations;Boolean  ;CaptionML=[ENU=Use Forecast on Locations;
                                                              PTG=Usar Previs�o em Localiza��es] }
    { 38  ;   ;Combined MPS/MRP Calculation;Boolean;
                                                   AccessByPermission=TableData 99000829=R;
                                                   CaptionML=[ENU=Combined MPS/MRP Calculation;
                                                              PTG=C�lculo MPS/MRP Combinado] }
    { 39  ;   ;Components at Location;Code10      ;TableRelation=Location WHERE (Use As In-Transit=CONST(No));
                                                   CaptionML=[ENU=Components at Location;
                                                              PTG=Componentes na Localiza��o] }
    { 40  ;   ;Default Dampener Period;DateFormula;OnValidate=VAR
                                                                CalendarMgt@1000 : Codeunit 7600;
                                                              BEGIN
                                                                CalendarMgt.CheckDateFormulaPositive("Default Dampener Period");
                                                              END;

                                                   CaptionML=[ENU=Default Dampener Period;
                                                              PTG=Per�odo Amortecedor Padr�o] }
    { 41  ;   ;Default Dampener %  ;Decimal       ;CaptionML=[ENU=Default Dampener %;
                                                              PTG=% Amortecedora Padr�o];
                                                   DecimalPlaces=1:1;
                                                   MinValue=0 }
    { 42  ;   ;Default Safety Lead Time;DateFormula;
                                                   CaptionML=[ENU=Default Safety Lead Time;
                                                              PTG=Prazo Seguran�a Pr�-def.] }
    { 43  ;   ;Blank Overflow Level;Option        ;CaptionML=[ENU=Blank Overflow Level;
                                                              PTG=N�vel Excedente Vazio];
                                                   OptionCaptionML=[ENU=Allow Default Calculation,Use Item/SKU Values Only;
                                                                    PTG=Permitir C�lculo Padr�o,Usar Apenas Valores Produto/UA];
                                                   OptionString=Allow Default Calculation,Use Item/SKU Values Only }
    { 50  ;   ;Show Capacity In    ;Code10        ;TableRelation="Capacity Unit of Measure".Code;
                                                   CaptionML=[ENU=Show Capacity In;
                                                              PTG=Mostrar Capacidade Em] }
    { 5500;   ;Preset Output Quantity;Option      ;CaptionML=[ENU=Preset Output Quantity;
                                                              PTG=Predefinir Quantidade Sa�da];
                                                   OptionCaptionML=[ENU=Expected Quantity,Zero on All Operations,Zero on Last Operation;
                                                                    PTG=Quantidade Esperada,Zero em Todas as Opera��es,Zero na �ltima Opera��o];
                                                   OptionString=Expected Quantity,Zero on All Operations,Zero on Last Operation }
  }
  KEYS
  {
    {    ;Primary Key                             ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    END.
  }
}

