OBJECT Table 9805 Table Filter
{
  OBJECT-PROPERTIES
  {
    Date=07/09/12;
    Time=12:00:00;
    Version List=NAVW17.00;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Table Filter;
               PTG=Filtro Tabela];
  }
  FIELDS
  {
    { 1   ;   ;Table Number        ;Integer       ;CaptionML=[ENU=Table Number;
                                                              PTG=N�mero Tabela] }
    { 2   ;   ;Line No.            ;Integer       ;CaptionML=[ENU=Line No.;
                                                              PTG=N� Linha] }
    { 4   ;   ;Table Name          ;Text30        ;CaptionML=[ENU=Table Name;
                                                              PTG=Nome Tabela] }
    { 5   ;   ;Field Number        ;Integer       ;TableRelation=Field.No. WHERE (TableNo=FIELD(Table Number));
                                                   OnValidate=VAR
                                                                Field@1000 : Record 2000000041;
                                                              BEGIN
                                                                IF xRec."Field Number" = "Field Number" THEN
                                                                  EXIT;

                                                                Field.GET("Table Number","Field Number");
                                                                CheckDuplicateField(Field);

                                                                "Field Caption" := Field."Field Caption";
                                                                "Field Filter" := '';
                                                              END;

                                                   CaptionML=[ENU=Field Number;
                                                              PTG=N�mero Campo] }
    { 6   ;   ;Field Name          ;Text30        ;CaptionML=[ENU=Field Name;
                                                              PTG=Nome Campo] }
    { 7   ;   ;Field Caption       ;Text80        ;CaptionML=[ENU=Field Caption;
                                                              PTG=Legenda Campo] }
    { 8   ;   ;Field Filter        ;Text250       ;CaptionML=[ENU=Field Filter;
                                                              PTG=Filtro Campo] }
  }
  KEYS
  {
    {    ;Table Number,Line No.                   ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {
    VAR
      Text001@1000 : TextConst '@@@=The filter for the field <Field Number> <Field Name> already exists. Example: The filter for the field 15 Base Unit of Measure already exists.;ENU=The filter for the field %1 %2 already exists.;PTG=J� existe filtro para o campo %1 %2.';

    PROCEDURE CheckDuplicateField@1(Field@1000 : Record 2000000041);
    VAR
      TableFilter@1001 : Record 9805;
    BEGIN
      TableFilter.COPY(Rec);
      RESET;
      SETRANGE("Table Number",Field.TableNo);
      SETRANGE("Field Number",Field."No.");
      SETFILTER("Line No.",'<>%1',"Line No.");
      IF NOT ISEMPTY THEN
        ERROR(Text001,Field."No.",Field."Field Caption");
      COPY(TableFilter);
    END;

    BEGIN
    END.
  }
}

