OBJECT Table 5603 FA Setup
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0,NAVPTSS81.00;
  }
  PROPERTIES
  {
    Permissions=TableData 5629=r;
    CaptionML=[ENU=FA Setup;
               PTG=Configura��o Imobilizado];
  }
  FIELDS
  {
    { 1   ;   ;Primary Key         ;Code10        ;CaptionML=[ENU=Primary Key;
                                                              PTG=Chave Prim�ria] }
    { 3   ;   ;Allow Posting to Main Assets;Boolean;
                                                   CaptionML=[ENU=Allow Posting to Main Assets;
                                                              PTG=Permite Reg. p/ Imob. Principal] }
    { 4   ;   ;Default Depr. Book  ;Code10        ;TableRelation="Depreciation Book";
                                                   OnValidate=BEGIN
                                                                IF "Insurance Depr. Book" = '' THEN
                                                                  VALIDATE("Insurance Depr. Book","Default Depr. Book");
                                                              END;

                                                   CaptionML=[ENU=Default Depr. Book;
                                                              PTG=Livro Amortiza��o Padr�o] }
    { 5   ;   ;Allow FA Posting From;Date         ;CaptionML=[ENU=Allow FA Posting From;
                                                              PTG=Permite Registo Imob. De] }
    { 6   ;   ;Allow FA Posting To ;Date          ;CaptionML=[ENU=Allow FA Posting To;
                                                              PTG=Permite Regisot Imob. At�] }
    { 7   ;   ;Insurance Depr. Book;Code10        ;TableRelation="Depreciation Book";
                                                   OnValidate=VAR
                                                                InsCoverageLedgEntry@1001 : Record 5629;
                                                                MakeInsCoverageLedgEntry@1000 : Codeunit 5657;
                                                              BEGIN
                                                                IF InsCoverageLedgEntry.ISEMPTY THEN
                                                                  EXIT;

                                                                IF "Insurance Depr. Book" <> xRec."Insurance Depr. Book" THEN
                                                                  MakeInsCoverageLedgEntry.UpdateInsCoverageLedgerEntryFromFASetup("Insurance Depr. Book");
                                                              END;

                                                   CaptionML=[ENU=Insurance Depr. Book;
                                                              PTG=Livro Amortiza��o Seguro] }
    { 8   ;   ;Automatic Insurance Posting;Boolean;InitValue=Yes;
                                                   CaptionML=[ENU=Automatic Insurance Posting;
                                                              PTG=Registo Autom�tico Seguro] }
    { 9   ;   ;Fixed Asset Nos.    ;Code10        ;TableRelation="No. Series";
                                                   CaptionML=[ENU=Fixed Asset Nos.;
                                                              PTG=N� S�ries Imobilizado] }
    { 10  ;   ;Insurance Nos.      ;Code10        ;TableRelation="No. Series";
                                                   AccessByPermission=TableData 5628=R;
                                                   CaptionML=[ENU=Insurance Nos.;
                                                              PTG=N� S�ries seguros] }
    { 31022890;;Mod.33.10-DL 111/88;Code10        ;TableRelation="Depreciation Book";
                                                   CaptionML=[ENU=Mod.33.10-DL 111/88;
                                                              PTG=Mod.33.10-DL 111/88] }
    { 31022891;;Mod.33.11-DL 49/91 ;Code10        ;TableRelation="Depreciation Book";
                                                   CaptionML=[ENU=Mod.33.11-DL 49/91;
                                                              PTG=Mod.33.11-DL 49/91] }
    { 31022892;;Mod.33.12-DL 49/91 ;Code10        ;TableRelation="Depreciation Book";
                                                   CaptionML=[ENU=Mod.33.12-DL 49/91;
                                                              PTG=Mod.33.12-DL 49/91] }
    { 31022893;;Mod.33.13-DL 49/91 ;Code10        ;TableRelation="Depreciation Book";
                                                   CaptionML=[ENU=Mod.33.13-DL 49/91;
                                                              PTG=Mod.33.13-DL 49/91] }
    { 31022894;;Mod.33.14-DL 264/92;Code10        ;TableRelation="Depreciation Book";
                                                   CaptionML=[ENU=Mod.33.14-DL 264/92;
                                                              PTG=Mod.33.14-DL 264/92] }
    { 31022895;;Mod.33.15-DL 264/92;Code10        ;TableRelation="Depreciation Book";
                                                   CaptionML=[ENU=Mod.33.15-DL 264/92;
                                                              PTG=Mod.33.15-DL 264/92] }
    { 31022896;;Mod.33.16-DL 264/92;Code10        ;TableRelation="Depreciation Book";
                                                   CaptionML=[ENU=Mod.33.16-DL 264/92;
                                                              PTG=Mod.33.16-DL 264/92] }
    { 31022897;;Mod.33.17-DL 31/98 ;Code10        ;TableRelation="Depreciation Book";
                                                   CaptionML=[ENU=Mod.33.17-DL 31/98;
                                                              PTG=Mod.33.17-DL 31/98] }
    { 31022898;;Mod.33.18-DL 31/98 ;Code10        ;TableRelation="Depreciation Book";
                                                   CaptionML=[ENU=Mod.33.18-DL 31/98;
                                                              PTG=Mod.33.18-DL 31/98] }
    { 31022899;;Mod.33.19-DL 31/98 ;Code10        ;TableRelation="Depreciation Book";
                                                   CaptionML=[ENU=Mod.33.19-DL 31/98;
                                                              PTG=Mod.33.19-DL 31/98] }
    { 31022900;;Mod.33.1-DL 126/77 ;Code10        ;TableRelation="Depreciation Book";
                                                   CaptionML=[ENU=Mod.33.1-DL 126/77;
                                                              PTG=Mod.33.1-DL 126/77] }
    { 31022901;;Mod.33.1-Portaria 20258;Code10    ;TableRelation="Depreciation Book";
                                                   CaptionML=[ENU=Mod.33.1-Portaria 20258;
                                                              PTG=Mod.33.1-Portaria 20258] }
    { 31022902;;Mod.33.2-DL 430/78 ou 24/82;Code10;TableRelation="Depreciation Book";
                                                   CaptionML=[ENU=Mod.33.2-DL 430/78 ou 24/82;
                                                              PTG=Mod.33.2-DL 430/78 ou 24/82] }
    { 31022903;;Mod.33.3-DL 219/82 ;Code10        ;TableRelation="Depreciation Book";
                                                   CaptionML=[ENU=Mod.33.3-DL 219/82;
                                                              PTG=Mod.33.3-DL 219/82] }
    { 31022904;;Mod.33.4-DL 219/82 ;Code10        ;TableRelation="Depreciation Book";
                                                   CaptionML=[ENU=Mod.33.4-DL 219/82;
                                                              PTG=Mod.33.4-DL 219/82] }
    { 31022905;;Mod.33.5-DL 399-G/84;Code10       ;TableRelation="Depreciation Book";
                                                   CaptionML=[ENU=Mod.33.5-DL 399-G/84;
                                                              PTG=Mod.33.5-DL 399-G/84] }
    { 31022906;;Mod.33.6-DL 399-G/84;Code10       ;TableRelation="Depreciation Book";
                                                   CaptionML=[ENU=Mod.33.6-DL 399-G/84;
                                                              PTG=Mod.33.6-DL 399-G/84] }
    { 31022907;;Mod.33.7-DL 118-B/86;Code10       ;TableRelation="Depreciation Book";
                                                   CaptionML=[ENU=Mod.33.7-DL 118-B/86;
                                                              PTG=Mod.33.7-DL 118-B/86] }
    { 31022908;;Mod.33.8-DL 118-B/86;Code10       ;TableRelation="Depreciation Book";
                                                   CaptionML=[ENU=Mod.33.8-DL 118-B/86;
                                                              PTG=Mod.33.8-DL 118-B/86] }
    { 31022909;;Mod.33.9-DL 111/88 ;Code10        ;TableRelation="Depreciation Book";
                                                   CaptionML=[ENU=Mod.33.9-DL 111/88;
                                                              PTG=Mod.33.9-DL 111/88] }
  }
  KEYS
  {
    {    ;Primary Key                             ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    END.
  }
}

