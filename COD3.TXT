OBJECT Codeunit 3 G/L Account-Indent
{
  OBJECT-PROPERTIES
  {
    Date=21/12/16;
    Time=13:00:00;
    Version List=NAVW19.00,NAVPTSS81.00;
  }
  PROPERTIES
  {
    OnRun=BEGIN
            IF NOT
               CONFIRM(
                 Text000 +
                 //soft,so
                 //Text001 +
                 //Text002 +
                 //soft,eo
                 Text003,TRUE)
            THEN
              EXIT;

            Indent;
          END;

  }
  CODE
  {
    VAR
      Text000@1000 : TextConst 'ENU="This function updates the indentation of all the G/L accounts in the chart of accounts. ";PTG=Esta fun��o atualiza a indenta��o de todas as contas C/G no plano de contas.';
      Text001@1001 : TextConst 'ENU="All accounts between a Begin-Total and the matching End-Total are indented one level. ";PTG=Todas as contas entre Total-In�cio e a Total-Final correspondente, s�o indentados um n�vel.';
      Text002@1002 : TextConst 'ENU=The Totaling for each End-total is also updated.;PTG=O Somat�rio para cada Total-Final tamb�m � atualizado.';
      Text003@1003 : TextConst 'ENU=\\Do you want to indent the chart of accounts?;PTG=\\Deseja indentar o plano de contas?';
      Text004@1004 : TextConst 'ENU=Indenting the Chart of Accounts #1##########;PTG=A Indentar o Plano de Contas    #1##########';
      Text005@1005 : TextConst 'ENU=End-Total %1 is missing a matching Begin-Total.;PTG=Total-Final %1 n�o tem um Total-In�cio correspondente.';
      GLAcc@1006 : Record 15;
      Window@1007 : Dialog;
      AccNo@1008 : ARRAY [10] OF Code[20];
      i@1009 : Integer;
      "//--soft-global--//"@90000000 : Integer;
      FindAcc@1110011 : Record 15;
      GLSetup@1110002 : Record 98;
      NewGLAcc@1110010 : Record 31022910;
      NewFindAcc@1110014 : Record 31022910;
      LineCounter@1110001 : Integer;
      NoOfRecords@1110000 : Integer;
      HidePrintDialog@1110100 : Boolean;
      "//--soft-text--//"@90000001 : TextConst;
      Text31022890@1110003 : TextConst 'ENU=The Chart of Accounts is corrett.;PTG=O Plano de Contas est� correto.';
      Text31022891@1110004 : TextConst 'ENU=Checking the Chart of Accounts #1########## @2@@@@@@@@@@@@@;PTG=A Verificar o Plano de Contas #1########## @2@@@@@@@@@@@@@';
      Text31022892@1110005 : TextConst 'ENU=Do you want to Check the Chart of Accounts?;PTG=Deseja verificar o Plano de Contas?';
      Text31022893@1110006 : TextConst 'ENU=- Assigns values to the following fields: Income/Balance, Account Type, Totaling and Debit/Credit.\;PTG=- Atribui valores aos seguintes campos: Comercial/Balan�o, Tipo Conta, Somat�rio e D�bito/Cr�dito.\';
      Text31022894@1110007 : TextConst 'ENU=- Checks that all accounts comply with the Chart of Account account length requisites.\;PTG=- Verifica que todas as contas est�o de acordo com os requisitos de comprimento do Plano de Contas.\';
      Text31022895@1110008 : TextConst 'ENU=- Checks that a corresponding balance account exists for every posting account.\;PTG=- Verifica que existe uma conta de contrapartida para cada conta de registo.\';
      Text31022896@1110009 : TextConst 'ENU=This function checks the consistency of and completes the Chart of Accounts:\;PTG=Esta fun��o verifica a consist�ncia e completa o Plano de Contas:\';
      Text31022897@1110012 : TextConst 'ENU=Missing group %1 - Account No. %2;PTG=Grupo em falta %1 - Conta N� %2';

    PROCEDURE Indent@1();
    BEGIN
      Window.OPEN(Text004);

      WITH GLAcc DO BEGIN
        //soft,o IF FIND('-') THEN
        //soft,sn
        RESET;
        IF FINDSET THEN
        //soft,en
          REPEAT
            Window.UPDATE(1,"No.");
            //soft,so
            //IF "Account Type" = "Account Type"::"End-Total" THEN BEGIN
            //  IF i < 1 THEN
            //    ERROR(
            //      Text005,
            //      "No.");
            //  Totaling := AccNo[i] + '..' + "No.";
            //  i := i - 1;
            //END;

            //Indentation := i;
            //soft,eo
            //soft,sn
            TESTFIELD(Name);
            Indentation  := STRLEN("No.") - 1;
            //soft,en
            MODIFY;
            //soft,so
            //IF "Account Type" = "Account Type"::"Begin-Total" THEN BEGIN
            //  i := i + 1;
            //  AccNo[i] := "No.";
            //END;
            //soft,eo
          UNTIL NEXT = 0;
      END; //soft,n
      Window.CLOSE;
    END;

    PROCEDURE RunICAccountIndent@4();
    BEGIN
      IF NOT
         CONFIRM(
           Text000 +
           //soft,o Text001 +
           Text003,TRUE)
      THEN
        EXIT;

      IndentICAccount;
    END;

    LOCAL PROCEDURE IndentICAccount@2();
    VAR
      ICGLAcc@1000 : Record 410;
    BEGIN
      Window.OPEN(Text004);
      WITH ICGLAcc DO BEGIN
        //soft,o IF FIND('-') THEN
        //soft,sn
        RESET;
        IF FINDSET THEN
        //soft,en
          REPEAT
            Window.UPDATE(1,"No.");
            //soft,so
            //IF "Account Type" = "Account Type"::"End-Total" THEN BEGIN
            //  IF i < 1 THEN
            //    ERROR(
            //      Text005,
            //      "No.");
            //  i := i - 1;
            //END;

            //Indentation := i;
            //MODIFY;

            //IF "Account Type" = "Account Type"::"Begin-Total" THEN BEGIN
            //  i := i + 1;
            //  AccNo[i] := "No.";
            //END;
            //soft,eo
            //soft,sn
            TESTFIELD(Name);
            CASE "Account Type" OF
               "Account Type"::Total:
                 Indentation  := STRLEN("No.") - 1;
               "Account Type"::Posting:
                 Indentation  := STRLEN("No.");
            END;

            MODIFY;
            //soft,en
          UNTIL NEXT = 0;
      END;
      Window.CLOSE;
    END;

    PROCEDURE "//--soft-func--//"@90000005();
    BEGIN
    END;

    PROCEDURE CheckChartAcc@1110000();
    BEGIN
      //soft,sn
      IF NOT HidePrintDialog THEN
        IF NOT
           CONFIRM(
             Text31022896 +
             Text31022895 +
             Text31022893 +
             Text31022892,TRUE)
        THEN
          EXIT;

      Window.OPEN(Text31022891);
      WITH GLAcc DO BEGIN
        RESET;
        LineCounter := 0;
        NoOfRecords := COUNT;
        IF NoOfRecords <> 0 THEN BEGIN
          IF FINDSET THEN
            REPEAT
              Window.UPDATE(1,"No.");
              TESTFIELD(Name);
              IF STRLEN("No.") = 1 THEN
                VALIDATE("Account Type","Account Type"::Total);
              IF "Account Type" = "Account Type"::Posting THEN BEGIN
                 CLEAR(FindAcc);
                 FindAcc.SETFILTER("No.",'%1 & <> %2',"No."+'*', "No.");
                 IF FindAcc.FINDFIRST THEN
                   VALIDATE("Account Type","Account Type"::Total)
                 ELSE
                   VALIDATE("Account Type","Account Type"::Posting);
              END;
              IF "Account Type" = "Account Type"::Total THEN BEGIN
                Totaling := "No." + '..' + "No.";
                Totaling := PADSTR(Totaling,20, '9');
              END ELSE BEGIN
                FindAcc.SETFILTER("No.",'<>%1 & %2',"No."+'*',COPYSTR("No.",1,1)+'*');
                IF FindAcc.ISEMPTY THEN
                  ERROR(Text31022897,COPYSTR("No.",1,3),"No.");

                IF COPYSTR("No.",1,1) IN ['6','7'] THEN
                   "Income/Balance" := "Income/Balance"::"Income Statement"
                ELSE
                  IF COPYSTR("No.",1,1)<>'8' THEN
                    "Income/Balance" := "Income/Balance"::"Balance Sheet";

                IF "Income/Balance" = "Income/Balance"::"Income Statement" THEN BEGIN
                  IF COPYSTR("No.",1,1)<>'8' THEN
                    TESTFIELD("Income Stmt. Bal. Acc.");
                END ELSE
                  "Income Stmt. Bal. Acc." := '';

              END;
              MODIFY;
              LineCounter := LineCounter + 1;
              Window.UPDATE(2,ROUND(LineCounter / NoOfRecords * 10000,1));
            UNTIL NEXT = 0;
        END;
      END;
      Window.CLOSE;
      IF NOT HidePrintDialog THEN
        MESSAGE(Text31022890);
      //soft,en
    END;

    PROCEDURE SetHidePrintDialog@1110100(NewHidePrintDialog@1110000 : Boolean);
    BEGIN
      //soft,sn
      HidePrintDialog := NewHidePrintDialog;
      //soft,en
    END;

    PROCEDURE IndentNewChartOfAccounts@1110001();
    BEGIN
      //soft,sn
      IF NOT
         CONFIRM(
           Text000 +
           Text003,TRUE)
      THEN
        EXIT;

      Window.OPEN(Text004);
      WITH NewGLAcc DO BEGIN
        RESET;
        NoOfRecords := COUNT;
        IF NoOfRecords <> 0 THEN BEGIN
        IF FINDFIRST THEN
          REPEAT
            Window.UPDATE(1,"No.");
            TESTFIELD(Name);
            Indentation  := STRLEN("No.") - 1;
            MODIFY;
          UNTIL NEXT = 0;
        END;
      END;
      Window.CLOSE;
      //soft,en
    END;

    PROCEDURE CheckNewChartAcc@1110002();
    BEGIN
      //soft,sn
      IF NOT HidePrintDialog THEN
        IF NOT
          CONFIRM(
            Text31022896 +
            Text31022895 +
            Text31022893 +
            Text31022892,TRUE)
        THEN
          EXIT;

      Window.OPEN(Text31022891);
      WITH NewGLAcc DO BEGIN
        RESET;
        LineCounter := 0;
        NoOfRecords := COUNT;
        IF NoOfRecords <> 0 THEN BEGIN
          IF FINDSET THEN
            REPEAT
              Window.UPDATE(1,"No.");
              TESTFIELD(Name);
              IF STRLEN("No.") = 1 THEN
                "Account Type" := "Account Type"::Total;
              IF "Account Type" = "Account Type"::Posting THEN BEGIN
                 CLEAR(NewFindAcc);
                 NewFindAcc.SETFILTER("No.",'%1 & <> %2',"No."+'*', "No.");
                 IF NewFindAcc.FINDFIRST THEN
                   "Account Type" := "Account Type"::Total
                 ELSE
                   "Account Type" := "Account Type"::Posting;
              END;

              IF "Account Type" = "Account Type"::Total THEN BEGIN
                Totaling := "No." + '..' + "No.";
                Totaling := PADSTR(Totaling,20, '9');
              END ELSE BEGIN
                NewFindAcc.SETFILTER("No.",'<>%1 & %2',"No."+'*',COPYSTR("No.",1,1)+'*');
                IF NewFindAcc.ISEMPTY THEN
                  ERROR(Text31022897,COPYSTR("No.",1,3),"No.");

                IF COPYSTR("No.",1,1) IN ['6','7'] THEN
                   "Income/Balance" := "Income/Balance"::"Income Statement"
                ELSE
                  IF COPYSTR("No.",1,1)<>'8' THEN
                    "Income/Balance" := "Income/Balance"::"Balance Sheet";

                IF "Income/Balance" = "Income/Balance"::"Income Statement" THEN BEGIN
                  IF COPYSTR("No.",1,1)<>'8' THEN
                    TESTFIELD("Income Stmt. Bal. Acc.");
                END ELSE
                  "Income Stmt. Bal. Acc." := '';
              END;
              MODIFY;
              LineCounter := LineCounter + 1;
              Window.UPDATE(2,ROUND(LineCounter / NoOfRecords * 10000,1));
            UNTIL NEXT = 0;
        END;
      END;
      Window.CLOSE;
      IF NOT HidePrintDialog THEN
        MESSAGE(Text31022890);
      //soft,en
    END;

    BEGIN
    END.
  }
}

