OBJECT Table 5345 CRM Transactioncurrency
{
  OBJECT-PROPERTIES
  {
    Date=24/11/16;
    Time=12:00:00;
    Version List=NAVW110.0.00.14199;
  }
  PROPERTIES
  {
    TableType=CRM;
    ExternalName=transactioncurrency;
    CaptionML=[ENU=CRM Transactioncurrency;
               PTG=Divisa];
    Description=Currency in which a financial transaction is carried out.;
  }
  FIELDS
  {
    { 1   ;   ;StatusCode          ;Option        ;InitValue=[ ];
                                                   ExternalName=statuscode;
                                                   ExternalType=Status;
                                                   OptionOrdinalValues=[-1;1;2];
                                                   CaptionML=[ENU=Status Reason;
                                                              PTG=Motivo Estado];
                                                   OptionCaptionML=[ENU=" ,Active,Inactive";
                                                                    PTG=" ,Ativo,Inativo"];
                                                   OptionString=[ ,Active,Inactive];
                                                   Description=Reason for the status of the transaction currency. }
    { 2   ;   ;ModifiedOn          ;DateTime      ;ExternalName=modifiedon;
                                                   ExternalType=DateTime;
                                                   ExternalAccess=Read;
                                                   CaptionML=[ENU=Modified On;
                                                              PTG=Modificado Em];
                                                   Description=Date and time when the transaction currency was last modified. }
    { 3   ;   ;StateCode           ;Option        ;InitValue=Active;
                                                   ExternalName=statecode;
                                                   ExternalType=State;
                                                   ExternalAccess=Modify;
                                                   OptionOrdinalValues=[0;1];
                                                   CaptionML=[ENU=Status;
                                                              PTG=Estado];
                                                   OptionCaptionML=[ENU=Active,Inactive;
                                                                    PTG=Ativo, Inativo];
                                                   OptionString=Active,Inactive;
                                                   Description=Status of the transaction currency. }
    { 4   ;   ;VersionNumber       ;BigInteger    ;ExternalName=versionnumber;
                                                   ExternalType=BigInt;
                                                   ExternalAccess=Read;
                                                   CaptionML=[ENU=Version Number;
                                                              PTG=N�mero Vers�o];
                                                   Description=Version number of the transaction currency. }
    { 5   ;   ;ModifiedBy          ;GUID          ;TableRelation="CRM Systemuser".SystemUserId;
                                                   ExternalName=modifiedby;
                                                   ExternalType=Lookup;
                                                   ExternalAccess=Read;
                                                   CaptionML=[ENU=Modified By;
                                                              PTG=Modificado Por];
                                                   Description=Unique identifier of the user who last modified the transaction currency. }
    { 6   ;   ;ImportSequenceNumber;Integer       ;ExternalName=importsequencenumber;
                                                   ExternalType=Integer;
                                                   ExternalAccess=Insert;
                                                   CaptionML=[ENU=Import Sequence Number;
                                                              PTG=Importar N�mero Sequ�ncia];
                                                   Description=Unique identifier of the data import or data migration that created this record. }
    { 7   ;   ;OverriddenCreatedOn ;Date          ;ExternalName=overriddencreatedon;
                                                   ExternalType=DateTime;
                                                   ExternalAccess=Insert;
                                                   CaptionML=[ENU=Record Created On;
                                                              PTG=Registo Criado Em];
                                                   Description=Date and time that the record was migrated. }
    { 8   ;   ;CreatedOn           ;DateTime      ;ExternalName=createdon;
                                                   ExternalType=DateTime;
                                                   ExternalAccess=Read;
                                                   CaptionML=[ENU=Created On;
                                                              PTG=Criado Em];
                                                   Description=Date and time when the transaction currency was created. }
    { 9   ;   ;TransactionCurrencyId;GUID         ;ExternalName=transactioncurrencyid;
                                                   ExternalType=Uniqueidentifier;
                                                   ExternalAccess=Insert;
                                                   CaptionML=[ENU=Transaction Currency;
                                                              PTG=Transa��o Divisa];
                                                   Description=Unique identifier of the transaction currency. }
    { 10  ;   ;ExchangeRate        ;Decimal       ;ExternalName=exchangerate;
                                                   ExternalType=Decimal;
                                                   CaptionML=[ENU=Exchange Rate;
                                                              PTG=Taxa de C�mbio];
                                                   Description=Exchange rate between the transaction currency and the base currency. }
    { 11  ;   ;CurrencySymbol      ;Text10        ;ExternalName=currencysymbol;
                                                   ExternalType=String;
                                                   CaptionML=[ENU=Currency Symbol;
                                                              PTG=S�mbolo Divisa];
                                                   Description=Symbol for the transaction currency. }
    { 12  ;   ;CurrencyName        ;Text100       ;ExternalName=currencyname;
                                                   ExternalType=String;
                                                   CaptionML=[ENU=Currency Name;
                                                              PTG=Nome Divisa];
                                                   Description=Name of the transaction currency. }
    { 13  ;   ;CreatedBy           ;GUID          ;TableRelation="CRM Systemuser".SystemUserId;
                                                   ExternalName=createdby;
                                                   ExternalType=Lookup;
                                                   ExternalAccess=Read;
                                                   CaptionML=[ENU=Created By;
                                                              PTG=Criado Por];
                                                   Description=Unique identifier of the user who created the transaction currency. }
    { 14  ;   ;ISOCurrencyCode     ;Text5         ;ExternalName=isocurrencycode;
                                                   ExternalType=String;
                                                   ExternalAccess=Insert;
                                                   CaptionML=[ENU=Currency Code;
                                                              PTG=C�digo Divisa];
                                                   Description=ISO currency code for the transaction currency. }
    { 15  ;   ;OrganizationId      ;GUID          ;TableRelation="CRM Organization".OrganizationId;
                                                   ExternalName=organizationid;
                                                   ExternalType=Lookup;
                                                   ExternalAccess=Read;
                                                   CaptionML=[ENU=Organization;
                                                              PTG=Organiza��o];
                                                   Description=Unique identifier of the organization associated with the transaction currency. }
    { 16  ;   ;ModifiedByName      ;Text200       ;FieldClass=FlowField;
                                                   CalcFormula=Lookup("CRM Systemuser".FullName WHERE (SystemUserId=FIELD(ModifiedBy)));
                                                   ExternalName=modifiedbyname;
                                                   ExternalType=String;
                                                   ExternalAccess=Read;
                                                   CaptionML=[ENU=ModifiedByName;
                                                              PTG=ModificadoPorNome] }
    { 17  ;   ;CreatedByName       ;Text200       ;FieldClass=FlowField;
                                                   CalcFormula=Lookup("CRM Systemuser".FullName WHERE (SystemUserId=FIELD(CreatedBy)));
                                                   ExternalName=createdbyname;
                                                   ExternalType=String;
                                                   ExternalAccess=Read;
                                                   CaptionML=[ENU=CreatedByName;
                                                              PTG=CriadoPorNome] }
    { 18  ;   ;CurrencyPrecision   ;Integer       ;ExternalName=currencyprecision;
                                                   ExternalType=Integer;
                                                   CaptionML=[ENU=Currency Precision;
                                                              PTG=Precis�o Divisa];
                                                   MinValue=0;
                                                   MaxValue=4;
                                                   Description=Number of decimal places that can be used for currency. }
    { 19  ;   ;CreatedOnBehalfBy   ;GUID          ;TableRelation="CRM Systemuser".SystemUserId;
                                                   ExternalName=createdonbehalfby;
                                                   ExternalType=Lookup;
                                                   ExternalAccess=Read;
                                                   CaptionML=[ENU=Created By (Delegate);
                                                              PTG=Criado Por (Delegado)];
                                                   Description=Unique identifier of the delegate user who created the transactioncurrency. }
    { 20  ;   ;CreatedOnBehalfByName;Text200      ;FieldClass=FlowField;
                                                   CalcFormula=Lookup("CRM Systemuser".FullName WHERE (SystemUserId=FIELD(CreatedOnBehalfBy)));
                                                   ExternalName=createdonbehalfbyname;
                                                   ExternalType=String;
                                                   ExternalAccess=Read;
                                                   CaptionML=[ENU=CreatedOnBehalfByName;
                                                              PTG=CriadoEmNomePorNome] }
    { 21  ;   ;ModifiedOnBehalfBy  ;GUID          ;TableRelation="CRM Systemuser".SystemUserId;
                                                   ExternalName=modifiedonbehalfby;
                                                   ExternalType=Lookup;
                                                   ExternalAccess=Read;
                                                   CaptionML=[ENU=Modified By (Delegate);
                                                              PTG=Modificado Por (Delegado)];
                                                   Description=Unique identifier of the delegate user who last modified the transactioncurrency. }
    { 22  ;   ;ModifiedOnBehalfByName;Text200     ;FieldClass=FlowField;
                                                   CalcFormula=Lookup("CRM Systemuser".FullName WHERE (SystemUserId=FIELD(ModifiedOnBehalfBy)));
                                                   ExternalName=modifiedonbehalfbyname;
                                                   ExternalType=String;
                                                   ExternalAccess=Read;
                                                   CaptionML=[ENU=ModifiedOnBehalfByName;
                                                              PTG=ModificadoEmNomePorNome] }
    { 23  ;   ;EntityImageId       ;GUID          ;ExternalName=entityimageid;
                                                   ExternalType=Uniqueidentifier;
                                                   ExternalAccess=Read;
                                                   CaptionML=[ENU=Entity Image Id;
                                                              PTG=Id Entity Image];
                                                   Description=For internal use only. }
  }
  KEYS
  {
    {    ;TransactionCurrencyId                   ;Clustered=Yes }
    {    ;CurrencyName                             }
  }
  FIELDGROUPS
  {
    { 1   ;DropDown            ;CurrencyName                             }
  }
  CODE
  {

    BEGIN
    {
      Dynamics CRM Version: 7.1.0.2040
    }
    END.
  }
}

