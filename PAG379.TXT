OBJECT Page 379 Bank Acc. Reconciliation
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Bank Acc. Reconciliation;
               PTG=Reconcilia��o Conta Banc�ria];
    SaveValues=No;
    SourceTable=Table273;
    SourceTableView=WHERE(Statement Type=CONST(Bank Reconciliation));
    PageType=Document;
    PromotedActionCategoriesML=[ENU=New,Process,Report,Bank,Matching;
                                PTG=Nova,Processar,Mapas,Banco,Correspond�ncia];
    ActionList=ACTIONS
    {
      { 1900000003;0 ;ActionContainer;
                      ActionContainerType=RelatedInformation }
      { 23      ;1   ;ActionGroup;
                      CaptionML=[ENU=&Recon.;
                                 PTG=&Reconcilia��o];
                      Image=BankAccountRec }
      { 26      ;2   ;Action    ;
                      ShortCutKey=Shift+F7;
                      CaptionML=[ENU=&Card;
                                 PTG=Fi&cha];
                      ToolTipML=ENU=View or change detailed information about the record that is being processed on the journal line.;
                      ApplicationArea=#Basic,#Suite;
                      RunObject=Page 370;
                      RunPageLink=No.=FIELD(Bank Account No.);
                      Image=EditLines }
      { 1900000004;0 ;ActionContainer;
                      ActionContainerType=ActionItems }
      { 14      ;1   ;ActionGroup;
                      CaptionML=[ENU=F&unctions;
                                 PTG=F&un��es];
                      Image=Action }
      { 16      ;2   ;Action    ;
                      Name=SuggestLines;
                      Ellipsis=Yes;
                      CaptionML=[ENU=Suggest Lines;
                                 PTG=Sugerir linhas];
                      ToolTipML=ENU=Create bank account ledger entries suggestions and enter them automatically.;
                      ApplicationArea=#Basic,#Suite;
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=SuggestLines;
                      PromotedCategory=Process;
                      OnAction=BEGIN
                                 SuggestBankAccStatement.SetStmt(Rec);
                                 SuggestBankAccStatement.RUNMODAL;
                                 CLEAR(SuggestBankAccStatement);
                               END;
                                }
      { 24      ;2   ;Action    ;
                      Ellipsis=Yes;
                      CaptionML=[ENU=Transfer to General Journal;
                                 PTG=Transferir para Di�rio Geral];
                      ToolTipML=ENU=Transfer the lines from the current window to the general journal.;
                      ApplicationArea=#Basic,#Suite;
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=TransferToGeneralJournal;
                      PromotedCategory=Process;
                      OnAction=BEGIN
                                 TransferToGLJnl.SetBankAccRecon(Rec);
                                 TransferToGLJnl.RUN;
                               END;
                                }
      { 19      ;1   ;ActionGroup;
                      CaptionML=[ENU=Ba&nk;
                                 PTG=Ba&nco];
                      ActionContainerType=NewDocumentItems }
      { 3       ;2   ;Action    ;
                      Name=ImportBankStatement;
                      CaptionML=[ENU=Import Bank Statement;
                                 PTG=Importar Extrato Banc�rio];
                      ToolTipML=ENU=Import electronic bank statements from your bank to populate with data about actual bank transactions.;
                      ApplicationArea=#Basic,#Suite;
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=Import;
                      PromotedCategory=Category4;
                      OnAction=BEGIN
                                 CurrPage.UPDATE;
                                 ImportBankStatement;
                               END;
                                }
      { 12      ;1   ;ActionGroup;
                      CaptionML=[ENU=M&atching;
                                 PTG=Correspond�ncia] }
      { 4       ;2   ;Action    ;
                      Name=MatchAutomatically;
                      CaptionML=[ENU=Match Automatically;
                                 PTG=Correspond�ncia Autom�tica];
                      ToolTipML=ENU=Automatically search for and match bank statement lines.;
                      ApplicationArea=#Basic,#Suite;
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=MapAccounts;
                      PromotedCategory=Category5;
                      OnAction=BEGIN
                                 SETRANGE("Statement Type","Statement Type");
                                 SETRANGE("Bank Account No.","Bank Account No.");
                                 SETRANGE("Statement No.","Statement No.");
                                 REPORT.RUN(REPORT::"Match Bank Entries",TRUE,TRUE,Rec);
                               END;
                                }
      { 18      ;2   ;Action    ;
                      Name=MatchManually;
                      CaptionML=[ENU=Match Manually;
                                 PTG=Correspond�ncia Manual];
                      ToolTipML=ENU=Manually match selected lines in both panes to link each bank statement line to one or more related bank account ledger entries.;
                      ApplicationArea=#Basic,#Suite;
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=CheckRulesSyntax;
                      PromotedCategory=Category5;
                      OnAction=VAR
                                 TempBankAccReconciliationLine@1001 : TEMPORARY Record 274;
                                 TempBankAccountLedgerEntry@1000 : TEMPORARY Record 271;
                                 MatchBankRecLines@1002 : Codeunit 1252;
                               BEGIN
                                 CurrPage.StmtLine.PAGE.GetSelectedRecords(TempBankAccReconciliationLine);
                                 CurrPage.ApplyBankLedgerEntries.PAGE.GetSelectedRecords(TempBankAccountLedgerEntry);
                                 MatchBankRecLines.MatchManually(TempBankAccReconciliationLine,TempBankAccountLedgerEntry);
                               END;
                                }
      { 13      ;2   ;Action    ;
                      Name=RemoveMatch;
                      CaptionML=[ENU=Remove Match;
                                 PTG=Remover Correspond�ncia];
                      ToolTipML=ENU=Remove selection of matched bank statement lines.;
                      ApplicationArea=#Basic,#Suite;
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=RemoveContacts;
                      PromotedCategory=Category5;
                      OnAction=VAR
                                 TempBankAccReconciliationLine@1002 : TEMPORARY Record 274;
                                 TempBankAccountLedgerEntry@1001 : TEMPORARY Record 271;
                                 MatchBankRecLines@1000 : Codeunit 1252;
                               BEGIN
                                 CurrPage.StmtLine.PAGE.GetSelectedRecords(TempBankAccReconciliationLine);
                                 CurrPage.ApplyBankLedgerEntries.PAGE.GetSelectedRecords(TempBankAccountLedgerEntry);
                                 MatchBankRecLines.RemoveMatch(TempBankAccReconciliationLine,TempBankAccountLedgerEntry);
                               END;
                                }
      { 10      ;2   ;Action    ;
                      Name=All;
                      CaptionML=[ENU=Show All;
                                 PTG=Mostrar Todos];
                      ToolTipML=ENU=Show all bank statement lines.;
                      ApplicationArea=#Basic,#Suite;
                      Image=AddWatch;
                      OnAction=BEGIN
                                 CurrPage.StmtLine.PAGE.ToggleMatchedFilter(FALSE);
                                 CurrPage.ApplyBankLedgerEntries.PAGE.ToggleMatchedFilter(FALSE);
                               END;
                                }
      { 7       ;2   ;Action    ;
                      Name=NotMatched;
                      CaptionML=[ENU=Show Nonmatched;
                                 PTG=Mostrar Sem Correspond�ncia];
                      ToolTipML=ENU=Show all bank statement lines that have not yet been matched.;
                      ApplicationArea=#Basic,#Suite;
                      Image=AddWatch;
                      OnAction=BEGIN
                                 CurrPage.StmtLine.PAGE.ToggleMatchedFilter(TRUE);
                                 CurrPage.ApplyBankLedgerEntries.PAGE.ToggleMatchedFilter(TRUE);
                               END;
                                }
      { 1       ;1   ;ActionGroup;
                      CaptionML=[ENU=P&osting;
                                 PTG=&Registo];
                      Image=Post }
      { 15      ;2   ;Action    ;
                      Ellipsis=Yes;
                      CaptionML=[ENU=&Test Report;
                                 PTG=Veri&ficar];
                      ToolTipML=ENU=Preview the resulting bank account reconciliations to see the consequences before you perform the actual posting.;
                      ApplicationArea=#Basic,#Suite;
                      Image=TestReport;
                      OnAction=BEGIN
                                 ReportPrint.PrintBankAccRecon(Rec);
                               END;
                                }
      { 17      ;2   ;Action    ;
                      Name=Post;
                      ShortCutKey=F9;
                      CaptionML=[ENU=P&ost;
                                 PTG=R&egistar];
                      ToolTipML=ENU=Finalize the document or journal by posting the amounts and quantities to the related accounts in your company books.;
                      ApplicationArea=#Basic,#Suite;
                      RunObject=Codeunit 371;
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=PostOrder;
                      PromotedCategory=Process }
      { 9       ;2   ;Action    ;
                      Name=PostAndPrint;
                      ShortCutKey=Shift+F9;
                      CaptionML=[ENU=Post and &Print;
                                 PTG=Registar e &Imprimir];
                      ToolTipML=ENU=Finalize and prepare to print the document or journal. The values and quantities are posted to the related accounts. A report request window where you can specify what to include on the print-out.;
                      ApplicationArea=#Basic,#Suite;
                      RunObject=Codeunit 372;
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=PostPrint;
                      PromotedCategory=Process }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 2   ;1   ;Group     ;
                CaptionML=[ENU=General;
                           PTG=Geral] }

    { 5   ;2   ;Field     ;
                Name=BankAccountNo;
                CaptionML=[ENU=Bank Account No.;
                           PTG=N� Conta Banc�ria];
                ToolTipML=ENU=Specifies the number of the bank account that you want to reconcile with the bank's statement.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Bank Account No." }

    { 20  ;2   ;Field     ;
                Name=StatementNo;
                CaptionML=[ENU=Statement No.;
                           PTG=N� Extrato];
                ToolTipML=ENU=Specifies the number of the bank account statement.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Statement No." }

    { 22  ;2   ;Field     ;
                Name=StatementDate;
                CaptionML=[ENU=Statement Date;
                           PTG=Data Extrato];
                ToolTipML=ENU=Specifies the date on the bank account statement.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Statement Date" }

    { 28  ;2   ;Field     ;
                Name=BalanceLastStatement;
                CaptionML=[ENU=Balance Last Statement;
                           PTG=Saldo �lt. Ext. Cta.];
                ToolTipML=ENU=Specifies the ending balance shown on the last bank statement, which was used in the last posted bank reconciliation for this bank account.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Balance Last Statement" }

    { 30  ;2   ;Field     ;
                Name=StatementEndingBalance;
                CaptionML=[ENU=Statement Ending Balance;
                           PTG=Saldo Final Extrato Cta.];
                ToolTipML=ENU=Specifies the ending balance shown on the bank's statement that you want to reconcile with the bank account.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Statement Ending Balance" }

    { 8   ;1   ;Group     ;
                GroupType=Group }

    { 11  ;2   ;Part      ;
                Name=StmtLine;
                CaptionML=[ENU=Bank Statement Lines;
                           PTG=Linhas Extrato Banc�rio];
                ApplicationArea=#Basic,#Suite;
                SubPageLink=Bank Account No.=FIELD(Bank Account No.),
                            Statement No.=FIELD(Statement No.);
                PagePartID=Page380;
                PartType=Page }

    { 6   ;2   ;Part      ;
                Name=ApplyBankLedgerEntries;
                CaptionML=[ENU=Bank Account Ledger Entries;
                           PTG=Movs. Contas Banc�rias];
                ApplicationArea=#Basic,#Suite;
                SubPageLink=Bank Account No.=FIELD(Bank Account No.),
                            Open=CONST(Yes),
                            Statement Status=FILTER(Open|Bank Acc. Entry Applied|Check Entry Applied);
                PagePartID=Page381;
                PartType=Page }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {
    VAR
      SuggestBankAccStatement@1000 : Report 1496;
      TransferToGLJnl@1001 : Report 1497;
      ReportPrint@1002 : Codeunit 228;

    BEGIN
    END.
  }
}

