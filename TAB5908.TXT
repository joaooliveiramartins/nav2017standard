OBJECT Table 5908 Warranty Ledger Entry
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Warranty Ledger Entry;
               PTG=Mov. Garantia];
    LookupPageID=Page5913;
    DrillDownPageID=Page5913;
  }
  FIELDS
  {
    { 1   ;   ;Entry No.           ;Integer       ;CaptionML=[ENU=Entry No.;
                                                              PTG=N� Mov.] }
    { 3   ;   ;Document No.        ;Code20        ;CaptionML=[ENU=Document No.;
                                                              PTG=N� Documento] }
    { 4   ;   ;Posting Date        ;Date          ;CaptionML=[ENU=Posting Date;
                                                              PTG=Data Registo] }
    { 5   ;   ;Customer No.        ;Code20        ;TableRelation=Customer;
                                                   CaptionML=[ENU=Customer No.;
                                                              PTG=N� Cliente] }
    { 6   ;   ;Ship-to Code        ;Code10        ;TableRelation="Ship-to Address".Code WHERE (Customer No.=FIELD(Customer No.));
                                                   CaptionML=[ENU=Ship-to Code;
                                                              PTG=C�d. Endere�o Envio] }
    { 7   ;   ;Bill-to Customer No.;Code20        ;TableRelation=Customer;
                                                   CaptionML=[ENU=Bill-to Customer No.;
                                                              PTG=Fatura-a N� Cliente] }
    { 8   ;   ;Variant Code (Serviced);Code10     ;TableRelation="Item Variant".Code WHERE (Item No.=FIELD("Item No. (Serviced)"));
                                                   CaptionML=[ENU=Variant Code (Serviced);
                                                              PTG=C�d. Variante (Reparado)] }
    { 9   ;   ;Service Item No. (Serviced);Code20 ;TableRelation="Service Item";
                                                   CaptionML=[ENU=Service Item No. (Serviced);
                                                              PTG=N� Produto Servi�o (Reparado)] }
    { 10  ;   ;Item No. (Serviced) ;Code20        ;TableRelation=Item.No.;
                                                   CaptionML=[ENU=Item No. (Serviced);
                                                              PTG=N� Produto (Reparado)] }
    { 11  ;   ;Serial No. (Serviced);Code20       ;CaptionML=[ENU=Serial No. (Serviced);
                                                              PTG=N� S�rie (Reparado)] }
    { 12  ;   ;Service Item Group (Serviced);Code10;
                                                   TableRelation="Service Item Group";
                                                   CaptionML=[ENU=Service Item Group (Serviced);
                                                              PTG=Grupo Produto Servi�o (Reparado)] }
    { 13  ;   ;Service Order No.   ;Code20        ;OnLookup=BEGIN
                                                              CLEAR(ServOrderMgt);
                                                              ServOrderMgt.ServHeaderLookup(1,"Service Order No.");
                                                            END;

                                                   CaptionML=[ENU=Service Order No.;
                                                              PTG=N� Ordem Servi�o] }
    { 14  ;   ;Service Contract No.;Code20        ;TableRelation="Service Contract Header"."Contract No." WHERE (Contract Type=CONST(Contract));
                                                   CaptionML=[ENU=Service Contract No.;
                                                              PTG=N� Contrato Servi�o] }
    { 15  ;   ;Fault Reason Code   ;Code10        ;TableRelation="Fault Reason Code";
                                                   CaptionML=[ENU=Fault Reason Code;
                                                              PTG=C�d. Raz�o Falha] }
    { 16  ;   ;Fault Area Code     ;Code10        ;TableRelation="Fault Area";
                                                   CaptionML=[ENU=Fault Area Code;
                                                              PTG=C�d. �rea Falha] }
    { 17  ;   ;Fault Code          ;Code10        ;TableRelation="Fault Code".Code WHERE (Fault Area Code=FIELD(Fault Area Code),
                                                                                          Symptom Code=FIELD(Symptom Code));
                                                   CaptionML=[ENU=Fault Code;
                                                              PTG=C�d. Falha] }
    { 18  ;   ;Symptom Code        ;Code10        ;TableRelation="Symptom Code";
                                                   CaptionML=[ENU=Symptom Code;
                                                              PTG=C�d. Sintoma] }
    { 19  ;   ;Resolution Code     ;Code10        ;TableRelation="Resolution Code";
                                                   CaptionML=[ENU=Resolution Code;
                                                              PTG=C�d. Resolu��o] }
    { 20  ;   ;Type                ;Option        ;CaptionML=[ENU=Type;
                                                              PTG=Tipo];
                                                   OptionCaptionML=[ENU=" ,Item,Resource,Service Cost";
                                                                    PTG=" ,Produto,Recurso,Custo Servi�o"];
                                                   OptionString=[ ,Item,Resource,Service Cost] }
    { 21  ;   ;No.                 ;Code20        ;TableRelation=IF (Type=CONST(" ")) "Standard Text"
                                                                 ELSE IF (Type=CONST(Item)) Item
                                                                 ELSE IF (Type=CONST(Resource)) Resource
                                                                 ELSE IF (Type=CONST(Service Cost)) "Service Cost";
                                                   CaptionML=[ENU=No.;
                                                              PTG=N�] }
    { 22  ;   ;Quantity            ;Decimal       ;CaptionML=[ENU=Quantity;
                                                              PTG=Quantidade];
                                                   DecimalPlaces=0:5 }
    { 24  ;   ;Work Type Code      ;Code10        ;TableRelation="Work Type";
                                                   CaptionML=[ENU=Work Type Code;
                                                              PTG=C�d. Tipo Trabalho] }
    { 25  ;   ;Unit of Measure Code;Code10        ;TableRelation=IF (Type=CONST(Item)) "Item Unit of Measure".Code WHERE (Item No.=FIELD(No.))
                                                                 ELSE "Unit of Measure";
                                                   CaptionML=[ENU=Unit of Measure Code;
                                                              PTG=C�d. Unidade Medida] }
    { 26  ;   ;Amount              ;Decimal       ;CaptionML=[ENU=Amount;
                                                              PTG=Valor];
                                                   AutoFormatType=1 }
    { 27  ;   ;Description         ;Text50        ;CaptionML=[ENU=Description;
                                                              PTG=Descri��o] }
    { 29  ;   ;Gen. Bus. Posting Group;Code10     ;TableRelation="Gen. Business Posting Group";
                                                   CaptionML=[ENU=Gen. Bus. Posting Group;
                                                              PTG=Gr. Contabil�stico Neg�cio] }
    { 30  ;   ;Gen. Prod. Posting Group;Code10    ;TableRelation="Gen. Product Posting Group";
                                                   CaptionML=[ENU=Gen. Prod. Posting Group;
                                                              PTG=Gr. Contabil�stico Produto] }
    { 31  ;   ;Global Dimension 1 Code;Code20     ;TableRelation="Dimension Value".Code WHERE (Global Dimension No.=CONST(1));
                                                   CaptionML=[ENU=Global Dimension 1 Code;
                                                              PTG=C�d. Dimens�o 1 Global];
                                                   CaptionClass='1,2,1' }
    { 32  ;   ;Global Dimension 2 Code;Code20     ;TableRelation="Dimension Value".Code WHERE (Global Dimension No.=CONST(2));
                                                   CaptionML=[ENU=Global Dimension 2 Code;
                                                              PTG=C�d. Dimens�o 2 Global];
                                                   CaptionClass='1,2,2' }
    { 33  ;   ;Open                ;Boolean       ;CaptionML=[ENU=Open;
                                                              PTG=Pendente] }
    { 35  ;   ;Vendor No.          ;Code20        ;TableRelation=Vendor.No.;
                                                   CaptionML=[ENU=Vendor No.;
                                                              PTG=N� Fornecedor] }
    { 36  ;   ;Vendor Item No.     ;Text20        ;CaptionML=[ENU=Vendor Item No.;
                                                              PTG=C�d. Produto Forn.] }
    { 38  ;   ;Variant Code        ;Code10        ;TableRelation=IF (Type=CONST(Item)) "Item Variant".Code WHERE (Item No.=FIELD(No.));
                                                   CaptionML=[ENU=Variant Code;
                                                              PTG=C�d. Variante] }
    { 39  ;   ;Service Order Line No.;Integer     ;CaptionML=[ENU=Service Order Line No.;
                                                              PTG=N� Linha Ordem Servi�o] }
    { 480 ;   ;Dimension Set ID    ;Integer       ;TableRelation="Dimension Set Entry";
                                                   OnLookup=BEGIN
                                                              ShowDimensions;
                                                            END;

                                                   CaptionML=[ENU=Dimension Set ID;
                                                              PTG=ID Combina��o Dimens�o];
                                                   Editable=No }
  }
  KEYS
  {
    {    ;Entry No.                               ;Clustered=Yes }
    {    ;Service Item No. (Serviced),Posting Date,Document No. }
    {    ;Service Order No.,Posting Date,Document No. }
    {    ;Service Contract No.,Posting Date,Document No. }
    {    ;Document No.,Posting Date                }
  }
  FIELDGROUPS
  {
    { 1   ;DropDown            ;Entry No.,Type,Document No.,Posting Date }
  }
  CODE
  {
    VAR
      ServOrderMgt@1001 : Codeunit 5900;
      DimMgt@1000 : Codeunit 408;

    PROCEDURE ShowDimensions@1();
    BEGIN
      DimMgt.ShowDimensionSet("Dimension Set ID",STRSUBSTNO('%1 %2',TABLECAPTION,"Entry No."));
    END;

    BEGIN
    END.
  }
}

