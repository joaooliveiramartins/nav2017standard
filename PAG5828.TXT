OBJECT Page 5828 Inventory Periods
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Inventory Periods;
               PTG=Per�odos Invent�rio];
    SourceTable=Table5814;
    PageType=List;
    ActionList=ACTIONS
    {
      { 1900000003;0 ;ActionContainer;
                      ActionContainerType=RelatedInformation }
      { 9       ;1   ;ActionGroup;
                      CaptionML=[ENU=&Invt. Period;
                                 PTG=Per�odo &Invent.];
                      Image=Period }
      { 14      ;2   ;Action    ;
                      ShortCutKey=Ctrl+F7;
                      CaptionML=[ENU=Invt. Period E&ntries;
                                 PTG=Movime&ntos Per�odo Invent.];
                      ToolTipML=[ENU=Define how to track the closings and re-openings of an inventory period.;
                                 PTG=""];
                      ApplicationArea=#Basic,#Suite;
                      RunObject=Page 5829;
                      RunPageView=SORTING(Ending Date,Entry No.);
                      RunPageLink=Ending Date=FIELD(Ending Date);
                      Image=PeriodEntries }
      { 8       ;2   ;Action    ;
                      CaptionML=[ENU=&Accounting Periods;
                                 PTG=&Per�odos Contabil�sticos];
                      ToolTipML=[ENU=Set up accounting periods, one line per period. You must set up at least one accounting period for each fiscal year.;
                                 PTG=""];
                      ApplicationArea=#Basic,#Suite;
                      RunObject=Page 100;
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=AccountingPeriods;
                      PromotedCategory=Process }
      { 1900000004;0 ;ActionContainer;
                      ActionContainerType=ActionItems }
      { 16      ;1   ;ActionGroup;
                      CaptionML=[ENU=F&unctions;
                                 PTG=F&un��es];
                      Image=Action }
      { 13      ;2   ;Action    ;
                      Ellipsis=Yes;
                      CaptionML=[ENU=Test Report;
                                 PTG=Verificar];
                      ToolTipML=[ENU=View a test report so that you can find and correct any errors before you perform the actual posting of the journal or document.;
                                 PTG=""];
                      ApplicationArea=#Basic,#Suite;
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=TestReport;
                      PromotedCategory=Process;
                      OnAction=BEGIN
                                 ReportPrint.PrintInvtPeriod(Rec);
                               END;
                                }
      { 18      ;2   ;Action    ;
                      Ellipsis=Yes;
                      CaptionML=[ENU=&Close Period;
                                 PTG=Fe&char Per�odo];
                      ToolTipML=[ENU=Close the selected period. Once it is closed, you cannot post in the period, until you re-open it.;
                                 PTG=""];
                      ApplicationArea=#Basic,#Suite;
                      Promoted=Yes;
                      Image=ClosePeriod;
                      PromotedCategory=Process;
                      OnAction=BEGIN
                                 CloseInventoryPeriod.SetReOpen(FALSE);
                                 CloseInventoryPeriod.RUN(Rec);
                               END;
                                }
      { 19      ;2   ;Action    ;
                      Ellipsis=Yes;
                      CaptionML=[ENU=&Reopen Period;
                                 PTG=&Reabrir Per�odo];
                      ToolTipML=[ENU=Reopen a closed period in order to be able to post in it.;
                                 PTG=""];
                      ApplicationArea=#Basic,#Suite;
                      Promoted=Yes;
                      Image=ReopenPeriod;
                      PromotedCategory=Process;
                      OnAction=BEGIN
                                 CloseInventoryPeriod.SetReOpen(TRUE);
                                 CloseInventoryPeriod.RUN(Rec);
                               END;
                                }
      { 15      ;2   ;Separator  }
      { 17      ;2   ;Action    ;
                      Ellipsis=Yes;
                      CaptionML=[ENU=&Adjust Cost - Item Entries;
                                 PTG=&Ajustar Custo - Movs. Produto];
                      ToolTipML=[ENU=Adjust inventory values in value entries so that you use the correct adjusted cost for updating the general ledger and so that sales and profit statistics are up to date.;
                                 PTG=""];
                      ApplicationArea=#Basic,#Suite;
                      RunObject=Report 795;
                      Image=AdjustEntries }
      { 20      ;2   ;Action    ;
                      Ellipsis=Yes;
                      CaptionML=[ENU=&Post Inventory to G/L;
                                 PTG=&Registar Invent�rio na C/G];
                      ToolTipML=[ENU=Record the quantity and value changes to the inventory in the item ledger entries and the value entries when you post inventory transactions, such as sales shipments or purchase receipts.;
                                 PTG=""];
                      ApplicationArea=#Basic,#Suite;
                      RunObject=Report 1002;
                      Promoted=Yes;
                      Image=PostInventoryToGL;
                      PromotedCategory=Process }
      { 21      ;2   ;Action    ;
                      Ellipsis=Yes;
                      CaptionML=[ENU=Post &Inventory to G/L - Test;
                                 PTG=Registar &Invent�rio na C/G - Teste];
                      ToolTipML=[ENU=Run a test of the Post Inventory to G/L.;
                                 PTG=""];
                      ApplicationArea=#Basic,#Suite;
                      RunObject=Report 1003;
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=PostInventoryToGLTest;
                      PromotedCategory=Process }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 2   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the ending date of an inventory period is the last day of the inventory period.;
                           PTG=""];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Ending Date" }

    { 4   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies a descriptive name that helps users identify the inventory period.;
                           PTG=""];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=Name }

    { 6   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies that an inventory period can be open or closed.;
                           PTG=""];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=Closed }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {
    VAR
      CloseInventoryPeriod@1000 : Codeunit 5820;
      ReportPrint@1001 : Codeunit 228;

    BEGIN
    END.
  }
}

