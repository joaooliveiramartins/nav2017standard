OBJECT Page 9187 Copy Generic Chart
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Copy Generic Chart;
               PTG=Copiar Gr�fico Padr�o];
    InsertAllowed=No;
    DeleteAllowed=No;
    ModifyAllowed=No;
    PageType=StandardDialog;
    ShowFilter=No;
    OnQueryClosePage=VAR
                       GenericChartMgt@1000 : Codeunit 9180;
                     BEGIN
                       IF CloseAction IN [ACTION::OK,ACTION::LookupOK] THEN BEGIN
                         ValidateUserInput;
                         GenericChartMgt.CopyChart(SourceChart,NewChartID,NewChartTitle);
                         MESSAGE(Text001);
                       END
                     END;

  }
  CONTROLS
  {
    { 1   ;    ;Container ;
                ContainerType=ContentArea }

    { 2   ;1   ;Field     ;
                CaptionML=[ENU=New Chart ID;
                           PTG=Novo ID Gr�fico];
                ToolTipML=[ENU=Specifies the ID of the new chart that you copy information to.;
                           PTG=""];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=NewChartID }

    { 3   ;1   ;Field     ;
                CaptionML=[ENU=New Chart Title;
                           PTG=Novo T�tulo Gr�fico];
                ToolTipML=[ENU=Specifies the name of the new chart that you copy information to.;
                           PTG=""];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=NewChartTitle }

  }
  CODE
  {
    VAR
      SourceChart@1004 : Record 2000000078;
      NewChartID@1000 : Code[20];
      NewChartTitle@1001 : Text[50];
      Text001@1002 : TextConst 'ENU=The chart was successfully copied.;PTG=O gr�fico foi copiado com sucesso.';
      Text002@1003 : TextConst 'ENU=Specify a chart ID.;PTG=Indique um ID gr�fico.';

    LOCAL PROCEDURE ValidateUserInput@1();
    BEGIN
      IF NewChartID = '' THEN
        ERROR(Text002);
    END;

    PROCEDURE SetSourceChart@2(SourceChartInput@1000 : Record 2000000078);
    BEGIN
      SourceChart := SourceChartInput;
      CurrPage.CAPTION(CurrPage.CAPTION + ' ' + SourceChart.ID);
    END;

    BEGIN
    END.
  }
}

