OBJECT Page 31023041 Imparities List
{
  OBJECT-PROPERTIES
  {
    Date=21/12/16;
    Time=13:00:00;
    Version List=NAVPTSS10.00;
  }
  PROPERTIES
  {
    Editable=No;
    CaptionML=[ENU=Imparities;
               PTG=Imparidades];
    SourceTable=Table31022965;
    PageType=List;
    CardPageID=Imparity Card;
    ActionList=ACTIONS
    {
      { 1102058012;0 ;ActionContainer;
                      ActionContainerType=RelatedInformation }
      { 1102058010;1 ;ActionGroup;
                      CaptionML=[ENU=Fiscal Limits;
                                 PTG=Limites Fiscais] }
      { 1102058011;2 ;Action    ;
                      Name=<Action1102058011>;
                      CaptionML=[ENU=Fiscal Limits;
                                 PTG=Limites Fiscais];
                      Image=ChangePaymentTolerance;
                      OnAction=VAR
                                 FiscalLimits@1102058000 : Record 31022971;
                               BEGIN
                                 TESTFIELD("Imparity Type","Imparity Type"::"Credits and Inventory");
                                 FiscalLimits.SETRANGE("Imparity Code","Imparity Code");
                                 PAGE.RUN(PAGE::"Fiscal Limits",FiscalLimits);
                               END;
                                }
      { 1000000000;2 ;Action    ;
                      CaptionML=[ENU=Entries;
                                 PTG=Movimentos];
                      RunObject=Page 31023043;
                      Image=FixedAssetLedger }
    }
  }
  CONTROLS
  {
    { 1102058000;0;Container;
                ContainerType=ContentArea }

    { 1102058001;1;Group  ;
                Name=Group;
                GroupType=Repeater }

    { 1102058002;2;Field  ;
                ApplicationArea=#All;
                SourceExpr="Imparity Code" }

    { 1102058003;2;Field  ;
                ApplicationArea=#All;
                SourceExpr=Description }

    { 1102058004;2;Field  ;
                ApplicationArea=#All;
                SourceExpr="Imparity Type" }

    { 1102058005;2;Field  ;
                ApplicationArea=#All;
                SourceExpr="Imparity SubType" }

    { 1102058006;2;Field  ;
                ApplicationArea=#All;
                SourceExpr="Tax behavior" }

    { 1102058013;2;Field  ;
                ApplicationArea=#All;
                SourceExpr=XMLoption }

    { 1102058007;2;Field  ;
                ApplicationArea=#All;
                SourceExpr="Imparity Posting Gr." }

    { 1102058008;2;Field  ;
                ApplicationArea=#All;
                SourceExpr=Blocked }

    { 1102058009;2;Field  ;
                ApplicationArea=#All;
                SourceExpr=Amount }

    { 1102058014;2;Field  ;
                ApplicationArea=#All;
                SourceExpr=XMLDescription }

  }
  CODE
  {

    BEGIN
    END.
  }
}

