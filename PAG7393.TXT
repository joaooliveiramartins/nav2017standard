OBJECT Page 7393 Posted Invt. Pick Subform
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0;
  }
  PROPERTIES
  {
    Editable=No;
    CaptionML=[ENU=Lines;
               PTG=Linhas];
    MultipleNewLines=Yes;
    InsertAllowed=No;
    LinksAllowed=No;
    SourceTable=Table7343;
    DelayedInsert=Yes;
    PageType=ListPart;
    AutoSplitKey=Yes;
    ActionList=ACTIONS
    {
      { 1900000004;0 ;ActionContainer;
                      ActionContainerType=ActionItems }
      { 1907935204;1 ;ActionGroup;
                      CaptionML=[ENU=&Line;
                                 PTG=&Linha];
                      Image=Line }
      { 1900206304;2 ;Action    ;
                      CaptionML=[ENU=Bin Contents List;
                                 PTG=Lista Conte�do Posi��o];
                      Image=BinContent;
                      OnAction=BEGIN
                                 ShowBinContents;
                               END;
                                }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 36  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the action type for the inventory pick line. For posted inventory pick lines, the action type is always Take, meaning that the items are being taken out of the bin.;
                           PTG=""];
                SourceExpr="Action Type";
                Visible=FALSE }

    { 12  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the type of document (for example, sales order, purchase order or transfer) to which the line relates.;
                           PTG=""];
                OptionCaptionML=[ENU=" ,,,,Sales Return Order,Purchase Order,,,,Inbound Transfer";
                                 PTG=" ,,,,Devolu��o Venda,Enc. Compra,,,,Transf. Entrada"];
                SourceExpr="Source Document";
                Visible=FALSE }

    { 6   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the number of the source document from which the inventory pick line originated.;
                           PTG=""];
                SourceExpr="Source No.";
                Visible=FALSE }

    { 22  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the number of the item that was picked.;
                           PTG=""];
                SourceExpr="Item No." }

    { 2   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the variant code of the item that was picked.;
                           PTG=""];
                SourceExpr="Variant Code";
                Visible=FALSE }

    { 30  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies a description of the item that was picked.;
                           PTG=""];
                SourceExpr=Description }

    { 14  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the serial number for the item that was picked.;
                           PTG=""];
                SourceExpr="Serial No.";
                Visible=FALSE }

    { 40  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the lot number for the item that was picked.;
                           PTG=""];
                SourceExpr="Lot No.";
                Visible=FALSE }

    { 1106000000;2;Field  ;
                ToolTipML=[ENU=Specifies the expiration date for the item that was picked.;
                           PTG=""];
                SourceExpr="Expiration Date";
                Visible=FALSE }

    { 4   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the code for the location at which the items were pick.;
                           PTG=""];
                SourceExpr="Location Code";
                Visible=FALSE }

    { 46  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the code for the bin in which item was picked.;
                           PTG=""];
                SourceExpr="Bin Code";
                OnValidate=BEGIN
                             BinCodeOnAfterValidate;
                           END;
                            }

    { 8   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the shelf number of the item for informational use.;
                           PTG=""];
                SourceExpr="Shelf No.";
                Visible=FALSE }

    { 34  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the quantity of the item that was picked.;
                           PTG=""];
                SourceExpr=Quantity }

    { 10  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the quantity, in the base unit of measure, of the item that was picked.;
                           PTG=""];
                SourceExpr="Qty. (Base)";
                Visible=FALSE }

    { 62  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the date when the pick should have been completed.;
                           PTG=""];
                SourceExpr="Due Date" }

    { 26  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the unit of measure code of the item that was picked.;
                           PTG=""];
                SourceExpr="Unit of Measure Code" }

    { 28  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the quantity per unit of measure of the item that was picked.;
                           PTG=""];
                SourceExpr="Qty. per Unit of Measure";
                Visible=FALSE }

    { 16  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the type of destination associated with the posted inventory pick line.;
                           PTG=""];
                SourceExpr="Destination Type";
                Visible=FALSE }

    { 56  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the number or the code of the customer or vendor linked to the line.;
                           PTG=""];
                SourceExpr="Destination No.";
                Visible=FALSE }

    { 54  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the special equipment code for the item that was picked.;
                           PTG=""];
                SourceExpr="Special Equipment Code";
                Visible=FALSE }

  }
  CODE
  {

    LOCAL PROCEDURE ShowBinContents@7301();
    VAR
      BinContent@1000 : Record 7302;
    BEGIN
      BinContent.ShowBinContents("Location Code","Item No.","Variant Code","Bin Code");
    END;

    LOCAL PROCEDURE BinCodeOnAfterValidate@19073508();
    BEGIN
      CurrPage.UPDATE;
    END;

    BEGIN
    END.
  }
}

