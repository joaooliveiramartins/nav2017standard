OBJECT Table 5952 Resource Location
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Resource Location;
               PTG=Localiza��o Recurso];
    LookupPageID=Page6015;
    DrillDownPageID=Page6015;
  }
  FIELDS
  {
    { 1   ;   ;Location Code       ;Code10        ;TableRelation=Location;
                                                   OnValidate=BEGIN
                                                                CALCFIELDS("Location Name");
                                                              END;

                                                   CaptionML=[ENU=Location Code;
                                                              PTG=C�d. Localiza��o] }
    { 2   ;   ;Location Name       ;Text50        ;FieldClass=FlowField;
                                                   CalcFormula=Lookup(Location.Name WHERE (Code=FIELD(Location Code)));
                                                   CaptionML=[ENU=Location Name;
                                                              PTG=Nome Localiza��o];
                                                   Editable=No }
    { 3   ;   ;Resource No.        ;Code20        ;TableRelation=Resource;
                                                   OnValidate=BEGIN
                                                                CALCFIELDS("Resource Name");
                                                              END;

                                                   CaptionML=[ENU=Resource No.;
                                                              PTG=N� Recurso] }
    { 4   ;   ;Resource Name       ;Text50        ;FieldClass=FlowField;
                                                   CalcFormula=Lookup(Resource.Name WHERE (No.=FIELD(Resource No.)));
                                                   CaptionML=[ENU=Resource Name;
                                                              PTG=Nome Recurso];
                                                   Editable=No }
    { 5   ;   ;Starting Date       ;Date          ;CaptionML=[ENU=Starting Date;
                                                              PTG=Data Inicial] }
  }
  KEYS
  {
    {    ;Location Code,Resource No.,Starting Date;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    END.
  }
}

