OBJECT Table 5905 Service Cost
{
  OBJECT-PROPERTIES
  {
    Date=07/09/12;
    Time=12:00:00;
    Version List=NAVW17.00;
  }
  PROPERTIES
  {
    OnDelete=BEGIN
               MoveEntries.MoveServiceCostLedgerEntries(Rec);
             END;

    CaptionML=[ENU=Service Cost;
               PTG=Custo Servi�o];
    LookupPageID=Page5910;
  }
  FIELDS
  {
    { 1   ;   ;Code                ;Code10        ;CaptionML=[ENU=Code;
                                                              PTG=C�digo];
                                                   NotBlank=Yes }
    { 2   ;   ;Description         ;Text50        ;CaptionML=[ENU=Description;
                                                              PTG=Descri��o] }
    { 3   ;   ;Account No.         ;Code20        ;TableRelation="G/L Account";
                                                   CaptionML=[ENU=Account No.;
                                                              PTG=N� Conta] }
    { 4   ;   ;Default Unit Price  ;Decimal       ;CaptionML=[ENU=Default Unit Price;
                                                              PTG=Pre�o Unit�rio Padr�o];
                                                   AutoFormatType=2 }
    { 5   ;   ;Default Quantity    ;Decimal       ;CaptionML=[ENU=Default Quantity;
                                                              PTG=Quantidade Padr�o];
                                                   DecimalPlaces=0:5 }
    { 6   ;   ;Unit of Measure Code;Code10        ;TableRelation="Unit of Measure";
                                                   CaptionML=[ENU=Unit of Measure Code;
                                                              PTG=C�d. Unidade Medida] }
    { 7   ;   ;Cost Type           ;Option        ;OnValidate=BEGIN
                                                                VALIDATE("Service Zone Code");
                                                              END;

                                                   CaptionML=[ENU=Cost Type;
                                                              PTG=Tipo Custo];
                                                   OptionCaptionML=[ENU=Travel,Support,Other;
                                                                    PTG=Desloca��o,Suporte,Outros];
                                                   OptionString=Travel,Support,Other }
    { 8   ;   ;Service Zone Code   ;Code10        ;TableRelation="Service Zone";
                                                   OnValidate=BEGIN
                                                                IF "Service Zone Code" <> '' THEN
                                                                  TESTFIELD("Cost Type","Cost Type"::Travel);
                                                              END;

                                                   CaptionML=[ENU=Service Zone Code;
                                                              PTG=C�d. Zona Servi�o] }
    { 9   ;   ;Default Unit Cost   ;Decimal       ;CaptionML=[ENU=Default Unit Cost;
                                                              PTG=Custo Unit�rio Padr�o];
                                                   AutoFormatType=2 }
  }
  KEYS
  {
    {    ;Code                                    ;Clustered=Yes }
    {    ;Service Zone Code                        }
  }
  FIELDGROUPS
  {
    { 1   ;DropDown            ;Code,Description,Cost Type,Default Unit Price }
  }
  CODE
  {
    VAR
      MoveEntries@1000 : Codeunit 361;

    BEGIN
    END.
  }
}

