OBJECT Page 214 Res. Group Capacity
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Res. Group Capacity;
               PTG=Capacidade Fam�lia Recurso];
    SaveValues=Yes;
    InsertAllowed=No;
    DeleteAllowed=No;
    ModifyAllowed=No;
    LinksAllowed=No;
    SourceTable=Table152;
    DataCaptionExpr='';
    PageType=ListPlus;
    OnOpenPage=BEGIN
                 SetColumns(SetWanted::Initial);
                 UpdateMatrixSubform;
               END;

    ActionList=ACTIONS
    {
      { 1900000004;0 ;ActionContainer;
                      ActionContainerType=ActionItems }
      { 4       ;1   ;Action    ;
                      Name=Previous Set;
                      CaptionML=[ENU=Previous Set;
                                 PTG=Conjunto Anterior];
                      ToolTipML=ENU=Go to the previous set of data.;
                      ApplicationArea=#Jobs;
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=PreviousSet;
                      PromotedCategory=Process;
                      OnAction=BEGIN
                                 SetColumns(SetWanted::Previous);
                                 UpdateMatrixSubform;
                               END;
                                }
      { 5       ;1   ;Action    ;
                      Name=Previous Column;
                      CaptionML=[ENU=Previous Column;
                                 PTG=Coluna Anterior];
                      ToolTipML=ENU=Go to the previous column.;
                      ApplicationArea=#Jobs;
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=PreviousRecord;
                      PromotedCategory=Process;
                      OnAction=BEGIN
                                 SetColumns(SetWanted::PreviousColumn);
                                 UpdateMatrixSubform;
                               END;
                                }
      { 10      ;1   ;Action    ;
                      Name=Next Column;
                      CaptionML=[ENU=Next Column;
                                 PTG=Coluna Seguinte];
                      ToolTipML=ENU=Go to the next column.;
                      ApplicationArea=#Jobs;
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=NextRecord;
                      PromotedCategory=Process;
                      OnAction=BEGIN
                                 SetColumns(SetWanted::NextColumn);
                                 UpdateMatrixSubform;
                               END;
                                }
      { 11      ;1   ;Action    ;
                      Name=Next Set;
                      CaptionML=[ENU=Next Set;
                                 PTG=Conjunto Seguinte];
                      ToolTipML=ENU=Go to the next set of data.;
                      ApplicationArea=#Jobs;
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=NextSet;
                      PromotedCategory=Process;
                      OnAction=BEGIN
                                 SetColumns(SetWanted::Next);
                                 UpdateMatrixSubform;
                               END;
                                }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                CaptionML=[ENU=Matrix Options;
                           PTG=Op��es de Matriz] }

    { 2   ;2   ;Field     ;
                CaptionML=[ENU=View by;
                           PTG=Ver por];
                ToolTipML=ENU=Specifies by which period amounts are displayed.;
                OptionCaptionML=[ENU=Day,Week,Month,Quarter,Year,Accounting Period;
                                 PTG=Dia,Semana,M�s,Trimestre,Ano,Per�odo Contabil�stico];
                ApplicationArea=#Jobs;
                SourceExpr=PeriodType;
                OnValidate=BEGIN
                             SetColumns(SetWanted::Initial);
                             UpdateMatrixSubform;
                           END;
                            }

    { 8   ;2   ;Field     ;
                CaptionML=[ENU=View as;
                           PTG=Ver como];
                ToolTipML=ENU=Specifies how amounts are displayed. Net Change: The net change in the balance for the selected period. Balance at Date: The balance as of the last day in the selected period.;
                OptionCaptionML=[ENU=Net Change,Balance at Date;
                                 PTG=Saldo Per�odo,Saldo � Data];
                ApplicationArea=#Jobs;
                SourceExpr=QtyType;
                OnValidate=BEGIN
                             UpdateMatrixSubform;
                           END;
                            }

    { 1102601000;1;Part   ;
                Name=MatrixForm;
                ApplicationArea=#Jobs;
                PagePartID=Page9243 }

  }
  CODE
  {
    VAR
      MatrixRecords@1008 : ARRAY [32] OF Record 2000000007;
      PeriodType@1001 : 'Day,Week,Month,Quarter,Year,Accounting Period';
      QtyType@1002 : 'Net Change,Balance at Date';
      MatrixColumnCaptions@1006 : ARRAY [32] OF Text[1024];
      ColumnSet@1005 : Text[1024];
      SetWanted@1014 : 'Initial,Previous,Same,Next,PreviousColumn,NextColumn';
      PKFirstRecInCurrSet@1013 : Text[100];
      CurrSetLength@1012 : Integer;

    PROCEDURE SetColumns@11(SetWanted@1001 : 'Initial,Previous,Same,Next,PreviousColumn,NextColumn');
    VAR
      MatrixMgt@1000 : Codeunit 9200;
    BEGIN
      MatrixMgt.GeneratePeriodMatrixData(SetWanted,12,FALSE,PeriodType,'',
        PKFirstRecInCurrSet,MatrixColumnCaptions,ColumnSet,CurrSetLength,MatrixRecords);
    END;

    LOCAL PROCEDURE UpdateMatrixSubform@1102601000();
    BEGIN
      CurrPage.MatrixForm.PAGE.Load(PeriodType,QtyType,MatrixColumnCaptions,MatrixRecords);
      CurrPage.UPDATE(FALSE);
    END;

    BEGIN
    END.
  }
}

