OBJECT Codeunit 31022915 Imp Jnl.-Post Batch
{
  OBJECT-PROPERTIES
  {
    Date=13/02/15;
    Time=13:00:00;
    Version List=NAVPTSS82.00;
  }
  PROPERTIES
  {
    TableNo=31022970;
    Permissions=TableData 5620=imd;
    OnRun=BEGIN
            ImpJnlLine.COPY(Rec);
            Code;
            Rec := ImpJnlLine;
          END;

  }
  CODE
  {
    VAR
      Text31022890@1000 : TextConst 'ENU=cannot exceed %1 characters;PTG=n�o pode ultrapassar %1 caracteres';
      Text31022891@1001 : TextConst 'ENU=Journal Batch Name    #1##########\\;PTG=Nome Sec��o Di�rio    #1##########\\';
      Text31022892@1002 : TextConst 'ENU=Checking lines        #2######\;PTG=A verificar linhas    #2######\';
      Text31022893@1005 : TextConst 'ENU=Posting lines         #3###### @4@@@@@@@@@@@@@;PTG=A registar linhas     #3###### @4@@@@@@@@@@@@@';
      Text31022894@1006 : TextConst 'ENU=A maximum of %1 posting number series can be used in each journal.;PTG=Pode utilizar um m�ximo de %1 n�meros de s�rie de registo em cada di�rio.';
      ImpJnlLine@1008 : Record 31022970;
      ImpJnlLine2@1009 : Record 31022970;
      ImpJnlLine3@1010 : Record 31022970;
      ImpJnlTemplate@1011 : Record 31022968;
      ImpJnlBatch@1012 : Record 31022969;
      ImpLedgEntry@1015 : Record 31022967;
      NoSeries@1016 : TEMPORARY Record 308;
      DimSetEntry@1000000000 : Record 480;
      Imparities@1014 : Record 31022965;
      GenJnlPostLine@1013 : Codeunit 12;
      FAJnlCheckLine@1018 : Codeunit 5631;
      NoSeriesMgt@1020 : Codeunit 396;
      NoSeriesMgt2@1021 : ARRAY [10] OF Codeunit 396;
      DimMgt@1022 : Codeunit 408;
      ImpJnlPostLine@1000000001 : Codeunit 31022916;
      LastDocNo@1007 : Code[20];
      LastDocNo2@1004 : Code[20];
      LastPostedDocNo@1003 : Code[20];
      "0DF"@1017 : DateFormula;
      Window@1023 : Dialog;
      LineCount@1024 : Integer;
      StartLineNo@1025 : Integer;
      NoOfRecords@1026 : Integer;
      FARegNo@1027 : Integer;
      NoOfPostingNoSeries@1031 : Integer;
      PostingNoSeriesNo@1032 : Integer;

    LOCAL PROCEDURE Code@1();
    VAR
      GenJnlLine@1102058000 : Record 81;
      SourceCodeSetup@1102058003 : Record 242;
      FirstEntryNo@1102058005 : BigInteger;
      ImpEntryNo@1102058007 : Record 31022967;
      GLEntryNo@1102058008 : Record 17;
    BEGIN
      WITH ImpJnlLine DO BEGIN
        SETRANGE("Journal Template Name","Journal Template Name");
        SETRANGE("Journal Batch Name","Journal Batch Name");
        IF RECORDLEVELLOCKING THEN
          LOCKTABLE;

        ImpJnlTemplate.GET("Journal Template Name");
        ImpJnlBatch.GET("Journal Template Name","Journal Batch Name");
        IF STRLEN(INCSTR(ImpJnlBatch.Name)) > MAXSTRLEN(ImpJnlBatch.Name) THEN
          ImpJnlBatch.FIELDERROR(
            Name,
            STRSUBSTNO(
              Text31022890,
              MAXSTRLEN(ImpJnlBatch.Name)));


        IF NOT FIND('=><') THEN BEGIN
          "Line No." := 0;
          COMMIT;
          EXIT;
        END;

        Window.OPEN(
            Text31022891 +
            Text31022892 +
            Text31022893);
        Window.UPDATE(1,"Journal Batch Name");

        // Check lines
        LineCount := 0;
        StartLineNo := "Line No.";
        REPEAT
          LineCount := LineCount + 1;
          Window.UPDATE(2,LineCount);

          IF NEXT = 0 THEN
            FINDFIRST;
        UNTIL "Line No." = StartLineNo;
        NoOfRecords := LineCount;

        IF ImpJnlBatch."Post to G/L" THEN BEGIN
          SourceCodeSetup.GET;
          GenJnlLine.SETRANGE("Journal Template Name",'');
          GenJnlLine.SETRANGE("Journal Batch Name",'');
          GenJnlLine.DELETEALL;
          FINDSET;
          REPEAT
            Imparities.GET("Imparity Code");
            GenJnlLine.INIT;
            GenJnlLine."Line No." := "Line No.";
            GenJnlLine."Account Type" := GenJnlLine."Account Type"::"G/L Account";
            GenJnlLine.VALIDATE("Account No.","Account No.");
            GenJnlLine."Posting Date" := "Posting Date";
            GenJnlLine."Source Code" := SourceCodeSetup."Imparities Journal";
            GenJnlLine."Document No." := "Document No.";
            GenJnlLine.Description := Description;
            GenJnlLine."Bal. Account Type" := GenJnlLine."Bal. Account Type"::"G/L Account";
            GenJnlLine.VALIDATE("Bal. Account No.","Bal. Account No.");
            GenJnlLine.VALIDATE(Quantity,Quantity);
            GenJnlLine.VALIDATE(Amount,Amount);
            GenJnlLine.VALIDATE("Shortcut Dimension 1 Code","Shortcut Dimension 1 Code");
            GenJnlLine.VALIDATE("Shortcut Dimension 2 Code","Shortcut Dimension 2 Code");
            GenJnlLine.VALIDATE("DRF Code","DRF Code");
            GenJnlLine.VALIDATE("Bal. DRF Code","Bal. DRF Code");
            GenJnlLine."Imparity Code" := "Imparity Code";
            GenJnlLine."Imparity Type" := "Imparity Type";
            GenJnlLine."Imparity SubType" := "Imparity SubType";
            GenJnlLine."Tax behavior" := "Tax behavior";
            GenJnlLine."Imparity Posting Gr." := "Imparity Posting Gr.";
            GenJnlLine."Imparity Entry" := "Imparity Entry";
            IF Correction THEN
              GenJnlLine.VALIDATE(Correction);
            GenJnlLine."Dimension Set ID":= "Dimension Set ID";
            GenJnlLine.INSERT;
          UNTIL NEXT = 0;
        END;
        IF RECORDLEVELLOCKING THEN
          IF ImpLedgEntry.FINDLAST THEN;

        // Post lines
        LineCount := 0;
        LastDocNo := '';
        LastDocNo2 := '';
        LastPostedDocNo := '';
        FINDSET;
        REPEAT
          LineCount := LineCount + 1;
          Window.UPDATE(3,LineCount);
          Window.UPDATE(4,ROUND(LineCount / NoOfRecords * 10000,1));
          IF NOT ("Imparity Code" = '') AND
             (ImpJnlBatch."No. Series" <> '') AND
             ("Document No." <> LastDocNo2)
          THEN
            TESTFIELD("Document No.",NoSeriesMgt.GetNextNo(ImpJnlBatch."No. Series","Posting Date",FALSE));
          LastDocNo2 := "Document No.";
          IF "Posting No. Series" = '' THEN
            "Posting No. Series" := ImpJnlBatch."No. Series"
          ELSE
            IF NOT ("Imparity Code" = '') THEN
              IF "Document No." = LastDocNo THEN
                "Document No." := LastPostedDocNo
              ELSE BEGIN
                IF NOT NoSeries.GET("Posting No. Series") THEN BEGIN
                  NoOfPostingNoSeries := NoOfPostingNoSeries + 1;
                  IF NoOfPostingNoSeries > ARRAYLEN(NoSeriesMgt2) THEN
                    ERROR(
                      Text31022894,
                      ARRAYLEN(NoSeriesMgt2));
                  NoSeries.Code := "Posting No. Series";
                  NoSeries.Description := FORMAT(NoOfPostingNoSeries);
                  NoSeries.INSERT;
                END;
                LastDocNo := "Document No.";
                EVALUATE(PostingNoSeriesNo,NoSeries.Description);
                "Document No." := NoSeriesMgt2[PostingNoSeriesNo].GetNextNo("Posting No. Series","Posting Date",FALSE);
                LastPostedDocNo := "Document No.";
              END;

          ImpJnlPostLine.ImpJnlPostLine(ImpJnlLine,FALSE);

          IF FirstEntryNo = 0 THEN BEGIN
            ImpEntryNo.FINDLAST;
            FirstEntryNo := ImpEntryNo."Entry No.";
          END;
        UNTIL NEXT = 0;

        IF ImpJnlBatch."Post to G/L" THEN BEGIN
          GenJnlLine.RESET;
          GenJnlLine.SETRANGE("Journal Template Name",'');
          GenJnlLine.SETRANGE("Journal Batch Name",'');
          GenJnlLine.SETRANGE("Document No.","Document No.");
          IF GenJnlLine.FINDSET THEN REPEAT

            //missing
            GenJnlPostLine.RunWithCheck(GenJnlLine);
            GLEntryNo.FINDLAST;
            GLEntryNo."Imparity Ldg. Entry No." := FirstEntryNo;
            GLEntryNo.MODIFY;
            FirstEntryNo += 1;
          UNTIL GenJnlLine.NEXT = 0;
        END;
        INIT;

        // Update/delete lines
        IF NOT RECORDLEVELLOCKING THEN BEGIN
          LOCKTABLE(TRUE,TRUE);
        END;

        ImpJnlLine2.COPY(ImpJnlLine);
        ImpJnlLine2.SETFILTER("Imparity Code",'<>%1','');
        IF ImpJnlLine2.FINDLAST THEN; // Remember the last line

        ImpJnlLine3.COPY(ImpJnlLine);
        ImpJnlLine3.DELETEALL;
        ImpJnlLine3.RESET;
        ImpJnlLine3.SETRANGE("Journal Template Name","Journal Template Name");
        ImpJnlLine3.SETRANGE("Journal Batch Name","Journal Batch Name");

        IF NOT ImpJnlLine3.FINDLAST THEN
          IF INCSTR("Journal Batch Name") <> '' THEN BEGIN
            ImpJnlBatch.GET("Journal Template Name","Journal Batch Name");
            ImpJnlBatch.DELETE;
            ImpJnlBatch.Name := INCSTR("Journal Batch Name");
            IF ImpJnlBatch.INSERT THEN;
            "Journal Batch Name" := ImpJnlBatch.Name;
          END;

        ImpJnlLine3.SETRANGE("Journal Batch Name","Journal Batch Name");
        IF (ImpJnlBatch."No. Series" = '') AND ImpJnlLine3.ISEMPTY THEN BEGIN
          ImpJnlLine3.INIT;
          ImpJnlLine3."Journal Template Name" := "Journal Template Name";
          ImpJnlLine3."Journal Batch Name" := "Journal Batch Name";
          ImpJnlLine3."Line No." := 10000;
          ImpJnlLine3.INSERT;
          ImpJnlLine3.SetUpNewLine(ImpJnlLine2);
          ImpJnlLine3.MODIFY;
        END;

        IF ImpJnlBatch."No. Series" <> '' THEN
          NoSeriesMgt.SaveNoSeries;
        IF NoSeries.FINDSET THEN
          REPEAT
            EVALUATE(PostingNoSeriesNo,NoSeries.Description);
            NoSeriesMgt2[PostingNoSeriesNo].SaveNoSeries;
          UNTIL NoSeries.NEXT = 0;

        COMMIT;
        CLEAR(FAJnlCheckLine);
        CLEAR(ImpJnlPostLine);
      END;
      COMMIT;
    END;

    BEGIN
    END.
  }
}

