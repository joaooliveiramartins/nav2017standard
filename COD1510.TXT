OBJECT Codeunit 1510 Notification Management
{
  OBJECT-PROPERTIES
  {
    Date=21/02/17;
    Time=16:47:36;
    Modified=Yes;
    Version List=NAVW110.0;
  }
  PROPERTIES
  {
    Permissions=TableData 458=i,
                TableData 1510=r,
                TableData 1511=rimd;
    OnRun=BEGIN
          END;

  }
  CODE
  {
    VAR
      OverdueEntriesMsg@1047 : TextConst 'ENU=Overdue approval entries have been created.;PTG=Foram criados movimentos aprova��o vencidos.';
      SalesTxt@1008 : TextConst 'ENU=Sales;PTG=Vendas';
      PurchaseTxt@1007 : TextConst 'ENU=Purchase;PTG=Compra';
      ServiceTxt@1006 : TextConst 'ENU=Service;PTG=Servi�o';
      SalesInvoiceTxt@1005 : TextConst 'ENU=Sales Invoice;PTG=Fatura Venda';
      PurchaseInvoiceTxt@1004 : TextConst 'ENU=Purchase Invoice;PTG=Fatura Compra';
      ServiceInvoiceTxt@1003 : TextConst 'ENU=Service Invoice;PTG=Fatura Servi�o';
      SalesCreditMemoTxt@1002 : TextConst 'ENU=Sales Credit Memo;PTG=Nota Cr�dito Venda';
      PurchaseCreditMemoTxt@1001 : TextConst 'ENU=Purchase Credit Memo;PTG=Nota Cr�dito Compra';
      ServiceCreditMemoTxt@1000 : TextConst 'ENU=Service Credit Memo;PTG=Nota Cr�dito Servi�o';
      ActionNewRecordTxt@1015 : TextConst '@@@=E.g. Sales Invoice 10000 has been created.;ENU=has been created.;PTG=foi criada.';
      ActionApproveTxt@1014 : TextConst 'ENU=requires your approval.;PTG=requer a sua aprova��o.';
      ActionApprovedTxt@1013 : TextConst 'ENU=has been approved.;PTG=foi aprovado.';
      ActionApprovalCreatedTxt@1012 : TextConst '@@@=E.g. Sales Invoice 10000 approval request has been created.;ENU=approval request has been created.;PTG=pedido de aprova��o foi criado.';
      ActionApprovalCanceledTxt@1011 : TextConst '@@@=E.g. Sales Invoice 10000 approval request has been canceled.;ENU=approval request has been canceled.;PTG=pedido aprova��o foi cancelado.';
      ActionApprovalRejectedTxt@1010 : TextConst '@@@=E.g. Sales Invoice 10000 approval request has been rejected.;ENU=approval has been rejected.;PTG=pedido de aprova��o foi rejeitado.';
      ActionOverdueTxt@1009 : TextConst '@@@="%1 = date when document was due.";ENU=has a pending approval with due date %1.;PTG=tem uma aprova��o pendente com data de vencimento %1.';

    PROCEDURE CreateOverdueNotifications@14(WorkflowStepArgument@1002 : Record 1523);
    VAR
      UserSetup@1006 : Record 91;
      ApprovalEntry@1000 : Record 454;
      OverdueApprovalEntry@1001 : Record 458;
      NotificationEntry@1003 : Record 1511;
    BEGIN
      IF UserSetup.FINDSET THEN
        REPEAT
          ApprovalEntry.RESET;
          ApprovalEntry.SETRANGE("Approver ID",UserSetup."User ID");
          ApprovalEntry.SETRANGE(Status,ApprovalEntry.Status::Open);
          ApprovalEntry.SETFILTER("Due Date",'<=%1',TODAY);
          IF ApprovalEntry.FINDSET THEN
            REPEAT
              InsertOverdueEntry(ApprovalEntry,OverdueApprovalEntry);
              NotificationEntry.CreateNew(NotificationEntry.Type::Overdue,
                UserSetup."User ID",OverdueApprovalEntry,WorkflowStepArgument."Link Target Page",
                WorkflowStepArgument."Custom Link");
            UNTIL ApprovalEntry.NEXT = 0;
        UNTIL UserSetup.NEXT = 0;

      MESSAGE(OverdueEntriesMsg);
    END;

    LOCAL PROCEDURE InsertOverdueEntry@13(ApprovalEntry@1000 : Record 454;VAR OverdueApprovalEntry@1001 : Record 458);
    VAR
      User@1002 : Record 2000000120;
      UserSetup@1003 : Record 91;
    BEGIN
      WITH OverdueApprovalEntry DO BEGIN
        INIT;
        "Approver ID" := ApprovalEntry."Approver ID";
        User.SETRANGE("User Name",ApprovalEntry."Approver ID");
        IF User.FINDFIRST THEN BEGIN
          "Sent to Name" := COPYSTR(User."Full Name",1,MAXSTRLEN("Sent to Name"));
          UserSetup.GET(User."User Name");
        END;

        "Table ID" := ApprovalEntry."Table ID";
        "Document Type" := ApprovalEntry."Document Type";
        "Document No." := ApprovalEntry."Document No.";
        "Sent to ID" := ApprovalEntry."Approver ID";
        "Sent Date" := TODAY;
        "Sent Time" := TIME;
        "E-Mail" := UserSetup."E-Mail";
        "Sequence No." := ApprovalEntry."Sequence No.";
        "Due Date" := ApprovalEntry."Due Date";
        "Approval Code" := ApprovalEntry."Approval Code";
        "Approval Type" := ApprovalEntry."Approval Type";
        "Limit Type" := ApprovalEntry."Limit Type";
        "Record ID to Approve" := ApprovalEntry."Record ID to Approve";
        INSERT;
      END;
    END;

    PROCEDURE CreateDefaultNotificationSetup@1(NotificationType@1000 : Option);
    VAR
      NotificationSetup@1001 : Record 1512;
    BEGIN
      IF DefaultNotificationEntryExists(NotificationType) THEN
        EXIT;

      NotificationSetup.INIT;
      NotificationSetup.VALIDATE("Notification Type",NotificationType);
      NotificationSetup.VALIDATE("Notification Method",NotificationSetup."Notification Method"::Email);
      NotificationSetup.INSERT(TRUE);
    END;

    LOCAL PROCEDURE DefaultNotificationEntryExists@4(NotificationType@1000 : Option) : Boolean;
    VAR
      NotificationSetup@1001 : Record 1512;
    BEGIN
      NotificationSetup.SETRANGE("User ID",'');
      NotificationSetup.SETRANGE("Notification Type",NotificationType);
      EXIT(NOT NotificationSetup.ISEMPTY)
    END;

    PROCEDURE MoveNotificationEntryToSentNotificationEntries@11(VAR NotificationEntry@1000 : Record 1511;NotificationBody@1001 : Text;AggregatedNotifications@1002 : Boolean;NotificationMethod@1005 : Option);
    VAR
      SentNotificationEntry@1003 : Record 1514;
      InitialSentNotificationEntry@1006 : Record 1514;
    BEGIN
      IF AggregatedNotifications THEN BEGIN
        IF NotificationEntry.FINDSET THEN BEGIN
          InitialSentNotificationEntry.NewRecord(NotificationEntry,NotificationBody,NotificationMethod);
          WHILE NotificationEntry.NEXT <> 0 DO BEGIN
            SentNotificationEntry.NewRecord(NotificationEntry,NotificationBody,NotificationMethod);
            SentNotificationEntry.VALIDATE("Aggregated with Entry",InitialSentNotificationEntry.ID);
            SentNotificationEntry.MODIFY(TRUE);
          END;
        END;
        NotificationEntry.DELETEALL(TRUE);
      END ELSE BEGIN
        SentNotificationEntry.NewRecord(NotificationEntry,NotificationBody,NotificationMethod);
        NotificationEntry.DELETE(TRUE);
      END;
    END;

    PROCEDURE GetDocumentTypeAndNumber@3(VAR RecRef@1000 : RecordRef;VAR DocumentType@1001 : Text;VAR DocumentNo@1002 : Text);
    VAR
      FieldRef@1003 : FieldRef;
    BEGIN
      CASE RecRef.NUMBER OF
        DATABASE::"Incoming Document":
          BEGIN
            DocumentType := RecRef.CAPTION;
            FieldRef := RecRef.FIELD(2);
            DocumentNo := FORMAT(FieldRef.VALUE);
          END;
        DATABASE::"Sales Header":
          BEGIN
            FieldRef := RecRef.FIELD(1);
            DocumentType := SalesTxt + ' ' + FORMAT(FieldRef.VALUE);
            FieldRef := RecRef.FIELD(3);
            DocumentNo := FORMAT(FieldRef.VALUE);
          END;
        DATABASE::"Purchase Header":
          BEGIN
            FieldRef := RecRef.FIELD(1);
            DocumentType := PurchaseTxt + ' ' + FORMAT(FieldRef.VALUE);
            FieldRef := RecRef.FIELD(3);
            DocumentNo := FORMAT(FieldRef.VALUE);
          END;
        DATABASE::"Service Header":
          BEGIN
            FieldRef := RecRef.FIELD(1);
            DocumentType := ServiceTxt + ' ' + FORMAT(FieldRef.VALUE);
            FieldRef := RecRef.FIELD(3);
            DocumentNo := FORMAT(FieldRef.VALUE);
          END;
        DATABASE::"Sales Invoice Header":
          BEGIN
            DocumentType := SalesInvoiceTxt;
            FieldRef := RecRef.FIELD(3);
            DocumentNo := FORMAT(FieldRef.VALUE);
          END;
        DATABASE::"Purch. Inv. Header":
          BEGIN
            DocumentType := PurchaseInvoiceTxt;
            FieldRef := RecRef.FIELD(3);
            DocumentNo := FORMAT(FieldRef.VALUE);
          END;
        DATABASE::"Service Invoice Header":
          BEGIN
            DocumentType := ServiceInvoiceTxt;
            FieldRef := RecRef.FIELD(3);
            DocumentNo := FORMAT(FieldRef.VALUE);
          END;
        DATABASE::"Sales Cr.Memo Header":
          BEGIN
            DocumentType := SalesCreditMemoTxt;
            FieldRef := RecRef.FIELD(3);
            DocumentNo := FORMAT(FieldRef.VALUE);
          END;
        DATABASE::"Purch. Cr. Memo Hdr.":
          BEGIN
            DocumentType := PurchaseCreditMemoTxt;
            FieldRef := RecRef.FIELD(3);
            DocumentNo := FORMAT(FieldRef.VALUE);
          END;
        DATABASE::"Service Cr.Memo Header":
          BEGIN
            DocumentType := ServiceCreditMemoTxt;
            FieldRef := RecRef.FIELD(3);
            DocumentNo := FORMAT(FieldRef.VALUE);
          END;
        DATABASE::"Gen. Journal Line":
          BEGIN
            DocumentType := RecRef.CAPTION;
            FieldRef := RecRef.FIELD(1);
            DocumentNo := FORMAT(FieldRef.VALUE);
            FieldRef := RecRef.FIELD(51);
            DocumentNo += ',' + FORMAT(FieldRef.VALUE);
            FieldRef := RecRef.FIELD(2);
            DocumentNo += ',' + FORMAT(FieldRef.VALUE);
          END;
        DATABASE::"Gen. Journal Batch":
          BEGIN
            DocumentType := RecRef.CAPTION;
            FieldRef := RecRef.FIELD(1);
            DocumentNo := FORMAT(FieldRef.VALUE);
            FieldRef := RecRef.FIELD(2);
            DocumentNo += ',' + FORMAT(FieldRef.VALUE);
          END;
        DATABASE::Customer,
        DATABASE::Vendor,
        DATABASE::Item:
          BEGIN
            DocumentType := RecRef.CAPTION;
            FieldRef := RecRef.FIELD(1);
            DocumentNo := FORMAT(FieldRef.VALUE);
          END;
        ELSE BEGIN
          DocumentType := RecRef.CAPTION;
          FieldRef := RecRef.FIELD(3);
          DocumentNo := FORMAT(FieldRef.VALUE);
        END;
      END;
    END;

    PROCEDURE GetActionTextFor@7(VAR NotificationEntry@1000 : Record 1511) : Text;
    VAR
      ApprovalEntry@1001 : Record 454;
      DataTypeManagement@1002 : Codeunit 701;
      RecRef@1003 : RecordRef;
    BEGIN
      CASE NotificationEntry.Type OF
        NotificationEntry.Type::"New Record":
          EXIT(ActionNewRecordTxt);
        NotificationEntry.Type::Approval:
          BEGIN
            DataTypeManagement.GetRecordRef(NotificationEntry."Triggered By Record",RecRef);
            RecRef.SETTABLE(ApprovalEntry);
            CASE ApprovalEntry.Status OF
              ApprovalEntry.Status::Open:
                EXIT(ActionApproveTxt);
              ApprovalEntry.Status::Canceled:
                EXIT(ActionApprovalCanceledTxt);
              ApprovalEntry.Status::Rejected:
                EXIT(ActionApprovalRejectedTxt);
              ApprovalEntry.Status::Created:
                EXIT(ActionApprovalCreatedTxt);
              ApprovalEntry.Status::Approved:
                EXIT(ActionApprovedTxt);
            END;
          END;
        NotificationEntry.Type::Overdue:
          EXIT(ActionOverdueTxt);
      END;
    END;

    BEGIN
    END.
  }
}

