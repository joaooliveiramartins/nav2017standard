OBJECT Table 2000000161 NAV App Dependencies
{
  OBJECT-PROPERTIES
  {
    Date=13/04/17;
    Time=14:36:27;
    Version List=NAVW19.00;
  }
  PROPERTIES
  {
    DataPerCompany=No;
    CaptionML=[ENU=NAV App Dependencies;
               PTG=Dependencias NAV App];
  }
  FIELDS
  {
    { 1   ;   ;Package ID          ;GUID          ;CaptionML=[ENU=Package ID;
                                                              PTG=ID Pacote] }
    { 2   ;   ;ID                  ;GUID          ;CaptionML=[ENU=ID;
                                                              PTG=ID] }
    { 3   ;   ;Name                ;Text250       ;CaptionML=[ENU=Name;
                                                              PTG=Nome] }
    { 4   ;   ;Publisher           ;Text250       ;CaptionML=[ENU=Publisher;
                                                              PTG=Publicador] }
    { 5   ;   ;Version Major       ;Integer       ;CaptionML=[ENU=Version Major;
                                                              PTG=Vers�o Major] }
    { 6   ;   ;Version Minor       ;Integer       ;CaptionML=[ENU=Version Minor;
                                                              PTG=Vers�o Minor] }
    { 7   ;   ;Version Build       ;Integer       ;CaptionML=[ENU=Version Build;
                                                              PTG=Vers�o Build] }
    { 8   ;   ;Version Revision    ;Integer       ;CaptionML=[ENU=Version Revision;
                                                              PTG=Vers�o Revis�o] }
    { 9   ;   ;Compatibility Major ;Integer       ;CaptionML=[ENU=Compatibility Major;
                                                              PTG=Compatibilidade Major] }
    { 10  ;   ;Compatibility Minor ;Integer       ;CaptionML=[ENU=Compatibility Minor;
                                                              PTG=Compatibilidade Minor] }
    { 11  ;   ;Compatibility Build ;Integer       ;CaptionML=[ENU=Compatibility Build;
                                                              PTG=Compatibilidade Build] }
    { 12  ;   ;Compatibility Revision;Integer     ;CaptionML=[ENU=Compatibility Revision;
                                                              PTG=Compatibilidade Revis�o] }
  }
  KEYS
  {
    {    ;Package ID,ID                           ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    END.
  }
}

