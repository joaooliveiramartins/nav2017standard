OBJECT Codeunit 1401 Cancel PstdPurchCrM (Yes/No)
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0;
  }
  PROPERTIES
  {
    TableNo=124;
    Permissions=TableData 112=rm,
                TableData 114=rm;
    OnRun=BEGIN
            CancelCrMemo(Rec);
          END;

  }
  CODE
  {
    VAR
      CancelPostedCrMemoQst@1000 : TextConst 'ENU=The posted purchase credit memo will be canceled, and a purchase invoice will be created and posted, which reverses the posted purchase credit memo. Do you want to continue?;PTG=A nota de cr�dito de compra registada vai ser cancelada e uma fatura de compra vai ser criada e registada, o que vai estornar a nota de cr�dito de compra registada. Deseja continuar?';
      OpenPostedInvQst@1001 : TextConst 'ENU=The invoice was successfully created. Do you want to open the posted invoice?;PTG=A fatura foi registada com sucesso. Deseja abrir a fatura registada?';

    LOCAL PROCEDURE CancelCrMemo@1(VAR PurchCrMemoHdr@1002 : Record 124) : Boolean;
    VAR
      PurchInvHeader@1003 : Record 122;
      CancelledDocument@1000 : Record 1900;
      CancelPostedPurchCrMemo@1001 : Codeunit 1402;
    BEGIN
      CancelPostedPurchCrMemo.TestCorrectCrMemoIsAllowed(PurchCrMemoHdr);
      IF CONFIRM(CancelPostedCrMemoQst) THEN
        IF CancelPostedPurchCrMemo.CancelPostedCrMemo(PurchCrMemoHdr) THEN
          IF CONFIRM(OpenPostedInvQst) THEN BEGIN
            CancelledDocument.FindPurchCancelledCrMemo(PurchCrMemoHdr."No.");
            PurchInvHeader.GET(CancelledDocument."Cancelled By Doc. No.");
            PAGE.RUN(PAGE::"Posted Purchase Invoice",PurchInvHeader);
            EXIT(TRUE);
          END;

      EXIT(FALSE);
    END;

    BEGIN
    END.
  }
}

