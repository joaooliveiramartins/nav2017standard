OBJECT Page 390 Phys. Inventory Ledger Entries
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0;
  }
  PROPERTIES
  {
    Editable=No;
    CaptionML=[ENU=Phys. Inventory Ledger Entries;
               PTG=Movs. Invent�rio F�sico];
    SourceTable=Table281;
    DataCaptionFields=Item No.;
    PageType=List;
    ActionList=ACTIONS
    {
      { 1900000003;0 ;ActionContainer;
                      ActionContainerType=RelatedInformation }
      { 52      ;1   ;ActionGroup;
                      CaptionML=[ENU=Ent&ry;
                                 PTG=M&ovimento];
                      Image=Entry }
      { 53      ;2   ;Action    ;
                      AccessByPermission=TableData 348=R;
                      ShortCutKey=Shift+Ctrl+D;
                      CaptionML=[ENU=Dimensions;
                                 PTG=Dimens�es];
                      ToolTipML=ENU=View or edit dimensions, such as area, project, or department, that you can assign to sales and purchase documents to distribute costs and analyze transaction history.;
                      Image=Dimensions;
                      OnAction=BEGIN
                                 ShowDimensions;
                                 CurrPage.SAVERECORD;
                               END;
                                }
      { 1900000004;0 ;ActionContainer;
                      ActionContainerType=ActionItems }
      { 45      ;1   ;Action    ;
                      CaptionML=[ENU=&Navigate;
                                 PTG=&Navegar];
                      Promoted=Yes;
                      Image=Navigate;
                      PromotedCategory=Process;
                      OnAction=BEGIN
                                 Navigate.SetDoc("Posting Date","Document No.");
                                 Navigate.RUN;
                               END;
                                }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 2   ;2   ;Field     ;
                ToolTipML=ENU=Specifies the date on which you posted the Physical Inventory Journal that created this ledger entry.;
                SourceExpr="Posting Date" }

    { 4   ;2   ;Field     ;
                ToolTipML=ENU=Specifies the type of transaction that was posted from the Physical Inventory Journal that created the ledger entry.;
                SourceExpr="Entry Type" }

    { 6   ;2   ;Field     ;
                ToolTipML=ENU=Specifies the document number on the physical inventory ledger entry.;
                SourceExpr="Document No." }

    { 8   ;2   ;Field     ;
                ToolTipML=ENU=Specifies the number of the item in the ledger entry.;
                SourceExpr="Item No." }

    { 46  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the variant code for the items.;
                SourceExpr="Variant Code";
                Visible=FALSE }

    { 10  ;2   ;Field     ;
                ToolTipML=ENU=Specifies a description of the item in the ledger entry.;
                SourceExpr=Description }

    { 12  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the dimension value code that the ledger entry is linked to.;
                SourceExpr="Global Dimension 1 Code";
                Visible=FALSE }

    { 14  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the dimension value code that the ledger entry is linked to.;
                SourceExpr="Global Dimension 2 Code";
                Visible=FALSE }

    { 16  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the code of the location for the item in the ledger entry.;
                SourceExpr="Location Code";
                Visible=TRUE }

    { 18  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the quantity on hand, as calculated by the program, of the item in the ledger entry.;
                SourceExpr="Qty. (Calculated)" }

    { 20  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the actual quantity on hand, as determined by taking a physical inventory, of the item in the ledger entry.;
                SourceExpr="Qty. (Phys. Inventory)" }

    { 22  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the difference between the quantities in the Quantity (Calculated) field and the Quantity (Physical Inventory) field for ledger entry.;
                SourceExpr=Quantity }

    { 24  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the amount for one unit of the item in the ledger entry.;
                SourceExpr="Unit Amount";
                Visible=FALSE }

    { 26  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the total amount for the items in the ledger entry.;
                SourceExpr=Amount }

    { 28  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the direct unit cost of the item in the ledger entry.;
                SourceExpr="Unit Cost" }

    { 32  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the ID of the user who is associated with the ledger entry.;
                SourceExpr="User ID";
                Visible=FALSE }

    { 34  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the source code associated with the ledger entry.;
                SourceExpr="Source Code";
                Visible=FALSE }

    { 36  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the reason code in the ledger entry.;
                SourceExpr="Reason Code";
                Visible=FALSE }

    { 38  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the number of the last item ledger entry that provided the basis for the physical inventory ledger entry.;
                SourceExpr="Last Item Ledger Entry No." }

    { 40  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the entry number assigned to the ledger entry.;
                SourceExpr="Entry No." }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {
    VAR
      Navigate@1000 : Page 344;

    BEGIN
    END.
  }
}

