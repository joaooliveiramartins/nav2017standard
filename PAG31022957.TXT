OBJECT Page 31022957 Posted Bill Groups
{
  OBJECT-PROPERTIES
  {
    Date=12/04/17;
    Time=14:49:35;
    Modified=Yes;
    Version List=NAVPTSS10.00;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Posted Bill Groups;
               PTG=Remessas Registadas];
    InsertAllowed=No;
    DeleteAllowed=No;
    SourceTable=Table31022939;
    DataCaptionExpr=Caption;
    PageType=Document;
    RefreshOnActivate=Yes;
    ActionList=ACTIONS
    {
      { 1900000003;0 ;ActionContainer;
                      ActionContainerType=RelatedInformation }
      { 35      ;1   ;ActionGroup;
                      Name=BillGroup;
                      CaptionML=[ENU=Bill &Group;
                                 PTG=&Remessa] }
      { 43      ;2   ;Action    ;
                      Name=Comments;
                      CaptionML=[ENU=Co&mments;
                                 PTG=Co&ment�rios];
                      ApplicationArea=#Basic,#Suite;
                      RunObject=Page 31022969;
                      RunPageLink=Type=FILTER(Receivable),
                                  BG/PO No.=FIELD(No.);
                      Image=ViewComments }
      { 20      ;2   ;Separator  }
      { 51      ;2   ;Action    ;
                      Name=Analysis;
                      CaptionML=[ENU=Analysis;
                                 PTG=An�lise];
                      ApplicationArea=#Basic,#Suite;
                      RunObject=Page 31022965;
                      RunPageLink=No.=FIELD(No.),
                                  Due Date Filter=FIELD(Due Date Filter),
                                  Global Dimension 1 Filter=FIELD(Global Dimension 1 Filter),
                                  Global Dimension 2 Filter=FIELD(Global Dimension 2 Filter),
                                  Category Filter=FIELD(Category Filter);
                      Image=Report }
      { 58      ;2   ;Separator  }
      { 16      ;2   ;Action    ;
                      Name=Listing;
                      Ellipsis=Yes;
                      CaptionML=[ENU=Listing;
                                 PTG=Listagem];
                      ApplicationArea=#Basic,#Suite;
                      Promoted=Yes;
                      Image=List;
                      PromotedCategory=Report;
                      OnAction=BEGIN
                                 IF FIND THEN BEGIN
                                   PostedBillGr.COPY(Rec);
                                   PostedBillGr.SETRECFILTER;
                                   REPORT.RUN(REPORT::"Posted Bill Group Listing",TRUE,FALSE,PostedBillGr);
                                 END;
                               END;
                                }
      { 1900000004;0 ;ActionContainer;
                      ActionContainerType=ActionItems }
      { 21      ;1   ;Action    ;
                      Name=Navigate;
                      CaptionML=[ENU=&Navigate;
                                 PTG=&Navegar];
                      ApplicationArea=#Basic,#Suite;
                      Promoted=Yes;
                      Image=Navigate;
                      PromotedCategory=Process;
                      OnAction=VAR
                                 Option@1110001 : Integer;
                               BEGIN
                                 Navigate.SetDoc("Posting Date","No.");
                                 Navigate.RUN;
                               END;
                                }
      { 1905956704;1 ;Action    ;
                      CaptionML=[ENU=Posted Bill Groups Maturity;
                                 PTG=Vencimento Remessas Registadas];
                      ApplicationArea=#Basic,#Suite;
                      RunObject=Page 31022975;
                      RunPageLink=No.=FIELD(No.),
                                  Global Dimension 1 Filter=FIELD(Global Dimension 1 Filter),
                                  Global Dimension 2 Filter=FIELD(Global Dimension 2 Filter),
                                  Category Filter=FIELD(Category Filter);
                      Promoted=Yes;
                      Image=DocumentsMaturity;
                      PromotedCategory=Process }
      { 31022891;0   ;ActionContainer;
                      ActionContainerType=Reports }
      { 31022892;1   ;Action    ;
                      Name=PostedBillGroupsListing;
                      Ellipsis=Yes;
                      CaptionML=[ENU=Posted Bill Groups Listing;
                                 PTG=Listagem Remessas Registadas];
                      ApplicationArea=#Basic,#Suite;
                      Promoted=Yes;
                      Image=List;
                      PromotedCategory=Report;
                      OnAction=BEGIN
                                 IF FIND THEN BEGIN
                                   PostedBillGr.COPY(Rec);
                                   PostedBillGr.SETRECFILTER;
                                   REPORT.RUN(REPORT::"Posted Bill Group Listing",TRUE,FALSE,PostedBillGr);
                                 END;
                               END;
                                }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                CaptionML=[ENU=General;
                           PTG=Geral] }

    { 2   ;2   ;Field     ;
                SourceExpr="No.";
                Editable=FALSE }

    { 6   ;2   ;Field     ;
                SourceExpr="Bank Account No.";
                Editable=FALSE }

    { 4   ;2   ;Field     ;
                SourceExpr="Bank Account Name";
                Editable=FALSE }

    { 8   ;2   ;Field     ;
                SourceExpr="Dealing Type";
                Importance=Promoted;
                Editable=FALSE }

    { 31022890;2;Field    ;
                ToolTipML=[ENU=Specifies if the posted bill group is a person or company.;
                           ESP=Especifica se a remessa registada � uma pessoa ou empresa.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Partner Type" }

    { 60  ;2   ;Field     ;
                SourceExpr=Factoring;
                Editable=FALSE }

    { 28  ;2   ;Field     ;
                SourceExpr="Posting Date";
                Importance=Promoted;
                Editable=FALSE }

    { 42  ;2   ;Field     ;
                SourceExpr="Currency Code" }

    { 13  ;2   ;Field     ;
                SourceExpr="Amount Grouped" }

    { 30  ;2   ;Field     ;
                SourceExpr="Remaining Amount" }

    { 45  ;2   ;Field     ;
                SourceExpr="Amount Grouped (LCY)";
                Importance=Promoted }

    { 47  ;2   ;Field     ;
                SourceExpr="Remaining Amount (LCY)";
                Importance=Promoted }

    { 32  ;1   ;Part      ;
                Name=Documents;
                CaptionML=[ENU=Docs. in Posted BG;
                           PTG=Documentos Remessa Reg.];
                SubPageView=SORTING(Bill Gr./Pmt. Order No.)
                            WHERE(Type=CONST(Receivable));
                SubPageLink=Bill Gr./Pmt. Order No.=FIELD(No.);
                PagePartID=Page31022950;
                PartType=Page }

    { 1906223801;1;Group  ;
                CaptionML=[ENU=Expenses;
                           PTG=Despesas] }

    { 49  ;2   ;Field     ;
                SourceExpr="Collection Expenses Amt.";
                Editable=FALSE }

    { 52  ;2   ;Field     ;
                SourceExpr="Discount Expenses Amt.";
                Editable=FALSE }

    { 54  ;2   ;Field     ;
                SourceExpr="Discount Interests Amt.";
                Editable=FALSE }

    { 24  ;2   ;Field     ;
                SourceExpr="Rejection Expenses Amt.";
                Editable=FALSE }

    { 26  ;2   ;Field     ;
                SourceExpr="Risked Factoring Exp. Amt.";
                Enabled=TRUE;
                Editable=FALSE }

    { 37  ;2   ;Field     ;
                SourceExpr="Unrisked Factoring Exp. Amt.";
                Editable=FALSE }

    { 1904361101;1;Group  ;
                CaptionML=[ENU=Auditing;
                           PTG=Auditoria] }

    { 10  ;2   ;Field     ;
                SourceExpr="Reason Code";
                Editable=FALSE }

    { 14  ;2   ;Field     ;
                SourceExpr="No. Printed";
                Editable=FALSE }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1907055607;1;Part   ;
                SubPageLink=No.=FIELD(No.);
                PagePartID=Page31023019;
                Visible=TRUE;
                PartType=Page }

    { 1907055707;1;Part   ;
                SubPageLink=No.=FIELD(No.);
                PagePartID=Page31023020;
                Visible=FALSE;
                PartType=Page }

    { 1905767507;1;Part   ;
                Visible=TRUE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {
    VAR
      Navigate@1110001 : Page 344;
      PostedBillGr@1110000 : Record 31022939;

    BEGIN
    END.
  }
}

