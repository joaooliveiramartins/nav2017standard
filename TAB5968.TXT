OBJECT Table 5968 Service Contract Template
{
  OBJECT-PROPERTIES
  {
    Date=07/09/12;
    Time=12:00:00;
    Version List=NAVW17.00;
  }
  PROPERTIES
  {
    DataCaptionFields=No.,Description;
    OnInsert=BEGIN
               ServMgtSetup.GET;
               IF "No." = '' THEN BEGIN
                 ServMgtSetup.TESTFIELD("Contract Template Nos.");
                 NoSeriesMgt.InitSeries(ServMgtSetup."Contract Template Nos.",xRec."No. Series",0D,
                   "No.","No. Series");
               END;
             END;

    OnDelete=BEGIN
               DimMgt.DeleteDefaultDim(DATABASE::"Service Contract Header","No.");
               ServContract.SETCURRENTKEY("Template No.");
               ServContract.SETRANGE("Template No.","No.");
               ServContract.MODIFYALL("Template No.",'');
             END;

    CaptionML=[ENU=Service Contract Template;
               PTG=Modelo Contrato Servi�o];
    LookupPageID=Page6056;
    DrillDownPageID=Page6056;
  }
  FIELDS
  {
    { 1   ;   ;No.                 ;Code20        ;OnValidate=BEGIN
                                                                IF "No." <> xRec."No." THEN BEGIN
                                                                  ServMgtSetup.GET;
                                                                  NoSeriesMgt.TestManual(ServMgtSetup."Contract Template Nos.");
                                                                  "No. Series" := '';
                                                                END;
                                                              END;

                                                   CaptionML=[ENU=No.;
                                                              PTG=N�] }
    { 2   ;   ;Description         ;Text50        ;CaptionML=[ENU=Description;
                                                              PTG=Descri��o] }
    { 3   ;   ;Invoice Period      ;Option        ;CaptionML=[ENU=Invoice Period;
                                                              PTG=Per�odo fatura];
                                                   OptionCaptionML=[ENU=Month,Two Months,Quarter,Half Year,Year,None;
                                                                    PTG=M�s,Dois Meses,Trimestre,Semestre,Ano,Nenhum];
                                                   OptionString=Month,Two Months,Quarter,Half Year,Year,None }
    { 4   ;   ;Max. Labor Unit Price;Decimal      ;CaptionML=[ENU=Max. Labor Unit Price;
                                                              PTG=Pre�o Unit�rio M�x. Trabalho];
                                                   BlankZero=Yes;
                                                   AutoFormatType=2 }
    { 5   ;   ;Combine Invoices    ;Boolean       ;CaptionML=[ENU=Combine Invoices;
                                                              PTG=Juntar Faturas] }
    { 6   ;   ;Prepaid             ;Boolean       ;OnValidate=BEGIN
                                                                IF "Invoice after Service" AND Prepaid THEN
                                                                  ERROR(
                                                                    Text001,
                                                                    FIELDCAPTION("Invoice after Service"),
                                                                    FIELDCAPTION(Prepaid));
                                                              END;

                                                   CaptionML=[ENU=Prepaid;
                                                              PTG=Pr�-Pago] }
    { 7   ;   ;Service Zone Code   ;Code10        ;TableRelation="Service Zone";
                                                   CaptionML=[ENU=Service Zone Code;
                                                              PTG=C�d. Zona Servi�o] }
    { 8   ;   ;Language Code       ;Code10        ;TableRelation=Language;
                                                   CaptionML=[ENU=Language Code;
                                                              PTG=C�d. Idioma] }
    { 9   ;   ;Default Response Time (Hours);Decimal;
                                                   CaptionML=[ENU=Default Response Time (Hours);
                                                              PTG=Tempo Resposta Padr�o (Horas)];
                                                   DecimalPlaces=0:5;
                                                   BlankZero=Yes }
    { 10  ;   ;Contract Lines on Invoice;Boolean  ;CaptionML=[ENU=Contract Lines on Invoice;
                                                              PTG=Linhas Contrato na Fatura] }
    { 11  ;   ;Default Service Period;DateFormula ;CaptionML=[ENU=Default Service Period;
                                                              PTG=Per�odo Servi�o Padr�o] }
    { 14  ;   ;Invoice after Service;Boolean      ;OnValidate=BEGIN
                                                                IF NOT ServHeader.READPERMISSION AND
                                                                   "Invoice after Service" = TRUE
                                                                THEN
                                                                  ERROR(Text000);
                                                                IF "Invoice after Service" AND Prepaid THEN
                                                                  ERROR(
                                                                    Text001,
                                                                    FIELDCAPTION("Invoice after Service"),
                                                                    FIELDCAPTION(Prepaid));
                                                              END;

                                                   CaptionML=[ENU=Invoice after Service;
                                                              PTG=Fatura Ap�s Servi�o] }
    { 15  ;   ;Allow Unbalanced Amounts;Boolean   ;CaptionML=[ENU=Allow Unbalanced Amounts;
                                                              PTG=Permite Valores N�o Balanceados] }
    { 16  ;   ;Contract Group Code ;Code10        ;TableRelation="Contract Group";
                                                   CaptionML=[ENU=Contract Group Code;
                                                              PTG=C�d. Grupo Contrato] }
    { 17  ;   ;Service Order Type  ;Code10        ;TableRelation="Service Order Type";
                                                   CaptionML=[ENU=Service Order Type;
                                                              PTG=Tipo Ordem Servi�o] }
    { 18  ;   ;Automatic Credit Memos;Boolean     ;CaptionML=[ENU=Automatic Credit Memos;
                                                              PTG=Notas Cr�dito Autom�ticas] }
    { 20  ;   ;No. Series          ;Code10        ;TableRelation="No. Series";
                                                   CaptionML=[ENU=No. Series;
                                                              PTG=N� S�ries];
                                                   Editable=No }
    { 21  ;   ;Price Update Period ;DateFormula   ;CaptionML=[ENU=Price Update Period;
                                                              PTG=Per�odo Atualiza��o Pre�o] }
    { 22  ;   ;Price Inv. Increase Code;Code20    ;TableRelation="Standard Text";
                                                   CaptionML=[ENU=Price Inv. Increase Code;
                                                              PTG=C�d. Aumento Pre�o Fatura] }
    { 23  ;   ;Serv. Contract Acc. Gr. Code;Code10;TableRelation="Service Contract Account Group".Code;
                                                   CaptionML=[ENU=Serv. Contract Acc. Gr. Code;
                                                              PTG=C�d. Gr. Conta Contrato Servi�o] }
  }
  KEYS
  {
    {    ;No.                                     ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {
    VAR
      ServHeader@1001 : Record 5900;
      ServContractTmplt@1000 : Record 5968;
      ServContract@1032 : Record 5965;
      ServMgtSetup@1034 : Record 5911;
      NoSeriesMgt@1045 : Codeunit 396;
      DimMgt@1040 : Codeunit 408;
      Text000@1002 : TextConst 'ENU=You cannot checkmark this field because you do not have permissions for the Service Order Management Area.;PTG=N�o pode selecionar este campo porque n�o possui permiss�es para a �rea de Gest�o de Ordens Servi�o.';
      Text001@1003 : TextConst 'ENU=You cannot select both %1 and %2 check boxes.;PTG=N�o pode selecionar simultaneamente as caixas de verifica��o %1 e %2.';

    PROCEDURE AssistEdit@1(OldServContractTmplt@1000 : Record 5968) : Boolean;
    BEGIN
      WITH ServContractTmplt DO BEGIN
        ServContractTmplt := Rec;
        ServMgtSetup.GET;
        ServMgtSetup.TESTFIELD("Contract Template Nos.");
        IF NoSeriesMgt.SelectSeries(ServMgtSetup."Contract Template Nos.",OldServContractTmplt."No. Series","No. Series") THEN BEGIN
          NoSeriesMgt.SetSeries("No.");
          Rec := ServContractTmplt;
          EXIT(TRUE);
        END;
      END;
    END;

    BEGIN
    END.
  }
}

