OBJECT Page 317 VAT Statement
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0,NAVPTSS81.00;
  }
  PROPERTIES
  {
    CaptionML=[ENU=VAT Statement;
               PTG=Declara��o IVA];
    SaveValues=Yes;
    MultipleNewLines=Yes;
    SourceTable=Table256;
    PageType=Worksheet;
    AutoSplitKey=Yes;
    OnOpenPage=VAR
                 StmtSelected@1000 : Boolean;
               BEGIN
                 OpenedFromBatch := ("Statement Name" <> '') AND ("Statement Template Name" = '');
                 IF OpenedFromBatch THEN BEGIN
                   CurrentStmtName := "Statement Name";
                   VATStmtManagement.OpenStmt(CurrentStmtName,Rec);
                   EXIT;
                 END;
                 VATStmtManagement.TemplateSelection(PAGE::"VAT Statement",Rec,StmtSelected);
                 IF NOT StmtSelected THEN
                   ERROR('');
                 VATStmtManagement.OpenStmt(CurrentStmtName,Rec);
               END;

    ActionList=ACTIONS
    {
      { 1900000003;0 ;ActionContainer;
                      ActionContainerType=RelatedInformation }
      { 28      ;1   ;ActionGroup;
                      CaptionML=[ENU=VAT &Statement;
                                 PTG=&Declara��o IVA];
                      Image=Suggest }
      { 37      ;2   ;Action    ;
                      CaptionML=[ENU=P&review;
                                 PTG=Pr�-Visu&alizar];
                      ToolTipML=ENU=Preview the Tax statement.;
                      ApplicationArea=#Basic,#Suite;
                      RunObject=Page 474;
                      RunPageLink=Statement Template Name=FIELD(Statement Template Name),
                                  Name=FIELD(Statement Name);
                      Promoted=Yes;
                      Image=View;
                      PromotedCategory=Process }
      { 1900000006;0 ;ActionContainer;
                      ActionContainerType=Reports }
      { 1902106506;1 ;Action    ;
                      CaptionML=[ENU=VAT Statement;
                                 PTG=Declara��o IVA];
                      ToolTipML=ENU=View a statement of posted Tax and calculates the duty liable to the customs authorities for the selected period.;
                      ApplicationArea=#Basic,#Suite;
                      Promoted=Yes;
                      Image=Report;
                      PromotedCategory=Report;
                      OnAction=VAR
                                 "//--soft-local--//"@1000000003 : Integer;
                                 VATStmtLine@1000000002 : Record 256;
                                 VATStmtName@1000000001 : Record 257;
                                 VATStatementPT@1000000000 : Report 31022959;
                               BEGIN
                                 ReportPrint.PrintVATStmtLine(Rec);
                               END;
                                }
      { 1900000004;0 ;ActionContainer;
                      ActionContainerType=ActionItems }
      { 31      ;1   ;ActionGroup;
                      CaptionML=[ENU=F&unctions;
                                 PTG=F&un��es];
                      Image=Action }
      { 32      ;2   ;Action    ;
                      Name=Print;
                      Ellipsis=Yes;
                      CaptionML=[ENU=Print;
                                 PTG=Imprimir];
                      ToolTipML=ENU=Print the information in the window. A print request window opens where you can specify what to include on the print-out.;
                      ApplicationArea=#Basic,#Suite;
                      Promoted=Yes;
                      Image=Print;
                      PromotedCategory=Process;
                      OnAction=VAR
                                 "//--soft-local--//"@9000002 : Integer;
                                 VATStmtLine@1110101 : Record 256;
                                 VATStmtName@1110002 : Record 257;
                                 VATStatementPT@1000000000 : Report 31022959;
                               BEGIN
                                 //soft,sn
                                 VATStmtName.SETRANGE(VATStmtName.Name,CurrentStmtName);
                                 IF VATStmtName.ISEMPTY THEN
                                   ERROR(Text31022890);

                                 FINDFIRST;
                                 IF VATStmtName."Template Type" = VATStmtName."Template Type"::"One Column Report" THEN
                                   BEGIN
                                     VATStmtLine.SETRANGE("Statement Template Name","Statement Template Name");
                                     VATStmtLine.SETRANGE("Statement Name","Statement Name");
                                     REPORT.RUN(REPORT::"VAT Statement (PT)",TRUE,FALSE,VATStmtLine);
                                 END ELSE
                                 //soft,en
                                   ReportPrint.PrintVATStmtLine(Rec);
                               END;
                                }
      { 33      ;2   ;Action    ;
                      Ellipsis=Yes;
                      CaptionML=[ENU=Calc. and Post VAT Settlement;
                                 PTG=Calcular e Registar Liq. IVA];
                      ToolTipML=ENU=Close open Tax entries and transfers purchase and sales Tax amounts to the Tax settlement account.;
                      RunObject=Report 20;
                      Promoted=Yes;
                      Image=SettleOpenTransactions;
                      PromotedCategory=Process }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 35  ;1   ;Field     ;
                Lookup=Yes;
                CaptionML=[ENU=Name;
                           PTG=Nome];
                ToolTipML=ENU=Specifies the name of the VAT statement.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr=CurrentStmtName;
                OnValidate=BEGIN
                             VATStmtManagement.CheckName(CurrentStmtName,Rec);
                             CurrentStmtNameOnAfterValidate;
                           END;

                OnLookup=BEGIN
                           EXIT(VATStmtManagement.LookupName(GETRANGEMAX("Statement Template Name"),CurrentStmtName,Text));
                         END;
                          }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 2   ;2   ;Field     ;
                ToolTipML=ENU=Specifies a number that identifies this row.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Row No." }

    { 4   ;2   ;Field     ;
                ToolTipML=ENU=Specifies a description of the VAT statement line.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr=Description }

    { 6   ;2   ;Field     ;
                ToolTipML=ENU=Specifies what the VAT statement line will include.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr=Type }

    { 8   ;2   ;Field     ;
                ToolTipML=ENU=Specifies an account interval or a series of account numbers.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Account Totaling";
                OnLookup=VAR
                           GLAccountList@1000 : Page 18;
                         BEGIN
                           GLAccountList.LOOKUPMODE(TRUE);
                           IF NOT (GLAccountList.RUNMODAL = ACTION::LookupOK) THEN
                             EXIT(FALSE);
                           Text := GLAccountList.GetSelectionFilter;
                           EXIT(TRUE);
                         END;
                          }

    { 1110007;2;Field     ;
                SourceExpr="Before Period" }

    { 1110005;2;Field     ;
                SourceExpr="Results type" }

    { 10  ;2   ;Field     ;
                ToolTipML=ENU=Specifies a general posting type that will be used with the VAT statement.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Gen. Posting Type" }

    { 12  ;2   ;Field     ;
                ToolTipML=ENU=Specifies a VAT business posting group code for the VAT statement.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="VAT Bus. Posting Group" }

    { 14  ;2   ;Field     ;
                ToolTipML=ENU=Specifies a VAT product posting group code for the VAT Statement.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="VAT Prod. Posting Group" }

    { 16  ;2   ;Field     ;
                ToolTipML=ENU=Specifies if the VAT statement line shows the VAT amounts, or the base amounts on which the VAT is calculated.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Amount Type" }

    { 18  ;2   ;Field     ;
                ToolTipML=ENU=Specifies a row-number interval or a series of row numbers.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Row Totaling" }

    { 1110010;2;Field     ;
                SourceExpr="VAT Refund Annexes";
                Visible=FALSE }

    { 20  ;2   ;Field     ;
                ToolTipML=ENU=Specifies whether to reverse the sign of VAT entries when it performs calculations.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Calculate with" }

    { 22  ;2   ;Field     ;
                ToolTipML=ENU=Specifies whether the VAT statement line will be printed on the report that contains the finished VAT statement. A check mark in the field means that the line will be printed.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr=Print }

    { 24  ;2   ;Field     ;
                ToolTipML=ENU=Specifies whether amounts on the VAT statement will be printed with their original sign or with the sign reversed.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Print with" }

    { 26  ;2   ;Field     ;
                ToolTipML=ENU=Specifies whether a new page should begin immediately after this line when the VAT statement is printed. To start a new page after this line, place a check mark in the field.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="New Page" }

    { 1110003;2;Field     ;
                SourceExpr=Box }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {
    VAR
      ReportPrint@1000 : Codeunit 228;
      VATStmtManagement@1001 : Codeunit 340;
      CurrentStmtName@1002 : Code[10];
      OpenedFromBatch@1003 : Boolean;
      "//--soft-text--//"@9000001 : TextConst;
      Text31022890@1110100 : TextConst 'ENU=The declaration does not exist;PTG=A declara��o n�o existe';

    LOCAL PROCEDURE CurrentStmtNameOnAfterValidate@19076269();
    BEGIN
      CurrPage.SAVERECORD;
      VATStmtManagement.SetName(CurrentStmtName,Rec);
      CurrPage.UPDATE(FALSE);
    END;

    BEGIN
    END.
  }
}

