OBJECT Table 5063 Interaction Group
{
  OBJECT-PROPERTIES
  {
    Date=07/09/12;
    Time=12:00:00;
    Version List=NAVW17.00;
  }
  PROPERTIES
  {
    DataCaptionFields=Code,Description;
    CaptionML=[ENU=Interaction Group;
               PTG=Grupo Intera��o];
    LookupPageID=Page5074;
  }
  FIELDS
  {
    { 1   ;   ;Code                ;Code10        ;CaptionML=[ENU=Code;
                                                              PTG=C�digo];
                                                   NotBlank=Yes }
    { 2   ;   ;Description         ;Text50        ;CaptionML=[ENU=Description;
                                                              PTG=Descri��o] }
    { 3   ;   ;Date Filter         ;Date          ;FieldClass=FlowFilter;
                                                   CaptionML=[ENU=Date Filter;
                                                              PTG=Filtro Data] }
    { 4   ;   ;No. of Interactions ;Integer       ;FieldClass=FlowField;
                                                   CalcFormula=Count("Interaction Log Entry" WHERE (Interaction Group Code=FIELD(Code),
                                                                                                    Canceled=CONST(No),
                                                                                                    Date=FIELD(Date Filter),
                                                                                                    Postponed=CONST(No)));
                                                   CaptionML=[ENU=No. of Interactions;
                                                              PTG=N� Intera��es];
                                                   Editable=No }
    { 5   ;   ;Cost (LCY)          ;Decimal       ;FieldClass=FlowField;
                                                   CalcFormula=Sum("Interaction Log Entry"."Cost (LCY)" WHERE (Interaction Group Code=FIELD(Code),
                                                                                                               Canceled=CONST(No),
                                                                                                               Date=FIELD(Date Filter),
                                                                                                               Postponed=CONST(No)));
                                                   CaptionML=[ENU=Cost (LCY);
                                                              PTG=Custo (DL)];
                                                   Editable=No;
                                                   AutoFormatType=1 }
    { 6   ;   ;Duration (Min.)     ;Decimal       ;FieldClass=FlowField;
                                                   CalcFormula=Sum("Interaction Log Entry"."Duration (Min.)" WHERE (Interaction Group Code=FIELD(Code),
                                                                                                                    Canceled=CONST(No),
                                                                                                                    Date=FIELD(Date Filter),
                                                                                                                    Postponed=CONST(No)));
                                                   CaptionML=[ENU=Duration (Min.);
                                                              PTG=Dura��o (Min.)];
                                                   DecimalPlaces=0:0;
                                                   Editable=No }
  }
  KEYS
  {
    {    ;Code                                    ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    END.
  }
}

