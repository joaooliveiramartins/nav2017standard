OBJECT Page 31022922 IES Statement
{
  OBJECT-PROPERTIES
  {
    Date=01/02/15;
    Time=13:00:00;
    Version List=NAVPTSS81.00;
  }
  PROPERTIES
  {
    CaptionML=[ENU=IES Statement;
               PTG=Declara��o IES];
    SourceTable=Table31022920;
    PageType=Document;
    OnInit=BEGIN
             InactiveDuringMonthsEditable := TRUE;
             "Activity StopEditable" := TRUE;
             SplitEditable := TRUE;
             FusionEditable := TRUE;
             "Company Situation DateEditable" := TRUE;
             "Company SituationEditable" := TRUE;
             FinYearStmtforTaxStartEditable := TRUE;
             AfterChangeSplTaxPerEditable := TRUE;
             BeforeChangeSplTaxPerEditable := TRUE;
             TerminationPeriodStatementEdit := TRUE;
             ConsolidationStatementEditable := TRUE;
             FirstAnnualDeclarationEditable := TRUE;
             BranchesEditable := TRUE;
             BusinessVolumePercentageEditab := TRUE;
             "CAE Code - REV2Editable" := TRUE;
             "Ending DateEditable" := TRUE;
             "Starting DateEditable" := TRUE;
             "IES Statement NameEditable" := TRUE;
             "No.Editable" := TRUE;
           END;

    OnAfterGetRecord=BEGIN
                       OnAfterGetCurrRecord;
                     END;

    OnNewRecord=BEGIN
                  OnAfterGetCurrRecord;
                END;

    ActionList=ACTIONS
    {
      { 1900000004;0 ;ActionContainer;
                      ActionContainerType=ActionItems }
      { 1110014 ;1   ;ActionGroup;
                      CaptionML=[ENU=F&unctions;
                                 PTG=F&un��es] }
      { 1110016 ;2   ;Action    ;
                      Ellipsis=Yes;
                      CaptionML=[ENU=Suggest Statement Lines;
                                 PTG=Sugerir Linhas Extrato];
                      OnAction=BEGIN
                                 SuggestStLines;
                               END;
                                }
      { 1110017 ;2   ;Action    ;
                      Ellipsis=Yes;
                      CaptionML=[ENU=Export Statement;
                                 PTG=Exportar Extrato];
                      OnAction=BEGIN
                                 ExportStatement;
                               END;
                                }
      { 1110018 ;2   ;Action    ;
                      CaptionML=[ENU=Reopen;
                                 PTG=Reabrir];
                      Image=ReOpen;
                      OnAction=BEGIN
                                 ReopenStatement;
                               END;
                                }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1110000;1;Group     ;
                CaptionML=[ENU=General;
                           PTG=Geral] }

    { 1110001;2;Field     ;
                SourceExpr="No.";
                Editable="No.Editable" }

    { 1110003;2;Field     ;
                SourceExpr="IES Statement Name";
                Editable="IES Statement NameEditable" }

    { 1110007;2;Field     ;
                SourceExpr="Starting Date";
                Editable="Starting DateEditable" }

    { 1110019;2;Field     ;
                SourceExpr="Ending Date";
                Editable="Ending DateEditable" }

    { 1110027;2;Field     ;
                SourceExpr=Year;
                Editable=FALSE }

    { 1110005;2;Field     ;
                SourceExpr=Status;
                Editable=FALSE }

    { 1110021;2;Field     ;
                SourceExpr="CAE Code - REV2";
                Editable="CAE Code - REV2Editable" }

    { 1110023;2;Field     ;
                SourceExpr="Business Volume Percentage";
                Editable=BusinessVolumePercentageEditab }

    { 1110025;2;Field     ;
                SourceExpr=Branches;
                Editable=BranchesEditable }

    { 1110029;2;Field     ;
                SourceExpr="First Annual Declaration";
                Editable=FirstAnnualDeclarationEditable }

    { 1110009;2;Field     ;
                SourceExpr="Date Exported";
                Editable=FALSE }

    { 1110012;1;Part      ;
                Name=IES Subform;
                SubPageLink=Statement No.=FIELD(No.);
                PagePartID=Page31022923 }

    { 1903691201;1;Group  ;
                CaptionML=[ENU=Special Declarations;
                           PTG=Declara��es Especiais] }

    { 1110031;2;Field     ;
                SourceExpr="Consolidation Statement";
                Editable=ConsolidationStatementEditable }

    { 1110043;2;Field     ;
                SourceExpr="Termination Period Statement";
                Editable=TerminationPeriodStatementEdit }

    { 1110045;2;Field     ;
                SourceExpr="Before Change (Spl. Tax. Per.)";
                Editable=BeforeChangeSplTaxPerEditable }

    { 1110047;2;Field     ;
                SourceExpr="After Change (Spl. Tax. Per.)";
                Editable=AfterChangeSplTaxPerEditable }

    { 1110049;2;Field     ;
                SourceExpr="Fin. Year Stmt. for Tax. Start";
                Editable=FinYearStmtforTaxStartEditable }

    { 1903160401;1;Group  ;
                CaptionML=[ENU=Company Situation;
                           PTG=Situa��o Empresa] }

    { 1110055;2;Field     ;
                SourceExpr="Company Situation";
                Editable="Company SituationEditable" }

    { 1110057;2;Field     ;
                SourceExpr="Company Situation Date";
                Editable="Company Situation DateEditable" }

    { 1904978301;1;Group  ;
                CaptionML=[ENU=Important Events;
                           PTG=Eventos Importantes] }

    { 1110059;2;Field     ;
                SourceExpr=Fusion;
                Editable=FusionEditable }

    { 1110061;2;Field     ;
                SourceExpr=Split;
                Editable=SplitEditable }

    { 1110063;2;Field     ;
                SourceExpr="Activity Stop";
                Editable="Activity StopEditable";
                OnValidate=BEGIN
                             ActivityStopOnAfterValidate;
                           END;
                            }

    { 1110065;2;Field     ;
                SourceExpr="Inactive During (Months)";
                Editable=InactiveDuringMonthsEditable }

    { 1110067;2;Field     ;
                CaptionClass=Text19037042;
                MultiLine=Yes }

  }
  CODE
  {
    VAR
      "No.Editable"@19005397 : Boolean INDATASET;
      "IES Statement NameEditable"@19024322 : Boolean INDATASET;
      "Starting DateEditable"@19073118 : Boolean INDATASET;
      "Ending DateEditable"@19073491 : Boolean INDATASET;
      "CAE Code - REV2Editable"@19032629 : Boolean INDATASET;
      BusinessVolumePercentageEditab@19064613 : Boolean INDATASET;
      BranchesEditable@19021219 : Boolean INDATASET;
      FirstAnnualDeclarationEditable@19057668 : Boolean INDATASET;
      ConsolidationStatementEditable@19066675 : Boolean INDATASET;
      TerminationPeriodStatementEdit@19069547 : Boolean INDATASET;
      BeforeChangeSplTaxPerEditable@19075179 : Boolean INDATASET;
      AfterChangeSplTaxPerEditable@19047160 : Boolean INDATASET;
      FinYearStmtforTaxStartEditable@19062722 : Boolean INDATASET;
      "Company SituationEditable"@19019184 : Boolean INDATASET;
      "Company Situation DateEditable"@19036765 : Boolean INDATASET;
      FusionEditable@19074627 : Boolean INDATASET;
      SplitEditable@19050365 : Boolean INDATASET;
      "Activity StopEditable"@19039002 : Boolean INDATASET;
      InactiveDuringMonthsEditable@19005095 : Boolean INDATASET;
      Text19037042@19055797 : TextConst 'ENU=Additional information about important events might be needed to be filled up in the external application before submitting the IES / Annual Declaration.;PTG=Poder� ser necess�rio preencher informa��es adicionais sobre eventos importantes na aplica��o externa antes de submeter a IES/Declara��o Anual.';

    LOCAL PROCEDURE UpdateControls@1110000();
    BEGIN
      CASE Status OF
        Status::New:
          BEGIN
            "No.Editable" := TRUE;
            "IES Statement NameEditable" := TRUE;
            "Starting DateEditable" := TRUE;
            "Ending DateEditable" := TRUE;
            "CAE Code - REV2Editable" := TRUE;
            BusinessVolumePercentageEditab := TRUE;
            BranchesEditable := TRUE;
            FirstAnnualDeclarationEditable := TRUE;
            ConsolidationStatementEditable := TRUE;
            TerminationPeriodStatementEdit := TRUE;
            BeforeChangeSplTaxPerEditable := TRUE;
            AfterChangeSplTaxPerEditable := TRUE;
            FinYearStmtforTaxStartEditable := TRUE;
            "Company SituationEditable" := TRUE;
            "Company Situation DateEditable" := TRUE;
            FusionEditable := TRUE;
            SplitEditable := TRUE;
            "Activity StopEditable" := TRUE;
            InactiveDuringMonthsEditable := "Activity Stop";
          END;
        Status::Suggested:
          BEGIN
            "No.Editable" := TRUE;
            "IES Statement NameEditable" := FALSE;
            "Starting DateEditable" := FALSE;
            "Ending DateEditable" := FALSE;
            "CAE Code - REV2Editable" := TRUE;
            BusinessVolumePercentageEditab := TRUE;
            BranchesEditable := TRUE;
            FirstAnnualDeclarationEditable := TRUE;
            ConsolidationStatementEditable := TRUE;
            TerminationPeriodStatementEdit := TRUE;
            BeforeChangeSplTaxPerEditable := TRUE;
            AfterChangeSplTaxPerEditable := TRUE;
            FinYearStmtforTaxStartEditable := TRUE;
            "Company SituationEditable" := TRUE;
            "Company Situation DateEditable" := TRUE;
            FusionEditable := TRUE;
            SplitEditable := TRUE;
            "Activity StopEditable" := TRUE;
            InactiveDuringMonthsEditable := "Activity Stop";
          END;
        Status::Exported:
          BEGIN
            "No.Editable" := FALSE;
            "IES Statement NameEditable" := FALSE;
            "Starting DateEditable" := FALSE;
            "Ending DateEditable" := FALSE;
            "CAE Code - REV2Editable" := FALSE;
            BusinessVolumePercentageEditab := FALSE;
            BranchesEditable := FALSE;
            FirstAnnualDeclarationEditable := FALSE;
            ConsolidationStatementEditable := FALSE;
            TerminationPeriodStatementEdit := FALSE;
            BeforeChangeSplTaxPerEditable := FALSE;
            AfterChangeSplTaxPerEditable := FALSE;
            FinYearStmtforTaxStartEditable := FALSE;
            "Company SituationEditable" := FALSE;
            "Company Situation DateEditable" := FALSE;
            FusionEditable := FALSE;
            SplitEditable := FALSE;
            "Activity StopEditable" := FALSE;
            InactiveDuringMonthsEditable := FALSE;
          END;
      END;
    END;

    LOCAL PROCEDURE ActivityStopOnAfterValidate@19002326();
    BEGIN
      CurrPage.UPDATE;
    END;

    LOCAL PROCEDURE OnAfterGetCurrRecord@19077479();
    BEGIN
      xRec := Rec;
      UpdateControls;
    END;

    BEGIN
    END.
  }
}

