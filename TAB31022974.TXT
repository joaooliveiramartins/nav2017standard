OBJECT Table 31022974 Series Groups
{
  OBJECT-PROPERTIES
  {
    Date=30/04/16;
    Time=13:00:00;
    Version List=NAVPTSS92.00;
  }
  PROPERTIES
  {
    OnDelete=BEGIN
               //V92.00#00030,sn
               NoSeries.SETRANGE("Series Group", xRec.Code);
               IF NoSeries.FINDSET THEN
                 REPEAT
                   NoSeries."Series Group" := '';
                   NoSeries.MODIFY;
                 UNTIL NoSeries.NEXT = 0;
               //V92.00#00030,en
             END;

    CaptionML=[ENU=Series Groups;
               PTG=Sequ�ncias S�ries];
    LookupPageID=Page31023081;
    DrillDownPageID=Page31023081;
  }
  FIELDS
  {
    { 10  ;   ;Code                ;Code10        ;CaptionML=[ENU=Code;
                                                              PTG=C�digo];
                                                   NotBlank=Yes }
    { 20  ;   ;Type                ;Option        ;OnValidate=BEGIN
                                                                IF Type = Type::Purchase THEN BEGIN
                                                                  Reminder := '';
                                                                  "Issued Reminder" := '';
                                                                  "Finance Charge Memo" := '';
                                                                  "Issued F. Charge Memo" := '';
                                                                 END;
                                                              END;

                                                   CaptionML=[ENU=Type;
                                                              PTG=Tipo];
                                                   OptionCaptionML=[ENU=Sales,Purchase,Services;
                                                                    PTG=Vendas,Compras,Servi�os];
                                                   OptionString=Sales,Purchase,Services }
    { 30  ;   ;Quote               ;Code10        ;TableRelation="No. Series" WHERE (Default Nos.=CONST(Yes));
                                                   OnValidate=BEGIN
                                                                IF xRec.Quote <> Quote THEN
                                                                  UpdateGrNoSeries(xRec.Quote,Quote,Code);
                                                              END;

                                                   CaptionML=[ENU=Quote;
                                                              PTG=Proposta] }
    { 40  ;   ;Order               ;Code10        ;TableRelation="No. Series" WHERE (Default Nos.=CONST(Yes));
                                                   OnValidate=BEGIN
                                                                IF xRec.Order <> Order THEN
                                                                  UpdateGrNoSeries(xRec.Order,Order,Code);
                                                              END;

                                                   CaptionML=[ENU=Order;
                                                              PTG=Encomenda] }
    { 50  ;   ;Invoice             ;Code10        ;TableRelation="No. Series" WHERE (Default Nos.=CONST(Yes));
                                                   OnValidate=BEGIN
                                                                IF xRec.Invoice <> Invoice THEN
                                                                  UpdateGrNoSeries(xRec.Invoice,Invoice,Code);
                                                              END;

                                                   CaptionML=[ENU=Invoice;
                                                              PTG=Factura] }
    { 60  ;   ;Posted Invoice      ;Code10        ;TableRelation="No. Series" WHERE (Default Nos.=CONST(Yes));
                                                   CaptionML=[ENU=Posted Invoice;
                                                              PTG=Factura Registada] }
    { 70  ;   ;Credit Memo         ;Code10        ;TableRelation="No. Series" WHERE (Default Nos.=CONST(Yes));
                                                   OnValidate=BEGIN
                                                                IF xRec."Credit Memo" <> "Credit Memo" THEN
                                                                  UpdateGrNoSeries(xRec."Credit Memo","Credit Memo",Code);
                                                              END;

                                                   CaptionML=[ENU=Credit Memo;
                                                              PTG=Nota Cr�dito] }
    { 80  ;   ;Posted Credit Memo  ;Code10        ;TableRelation="No. Series" WHERE (Default Nos.=CONST(Yes));
                                                   CaptionML=[ENU=Posted Credit Memo;
                                                              PTG=Nota Cr�dito Reg.] }
    { 90  ;   ;Return              ;Code10        ;TableRelation="No. Series" WHERE (Default Nos.=CONST(Yes));
                                                   OnValidate=BEGIN
                                                                IF xRec.Return <> Return THEN
                                                                  UpdateGrNoSeries(xRec.Return,Return,Code);
                                                              END;

                                                   CaptionML=[ENU=Return;
                                                              PTG=Devolu��o] }
    { 100 ;   ;Shipment/Receipts   ;Code10        ;TableRelation="No. Series" WHERE (Default Nos.=CONST(Yes));
                                                   CaptionML=[ENU=Shipment/Receipts;
                                                              PTG=Guias de Remessa] }
    { 110 ;   ;Reminder            ;Code10        ;TableRelation="No. Series" WHERE (Default Nos.=CONST(Yes));
                                                   OnValidate=BEGIN
                                                                TESTFIELD(Type,Type::Sales);
                                                                IF xRec.Reminder <> Reminder THEN
                                                                  UpdateGrNoSeries(xRec.Reminder,Reminder,Code);
                                                              END;

                                                   CaptionML=[ENU=Reminder;
                                                              PTG=Carta de Aviso] }
    { 120 ;   ;Issued Reminder     ;Code10        ;TableRelation="No. Series" WHERE (Default Nos.=CONST(Yes));
                                                   OnValidate=BEGIN
                                                                TESTFIELD(Type,Type::Sales);
                                                              END;

                                                   CaptionML=[ENU=Issued Reminder;
                                                              PTG=Carta de Aviso Emit.] }
    { 130 ;   ;Finance Charge Memo ;Code10        ;TableRelation="No. Series" WHERE (Default Nos.=CONST(Yes));
                                                   OnValidate=BEGIN
                                                                TESTFIELD(Type,Type::Sales);
                                                                IF xRec."Finance Charge Memo" <> "Finance Charge Memo" THEN
                                                                  UpdateGrNoSeries(xRec."Finance Charge Memo","Finance Charge Memo",Code);
                                                              END;

                                                   CaptionML=[ENU=Finance Charge Memo;
                                                              PTG=Nota de Juros] }
    { 140 ;   ;Issued F. Charge Memo;Code10       ;TableRelation="No. Series" WHERE (Default Nos.=CONST(Yes));
                                                   OnValidate=BEGIN
                                                                TESTFIELD(Type,Type::Sales);
                                                              END;

                                                   CaptionML=[ENU=Issue F. Charge Memo;
                                                              PTG=Nota de Juros Emitida] }
    { 150 ;   ;Blanket Order       ;Code10        ;TableRelation="No. Series" WHERE (Default Nos.=CONST(Yes));
                                                   OnValidate=BEGIN
                                                                IF xRec."Blanket Order" <> "Blanket Order" THEN
                                                                  UpdateGrNoSeries(xRec."Blanket Order","Blanket Order",Code);
                                                              END;

                                                   CaptionML=[ENU=Blanket Order;
                                                              PTG=Encomenda Aberta] }
    { 160 ;   ;Posted Ret. Receipts/Ship.;Code10  ;TableRelation="No. Series" WHERE (Default Nos.=CONST(Yes));
                                                   OnValidate=BEGIN
                                                                //V92.00#00021,so
                                                                //IF xRec."Posted Ret. Receipts/Ship." <> "Posted Ret. Receipts/Ship." THEN
                                                                //  UpdateGrNoSeries(xRec."Posted Ret. Receipts/Ship.","Posted Ret. Receipts/Ship.",Code);
                                                                //V92.00#00021,eo
                                                              END;

                                                   CaptionML=[ENU=Posted Ret. Receipts/Ship.;
                                                              PTG=Rec./Env. Devolu��es Reg.] }
    { 170 ;   ;Customer            ;Code10        ;TableRelation="No. Series" WHERE (Default Nos.=CONST(Yes));
                                                   CaptionML=[ENU=Customer;
                                                              PTG=Cliente] }
    { 180 ;   ;Vendor              ;Code10        ;TableRelation="No. Series" WHERE (Default Nos.=CONST(Yes));
                                                   CaptionML=[ENU=Vendor;
                                                              PTG=Fornecedor] }
    { 31022890;;Shipment           ;Code10        ;TableRelation="No. Series" WHERE (Default Nos.=CONST(Yes));
                                                   OnValidate=BEGIN
                                                                //V92.00#00021,sn
                                                                IF xRec.Shipment <> Shipment THEN
                                                                  UpdateGrNoSeries(xRec.Shipment,Shipment,Code);
                                                                //V92.00#00021,en
                                                              END;

                                                   CaptionML=[ENU=Shipment;
                                                              PTG=Envio] }
    { 31022891;;Posted Shipment    ;Code10        ;TableRelation="No. Series" WHERE (Default Nos.=CONST(Yes));
                                                   CaptionML=[ENU=Posted Shipment;
                                                              PTG=Envio Registado] }
    { 31022892;;Receipt            ;Code10        ;TableRelation="No. Series" WHERE (Default Nos.=CONST(Yes));
                                                   OnValidate=BEGIN
                                                                //V92.00#00021,sn
                                                                IF xRec.Receipt <> Receipt THEN
                                                                  UpdateGrNoSeries(xRec.Receipt,Receipt,Code);
                                                                //V92.00#00021,en
                                                              END;

                                                   CaptionML=[ENU=Receipt;
                                                              PTG=Recep��o] }
    { 31022893;;Posted Receipt     ;Code10        ;TableRelation="No. Series" WHERE (Default Nos.=CONST(Yes));
                                                   CaptionML=[ENU=Posted Receipt;
                                                              PTG=Recep��o Registada] }
    { 31022894;;Put-away           ;Code10        ;TableRelation="No. Series" WHERE (Default Nos.=CONST(Yes));
                                                   OnValidate=BEGIN
                                                                //V92.00#00021,sn
                                                                IF xRec."Put-away" <> "Put-away" THEN
                                                                  UpdateGrNoSeries(xRec."Put-away", "Put-away",Code);
                                                                //V92.00#00021,en
                                                              END;

                                                   CaptionML=[ENU=Put-away;
                                                              PTG=Arruma��o] }
    { 31022895;;Register Put-away  ;Code10        ;TableRelation="No. Series" WHERE (Default Nos.=CONST(Yes));
                                                   CaptionML=[ENU=Register Put-away;
                                                              PTG=Arruma��o Registada] }
    { 31022896;;Pick               ;Code10        ;TableRelation="No. Series" WHERE (Default Nos.=CONST(Yes));
                                                   OnValidate=BEGIN
                                                                //V92.00#00021,sn
                                                                IF xRec.Pick <> Pick THEN
                                                                  UpdateGrNoSeries(xRec.Pick,Pick,Code);
                                                                //V92.00#00021,en
                                                              END;

                                                   CaptionML=[ENU=Pick;
                                                              PTG=Recolha] }
    { 31022897;;Register Pick      ;Code10        ;TableRelation="No. Series" WHERE (Default Nos.=CONST(Yes));
                                                   CaptionML=[ENU=Register Pick;
                                                              PTG=Recolha Registada] }
    { 31022898;;Transfer           ;Code10        ;TableRelation="No. Series" WHERE (Default Nos.=CONST(Yes));
                                                   OnValidate=BEGIN
                                                                //V92.00#00021,sn
                                                                IF xRec.Transfer <> Transfer THEN
                                                                  UpdateGrNoSeries(xRec.Transfer,Transfer,Code);
                                                                //V92.00#00021,en
                                                              END;

                                                   CaptionML=[ENU=Transfer;
                                                              PTG=Transfer�ncia] }
    { 31022899;;Ship. Transfer     ;Code10        ;TableRelation="No. Series" WHERE (Default Nos.=CONST(Yes));
                                                   OnValidate=BEGIN
                                                                //V92.00#00021,sn
                                                                IF xRec."Ship. Transfer" <> "Ship. Transfer" THEN
                                                                  UpdateGrNoSeries(xRec."Ship. Transfer","Ship. Transfer",Code);
                                                                //V92.00#00021,en
                                                              END;

                                                   CaptionML=[ENU=Ship. Transfer;
                                                              PTG=Transfer�ncia Envio] }
    { 31022900;;Receipt Transfer   ;Code10        ;TableRelation="No. Series" WHERE (Default Nos.=CONST(Yes));
                                                   OnValidate=BEGIN
                                                                //V92.00#00021,sn
                                                                IF xRec."Receipt Transfer" <> "Receipt Transfer" THEN
                                                                  UpdateGrNoSeries(xRec."Receipt Transfer","Receipt Transfer",Code);
                                                                //V92.00#00021,en
                                                              END;

                                                   CaptionML=[ENU=Receipt Transfer;
                                                              PTG=Transfer�ncia Recep��o] }
    { 31022901;;Contract           ;Code10        ;TableRelation="No. Series" WHERE (Default Nos.=CONST(Yes));
                                                   CaptionML=[ENU=Contract;
                                                              PTG=Contrato] }
    { 31022902;;Service Quote      ;Code10        ;TableRelation="No. Series" WHERE (Default Nos.=CONST(Yes));
                                                   OnValidate=BEGIN
                                                                //V92.00#00021,sn
                                                                IF xRec."Service Quote" <> "Service Quote" THEN
                                                                  UpdateGrNoSeries(xRec."Service Quote","Service Quote",Code);
                                                                //V92.00#00021,en
                                                              END;

                                                   CaptionML=[ENU=Service Quote;
                                                              PTG=Proposta Servi�o] }
    { 31022903;;Service Order      ;Code10        ;TableRelation="No. Series" WHERE (Default Nos.=CONST(Yes));
                                                   OnValidate=BEGIN
                                                                //V92.00#00021,sn
                                                                IF xRec."Service Order" <> "Service Order" THEN
                                                                  UpdateGrNoSeries(xRec."Service Order","Service Order",Code);
                                                                //V92.00#00021,en
                                                              END;

                                                   CaptionML=[ENU=Service Order;
                                                              PTG=Ordem Servi�o] }
    { 31022904;;Delivery Order     ;Code10        ;TableRelation="No. Series" WHERE (Default Nos.=CONST(Yes));
                                                   OnValidate=BEGIN
                                                                //V92.00#00021,sn
                                                                IF xRec."Delivery Order" <> "Delivery Order" THEN
                                                                  UpdateGrNoSeries(xRec."Delivery Order","Delivery Order",Code);
                                                                //V92.00#00021,en
                                                              END;

                                                   CaptionML=[ENU=Delivery Order;
                                                              PTG=Ordem Entrega] }
    { 31022905;;Posted Delivery Order;Code10      ;TableRelation="No. Series" WHERE (Default Nos.=CONST(Yes));
                                                   CaptionML=[ENU=Posted Delivery Order;
                                                              PTG=Ordem Entrega Registada] }
    { 31022906;;Receipt Order      ;Code10        ;TableRelation="No. Series" WHERE (Default Nos.=CONST(Yes));
                                                   OnValidate=BEGIN
                                                                //V92.00#00021,sn
                                                                IF xRec."Receipt Order" <> "Receipt Order" THEN
                                                                  UpdateGrNoSeries(xRec."Receipt Order","Receipt Order",Code);
                                                                //V92.00#00021,en
                                                              END;

                                                   CaptionML=[ENU=Receipt Order;
                                                              PTG=Ordem Recep��o] }
    { 31022907;;Posted Receipt Order;Code10       ;TableRelation="No. Series" WHERE (Default Nos.=CONST(Yes));
                                                   CaptionML=[ENU=Posted Receipt Order;
                                                              PTG=Ordem Recep��o Registada] }
    { 31022908;;Debit Memo         ;Code10        ;TableRelation="No. Series" WHERE (Default Nos.=CONST(Yes));
                                                   OnValidate=BEGIN
                                                                //V92.00#00021,sn
                                                                IF xRec."Debit Memo" <> "Debit Memo" THEN
                                                                  UpdateGrNoSeries(xRec."Debit Memo","Debit Memo",Code);
                                                                //V92.00#00021,en
                                                              END;

                                                   CaptionML=[ENU=Debit Memo;
                                                              PTG=Nota D�bito] }
    { 31022909;;Posted Debit Memo  ;Code10        ;TableRelation="No. Series" WHERE (Default Nos.=CONST(Yes));
                                                   CaptionML=[ENU=Posted Debit Memo;
                                                              PTG=Nota D�bito Registada] }
  }
  KEYS
  {
    {    ;Code                                    ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {
    VAR
      NoSeries@1000 : Record 308;
      Text31022890@1001 : TextConst 'ENU=Can''t associate Cod. Series ''%1'', because the Cod. Series ''%1'' is already being used in ''%2'' Series Group.;PTG=N�o � poss�vel associar Cod. S�ries ''%1'', porque o Cod. S�ries ''%1'' j� est� a ser usado na Sequ�ncia de S�ries ''%2''.';

    PROCEDURE UpdateGrNoSeries@1000000002(xOldNoSeries@1000000000 : Code[10];xNewNoSeries@1000000001 : Code[10];xSeriesGroup@1000000002 : Code[10]);
    VAR
      xSeries@1000000003 : Record 308;
    BEGIN
      //V92.00#00039,so
      ////V92.00#00021,sn
      //IF NoSeries.GET(xNewNoSeries) AND (NoSeries."Series Group" <> Code) AND  (NoSeries."Series Group" <> '') THEN
      //  ERROR(Text31022890, xNewNoSeries, NoSeries."Series Group");
      ////V92.00#00021,en
      //V92.00#00039,eo

      IF NoSeries.GET(xOldNoSeries) AND (NoSeries."Series Group" = Code) THEN BEGIN //V92.00#00039,n
        IF xOldNoSeries <> '' THEN BEGIN
          xSeries.GET(xOldNoSeries);
          xSeries."Series Group" := '';
          xSeries.MODIFY;
        END;
      END ELSE  //V92.00#00039,n
        IF xNewNoSeries <> '' THEN BEGIN
          xSeries.GET(xNewNoSeries);
          xSeries."Series Group" := xSeriesGroup;
          xSeries.MODIFY;
        END;
    END;

    BEGIN
    {
      V92.00#00014 - Sequ�ncia de S�ries - Bug na cria��o de c�digos - EFD - 2016.02.17
      V92.00#00021 - Integridade entre Sequ�ncia de S�ries e tabela de S�ries - EFD - 2016.02.26
      V92.00#00030 - Erro ao eliminar C�digo de S�rie - EFD - 2016.03.11
      V92.00#00039 - Sequ�ncia S�ries Recolhas Invent�rio - EFD - 2016.06.01
    }
    END.
  }
}

