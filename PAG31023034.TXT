OBJECT Page 31023034 Bank Cat.Post.Rec.Bills Matrix
{
  OBJECT-PROPERTIES
  {
    Date=01/02/15;
    Time=13:00:00;
    Version List=NAVPTSS81.00;
  }
  PROPERTIES
  {
    Editable=No;
    CaptionML=[ENU=Bank Cat.Post.Rec.Bills Matrix;
               PTG=Matriz T�tulos � Cobran�a por Banco];
    SourceTable=Table270;
    PageType=List;
    OnAfterGetRecord=VAR
                       MATRIX_CurrentColumnOrdinal@1110001 : Integer;
                       MATRIX_NoOfColumns@1110000 : Integer;
                     BEGIN
                       MATRIX_CurrentColumnOrdinal := 1;
                       MATRIX_NoOfColumns := ARRAYLEN(MATRIX_CellData);

                       WHILE (MATRIX_CurrentColumnOrdinal <= MATRIX_NoOfColumns) DO BEGIN
                         MATRIX_OnAfterGetRecord(MATRIX_CurrentColumnOrdinal);
                         MATRIX_CurrentColumnOrdinal := MATRIX_CurrentColumnOrdinal + 1;
                       END;
                     END;

  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 6   ;2   ;Field     ;
                ApplicationArea=#All;
                SourceExpr="No." }

    { 4   ;2   ;Field     ;
                ApplicationArea=#All;
                SourceExpr=Name }

    { 1012;2   ;Field     ;
                Name=Field1;
                ApplicationArea=#All;
                SourceExpr=MATRIX_CellData[1];
                CaptionClass='3,' + MATRIX_ColumnCaption[1];
                OnDrillDown=BEGIN
                              MATRIX_OnDrillDown(1);
                            END;
                             }

    { 1013;2   ;Field     ;
                Name=Field2;
                ApplicationArea=#All;
                SourceExpr=MATRIX_CellData[2];
                CaptionClass='3,' + MATRIX_ColumnCaption[2];
                OnDrillDown=BEGIN
                              MATRIX_OnDrillDown(2);
                            END;
                             }

    { 1014;2   ;Field     ;
                Name=Field3;
                ApplicationArea=#All;
                SourceExpr=MATRIX_CellData[3];
                CaptionClass='3,' + MATRIX_ColumnCaption[3];
                OnDrillDown=BEGIN
                              MATRIX_OnDrillDown(3);
                            END;
                             }

    { 1015;2   ;Field     ;
                Name=Field4;
                ApplicationArea=#All;
                SourceExpr=MATRIX_CellData[4];
                CaptionClass='3,' + MATRIX_ColumnCaption[4];
                OnDrillDown=BEGIN
                              MATRIX_OnDrillDown(4);
                            END;
                             }

    { 1016;2   ;Field     ;
                Name=Field5;
                ApplicationArea=#All;
                SourceExpr=MATRIX_CellData[5];
                CaptionClass='3,' + MATRIX_ColumnCaption[5];
                OnDrillDown=BEGIN
                              MATRIX_OnDrillDown(5);
                            END;
                             }

    { 1017;2   ;Field     ;
                Name=Field6;
                ApplicationArea=#All;
                SourceExpr=MATRIX_CellData[6];
                CaptionClass='3,' + MATRIX_ColumnCaption[6];
                OnDrillDown=BEGIN
                              MATRIX_OnDrillDown(6);
                            END;
                             }

    { 1018;2   ;Field     ;
                Name=Field7;
                ApplicationArea=#All;
                SourceExpr=MATRIX_CellData[7];
                CaptionClass='3,' + MATRIX_ColumnCaption[7];
                OnDrillDown=BEGIN
                              MATRIX_OnDrillDown(7);
                            END;
                             }

    { 1019;2   ;Field     ;
                Name=Field8;
                ApplicationArea=#All;
                SourceExpr=MATRIX_CellData[8];
                CaptionClass='3,' + MATRIX_ColumnCaption[8];
                OnDrillDown=BEGIN
                              MATRIX_OnDrillDown(8);
                            END;
                             }

    { 1020;2   ;Field     ;
                Name=Field9;
                ApplicationArea=#All;
                SourceExpr=MATRIX_CellData[9];
                CaptionClass='3,' + MATRIX_ColumnCaption[9];
                OnDrillDown=BEGIN
                              MATRIX_OnDrillDown(9);
                            END;
                             }

    { 1021;2   ;Field     ;
                Name=Field10;
                ApplicationArea=#All;
                SourceExpr=MATRIX_CellData[10];
                CaptionClass='3,' + MATRIX_ColumnCaption[10];
                OnDrillDown=BEGIN
                              MATRIX_OnDrillDown(10);
                            END;
                             }

    { 1022;2   ;Field     ;
                Name=Field11;
                ApplicationArea=#All;
                SourceExpr=MATRIX_CellData[11];
                CaptionClass='3,' + MATRIX_ColumnCaption[11];
                OnDrillDown=BEGIN
                              MATRIX_OnDrillDown(11);
                            END;
                             }

    { 1023;2   ;Field     ;
                Name=Field12;
                ApplicationArea=#All;
                SourceExpr=MATRIX_CellData[12];
                CaptionClass='3,' + MATRIX_ColumnCaption[12];
                OnDrillDown=BEGIN
                              MATRIX_OnDrillDown(12);
                            END;
                             }

    { 1024;2   ;Field     ;
                Name=Field13;
                ApplicationArea=#All;
                SourceExpr=MATRIX_CellData[13];
                CaptionClass='3,' + MATRIX_ColumnCaption[13];
                OnDrillDown=BEGIN
                              MATRIX_OnDrillDown(13);
                            END;
                             }

    { 1025;2   ;Field     ;
                Name=Field14;
                ApplicationArea=#All;
                SourceExpr=MATRIX_CellData[14];
                CaptionClass='3,' + MATRIX_ColumnCaption[14];
                OnDrillDown=BEGIN
                              MATRIX_OnDrillDown(14);
                            END;
                             }

    { 1026;2   ;Field     ;
                Name=Field15;
                ApplicationArea=#All;
                SourceExpr=MATRIX_CellData[15];
                CaptionClass='3,' + MATRIX_ColumnCaption[15];
                OnDrillDown=BEGIN
                              MATRIX_OnDrillDown(15);
                            END;
                             }

    { 1027;2   ;Field     ;
                Name=Field16;
                ApplicationArea=#All;
                SourceExpr=MATRIX_CellData[16];
                CaptionClass='3,' + MATRIX_ColumnCaption[16];
                OnDrillDown=BEGIN
                              MATRIX_OnDrillDown(16);
                            END;
                             }

    { 1028;2   ;Field     ;
                Name=Field17;
                ApplicationArea=#All;
                SourceExpr=MATRIX_CellData[17];
                CaptionClass='3,' + MATRIX_ColumnCaption[17];
                OnDrillDown=BEGIN
                              MATRIX_OnDrillDown(17);
                            END;
                             }

    { 1029;2   ;Field     ;
                Name=Field18;
                ApplicationArea=#All;
                SourceExpr=MATRIX_CellData[18];
                CaptionClass='3,' + MATRIX_ColumnCaption[18];
                OnDrillDown=BEGIN
                              MATRIX_OnDrillDown(18);
                            END;
                             }

    { 1030;2   ;Field     ;
                Name=Field19;
                ApplicationArea=#All;
                SourceExpr=MATRIX_CellData[19];
                CaptionClass='3,' + MATRIX_ColumnCaption[19];
                OnDrillDown=BEGIN
                              MATRIX_OnDrillDown(19);
                            END;
                             }

    { 1031;2   ;Field     ;
                Name=Field20;
                ApplicationArea=#All;
                SourceExpr=MATRIX_CellData[20];
                CaptionClass='3,' + MATRIX_ColumnCaption[20];
                OnDrillDown=BEGIN
                              MATRIX_OnDrillDown(20);
                            END;
                             }

    { 1032;2   ;Field     ;
                Name=Field21;
                ApplicationArea=#All;
                SourceExpr=MATRIX_CellData[21];
                CaptionClass='3,' + MATRIX_ColumnCaption[21];
                OnDrillDown=BEGIN
                              MATRIX_OnDrillDown(21);
                            END;
                             }

    { 1033;2   ;Field     ;
                Name=Field22;
                ApplicationArea=#All;
                SourceExpr=MATRIX_CellData[22];
                CaptionClass='3,' + MATRIX_ColumnCaption[22];
                OnDrillDown=BEGIN
                              MATRIX_OnDrillDown(22);
                            END;
                             }

    { 1034;2   ;Field     ;
                Name=Field23;
                ApplicationArea=#All;
                SourceExpr=MATRIX_CellData[23];
                CaptionClass='3,' + MATRIX_ColumnCaption[23];
                OnDrillDown=BEGIN
                              MATRIX_OnDrillDown(23);
                            END;
                             }

    { 1035;2   ;Field     ;
                Name=Field24;
                ApplicationArea=#All;
                SourceExpr=MATRIX_CellData[24];
                CaptionClass='3,' + MATRIX_ColumnCaption[24];
                OnDrillDown=BEGIN
                              MATRIX_OnDrillDown(24);
                            END;
                             }

    { 1036;2   ;Field     ;
                Name=Field25;
                ApplicationArea=#All;
                SourceExpr=MATRIX_CellData[25];
                CaptionClass='3,' + MATRIX_ColumnCaption[25];
                OnDrillDown=BEGIN
                              MATRIX_OnDrillDown(25);
                            END;
                             }

    { 1037;2   ;Field     ;
                Name=Field26;
                ApplicationArea=#All;
                SourceExpr=MATRIX_CellData[26];
                CaptionClass='3,' + MATRIX_ColumnCaption[26];
                OnDrillDown=BEGIN
                              MATRIX_OnDrillDown(26);
                            END;
                             }

    { 1038;2   ;Field     ;
                Name=Field27;
                ApplicationArea=#All;
                SourceExpr=MATRIX_CellData[27];
                CaptionClass='3,' + MATRIX_ColumnCaption[27];
                OnDrillDown=BEGIN
                              MATRIX_OnDrillDown(27);
                            END;
                             }

    { 1039;2   ;Field     ;
                Name=Field28;
                ApplicationArea=#All;
                SourceExpr=MATRIX_CellData[28];
                CaptionClass='3,' + MATRIX_ColumnCaption[28];
                OnDrillDown=BEGIN
                              MATRIX_OnDrillDown(28);
                            END;
                             }

    { 1040;2   ;Field     ;
                Name=Field29;
                ApplicationArea=#All;
                SourceExpr=MATRIX_CellData[29];
                CaptionClass='3,' + MATRIX_ColumnCaption[29];
                OnDrillDown=BEGIN
                              MATRIX_OnDrillDown(29);
                            END;
                             }

    { 1041;2   ;Field     ;
                Name=Field30;
                ApplicationArea=#All;
                SourceExpr=MATRIX_CellData[30];
                CaptionClass='3,' + MATRIX_ColumnCaption[30];
                OnDrillDown=BEGIN
                              MATRIX_OnDrillDown(30);
                            END;
                             }

    { 1042;2   ;Field     ;
                Name=Field31;
                ApplicationArea=#All;
                SourceExpr=MATRIX_CellData[31];
                CaptionClass='3,' + MATRIX_ColumnCaption[31];
                OnDrillDown=BEGIN
                              MATRIX_OnDrillDown(31);
                            END;
                             }

    { 1043;2   ;Field     ;
                Name=Field32;
                ApplicationArea=#All;
                SourceExpr=MATRIX_CellData[32];
                CaptionClass='3,' + MATRIX_ColumnCaption[32];
                OnDrillDown=BEGIN
                              MATRIX_OnDrillDown(32);
                            END;
                             }

  }
  CODE
  {
    VAR
      AccountingPeriod@1110000 : Record 50;
      PostedDoc@1110001 : Record 31022936;
      MatrixRecords@1110002 : ARRAY [32] OF Record 2000000007;
      PeriodFormManagement@1110004 : Codeunit 359;
      PeriodType@1110005 : 'Day,Week,Month,Quarter,Year,Accounting Period';
      AmountType@1110006 : 'Net Change,Balance at Date';
      StatusFilterOption@1110007 : 'Open,Honored,Rejected,All';
      CalcAmt@1110008 : Decimal;
      MATRIX_CellData@1110009 : ARRAY [32] OF Decimal;
      CategoryFilter@1110010 : Code[250];
      MATRIX_ColumnCaption@1110011 : ARRAY [32] OF Text[1024];
      DueDateFilter@1110012 : Date;

    LOCAL PROCEDURE SetDateFilter@1110000(ColumnID@1110000 : Integer);
    BEGIN
      IF CategoryFilter = '' THEN
        SETRANGE("Category Filter")
      ELSE
        SETFILTER("Category Filter",CategoryFilter);
      IF StatusFilterOption = StatusFilterOption::All THEN
        SETFILTER("Status Filter",'%1|%2|%3',
        StatusFilterOption::Open,
        StatusFilterOption::Honored,
        StatusFilterOption::Rejected)
      ELSE
        SETRANGE("Status Filter",StatusFilterOption);

      IF MatrixRecords[ColumnID]."Period Start" = MatrixRecords[ColumnID]."Period End" THEN
        SETRANGE("Due Date Filter",MatrixRecords[ColumnID]."Period Start")
      ELSE
        SETRANGE("Due Date Filter",MatrixRecords[ColumnID]."Period Start",MatrixRecords[ColumnID]."Period End")
    END;

    PROCEDURE Load@1110001(VAR MatrixColumns1@1110001 : ARRAY [32] OF Text[1024];VAR MatrixRecords1@1110002 : ARRAY [32] OF Record 2000000007;CategoryFilter1@1110003 : Code[20];StatusFilter@1110004 : Text[30]);
    BEGIN
      COPYARRAY(MATRIX_ColumnCaption,MatrixColumns1,1);
      COPYARRAY(MatrixRecords,MatrixRecords1,1);
      CategoryFilter := CategoryFilter1;
      IF EVALUATE(StatusFilterOption, StatusFilter) THEN;
    END;

    LOCAL PROCEDURE MATRIX_OnDrillDown@1110002(ColumnID@1110001 : Integer);
    VAR
      PostedCarteraDoc@1110002 : Record 31022936;
      PostedBills@1110003 : Page 31023005;
    BEGIN
      SetDateFilter(ColumnID);
      PostedCarteraDoc.SETRANGE("Document Type",PostedCarteraDoc."Document Type"::Bill);
      PostedCarteraDoc.SETRANGE(Type,PostedCarteraDoc.Type::Receivable);

      IF MatrixRecords[ColumnID]."Period Start" = MatrixRecords[ColumnID]."Period End" THEN
        PostedCarteraDoc.SETRANGE("Due Date",MatrixRecords[ColumnID]."Period Start")
      ELSE
        PostedCarteraDoc.SETRANGE("Due Date",MatrixRecords[ColumnID]."Period Start",MatrixRecords[ColumnID]."Period End");

      PostedCarteraDoc.SETFILTER("Category Code",CategoryFilter);
      PostedCarteraDoc.SETRANGE(Status,StatusFilterOption);
      PostedBills.SETTABLEVIEW(PostedCarteraDoc);
      PostedBills.RUNMODAL;
    END;

    LOCAL PROCEDURE MATRIX_OnAfterGetRecord@1110003(ColumnID@1110001 : Integer);
    BEGIN
      SetDateFilter(ColumnID);
      SETFILTER("Category Filter",CategoryFilter);
      SETRANGE("Status Filter",StatusFilterOption);
      CALCFIELDS("Posted Receiv Bills Amt. (LCY)");
      CalcAmt := "Posted Receiv Bills Amt. (LCY)";
      MATRIX_CellData[ColumnID] := "Posted Receiv Bills Amt. (LCY)";
    END;

    BEGIN
    END.
  }
}

