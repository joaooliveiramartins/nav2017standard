OBJECT Page 529 Posted Purchase Invoice Lines
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0,NAVPTSS83.00;
  }
  PROPERTIES
  {
    Editable=No;
    CaptionML=[ENU=Posted Purchase Invoice Lines;
               PTG=Linhas Faturas Compra Registadas];
    SourceTable=Table123;
    PageType=List;
    ActionList=ACTIONS
    {
      { 1900000003;0 ;ActionContainer;
                      ActionContainerType=RelatedInformation }
      { 71      ;1   ;ActionGroup;
                      CaptionML=[ENU=&Line;
                                 PTG=&Linha];
                      Image=Line }
      { 72      ;2   ;Action    ;
                      ShortCutKey=Shift+F7;
                      CaptionML=[ENU=Show Document;
                                 PTG=Mostrar Documento];
                      ToolTipML=ENU=Open the document that the selected line exists on.;
                      ApplicationArea=#Basic,#Suite;
                      Image=View;
                      OnAction=BEGIN
                                 PurchInvHeader.GET("Document No.");
                                 //soft,sn
                                 IF PurchInvHeader."Debit Memo" THEN
                                   PAGE.RUN(PAGE::"Posted Purchase Debit Memo",PurchInvHeader)
                                 ELSE
                                 //soft,en
                                  PAGE.RUN(PAGE::"Posted Purchase Invoice",PurchInvHeader);
                               END;
                                }
      { 73      ;2   ;Action    ;
                      AccessByPermission=TableData 348=R;
                      ShortCutKey=Shift+Ctrl+D;
                      CaptionML=[ENU=Dimensions;
                                 PTG=Dimens�es];
                      ToolTipML=ENU=View or edits dimensions, such as area, project, or department, that you can assign to sales and purchase documents to distribute costs and analyze transaction history.;
                      ApplicationArea=#Suite;
                      Image=Dimensions;
                      OnAction=BEGIN
                                 ShowDimensions;
                               END;
                                }
      { 6500    ;2   ;Action    ;
                      ShortCutKey=Shift+Ctrl+I;
                      CaptionML=[ENU=Item &Tracking Lines;
                                 PTG=Linhas Ras&treio Produto];
                      ToolTipML=ENU=View or edit serial numbers and lot numbers that are assigned to the item on the document or journal line.;
                      Image=ItemTrackingLines;
                      OnAction=BEGIN
                                 ShowItemTrackingLines;
                               END;
                                }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 2   ;2   ;Field     ;
                ToolTipML=ENU=Specifies the document number.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Document No." }

    { 4   ;2   ;Field     ;
                ToolTipML=ENU=Specifies the number of the vendor that you bought the items on the invoice from.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Buy-from Vendor No." }

    { 6   ;2   ;Field     ;
                ToolTipML=ENU=Specifies the line type.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr=Type }

    { 8   ;2   ;Field     ;
                ToolTipML=ENU=Specifies an item number that identifies the account number that identifies the general ledger account used when posting the line.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="No." }

    { 10  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the variant code for the item.;
                SourceExpr="Variant Code" }

    { 12  ;2   ;Field     ;
                ToolTipML=ENU=Specifies either the name of, or a description of, the item or general ledger account.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr=Description }

    { 14  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the code for the dimension value associated with the invoice.;
                SourceExpr="Shortcut Dimension 1 Code" }

    { 16  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the code for the dimension value associated with the invoice.;
                SourceExpr="Shortcut Dimension 2 Code" }

    { 20  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the FA posting type of the purchase invoice line.;
                SourceExpr="FA Posting Type" }

    { 22  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the depreciation book code on the purchase invoice line.;
                SourceExpr="Depreciation Book Code" }

    { 24  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the quantity posted from the line.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr=Quantity }

    { 26  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the unit of measure code for the item.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Unit of Measure Code" }

    { 28  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the unit of measure for the item (one bottle or one piece, for example).;
                SourceExpr="Unit of Measure" }

    { 30  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the direct unit cost of one unit of the item.;
                SourceExpr="Direct Unit Cost" }

    { 32  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the item's indirect cost, as a percentage.;
                SourceExpr="Indirect Cost %" }

    { 34  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the cost per unit.;
                SourceExpr="Unit Cost (LCY)" }

    { 36  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the price, in LCY, for one unit of the item.;
                SourceExpr="Unit Price (LCY)" }

    { 38  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the line's net amount.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr=Amount }

    { 40  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the total, in the currency of the invoice, of the amount on the invoice line, including VAT.;
                SourceExpr="Amount Including VAT" }

    { 42  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the line discount % granted on items on each individual line.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Line Discount %" }

    { 44  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the discount amount.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Line Discount Amount" }

    { 46  ;2   ;Field     ;
                ToolTipML=ENU=Specifies whether the invoice line could have been included in an invoice discount calculation.;
                SourceExpr="Allow Invoice Disc." }

    { 48  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the invoice discount amount for the line.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Inv. Discount Amount" }

    { 54  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the number of a particular item entry to which the invoice line was applied when it was posted.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Appl.-to Item Entry" }

    { 56  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the number of the job that the purchase invoice line is linked to.;
                SourceExpr="Job No." }

    { 58  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the insurance number on the purchase invoice line.;
                SourceExpr="Insurance No." }

    { 60  ;2   ;Field     ;
                ToolTipML=ENU=Specifies whether depreciation was calculated until the FA posting date of the line. If the field contains a Yes or a check mark, depreciation was posted for the asset for the period from the FA posting date of the previous FA ledger entry to the FA posting date of this purchase line.;
                SourceExpr="Depr. until FA Posting Date" }

    { 62  ;2   ;Field     ;
                ToolTipML=ENU=Specifies whether, when this line was posted, the additional acquisition cost posted on the line was depreciated in proportion to the amount by which the fixed asset had already been depreciated.;
                SourceExpr="Depr. Acquisition Cost" }

    { 64  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the budgeted FA number on the purchase invoice line.;
                SourceExpr="Budgeted FA No." }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {
    VAR
      PurchInvHeader@1000 : Record 122;

    BEGIN
    END.
  }
}

