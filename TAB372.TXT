OBJECT Table 372 Payment Buffer
{
  OBJECT-PROPERTIES
  {
    Date=30/04/16;
    Time=13:00:00;
    Version List=NAVW17.10,NAVPTSS92.00;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Payment Buffer;
               PTG=Mem. Int. Pagamento];
  }
  FIELDS
  {
    { 1   ;   ;Vendor No.          ;Code20        ;TableRelation=Vendor;
                                                   CaptionML=[ENU=Vendor No.;
                                                              PTG=N� Fornecedor] }
    { 2   ;   ;Currency Code       ;Code10        ;TableRelation=Currency;
                                                   CaptionML=[ENU=Currency Code;
                                                              PTG=C�d. Divisa] }
    { 3   ;   ;Vendor Ledg. Entry No.;Integer     ;TableRelation="Vendor Ledger Entry";
                                                   CaptionML=[ENU=Vendor Ledg. Entry No.;
                                                              PTG=N� mov. fornecedor] }
    { 4   ;   ;Dimension Entry No. ;Integer       ;CaptionML=[ENU=Dimension Entry No.;
                                                              PTG=N� Mov. Dimens�o] }
    { 5   ;   ;Global Dimension 1 Code;Code20     ;TableRelation="Dimension Value".Code WHERE (Global Dimension No.=CONST(1));
                                                   CaptionML=[ENU=Global Dimension 1 Code;
                                                              PTG=C�d. Dimens�o 1 Global];
                                                   CaptionClass='1,1,1' }
    { 6   ;   ;Global Dimension 2 Code;Code20     ;TableRelation="Dimension Value".Code WHERE (Global Dimension No.=CONST(2));
                                                   CaptionML=[ENU=Global Dimension 2 Code;
                                                              PTG=C�d. Dimens�o 2 Global];
                                                   CaptionClass='1,1,2' }
    { 7   ;   ;Document No.        ;Code20        ;CaptionML=[ENU=Document No.;
                                                              PTG=N� Documento] }
    { 8   ;   ;Amount              ;Decimal       ;CaptionML=[ENU=Amount;
                                                              PTG=Valor];
                                                   AutoFormatType=1 }
    { 9   ;   ;Vendor Ledg. Entry Doc. Type;Option;CaptionML=[ENU=Vendor Ledg. Entry Doc. Type;
                                                              PTG=Tipo Documento Mov. Fornecedor];
                                                   OptionCaptionML=[ENU=" ,Payment,Invoice,Credit Memo,Finance Charge Memo,Reminder,Refund,,,,,,Bill";
                                                                    PTG=" ,Pagamento,Fatura,Nota Cr�dito,Nota Juros,Carta Aviso,Reembolso,,,,,,T�tulo"];
                                                   OptionString=[ ,Payment,Invoice,Credit Memo,Finance Charge Memo,Reminder,Refund,,,,,,Bill];
                                                   Description=soft }
    { 10  ;   ;Vendor Ledg. Entry Doc. No.;Code20 ;CaptionML=[ENU=Vendor Ledg. Entry Doc. No.;
                                                              PTG=N� Documento Mov. Fornecedor] }
    { 170 ;   ;Creditor No.        ;Code20        ;TableRelation="Vendor Ledger Entry"."Creditor No." WHERE (Entry No.=FIELD(Vendor Ledg. Entry No.));
                                                   CaptionML=[ENU=Creditor No.;
                                                              PTG=N� Credor];
                                                   Numeric=Yes }
    { 171 ;   ;Payment Reference   ;Code50        ;TableRelation="Vendor Ledger Entry"."Payment Reference" WHERE (Entry No.=FIELD(Vendor Ledg. Entry No.));
                                                   CaptionML=[ENU=Payment Reference;
                                                              PTG=Refer�ncia Pagamento];
                                                   Numeric=Yes }
    { 172 ;   ;Payment Method Code ;Code10        ;TableRelation="Vendor Ledger Entry"."Payment Method Code" WHERE (Vendor No.=FIELD(Vendor No.));
                                                   CaptionML=[ENU=Payment Method Code;
                                                              PTG=C�d. Forma Pagamento] }
    { 173 ;   ;Applies-to Ext. Doc. No.;Code35    ;CaptionML=[ENU=Applies-to Ext. Doc. No.;
                                                              PTG=Liq. por N� Doc. Ext.] }
    { 290 ;   ;Exported to Payment File;Boolean   ;CaptionML=[ENU=Exported to Payment File;
                                                              PTG=Ficheiro Exporta��o Pagamento];
                                                   Editable=No }
    { 480 ;   ;Dimension Set ID    ;Integer       ;TableRelation="Dimension Set Entry";
                                                   CaptionML=[ENU=Dimension Set ID;
                                                              PTG=ID Combina��o Dimens�o];
                                                   Editable=No }
    { 31022890;;Vendor Posting Group;Code10       ;CaptionML=[ENU=Vendor Posting Group;
                                                              PTG=Gr. Contabil�stico Fornecedor];
                                                   Description=V92.00#00036 }
  }
  KEYS
  {
    {    ;Vendor No.,Currency Code,Vendor Ledg. Entry No.,Dimension Entry No.,Vendor Posting Group;
                                                   Clustered=Yes }
    {    ;Document No.                             }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    {
      V92.00#00036 - NF: Permitir Alteracoes de Grupos Cont. (correcoes) -  - 2018.01.01
    }
    END.
  }
}

