OBJECT Table 367 Dimension Code Buffer
{
  OBJECT-PROPERTIES
  {
    Date=05/08/11;
    Time=12:00:00;
    Version List=NAVW16.00;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Dimension Code Buffer;
               PTG=Mem. Int. C�d. Dimens�o];
  }
  FIELDS
  {
    { 1   ;   ;Code                ;Code20        ;CaptionML=[ENU=Code;
                                                              PTG=C�digo] }
    { 2   ;   ;Name                ;Text50        ;CaptionML=[ENU=Name;
                                                              PTG=Nome] }
    { 3   ;   ;Totaling            ;Text250       ;CaptionML=[ENU=Totaling;
                                                              PTG=Somat�rio] }
    { 4   ;   ;Period Start        ;Date          ;CaptionML=[ENU=Period Start;
                                                              PTG=In�cio Per�odo] }
    { 5   ;   ;Period End          ;Date          ;CaptionML=[ENU=Period End;
                                                              PTG=Per�odo Final] }
    { 6   ;   ;Visible             ;Boolean       ;InitValue=Yes;
                                                   CaptionML=[ENU=Visible;
                                                              PTG=Vis�vel] }
    { 7   ;   ;Indentation         ;Integer       ;CaptionML=[ENU=Indentation;
                                                              PTG=Indentar] }
    { 8   ;   ;Show in Bold        ;Boolean       ;CaptionML=[ENU=Show in Bold;
                                                              PTG=Mostrar em Negrito] }
    { 9   ;   ;Amount              ;Decimal       ;FieldClass=FlowField;
                                                   CalcFormula=Sum("Analysis View Entry".Amount WHERE (Analysis View Code=CONST(''),
                                                                                                       Dimension 1 Value Code=FIELD(Dimension 1 Value Filter),
                                                                                                       Dimension 2 Value Code=FIELD(Dimension 2 Value Filter),
                                                                                                       Dimension 3 Value Code=FIELD(Dimension 3 Value Filter),
                                                                                                       Dimension 4 Value Code=FIELD(Dimension 4 Value Filter)));
                                                   CaptionML=[ENU=Amount;
                                                              PTG=Valor];
                                                   AutoFormatType=1 }
    { 10  ;   ;Dimension 1 Value Filter;Code20    ;FieldClass=FlowFilter;
                                                   CaptionML=[ENU=Dimension 1 Value Filter;
                                                              PTG=Filtro Valor Dimens�o 1] }
    { 11  ;   ;Dimension 2 Value Filter;Code20    ;FieldClass=FlowFilter;
                                                   CaptionML=[ENU=Dimension 2 Value Filter;
                                                              PTG=Filtro Valor Dimens�o 2] }
    { 12  ;   ;Dimension 3 Value Filter;Code20    ;FieldClass=FlowFilter;
                                                   CaptionML=[ENU=Dimension 3 Value Filter;
                                                              PTG=Filtro C�d. Valor Dimens�o 3] }
    { 13  ;   ;Dimension 4 Value Filter;Code20    ;FieldClass=FlowFilter;
                                                   CaptionML=[ENU=Dimension 4 Value Filter;
                                                              PTG=Filtro C�d. Valor Dimens�o 4] }
    { 7101;   ;Quantity            ;Decimal       ;FieldClass=FlowField;
                                                   CalcFormula=Sum("Analysis View Entry".Amount WHERE (Analysis View Code=CONST(''),
                                                                                                       Dimension 1 Value Code=FIELD(Dimension 1 Value Filter),
                                                                                                       Dimension 2 Value Code=FIELD(Dimension 2 Value Filter),
                                                                                                       Dimension 3 Value Code=FIELD(Dimension 3 Value Filter),
                                                                                                       Dimension 4 Value Code=FIELD(Dimension 4 Value Filter)));
                                                   CaptionML=[ENU=Quantity;
                                                              PTG=Quantidade];
                                                   DecimalPlaces=0:5 }
  }
  KEYS
  {
    {    ;Code                                    ;Clustered=Yes }
    {    ;Period Start                             }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    END.
  }
}

