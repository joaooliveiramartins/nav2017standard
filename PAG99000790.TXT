OBJECT Page 99000790 Family
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Family;
               PTG=Fam�lia];
    SourceTable=Table99000773;
    PageType=ListPlus;
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                CaptionML=[ENU=General;
                           PTG=Geral] }

    { 2   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the production family number.;
                           PTG=""];
                SourceExpr="No." }

    { 4   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies a description for a product family.;
                           PTG=""];
                SourceExpr=Description }

    { 6   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies an additional description of the product family if there is not enough space in the Description field.;
                           PTG=""];
                SourceExpr="Description 2" }

    { 8   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies a search name.;
                           PTG=""];
                SourceExpr="Search Name" }

    { 10  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the number of the routing which is used for the production of the family.;
                           PTG=""];
                SourceExpr="Routing No." }

    { 17  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies whether the family is blocked.;
                           PTG=""];
                SourceExpr=Blocked }

    { 19  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies when the standard data of this production family was last modified.;
                           PTG=""];
                SourceExpr="Last Date Modified" }

    { 13  ;1   ;Part      ;
                SubPageView=SORTING(Family No.,Line No.);
                SubPageLink=Family No.=FIELD(No.);
                PagePartID=Page99000792 }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {

    BEGIN
    END.
  }
}

