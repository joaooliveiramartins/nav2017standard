OBJECT Table 5600 Fixed Asset
{
  OBJECT-PROPERTIES
  {
    Date=15/06/17;
    Time=14:25:40;
    Modified=Yes;
    Version List=NAVW110.0.00.15601,NAVPTSS94.00;
  }
  PROPERTIES
  {
    Permissions=TableData 5629=r;
    DataCaptionFields=No.,Description;
    OnInsert=BEGIN
               IF "No." = '' THEN BEGIN
                 FASetup.GET;
                 FASetup.TESTFIELD("Fixed Asset Nos.");
                 NoSeriesMgt.InitSeries(FASetup."Fixed Asset Nos.",xRec."No. Series",0D,"No.","No. Series");
               END;

               "Main Asset/Component" := "Main Asset/Component"::" ";
               "Component of Main Asset" := '';

               DimMgt.UpdateDefaultDim(
                 DATABASE::"Fixed Asset","No.",
                 "Global Dimension 1 Code","Global Dimension 2 Code");
             END;

    OnModify=BEGIN
               "Last Date Modified" := TODAY;
             END;

    OnDelete=VAR
               FADeprBook@1000 : Record 5612;
             BEGIN
               LOCKTABLE;
               MainAssetComp.LOCKTABLE;
               InsCoverageLedgEntry.LOCKTABLE;
               IF "Main Asset/Component" = "Main Asset/Component"::"Main Asset" THEN
                 ERROR(Text000);
               FAMoveEntries.MoveFAInsuranceEntries("No.");
               FADeprBook.SETRANGE("FA No.","No.");
               FADeprBook.DELETEALL(TRUE);
               IF NOT FADeprBook.ISEMPTY THEN
                 ERROR(Text001,TABLECAPTION,"No.");

               MainAssetComp.SETCURRENTKEY("FA No.");
               MainAssetComp.SETRANGE("FA No.","No.");
               MainAssetComp.DELETEALL;
               IF "Main Asset/Component" = "Main Asset/Component"::Component THEN BEGIN
                 MainAssetComp.RESET;
                 MainAssetComp.SETRANGE("Main Asset No.","Component of Main Asset");
                 MainAssetComp.SETRANGE("FA No.",'');
                 MainAssetComp.DELETEALL;
                 MainAssetComp.SETRANGE("FA No.");
                 IF NOT MainAssetComp.FINDFIRST THEN BEGIN
                   FA.GET("Component of Main Asset");
                   FA."Main Asset/Component" := FA."Main Asset/Component"::" ";
                   FA."Component of Main Asset" := '';
                   FA.MODIFY;
                 END;
               END;

               MaintenanceRegistration.SETRANGE("FA No.","No.");
               MaintenanceRegistration.DELETEALL;

               CommentLine.SETRANGE("Table Name",CommentLine."Table Name"::"Fixed Asset");
               CommentLine.SETRANGE("No.","No.");
               CommentLine.DELETEALL;

               DimMgt.DeleteDefaultDim(DATABASE::"Fixed Asset","No.");
             END;

    OnRename=VAR
               SalesLine@1000 : Record 37;
               PurchLine@1001 : Record 39;
             BEGIN
               SalesLine.RenameNo(SalesLine.Type::"Fixed Asset",xRec."No.","No.");
               PurchLine.RenameNo(PurchLine.Type::"Fixed Asset",xRec."No.","No.");
               "Last Date Modified" := TODAY;
             END;

    CaptionML=[ENU=Fixed Asset;
               PTG=Imobilizado];
    LookupPageID=Page5601;
    DrillDownPageID=Page5601;
  }
  FIELDS
  {
    { 1   ;   ;No.                 ;Code20        ;AltSearchField=Search Description;
                                                   OnValidate=BEGIN
                                                                IF "No." <> xRec."No." THEN BEGIN
                                                                  FASetup.GET;
                                                                  NoSeriesMgt.TestManual(FASetup."Fixed Asset Nos.");
                                                                  "No. Series" := '';
                                                                END;
                                                              END;

                                                   CaptionML=[ENU=No.;
                                                              PTG=N�] }
    { 2   ;   ;Description         ;Text50        ;OnValidate=VAR
                                                                FADeprBook@1000 : Record 5612;
                                                              BEGIN
                                                                IF ("Search Description" = UPPERCASE(xRec.Description)) OR ("Search Description" = '') THEN
                                                                  "Search Description" := Description;
                                                                IF Description <> xRec.Description THEN BEGIN
                                                                  FADeprBook.SETCURRENTKEY("FA No.");
                                                                  FADeprBook.SETRANGE("FA No.","No.");
                                                                  FADeprBook.MODIFYALL(Description,Description);
                                                                END;
                                                              END;

                                                   CaptionML=[ENU=Description;
                                                              PTG=Descri��o] }
    { 3   ;   ;Search Description  ;Code50        ;CaptionML=[ENU=Search Description;
                                                              PTG=Alias Descri��o] }
    { 4   ;   ;Description 2       ;Text50        ;CaptionML=[ENU=Description 2;
                                                              PTG=Descri��o 2] }
    { 5   ;   ;FA Class Code       ;Code10        ;TableRelation="FA Class";
                                                   OnValidate=VAR
                                                                FASubclass@1000 : Record 5608;
                                                              BEGIN
                                                                IF "FA Subclass Code" = '' THEN
                                                                  EXIT;

                                                                FASubclass.GET("FA Subclass Code");
                                                                IF NOT (FASubclass."FA Class Code" IN ['',"FA Class Code"]) THEN
                                                                  "FA Subclass Code" := '';
                                                              END;

                                                   CaptionML=[ENU=FA Class Code;
                                                              PTG=C�d. Classe Imob.] }
    { 6   ;   ;FA Subclass Code    ;Code10        ;TableRelation="FA Subclass";
                                                   OnValidate=VAR
                                                                FASubclass@1000 : Record 5608;
                                                              BEGIN
                                                                FASubclass.GET("FA Subclass Code");
                                                                IF "FA Class Code" <> '' THEN BEGIN
                                                                  IF FASubclass."FA Class Code" IN ['',"FA Class Code"] THEN
                                                                    EXIT;

                                                                  ERROR(UnexpctedSubclassErr);
                                                                END;

                                                                VALIDATE("FA Class Code",FASubclass."FA Class Code");
                                                              END;

                                                   CaptionML=[ENU=FA Subclass Code;
                                                              PTG=C�d. Subclasse Imob.] }
    { 7   ;   ;Global Dimension 1 Code;Code20     ;TableRelation="Dimension Value".Code WHERE (Global Dimension No.=CONST(1));
                                                   OnValidate=BEGIN
                                                                ValidateShortcutDimCode(1,"Global Dimension 1 Code");
                                                              END;

                                                   CaptionML=[ENU=Global Dimension 1 Code;
                                                              PTG=C�d. Dimens�o 1 Global];
                                                   CaptionClass='1,1,1' }
    { 8   ;   ;Global Dimension 2 Code;Code20     ;TableRelation="Dimension Value".Code WHERE (Global Dimension No.=CONST(2));
                                                   OnValidate=BEGIN
                                                                ValidateShortcutDimCode(2,"Global Dimension 2 Code");
                                                              END;

                                                   CaptionML=[ENU=Global Dimension 2 Code;
                                                              PTG=C�d. Dimens�o 2 Global];
                                                   CaptionClass='1,1,2' }
    { 9   ;   ;Location Code       ;Code10        ;TableRelation=Location WHERE (Use As In-Transit=CONST(No));
                                                   CaptionML=[ENU=Location Code;
                                                              PTG=C�d. Localiza��o] }
    { 10  ;   ;FA Location Code    ;Code10        ;TableRelation="FA Location";
                                                   CaptionML=[ENU=FA Location Code;
                                                              PTG=C�d. Localiza��o Imob.] }
    { 11  ;   ;Vendor No.          ;Code20        ;TableRelation=Vendor;
                                                   CaptionML=[ENU=Vendor No.;
                                                              PTG=N� Fornecedor] }
    { 12  ;   ;Main Asset/Component;Option        ;CaptionML=[ENU=Main Asset/Component;
                                                              PTG=Principal/Componente];
                                                   OptionCaptionML=[ENU=" ,Main Asset,Component";
                                                                    PTG=" ,Principal,Componente"];
                                                   OptionString=[ ,Main Asset,Component];
                                                   Editable=No }
    { 13  ;   ;Component of Main Asset;Code20     ;TableRelation="Fixed Asset";
                                                   CaptionML=[ENU=Component of Main Asset;
                                                              PTG=Componente Imob. Principal];
                                                   Editable=No }
    { 14  ;   ;Budgeted Asset      ;Boolean       ;OnValidate=BEGIN
                                                                FAMoveEntries.ChangeBudget(Rec);
                                                              END;

                                                   CaptionML=[ENU=Budgeted Asset;
                                                              PTG=Imobilizado Or�amentado] }
    { 15  ;   ;Warranty Date       ;Date          ;CaptionML=[ENU=Warranty Date;
                                                              PTG=Data Garantia] }
    { 16  ;   ;Responsible Employee;Code20        ;TableRelation=Employee;
                                                   CaptionML=[ENU=Responsible Employee;
                                                              PTG=Empregado Respons�vel] }
    { 17  ;   ;Serial No.          ;Text30        ;CaptionML=[ENU=Serial No.;
                                                              PTG=N� S�rie] }
    { 18  ;   ;Last Date Modified  ;Date          ;CaptionML=[ENU=Last Date Modified;
                                                              PTG=Data �ltima Modif.];
                                                   Editable=No }
    { 19  ;   ;Insured             ;Boolean       ;FieldClass=FlowField;
                                                   CalcFormula=Exist("Ins. Coverage Ledger Entry" WHERE (FA No.=FIELD(No.),
                                                                                                         Disposed FA=CONST(No)));
                                                   CaptionML=[ENU=Insured;
                                                              PTG=Segurado];
                                                   Editable=No }
    { 20  ;   ;Comment             ;Boolean       ;FieldClass=FlowField;
                                                   CalcFormula=Exist("Comment Line" WHERE (Table Name=CONST(Fixed Asset),
                                                                                           No.=FIELD(No.)));
                                                   CaptionML=[ENU=Comment;
                                                              PTG=Coment�rio];
                                                   Editable=No }
    { 21  ;   ;Blocked             ;Boolean       ;CaptionML=[ENU=Blocked;
                                                              PTG=Bloqueado] }
    { 22  ;   ;Picture             ;BLOB          ;CaptionML=[ENU=Picture;
                                                              PTG=Imagem];
                                                   SubType=Bitmap }
    { 23  ;   ;Maintenance Vendor No.;Code20      ;TableRelation=Vendor;
                                                   CaptionML=[ENU=Maintenance Vendor No.;
                                                              PTG=N� Fornecedor Manuten��o] }
    { 24  ;   ;Under Maintenance   ;Boolean       ;CaptionML=[ENU=Under Maintenance;
                                                              PTG=Sob manuten��o] }
    { 25  ;   ;Next Service Date   ;Date          ;CaptionML=[ENU=Next Service Date;
                                                              PTG=Pr�xima Data Servi�o] }
    { 26  ;   ;Inactive            ;Boolean       ;InitValue=No;
                                                   CaptionML=[ENU=Inactive;
                                                              PTG=Inativo];
                                                   Description=soft }
    { 27  ;   ;FA Posting Date Filter;Date        ;FieldClass=FlowFilter;
                                                   CaptionML=[ENU=FA Posting Date Filter;
                                                              PTG=Filtro Data Registo Imob.] }
    { 28  ;   ;No. Series          ;Code10        ;TableRelation="No. Series";
                                                   CaptionML=[ENU=No. Series;
                                                              PTG=N� S�ries];
                                                   Editable=No }
    { 29  ;   ;FA Posting Group    ;Code10        ;TableRelation="FA Posting Group";
                                                   CaptionML=[ENU=FA Posting Group;
                                                              PTG=Gr. Contabil�stico Imob.] }
    { 30  ;   ;Acquired            ;Boolean       ;FieldClass=FlowField;
                                                   CalcFormula=Exist("FA Depreciation Book" WHERE (FA No.=FIELD(No.),
                                                                                                   Acquisition Date=FILTER(<>'')));
                                                   CaptionML=[ENU=Acquired;
                                                              PTG=Adquirido] }
    { 140 ;   ;Image               ;Media         ;CaptionML=[ENU=Image;
                                                              PTG=Imagem] }
    { 50000;  ;DRF Code            ;Code10        ;TableRelation="DRF Codes"."DRF Code";
                                                   CaptionML=[ENU=Depreciation DRF Code;
                                                              PTG=C�d. DRF Amortiza��es] }
    { 50001;  ;Diminished Value Asset;Boolean     ;CaptionML=[ENU=Diminished Value Asset;
                                                              PTG=Bem Reduzido Valor] }
    { 50500;  ;Asset Code          ;Text30        ;CaptionML=PTG=N�mero de Invent�rio }
    { 50501;  ;Qty.                ;Decimal       ;CaptionML=PTG=Quantidade }
    { 50502;  ;Material            ;Text30        ;CaptionML=PTG=Material }
    { 50503;  ;Color               ;Text30        ;CaptionML=PTG=C�r }
    { 50504;  ;Dimensions          ;Text30        ;CaptionML=PTG=Dimens�es }
    { 50505;  ;Other Characteristics;Text100      ;CaptionML=PTG=Outras Caracter�sticas }
    { 50506;  ;Brand               ;Text50        ;CaptionML=PTG=Marca }
    { 50507;  ;Model               ;Text50        ;CaptionML=PTG=Modelo }
    { 50508;  ;Label Position      ;Text30        ;CaptionML=PTG=Posi��o da Etiqueta }
    { 70001;  ;General Text1       ;Text30         }
    { 70002;  ;General Text2       ;Text30         }
    { 70003;  ;General Text3       ;Text50         }
    { 70004;  ;General Text4       ;Text50         }
    { 70005;  ;General Text5       ;Text250        }
    { 70011;  ;General Date1       ;Date           }
    { 70012;  ;General Date2       ;Date           }
    { 70013;  ;General Date3       ;Date           }
    { 70021;  ;General Int1        ;Integer        }
    { 70022;  ;General Int2        ;Integer        }
    { 70023;  ;General Int3        ;Integer        }
    { 70031;  ;General Bool1       ;Boolean        }
    { 70032;  ;General Bool2       ;Boolean        }
    { 70033;  ;General Bool3       ;Boolean        }
    { 70041;  ;General Dec1        ;Decimal        }
    { 70042;  ;General Dec2        ;Decimal        }
    { 70051;  ;General Code1       ;Code20         }
    { 70052;  ;General Code2       ;Code20         }
    { 70053;  ;General Code3       ;Code20         }
    { 31022890;;Asset Status       ;Option        ;InitValue=New;
                                                   CaptionML=[ENU=Asset Status;
                                                              PTG=Estado Imobilizado];
                                                   OptionCaptionML=[ENU=New,Used,Leased,In Progress;
                                                                    PTG=Novo,Usado,Leased,Em Curso];
                                                   OptionString=New,Used,Leased,In Progress;
                                                   Description=soft }
    { 31022891;;Asset Type         ;Option        ;InitValue=Tangible;
                                                   CaptionML=[ENU=Asset Type;
                                                              PTG=Classes Imobilizado];
                                                   OptionCaptionML=[ENU=Tangible,Intangible,Finance,Not Consumable Biological Asset,Capital Asset;
                                                                    PTG=Ativo Fixo Tang�vel,Ativo Intang�vel,Propriedades Investimento,Ativo Biol�gico N�o Consum�vel,Partes de Capital];
                                                   OptionString=Tangible,Intangible,Finance,Not Consumable Biological Asset,Capital Asset;
                                                   Description=soft }
  }
  KEYS
  {
    {    ;No.                                     ;Clustered=Yes }
    {    ;Search Description                       }
    {    ;FA Class Code                            }
    {    ;FA Subclass Code                         }
    {    ;Component of Main Asset,Main Asset/Component }
    {    ;FA Location Code                         }
    {    ;Global Dimension 1 Code                  }
    {    ;Global Dimension 2 Code                  }
    {    ;FA Posting Group                         }
    {    ;Description                              }
    {    ;Asset Type                               }
  }
  FIELDGROUPS
  {
    { 1   ;DropDown            ;No.,Description,FA Class Code            }
    { 2   ;Brick               ;No.,Description,FA Class Code,Image      }
  }
  CODE
  {
    VAR
      Text000@1000 : TextConst 'ENU=A main asset cannot be deleted.;PTG=N�o pode eliminar um imobilizado principal.';
      Text001@1001 : TextConst 'ENU=You cannot delete %1 %2 because it has associated depreciation books.;PTG=N�o pode eliminar %1 %2 j� que t�m associados livros de amortiza��o.';
      CommentLine@1002 : Record 97;
      FA@1003 : Record 5600;
      FASetup@1004 : Record 5603;
      MaintenanceRegistration@1005 : Record 5616;
      MainAssetComp@1007 : Record 5640;
      InsCoverageLedgEntry@1008 : Record 5629;
      FAMoveEntries@1009 : Codeunit 5623;
      NoSeriesMgt@1010 : Codeunit 396;
      DimMgt@1011 : Codeunit 408;
      UnexpctedSubclassErr@1006 : TextConst 'ENU=This fixed asset subclass belongs to a different fixed asset class.;PTG=Esta subclasse de imobilizado pertence a outra classe de imobilizado.';
      "//--soft-global--//"@9000000 : Integer;
      FALedgerEntry@1000000002 : Record 5601;

    PROCEDURE AssistEdit@2(OldFA@1000 : Record 5600) : Boolean;
    BEGIN
      WITH FA DO BEGIN
        FA := Rec;
        FASetup.GET;
        FASetup.TESTFIELD("Fixed Asset Nos.");
        IF NoSeriesMgt.SelectSeries(FASetup."Fixed Asset Nos.",OldFA."No. Series","No. Series") THEN BEGIN
          NoSeriesMgt.SetSeries("No.");
          Rec := FA;
          EXIT(TRUE);
        END;
      END;
    END;

    LOCAL PROCEDURE ValidateShortcutDimCode@29(FieldNumber@1000 : Integer;VAR ShortcutDimCode@1001 : Code[20]);
    BEGIN
      DimMgt.ValidateDimValueCode(FieldNumber,ShortcutDimCode);
      DimMgt.SaveDefaultDim(DATABASE::"Fixed Asset","No.",FieldNumber,ShortcutDimCode);
      MODIFY(TRUE);
    END;

    PROCEDURE FieldsForAcquitionInGeneralGroupAreCompleted@30() : Boolean;
    BEGIN
      EXIT(("No." <> '') AND (Description <> '') AND ("FA Subclass Code" <> ''));
    END;

    BEGIN
    END.
  }
}

