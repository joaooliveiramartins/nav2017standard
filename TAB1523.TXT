OBJECT Table 1523 Workflow Step Argument
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0;
  }
  PROPERTIES
  {
    Permissions=TableData 1523=rim;
    DataCaptionFields=General Journal Template Name,General Journal Batch Name,Notification User ID;
    OnInsert=BEGIN
               ID := CREATEGUID;
             END;

    OnModify=BEGIN
               CheckEditingIsAllowed;
             END;

    OnDelete=BEGIN
               CheckEditingIsAllowed;
             END;

    OnRename=BEGIN
               CheckEditingIsAllowed;
             END;

    CaptionML=[ENU=Workflow Step Argument;
               PTG=Argumento Etapa Workflow];
    LookupPageID=Page1523;
  }
  FIELDS
  {
    { 1   ;   ;ID                  ;GUID          ;CaptionML=[ENU=ID;
                                                              PTG=ID] }
    { 2   ;   ;Type                ;Option        ;TableRelation="Workflow Step".Type WHERE (Argument=FIELD(ID));
                                                   CaptionML=[ENU=Type;
                                                              PTG=Tipo];
                                                   OptionCaptionML=[ENU=Event,Response;
                                                                    PTG=Evento,Resposta];
                                                   OptionString=Event,Response }
    { 3   ;   ;General Journal Template Name;Code10;
                                                   TableRelation="Gen. Journal Template".Name;
                                                   CaptionML=[ENU=General Journal Template Name;
                                                              PTG=Nome Modelo Di�rio Geral] }
    { 4   ;   ;General Journal Batch Name;Code10  ;TableRelation="Gen. Journal Batch".Name WHERE (Journal Template Name=FIELD(General Journal Template Name));
                                                   CaptionML=[ENU=General Journal Batch Name;
                                                              PTG=Nome Sec��o Di�rio Geral] }
    { 5   ;   ;Notification User ID;Code50        ;TableRelation="User Setup"."User ID";
                                                   CaptionML=[ENU=Notification User ID;
                                                              PTG=Notifica��o ID Utilizador] }
    { 6   ;   ;Notification User License Type;Option;
                                                   FieldClass=FlowField;
                                                   CalcFormula=Lookup(User."License Type" WHERE (User Name=FIELD(Notification User ID)));
                                                   CaptionML=[ENU=Notification User License Type;
                                                              PTG=Tipo de Licen�a de Utilizador de Notifica��o];
                                                   OptionCaptionML=[ENU=Full User,Limited User,Device Only User,Windows Group,External User;
                                                                    PTG=Full User,Limited User,Device Only User,Windows Group,External User];
                                                   OptionString=Full User,Limited User,Device Only User,Windows Group,External User }
    { 7   ;   ;Response Function Name;Code128     ;TableRelation="Workflow Response"."Function Name";
                                                   CaptionML=[ENU=Response Function Name;
                                                              PTG=Nome Resposta Fun��o] }
    { 9   ;   ;Link Target Page    ;Integer       ;TableRelation=AllObjWithCaption."Object ID" WHERE (Object Type=CONST(Page));
                                                   CaptionML=[ENU=Link Target Page;
                                                              PTG=P�gina Alvo Liga��o] }
    { 10  ;   ;Custom Link         ;Text250       ;OnValidate=VAR
                                                                WebRequestHelper@1000 : Codeunit 1299;
                                                              BEGIN
                                                                WebRequestHelper.IsValidUri("Custom Link");
                                                              END;

                                                   ExtendedDatatype=URL;
                                                   CaptionML=[ENU=Custom Link;
                                                              PTG=Liga��o Personalizada] }
    { 11  ;   ;Event Conditions    ;BLOB          ;CaptionML=[ENU=Event Conditions;
                                                              PTG=Condi��es Evento] }
    { 12  ;   ;Approver Type       ;Option        ;CaptionML=[ENU=Approver Type;
                                                              PTG=Tipo Aprovador];
                                                   OptionCaptionML=[ENU=Salesperson/Purchaser,Approver,Workflow User Group;
                                                                    PTG=Vendedor/Comprador,Aprovador,Grupo Utilizador Workflow];
                                                   OptionString=Salesperson/Purchaser,Approver,Workflow User Group }
    { 13  ;   ;Approver Limit Type ;Option        ;CaptionML=[ENU=Approver Limit Type;
                                                              PTG=Tipo Limite Aprovador];
                                                   OptionCaptionML=[ENU=Approver Chain,Direct Approver,First Qualified Approver,Specific Approver;
                                                                    PTG=Cadeia Aprovador,Aprovador Direto,Grupo Utilizador Workflow,Aprovador Espec�fico];
                                                   OptionString=Approver Chain,Direct Approver,First Qualified Approver,Specific Approver }
    { 14  ;   ;Workflow User Group Code;Code20    ;TableRelation="Workflow User Group".Code;
                                                   CaptionML=[ENU=Workflow User Group Code;
                                                              PTG=C�digo Grupo Utilizador Workflow] }
    { 15  ;   ;Due Date Formula    ;DateFormula   ;OnValidate=BEGIN
                                                                IF COPYSTR(FORMAT("Due Date Formula"),1,1) = '-' THEN
                                                                  ERROR(STRSUBSTNO(NoNegValuesErr,FIELDCAPTION("Due Date Formula")));
                                                              END;

                                                   CaptionML=[ENU=Due Date Formula;
                                                              PTG=F�rmula Data Vencimento] }
    { 16  ;   ;Message             ;Text250       ;CaptionML=[ENU=Message;
                                                              PTG=Mensagem] }
    { 17  ;   ;Delegate After      ;Option        ;CaptionML=[ENU=Delegate After;
                                                              PTG=Delegado Depois];
                                                   OptionCaptionML=[ENU=Never,1 day,2 days,5 days;
                                                                    PTG=Nunca,1 dia,2 dias,5 dias];
                                                   OptionString=Never,1 day,2 days,5 days }
    { 18  ;   ;Show Confirmation Message;Boolean  ;CaptionML=[ENU=Show Confirmation Message;
                                                              PTG=Mostrar Mensagem Confirma��o] }
    { 19  ;   ;Table No.           ;Integer       ;CaptionML=[ENU=Table No.;
                                                              PTG=N� Tabela] }
    { 20  ;   ;Field No.           ;Integer       ;CaptionML=[ENU=Field No.;
                                                              PTG=N� Campo] }
    { 21  ;   ;Field Caption       ;Text80        ;FieldClass=FlowField;
                                                   CalcFormula=Lookup(Field."Field Caption" WHERE (TableNo=FIELD(Table No.),
                                                                                                   No.=FIELD(Field No.)));
                                                   CaptionML=[ENU=Field Caption;
                                                              PTG=Nome Campo];
                                                   Editable=No }
    { 22  ;   ;Approver User ID    ;Code50        ;TableRelation="User Setup"."User ID";
                                                   OnLookup=VAR
                                                              UserSetup@1000 : Record 91;
                                                            BEGIN
                                                              IF PAGE.RUNMODAL(PAGE::"Approval User Setup",UserSetup) = ACTION::LookupOK THEN
                                                                VALIDATE("Approver User ID",UserSetup."User ID");
                                                            END;

                                                   CaptionML=[ENU=Approver User ID;
                                                              PTG=ID do Utilizador de Aprova��o] }
    { 100 ;   ;Response Option Group;Code20       ;FieldClass=FlowField;
                                                   CalcFormula=Lookup("Workflow Response"."Response Option Group" WHERE (Function Name=FIELD(Response Function Name)));
                                                   CaptionML=[ENU=Response Option Group;
                                                              PTG=Grupo Op��o Resposta];
                                                   Editable=No }
  }
  KEYS
  {
    {    ;ID                                      ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {
    VAR
      NoNegValuesErr@1000 : TextConst 'ENU=%1 must be a positive value.;PTG=%1 dever� ser um valor positivo';

    PROCEDURE Clone@1() : GUID;
    VAR
      WorkflowStepArgument@1001 : Record 1523;
    BEGIN
      CALCFIELDS("Event Conditions");
      WorkflowStepArgument.COPY(Rec);
      WorkflowStepArgument.INSERT(TRUE);
      EXIT(WorkflowStepArgument.ID);
    END;

    PROCEDURE Equals@9(WorkflowStepArgument@1000 : Record 1523;SkipBlob@1004 : Boolean) : Boolean;
    VAR
      TypeHelper@1003 : Codeunit 10;
      OtherRecRef@1002 : RecordRef;
      ThisRecRef@1001 : RecordRef;
    BEGIN
      ThisRecRef.GETTABLE(Rec);
      OtherRecRef.GETTABLE(WorkflowStepArgument);

      IF NOT TypeHelper.Equals(ThisRecRef,OtherRecRef,SkipBlob) THEN
        EXIT(FALSE);

      EXIT(TRUE);
    END;

    PROCEDURE GetEventFilters@8() Filters : Text;
    VAR
      FiltersInStream@1002 : InStream;
    BEGIN
      IF "Event Conditions".HASVALUE THEN BEGIN
        CALCFIELDS("Event Conditions");
        "Event Conditions".CREATEINSTREAM(FiltersInStream);
        FiltersInStream.READ(Filters);
      END;
    END;

    PROCEDURE SetEventFilters@2(Filters@1000 : Text);
    VAR
      FiltersOutStream@1001 : OutStream;
    BEGIN
      "Event Conditions".CREATEOUTSTREAM(FiltersOutStream);
      FiltersOutStream.WRITE(Filters);
      MODIFY(TRUE);
    END;

    LOCAL PROCEDURE CheckEditingIsAllowed@12();
    VAR
      Workflow@1000 : Record 1501;
      WorkflowStep@1001 : Record 1502;
    BEGIN
      IF ISNULLGUID(ID) THEN
        EXIT;

      WorkflowStep.SETRANGE(Argument,ID);
      IF WorkflowStep.FINDFIRST THEN BEGIN
        Workflow.GET(WorkflowStep."Workflow Code");
        Workflow.CheckEditingIsAllowed;
      END;
    END;

    PROCEDURE HideExternalUsers@5();
    VAR
      PermissionManager@1001 : Codeunit 9002;
      OriginalFilterGroup@1000 : Integer;
    BEGIN
      IF NOT PermissionManager.SoftwareAsAService THEN
        EXIT;

      OriginalFilterGroup := FILTERGROUP;
      FILTERGROUP := 2;
      CALCFIELDS("Notification User License Type");
      SETFILTER("Notification User License Type",'<>%1',"Notification User License Type"::"External User");
      FILTERGROUP := OriginalFilterGroup;
    END;

    BEGIN
    END.
  }
}

