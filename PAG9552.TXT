OBJECT Page 9552 Document Service Acc. Pwd.
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Document Service Acc. Pwd.;
               PTG=Palavra-Passe Conta Documentos Servi�o];
    PageType=StandardDialog;
    OnQueryClosePage=BEGIN
                       IF CloseAction = ACTION::OK THEN
                         IF PasswordField <> ConfirmPasswordField THEN
                           ERROR(PasswordValidationErr);
                     END;

  }
  CONTROLS
  {
    { 1   ;0   ;Container ;
                ContainerType=ContentArea }

    { 2   ;1   ;Group     ;
                GroupType=Group;
                InstructionalTextML=[ENU=Enter the password for your online document storage account.;
                                     PTG=Introduza a Palavra-Passe da conta de arquivo de documentos online.] }

    { 3   ;2   ;Field     ;
                Name=PasswordField;
                ExtendedDatatype=Masked;
                CaptionML=[ENU=Set Password;
                           PTG=Definir Palavra-Passe];
                ToolTipML=[ENU=Specifies the password for your online storage account.;
                           PTG=""];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=PasswordField;
                ShowCaption=Yes }

    { 4   ;2   ;Field     ;
                Name=ConfirmPasswordField;
                ExtendedDatatype=Masked;
                CaptionML=[ENU=Confirm Password;
                           PTG=Confirmar Palavra-Passe];
                ToolTipML=[ENU=Specifies the password repeated.;
                           PTG=""];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=ConfirmPasswordField;
                ShowCaption=Yes }

  }
  CODE
  {
    VAR
      PasswordField@1000 : Text[80];
      ConfirmPasswordField@1001 : Text[80];
      PasswordValidationErr@1002 : TextConst 'ENU=The passwords that you entered do not match.;PTG=A palavra-passe indicada n�o � v�lida.';

    PROCEDURE GetData@1() : Text[80];
    BEGIN
      EXIT(PasswordField);
    END;

    BEGIN
    END.
  }
}

