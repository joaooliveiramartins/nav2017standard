OBJECT Table 130403 CAL Test Enabled Codeunit
{
  OBJECT-PROPERTIES
  {
    Date=15/09/15;
    Time=12:00:00;
    Version List=NAVW19.00;
  }
  PROPERTIES
  {
    CaptionML=[ENU=CAL Test Enabled Codeunit;
               PTG=Codeunit Teste Ativa CAL];
  }
  FIELDS
  {
    { 1   ;   ;No.                 ;Integer       ;AutoIncrement=Yes;
                                                   CaptionML=[ENU=No.;
                                                              PTG=N�] }
    { 2   ;   ;Test Codeunit ID    ;Integer       ;CaptionML=[ENU=Test Codeunit ID;
                                                              PTG=ID Codeunit Teste] }
  }
  KEYS
  {
    {    ;No.                                     ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    END.
  }
}

