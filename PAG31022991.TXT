OBJECT Page 31022991 Payment Orders Maturity
{
  OBJECT-PROPERTIES
  {
    Date=01/02/15;
    Time=13:00:00;
    Version List=NAVPTSS81.00;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Payment Orders Maturity;
               PTG=Vencimento Ordens Pagamento];
    SaveValues=Yes;
    InsertAllowed=No;
    DeleteAllowed=No;
    ModifyAllowed=No;
    SourceTable=Table31022952;
    DataCaptionExpr=Caption;
    PageType=List;
    OnOpenPage=BEGIN
                 UpdateSubPage;
               END;

    OnAfterGetRecord=BEGIN
                       UpdateSubPage;
                     END;

  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 13  ;1   ;Group     ;
                CaptionML=[ENU=General;
                           PTG=Geral] }

    { 14  ;2   ;Field     ;
                CaptionML=[ENU=Category Filter;
                           PTG=Filtro Categoria];
                SourceExpr="Category Filter";
                OnValidate=BEGIN
                             UpdateSubPage;
                           END;
                            }

    { 1903121901;1;Group  ;
                CaptionML=[ENU=Options;
                           PTG=Op��es] }

    { 4   ;2   ;Field     ;
                CaptionML=[ENU=View by;
                           PTG=Ver por];
                ToolTipML=[ENU=Day;
                           PTG=Dia];
                OptionCaptionML=[ENU=Day,Week,Month,Quarter,Year,Period;
                                 PTG=Dia,Semana,M�s,Trimestre,Ano,Per�odo];
                SourceExpr=PeriodType;
                OnValidate=BEGIN
                             IF PeriodType = PeriodType::Period THEN
                               PeriodPeriodTypeOnValidate;
                             IF PeriodType = PeriodType::Year THEN
                               YearPeriodTypeOnValidate;
                             IF PeriodType = PeriodType::Quarter THEN
                               QuarterPeriodTypeOnValidate;
                             IF PeriodType = PeriodType::Month THEN
                               MonthPeriodTypeOnValidate;
                             IF PeriodType = PeriodType::Week THEN
                               WeekPeriodTypeOnValidate;
                             IF PeriodType = PeriodType::Day THEN
                               DayPeriodTypeOnValidate;
                           END;
                            }

    { 2   ;2   ;Field     ;
                CaptionML=[ENU=View as;
                           PTG=Ver como];
                ToolTipML=[ENU=Net Change;
                           PTG=Saldo Per�odo];
                OptionCaptionML=[ENU=Net Change,Balance at Date;
                                 PTG=Saldo Per�odo,Saldo � Data];
                SourceExpr=AmountType;
                OnValidate=BEGIN
                             IF AmountType = AmountType::"Balance at Date" THEN
                               BalanceatDateAmountTypeOnValid;
                             IF AmountType = AmountType::"Net Change" THEN
                               NetChangeAmountTypeOnValidate;
                           END;
                            }

    { 3   ;1   ;Part      ;
                Name=MaturityLines;
                PagePartID=Page31022974 }

  }
  CODE
  {
    VAR
      PeriodType@1110000 : 'Day,Week,Month,Quarter,Year,Period';
      AmountType@1110001 : 'Net Change,Balance at Date';

    LOCAL PROCEDURE UpdateSubPage@1();
    BEGIN
      CurrPage.MaturityLines.PAGE.TypePayable;
      CurrPage.MaturityLines.PAGE.SetPayable(Rec,PeriodType,AmountType);
    END;

    LOCAL PROCEDURE DayPeriodTypeOnPush@19008851();
    BEGIN
      UpdateSubPage;
    END;

    LOCAL PROCEDURE WeekPeriodTypeOnPush@19046063();
    BEGIN
      UpdateSubPage;
    END;

    LOCAL PROCEDURE MonthPeriodTypeOnPush@19047374();
    BEGIN
      UpdateSubPage;
    END;

    LOCAL PROCEDURE QuarterPeriodTypeOnPush@19018850();
    BEGIN
      UpdateSubPage;
    END;

    LOCAL PROCEDURE YearPeriodTypeOnPush@19051042();
    BEGIN
      UpdateSubPage;
    END;

    LOCAL PROCEDURE PeriodPeriodTypeOnPush@19032639();
    BEGIN
      UpdateSubPage;
    END;

    LOCAL PROCEDURE BalanceatDateAmountTypeOnPush@19049003();
    BEGIN
      UpdateSubPage;
    END;

    LOCAL PROCEDURE NetChangeAmountTypeOnPush@19074855();
    BEGIN
      UpdateSubPage;
    END;

    LOCAL PROCEDURE DayPeriodTypeOnValidate@19012979();
    BEGIN
      DayPeriodTypeOnPush;
    END;

    LOCAL PROCEDURE WeekPeriodTypeOnValidate@19058475();
    BEGIN
      WeekPeriodTypeOnPush;
    END;

    LOCAL PROCEDURE MonthPeriodTypeOnValidate@19021027();
    BEGIN
      MonthPeriodTypeOnPush;
    END;

    LOCAL PROCEDURE QuarterPeriodTypeOnValidate@19015346();
    BEGIN
      QuarterPeriodTypeOnPush;
    END;

    LOCAL PROCEDURE YearPeriodTypeOnValidate@19064743();
    BEGIN
      YearPeriodTypeOnPush;
    END;

    LOCAL PROCEDURE PeriodPeriodTypeOnValidate@19066307();
    BEGIN
      PeriodPeriodTypeOnPush;
    END;

    LOCAL PROCEDURE NetChangeAmountTypeOnValidate@19062218();
    BEGIN
      NetChangeAmountTypeOnPush;
    END;

    LOCAL PROCEDURE BalanceatDateAmountTypeOnValid@19007073();
    BEGIN
      BalanceatDateAmountTypeOnPush;
    END;

    BEGIN
    END.
  }
}

