OBJECT Page 7177 Available Credit
{
  OBJECT-PROPERTIES
  {
    Date=13/04/17;
    Time=15:20:18;
    Modified=Yes;
    Version List=NAVW110.0.00.15052;
  }
  PROPERTIES
  {
    Editable=No;
    CaptionML=[ENU=Available Credit;
               PTG=Cr�dito Dispon�vel];
    SourceTable=Table18;
    PageType=Card;
    OnAfterGetRecord=BEGIN
                       SETRANGE("Date Filter",0D,WORKDATE);
                       StyleTxt := SetStyle;
                     END;

  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                CaptionML=[ENU=General;
                           PTG=Geral] }

    { 13  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the payment amount that the customer owes for completed sales. This value is also known as the customer's balance.;
                           PTG=""];
                ApplicationArea=#Suite;
                SourceExpr="Balance (LCY)";
                OnDrillDown=VAR
                              DtldCustLedgEntry@1000 : Record 379;
                              CustLedgEntry@1001 : Record 21;
                            BEGIN
                              DtldCustLedgEntry.SETRANGE("Customer No.","No.");
                              COPYFILTER("Global Dimension 1 Filter",DtldCustLedgEntry."Initial Entry Global Dim. 1");
                              COPYFILTER("Global Dimension 2 Filter",DtldCustLedgEntry."Initial Entry Global Dim. 2");
                              COPYFILTER("Currency Filter",DtldCustLedgEntry."Currency Code");
                              CustLedgEntry.DrillDownOnEntries(DtldCustLedgEntry);
                            END;
                             }

    { 11  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies your expected sales income from the customer in LCY based on ongoing sales orders.;
                           PTG=""];
                ApplicationArea=#Suite;
                SourceExpr="Outstanding Orders (LCY)" }

    { 9   ;2   ;Field     ;
                CaptionML=[ENU=Shipped Not Invd. (LCY);
                           PTG=Enviado N�o Faturado (DL)];
                ToolTipML=[ENU=Specifies your expected sales income from the customer in LCY based on ongoing sales orders where items have been shipped.;
                           PTG=""];
                ApplicationArea=#Suite;
                SourceExpr="Shipped Not Invoiced (LCY)" }

    { 2   ;2   ;Field     ;
                CaptionML=[ENU=Outstanding Invoices (LCY);
                           PTG=Faturas Pendentes (DL)];
                ToolTipML=[ENU=Specifies your expected sales income from the customer in LCY based on unpaid sales invoices.;
                           PTG=""];
                ApplicationArea=#Suite;
                SourceExpr="Outstanding Invoices (LCY)" }

    { 18  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies your expected service income from the customer in LCY based on ongoing service orders.;
                           PTG=""];
                ApplicationArea=#Suite;
                SourceExpr="Outstanding Serv. Orders (LCY)" }

    { 23  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies your expected service income from the customer in LCY based on service orders that are shipped but not invoiced.;
                           PTG=""];
                ApplicationArea=#Suite;
                SourceExpr="Serv Shipped Not Invoiced(LCY)" }

    { 6   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies your expected service income from the customer in LCY based on unpaid service invoices.;
                           PTG=""];
                ApplicationArea=#Suite;
                SourceExpr="Outstanding Serv.Invoices(LCY)" }

    { 5   ;2   ;Field     ;
                CaptionML=[ENU=Total (LCY);
                           PTG=Total (DL)];
                ToolTipML=[ENU=Specifies the payment amount that you owe the vendor for completed purchases plus purchases that are still ongoing.;
                           PTG=""];
                ApplicationArea=#Suite;
                SourceExpr=GetTotalAmountLCYUI;
                AutoFormatType=1 }

    { 4   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the maximum amount you allow the customer to exceed the payment balance before warnings are issued.;
                           PTG=""];
                ApplicationArea=#Suite;
                SourceExpr="Credit Limit (LCY)";
                StyleExpr=StyleTxt }

    { 17  ;2   ;Field     ;
                CaptionML=[ENU=Available Credit (LCY);
                           PTG=Cr�dito Dispon�vel (DL)];
                ToolTipML=ENU=Specifies a customer's available credit. If the available credit is 0 and the customer's credit limit is also 0, then the customer has unlimited credit because no credit limit has been defined.;
                ApplicationArea=#Suite;
                SourceExpr=CalcAvailableCreditUI }

    { 3   ;2   ;Field     ;
                Name=Balance Due (LCY);
                ApplicationArea=#Suite;
                SourceExpr=CalcOverdueBalance;
                CaptionClass=FORMAT(STRSUBSTNO(Text000,FORMAT(WORKDATE)));
                OnDrillDown=VAR
                              DtldCustLedgEntry@1000 : Record 379;
                              CustLedgEntry@1001 : Record 21;
                            BEGIN
                              DtldCustLedgEntry.SETFILTER("Customer No.","No.");
                              COPYFILTER("Global Dimension 1 Filter",DtldCustLedgEntry."Initial Entry Global Dim. 1");
                              COPYFILTER("Global Dimension 2 Filter",DtldCustLedgEntry."Initial Entry Global Dim. 2");
                              COPYFILTER("Currency Filter",DtldCustLedgEntry."Currency Code");
                              CustLedgEntry.DrillDownOnOverdueEntries(DtldCustLedgEntry);
                            END;
                             }

    { 7   ;2   ;Field     ;
                CaptionML=[ENU=Invoiced Prepayment Amount (LCY);
                           PTG=Valor Pr�-Pagamento Faturado (DL)];
                ToolTipML=[ENU=Specifies your sales income from the customer based on invoiced prepayments.;
                           PTG=""];
                ApplicationArea=#Suite;
                SourceExpr=GetInvoicedPrepmtAmountLCY }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {
    VAR
      Text000@1000 : TextConst 'ENU=Overdue Amounts (LCY) as of %1;PTG=Valores Vencidos (DL) a %1';
      StyleTxt@1001 : Text;

    BEGIN
    END.
  }
}

