OBJECT Page 2128 O365 Email Settings
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Email for all new invoices;
               PTG=Email para todas as novas faturas];
    InsertAllowed=No;
    SourceTable=Table2118;
    PageType=List;
  }
  CONTROLS
  {
    { 1   ;    ;Container ;
                ContainerType=ContentArea }

    { 2   ;1   ;Part      ;
                Name=CC List;
                CaptionML=[ENU=CC List;
                           PTG=Lista CC];
                ToolTipML=[ENU=List of CC recipients on all new invoices;
                           PTG=Lista de destinat rios CC em todas as novas faturas];
                ApplicationArea=#Basic,#Suite;
                PagePartID=Page2126;
                PartType=Page }

    { 3   ;1   ;Part      ;
                Name=BCC List;
                CaptionML=[ENU=BCC List;
                           PTG=Lista BCC];
                ToolTipML=[ENU=List of BCC recipients on all new invoices;
                           PTG=Lista de destinatarios BCC];
                ApplicationArea=#Basic,#Suite;
                PagePartID=Page2127;
                PartType=Page }

  }
  CODE
  {

    BEGIN
    END.
  }
}

