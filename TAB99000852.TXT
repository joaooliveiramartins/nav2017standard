OBJECT Table 99000852 Production Forecast Entry
{
  OBJECT-PROPERTIES
  {
    Date=07/09/12;
    Time=12:00:00;
    Version List=NAVW17.00;
  }
  PROPERTIES
  {
    OnInsert=VAR
               ForecastEntry@1000 : Record 99000852;
             BEGIN
               TESTFIELD("Forecast Date");
               TESTFIELD("Production Forecast Name");
               LOCKTABLE;
               IF "Entry No." = 0 THEN
                 IF ForecastEntry.FINDLAST THEN
                   "Entry No." := ForecastEntry."Entry No." + 1;
               PlanningAssignment.AssignOne("Item No.",'',"Location Code","Forecast Date");
             END;

    OnModify=BEGIN
               PlanningAssignment.AssignOne("Item No.",'',"Location Code","Forecast Date");
             END;

    CaptionML=[ENU=Production Forecast Entry;
               PTG=Mov. Previs�o Produ��o];
    LookupPageID=Page99000922;
    DrillDownPageID=Page99000922;
  }
  FIELDS
  {
    { 1   ;   ;Production Forecast Name;Code10    ;TableRelation="Production Forecast Name";
                                                   CaptionML=[ENU=Production Forecast Name;
                                                              PTG=Nome Previs�o de Produ��o] }
    { 2   ;   ;Entry No.           ;Integer       ;CaptionML=[ENU=Entry No.;
                                                              PTG=N� Mov.] }
    { 3   ;   ;Item No.            ;Code20        ;TableRelation=Item;
                                                   CaptionML=[ENU=Item No.;
                                                              PTG=N� Produto] }
    { 4   ;   ;Forecast Date       ;Date          ;CaptionML=[ENU=Forecast Date;
                                                              PTG=Data Previs�o] }
    { 5   ;   ;Forecast Quantity   ;Decimal       ;OnValidate=BEGIN
                                                                "Forecast Quantity (Base)" := "Forecast Quantity" * "Qty. per Unit of Measure";
                                                              END;

                                                   CaptionML=[ENU=Forecast Quantity;
                                                              PTG=Quantidade Previs�o];
                                                   DecimalPlaces=0:5 }
    { 6   ;   ;Unit of Measure Code;Code10        ;TableRelation="Item Unit of Measure".Code WHERE (Item No.=FIELD(Item No.));
                                                   OnValidate=BEGIN
                                                                ItemUnitofMeasure.GET("Item No.","Unit of Measure Code");
                                                                "Qty. per Unit of Measure" := ItemUnitofMeasure."Qty. per Unit of Measure";
                                                                "Forecast Quantity" := "Forecast Quantity (Base)" / "Qty. per Unit of Measure";
                                                              END;

                                                   CaptionML=[ENU=Unit of Measure Code;
                                                              PTG=C�d. Unidade Medida] }
    { 7   ;   ;Qty. per Unit of Measure;Decimal   ;CaptionML=[ENU=Qty. per Unit of Measure;
                                                              PTG=Qtd. por Unidade Medida];
                                                   DecimalPlaces=0:5;
                                                   Editable=No }
    { 8   ;   ;Forecast Quantity (Base);Decimal   ;OnValidate=BEGIN
                                                                IF "Unit of Measure Code" = '' THEN BEGIN
                                                                  Item.GET("Item No.");
                                                                  "Unit of Measure Code" := Item."Sales Unit of Measure";
                                                                  ItemUnitofMeasure.GET("Item No.","Unit of Measure Code");
                                                                  "Qty. per Unit of Measure" := ItemUnitofMeasure."Qty. per Unit of Measure";
                                                                END;
                                                                VALIDATE("Unit of Measure Code");
                                                              END;

                                                   CaptionML=[ENU=Forecast Quantity (Base);
                                                              PTG=Quantidade Previs�o (Base)];
                                                   DecimalPlaces=0:5 }
    { 10  ;   ;Location Code       ;Code10        ;TableRelation=Location;
                                                   CaptionML=[ENU=Location Code;
                                                              PTG=C�d. Localiza��o] }
    { 12  ;   ;Component Forecast  ;Boolean       ;CaptionML=[ENU=Component Forecast;
                                                              PTG=Previs�o de Componente] }
    { 13  ;   ;Description         ;Text50        ;CaptionML=[ENU=Description;
                                                              PTG=Descri��o] }
  }
  KEYS
  {
    {    ;Entry No.                               ;Clustered=Yes }
    {    ;Production Forecast Name,Item No.,Location Code,Forecast Date,Component Forecast;
                                                   SumIndexFields=Forecast Quantity (Base) }
    {    ;Production Forecast Name,Item No.,Component Forecast,Forecast Date,Location Code;
                                                   SumIndexFields=Forecast Quantity (Base) }
  }
  FIELDGROUPS
  {
  }
  CODE
  {
    VAR
      ItemUnitofMeasure@1000 : Record 5404;
      Item@1001 : Record 27;
      PlanningAssignment@1002 : Record 99000850;

    BEGIN
    END.
  }
}

