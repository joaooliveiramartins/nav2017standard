OBJECT Page 286 Recurring Item Jnl.
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Recurring Item Jnl.;
               PTG=Di�rio Peri�dico Produto];
    SaveValues=Yes;
    SourceTable=Table83;
    DelayedInsert=Yes;
    DataCaptionFields=Journal Batch Name;
    PageType=Worksheet;
    AutoSplitKey=Yes;
    OnOpenPage=VAR
                 JnlSelected@1000 : Boolean;
               BEGIN
                 IF IsOpenedFromBatch THEN BEGIN
                   CurrentJnlBatchName := "Journal Batch Name";
                   ItemJnlMgt.OpenJnl(CurrentJnlBatchName,Rec);
                   EXIT;
                 END;
                 ItemJnlMgt.TemplateSelection(PAGE::"Recurring Item Jnl.",0,TRUE,Rec,JnlSelected);
                 IF NOT JnlSelected THEN
                   ERROR('');
                 ItemJnlMgt.OpenJnl(CurrentJnlBatchName,Rec);
               END;

    OnAfterGetRecord=BEGIN
                       ShowShortcutDimCode(ShortcutDimCode);
                       ShowNewShortcutDimCode(NewShortcutDimCode);
                     END;

    OnNewRecord=BEGIN
                  SetUpNewLine(xRec);
                  CLEAR(ShortcutDimCode);
                  CLEAR(NewShortcutDimCode);
                END;

    OnInsertRecord=BEGIN
                     IF "Entry Type" > "Entry Type"::"Negative Adjmt." THEN
                       ERROR(Text000,"Entry Type");
                   END;

    OnDeleteRecord=VAR
                     ReserveItemJnlLine@1000 : Codeunit 99000835;
                   BEGIN
                     COMMIT;
                     IF NOT ReserveItemJnlLine.DeleteLineConfirm(Rec) THEN
                       EXIT(FALSE);
                     ReserveItemJnlLine.DeleteLine(Rec);
                   END;

    OnAfterGetCurrRecord=BEGIN
                           ItemJnlMgt.GetItem("Item No.",ItemDescription);
                         END;

    ActionList=ACTIONS
    {
      { 1900000003;0 ;ActionContainer;
                      ActionContainerType=RelatedInformation }
      { 103     ;1   ;ActionGroup;
                      CaptionML=[ENU=&Line;
                                 PTG=&Linha];
                      Image=Line }
      { 104     ;2   ;Action    ;
                      AccessByPermission=TableData 348=R;
                      ShortCutKey=Shift+Ctrl+D;
                      CaptionML=[ENU=Dimensions;
                                 PTG=Dimens�es];
                      ToolTipML=ENU=View or edits dimensions, such as area, project, or department, that you can assign to sales and purchase documents to distribute costs and analyze transaction history.;
                      ApplicationArea=#Suite;
                      Image=Dimensions;
                      OnAction=BEGIN
                                 ShowDimensions;
                                 CurrPage.SAVERECORD;
                               END;
                                }
      { 6500    ;2   ;Action    ;
                      ShortCutKey=Shift+Ctrl+I;
                      CaptionML=[ENU=Item &Tracking Lines;
                                 PTG=Linhas Ras&treio Produto];
                      Image=ItemTrackingLines;
                      OnAction=BEGIN
                                 OpenItemTrackingLines(FALSE);
                               END;
                                }
      { 78      ;2   ;Action    ;
                      CaptionML=[ENU=Bin Contents;
                                 PTG=Conte�do Posi��o];
                      RunObject=Page 7305;
                      RunPageView=SORTING(Location Code,Bin Code,Item No.,Variant Code);
                      RunPageLink=Location Code=FIELD(Location Code),
                                  Item No.=FIELD(Item No.),
                                  Variant Code=FIELD(Variant Code);
                      Image=BinContent }
      { 33      ;1   ;ActionGroup;
                      CaptionML=[ENU=&Item;
                                 PTG=&Produto];
                      Image=Item }
      { 34      ;2   ;Action    ;
                      ShortCutKey=Shift+F7;
                      CaptionML=[ENU=Card;
                                 PTG=Ficha];
                      ToolTipML=ENU=View or change detailed information about the record that is being processed on the journal line.;
                      ApplicationArea=#Suite;
                      RunObject=Page 30;
                      RunPageLink=No.=FIELD(Item No.);
                      Image=EditLines }
      { 35      ;2   ;Action    ;
                      ShortCutKey=Ctrl+F7;
                      CaptionML=[ENU=Ledger E&ntries;
                                 PTG=Movime&ntos];
                      ToolTipML=ENU=View the history of transactions that have been posted for the selected record.;
                      ApplicationArea=#Suite;
                      RunObject=Page 38;
                      RunPageView=SORTING(Item No.);
                      RunPageLink=Item No.=FIELD(Item No.);
                      Promoted=No;
                      Image=ItemLedger;
                      PromotedCategory=Process }
      { 99      ;2   ;ActionGroup;
                      CaptionML=[ENU=Item Availability by;
                                 PTG=Disponibilidade Produto por];
                      Image=ItemAvailability }
      { 5       ;3   ;Action    ;
                      CaptionML=[ENU=Event;
                                 PTG=Ocorr�ncia];
                      ToolTipML=ENU=View how the actual and projected inventory level of an item will develop over time according to supply and demand events.;
                      ApplicationArea=#Suite;
                      Image=Event;
                      OnAction=BEGIN
                                 ItemAvailFormsMgt.ShowItemAvailFromItemJnlLine(Rec,ItemAvailFormsMgt.ByEvent)
                               END;
                                }
      { 100     ;3   ;Action    ;
                      CaptionML=[ENU=Period;
                                 PTG=Per�odo];
                      ToolTipML=ENU=Show the actual and projected quantity of an item over time according to a specified time interval, such as by day, week or month.;
                      ApplicationArea=#Suite;
                      Image=Period;
                      OnAction=BEGIN
                                 ItemAvailFormsMgt.ShowItemAvailFromItemJnlLine(Rec,ItemAvailFormsMgt.ByPeriod)
                               END;
                                }
      { 101     ;3   ;Action    ;
                      CaptionML=[ENU=Variant;
                                 PTG=Variante];
                      ToolTipML=ENU=View or edit the item's variants. Instead of setting up each color of an item as a separate item, you can set up the various colors as variants of the item.;
                      Image=ItemVariant;
                      OnAction=BEGIN
                                 ItemAvailFormsMgt.ShowItemAvailFromItemJnlLine(Rec,ItemAvailFormsMgt.ByVariant)
                               END;
                                }
      { 87      ;3   ;Action    ;
                      AccessByPermission=TableData 14=R;
                      CaptionML=[ENU=Location;
                                 PTG=Localiza��o];
                      Image=Warehouse;
                      OnAction=BEGIN
                                 ItemAvailFormsMgt.ShowItemAvailFromItemJnlLine(Rec,ItemAvailFormsMgt.ByLocation)
                               END;
                                }
      { 3       ;3   ;Action    ;
                      CaptionML=[ENU=BOM Level;
                                 PTG=N�vel L. M.];
                      ToolTipML=ENU=View how the inventory level of an item develops over time according to the bill of materials level that you select.;
                      Image=BOMLevel;
                      OnAction=BEGIN
                                 ItemAvailFormsMgt.ShowItemAvailFromItemJnlLine(Rec,ItemAvailFormsMgt.ByBOM)
                               END;
                                }
      { 1900000004;  ;ActionContainer;
                      ActionContainerType=ActionItems }
      { 90      ;1   ;ActionGroup;
                      CaptionML=[ENU=F&unctions;
                                 PTG=F&un��es];
                      Image=Action }
      { 92      ;2   ;Action    ;
                      CaptionML=[ENU=E&xplode BOM;
                                 PTG=E&xpandir L. M.];
                      ToolTipML=ENU=View the contents of the bill of materials.;
                      ApplicationArea=#Suite;
                      RunObject=Codeunit 246;
                      Image=ExplodeBOM }
      { 36      ;1   ;ActionGroup;
                      CaptionML=[ENU=P&osting;
                                 PTG=&Registo];
                      Image=Post }
      { 37      ;2   ;Action    ;
                      Ellipsis=Yes;
                      CaptionML=[ENU=Test Report;
                                 PTG=Verificar...];
                      ToolTipML=ENU=View a test report so that you can find and correct any errors before you perform the actual posting of the journal or document.;
                      ApplicationArea=#Suite;
                      Image=TestReport;
                      OnAction=BEGIN
                                 ReportPrint.PrintItemJnlLine(Rec);
                               END;
                                }
      { 38      ;2   ;Action    ;
                      ShortCutKey=F9;
                      CaptionML=[ENU=P&ost;
                                 PTG=R&egistar];
                      ToolTipML=ENU=Finalize the document or journal by posting the amounts and quantities to the related accounts in your company books.;
                      ApplicationArea=#Suite;
                      RunObject=Codeunit 241;
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=PostOrder;
                      PromotedCategory=Process }
      { 39      ;2   ;Action    ;
                      ShortCutKey=Shift+F9;
                      CaptionML=[ENU=Post and &Print;
                                 PTG=Registar e &Imprimir];
                      ToolTipML=ENU=Finalize and prepare to print the document or journal. The values and quantities are posted to the related accounts. A report request window where you can specify what to include on the print-out.;
                      ApplicationArea=#Suite;
                      RunObject=Codeunit 242;
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=PostPrint;
                      PromotedCategory=Process }
      { 65      ;1   ;Action    ;
                      Ellipsis=Yes;
                      CaptionML=[ENU=&Print;
                                 PTG=&Imprimir];
                      ToolTipML=ENU=Prepare to print the document. The report request window for the document opens where you can specify what to include on the print-out.;
                      ApplicationArea=#Suite;
                      Promoted=Yes;
                      Image=Print;
                      PromotedCategory=Process;
                      OnAction=VAR
                                 ItemJnlLine@1001 : Record 83;
                               BEGIN
                                 ItemJnlLine.COPY(Rec);
                                 ItemJnlLine.SETRANGE("Journal Template Name","Journal Template Name");
                                 ItemJnlLine.SETRANGE("Journal Batch Name","Journal Batch Name");
                                 REPORT.RUNMODAL(REPORT::"Inventory Movement",TRUE,TRUE,ItemJnlLine);
                               END;
                                }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 29  ;1   ;Field     ;
                Lookup=Yes;
                CaptionML=[ENU=Batch Name;
                           PTG=Nome Sec��o];
                ToolTipML=ENU=Specifies the journal batch of the recurring item journal.;
                ApplicationArea=#Suite;
                SourceExpr=CurrentJnlBatchName;
                OnValidate=BEGIN
                             ItemJnlMgt.CheckName(CurrentJnlBatchName,Rec);
                             CurrentJnlBatchNameOnAfterVali;
                           END;

                OnLookup=BEGIN
                           CurrPage.SAVERECORD;
                           ItemJnlMgt.LookupName(CurrentJnlBatchName,Rec);
                           CurrPage.UPDATE(FALSE);
                         END;
                          }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 2   ;2   ;Field     ;
                ToolTipML=ENU=Specifies a recurring method, if you have indicated that the journal is recurring.;
                ApplicationArea=#Suite;
                SourceExpr="Recurring Method" }

    { 4   ;2   ;Field     ;
                ToolTipML=ENU=Specifies a recurring frequency if you have indicated that the journal is recurring.;
                ApplicationArea=#Suite;
                SourceExpr="Recurring Frequency" }

    { 6   ;2   ;Field     ;
                ToolTipML=ENU=Specifies the posting date for the entry.;
                ApplicationArea=#Suite;
                SourceExpr="Posting Date" }

    { 71  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the date on the document that provides the basis for the entry on the item journal line.;
                SourceExpr="Document Date";
                Visible=FALSE }

    { 8   ;2   ;Field     ;
                ToolTipML=ENU=Specifies the type of transaction that will be posted from the item journal line.;
                OptionCaptionML=[ENU=Purchase,Sale,Positive Adjmt.,Negative Adjmt.;
                                 PTG=Compra,Venda,Ajuste Positivo,Ajuste Negativo];
                ApplicationArea=#Suite;
                SourceExpr="Entry Type" }

    { 10  ;2   ;Field     ;
                ToolTipML=ENU=Specifies a document number for the journal line.;
                ApplicationArea=#Suite;
                SourceExpr="Document No." }

    { 12  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the number of the item on the journal line.;
                ApplicationArea=#Suite;
                SourceExpr="Item No.";
                OnValidate=BEGIN
                             ItemJnlMgt.GetItem("Item No.",ItemDescription);
                             ShowShortcutDimCode(ShortcutDimCode);
                           END;
                            }

    { 31  ;2   ;Field     ;
                ToolTipML=ENU=Specifies a variant code for the item.;
                SourceExpr="Variant Code";
                Visible=FALSE }

    { 14  ;2   ;Field     ;
                ToolTipML=ENU=Specifies a description of the item on the journal line.;
                ApplicationArea=#Suite;
                SourceExpr=Description }

    { 59  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the dimension value code that the item journal line is linked to.;
                SourceExpr="Shortcut Dimension 1 Code";
                Visible=FALSE }

    { 49  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the new dimension value code that the item journal line will be linked to.;
                SourceExpr="New Shortcut Dimension 1 Code";
                Visible=FALSE }

    { 57  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the dimension value code that the item journal line is linked to.;
                SourceExpr="Shortcut Dimension 2 Code";
                Visible=FALSE }

    { 61  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the new dimension value code that the item journal line will be linked to.;
                SourceExpr="New Shortcut Dimension 2 Code";
                Visible=FALSE }

    { 300 ;2   ;Field     ;
                SourceExpr=ShortcutDimCode[3];
                CaptionClass='1,2,3';
                TableRelation="Dimension Value".Code WHERE (Global Dimension No.=CONST(3),
                                                            Dimension Value Type=CONST(Standard),
                                                            Blocked=CONST(No));
                Visible=FALSE;
                OnValidate=BEGIN
                             ValidateShortcutDimCode(3,ShortcutDimCode[3]);
                           END;
                            }

    { 312 ;2   ;Field     ;
                SourceExpr=NewShortcutDimCode[3];
                CaptionClass=Text001;
                Visible=FALSE;
                OnValidate=BEGIN
                             TESTFIELD("Entry Type","Entry Type"::Transfer);
                             ValidateNewShortcutDimCode(3,NewShortcutDimCode[3]);
                           END;

                OnLookup=BEGIN
                           LookupNewShortcutDimCode(3,NewShortcutDimCode[3]);
                         END;
                          }

    { 302 ;2   ;Field     ;
                SourceExpr=ShortcutDimCode[4];
                CaptionClass='1,2,4';
                TableRelation="Dimension Value".Code WHERE (Global Dimension No.=CONST(4),
                                                            Dimension Value Type=CONST(Standard),
                                                            Blocked=CONST(No));
                Visible=FALSE;
                OnValidate=BEGIN
                             ValidateShortcutDimCode(4,ShortcutDimCode[4]);
                           END;
                            }

    { 314 ;2   ;Field     ;
                SourceExpr=NewShortcutDimCode[4];
                CaptionClass=Text002;
                Visible=FALSE;
                OnValidate=BEGIN
                             TESTFIELD("Entry Type","Entry Type"::Transfer);
                             ValidateNewShortcutDimCode(4,NewShortcutDimCode[4]);
                           END;

                OnLookup=BEGIN
                           LookupNewShortcutDimCode(4,NewShortcutDimCode[4]);
                         END;
                          }

    { 304 ;2   ;Field     ;
                SourceExpr=ShortcutDimCode[5];
                CaptionClass='1,2,5';
                TableRelation="Dimension Value".Code WHERE (Global Dimension No.=CONST(5),
                                                            Dimension Value Type=CONST(Standard),
                                                            Blocked=CONST(No));
                Visible=FALSE;
                OnValidate=BEGIN
                             ValidateShortcutDimCode(5,ShortcutDimCode[5]);
                           END;
                            }

    { 316 ;2   ;Field     ;
                SourceExpr=NewShortcutDimCode[5];
                CaptionClass=Text003;
                Visible=FALSE;
                OnValidate=BEGIN
                             TESTFIELD("Entry Type","Entry Type"::Transfer);
                             ValidateNewShortcutDimCode(5,NewShortcutDimCode[5]);
                           END;

                OnLookup=BEGIN
                           LookupNewShortcutDimCode(5,NewShortcutDimCode[5]);
                         END;
                          }

    { 306 ;2   ;Field     ;
                SourceExpr=ShortcutDimCode[6];
                CaptionClass='1,2,6';
                TableRelation="Dimension Value".Code WHERE (Global Dimension No.=CONST(6),
                                                            Dimension Value Type=CONST(Standard),
                                                            Blocked=CONST(No));
                Visible=FALSE;
                OnValidate=BEGIN
                             ValidateShortcutDimCode(6,ShortcutDimCode[6]);
                           END;
                            }

    { 318 ;2   ;Field     ;
                SourceExpr=NewShortcutDimCode[6];
                CaptionClass=Text004;
                Visible=FALSE;
                OnValidate=BEGIN
                             TESTFIELD("Entry Type","Entry Type"::Transfer);
                             ValidateNewShortcutDimCode(6,NewShortcutDimCode[6]);
                           END;

                OnLookup=BEGIN
                           LookupNewShortcutDimCode(6,NewShortcutDimCode[6]);
                         END;
                          }

    { 308 ;2   ;Field     ;
                SourceExpr=ShortcutDimCode[7];
                CaptionClass='1,2,7';
                TableRelation="Dimension Value".Code WHERE (Global Dimension No.=CONST(7),
                                                            Dimension Value Type=CONST(Standard),
                                                            Blocked=CONST(No));
                Visible=FALSE;
                OnValidate=BEGIN
                             ValidateShortcutDimCode(7,ShortcutDimCode[7]);
                           END;
                            }

    { 320 ;2   ;Field     ;
                SourceExpr=NewShortcutDimCode[7];
                CaptionClass=Text005;
                Visible=FALSE;
                OnValidate=BEGIN
                             TESTFIELD("Entry Type","Entry Type"::Transfer);
                             ValidateNewShortcutDimCode(7,NewShortcutDimCode[7]);
                           END;

                OnLookup=BEGIN
                           LookupNewShortcutDimCode(7,NewShortcutDimCode[7]);
                         END;
                          }

    { 310 ;2   ;Field     ;
                SourceExpr=ShortcutDimCode[8];
                CaptionClass='1,2,8';
                TableRelation="Dimension Value".Code WHERE (Global Dimension No.=CONST(8),
                                                            Dimension Value Type=CONST(Standard),
                                                            Blocked=CONST(No));
                Visible=FALSE;
                OnValidate=BEGIN
                             ValidateShortcutDimCode(8,ShortcutDimCode[8]);
                           END;
                            }

    { 322 ;2   ;Field     ;
                SourceExpr=NewShortcutDimCode[8];
                CaptionClass=Text006;
                Visible=FALSE;
                OnValidate=BEGIN
                             TESTFIELD("Entry Type","Entry Type"::Transfer);
                             ValidateNewShortcutDimCode(8,NewShortcutDimCode[8]);
                           END;

                OnLookup=BEGIN
                           LookupNewShortcutDimCode(8,NewShortcutDimCode[8]);
                         END;
                          }

    { 55  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the code for the inventory location where the item on the journal line will be registered.;
                SourceExpr="Location Code";
                Visible=TRUE;
                OnValidate=VAR
                             WMSManagement@1001 : Codeunit 7302;
                           BEGIN
                             WMSManagement.CheckItemJnlLineLocation(Rec,xRec);
                           END;
                            }

    { 66  ;2   ;Field     ;
                ToolTipML=ENU=Specifies a bin code for the item.;
                SourceExpr="Bin Code";
                Visible=FALSE }

    { 63  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the new location to link to the items on this journal line.;
                SourceExpr="New Location Code";
                Visible=FALSE;
                OnValidate=VAR
                             WMSManagement@1001 : Codeunit 7302;
                           BEGIN
                             WMSManagement.CheckItemJnlLineLocation(Rec,xRec);
                           END;
                            }

    { 70  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the new bin code to link to the items on this journal line.;
                SourceExpr="New Bin Code";
                Visible=FALSE }

    { 53  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the code for the salesperson or purchaser who is linked to the sale or purchase on the journal line.;
                SourceExpr="Salespers./Purch. Code";
                Visible=FALSE }

    { 73  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the code of the general business posting group that will be used when you post the entry on the item journal line.;
                SourceExpr="Gen. Bus. Posting Group";
                Visible=FALSE }

    { 75  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the code of the general product posting group that will be used for this item when you post the entry on the item journal line.;
                SourceExpr="Gen. Prod. Posting Group";
                Visible=FALSE }

    { 16  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the number of units of the item to be included on the journal line.;
                ApplicationArea=#Suite;
                SourceExpr=Quantity }

    { 81  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the code if you have filled in the Sales Unit of Measure field on the item card.;
                ApplicationArea=#Suite;
                SourceExpr="Unit of Measure Code" }

    { 18  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the price of one unit of the item on the journal line.;
                ApplicationArea=#Suite;
                SourceExpr="Unit Amount" }

    { 20  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the line's net amount.;
                ApplicationArea=#Suite;
                SourceExpr=Amount }

    { 51  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the item indirect cost.;
                SourceExpr="Indirect Cost %";
                Visible=FALSE }

    { 22  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the unit cost of the item on the line.;
                ApplicationArea=#Suite;
                SourceExpr="Unit Cost" }

    { 67  ;2   ;Field     ;
                ToolTipML=ENU=Specifies if the quantity in the item journal line should be applied to an already-posted document.;
                ApplicationArea=#Suite;
                SourceExpr="Applies-to Entry" }

    { 45  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the number for the transaction type, for the purpose of reporting to Intrastat.;
                SourceExpr="Transaction Type";
                Visible=FALSE }

    { 43  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the code for the transport method.;
                SourceExpr="Transport Method";
                Visible=FALSE }

    { 41  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the country/region code that applies to the journal line.;
                SourceExpr="Country/Region Code";
                Visible=FALSE }

    { 47  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the reason code that will be inserted on the journal lines.;
                SourceExpr="Reason Code";
                Visible=FALSE }

    { 24  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the last date on which the recurring journal will be posted, if you have indicated that the journal is recurring.;
                ApplicationArea=#Suite;
                SourceExpr="Expiration Date" }

    { 26  ;1   ;Group      }

    { 1902454601;2;Group  ;
                GroupType=FixedLayout }

    { 1900295801;3;Group  ;
                CaptionML=[ENU=Item Description;
                           PTG=Descri��o Produto] }

    { 27  ;4   ;Field     ;
                ToolTipML=ENU=Specifies a description of the item.;
                ApplicationArea=#Suite;
                SourceExpr=ItemDescription;
                Editable=FALSE;
                ShowCaption=No }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {
    VAR
      Text000@1000 : TextConst 'ENU=You cannot use entry type %1 in this journal.;PTG=N�o pode usar o tipo de movimento %1 neste di�rio.';
      Text001@1001 : TextConst 'ENU="1,2,3,New ";PTG=1,2,3,Novo';
      Text002@1002 : TextConst 'ENU="1,2,4,New ";PTG=1,2,4,Novo';
      Text003@1003 : TextConst 'ENU="1,2,5,New ";PTG=1,2,5,Novo';
      Text004@1004 : TextConst 'ENU="1,2,6,New ";PTG=1,2,6,Novo';
      Text005@1005 : TextConst 'ENU="1,2,7,New ";PTG=1,2,7,Novo';
      Text006@1006 : TextConst 'ENU="1,2,8,New ";PTG=1,2,8,Novo';
      ItemJnlMgt@1007 : Codeunit 240;
      ReportPrint@1008 : Codeunit 228;
      ItemAvailFormsMgt@1014 : Codeunit 353;
      CurrentJnlBatchName@1009 : Code[10];
      ItemDescription@1010 : Text[50];
      ShortcutDimCode@1011 : ARRAY [8] OF Code[20];
      NewShortcutDimCode@1012 : ARRAY [8] OF Code[20];

    LOCAL PROCEDURE CurrentJnlBatchNameOnAfterVali@19002411();
    BEGIN
      CurrPage.SAVERECORD;
      ItemJnlMgt.SetName(CurrentJnlBatchName,Rec);
      CurrPage.UPDATE(FALSE);
    END;

    BEGIN
    END.
  }
}

