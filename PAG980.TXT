OBJECT Page 980 Balancing Account Setup
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Balancing Account Setup;
               PTG=Configura��o Cta. Contrapartida];
    SourceTable=Table980;
    DataCaptionExpr=PageCaption;
    PageType=StandardDialog;
    OnOpenPage=BEGIN
                 GET(USERID);
                 PageCaption := '';
               END;

  }
  CONTROLS
  {
    { 1   ;    ;Container ;
                ContainerType=ContentArea }

    { 4   ;1   ;Group     ;
                GroupType=Group;
                InstructionalTextML=[ENU=Select the balance account that you want to register payments for.;
                                     PTG=Selecione a conta contrapartida onde desejar registar os pagamentos.] }

    { 3   ;2   ;Field     ;
                CaptionML=[ENU=Balancing Account;
                           PTG=Cta. Contrapartida];
                ToolTipML=ENU=Specifies the account number that is used as the balancing account for payments.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Bal. Account No." }

    { 2   ;2   ;Field     ;
                CaptionML=[ENU=Use this Account as Default;
                           PTG=Utilize esta Conta por defeito];
                ToolTipML=ENU=Specifies if the Date Received and the Amount Received fields are automatically filled when you select the Payment Made check box.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Use this Account as Def." }

  }
  CODE
  {
    VAR
      PageCaption@1000 : Text[10];

    BEGIN
    END.
  }
}

