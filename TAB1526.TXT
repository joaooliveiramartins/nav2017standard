OBJECT Table 1526 Workflow Record Change Archive
{
  OBJECT-PROPERTIES
  {
    Date=15/09/15;
    Time=12:00:00;
    Version List=NAVW19.00;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Workflow Record Change Archive;
               PTG=Alterar Arquivo Registo Workflow];
  }
  FIELDS
  {
    { 1   ;   ;Entry No.           ;Integer       ;AutoIncrement=Yes;
                                                   CaptionML=[ENU=Entry No.;
                                                              PTG=N� Movimento] }
    { 2   ;   ;Table No.           ;Integer       ;CaptionML=[ENU=Table No.;
                                                              PTG=N� Tabela] }
    { 3   ;   ;Field No.           ;Integer       ;CaptionML=[ENU=Field No.;
                                                              PTG=N� Campo] }
    { 4   ;   ;Old Value           ;Text250       ;CaptionML=[ENU=Old Value;
                                                              PTG=Valor Antigo] }
    { 5   ;   ;New Value           ;Text250       ;CaptionML=[ENU=New Value;
                                                              PTG=Novo Valor] }
    { 6   ;   ;Record ID           ;RecordID      ;CaptionML=[ENU=Record ID;
                                                              PTG=ID Registo] }
    { 7   ;   ;Workflow Step Instance ID;GUID     ;CaptionML=[ENU=Workflow Step Instance ID;
                                                              PTG=ID Inst�ncia Passo Workflow] }
    { 9   ;   ;Inactive            ;Boolean       ;CaptionML=[ENU=Inactive;
                                                              PTG=Inativo] }
  }
  KEYS
  {
    {    ;Entry No.                               ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    END.
  }
}

