OBJECT Report 31022983 Batch Settl. Posted POs
{
  OBJECT-PROPERTIES
  {
    Date=13/02/15;
    Time=13:00:00;
    Version List=NAVPTSS82.00;
  }
  PROPERTIES
  {
    Permissions=TableData 21=imd,
                TableData 25=imd,
                TableData 45=m,
                TableData 31022936=imd,
                TableData 31022937=imd,
                TableData 31022953=imd,
                TableData 31022954=imd;
    CaptionML=[ENU=Batch Settl. Posted POs;
               PTG=Liquida��o OP Registadas em Lote];
    ProcessingOnly=Yes;
    OnInitReport=BEGIN
                   PostingDate := WORKDATE;
                 END;

    OnPreReport=BEGIN
                  GLSetup.GET;
                END;

  }
  DATASET
  {
    { 8361;    ;DataItem;PostedPmtOrd        ;
               DataItemTable=Table31022953;
               DataItemTableView=SORTING(No.)
                                 ORDER(Ascending);
               OnPreDataItem=BEGIN
                               DocPost.CheckPostingDate(PostingDate);

                               SourceCodeSetup.GET;
                               SourceCode := SourceCodeSetup."Cartera Journal";

                               GroupAmountLCY := 0;
                               PmtOrdCount := 0;
                               DocCount := 0;
                               TotalDocCount := 0;
                               TotalPmtOrd := PostedPmtOrd.COUNT;
                               ExistVATEntry := FALSE;

                               Window.OPEN(
                                 Text31022890 +
                                 Text31022891 +
                                 Text31022892);
                             END;

               OnAfterGetRecord=BEGIN
                                  PmtOrdCount := PmtOrdCount + 1;
                                  Window.UPDATE(1,ROUND(PmtOrdCount / TotalPmtOrd * 10000,1));
                                  Window.UPDATE(2,STRSUBSTNO('%1',"No."));
                                  Window.UPDATE(3,0);
                                  GroupAmount := 0;
                                END;

               OnPostDataItem=BEGIN
                                Window.CLOSE;

                                COMMIT;

                                MESSAGE(
                                  Text31022893,
                                  TotalDocCount,PmtOrdCount,GroupAmountLCY);
                              END;
                               }

    { 2883;1   ;DataItem;PostedDoc           ;
               DataItemTable=Table31022936;
               DataItemTableView=SORTING(Bill Gr./Pmt. Order No.,Status,Category Code,Redrawn,Due Date)
                                 WHERE(Status=CONST(Open),
                                       Type=CONST(Payable));
               OnPreDataItem=BEGIN
                               SumLCYAmt := 0;
                               GenJnlLineNextNo := 0;
                               TotalDoc := PostedDoc.COUNT;
                               DocCount := 0;
                               ExistInvoice := FALSE;
                               Counter := COUNT;
                               IF (Counter > 1) AND GLSetup."Unrealized VAT" THEN BEGIN
                                 VATEntry.LOCKTABLE;
                                 IF VATEntry.FINDLAST THEN
                                   FromVATEntryNo := VATEntry."Entry No." + 1;
                               END;

                               FirstVATEntryNo := 0;
                               LastVATEntryNo := 0;
                             END;

               OnAfterGetRecord=BEGIN
                                  IsRedrawn := CarteraManagement.CheckFromRedrawnDoc(PostedDoc."No.");
                                  IF PostedDoc."Document Type" = PostedDoc."Document Type"::Invoice THEN
                                    ExistInvoice := TRUE;
                                  BankAcc.GET(PostedPmtOrd."Bank Account No.");
                                  Delay := BankAcc."Delay for Notices";

                                  IF DueOnly AND (PostingDate < "Due Date" + Delay) THEN
                                    CurrReport.SKIP;

                                  TotalDocCount := TotalDocCount + 1;
                                  DocCount := DocCount + 1;
                                  Window.UPDATE(3,ROUND(DocCount / TotalDoc * 10000,1));
                                  Window.UPDATE(4,STRSUBSTNO('%1 %2',"Document Type","Document No."));
                                  CASE "Document Type" OF
                                    PostedDoc."Document Type"::Invoice,PostedDoc."Document Type"::"1":
                                      BEGIN
                                        WITH TempGenJnlLine DO BEGIN
                                          GenJnlLineNextNo := GenJnlLineNextNo + 10000;
                                          CLEAR(TempGenJnlLine);
                                          INIT;
                                          "Line No." := GenJnlLineNextNo;
                                          "Posting Date" := PostingDate;
                                          TempGenJnlLine."Document Date" := "Document Date";
                                          VALIDATE("Account Type","Account Type"::Vendor);
                                          VendLedgEntry.GET(PostedDoc."Entry No.");
                                          VALIDATE("Account No.",VendLedgEntry."Vendor No.");
                                          "Document Type" := "Document Type"::Payment;
                                          Description := COPYSTR(
                                            STRSUBSTNO(Text31022894,PostedDoc."Document No."),
                                            1,MAXSTRLEN(Description));
                                          "Document No." := PostedPmtOrd."No.";
                                          VALIDATE("Currency Code",PostedDoc."Currency Code");
                                          VALIDATE(Amount,PostedDoc."Remaining Amount");
                                          "Applies-to Doc. Type" := VendLedgEntry."Document Type";
                                          "Applies-to Doc. No." := VendLedgEntry."Document No.";
                                          "Applies-to Bill No." := VendLedgEntry."Bill No.";
                                          "Source Code" := SourceCode;
                                          "System-Created Entry" := TRUE;
                                          "Shortcut Dimension 1 Code" := VendLedgEntry."Global Dimension 1 Code";
                                          "Shortcut Dimension 2 Code" := VendLedgEntry."Global Dimension 2 Code";
                                          "Dimension Set ID" := VendLedgEntry."Dimension Set ID";
                                          INSERT;
                                          SumLCYAmt := SumLCYAmt + "Amount (LCY)";
                                        END;
                                        GroupAmount := GroupAmount + "Remaining Amount";
                                        GroupAmountLCY := GroupAmountLCY + "Remaining Amt. (LCY)";
                                        VendLedgEntry."Document Status" := VendLedgEntry."Document Status"::Honored;
                                        VendLedgEntry.MODIFY;
                                      END;
                                    PostedDoc."Document Type"::Bill:
                                      BEGIN
                                        WITH TempGenJnlLine DO BEGIN
                                          GenJnlLineNextNo := GenJnlLineNextNo + 10000;
                                          CLEAR(TempGenJnlLine);
                                          INIT;
                                          "Line No." := GenJnlLineNextNo;
                                          "Posting Date" := PostingDate;
                                          "Document Type" := "Document Type"::Payment;
                                          "Document No." := PostedPmtOrd."No.";
                                          VALIDATE("Account Type","Account Type"::Vendor);
                                          VendLedgEntry.GET(PostedDoc."Entry No.");

                                          IF GLSetup."Unrealized VAT" THEN BEGIN
                                            ExistsNoRealVAT := CarteraManagement.FindVendVATSetup(VATPostingSetup,VendLedgEntry);
                                          END;

                                          VALIDATE("Account No.",VendLedgEntry."Vendor No.");
                                          Description := COPYSTR(
                                            STRSUBSTNO(Text31022895,PostedDoc."Document No.",PostedDoc."No."),
                                            1,MAXSTRLEN(Description));
                                          VALIDATE("Currency Code",PostedDoc."Currency Code");
                                          VALIDATE(Amount,PostedDoc."Remaining Amount");
                                          "Applies-to Doc. Type" := VendLedgEntry."Document Type";
                                          "Applies-to Doc. No." := VendLedgEntry."Document No.";
                                          "Applies-to Bill No." := VendLedgEntry."Bill No.";
                                          "Source Code" := SourceCode;
                                          "System-Created Entry" := TRUE;
                                          "Shortcut Dimension 1 Code" := VendLedgEntry."Global Dimension 1 Code";
                                          "Shortcut Dimension 2 Code" := VendLedgEntry."Global Dimension 2 Code";
                                          "Dimension Set ID" := VendLedgEntry."Dimension Set ID";
                                          INSERT;
                                          SumLCYAmt := SumLCYAmt + "Amount (LCY)";
                                        END;
                                        IF GLSetup."Unrealized VAT" AND
                                           ExistsNoRealVAT AND
                                           (NOT IsRedrawn) THEN BEGIN
                                          VendLedgEntry.CALCFIELDS("Remaining Amount","Remaining Amt. (LCY)");
                                          CarteraManagement.VendUnrealizedVAT2(
                                            VendLedgEntry,
                                            VendLedgEntry."Remaining Amt. (LCY)",
                                            TempGenJnlLine,
                                            ExistVATEntry,
                                            FirstVATEntryNo,
                                            LastVATEntryNo,
                                            TempNoRealVATBuffer);

                                          IF TempNoRealVATBuffer.FINDSET THEN BEGIN
                                            REPEAT
                                              BEGIN
                                                InsertGenJournalLine(
                                                  TempGenJnlLine."Account Type"::"G/L Account",
                                                  TempNoRealVATBuffer.Account,
                                                  TempNoRealVATBuffer.Amount);
                                                InsertGenJournalLine(
                                                  TempGenJnlLine."Account Type"::"G/L Account",
                                                  TempNoRealVATBuffer."Balance Account",
                                                  -TempNoRealVATBuffer.Amount);
                                              END;
                                            UNTIL TempNoRealVATBuffer.NEXT = 0;
                                            TempNoRealVATBuffer.DELETEALL;
                                          END;

                                        END;

                                        GroupAmount := GroupAmount + "Remaining Amount";
                                        GroupAmountLCY := GroupAmountLCY + "Remaining Amt. (LCY)";
                                        VendLedgEntry."Document Status" := VendLedgEntry."Document Status"::Honored;
                                        VendLedgEntry.MODIFY;
                                      END;
                                  END;
                                  IF TempBGPOPostBuffer.GET('','',VendLedgEntry."Entry No.") THEN BEGIN
                                    TempBGPOPostBuffer.Amount := TempBGPOPostBuffer.Amount + "Remaining Amount";
                                    TempBGPOPostBuffer.MODIFY;
                                  END ELSE BEGIN
                                    TempBGPOPostBuffer.INIT;
                                    TempBGPOPostBuffer."Entry No." := VendLedgEntry."Entry No.";
                                    TempBGPOPostBuffer.Amount := TempBGPOPostBuffer.Amount + "Remaining Amount";
                                    TempBGPOPostBuffer.INSERT;
                                  END;
                                END;

               OnPostDataItem=VAR
                                VendLedgEntry2@1110001 : Record 25;
                              BEGIN
                                IF (DocCount = 0) OR (GroupAmount = 0) THEN BEGIN
                                  IF DueOnly THEN
                                    ERROR(
                                      Text31022896 +
                                      Text31022897,
                                      PostedPmtOrd."No.")
                                  ELSE
                                    ERROR(
                                      Text31022896 +
                                      Text31022898,
                                      PostedPmtOrd."No.");
                                END;


                                WITH TempGenJnlLine DO BEGIN
                                  IF TempBGPOPostBuffer.FINDSET THEN BEGIN
                                    REPEAT
                                      VendLedgEntry2.GET(TempBGPOPostBuffer."Entry No.");
                                      GenJnlLineNextNo := GenJnlLineNextNo + 10000;
                                      CLEAR(TempGenJnlLine);
                                      INIT;
                                      "Line No." := GenJnlLineNextNo;
                                      "Posting Date" := PostingDate;
                                      "Document Type" := "Document Type"::Payment;
                                      "Document No." := PostedPmtOrd."No.";
                                      VALIDATE("Account Type","Account Type"::"Bank Account");
                                      VALIDATE("Account No.",BankAcc."No.");
                                      Description := COPYSTR(STRSUBSTNO(Text31022899,PostedPmtOrd."No."),1,MAXSTRLEN(Description));
                                      VALIDATE("Currency Code",PostedPmtOrd."Currency Code");
                                      VALIDATE(Amount,-TempBGPOPostBuffer.Amount);
                                      "Shortcut Dimension 1 Code" := TempBGPOPostBuffer."Global Dimension 1 Code";
                                      "Shortcut Dimension 2 Code" := TempBGPOPostBuffer."Global Dimension 2 Code";
                                      "Dimension Set ID" := VendLedgEntry2."Dimension Set ID";
                                      "Source Code" := SourceCode;
                                      "System-Created Entry" := TRUE;
                                      INSERT;
                                      SumLCYAmt := SumLCYAmt + "Amount (LCY)";
                                    UNTIL TempBGPOPostBuffer.NEXT <= 0;
                                  END;
                                  TempBGPOPostBuffer.DELETEALL;
                                END;

                                IF PostedPmtOrd."Currency Code" <> '' THEN BEGIN
                                  IF SumLCYAmt <> 0 THEN BEGIN
                                    Currency.GET(PostedPmtOrd."Currency Code");
                                    IF SumLCYAmt > 0 THEN BEGIN
                                      Currency.TESTFIELD("Residual Gains Account");
                                      Acct := Currency."Residual Gains Account";
                                    END ELSE BEGIN
                                      Currency.TESTFIELD("Residual Losses Account");
                                      Acct := Currency."Residual Losses Account";
                                    END;
                                    GenJnlLineNextNo := GenJnlLineNextNo + 10000;
                                    WITH TempGenJnlLine DO BEGIN
                                      CLEAR(TempGenJnlLine);
                                      INIT;
                                      "Line No." := GenJnlLineNextNo;
                                      "Posting Date" := PostingDate;
                                      "Document Type" := "Document Type"::Payment;
                                      "Document No." := PostedPmtOrd."No.";
                                      VALIDATE("Account Type","Account Type"::"G/L Account");
                                      VALIDATE("Account No.",Acct);
                                      Description := Text31022900;
                                      VALIDATE("Currency Code",'');
                                      VALIDATE(Amount,-SumLCYAmt);
                                      "Shortcut Dimension 1 Code" := BankAcc."Global Dimension 1 Code";
                                      "Shortcut Dimension 2 Code" := BankAcc."Global Dimension 2 Code";
                                      "Dimension Set ID" := VendLedgEntry."Dimension Set ID";
                                      "Source Code" := SourceCode;
                                      "System-Created Entry" := TRUE;
                                      INSERT;
                                    END;
                                  END;
                                END;

                                IF TempGenJnlLine.FINDSET THEN
                                  REPEAT
                                    TempGenJnlLine2 := TempGenJnlLine;
                                    GenJnlPostLine.SetFromSettlement(TRUE);
                                    GenJnlPostLine.RunWithCheck(TempGenJnlLine2);
                                  UNTIL TempGenJnlLine.NEXT = 0;
                                TempGenJnlLine.DELETEALL;
                                CLEAR(GenJnlPostLine);

                                DocPost.ClosePmtOrdIfEmpty(PostedPmtOrd,PostingDate);

                                IF (Counter > 1) AND GLSetup."Unrealized VAT" AND
                                  ExistVATEntry AND ExistInvoice THEN BEGIN
                                  IF VATEntry.FINDLAST THEN
                                    ToVATEntryNo := VATEntry."Entry No.";
                                  GLReg.FINDLAST;
                                  GLReg."From VAT Entry No." := FromVATEntryNo;
                                  GLReg."To VAT Entry No." := ToVATEntryNo;
                                  GLReg.MODIFY;
                                END ELSE BEGIN
                                  IF ExistVATEntry THEN BEGIN
                                    GLReg.FINDLAST;
                                    GLReg."From VAT Entry No." := FirstVATEntryNo;
                                    GLReg."To VAT Entry No." := LastVATEntryNo;
                                    GLReg.MODIFY;
                                  END;
                                END;
                              END;

               DataItemLink=Bill Gr./Pmt. Order No.=FIELD(No.) }

  }
  REQUESTPAGE
  {
    PROPERTIES
    {
      SaveValues=Yes;
    }
    CONTROLS
    {
      { 1900000001;0;Container;
                  ContainerType=ContentArea }

      { 1900000002;1;Group  ;
                  CaptionML=[ENU=Options;
                             PTG=Op��es] }

      { 5   ;2   ;Field     ;
                  CaptionML=[ENU=Posting Date;
                             PTG=Data Registo];
                  NotBlank=Yes;
                  SourceExpr=PostingDate }

      { 6   ;2   ;Field     ;
                  CaptionML=[ENU=Due bills only;
                             PTG=Apenas Letras Vencidas];
                  SourceExpr=DueOnly }

    }
  }
  LABELS
  {
  }
  CODE
  {
    VAR
      Text31022890@1000 : TextConst 'ENU=Settling           @1@@@@@@@@@@@@@@@@@@@@@@@\\;PTG=A Liquidar           @1@@@@@@@@@@@@@@@@@@@@@@@\\';
      Text31022891@1001 : TextConst 'ENU=Payment Order No.  #2######  @3@@@@@@@@@@@@@\;PTG=Ordem Pagamento N�  #2######  @3@@@@@@@@@@@@@\';
      Text31022892@1002 : TextConst 'ENU=Payable document   #4######;PTG=Documento a receber   #4######';
      Text31022893@1003 : TextConst 'ENU=%1 Documents in %2 Payment Orders totaling %3 (LCY) have been settled.;PTG=%1 Documentos em %2 Ordens Pagamento que totalizam %3 (DL) foram liquidados.';
      Text31022894@1004 : TextConst 'ENU=Payable document settlement %1;PTG=Liquida��o de documento a pagar %1';
      Text31022895@1005 : TextConst 'ENU=Payable bill settlement %1/%2;PTG=Letras a pagar liquidadas %1/%2';
      Text31022896@1006 : TextConst 'ENU=No payable documents have been found that can be settled in Posted Payment Order No. %1.;PTG=N�o foram encontrados documentos a pagar que possam ser liquidados na Ordem Pagamento N� %1.';
      Text31022897@1007 : TextConst 'ENU=Please check that the selection is not empty and at least one payable document is open and due.;PTG=Por favor verifique que a sele��o n�o est� vazia e que pelo menos um documento a pagar est� aberto e vencido.';
      Text31022898@1008 : TextConst 'ENU=Please check that the selection is not empty and at least one payable document is open.;PTG=Por favor verifique que a sele��o n�o est� vazia e que pelo menos um documento a pagar est� aberto.';
      Text31022899@1009 : TextConst 'ENU=Payment order settlement %1;PTG=Liquida��o Ordem Pagamento %1';
      Text31022900@1010 : TextConst 'ENU=Residual adjust generated by rounding Amount;PTG=Ajuste residual gerado pelo arredondamento do Valor';
      Text31022901@1011 : TextConst 'ENU=Payable document settlement %1/%2;PTG=Liquida��o de documento a pagar %1/%2';
      SourceCodeSetup@1110000 : Record 242;
      TempGenJnlLine@1110001 : TEMPORARY Record 81;
      TempGenJnlLine2@1110002 : TEMPORARY Record 81;
      VendLedgEntry@1110003 : Record 25;
      BankAccPostingGr@1110004 : Record 277;
      BankAcc@1110005 : Record 270;
      CurrExchRate@1110006 : Record 330;
      Currency@1110007 : Record 4;
      GLReg@1110008 : Record 45;
      GLSetup@1110009 : Record 98;
      VATPostingSetup@1110010 : Record 325;
      VATEntry@1110011 : Record 254;
      DocPost@1110012 : Codeunit 31022906;
      CarteraManagement@1110013 : Codeunit 31022901;
      GenJnlPostLine@1110014 : Codeunit 12;
      Window@1110015 : Dialog;
      PostingDate@1110016 : Date;
      DueOnly@1110017 : Boolean;
      Delay@1110018 : Decimal;
      SourceCode@1110019 : Code[10];
      Acct@1110020 : Code[20];
      DocCount@1110021 : Integer;
      TotalDocCount@1110022 : Integer;
      GroupAmount@1110023 : Decimal;
      GroupAmountLCY@1110024 : Decimal;
      GenJnlLineNextNo@1110025 : Integer;
      SumLCYAmt@1110026 : Decimal;
      TotalDisctdAmt@1110027 : Decimal;
      PmtOrdCount@1110028 : Integer;
      TotalPmtOrd@1110029 : Integer;
      TotalDoc@1110030 : Integer;
      ExistVATEntry@1110031 : Boolean;
      FirstVATEntryNo@1110032 : Integer;
      LastVATEntryNo@1110033 : Integer;
      IsRedrawn@1110034 : Boolean;
      ExistInvoice@1110035 : Boolean;
      FromVATEntryNo@1110036 : Integer;
      ToVATEntryNo@1110037 : Integer;
      Counter@1110038 : Integer;
      TempBGPOPostBuffer@1110039 : TEMPORARY Record 31022945;
      TempNoRealVATBuffer@1110042 : TEMPORARY Record 31022945;
      ExistsNoRealVAT@1110041 : Boolean;

    LOCAL PROCEDURE InsertGenJournalLine@4(AccType@1110000 : Integer;AccNo@1110001 : Code[20];Amount2@1110002 : Decimal);
    BEGIN
      GenJnlLineNextNo :=  GenJnlLineNextNo + 10000;

      WITH TempGenJnlLine DO BEGIN
        CLEAR(TempGenJnlLine);
        INIT;
        "Line No." := GenJnlLineNextNo;
        "Posting Date" := PostingDate;
        "Document Type" := "Document Type"::Payment;
        "Document No." := PostedPmtOrd."No.";
        "Account Type" := AccType;
        "Account No." := AccNo;
        IF PostedDoc."Document Type" = PostedDoc."Document Type"::Bill THEN
          Description := COPYSTR(
            STRSUBSTNO(Text31022901,PostedDoc."Document No.",PostedDoc."No."),
            1,MAXSTRLEN(Description))
        ELSE
          Description := COPYSTR(
            STRSUBSTNO(Text31022901,PostedDoc."Document No.",PostedDoc."No."),
            1,MAXSTRLEN(Description));
        VALIDATE("Currency Code",PostedDoc."Currency Code");
        VALIDATE(Amount,-Amount2);
        "Applies-to Doc. Type" := VendLedgEntry."Document Type";
        "Applies-to Doc. No." := '';
        "Applies-to Bill No." := VendLedgEntry."Bill No.";
        "Source Code" := SourceCode;
        "System-Created Entry" := TRUE;
        "Shortcut Dimension 1 Code" := VendLedgEntry."Global Dimension 1 Code";
        "Shortcut Dimension 2 Code" := VendLedgEntry."Global Dimension 2 Code";
        "Dimension Set ID" := VendLedgEntry."Dimension Set ID";
        INSERT;
      END;
    END;

    BEGIN
    END.
  }
  RDLDATA
  {
  }
}

