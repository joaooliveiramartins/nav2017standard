OBJECT Page 9080 Sales Hist. Sell-to FactBox
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Sell-to Customer Sales History;
               PTG=Hist�rico Vendas de Venda-a Cliente];
    SourceTable=Table18;
    PageType=CardPart;
    OnOpenPage=VAR
                 OfficeManagement@1000 : Codeunit 1630;
               BEGIN
                 RegularFastTabVisible := CURRENTCLIENTTYPE = CLIENTTYPE::Windows;
                 CuesVisible := (NOT RegularFastTabVisible) OR OfficeManagement.IsAvailable;
               END;

  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Field     ;
                CaptionML=[ENU=Customer No.;
                           PTG=N� Cliente];
                ToolTipML=[ENU=Specifies the number of the customer. The field is either filled automatically from a defined number series, or you enter the number manually because you have enabled manual number entry in the number-series setup.;
                           PTG=""];
                ApplicationArea=#All;
                SourceExpr="No.";
                Visible=ShowCustomerNo;
                OnDrillDown=BEGIN
                              ShowDetails;
                            END;
                             }

    { 23  ;1   ;Group     ;
                Visible=RegularFastTabVisible;
                GroupType=Group }

    { 3   ;2   ;Field     ;
                CaptionML=[ENU=Ongoing Sales Quotes;
                           PTG=Propostas de Venda Ativas];
                ToolTipML=[ENU=Specifies the number of sales quotes that have been registered for the customer.;
                           PTG=""];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="No. of Quotes";
                DrillDownPageID=Sales Quotes }

    { 5   ;2   ;Field     ;
                CaptionML=[ENU=Ongoing Sales Blanket Orders;
                           PTG=Encomendas de Venda Abertas Ativas];
                ToolTipML=[ENU=Specifies the number of sales blanket orders that have been registered for the customer.;
                           PTG=""];
                SourceExpr="No. of Blanket Orders";
                DrillDownPageID=Blanket Sales Orders }

    { 7   ;2   ;Field     ;
                CaptionML=[ENU=Ongoing Sales Orders;
                           PTG=Encomendas de Venda Ativas];
                ToolTipML=[ENU=Specifies the number of sales orders that have been registered for the customer.;
                           PTG=""];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="No. of Orders";
                DrillDownPageID=Sales Order List }

    { 9   ;2   ;Field     ;
                CaptionML=[ENU=Ongoing Sales Invoices;
                           PTG=Faturas de Venda Ativas];
                ToolTipML=[ENU=Specifies the number of unposted sales invoices that have been registered for the customer.;
                           PTG=""];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="No. of Invoices";
                DrillDownPageID=Sales Invoice List }

    { 11  ;2   ;Field     ;
                CaptionML=[ENU=Ongoing Sales Return Orders;
                           PTG=Devolu��es de Venda Ativas];
                ToolTipML=[ENU=Specifies the number of sales return orders that have been registered for the customer.;
                           PTG=""];
                SourceExpr="No. of Return Orders";
                DrillDownPageID=Sales Return Order List }

    { 13  ;2   ;Field     ;
                CaptionML=[ENU=Ongoing Sales Credit Memos;
                           PTG=Notas Cr�dito de Venda Ativas];
                ToolTipML=[ENU=Specifies the number of unposted sales credit memos that have been registered for the customer.;
                           PTG=""];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="No. of Credit Memos";
                DrillDownPageID=Sales Credit Memos }

    { 15  ;2   ;Field     ;
                CaptionML=[ENU=Posted Sales Shipments;
                           PTG=Envios de Venda Registados];
                ToolTipML=[ENU=Specifies the number of posted sales shipments that have been registered for the customer.;
                           PTG=""];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="No. of Pstd. Shipments";
                DrillDownPageID=Posted Sales Shipments }

    { 17  ;2   ;Field     ;
                CaptionML=[ENU=Posted Sales Invoices;
                           PTG=Faturas de Venda Registadas];
                ToolTipML=[ENU=Specifies the number of posted sales invoices that have been registered for the customer.;
                           PTG=""];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="No. of Pstd. Invoices";
                DrillDownPageID=Posted Sales Invoices }

    { 19  ;2   ;Field     ;
                CaptionML=[ENU=Posted Sales Return Receipts;
                           PTG=Rece��es Devolu��o de Venda Registadas];
                ToolTipML=[ENU=Specifies the number of posted sales return receipts that have been registered for the customer.;
                           PTG=""];
                SourceExpr="No. of Pstd. Return Receipts";
                DrillDownPageID=Posted Return Receipts }

    { 21  ;2   ;Field     ;
                CaptionML=[ENU=Posted Sales Credit Memos;
                           PTG=Notas de Cr�dito de Venda Registadas];
                ToolTipML=[ENU=Specifies the number of posted sales credit memos that have been registered for the customer.;
                           PTG=""];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="No. of Pstd. Credit Memos";
                DrillDownPageID=Posted Sales Credit Memos }

    { 2   ;1   ;Group     ;
                Visible=CuesVisible;
                GroupType=CueGroup }

    { 22  ;2   ;Field     ;
                Name=NoofQuotesTile;
                CaptionML=[ENU=Ongoing Sales Quotes;
                           PTG=Propostas de Venda Em Curso];
                ToolTipML=[ENU=Specifies the number of sales quotes that have been registered for the customer.;
                           PTG=""];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="No. of Quotes";
                DrillDownPageID=Sales Quotes }

    { 20  ;2   ;Field     ;
                Name=NoofBlanketOrdersTile;
                CaptionML=[ENU=Ongoing Sales Blanket Orders;
                           PTG=Encomendas de Venda Abertas em Curso];
                ToolTipML=[ENU=Specifies the number of sales blanket orders that have been registered for the customer.;
                           PTG=""];
                SourceExpr="No. of Blanket Orders";
                DrillDownPageID=Blanket Sales Orders }

    { 18  ;2   ;Field     ;
                Name=NoofOrdersTile;
                CaptionML=[ENU=Ongoing Sales Orders;
                           PTG=Encomendas de Venda Em Curso];
                ToolTipML=[ENU=Specifies the number of sales orders that have been registered for the customer.;
                           PTG=""];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="No. of Orders";
                DrillDownPageID=Sales Order List }

    { 16  ;2   ;Field     ;
                Name=NoofInvoicesTile;
                CaptionML=[ENU=Ongoing Sales Invoices;
                           PTG=Faturas de Venda Em Curso];
                ToolTipML=[ENU=Specifies the number of unposted sales invoices that have been registered for the customer.;
                           PTG=""];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="No. of Invoices";
                DrillDownPageID=Sales Invoice List }

    { 14  ;2   ;Field     ;
                Name=NoofReturnOrdersTile;
                CaptionML=[ENU=Ongoing Sales Return Orders;
                           PTG=Devolu��es de Venda Em Curso];
                ToolTipML=[ENU=Specifies the number of sales return orders that have been registered for the customer.;
                           PTG=""];
                SourceExpr="No. of Return Orders";
                DrillDownPageID=Sales Return Order List }

    { 12  ;2   ;Field     ;
                Name=NoofCreditMemosTile;
                CaptionML=[ENU=Ongoing Sales Credit Memos;
                           PTG=Notas de Cr�dito de Venda Em Curso];
                ToolTipML=[ENU=Specifies the number of unposted sales credit memos that have been registered for the customer.;
                           PTG=""];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="No. of Credit Memos";
                DrillDownPageID=Sales Credit Memos }

    { 10  ;2   ;Field     ;
                Name=NoofPstdShipmentsTile;
                CaptionML=[ENU=Posted Sales Shipments;
                           PTG=Guias de Remessa Registadas];
                ToolTipML=[ENU=Specifies the number of posted sales shipments that have been registered for the customer.;
                           PTG=""];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="No. of Pstd. Shipments";
                DrillDownPageID=Posted Sales Shipments }

    { 8   ;2   ;Field     ;
                Name=NoofPstdInvoicesTile;
                CaptionML=[ENU=Posted Sales Invoices;
                           PTG=Faturas de Venda Registadas];
                ToolTipML=[ENU=Specifies the number of posted sales invoices that have been registered for the customer.;
                           PTG=""];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="No. of Pstd. Invoices";
                DrillDownPageID=Posted Sales Invoices }

    { 6   ;2   ;Field     ;
                Name=NoofPstdReturnReceiptsTile;
                CaptionML=[ENU=Posted Sales Return Receipts;
                           PTG=Recebimentos Devolu��o de Venda Registados];
                ToolTipML=[ENU=Specifies the number of posted sales return receipts that have been registered for the customer.;
                           PTG=""];
                SourceExpr="No. of Pstd. Return Receipts";
                DrillDownPageID=Posted Return Receipts }

    { 4   ;2   ;Field     ;
                Name=NoofPstdCreditMemosTile;
                CaptionML=[ENU=Posted Sales Credit Memos;
                           PTG=Notas de Cr�dito de Venda Registadas];
                ToolTipML=[ENU=Specifies the number of posted sales credit memos that have been registered for the customer.;
                           PTG=""];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="No. of Pstd. Credit Memos";
                DrillDownPageID=Posted Sales Credit Memos }

  }
  CODE
  {
    VAR
      ShowCustomerNo@1000 : Boolean;
      RegularFastTabVisible@1002 : Boolean;
      CuesVisible@1001 : Boolean;

    LOCAL PROCEDURE ShowDetails@1102601000();
    BEGIN
      PAGE.RUN(PAGE::"Customer Card",Rec);
    END;

    PROCEDURE SetCustomerNoVisibility@1(Visible@1000 : Boolean);
    BEGIN
      ShowCustomerNo := Visible;
    END;

    BEGIN
    END.
  }
}

