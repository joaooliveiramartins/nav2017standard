OBJECT Table 1703 Deferral Post. Buffer
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Deferral Post. Buffer;
               PTG=Mem. Int. Diferimento];
  }
  FIELDS
  {
    { 1   ;   ;Type                ;Option        ;CaptionML=[ENU=Type;
                                                              PTG=Tipo];
                                                   OptionCaptionML=[ENU=Prepmt. Exch. Rate Difference,G/L Account,Item,Resource,Fixed Asset;
                                                                    PTG=Dif. Tx.C�mbio Pr�-Pagto,Conta C/G,Produto,Recurso,Imobilizado];
                                                   OptionString=Prepmt. Exch. Rate Difference,G/L Account,Item,Resource,Fixed Asset }
    { 2   ;   ;G/L Account         ;Code20        ;TableRelation="G/L Account" WHERE (Account Type=CONST(Posting),
                                                                                      Blocked=CONST(No));
                                                   CaptionML=[ENU=G/L Account;
                                                              PTG=Conta C/G];
                                                   NotBlank=Yes }
    { 3   ;   ;Gen. Bus. Posting Group;Code10     ;TableRelation="Gen. Business Posting Group";
                                                   CaptionML=[ENU=Gen. Bus. Posting Group;
                                                              PTG=Gr. Contabil�stico Neg�cio] }
    { 4   ;   ;Gen. Prod. Posting Group;Code10    ;TableRelation="Gen. Product Posting Group";
                                                   CaptionML=[ENU=Gen. Prod. Posting Group;
                                                              PTG=Gr. Contabil�stico Produto] }
    { 5   ;   ;VAT Bus. Posting Group;Code10      ;TableRelation="VAT Business Posting Group";
                                                   CaptionML=[ENU=VAT Bus. Posting Group;
                                                              PTG=Gr. Registo IVA Neg�cio] }
    { 6   ;   ;VAT Prod. Posting Group;Code10     ;TableRelation="VAT Product Posting Group";
                                                   CaptionML=[ENU=VAT Prod. Posting Group;
                                                              PTG=Gr. Registo IVA Produto] }
    { 7   ;   ;Tax Area Code       ;Code20        ;TableRelation="Tax Area";
                                                   CaptionML=[ENU=Tax Area Code;
                                                              PTG=C�d. �rea Imposto] }
    { 8   ;   ;Tax Group Code      ;Code10        ;TableRelation="Tax Group";
                                                   CaptionML=[ENU=Tax Group Code;
                                                              PTG=C�d. Grupo Imposto] }
    { 9   ;   ;Tax Liable          ;Boolean       ;CaptionML=[ENU=Tax Liable;
                                                              PTG=Sujeito a Imposto] }
    { 10  ;   ;Use Tax             ;Boolean       ;CaptionML=[ENU=Use Tax;
                                                              PTG=Usar Taxa] }
    { 11  ;   ;Job No.             ;Code20        ;TableRelation=Job;
                                                   CaptionML=[ENU=Job No.;
                                                              PTG=N� Projeto] }
    { 12  ;   ;Posting Date        ;Date          ;CaptionML=[ENU=Posting Date;
                                                              PTG=Data Registo] }
    { 13  ;   ;Amount              ;Decimal       ;CaptionML=[ENU=Amount;
                                                              PTG=Valor] }
    { 14  ;   ;Amount (LCY)        ;Decimal       ;CaptionML=[ENU=Amount (LCY);
                                                              PTG=Valor (DL)] }
    { 15  ;   ;System-Created Entry;Boolean       ;CaptionML=[ENU=System-Created Entry;
                                                              PTG=Lan�amento Autom�tico] }
    { 16  ;   ;Global Dimension 1 Code;Code20     ;TableRelation="Dimension Value".Code WHERE (Global Dimension No.=CONST(1));
                                                   CaptionML=[ENU=Global Dimension 1 Code;
                                                              PTG=C�d. Dimens�o 1 Global];
                                                   CaptionClass='1,1,1' }
    { 17  ;   ;Global Dimension 2 Code;Code20     ;TableRelation="Dimension Value".Code WHERE (Global Dimension No.=CONST(2));
                                                   CaptionML=[ENU=Global Dimension 2 Code;
                                                              PTG=C�d. Dimens�o 2 Global];
                                                   CaptionClass='1,1,2' }
    { 18  ;   ;Description         ;Text50        ;CaptionML=[ENU=Description;
                                                              PTG=Descri��o] }
    { 19  ;   ;Deferral Account    ;Code20        ;CaptionML=[ENU=Deferral Account;
                                                              PTG=Conta Diferimento] }
    { 20  ;   ;Period Description  ;Text50        ;CaptionML=[ENU=Period Description;
                                                              PTG=Descri��o Per�odo] }
    { 21  ;   ;Deferral Doc. Type  ;Option        ;CaptionML=[ENU=Deferral Doc. Type;
                                                              PTG=Tipo Documento Diferimento];
                                                   OptionCaptionML=[ENU=Purchase,Sales,G/L;
                                                                    PTG=Compra,Venda,C/G];
                                                   OptionString=Purchase,Sales,G/L }
    { 22  ;   ;Document No.        ;Code20        ;CaptionML=[ENU=Document No.;
                                                              PTG=N� Documento] }
    { 24  ;   ;Sales/Purch Amount  ;Decimal       ;CaptionML=[ENU=Sales/Purch Amount;
                                                              PTG=Valor Venda/Compra] }
    { 25  ;   ;Sales/Purch Amount (LCY);Decimal   ;CaptionML=[ENU=Sales/Purch Amount (LCY);
                                                              PTG=Valor Venda/Compra (DL)] }
    { 26  ;   ;Gen. Posting Type   ;Option        ;CaptionML=[ENU=Gen. Posting Type;
                                                              PTG=Tipo Grupo Contabil�stico];
                                                   OptionCaptionML=[ENU=" ,Purchase,Sale,Settlement";
                                                                    PTG=" ,Compra,Venda,Acordo"];
                                                   OptionString=[ ,Purchase,Sale,Settlement] }
    { 27  ;   ;Partial Deferral    ;Boolean       ;CaptionML=[ENU=Partial Deferral;
                                                              PTG=Adiamento Parcial] }
    { 480 ;   ;Dimension Set ID    ;Integer       ;TableRelation="Dimension Set Entry";
                                                   CaptionML=[ENU=Dimension Set ID;
                                                              PTG=ID Combina��o Dimens�o];
                                                   Editable=No }
    { 1700;   ;Deferral Code       ;Code10        ;TableRelation="Deferral Template"."Deferral Code";
                                                   CaptionML=[ENU=Deferral Code;
                                                              PTG=C�digo Diferimento] }
    { 1701;   ;Deferral Line No.   ;Integer       ;CaptionML=[ENU=Deferral Line No.;
                                                              PTG=N� Linha Diferimento] }
  }
  KEYS
  {
    {    ;Type,G/L Account,Gen. Bus. Posting Group,Gen. Prod. Posting Group,VAT Bus. Posting Group,VAT Prod. Posting Group,Tax Area Code,Tax Group Code,Tax Liable,Use Tax,Dimension Set ID,Job No.,Deferral Code,Posting Date,Partial Deferral;
                                                   Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    PROCEDURE PrepareSales@1(SalesLine@1000 : Record 37;DocumentNo@1001 : Code[20]);
    VAR
      DeferralUtilities@1002 : Codeunit 1720;
    BEGIN
      CLEAR(Rec);
      Type := SalesLine.Type;
      "System-Created Entry" := TRUE;
      "Gen. Bus. Posting Group" := SalesLine."Gen. Bus. Posting Group";
      "Gen. Prod. Posting Group" := SalesLine."Gen. Prod. Posting Group";
      "VAT Bus. Posting Group" := SalesLine."VAT Bus. Posting Group";
      "VAT Prod. Posting Group" := SalesLine."VAT Prod. Posting Group";
      "Global Dimension 1 Code" := SalesLine."Shortcut Dimension 1 Code";
      "Global Dimension 2 Code" := SalesLine."Shortcut Dimension 2 Code";
      "Dimension Set ID" := SalesLine."Dimension Set ID";
      "Job No." := SalesLine."Job No.";
      "Gen. Posting Type" := "Gen. Posting Type"::Sale;

      IF SalesLine."VAT Calculation Type" = SalesLine."VAT Calculation Type"::"Sales Tax" THEN BEGIN
        "Tax Area Code" := SalesLine."Tax Area Code";
        "Tax Group Code" := SalesLine."Tax Group Code";
        "Tax Liable" := SalesLine."Tax Liable";
        "Use Tax" := FALSE;
      END;
      "Deferral Code" := SalesLine."Deferral Code";
      "Deferral Doc. Type" := DeferralUtilities.GetSalesDeferralDocType;
      "Document No." := DocumentNo;
    END;

    PROCEDURE ReverseAmounts@2();
    BEGIN
      Amount := -Amount;
      "Amount (LCY)" := -"Amount (LCY)";
      "Sales/Purch Amount" := -"Sales/Purch Amount";
      "Sales/Purch Amount (LCY)" := -"Sales/Purch Amount (LCY)";
    END;

    PROCEDURE PreparePurch@3(PurchLine@1000 : Record 39;DocumentNo@1001 : Code[20]);
    VAR
      DeferralUtilities@1002 : Codeunit 1720;
    BEGIN
      CLEAR(Rec);
      Type := PurchLine.Type;
      "System-Created Entry" := TRUE;
      "Gen. Bus. Posting Group" := PurchLine."Gen. Bus. Posting Group";
      "Gen. Prod. Posting Group" := PurchLine."Gen. Prod. Posting Group";
      "VAT Bus. Posting Group" := PurchLine."VAT Bus. Posting Group";
      "VAT Prod. Posting Group" := PurchLine."VAT Prod. Posting Group";
      "Global Dimension 1 Code" := PurchLine."Shortcut Dimension 1 Code";
      "Global Dimension 2 Code" := PurchLine."Shortcut Dimension 2 Code";
      "Dimension Set ID" := PurchLine."Dimension Set ID";
      "Job No." := PurchLine."Job No.";
      "Gen. Posting Type" := "Gen. Posting Type"::Purchase;

      IF PurchLine."VAT Calculation Type" = PurchLine."VAT Calculation Type"::"Sales Tax" THEN BEGIN
        "Tax Area Code" := PurchLine."Tax Area Code";
        "Tax Group Code" := PurchLine."Tax Group Code";
        "Tax Liable" := PurchLine."Tax Liable";
        "Use Tax" := FALSE;
      END;
      "Deferral Code" := PurchLine."Deferral Code";
      "Deferral Doc. Type" := DeferralUtilities.GetPurchDeferralDocType;
      "Document No." := DocumentNo;
    END;

    BEGIN
    END.
  }
}

