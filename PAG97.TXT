OBJECT Page 97 Purchase Quote Subform
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Lines;
               PTG=Linhas];
    MultipleNewLines=Yes;
    LinksAllowed=No;
    SourceTable=Table39;
    DelayedInsert=Yes;
    SourceTableView=WHERE(Document Type=FILTER(Quote));
    PageType=ListPart;
    AutoSplitKey=Yes;
    OnAfterGetRecord=BEGIN
                       ShowShortcutDimCode(ShortcutDimCode);
                       CLEAR(DocumentTotals);
                     END;

    OnNewRecord=BEGIN
                  InitType;
                  CLEAR(ShortcutDimCode);
                END;

    OnDeleteRecord=VAR
                     ReservePurchLine@1000 : Codeunit 99000834;
                   BEGIN
                     IF (Quantity <> 0) AND ItemExists("No.") THEN BEGIN
                       COMMIT;
                       IF NOT ReservePurchLine.DeleteLineConfirm(Rec) THEN
                         EXIT(FALSE);
                       ReservePurchLine.DeleteLine(Rec);
                     END;
                   END;

    OnAfterGetCurrRecord=BEGIN
                           UpdateEditableOnRow;
                           IF PurchHeader.GET("Document Type","Document No.") THEN;

                           DocumentTotals.PurchaseUpdateTotalsControls(Rec,TotalPurchaseHeader,TotalPurchaseLine,RefreshMessageEnabled,
                             TotalAmountStyle,RefreshMessageText,InvDiscAmountEditable,VATAmount);
                         END;

    ActionList=ACTIONS
    {
      { 1900000004;0 ;ActionContainer;
                      ActionContainerType=ActionItems }
      { 1906587504;1 ;ActionGroup;
                      CaptionML=[ENU=F&unctions;
                                 PTG=F&un��es];
                      Image=Action }
      { 1901991404;2 ;Action    ;
                      AccessByPermission=TableData 90=R;
                      CaptionML=[ENU=E&xplode BOM;
                                 PTG=E&xpandir L.M.];
                      Image=ExplodeBOM;
                      OnAction=BEGIN
                                 ExplodeBOM;
                               END;
                                }
      { 1900295904;2 ;Action    ;
                      AccessByPermission=TableData 279=R;
                      CaptionML=[ENU=Insert &Ext. Texts;
                                 PTG=Inserir T&extos Adicionais];
                      Image=Text;
                      OnAction=BEGIN
                                 InsertExtendedText(TRUE);
                               END;
                                }
      { 1907935204;1 ;ActionGroup;
                      CaptionML=[ENU=&Line;
                                 PTG=&Linha];
                      Image=Line }
      { 1903134404;2 ;ActionGroup;
                      CaptionML=[ENU=Item Availability by;
                                 PTG=Disponibilidade Produto por];
                      Image=ItemAvailability }
      { 5       ;3   ;Action    ;
                      CaptionML=[ENU=Event;
                                 PTG=Ocorr�ncia];
                      Image=Event;
                      OnAction=BEGIN
                                 ItemAvailFormsMgt.ShowItemAvailFromPurchLine(Rec,ItemAvailFormsMgt.ByEvent)
                               END;
                                }
      { 1900609704;3 ;Action    ;
                      CaptionML=[ENU=Period;
                                 PTG=Per�odo];
                      Image=Period;
                      OnAction=BEGIN
                                 ItemAvailFormsMgt.ShowItemAvailFromPurchLine(Rec,ItemAvailFormsMgt.ByPeriod)
                               END;
                                }
      { 1904945204;3 ;Action    ;
                      CaptionML=[ENU=Variant;
                                 PTG=Variante];
                      Image=ItemVariant;
                      OnAction=BEGIN
                                 ItemAvailFormsMgt.ShowItemAvailFromPurchLine(Rec,ItemAvailFormsMgt.ByVariant)
                               END;
                                }
      { 1902740304;3 ;Action    ;
                      AccessByPermission=TableData 14=R;
                      CaptionML=[ENU=Location;
                                 PTG=Localiza��o];
                      Image=Warehouse;
                      OnAction=BEGIN
                                 ItemAvailFormsMgt.ShowItemAvailFromPurchLine(Rec,ItemAvailFormsMgt.ByLocation)
                               END;
                                }
      { 3       ;3   ;Action    ;
                      CaptionML=[ENU=BOM Level;
                                 PTG=N�vel L.M.];
                      Image=BOMLevel;
                      OnAction=BEGIN
                                 ItemAvailFormsMgt.ShowItemAvailFromPurchLine(Rec,ItemAvailFormsMgt.ByBOM)
                               END;
                                }
      { 1901033504;2 ;Action    ;
                      AccessByPermission=TableData 348=R;
                      ShortCutKey=Shift+Ctrl+D;
                      CaptionML=[ENU=Dimensions;
                                 PTG=Dimens�es];
                      ToolTipML=ENU=View or edit dimensions, such as area, project, or department, that you can assign to sales and purchase documents to distribute costs and analyze transaction history.;
                      Image=Dimensions;
                      OnAction=BEGIN
                                 ShowDimensions;
                               END;
                                }
      { 1901288104;2 ;Action    ;
                      CaptionML=[ENU=Co&mments;
                                 PTG=Co&ment�rios];
                      Image=ViewComments;
                      OnAction=BEGIN
                                 ShowLineComments;
                               END;
                                }
      { 1907184504;2 ;Action    ;
                      AccessByPermission=TableData 5800=R;
                      CaptionML=[ENU=Item Charge &Assignment;
                                 PTG=&Atribui��o Encargo Produto];
                      Image=ItemCosts;
                      OnAction=BEGIN
                                 ItemChargeAssgnt;
                               END;
                                }
      { 1905987604;2 ;Action    ;
                      ShortCutKey=Shift+Ctrl+I;
                      CaptionML=[ENU=Item &Tracking Lines;
                                 PTG=Linhas Ras&treio Produto];
                      Image=ItemTrackingLines;
                      OnAction=BEGIN
                                 OpenItemTrackingLines;
                               END;
                                }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 2   ;2   ;Field     ;
                ToolTipML=ENU=Specifies the line type.;
                SourceExpr=Type;
                OnValidate=BEGIN
                             NoOnAfterValidate;
                             IF xRec."No." <> '' THEN
                               RedistributeTotalsOnAfterValidate;
                           END;
                            }

    { 4   ;2   ;Field     ;
                ToolTipML=ENU=Specifies the number of a general ledger account, item, additional cost, or fixed asset, depending on what you selected in the Type field.;
                SourceExpr="No.";
                OnValidate=BEGIN
                             ShowShortcutDimCode(ShortcutDimCode);
                             NoOnAfterValidate;

                             IF xRec."No." <> '' THEN
                               RedistributeTotalsOnAfterValidate;
                           END;
                            }

    { 36  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the cross-reference number for this item.;
                SourceExpr="Cross-Reference No.";
                Visible=FALSE;
                OnValidate=BEGIN
                             CrossReferenceNoOnAfterValidat;
                             NoOnAfterValidate;
                           END;

                OnLookup=BEGIN
                           CrossReferenceNoLookUp;
                           InsertExtendedText(FALSE);
                           NoOnAfterValidate;
                         END;
                          }

    { 20  ;2   ;Field     ;
                ToolTipML=ENU=Specifies a variant code for the item.;
                SourceExpr="Variant Code";
                Visible=FALSE }

    { 38  ;2   ;Field     ;
                ToolTipML=ENU=Specifies that this item is a nonstock item.;
                SourceExpr=Nonstock;
                Visible=FALSE }

    { 14  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the code for the VAT product posting group of the item or general ledger account on this line.;
                SourceExpr="VAT Prod. Posting Group";
                Visible=FALSE;
                OnValidate=BEGIN
                             RedistributeTotalsOnAfterValidate;
                           END;
                            }

    { 6   ;2   ;Field     ;
                ToolTipML=ENU=Specifies a description of the entry, depending on what you chose in the Type field.;
                SourceExpr=Description;
                OnValidate=BEGIN
                             ShowShortcutDimCode(ShortcutDimCode);
                             NoOnAfterValidate;

                             IF xRec."No." <> '' THEN
                               RedistributeTotalsOnAfterValidate;
                           END;
                            }

    { 62  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the code for the location where the items on the line will be located.;
                SourceExpr="Location Code" }

    { 8   ;2   ;Field     ;
                ToolTipML=ENU=Specifies the number of units of the item that will be specified on the line.;
                BlankZero=Yes;
                SourceExpr=Quantity;
                OnValidate=BEGIN
                             RedistributeTotalsOnAfterValidate;
                           END;

                ShowMandatory="No." <> '' }

    { 24  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the unit of measure code that is valid for the purchase line.;
                SourceExpr="Unit of Measure Code";
                Enabled=UnitofMeasureCodeIsChangeable;
                Editable=UnitofMeasureCodeIsChangeable;
                OnValidate=BEGIN
                             RedistributeTotalsOnAfterValidate;
                           END;
                            }

    { 10  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the name of the unit of measure for the item, such as 1 bottle or 1 piece.;
                SourceExpr="Unit of Measure";
                Visible=FALSE;
                OnValidate=BEGIN
                             RedistributeTotalsOnAfterValidate;
                           END;
                            }

    { 12  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the direct cost of one item unit.;
                BlankZero=Yes;
                SourceExpr="Direct Unit Cost";
                OnValidate=BEGIN
                             RedistributeTotalsOnAfterValidate;
                           END;

                ShowMandatory="No." <> '' }

    { 58  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the item's indirect cost percentage.;
                SourceExpr="Indirect Cost %";
                Visible=FALSE;
                OnValidate=BEGIN
                             RedistributeTotalsOnAfterValidate;
                           END;
                            }

    { 56  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the unit cost of the item on the line.;
                SourceExpr="Unit Cost (LCY)";
                Visible=FALSE;
                OnValidate=BEGIN
                             RedistributeTotalsOnAfterValidate;
                           END;
                            }

    { 60  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the price for one unit of the item.;
                SourceExpr="Unit Price (LCY)";
                Visible=FALSE;
                OnValidate=BEGIN
                             RedistributeTotalsOnAfterValidate;
                           END;
                            }

    { 30  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the net amount (excluding the invoice discount amount) of the line, in the currency of the purchase document.;
                BlankZero=Yes;
                SourceExpr="Line Amount";
                OnValidate=BEGIN
                             RedistributeTotalsOnAfterValidate;
                           END;
                            }

    { 16  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the line discount percentage that is valid for the item on the line.;
                BlankZero=Yes;
                SourceExpr="Line Discount %";
                OnValidate=BEGIN
                             RedistributeTotalsOnAfterValidate;
                           END;
                            }

    { 50  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the amount of the line discount that will be granted on the purchase line.;
                SourceExpr="Line Discount Amount";
                Visible=FALSE;
                OnValidate=BEGIN
                             RedistributeTotalsOnAfterValidate;
                           END;
                            }

    { 48  ;2   ;Field     ;
                ToolTipML=ENU=Specifies whether the invoice line is included when the invoice discount is calculated.;
                SourceExpr="Allow Invoice Disc.";
                Visible=FALSE }

    { 22  ;2   ;Field     ;
                ToolTipML=ENU=Specifies that you can assign item charges to this line.;
                SourceExpr="Allow Item Charge Assignment";
                Visible=FALSE }

    { 32  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the quantity of the item charge that will be assigned when you post this line.;
                BlankZero=Yes;
                SourceExpr="Qty. to Assign";
                OnDrillDown=BEGIN
                              CurrPage.SAVERECORD;
                              ShowItemChargeAssgnt;
                              UpdateForm(FALSE);
                            END;
                             }

    { 26  ;2   ;Field     ;
                ToolTipML=ENU=Specifies how much of the item charge that has been assigned.;
                BlankZero=Yes;
                SourceExpr="Qty. Assigned";
                OnDrillDown=BEGIN
                              CurrPage.SAVERECORD;
                              ShowItemChargeAssgnt;
                              UpdateForm(FALSE);
                            END;
                             }

    { 28  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the number of the production order that the purchase order was created for.;
                SourceExpr="Prod. Order No.";
                Visible=FALSE }

    { 18  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the document number of the blanket order from which this purchase line originates.;
                SourceExpr="Blanket Order No.";
                Visible=FALSE }

    { 34  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the line number of the blanket order line from which this purchase line originates.;
                SourceExpr="Blanket Order Line No.";
                Visible=FALSE }

    { 42  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the item ledger entry number the line should be applied to.;
                SourceExpr="Appl.-to Item Entry";
                Visible=FALSE }

    { 66  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the dimension value code linked to the purchase.;
                SourceExpr="Shortcut Dimension 1 Code";
                Visible=FALSE }

    { 64  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the dimension value code linked to the purchase.;
                SourceExpr="Shortcut Dimension 2 Code";
                Visible=FALSE }

    { 300 ;2   ;Field     ;
                SourceExpr=ShortcutDimCode[3];
                CaptionClass='1,2,3';
                TableRelation="Dimension Value".Code WHERE (Global Dimension No.=CONST(3),
                                                            Dimension Value Type=CONST(Standard),
                                                            Blocked=CONST(No));
                Visible=FALSE;
                OnValidate=BEGIN
                             ValidateSaveShortcutDimCode(3,ShortcutDimCode[3]);
                           END;
                            }

    { 302 ;2   ;Field     ;
                SourceExpr=ShortcutDimCode[4];
                CaptionClass='1,2,4';
                TableRelation="Dimension Value".Code WHERE (Global Dimension No.=CONST(4),
                                                            Dimension Value Type=CONST(Standard),
                                                            Blocked=CONST(No));
                Visible=FALSE;
                OnValidate=BEGIN
                             ValidateSaveShortcutDimCode(4,ShortcutDimCode[4]);
                           END;
                            }

    { 304 ;2   ;Field     ;
                SourceExpr=ShortcutDimCode[5];
                CaptionClass='1,2,5';
                TableRelation="Dimension Value".Code WHERE (Global Dimension No.=CONST(5),
                                                            Dimension Value Type=CONST(Standard),
                                                            Blocked=CONST(No));
                Visible=FALSE;
                OnValidate=BEGIN
                             ValidateSaveShortcutDimCode(5,ShortcutDimCode[5]);
                           END;
                            }

    { 306 ;2   ;Field     ;
                SourceExpr=ShortcutDimCode[6];
                CaptionClass='1,2,6';
                TableRelation="Dimension Value".Code WHERE (Global Dimension No.=CONST(6),
                                                            Dimension Value Type=CONST(Standard),
                                                            Blocked=CONST(No));
                Visible=FALSE;
                OnValidate=BEGIN
                             ValidateSaveShortcutDimCode(6,ShortcutDimCode[6]);
                           END;
                            }

    { 308 ;2   ;Field     ;
                SourceExpr=ShortcutDimCode[7];
                CaptionClass='1,2,7';
                TableRelation="Dimension Value".Code WHERE (Global Dimension No.=CONST(7),
                                                            Dimension Value Type=CONST(Standard),
                                                            Blocked=CONST(No));
                Visible=FALSE;
                OnValidate=BEGIN
                             ValidateSaveShortcutDimCode(7,ShortcutDimCode[7]);
                           END;
                            }

    { 310 ;2   ;Field     ;
                SourceExpr=ShortcutDimCode[8];
                CaptionClass='1,2,8';
                TableRelation="Dimension Value".Code WHERE (Global Dimension No.=CONST(8),
                                                            Dimension Value Type=CONST(Standard),
                                                            Blocked=CONST(No));
                Visible=FALSE;
                OnValidate=BEGIN
                             ValidateSaveShortcutDimCode(8,ShortcutDimCode[8]);
                           END;
                            }

    { 40  ;1   ;Group     ;
                GroupType=Group }

    { 31  ;2   ;Group     ;
                GroupType=Group }

    { 33  ;3   ;Field     ;
                Name=Invoice Discount Amount;
                CaptionML=[ENU=Invoice Discount Amount;
                           PTG=Valor Desconto Fatura];
                ToolTipML=ENU=Specifies a discount amount that is deducted from the value in the Total Incl. VAT field.;
                SourceExpr=TotalPurchaseLine."Inv. Discount Amount";
                AutoFormatType=1;
                AutoFormatExpr=TotalPurchaseHeader."Currency Code";
                Editable=InvDiscAmountEditable;
                Style=Subordinate;
                StyleExpr=RefreshMessageEnabled;
                OnValidate=VAR
                             PurchaseHeader@1000 : Record 38;
                           BEGIN
                             PurchaseHeader.GET("Document Type","Document No.");
                             PurchCalcDiscByType.ApplyInvDiscBasedOnAmt(TotalPurchaseLine."Inv. Discount Amount",PurchaseHeader);
                             CurrPage.UPDATE(FALSE);
                           END;
                            }

    { 27  ;3   ;Field     ;
                Name=Invoice Disc. Pct.;
                CaptionML=[ENU=Invoice Discount %;
                           PTG=Valor Desconto %];
                ToolTipML=ENU=Specifies a discount percentage that is granted if criteria that you have set up for the customer are met.;
                DecimalPlaces=0:2;
                SourceExpr=PurchCalcDiscByType.GetVendInvoiceDiscountPct(Rec);
                Editable=FALSE;
                Style=Subordinate;
                StyleExpr=RefreshMessageEnabled }

    { 17  ;2   ;Group     ;
                GroupType=Group }

    { 11  ;3   ;Field     ;
                Name=Total Amount Excl. VAT;
                DrillDown=No;
                CaptionML=[ENU=Total Amount Excl. VAT;
                           PTG=Valor Total Excl.IVA];
                ToolTipML=ENU=Specifies the sum of the value in the Line Amount Excl. VAT field on all lines in the document, minus any discount amount in the Invoice Discount Amount field.;
                SourceExpr=TotalPurchaseLine.Amount;
                AutoFormatType=1;
                AutoFormatExpr=TotalPurchaseHeader."Currency Code";
                CaptionClass=DocumentTotals.GetTotalExclVATCaption(PurchHeader."Currency Code");
                Editable=FALSE;
                Style=Subordinate;
                StyleExpr=RefreshMessageEnabled }

    { 13  ;3   ;Field     ;
                Name=Total VAT Amount;
                CaptionML=[ENU=Total VAT;
                           PTG=Total IVA];
                ToolTipML=ENU=Specifies the sum of VAT amounts on all lines in the document.;
                SourceExpr=VATAmount;
                AutoFormatType=1;
                AutoFormatExpr=TotalPurchaseHeader."Currency Code";
                CaptionClass=DocumentTotals.GetTotalVATCaption(PurchHeader."Currency Code");
                Editable=FALSE;
                Style=Subordinate;
                StyleExpr=RefreshMessageEnabled }

    { 7   ;3   ;Field     ;
                Name=Total Amount Incl. VAT;
                CaptionML=[ENU=Total Amount Incl. VAT;
                           PTG=Valor Total Incl. IVA];
                ToolTipML=ENU=Specifies the sum of the value in the Line Amount Incl. VAT field on all lines in the document, minus any discount amount in the Invoice Discount Amount field.;
                SourceExpr=TotalPurchaseLine."Amount Including VAT";
                AutoFormatType=1;
                AutoFormatExpr=TotalPurchaseHeader."Currency Code";
                CaptionClass=DocumentTotals.GetTotalInclVATCaption(PurchHeader."Currency Code");
                Editable=FALSE;
                StyleExpr=TotalAmountStyle }

    { 9   ;3   ;Field     ;
                Name=RefreshTotals;
                DrillDown=Yes;
                SourceExpr=RefreshMessageText;
                Enabled=RefreshMessageEnabled;
                Editable=FALSE;
                OnDrillDown=BEGIN
                              DocumentTotals.PurchaseRedistributeInvoiceDiscountAmounts(Rec,VATAmount,TotalPurchaseLine);
                              DocumentTotals.PurchaseUpdateTotalsControls(Rec,TotalPurchaseHeader,TotalPurchaseLine,RefreshMessageEnabled,
                                TotalAmountStyle,RefreshMessageText,InvDiscAmountEditable,VATAmount);
                            END;

                ShowCaption=No }

  }
  CODE
  {
    VAR
      TotalPurchaseHeader@1013 : Record 38;
      TotalPurchaseLine@1012 : Record 39;
      PurchHeader@1014 : Record 38;
      PurchCalcDiscByType@1016 : Codeunit 66;
      DocumentTotals@1015 : Codeunit 57;
      TransferExtendedText@1002 : Codeunit 378;
      ItemAvailFormsMgt@1004 : Codeunit 353;
      ShortcutDimCode@1003 : ARRAY [8] OF Code[20];
      VATAmount@1011 : Decimal;
      InvDiscAmountEditable@1010 : Boolean;
      TotalAmountStyle@1009 : Text;
      RefreshMessageEnabled@1008 : Boolean;
      RefreshMessageText@1007 : Text;
      UnitofMeasureCodeIsChangeable@1005 : Boolean;

    PROCEDURE ApproveCalcInvDisc@6();
    BEGIN
      CODEUNIT.RUN(CODEUNIT::"Purch.-Disc. (Yes/No)",Rec);
    END;

    LOCAL PROCEDURE ExplodeBOM@3();
    BEGIN
      CODEUNIT.RUN(CODEUNIT::"Purch.-Explode BOM",Rec);
    END;

    LOCAL PROCEDURE InsertExtendedText@5(Unconditionally@1000 : Boolean);
    BEGIN
      IF TransferExtendedText.PurchCheckIfAnyExtText(Rec,Unconditionally) THEN BEGIN
        CurrPage.SAVERECORD;
        TransferExtendedText.InsertPurchExtText(Rec);
      END;
      IF TransferExtendedText.MakeUpdate THEN
        UpdateForm(TRUE);
    END;

    LOCAL PROCEDURE ItemChargeAssgnt@5800();
    BEGIN
      ShowItemChargeAssgnt;
    END;

    LOCAL PROCEDURE OpenItemTrackingLines@6500();
    BEGIN
      OpenItemTrackingLines;
    END;

    PROCEDURE UpdateForm@12(SetSaveRecord@1000 : Boolean);
    BEGIN
      CurrPage.UPDATE(SetSaveRecord);
    END;

    LOCAL PROCEDURE NoOnAfterValidate@19066594();
    BEGIN
      UpdateEditableOnRow;
      InsertExtendedText(FALSE);
      IF (Type = Type::"Charge (Item)") AND ("No." <> xRec."No.") AND
         (xRec."No." <> '')
      THEN
        CurrPage.SAVERECORD;
    END;

    LOCAL PROCEDURE CrossReferenceNoOnAfterValidat@19048248();
    BEGIN
      InsertExtendedText(FALSE);
    END;

    LOCAL PROCEDURE RedistributeTotalsOnAfterValidate@8();
    BEGIN
      CurrPage.SAVERECORD;

      PurchHeader.GET("Document Type","Document No.");
      IF DocumentTotals.PurchaseCheckNumberOfLinesLimit(PurchHeader) THEN
        DocumentTotals.PurchaseRedistributeInvoiceDiscountAmounts(Rec,VATAmount,TotalPurchaseLine);
      CurrPage.UPDATE;
    END;

    LOCAL PROCEDURE ValidateSaveShortcutDimCode@7(FieldNumber@1001 : Integer;VAR ShortcutDimCode@1000 : Code[20]);
    BEGIN
      ValidateShortcutDimCode(FieldNumber,ShortcutDimCode);
      CurrPage.SAVERECORD;
    END;

    LOCAL PROCEDURE UpdateEditableOnRow@4();
    BEGIN
      UnitofMeasureCodeIsChangeable := CanEditUnitOfMeasureCode;
    END;

    BEGIN
    END.
  }
}

