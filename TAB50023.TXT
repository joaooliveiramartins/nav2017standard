OBJECT Table 50023 Conciliation Sections
{
  OBJECT-PROPERTIES
  {
    Date=28/08/06;
    Time=16:45:00;
    Modified=Yes;
    Version List=SGG07.00;
  }
  PROPERTIES
  {
    OnDelete=BEGIN
               Concilia��o.SETCURRENTKEY(Section,Type,Code);
               Concilia��o.SETRANGE(Section,Code);
               Concilia��o.SETRANGE(Type,Type);
               IF Concilia��o.FIND('-') THEN REPEAT
                 Concilia��o.DELETE(TRUE);
               UNTIL Concilia��o.NEXT = 0;
             END;

    CaptionML=[ENU=Conciliation Sections;
               PTG=Sec��es concilia��o];
  }
  FIELDS
  {
    { 1   ;   ;Type                ;Option        ;CaptionML=[ENU=Type;
                                                              PTG=Tipo];
                                                   OptionString=Bancos,Contas,Ficheiros,Misto }
    { 2   ;   ;Code                ;Code10        ;CaptionML=[ENU=Code;
                                                              PTG=C�digo];
                                                   NotBlank=Yes }
    { 3   ;   ;Description         ;Text30        ;CaptionML=[ENU=Description;
                                                              PTG=Descri��o] }
  }
  KEYS
  {
    {    ;Type,Code                               ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {
    VAR
      "Concilia��o"@1000000000 : Record 50016;

    BEGIN
    {
      SGG07.00 PFILIPE 28-08-06 Criada nova tabela
    }
    END.
  }
}

