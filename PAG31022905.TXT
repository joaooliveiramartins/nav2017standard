OBJECT Page 31022905 MOAF manual selection
{
  OBJECT-PROPERTIES
  {
    Date=01/02/15;
    Time=13:00:00;
    Version List=NAVPTSS81.00;
  }
  PROPERTIES
  {
    CaptionML=[ENU=MOAF manual selection;
               PTG=Sele��o manual MOAF];
    SourceTable=Table31022902;
    PageType=List;
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 4   ;2   ;Field     ;
                SourceExpr=Type }

    { 2   ;2   ;Field     ;
                SourceExpr=Description }

    { 6   ;2   ;Field     ;
                SourceExpr="Manual value" }

  }
  CODE
  {

    BEGIN
    END.
  }
}

