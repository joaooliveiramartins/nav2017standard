OBJECT Table 7132 Item Budget Name
{
  OBJECT-PROPERTIES
  {
    Date=07/09/12;
    Time=12:00:00;
    Version List=NAVW17.00;
  }
  PROPERTIES
  {
    OnDelete=VAR
               ItemBudgetEntry@1000 : Record 7134;
               ItemAnalysisViewBudgetEntry@1001 : Record 7156;
             BEGIN
               ItemBudgetEntry.SETCURRENTKEY("Analysis Area","Budget Name");
               ItemBudgetEntry.SETRANGE("Analysis Area","Analysis Area");
               ItemBudgetEntry.SETRANGE("Budget Name",Name);
               ItemBudgetEntry.DELETEALL(TRUE);

               ItemAnalysisViewBudgetEntry.SETRANGE("Analysis Area","Analysis Area");
               ItemAnalysisViewBudgetEntry.SETRANGE("Budget Name",Name);
               ItemAnalysisViewBudgetEntry.DELETEALL;
             END;

    CaptionML=[ENU=Item Budget Name;
               PTG=Nome Or�amento Produto];
    LookupPageID=Page7132;
  }
  FIELDS
  {
    { 1   ;   ;Analysis Area       ;Option        ;CaptionML=[ENU=Analysis Area;
                                                              PTG=�rea de An�lise];
                                                   OptionCaptionML=[ENU=Sales,Purchase;
                                                                    PTG=Vendas,Compras];
                                                   OptionString=Sales,Purchase }
    { 2   ;   ;Name                ;Code10        ;CaptionML=[ENU=Name;
                                                              PTG=Nome];
                                                   NotBlank=Yes }
    { 3   ;   ;Description         ;Text80        ;CaptionML=[ENU=Description;
                                                              PTG=Descri��o] }
    { 4   ;   ;Blocked             ;Boolean       ;CaptionML=[ENU=Blocked;
                                                              PTG=Bloqueado] }
    { 5   ;   ;Budget Dimension 1 Code;Code20     ;TableRelation=Dimension;
                                                   OnValidate=BEGIN
                                                                IF "Budget Dimension 1 Code" <> xRec."Budget Dimension 1 Code" THEN BEGIN
                                                                  IF Dim.CheckIfDimUsed("Budget Dimension 1 Code",17,Name,'',"Analysis Area") THEN
                                                                    ERROR(Text000,Dim.GetCheckDimErr);
                                                                  MODIFY;
                                                                END;
                                                              END;

                                                   CaptionML=[ENU=Budget Dimension 1 Code;
                                                              PTG=C�d. Dimens�o 1 Or�.] }
    { 6   ;   ;Budget Dimension 2 Code;Code20     ;TableRelation=Dimension;
                                                   OnValidate=BEGIN
                                                                IF "Budget Dimension 2 Code" <> xRec."Budget Dimension 2 Code" THEN BEGIN
                                                                  IF Dim.CheckIfDimUsed("Budget Dimension 2 Code",18,Name,'',"Analysis Area") THEN
                                                                    ERROR(Text000,Dim.GetCheckDimErr);
                                                                  MODIFY;
                                                                END;
                                                              END;

                                                   CaptionML=[ENU=Budget Dimension 2 Code;
                                                              PTG=C�d. Dimens�o 2 Or�.] }
    { 7   ;   ;Budget Dimension 3 Code;Code20     ;TableRelation=Dimension;
                                                   OnValidate=BEGIN
                                                                IF "Budget Dimension 3 Code" <> xRec."Budget Dimension 3 Code" THEN BEGIN
                                                                  IF Dim.CheckIfDimUsed("Budget Dimension 3 Code",19,Name,'',"Analysis Area") THEN
                                                                    ERROR(Text000,Dim.GetCheckDimErr);
                                                                  MODIFY;
                                                                END;
                                                              END;

                                                   CaptionML=[ENU=Budget Dimension 3 Code;
                                                              PTG=C�d. Dimens�o 3 Or�.] }
  }
  KEYS
  {
    {    ;Analysis Area,Name                      ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {
    VAR
      Text000@1000 : TextConst 'ENU=%1\You cannot use the same dimension twice in the same budget.;PTG=%1\N�o � poss�vel utilizar duas vezes a mesma dimens�o no mesmo or�amento.';
      Dim@1002 : Record 348;

    BEGIN
    END.
  }
}

