OBJECT Page 5847 Average Cost Calc. Overview
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Average Cost Calc. Overview;
               PTG=Descri��o Geral C�lculo Custo M�dio];
    InsertAllowed=No;
    DeleteAllowed=No;
    ModifyAllowed=No;
    SourceTable=Table5847;
    DataCaptionExpr=ItemName;
    SourceTableView=SORTING(Attached to Valuation Date,Attached to Entry No.,Type);
    PageType=List;
    SourceTableTemporary=Yes;
    OnOpenPage=BEGIN
                 InitTempTable;
                 ExpandAll(AvgCostCalcOverview);

                 SetRecFilters;
                 CurrPage.UPDATE(FALSE);

                 ItemName := STRSUBSTNO('%1  %2',Item."No.",Item.Description);
               END;

    OnAfterGetRecord=BEGIN
                       DocumentLineNoHideValue := FALSE;
                       EntryTypeHideValue := FALSE;
                       ItemLedgerEntryNoHideValue := FALSE;
                       TypeIndent := 0;
                       SetExpansionStatus;
                       IF Type = Type::"Closing Entry" THEN BEGIN
                         Quantity := CalculateRemainingQty;
                         "Cost Amount (Expected)" := CalculateCostAmt(FALSE);
                         "Cost Amount (Actual)" := CalculateCostAmt(TRUE);
                       END;
                       TypeOnFormat;
                       ItemLedgerEntryNoOnFormat;
                       EntryTypeOnFormat;
                       DocumentLineNoOnFormat;
                     END;

    ActionList=ACTIONS
    {
      { 1900000003;  ;ActionContainer;
                      ActionContainerType=ActionItems }
      { 41      ;1   ;ActionGroup;
                      CaptionML=[ENU=&Line;
                                 PTG=&Linha];
                      Image=Line }
      { 54      ;2   ;Action    ;
                      AccessByPermission=TableData 348=R;
                      ShortCutKey=Shift+Ctrl+D;
                      CaptionML=[ENU=Dimensions;
                                 PTG=Dimens�es];
                      ToolTipML=[ENU=View or edits dimensions, such as area, project, or department, that you can assign to sales and purchase documents to distribute costs and analyze transaction history.;
                                 PTG=""];
                      ApplicationArea=#Suite;
                      Image=Dimensions;
                      OnAction=BEGIN
                                 IF ItemLedgEntry.GET("Entry No.") THEN
                                   ItemLedgEntry.ShowDimensions;
                               END;
                                }
      { 55      ;2   ;Action    ;
                      ShortCutKey=Ctrl+F7;
                      CaptionML=[ENU=&Value Entries;
                                 PTG=Movs. &Valor];
                      RunObject=Page 5802;
                      RunPageView=SORTING(Item Ledger Entry No.);
                      RunPageLink=Item Ledger Entry No.=FIELD(Item Ledger Entry No.),
                                  Valuation Date=FIELD(Valuation Date);
                      Image=ValueLedger }
      { 45      ;1   ;ActionGroup;
                      CaptionML=[ENU=&Application;
                                 PTG=&Liquida��o];
                      Image=Apply }
      { 56      ;2   ;Action    ;
                      CaptionML=[ENU=Applied E&ntries;
                                 PTG=Movs. Liq&uidados];
                      ToolTipML=[ENU=View the ledger entries that have been applied to this record.;
                                 PTG=""];
                      ApplicationArea=#Basic,#Suite;
                      Image=Approve;
                      OnAction=VAR
                                 ItemLedgEntry@1000 : Record 32;
                               BEGIN
                                 IF ItemLedgEntry.GET("Item Ledger Entry No.") THEN
                                   CODEUNIT.RUN(CODEUNIT::"Show Applied Entries",ItemLedgEntry);
                               END;
                                }
      { 57      ;2   ;Action    ;
                      AccessByPermission=TableData 27=R;
                      CaptionML=[ENU=Reservation Entries;
                                 PTG=Movs. Reserva];
                      Image=ReservationLedger;
                      OnAction=VAR
                                 ItemLedgEntry@1000 : Record 32;
                               BEGIN
                                 ItemLedgEntry.GET("Item Ledger Entry No.");
                                 ItemLedgEntry.ShowReservationEntries(TRUE);
                               END;
                                }
      { 1900000004;  ;ActionContainer;
                      ActionContainerType=RelatedInformation }
      { 53      ;1   ;Action    ;
                      CaptionML=[ENU=&Navigate;
                                 PTG=&Navegar];
                      ToolTipML=[ENU=Find all entries and documents that exist for the document number and posting date on the selected entry or document.;
                                 PTG=""];
                      ApplicationArea=#Basic,#Suite;
                      Promoted=Yes;
                      Image=Navigate;
                      PromotedCategory=Process;
                      OnAction=BEGIN
                                 Navigate.SetDoc("Posting Date","Document No.");
                                 Navigate.RUN;
                               END;
                                }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                IndentationColumnName=TypeIndent;
                IndentationControls=Type;
                ShowAsTree=Yes;
                GroupType=Repeater }

    { 19  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies either that the entry is a summary entry, Closing Entry, or the type that was used in the calculation of the average cost of the item.;
                           PTG=""];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=Type;
                Editable=FALSE;
                StyleExpr='Strong' }

    { 34  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the valuation date associated with the average cost calculation.;
                           PTG=""];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Valuation Date";
                Editable=FALSE;
                StyleExpr='Strong' }

    { 4   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the number of the item associated with the entry.;
                           PTG=""];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Item No.";
                Editable=FALSE }

    { 15  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the location code associated with the entry.;
                           PTG=""];
                SourceExpr="Location Code";
                Visible=FALSE;
                Editable=FALSE }

    { 47  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the variant of the item on the line.;
                           PTG=""];
                SourceExpr="Variant Code";
                Visible=FALSE;
                Editable=FALSE }

    { 49  ;2   ;Field     ;
                Name=AverageCostCntrl;
                CaptionML=[ENU=Unit Cost;
                           PTG=Custo Unit�rio];
                ToolTipML=[ENU=Specifies the average cost for this entry.;
                           PTG=""];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=CalculateAverageCost;
                AutoFormatType=2;
                Editable=FALSE;
                StyleExpr='Strong' }

    { 28  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies whether the cost is adjusted for the entry.;
                           PTG=""];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Cost is Adjusted";
                Editable=FALSE }

    { 2   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the number of the item ledger entry that this entry is linked to.;
                           PTG=""];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Item Ledger Entry No.";
                Visible=FALSE;
                Editable=FALSE;
                HideValue=ItemLedgerEntryNoHideValue }

    { 6   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the posting date for the entry.;
                           PTG=""];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Posting Date";
                Visible=FALSE;
                Editable=FALSE }

    { 8   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies which type of transaction that the entry is created from.;
                           PTG=""];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Entry Type";
                Editable=FALSE;
                HideValue=EntryTypeHideValue }

    { 23  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the type of document that the average cost applies to.;
                           PTG=""];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Document Type";
                Visible=FALSE;
                Editable=FALSE }

    { 10  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies a document number for the entry.;
                           PTG=""];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Document No.";
                Visible=FALSE;
                Editable=FALSE }

    { 25  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the document line that the comment applies to.;
                           PTG=""];
                SourceExpr="Document Line No.";
                Visible=FALSE;
                Editable=FALSE;
                HideValue=DocumentLineNoHideValue }

    { 21  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the quantity associated with the entry.;
                           PTG=""];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=Quantity;
                Editable=FALSE;
                StyleExpr='Strong' }

    { 17  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the expected cost in LCY of the quantity posting.;
                           PTG=""];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Cost Amount (Expected)";
                Editable=FALSE;
                StyleExpr='Strong' }

    { 51  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the adjusted cost in LCY of the quantity posting.;
                           PTG=""];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Cost Amount (Actual)";
                Editable=FALSE;
                StyleExpr='Strong' }

  }
  CODE
  {
    VAR
      Item@1003 : Record 27;
      AvgCostCalcOverview@1002 : TEMPORARY Record 5847;
      ItemLedgEntry@1001 : Record 32;
      GetAvgCostCalcOverview@1004 : Codeunit 5847;
      Navigate@1005 : Page 344;
      ActualExpansionStatus@1000 : Integer;
      ItemName@1006 : Text[250];
      TypeIndent@19015277 : Integer INDATASET;
      ItemLedgerEntryNoHideValue@19024839 : Boolean INDATASET;
      EntryTypeHideValue@19058570 : Boolean INDATASET;
      DocumentLineNoHideValue@19051570 : Boolean INDATASET;

    PROCEDURE SetExpansionStatus@16();
    BEGIN
      CASE TRUE OF
        IsExpanded(Rec):
          ActualExpansionStatus := 1;
        HasChildren(Rec):
          ActualExpansionStatus := 0
        ELSE
          ActualExpansionStatus := 2;
      END;
    END;

    PROCEDURE InitTempTable@1();
    VAR
      AvgCostCalcOverviewFilters@1000 : Record 5847;
    BEGIN
      AvgCostCalcOverview."Item No." := Item."No.";
      AvgCostCalcOverview.SETFILTER("Valuation Date",Item.GETFILTER("Date Filter"));
      AvgCostCalcOverview.SETFILTER("Location Code",Item.GETFILTER("Location Filter"));
      AvgCostCalcOverview.SETFILTER("Variant Code",Item.GETFILTER("Variant Filter"));

      GetAvgCostCalcOverview.RUN(AvgCostCalcOverview);
      AvgCostCalcOverview.RESET;
      AvgCostCalcOverviewFilters.COPYFILTERS(Rec);
      RESET;
      DELETEALL;
      IF AvgCostCalcOverview.FIND('-') THEN
        REPEAT
          IF AvgCostCalcOverview.Level = 0 THEN BEGIN
            Rec := AvgCostCalcOverview;
            INSERT;
          END;
        UNTIL AvgCostCalcOverview.NEXT = 0;
      COPYFILTERS(AvgCostCalcOverviewFilters);
    END;

    LOCAL PROCEDURE ExpandAll@12(VAR AvgCostCalcOverview@1000 : Record 5847);
    VAR
      AvgCostCalcOverviewFilters@1001 : Record 5847;
    BEGIN
      AvgCostCalcOverview."Item No." := Item."No.";
      AvgCostCalcOverview.SETFILTER("Valuation Date",Item.GETFILTER("Date Filter"));
      AvgCostCalcOverview.SETFILTER("Location Code",Item.GETFILTER("Location Filter"));
      AvgCostCalcOverview.SETFILTER("Variant Code",Item.GETFILTER("Variant Filter"));

      GetAvgCostCalcOverview.RUN(AvgCostCalcOverview);
      AvgCostCalcOverviewFilters.COPYFILTERS(Rec);
      RESET;
      DELETEALL;

      IF AvgCostCalcOverview.FIND('+') THEN
        REPEAT
          Rec := AvgCostCalcOverview;
          GetAvgCostCalcOverview.Calculate(AvgCostCalcOverview);
          AvgCostCalcOverview.RESET;
          AvgCostCalcOverview := Rec;
        UNTIL AvgCostCalcOverview.NEXT(-1) = 0;

      IF AvgCostCalcOverview.FIND('+') THEN
        REPEAT
          Rec := AvgCostCalcOverview;
          INSERT;
        UNTIL AvgCostCalcOverview.NEXT(-1) = 0;

      COPYFILTERS(AvgCostCalcOverviewFilters);
    END;

    LOCAL PROCEDURE IsExpanded@20(ActualAvgCostCalcOverview@1000 : Record 5847) : Boolean;
    VAR
      xAvgCostCalcOverview@1002 : TEMPORARY Record 5847;
      Found@1003 : Boolean;
    BEGIN
      xAvgCostCalcOverview := Rec;
      SETCURRENTKEY("Attached to Valuation Date","Attached to Entry No.",Type);
      Rec := ActualAvgCostCalcOverview;
      Found := (NEXT(GetDirection) <> 0);
      IF Found THEN
        Found := (Level > ActualAvgCostCalcOverview.Level);
      Rec := xAvgCostCalcOverview;
      EXIT(Found);
    END;

    LOCAL PROCEDURE HasChildren@19(VAR ActualAvgCostCalcOverview@1000 : Record 5847) : Boolean;
    BEGIN
      AvgCostCalcOverview := ActualAvgCostCalcOverview;
      IF Type = Type::"Closing Entry" THEN
        EXIT(GetAvgCostCalcOverview.EntriesExist(AvgCostCalcOverview));
      EXIT(FALSE);
    END;

    LOCAL PROCEDURE GetDirection@3() : Integer;
    BEGIN
      IF ASCENDING THEN
        EXIT(1);
      EXIT(-1);
    END;

    PROCEDURE SetRecFilters@5();
    BEGIN
      RESET;
      SETCURRENTKEY("Attached to Valuation Date","Attached to Entry No.",Type);
      CurrPage.UPDATE(FALSE);
    END;

    PROCEDURE SetItem@2(VAR Item2@1000 : Record 27);
    BEGIN
      Item.COPY(Item2);
    END;

    LOCAL PROCEDURE TypeOnFormat@19025827();
    BEGIN
      IF Type <> Type::"Closing Entry" THEN
        TypeIndent := 1;
    END;

    LOCAL PROCEDURE ItemLedgerEntryNoOnFormat@19009322();
    BEGIN
      IF Type = Type::"Closing Entry" THEN
        ItemLedgerEntryNoHideValue := TRUE;
    END;

    LOCAL PROCEDURE EntryTypeOnFormat@19043113();
    BEGIN
      IF Type = Type::"Closing Entry" THEN
        EntryTypeHideValue := TRUE;
    END;

    LOCAL PROCEDURE DocumentLineNoOnFormat@19065899();
    BEGIN
      IF Type = Type::"Closing Entry" THEN
        DocumentLineNoHideValue := TRUE;
    END;

    BEGIN
    END.
  }
}

