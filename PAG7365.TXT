OBJECT Page 7365 Whse. Reclassification Journal
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Whse. Reclassification Journal;
               PTG=Di�rio Reclassifica��o Arm.];
    SaveValues=Yes;
    SourceTable=Table7311;
    DelayedInsert=Yes;
    DataCaptionFields=Journal Batch Name;
    PageType=Worksheet;
    AutoSplitKey=Yes;
    OnOpenPage=VAR
                 JnlSelected@1000 : Boolean;
               BEGIN
                 IF IsOpenedFromBatch THEN BEGIN
                   CurrentJnlBatchName := "Journal Batch Name";
                   CurrentLocationCode := "Location Code";
                   OpenJnl(CurrentJnlBatchName,CurrentLocationCode,Rec);
                   EXIT;
                 END;
                 TemplateSelection(PAGE::"Whse. Reclassification Journal",2,Rec,JnlSelected);
                 IF NOT JnlSelected THEN
                   ERROR('');
                 OpenJnl(CurrentJnlBatchName,CurrentLocationCode,Rec);
               END;

    OnNewRecord=BEGIN
                  IF "Journal Batch Name" <> '' THEN
                    SetUpNewLine(xRec);
                END;

    OnAfterGetCurrRecord=BEGIN
                           GetItem("Item No.",ItemDescription);
                         END;

    ActionList=ACTIONS
    {
      { 1900000003;0 ;ActionContainer;
                      ActionContainerType=RelatedInformation }
      { 16      ;1   ;ActionGroup;
                      CaptionML=[ENU=&Line;
                                 PTG=&Linha];
                      Image=Line }
      { 17      ;2   ;Action    ;
                      Name=ItemTrackingLines;
                      ShortCutKey=Shift+Ctrl+I;
                      CaptionML=[ENU=Item &Tracking Lines;
                                 PTG=Linhas Ras&treio Produto];
                      Image=ItemTrackingLines;
                      OnAction=BEGIN
                                 OpenItemTrackingLines;
                               END;
                                }
      { 29      ;1   ;ActionGroup;
                      CaptionML=[ENU=&Item;
                                 PTG=&Produto];
                      Image=Item }
      { 30      ;2   ;Action    ;
                      ShortCutKey=Shift+F7;
                      CaptionML=[ENU=Card;
                                 PTG=Ficha];
                      RunObject=Page 30;
                      RunPageLink=No.=FIELD(Item No.);
                      Image=EditLines }
      { 4       ;2   ;Action    ;
                      ShortCutKey=Ctrl+F7;
                      CaptionML=[ENU=Warehouse Entries;
                                 PTG=Movs. Armaz�m];
                      RunObject=Page 7318;
                      RunPageView=SORTING(Item No.,Location Code,Variant Code);
                      RunPageLink=Item No.=FIELD(Item No.),
                                  Variant Code=FIELD(Variant Code),
                                  Location Code=FIELD(Location Code);
                      Image=BinLedger }
      { 31      ;2   ;Action    ;
                      CaptionML=[ENU=Ledger E&ntries;
                                 PTG=Movime&ntos];
                      RunObject=Page 38;
                      RunPageView=SORTING(Item No.);
                      RunPageLink=Item No.=FIELD(Item No.),
                                  Variant Code=FIELD(Variant Code),
                                  Location Code=FIELD(Location Code);
                      Promoted=No;
                      Image=ItemLedger;
                      PromotedCategory=Process }
      { 44      ;2   ;Action    ;
                      CaptionML=[ENU=Bin Contents;
                                 PTG=Conte�do Posi��o];
                      RunObject=Page 7305;
                      RunPageView=SORTING(Location Code,Item No.,Variant Code);
                      RunPageLink=Location Code=FIELD(Location Code),
                                  Item No.=FIELD(Item No.),
                                  Variant Code=FIELD(Variant Code);
                      Image=BinContent }
      { 1900000004;0 ;ActionContainer;
                      ActionContainerType=ActionItems }
      { 32      ;1   ;ActionGroup;
                      CaptionML=[ENU=&Registering;
                                 PTG=&Registo];
                      Image=PostOrder }
      { 33      ;2   ;Action    ;
                      Ellipsis=Yes;
                      CaptionML=[ENU=Test Report;
                                 PTG=Verificar...];
                      ToolTipML=[ENU=View a test report so that you can find and correct any errors before you perform the actual posting of the journal or document.;
                                 PTG=""];
                      Image=TestReport;
                      OnAction=BEGIN
                                 ReportPrint.PrintWhseJnlLine(Rec);
                               END;
                                }
      { 34      ;2   ;Action    ;
                      Name=Register;
                      ShortCutKey=F9;
                      CaptionML=[ENU=&Register;
                                 PTG=&Registos];
                      Promoted=Yes;
                      Image=Confirm;
                      PromotedCategory=Process;
                      OnAction=BEGIN
                                 CODEUNIT.RUN(CODEUNIT::"Whse. Jnl.-Register",Rec);
                                 CurrentJnlBatchName := GETRANGEMAX("Journal Batch Name");
                                 CurrPage.UPDATE(FALSE);
                               END;
                                }
      { 35      ;2   ;Action    ;
                      ShortCutKey=Shift+F9;
                      CaptionML=[ENU=Register and &Print;
                                 PTG=Registar e &Imprimir];
                      Promoted=Yes;
                      Image=ConfirmAndPrint;
                      PromotedCategory=Process;
                      OnAction=BEGIN
                                 CODEUNIT.RUN(CODEUNIT::"Whse. Jnl.-Register+Print",Rec);
                                 CurrentJnlBatchName := GETRANGEMAX("Journal Batch Name");
                                 CurrPage.UPDATE(FALSE);
                               END;
                                }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 25  ;1   ;Field     ;
                Lookup=Yes;
                CaptionML=[ENU=Batch Name;
                           PTG=Nome Sec��o];
                SourceExpr=CurrentJnlBatchName;
                OnValidate=BEGIN
                             CheckName(CurrentJnlBatchName,CurrentLocationCode,Rec);
                             CurrentJnlBatchNameOnAfterVali;
                           END;

                OnLookup=BEGIN
                           CurrPage.SAVERECORD;
                           LookupName(CurrentJnlBatchName,CurrentLocationCode,Rec);
                           CurrPage.UPDATE(FALSE);
                         END;
                          }

    { 9   ;1   ;Field     ;
                Lookup=Yes;
                CaptionML=[ENU=Location Code;
                           PTG=C�d. Localiza��o];
                SourceExpr=CurrentLocationCode;
                TableRelation=Location;
                Editable=FALSE }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 27  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the date the line is registered.;
                           PTG=""];
                SourceExpr="Registering Date" }

    { 20  ;2   ;Field     ;
                CaptionML=[ENU=Whse. Document No.;
                           PTG=N� Documento Armaz�m];
                ToolTipML=[ENU=Specifies the warehouse document number of the journal line.;
                           PTG=""];
                SourceExpr="Whse. Document No." }

    { 39  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the number of the item on the journal line.;
                           PTG=""];
                SourceExpr="Item No.";
                OnValidate=BEGIN
                             GetItem("Item No.",ItemDescription);
                           END;
                            }

    { 63  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the code of the item variant.;
                           PTG=""];
                SourceExpr="Variant Code";
                Visible=FALSE }

    { 10  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the description of the item.;
                           PTG=""];
                SourceExpr=Description }

    { 6   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the code of the zone from which the item on the journal line is taken.;
                           PTG=""];
                SourceExpr="From Zone Code" }

    { 14  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the code of the bin from which the item on the journal line is taken.;
                           PTG=""];
                SourceExpr="From Bin Code" }

    { 5   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the code of the zone to which the item on the journal line will be moved.;
                           PTG=""];
                SourceExpr="To Zone Code" }

    { 18  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the code of the bin to which the item on the journal line will be moved.;
                           PTG=""];
                SourceExpr="To Bin Code" }

    { 12  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the number of units of the item in the adjustment (positive or negative) or the reclassification.;
                           PTG=""];
                SourceExpr=Quantity;
                MinValue=0 }

    { 67  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the code of the unit of measure for this item.;
                           PTG=""];
                SourceExpr="Unit of Measure Code" }

    { 2   ;2   ;Field     ;
                CaptionML=[ENU=Reason Code;
                           PTG=C�d. Auditoria];
                ToolTipML=[ENU=Specifies the reason code for the warehouse journal line.;
                           PTG=""];
                SourceExpr="Reason Code";
                Visible=FALSE }

    { 22  ;1   ;Group      }

    { 1900669001;2;Group  ;
                GroupType=FixedLayout }

    { 1901652601;3;Group  ;
                CaptionML=[ENU=Item Description;
                           PTG=Descri��o Produto] }

    { 23  ;4   ;Field     ;
                SourceExpr=ItemDescription;
                Editable=FALSE;
                ShowCaption=No }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {
    VAR
      ReportPrint@1002 : Codeunit 228;
      CurrentJnlBatchName@1003 : Code[10];
      CurrentLocationCode@1006 : Code[10];
      ItemDescription@1004 : Text[50];

    LOCAL PROCEDURE CurrentJnlBatchNameOnAfterVali@19002411();
    BEGIN
      CurrPage.SAVERECORD;
      SetName(CurrentJnlBatchName,CurrentLocationCode,Rec);
      CurrPage.UPDATE(FALSE);
    END;

    BEGIN
    END.
  }
}

