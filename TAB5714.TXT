OBJECT Table 5714 Responsibility Center
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0;
  }
  PROPERTIES
  {
    OnDelete=BEGIN
               DimMgt.DeleteDefaultDim(DATABASE::"Responsibility Center",Code);
             END;

    CaptionML=[ENU=Responsibility Center;
               PTG=Centro Responsabilidade];
    LookupPageID=Page5715;
    DrillDownPageID=Page5715;
  }
  FIELDS
  {
    { 1   ;   ;Code                ;Code10        ;CaptionML=[ENU=Code;
                                                              PTG=C�digo];
                                                   NotBlank=Yes }
    { 2   ;   ;Name                ;Text50        ;CaptionML=[ENU=Name;
                                                              PTG=Nome] }
    { 3   ;   ;Address             ;Text50        ;CaptionML=[ENU=Address;
                                                              PTG=Endere�o] }
    { 4   ;   ;Address 2           ;Text50        ;CaptionML=[ENU=Address 2;
                                                              PTG=Endere�o 2] }
    { 5   ;   ;City                ;Text30        ;TableRelation=IF (Country/Region Code=CONST()) "Post Code".City
                                                                 ELSE IF (Country/Region Code=FILTER(<>'')) "Post Code".City WHERE (Country/Region Code=FIELD(Country/Region Code));
                                                   OnValidate=BEGIN
                                                                PostCode.ValidateCity(City,"Post Code",County,"Country/Region Code",(CurrFieldNo <> 0) AND GUIALLOWED);
                                                              END;

                                                   ValidateTableRelation=No;
                                                   TestTableRelation=No;
                                                   CaptionML=[ENU=City;
                                                              PTG=Cidade] }
    { 6   ;   ;Post Code           ;Code20        ;TableRelation=IF (Country/Region Code=CONST()) "Post Code"
                                                                 ELSE IF (Country/Region Code=FILTER(<>'')) "Post Code" WHERE (Country/Region Code=FIELD(Country/Region Code));
                                                   OnValidate=BEGIN
                                                                PostCode.ValidatePostCode(City,"Post Code",County,"Country/Region Code",(CurrFieldNo <> 0) AND GUIALLOWED);
                                                              END;

                                                   ValidateTableRelation=No;
                                                   TestTableRelation=No;
                                                   CaptionML=[ENU=Post Code;
                                                              PTG=C�d. Postal] }
    { 7   ;   ;Country/Region Code ;Code10        ;TableRelation=Country/Region;
                                                   CaptionML=[ENU=Country/Region Code;
                                                              PTG=C�digo Pa�s/Regi�o] }
    { 8   ;   ;Phone No.           ;Text30        ;ExtendedDatatype=Phone No.;
                                                   CaptionML=[ENU=Phone No.;
                                                              PTG=Telefone] }
    { 9   ;   ;Fax No.             ;Text30        ;CaptionML=[ENU=Fax No.;
                                                              PTG=N� Fax] }
    { 10  ;   ;Name 2              ;Text50        ;CaptionML=[ENU=Name 2;
                                                              PTG=Nome 2] }
    { 11  ;   ;Contact             ;Text50        ;CaptionML=[ENU=Contact;
                                                              PTG=Contacto] }
    { 12  ;   ;Global Dimension 1 Code;Code20     ;TableRelation="Dimension Value".Code WHERE (Global Dimension No.=CONST(1));
                                                   OnValidate=BEGIN
                                                                ValidateShortcutDimCode(1,"Global Dimension 1 Code");
                                                              END;

                                                   CaptionML=[ENU=Global Dimension 1 Code;
                                                              PTG=C�d. Dimens�o 1 Global];
                                                   CaptionClass='1,1,1' }
    { 13  ;   ;Global Dimension 2 Code;Code20     ;TableRelation="Dimension Value".Code WHERE (Global Dimension No.=CONST(2));
                                                   OnValidate=BEGIN
                                                                ValidateShortcutDimCode(2,"Global Dimension 2 Code");
                                                              END;

                                                   CaptionML=[ENU=Global Dimension 2 Code;
                                                              PTG=C�d. Dimens�o 2 Global];
                                                   CaptionClass='1,1,2' }
    { 14  ;   ;Location Code       ;Code10        ;TableRelation=Location WHERE (Use As In-Transit=CONST(No));
                                                   CaptionML=[ENU=Location Code;
                                                              PTG=C�d. Localiza��o] }
    { 15  ;   ;County              ;Text30        ;CaptionML=[ENU=County;
                                                              PTG=Distrito] }
    { 102 ;   ;E-Mail              ;Text80        ;ExtendedDatatype=E-Mail;
                                                   CaptionML=[ENU=Email;
                                                              PTG=Email] }
    { 103 ;   ;Home Page           ;Text90        ;ExtendedDatatype=URL;
                                                   CaptionML=[ENU=Home Page;
                                                              PTG=Home Page] }
    { 5900;   ;Date Filter         ;Date          ;FieldClass=FlowFilter;
                                                   CaptionML=[ENU=Date Filter;
                                                              PTG=Filtro Data] }
    { 5901;   ;Contract Gain/Loss Amount;Decimal  ;FieldClass=FlowField;
                                                   CalcFormula=Sum("Contract Gain/Loss Entry".Amount WHERE (Responsibility Center=FIELD(Code),
                                                                                                            Change Date=FIELD(Date Filter)));
                                                   CaptionML=[ENU=Contract Gain/Loss Amount;
                                                              PTG=Ganhos/Perdas Valor Contrato];
                                                   Editable=No;
                                                   AutoFormatType=1 }
  }
  KEYS
  {
    {    ;Code                                    ;Clustered=Yes }
  }
  FIELDGROUPS
  {
    { 1   ;Brick               ;Code,Name,Location Code                  }
  }
  CODE
  {
    VAR
      PostCode@1000 : Record 225;
      DimMgt@1001 : Codeunit 408;

    LOCAL PROCEDURE ValidateShortcutDimCode@29(FieldNumber@1000 : Integer;VAR ShortcutDimCode@1001 : Code[20]);
    BEGIN
      DimMgt.ValidateDimValueCode(FieldNumber,ShortcutDimCode);
      DimMgt.SaveDefaultDim(DATABASE::"Responsibility Center",Code,FieldNumber,ShortcutDimCode);
      MODIFY;
    END;

    BEGIN
    END.
  }
}

