OBJECT Table 31022938 Bill Group
{
  OBJECT-PROPERTIES
  {
    Date=01/02/15;
    Time=13:00:00;
    Version List=NAVPTSS81.00;
  }
  PROPERTIES
  {
    OnInsert=BEGIN
               IF "No." = '' THEN BEGIN
                 CarteraSetup.GET;
                 CarteraSetup.TESTFIELD("Bill Group Nos.");
                 NoSeriesMgt.InitSeries(CarteraSetup."Bill Group Nos.",xRec."No. Series",0D,"No.","No. Series");
               END;

               IF GETFILTER("Bank Account No.") <> '' THEN
                 IF GETRANGEMIN("Bank Account No.") = GETRANGEMAX("Bank Account No.") THEN BEGIN
                   Option := STRMENU(Text31022890);
                   CASE Option OF
                     0:
                       ERROR(Text31022891,TABLECAPTION);
                     1:
                       "Dealing Type" := "Dealing Type"::Collection;
                     2:
                       "Dealing Type" := "Dealing Type"::Discount;
                   END;
                   BankAcc.GET(GETRANGEMIN("Bank Account No."));
                   VALIDATE("Currency Code",BankAcc."Currency Code");
                   VALIDATE("Bank Account No.",BankAcc."No.");
                 END;

               CheckNoNotUsed;
               UpdateDescription;
               "Posting Date" := WORKDATE;
             END;

    OnDelete=BEGIN
               Doc.SETCURRENTKEY(Type,"Bill Gr./Pmt. Order No.");
               Doc.SETRANGE(Type,Doc.Type::Receivable);
               Doc.SETRANGE("Bill Gr./Pmt. Order No.","No.");
               IF NOT Doc.ISEMPTY THEN
                 ERROR(Text31022892);

               BGPOCommentLine.SETRANGE("BG/PO No.","No.");
               BGPOCommentLine.DELETEALL;
             END;

    CaptionML=[ENU=Bill Group;
               PTG=Remessa];
    LookupPageID=Page31022956;
    DrillDownPageID=Page31022956;
  }
  FIELDS
  {
    { 2   ;   ;No.                 ;Code20        ;OnValidate=BEGIN
                                                                IF "No." = xRec."No." THEN
                                                                  EXIT;

                                                                CheckPrinted;
                                                                ResetPrinted;

                                                                CarteraSetup.GET;
                                                                NoSeriesMgt.TestManual(CarteraSetup."Bill Group Nos.");
                                                                "No. Series" := '';

                                                                UpdateDescription;
                                                                CheckNoNotUsed;
                                                              END;

                                                   CaptionML=[ENU=No.;
                                                              PTG=N�] }
    { 3   ;   ;Bank Account No.    ;Code20        ;TableRelation="Bank Account";
                                                   OnValidate=BEGIN
                                                                CALCFIELDS("Bank Account Name");

                                                                IF "Bank Account No." = '' THEN
                                                                  EXIT;

                                                                BankAcc.GET("Bank Account No.");
                                                                BankAcc.TESTFIELD(Blocked, FALSE);

                                                                IF BillGrIsEmpty THEN BEGIN
                                                                  VALIDATE("Currency Code",BankAcc."Currency Code");
                                                                  EXIT;
                                                                END;

                                                                BankAcc.TESTFIELD("Currency Code","Currency Code");

                                                                CALCFIELDS(Amount);
                                                                IF (Amount <> 0) AND (Factoring = Factoring::" ") THEN
                                                                  CarteraManagement.CheckDiscCreditLimit(Rec);

                                                                IF "Bank Account No." <> xRec."Bank Account No." THEN BEGIN
                                                                  CheckPrinted;
                                                                  ResetPrinted;
                                                                END;
                                                              END;

                                                   CaptionML=[ENU=Bank Account No.;
                                                              PTG=N� Conta Banc�ria] }
    { 4   ;   ;Bank Account Name   ;Text50        ;FieldClass=FlowField;
                                                   CalcFormula=Lookup("Bank Account".Name WHERE (No.=FIELD(Bank Account No.)));
                                                   CaptionML=[ENU=Bank Account Name;
                                                              PTG=Nome Conta Banc�ria];
                                                   Editable=No }
    { 5   ;   ;Posting Description ;Text50        ;CaptionML=[ENU=Posting Description;
                                                              PTG=Texto Registo] }
    { 6   ;   ;Dealing Type        ;Option        ;OnValidate=BEGIN
                                                                VALIDATE("Currency Code");

                                                                CALCFIELDS(Amount);
                                                                IF (Factoring = Factoring::" ") AND (Amount <> 0) THEN
                                                                  CarteraManagement.CheckDiscCreditLimit(Rec);

                                                                IF "Dealing Type" <> xRec."Dealing Type" THEN BEGIN
                                                                  CheckPrinted;
                                                                  ResetPrinted;
                                                                END;
                                                              END;

                                                   CaptionML=[ENU=Dealing Type;
                                                              PTG=Tipo Gest�o];
                                                   OptionCaptionML=[ENU=Collection,Discount;
                                                                    PTG=Cobran�a,Desconto];
                                                   OptionString=Collection,Discount }
    { 7   ;   ;Amount              ;Decimal       ;FieldClass=FlowField;
                                                   CalcFormula=Sum("Cartera Doc."."Remaining Amount" WHERE (Bill Gr./Pmt. Order No.=FIELD(No.),
                                                                                                            Global Dimension 1 Code=FIELD(Global Dimension 1 Filter),
                                                                                                            Global Dimension 2 Code=FIELD(Global Dimension 2 Filter),
                                                                                                            Category Code=FIELD(Category Filter),
                                                                                                            Due Date=FIELD(Due Date Filter),
                                                                                                            Type=CONST(Receivable)));
                                                   CaptionML=[ENU=Amount;
                                                              PTG=Valor];
                                                   Editable=No;
                                                   AutoFormatType=1;
                                                   AutoFormatExpr="Currency Code" }
    { 8   ;   ;Reason Code         ;Code10        ;TableRelation="Reason Code";
                                                   CaptionML=[ENU=Reason Code;
                                                              PTG=C�d. Auditoria] }
    { 9   ;   ;No. Printed         ;Integer       ;CaptionML=[ENU=No. Printed;
                                                              PTG=N� ImpressSes] }
    { 10  ;   ;Posting Date        ;Date          ;OnValidate=BEGIN
                                                                IF "Posting Date" <> xRec."Posting Date" THEN BEGIN
                                                                  CheckPrinted;
                                                                  ResetPrinted;
                                                                END;
                                                              END;

                                                   CaptionML=[ENU=Posting Date;
                                                              PTG=Data Registo] }
    { 11  ;   ;Comment             ;Boolean       ;FieldClass=FlowField;
                                                   CalcFormula=Exist("BG/PO Comment Line" WHERE (BG/PO No.=FIELD(No.),
                                                                                                 Type=FILTER(Receivable)));
                                                   CaptionML=[ENU=Comment;
                                                              PTG=Coment�rio];
                                                   Editable=No }
    { 12  ;   ;Category Filter     ;Code10        ;FieldClass=FlowFilter;
                                                   TableRelation="Category Code";
                                                   ValidateTableRelation=No;
                                                   CaptionML=[ENU=Category Filter;
                                                              PTG=Filtro Categoria] }
    { 13  ;   ;Due Date Filter     ;Date          ;FieldClass=FlowFilter;
                                                   CaptionML=[ENU=Due Date Filter;
                                                              PTG=Filtro Data Vencimento] }
    { 14  ;   ;Global Dimension 1 Filter;Code20   ;FieldClass=FlowFilter;
                                                   TableRelation="Dimension Value".Code WHERE (Global Dimension No.=CONST(1));
                                                   CaptionML=[ENU=Global Dimension 1 Filter;
                                                              PTG=Filtro Dimens�o 1 Global];
                                                   CaptionClass='1,3,1' }
    { 15  ;   ;Global Dimension 2 Filter;Code20   ;FieldClass=FlowFilter;
                                                   TableRelation="Dimension Value".Code WHERE (Global Dimension No.=CONST(2));
                                                   CaptionML=[ENU=Global Dimension 2 Filter;
                                                              PTG=Filtro Dimens�o 2 Global];
                                                   CaptionClass='1,3,2' }
    { 16  ;   ;No. Series          ;Code10        ;TableRelation="No. Series";
                                                   CaptionML=[ENU=No. Series;
                                                              PTG=N� S�ries];
                                                   Editable=No }
    { 33  ;   ;Currency Code       ;Code10        ;TableRelation=Currency;
                                                   OnValidate=BEGIN
                                                                IF BankAcc.GET("Bank Account No.") AND
                                                                  ("Currency Code" <> BankAcc."Currency Code")
                                                                THEN
                                                                  TESTFIELD("Currency Code",BankAcc."Currency Code");

                                                                IF ("Currency Code" <> xRec."Currency Code") AND
                                                                  NOT BillGrIsEmpty THEN
                                                                    FIELDERROR(
                                                                      "Currency Code",
                                                                      STRSUBSTNO(
                                                                        Text31022893,
                                                                        TABLECAPTION));

                                                                IF "Currency Code" <> '' THEN BEGIN
                                                                  Currency.RESET;
                                                                  IF "Dealing Type" = "Dealing Type"::Discount THEN
                                                                    Currency.SETRANGE("Bill Groups - Discount",TRUE)
                                                                  ELSE
                                                                    Currency.SETRANGE("Bill Groups - Collection",TRUE);
                                                                  Currency.Code := "Currency Code";
                                                                  IF Currency.ISEMPTY THEN
                                                                    ERROR(
                                                                      Text31022894,
                                                                      "Currency Code");
                                                                END;

                                                                IF "Currency Code" <> xRec."Currency Code" THEN BEGIN
                                                                  CheckPrinted;
                                                                  ResetPrinted;
                                                                END;
                                                              END;

                                                   OnLookup=BEGIN
                                                              Currency.RESET;

                                                              IF Currency.GET("Currency Code") THEN;
                                                              Currencies.SETRECORD(Currency);
                                                              Currencies.LOOKUPMODE(TRUE);
                                                              IF ACTION::LookupOK = Currencies.RUNMODAL THEN BEGIN
                                                                Currencies.GETRECORD(Currency);
                                                                CLEAR(Currencies);
                                                                VALIDATE("Currency Code",Currency.Code);
                                                              END ELSE
                                                                CLEAR(Currencies);
                                                            END;

                                                   CaptionML=[ENU=Currency Code;
                                                              PTG=C�d. Divisa] }
    { 34  ;   ;Amount (LCY)        ;Decimal       ;FieldClass=FlowField;
                                                   CalcFormula=Sum("Cartera Doc."."Remaining Amt. (LCY)" WHERE (Bill Gr./Pmt. Order No.=FIELD(No.),
                                                                                                                Global Dimension 1 Code=FIELD(Global Dimension 1 Filter),
                                                                                                                Global Dimension 2 Code=FIELD(Global Dimension 2 Filter),
                                                                                                                Category Code=FIELD(Category Filter),
                                                                                                                Due Date=FIELD(Due Date Filter),
                                                                                                                Type=CONST(Receivable)));
                                                   CaptionML=[ENU=Amount (LCY);
                                                              PTG=Valor (DL)];
                                                   Editable=No;
                                                   AutoFormatType=1 }
    { 39  ;   ;Factoring           ;Option        ;OnValidate=BEGIN
                                                                Doc.RESET;
                                                                Doc.SETCURRENTKEY(Type,"Collection Agent","Bill Gr./Pmt. Order No.","Currency Code",Accepted,"Due Date",Place,"Document Type");
                                                                Doc.SETRANGE(Type,Doc.Type::Receivable);
                                                                Doc.SETRANGE("Collection Agent",Doc."Collection Agent"::Bank);
                                                                Doc.SETRANGE("Bill Gr./Pmt. Order No.","No.");
                                                                IF (Factoring = Factoring::" ") THEN BEGIN
                                                                  Doc.SETFILTER("Document Type",'<>%1',Doc."Document Type"::Bill);
                                                                  IF NOT Doc.ISEMPTY THEN
                                                                    ERROR(Text31022895);
                                                                END ELSE BEGIN
                                                                  Doc.SETFILTER("Document Type",'%1',Doc."Document Type"::Bill);
                                                                  IF NOT Doc.ISEMPTY THEN
                                                                    ERROR(Text31022896);
                                                                END;
                                                              END;

                                                   CaptionML=[ENU=Factoring;
                                                              PTG=Factoring];
                                                   OptionCaptionML=[ENU=" ,Unrisked,Risked";
                                                                    PTG=" ,Sem Risco,Com Risco"];
                                                   OptionString=[ ,Unrisked,Risked] }
    { 40  ;   ;Partner Type        ;Option        ;CaptionML=[ENU=Partner Type;
                                                              PTG=Tipo Parceiro];
                                                   OptionCaptionML=[ENU=" ,Company,Person";
                                                                    PTG=" ,Empresa,Pessoa"];
                                                   OptionString=[ ,Company,Person] }
  }
  KEYS
  {
    {    ;No.                                     ;Clustered=Yes }
    {    ;Bank Account No.                         }
  }
  FIELDGROUPS
  {
  }
  CODE
  {
    VAR
      Text31022890@1000 : TextConst 'ENU=&Collection,&Discount;PTG=&Cobran�a,&Desconto';
      Text31022891@1001 : TextConst 'ENU=The creation of a new %1 was cancelled by the user;PTG=A cria��o de um(a) novo(a) %1 foi cancelado pelo utilizador';
      Text31022892@1002 : TextConst 'ENU=This Bill Group is not empty. Remove all its bills and try again.;PTG=Esta Remessa n�o est� vazia. Elimine todos seus t�tulos e tente de novo.';
      Text31022893@1003 : TextConst 'ENU=can only be changed when the %1 is empty;PTG=s� pode mudar quando %1 � vazio';
      Text31022894@1004 : TextConst 'ENU=The operation is not allowed for bill groups using %1. Check your currency setup.;PTG=A opera��o n�o � permitida para t�tulos que utilizam %1. Verifique a configura��o da divisa.';
      Text31022895@1005 : TextConst 'ENU=Invoices should be removed.;PTG=As faturas devem ser removidas.';
      Text31022896@1006 : TextConst 'ENU=Bills should be removed.;PTG=Os t�tulos devem ser removidos.';
      Text31022897@1007 : TextConst 'ENU=This Bill Group has already been printed. Proceed anyway?;PTG=A Remessa j� foi impressa. Confirma que deseja continuar?';
      Text31022898@1008 : TextConst 'ENU=The update has been interrupted by the user.;PTG=A atualiza��o foi interrompida pelo utilizador.';
      Text31022899@1009 : TextConst 'ENU=Bill Group;PTG=Remessa';
      Text31022900@1010 : TextConst 'ENU=" is currently in use in a Posted Bill Group.";PTG=" est� atualmente em utiliza��o numa Remessa Registada."';
      Text31022901@1011 : TextConst 'ENU=" is currently in use in a Closed Bill Group.";PTG=" est� atualmente em utiliza��o numa Remessa Fechada."';
      Text31022902@1012 : TextConst 'ENU=untitled;PTG=SemT�tulo';
      BillGr@1110001 : Record 31022938;
      PostedBillGr@1110002 : Record 31022939;
      ClosedBillGr@1110003 : Record 31022940;
      Doc@1110004 : Record 31022935;
      CarteraSetup@1110005 : Record 31022949;
      Currency@1110006 : Record 4;
      BankAcc@1110007 : Record 270;
      BGPOCommentLine@1110008 : Record 31022941;
      CarteraManagement@1110009 : Codeunit 31022901;
      NoSeriesMgt@1110010 : Codeunit 396;
      Currencies@1110011 : Page 5;
      Option@1110012 : Integer;
      DirectDebitOptionTxt@31022892 : TextConst 'ENU=Direct Debit;PTG=D�bito Direto';
      InvoiceDiscountingOptionTxt@31022891 : TextConst 'ENU=Invoice Discounting;ESP=Operaci�n de cesi�n de cr�dito;PTG=Desconto Fatura';
      InstructionTxt@31022890 : TextConst 'ENU=Select which format to use.;ESP=Seleccione el formato que quiere usar.;PTG=Selecione o formato a usar.';
      SilentDirectDebitFormat@31022894 : ' ,Standard,N58';
      DirectDebitFormatSilentlySelected@31022893 : Boolean;

    PROCEDURE AssistEdit@3(OldBillGr@1110000 : Record 31022938) : Boolean;
    BEGIN
      WITH BillGr DO BEGIN
        BillGr := Rec;
        CarteraSetup.GET;
        CarteraSetup.TESTFIELD("Bill Group Nos.");
        IF NoSeriesMgt.SelectSeries(CarteraSetup."Bill Group Nos.",OldBillGr."No. Series","No. Series") THEN BEGIN
          CarteraSetup.GET;
          CarteraSetup.TESTFIELD("Bill Group Nos.");
          NoSeriesMgt.SetSeries("No.");
          Rec := BillGr;
          EXIT(TRUE);
        END;
      END;
    END;

    LOCAL PROCEDURE CheckPrinted@1();
    BEGIN
      IF "No. Printed" <> 0 THEN
        IF NOT CONFIRM(Text31022897) THEN
          ERROR(Text31022898);
    END;

    PROCEDURE ResetPrinted@2();
    BEGIN
      "No. Printed" := 0;
    END;

    LOCAL PROCEDURE UpdateDescription@4();
    BEGIN
      "Posting Description" := Text31022899 + ' ' + "No.";
    END;

    LOCAL PROCEDURE CheckNoNotUsed@5();
    BEGIN
      IF PostedBillGr.GET("No.") THEN
        FIELDERROR("No.",PostedBillGr."No." + Text31022900);
      IF ClosedBillGr.GET("No.") THEN
        FIELDERROR("No.",ClosedBillGr."No." + Text31022901);
    END;

    PROCEDURE PrintRecords@6(ShowRequestForm@1110000 : Boolean);
    VAR
      CarteraReportSelection@1110001 : Record 31022946;
    BEGIN
      WITH BillGr DO BEGIN
        COPY(Rec);
        CarteraReportSelection.SETRANGE(Usage,CarteraReportSelection.Usage::"Bill Group");
        CarteraReportSelection.SETFILTER("Report ID",'<>0');
        CarteraReportSelection.FINDSET;
        REPEAT
          REPORT.RUNMODAL(CarteraReportSelection."Report ID",ShowRequestForm,FALSE,BillGr);
        UNTIL CarteraReportSelection.NEXT = 0;
      END;
    END;

    LOCAL PROCEDURE BillGrIsEmpty@7() : Boolean;
    BEGIN
      Doc.SETCURRENTKEY(Type,"Bill Gr./Pmt. Order No.");
      Doc.SETRANGE(Type,Doc.Type::Receivable);
      Doc.SETRANGE("Bill Gr./Pmt. Order No.","No.");
      EXIT(Doc.ISEMPTY);
    END;

    PROCEDURE Caption@9() : Text[100];
    VAR
      BankAcc@1110000 : Record 270;
    BEGIN
      IF "No." = '' THEN
        EXIT(Text31022902);
      CALCFIELDS("Bank Account Name");
      EXIT(STRSUBSTNO('%1 %2',"No.","Bank Account Name"));
    END;

    PROCEDURE ExportToFile@1100000();
    VAR
      DirectDebitCollection@1100002 : Record 1207;
      DirectDebitCollectionEntry@1100000 : Record 1208;
      BankAccount@1100001 : Record 270;
    BEGIN
      DirectDebitCollection.CreateNew("No.","Bank Account No.","Partner Type");
      DirectDebitCollection."Source Table ID" := DATABASE::"Bill Group";
      DirectDebitCollection.MODIFY;
      CheckSEPADirectDebitFormat(DirectDebitCollection);
      BankAccount.GET("Bank Account No.");
      COMMIT;
      DirectDebitCollectionEntry.SETRANGE("Direct Debit Collection No.",DirectDebitCollection."No.");
      RunFileExportCodeunit(BankAccount.GetDDExportCodeunitID,DirectDebitCollection."No.",DirectDebitCollectionEntry);
      DeleteDirectDebitCollection(DirectDebitCollection."No.");
    END;

    PROCEDURE RunFileExportCodeunit@1100001(CodeunitID@1100000 : Integer;DirectDebitCollectionNo@1100003 : Integer;VAR DirectDebitCollectionEntry@1100001 : Record 1208);
    VAR
      LastError@1100002 : Text;
    BEGIN
      IF NOT CODEUNIT.RUN(CodeunitID,DirectDebitCollectionEntry) THEN BEGIN
        LastError := GETLASTERRORTEXT;
        DeleteDirectDebitCollection(DirectDebitCollectionNo);
        COMMIT;
        ERROR(LastError);
      END;
    END;

    PROCEDURE DeleteDirectDebitCollection@1100002(DirectDebitCollectionNo@1100000 : Integer);
    VAR
      DirectDebitCollection@1100001 : Record 1207;
    BEGIN
      IF DirectDebitCollection.GET(DirectDebitCollectionNo) THEN
        DirectDebitCollection.DELETE(TRUE);
    END;

    PROCEDURE SelectDirectDebitFormatSilently@1100006(NewDirectDebitFormat@1100000 : Option);
    BEGIN
      SilentDirectDebitFormat := NewDirectDebitFormat;
      DirectDebitFormatSilentlySelected := TRUE;
    END;

    LOCAL PROCEDURE CheckSEPADirectDebitFormat@1100004(VAR DirectDebitCollection@1100002 : Record 1207);
    VAR
      BankAccount@1100003 : Record 270;
      DirectDebitFormat@1100000 : Option;
      Selection@1100001 : Integer;
    BEGIN
      BankAccount.GET("Bank Account No.");
      IF BankAccount.GetDDExportCodeunitID = CODEUNIT::"SEPA DD-Export File" THEN BEGIN
        IF NOT DirectDebitFormatSilentlySelected THEN BEGIN
          Selection := STRMENU(STRSUBSTNO('%1,%2',DirectDebitOptionTxt,InvoiceDiscountingOptionTxt),1,InstructionTxt);

          IF Selection = 0 THEN
            EXIT;

          CASE Selection OF
            1:
              DirectDebitFormat := DirectDebitCollection."Direct Debit Format"::Standard;
            2:
              DirectDebitFormat := DirectDebitCollection."Direct Debit Format"::N58;
          END;
        END ELSE
          DirectDebitFormat := SilentDirectDebitFormat;

        DirectDebitCollection."Direct Debit Format" := DirectDebitFormat;
        DirectDebitCollection.MODIFY;
      END;
    END;

    BEGIN
    END.
  }
}

