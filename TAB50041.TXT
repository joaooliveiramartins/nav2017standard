OBJECT Table 50041 OCR Vendor Prod.Rel.
{
  OBJECT-PROPERTIES
  {
    Date=22/12/16;
    Time=22:12:43;
    Modified=Yes;
    Version List=OCR;
  }
  PROPERTIES
  {
  }
  FIELDS
  {
    { 1   ;   ;Standard Purchase Code;Code20      ;TableRelation="Standard Purchase Code";
                                                   ValidateTableRelation=No;
                                                   TestTableRelation=No;
                                                   CaptionML=[ENU=Standard Purchase Code;
                                                              PTG=C�digo Compra Padr�o];
                                                   Editable=No }
    { 2   ;   ;Line No.            ;Integer       ;CaptionML=[ENU=Line No.;
                                                              PTG=N� Linha];
                                                   Editable=No }
    { 3   ;   ;Type                ;Option        ;OnValidate=VAR
                                                                OldType@1000 : Integer;
                                                              BEGIN
                                                              END;

                                                   CaptionML=[ENU=Type;
                                                              PTG=Tipo];
                                                   OptionCaptionML=[ENU=" ,G/L Account,Item,,Fixed Asset,Charge (Item)";
                                                                    PTG=" ,Conta CG,Produto,,Ativos Fixos,Encargo (Prod.)"];
                                                   OptionString=[ ,G/L Account,Item,,Fixed Asset,Charge (Item)] }
    { 4   ;   ;No.                 ;Code20        ;TableRelation=IF (Type=CONST(" ")) "Standard Text"
                                                                 ELSE IF (Type=CONST(G/L Account)) "G/L Account"
                                                                 ELSE IF (Type=CONST(Item)) Item
                                                                 ELSE IF (Type=CONST(3)) Resource
                                                                 ELSE IF (Type=CONST(Fixed Asset)) "Fixed Asset"
                                                                 ELSE IF (Type=CONST("Charge (Item)")) "Item Charge";
                                                   OnValidate=VAR
                                                                GLAcc@1001 : Record 15;
                                                                Item@1002 : Record 27;
                                                                ItemCharge@1006 : Record 5800;
                                                                FA@1003 : Record 5600;
                                                                StdTxt@1004 : Record 7;
                                                                StdPurchCode@1000 : Record 173;
                                                              BEGIN
                                                              END;

                                                   CaptionML=[ENU=No.;
                                                              PTG=N�] }
    { 5   ;   ;Description         ;Text50        ;CaptionML=[ENU=Description;
                                                              PTG=Descri��o] }
    { 6   ;   ;Quantity            ;Decimal       ;CaptionML=[ENU=Quantity;
                                                              PTG=Quantidade];
                                                   DecimalPlaces=0:5;
                                                   BlankZero=Yes }
    { 7   ;   ;Amount Excl. VAT    ;Decimal       ;CaptionML=[ENU=Amount Excl. VAT;
                                                              PTG=Valor Excl. IVA];
                                                   BlankZero=Yes;
                                                   AutoFormatType=2 }
    { 8   ;   ;Unit of Measure Code;Code10        ;TableRelation=IF (Type=CONST(Item)) "Item Unit of Measure".Code WHERE (Item No.=FIELD(No.))
                                                                 ELSE "Unit of Measure";
                                                   CaptionML=[ENU=Unit of Measure Code;
                                                              PTG=C�d. Unidade Medida] }
    { 9   ;   ;Shortcut Dimension 1 Code;Code20   ;TableRelation="Dimension Value".Code WHERE (Global Dimension No.=CONST(1));
                                                   CaptionML=[ENU=Shortcut Dimension 1 Code;
                                                              PTG=C�d. Atalho Dimens�o 1];
                                                   CaptionClass='1,2,1' }
    { 10  ;   ;Shortcut Dimension 2 Code;Code20   ;TableRelation="Dimension Value".Code WHERE (Global Dimension No.=CONST(2));
                                                   CaptionML=[ENU=Shortcut Dimension 2 Code;
                                                              PTG=C�d. Atalho Dimens�o 2];
                                                   CaptionClass='1,2,2' }
    { 11  ;   ;Variant Code        ;Code10        ;TableRelation=IF (Type=CONST(Item)) "Item Variant".Code WHERE (Item No.=FIELD(No.));
                                                   OnValidate=VAR
                                                                Item@1001 : Record 27;
                                                                ItemVariant@1000 : Record 5401;
                                                              BEGIN
                                                              END;

                                                   CaptionML=[ENU=Variant Code;
                                                              PTG=C�d. Variante] }
    { 86  ;   ;VAT Registration No.;Text20        ;OnValidate=VAR
                                                                VATRegNoFormat@1000 : Record 381;
                                                              BEGIN
                                                              END;

                                                   CaptionML=[ENU=VAT Registration No.;
                                                              PTG=N� Contribuinte] }
    { 480 ;   ;Dimension Set ID    ;Integer       ;TableRelation="Dimension Set Entry";
                                                   CaptionML=[ENU=Dimension Set ID;
                                                              PTG=ID Conj. Dimens�es];
                                                   Editable=No }
    { 75001;  ;OCR Batch ID        ;Text30        ;CaptionML=[ENU=OCR Batch ID;
                                                              PTG=OCR Batch ID] }
    { 75120;  ;Amount Type         ;Option        ;CaptionML=[ENU=Amount Type;
                                                              PTG=Tipo de Valor];
                                                   OptionCaptionML=[ENU=Percent,Fixed;
                                                                    PTG=Percentagem,Fixo];
                                                   OptionString=Percent,Fixed }
    { 75121;  ;VAT Bus. Posting Group;Code10      ;TableRelation="VAT Business Posting Group";
                                                   CaptionML=[ENU=VAT Bus. Posting Group;
                                                              PTG=Gr. Registo IVA Neg�cio] }
    { 75122;  ;VAT Prod. Posting Group;Code10     ;TableRelation="VAT Product Posting Group";
                                                   CaptionML=[ENU=VAT Prod. Posting Group;
                                                              PTG=Gr. Registo IVA Produto] }
  }
  KEYS
  {
    {    ;OCR Batch ID,Standard Purchase Code     ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {
    VAR
      recStandardPurchaseLine@1000 : Record 174;
      recOCRStandardPurchaseLine@1001 : Record 50041;

    PROCEDURE processStandardPurchaseLine@1000000000(pCode@1001 : Code[20];pBatchID@1002 : Text[30]);
    VAR
      recChanged@1000 : Boolean;
    BEGIN
      recOCRStandardPurchaseLine.SETRANGE("OCR Batch ID", pBatchID);
      recOCRStandardPurchaseLine.SETRANGE("Standard Purchase Code", pCode);
      IF recOCRStandardPurchaseLine.FINDSET THEN BEGIN
        REPEAT
          IF NOT recStandardPurchaseLine.GET(recOCRStandardPurchaseLine."Standard Purchase Code", recOCRStandardPurchaseLine."Line No.") THEN BEGIN
            recStandardPurchaseLine.INIT;
            recStandardPurchaseLine.TRANSFERFIELDS(recOCRStandardPurchaseLine, TRUE);
            recStandardPurchaseLine.INSERT;
          END;
        UNTIL recOCRStandardPurchaseLine.NEXT = 0;
      END;
    END;

    BEGIN
    END.
  }
}

