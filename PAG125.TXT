OBJECT Page 125 Comment List
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0;
  }
  PROPERTIES
  {
    Editable=No;
    CaptionML=[ENU=Comment List;
               PTG=Lista Coment rios];
    LinksAllowed=No;
    SourceTable=Table97;
    DataCaptionFields=No.;
    PageType=List;
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 2   ;2   ;Field     ;
                ToolTipML=ENU=Specifies the number of the account, bank account, customer, vendor, or item to which the comment applies.;
                SourceExpr="No." }

    { 4   ;2   ;Field     ;
                ToolTipML=ENU=Specifies the date the comment was created.;
                SourceExpr=Date }

    { 6   ;2   ;Field     ;
                ToolTipML=ENU=Specifies the comment itself.;
                SourceExpr=Comment }

    { 11  ;2   ;Field     ;
                ToolTipML=ENU=Specifies a code for the comment.;
                SourceExpr=Code;
                Visible=FALSE }

  }
  CODE
  {

    BEGIN
    END.
  }
}

