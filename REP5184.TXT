OBJECT Report 5184 Apply Mailing Group
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Apply Mailing Group;
               PTG=Aplicar Grupo Destino];
    ProcessingOnly=Yes;
    OnPreReport=BEGIN
                  MailingGroupCode := "Mailing Group".GETFILTER(Code);
                  IF NOT "Mailing Group".GET(MailingGroupCode) THEN
                    ERROR(Text000);
                END;

    OnPostReport=BEGIN
                   MESSAGE(
                     Text001,
                     "Mailing Group".TABLECAPTION,MailingGroupCode,"Segment Header"."No.");
                 END;

  }
  DATASET
  {
    { 6043;    ;DataItem;                    ;
               DataItemTable=Table5056;
               DataItemTableView=SORTING(Mailing Group Code);
               OnPreDataItem=BEGIN
                               IF NOT DeleteOld THEN
                                 CurrReport.BREAK;

                               SETRANGE("Mailing Group Code",MailingGroupCode);
                             END;

               OnAfterGetRecord=BEGIN
                                  DELETE;
                                END;
                                 }

    { 7133;    ;DataItem;                    ;
               DataItemTable=Table5076;
               DataItemTableView=SORTING(No.) }

    { 5030;1   ;DataItem;                    ;
               DataItemTable=Table5077;
               DataItemTableView=SORTING(Segment No.,Line No.);
               DataItemLink=Segment No.=FIELD(No.) }

    { 1563;2   ;DataItem;                    ;
               DataItemTable=Table5055;
               DataItemTableView=SORTING(Code);
               OnAfterGetRecord=BEGIN
                                  CLEAR("Contact Mailing Group");
                                  "Contact Mailing Group"."Contact No." := "Segment Line"."Contact No.";
                                  "Contact Mailing Group"."Mailing Group Code" := Code;
                                  IF "Contact Mailing Group".INSERT THEN;
                                END;

               ReqFilterFields=Code }

  }
  REQUESTPAGE
  {
    PROPERTIES
    {
    }
    CONTROLS
    {
      { 1900000001;0;Container;
                  ContainerType=ContentArea }

      { 1900000002;1;Group  ;
                  CaptionML=[ENU=Options;
                             PTG=Op��es] }

      { 1   ;2   ;Field     ;
                  CaptionML=[ENU=Delete Old Assignments;
                             PTG=Eliminar Atribui��es Antigas];
                  ToolTipML=ENU=Specifies if the previous contacts that were assigned to the mailing group are removed.;
                  ApplicationArea=#RelationshipMgmt;
                  SourceExpr=DeleteOld }

    }
  }
  LABELS
  {
  }
  CODE
  {
    VAR
      Text000@1000 : TextConst 'ENU=Specify a Mailing Group Code.;PTG=Especifique um C�d. Grupo Destino.';
      Text001@1001 : TextConst 'ENU=%1 %2 is now applied to Segment %3.;PTG=%1 %2 est� agora aplicado ao Segmento %3.';
      DeleteOld@1002 : Boolean;
      MailingGroupCode@1003 : Code[10];

    BEGIN
    END.
  }
  RDLDATA
  {
  }
}

