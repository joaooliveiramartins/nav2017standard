OBJECT Table 6635 Return Reason
{
  OBJECT-PROPERTIES
  {
    Date=15/09/15;
    Time=12:00:00;
    Version List=NAVW19.00;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Return Reason;
               PTG=Raz�o Devolu��o];
    LookupPageID=Page6635;
    DrillDownPageID=Page6635;
  }
  FIELDS
  {
    { 1   ;   ;Code                ;Code10        ;CaptionML=[ENU=Code;
                                                              PTG=C�digo];
                                                   NotBlank=Yes }
    { 2   ;   ;Description         ;Text50        ;CaptionML=[ENU=Description;
                                                              PTG=Descri��o] }
    { 3   ;   ;Default Location Code;Code10       ;TableRelation=Location WHERE (Use As In-Transit=CONST(No));
                                                   CaptionML=[ENU=Default Location Code;
                                                              PTG=C�d. Localiza��o Padr�o] }
    { 4   ;   ;Inventory Value Zero;Boolean       ;CaptionML=[ENU=Inventory Value Zero;
                                                              PTG=Invent�rio Valor Zero] }
  }
  KEYS
  {
    {    ;Code                                    ;Clustered=Yes }
  }
  FIELDGROUPS
  {
    { 1   ;DropDown            ;Code,Description,Default Location Code,Inventory Value Zero }
  }
  CODE
  {

    BEGIN
    END.
  }
}

