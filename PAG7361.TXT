OBJECT Page 7361 Whse. Internal Put-away Lines
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0;
  }
  PROPERTIES
  {
    Editable=No;
    CaptionML=[ENU=Whse. Internal Put-away Lines;
               PTG=Linhas Arrumar Interno Armaz�m];
    SourceTable=Table7332;
    PageType=List;
    ActionList=ACTIONS
    {
      { 1900000003;0 ;ActionContainer;
                      ActionContainerType=RelatedInformation }
      { 6       ;1   ;ActionGroup;
                      CaptionML=[ENU=&Line;
                                 PTG=&Linha];
                      Image=Line }
      { 7       ;2   ;Action    ;
                      ShortCutKey=Shift+F7;
                      CaptionML=[ENU=Show Whse. Document;
                                 PTG=Mostrar Documento Armaz�m];
                      Image=ViewOrder;
                      OnAction=VAR
                                 WhseInternalPutawayHeader@1000 : Record 7331;
                               BEGIN
                                 WhseInternalPutawayHeader.GET("No.");
                                 PAGE.RUN(PAGE::"Whse. Internal Put-away",WhseInternalPutawayHeader);
                               END;
                                }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 16  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the code for the location of the internal put-away line.;
                           PTG=""];
                SourceExpr="Location Code";
                Visible=FALSE }

    { 22  ;2   ;Field     ;
                ToolTipML=[ENU=Indicates the zone from which the items to be put away should be taken.;
                           PTG=""];
                SourceExpr="From Zone Code";
                Visible=FALSE }

    { 20  ;2   ;Field     ;
                ToolTipML=[ENU=Indicates the bin from which the items to be put away should be taken.;
                           PTG=""];
                SourceExpr="From Bin Code";
                Visible=FALSE }

    { 8   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the shelf number that is recorded on the item card or the stockkeeping unit card of the item being moved.;
                           PTG=""];
                SourceExpr="Shelf No.";
                Visible=FALSE }

    { 24  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the number of the item that you want to put away and have entered on the line.;
                           PTG=""];
                SourceExpr="Item No." }

    { 50  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the variant number of the item in the line, if any.;
                           PTG=""];
                SourceExpr="Variant Code" }

    { 52  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the description of the item on the line.;
                           PTG=""];
                SourceExpr=Description }

    { 54  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies a second description of the item on the line, if any.;
                           PTG=""];
                SourceExpr="Description 2";
                Visible=FALSE }

    { 26  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the quantity that should be put away.;
                           PTG=""];
                SourceExpr=Quantity }

    { 28  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the quantity that should be put away, in the base unit of measure.;
                           PTG=""];
                SourceExpr="Qty. (Base)" }

    { 30  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the quantity that still needs to be handled.;
                           PTG=""];
                SourceExpr="Qty. Outstanding" }

    { 32  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the quantity that still needs to be handled, expressed in the base unit of measure.;
                           PTG=""];
                SourceExpr="Qty. Outstanding (Base)";
                Visible=FALSE }

    { 46  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the code of unit of measure of the item on the line.;
                           PTG=""];
                SourceExpr="Unit of Measure Code" }

    { 48  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the number of base units of measure, that are in the unit of measure, specified for the item on the line.;
                           PTG=""];
                SourceExpr="Qty. per Unit of Measure" }

    { 58  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the due date of the line.;
                           PTG=""];
                SourceExpr="Due Date" }

    { 2   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the document number.;
                           PTG=""];
                SourceExpr="No." }

    { 4   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the number of the line.;
                           PTG=""];
                SourceExpr="Line No." }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {

    BEGIN
    END.
  }
}

