OBJECT Codeunit 71 Purch.-Disc. (Yes/No)
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0,NAVPTSS81.00;
  }
  PROPERTIES
  {
    TableNo=39;
    OnRun=VAR
            "//---soft-local---//"@31022890 : Integer;
            GLSetup@31022891 : Record 98;
          BEGIN
            //soft,sn
            GLSetup.GET;
            IF GLSetup."Payment Discount Type" <> GLSetup."Payment Discount Type"::"Calc. Pmt. Disc. on Lines" THEN BEGIN
            //soft,en
            IF CONFIRM(Text000,FALSE) THEN
              CODEUNIT.RUN(CODEUNIT::"Purch.-Calc.Discount",Rec);
            //soft,sn
            END ELSE BEGIN
              IF CONFIRM(Text31022890,FALSE) THEN
                CODEUNIT.RUN(CODEUNIT::"Purch.-Calc.Discount",Rec);
            END;
            //soft,en
          END;

  }
  CODE
  {
    VAR
      Text000@1000 : TextConst 'ENU=Do you want to calculate the invoice discount?;PTG=Confirma que deseja calcular o dsct. fatura?';
      "//--soft-text--//"@31022890 : TextConst;
      Text31022890@31022891 : TextConst 'ENU=Do you want to calculate the invoice discount and payment discount?;PTG=Confirma que deseja calcular o Dsct. Fatura e o Dsct. P.P.?';

    BEGIN
    END.
  }
}

