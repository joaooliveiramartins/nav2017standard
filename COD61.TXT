OBJECT Codeunit 61 Sales-Disc. (Yes/No)
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0,NAVPTSS81.00;
  }
  PROPERTIES
  {
    TableNo=37;
    OnRun=VAR
            "//---soft-local---//"@31022890 : Integer;
            GLSetup@31022891 : Record 98;
          BEGIN
            SalesLine.COPY(Rec);
            GLSetup.GET; //soft,n
            WITH SalesLine DO BEGIN
              IF GLSetup."Payment Discount Type" <> GLSetup."Payment Discount Type"::"Calc. Pmt. Disc. on Lines" THEN BEGIN //soft,n
              IF CONFIRM(Text000,FALSE) THEN
                CODEUNIT.RUN(CODEUNIT::"Sales-Calc. Discount",SalesLine);
              //soft,sn
              END ELSE
                IF CONFIRM(Text31022890,FALSE) THEN
                  CODEUNIT.RUN(CODEUNIT::"Sales-Calc. Discount",SalesLine);
              //soft,en
            END;
            Rec := SalesLine;
          END;

  }
  CODE
  {
    VAR
      Text000@1000 : TextConst 'ENU=Do you want to calculate the invoice discount?;PTG=Confirma que deseja calcular o desconto fatura?';
      SalesLine@1001 : Record 37;
      "//--soft-text--//"@31022890 : TextConst;
      Text31022890@31022891 : TextConst 'ENU=Do you want to calculate the invoice discount and payment discount?;PTG=Confirma que deseja calcular o desconto fatura e o desconto. p.p.?';

    BEGIN
    END.
  }
}

