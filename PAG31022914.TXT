OBJECT Page 31022914 Deleted Accounts Setup
{
  OBJECT-PROPERTIES
  {
    Date=01/02/15;
    Time=13:00:00;
    Version List=NAVPTSS81.00;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Deleted Accounts Setup;
               PTG=Configura��o Contas Eliminadas];
    SourceTable=Table31022911;
    PageType=Card;
    OnQueryClosePage=BEGIN
                       IF CloseAction IN [ACTION::Cancel,ACTION::LookupCancel] THEN
                         CancelOnPush;
                       IF CloseAction IN [ACTION::OK,ACTION::LookupOK] THEN
                           OKOnPush;
                     END;

  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1100000;1;Group      }

    { 1100006;2;Field     ;
                CaptionClass=Text19067744;
                MultiLine=Yes }

    { 1100002;2;Field     ;
                SourceExpr="Delete Acc. Old Chart of Acc." }

    { 1100004;2;Field     ;
                SourceExpr="Delete Acc. New Chart of Acc." }

  }
  CODE
  {
    VAR
      Text19067744@19060853 : TextConst 'ENU=Please, select a G/L Account for OLD and NEW Chart of Accounts in order to identify possible entries for which the G/L Account was deleted in the past:;PTG=Selecione uma Conta C/G para Planos de Contas ANTIGO e NOVO, de forma a identificar poss�veis movimentos para os quais a Conta C/G foi apagada no passado:';

    LOCAL PROCEDURE OKOnPush@19066895();
    BEGIN
      CurrPage.CLOSE;
    END;

    LOCAL PROCEDURE CancelOnPush@19054690();
    BEGIN
      CurrPage.CLOSE;
    END;

    BEGIN
    END.
  }
}

