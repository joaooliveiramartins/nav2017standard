OBJECT Table 2100 Sales Document Icon
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0;
  }
  PROPERTIES
  {
    DataPerCompany=No;
    CaptionML=[ENU=Sales Document Icon;
               PTG=�cone Documento de Venda];
  }
  FIELDS
  {
    { 1   ;   ;Type                ;Option        ;CaptionML=[ENU=Type;
                                                              PTG=Tipo];
                                                   OptionCaptionML=[ENU=Quote,Draft Invoice,Unpaid Invoice,Canceled Invoice,Paid Invoice,Overdue Invoice;
                                                                    PTG=Proposta,Fatura Provis�ria,Fatura N�o Paga,Fatura Cancelada,Fatura Paga,Fatura Vencida];
                                                   OptionString=Quote,Draft Invoice,Unpaid Invoice,Canceled Invoice,Paid Invoice,Overdue Invoice }
    { 2   ;   ;Picture             ;MediaSet      ;CaptionML=[ENU=Picture;
                                                              PTG=Fotografia] }
  }
  KEYS
  {
    {    ;Type                                    ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    END.
  }
}

