OBJECT Table 7345 Registered Invt. Movement Line
{
  OBJECT-PROPERTIES
  {
    Date=09/09/14;
    Time=12:00:00;
    Version List=NAVW18.00;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Registered Invt. Movement Line;
               PTG=Linha Mov. Invent�rio Reg.];
  }
  FIELDS
  {
    { 2   ;   ;No.                 ;Code20        ;CaptionML=[ENU=No.;
                                                              PTG=N�] }
    { 3   ;   ;Line No.            ;Integer       ;CaptionML=[ENU=Line No.;
                                                              PTG=N� Linha] }
    { 4   ;   ;Source Type         ;Integer       ;CaptionML=[ENU=Source Type;
                                                              PTG=Tipo Origem] }
    { 5   ;   ;Source Subtype      ;Option        ;CaptionML=[ENU=Source Subtype;
                                                              PTG=Subtipo Origem];
                                                   OptionCaptionML=[ENU=0,1,2,3,4,5,6,7,8,9,10;
                                                                    PTG=0,1,2,3,4,5,6,7,8,9,10];
                                                   OptionString=0,1,2,3,4,5,6,7,8,9,10 }
    { 6   ;   ;Source No.          ;Code20        ;CaptionML=[ENU=Source No.;
                                                              PTG=N� Origem] }
    { 7   ;   ;Source Line No.     ;Integer       ;CaptionML=[ENU=Source Line No.;
                                                              PTG=N� Linha Origem];
                                                   BlankZero=Yes }
    { 8   ;   ;Source Subline No.  ;Integer       ;CaptionML=[ENU=Source Subline No.;
                                                              PTG=N� SubLinha Origem];
                                                   BlankZero=Yes }
    { 9   ;   ;Source Document     ;Option        ;CaptionML=[ENU=Source Document;
                                                              PTG=Documento Origem];
                                                   OptionCaptionML=[ENU=,Sales Order,,,Sales Return Order,Purchase Order,,,Purchase Return Order,Inbound Transfer,Outbound Transfer,Prod. Consumption,Prod. Output,,,,,,,,Assembly Consumption,Assembly Order;
                                                                    PTG=,Enc. Venda,,,Devolu��o Venda,Enc. Compra,,,Devolu��o Compra,Transf. Entrada,Transf. Sa�da,Consumo Prod.,Sa�da Prod.,,,,,,,,Consumo Montagem,Enc. Montagem];
                                                   OptionString=,Sales Order,,,Sales Return Order,Purchase Order,,,Purchase Return Order,Inbound Transfer,Outbound Transfer,Prod. Consumption,Prod. Output,,,,,,,,Assembly Consumption,Assembly Order;
                                                   BlankZero=Yes }
    { 11  ;   ;Location Code       ;Code10        ;TableRelation=Location;
                                                   CaptionML=[ENU=Location Code;
                                                              PTG=C�d. Localiza��o] }
    { 12  ;   ;Shelf No.           ;Code10        ;CaptionML=[ENU=Shelf No.;
                                                              PTG=N� Prateleira] }
    { 13  ;   ;Sorting Sequence No.;Integer       ;CaptionML=[ENU=Sorting Sequence No.;
                                                              PTG=N� Sequ�ncia Ordena��o] }
    { 14  ;   ;Item No.            ;Code20        ;TableRelation=Item;
                                                   CaptionML=[ENU=Item No.;
                                                              PTG=N� Produto] }
    { 15  ;   ;Variant Code        ;Code10        ;TableRelation="Item Variant".Code WHERE (Item No.=FIELD(Item No.));
                                                   CaptionML=[ENU=Variant Code;
                                                              PTG=C�d. Variante] }
    { 16  ;   ;Unit of Measure Code;Code10        ;TableRelation="Item Unit of Measure".Code WHERE (Item No.=FIELD(Item No.));
                                                   CaptionML=[ENU=Unit of Measure Code;
                                                              PTG=C�d. Unidade Medida] }
    { 17  ;   ;Qty. per Unit of Measure;Decimal   ;InitValue=1;
                                                   CaptionML=[ENU=Qty. per Unit of Measure;
                                                              PTG=Qtd. por Unidade Medida];
                                                   DecimalPlaces=0:5 }
    { 18  ;   ;Description         ;Text50        ;CaptionML=[ENU=Description;
                                                              PTG=Descri��o] }
    { 19  ;   ;Description 2       ;Text50        ;CaptionML=[ENU=Description 2;
                                                              PTG=Descri��o 2] }
    { 20  ;   ;Quantity            ;Decimal       ;OnValidate=BEGIN
                                                                "Qty. (Base)" := CalcBaseQty(Quantity);
                                                              END;

                                                   CaptionML=[ENU=Quantity;
                                                              PTG=Quantidade];
                                                   DecimalPlaces=0:5 }
    { 21  ;   ;Qty. (Base)         ;Decimal       ;CaptionML=[ENU=Qty. (Base);
                                                              PTG=Quantidade (Base)];
                                                   DecimalPlaces=0:5 }
    { 31  ;   ;Shipping Advice     ;Option        ;CaptionML=[ENU=Shipping Advice;
                                                              PTG=Envio Parcial/Total];
                                                   OptionCaptionML=[ENU=Partial,Complete;
                                                                    PTG=Parcial,Total];
                                                   OptionString=Partial,Complete }
    { 34  ;   ;Due Date            ;Date          ;CaptionML=[ENU=Due Date;
                                                              PTG=Data Vencimento] }
    { 39  ;   ;Destination Type    ;Option        ;CaptionML=[ENU=Destination Type;
                                                              PTG=Tipo Destino];
                                                   OptionCaptionML=[ENU=" ,Customer,Vendor,Location,Item,Family,Sales Order";
                                                                    PTG=" ,Cliente,Fornecedor,Localiza��o,Produto,Fam�lia,Enc. Venda"];
                                                   OptionString=[ ,Customer,Vendor,Location,Item,Family,Sales Order] }
    { 40  ;   ;Destination No.     ;Code20        ;TableRelation=IF (Destination Type=CONST(Vendor)) Vendor
                                                                 ELSE IF (Destination Type=CONST(Customer)) Customer
                                                                 ELSE IF (Destination Type=CONST(Location)) Location
                                                                 ELSE IF (Destination Type=CONST(Item)) Item
                                                                 ELSE IF (Destination Type=CONST(Family)) Family
                                                                 ELSE IF (Destination Type=CONST(Sales Order)) "Sales Header".No. WHERE (Document Type=CONST(Order));
                                                   CaptionML=[ENU=Destination No.;
                                                              PTG=N� Destino] }
    { 41  ;   ;Whse. Activity No.  ;Code20        ;CaptionML=[ENU=Whse. Activity No.;
                                                              PTG=N� Atividade Arm.] }
    { 42  ;   ;Shipping Agent Code ;Code10        ;TableRelation="Shipping Agent";
                                                   AccessByPermission=TableData 5790=R;
                                                   CaptionML=[ENU=Shipping Agent Code;
                                                              PTG=C�d. Transportador] }
    { 43  ;   ;Shipping Agent Service Code;Code10 ;TableRelation="Shipping Agent Services".Code WHERE (Shipping Agent Code=FIELD(Shipping Agent Code));
                                                   CaptionML=[ENU=Shipping Agent Service Code;
                                                              PTG=C�d. Servi�o Transportador] }
    { 44  ;   ;Shipment Method Code;Code10        ;TableRelation="Shipment Method";
                                                   CaptionML=[ENU=Shipment Method Code;
                                                              PTG=C�d. Condi��es Envio] }
    { 6500;   ;Serial No.          ;Code20        ;OnLookup=BEGIN
                                                              ItemTrackingMgt.LookupLotSerialNoInfo("Item No.","Variant Code",0,"Serial No.");
                                                            END;

                                                   CaptionML=[ENU=Serial No.;
                                                              PTG=N� S�rie] }
    { 6501;   ;Lot No.             ;Code20        ;OnLookup=BEGIN
                                                              ItemTrackingMgt.LookupLotSerialNoInfo("Item No.","Variant Code",1,"Lot No.");
                                                            END;

                                                   CaptionML=[ENU=Lot No.;
                                                              PTG=N� Lote] }
    { 6502;   ;Warranty Date       ;Date          ;CaptionML=[ENU=Warranty Date;
                                                              PTG=Data Garantia] }
    { 6503;   ;Expiration Date     ;Date          ;CaptionML=[ENU=Expiration Date;
                                                              PTG=Data Expira��o] }
    { 7300;   ;Bin Code            ;Code20        ;TableRelation=IF (Action Type=FILTER(<>Take)) Bin.Code WHERE (Location Code=FIELD(Location Code),
                                                                                                                 Zone Code=FIELD(Zone Code))
                                                                                                                 ELSE IF (Action Type=FILTER(<>Take),
                                                                                                                          Zone Code=FILTER('')) Bin.Code WHERE (Location Code=FIELD(Location Code))
                                                                                                                          ELSE IF (Action Type=CONST(Take)) "Bin Content"."Bin Code" WHERE (Location Code=FIELD(Location Code));
                                                   CaptionML=[ENU=Bin Code;
                                                              PTG=C�d. Posi��o] }
    { 7301;   ;Zone Code           ;Code10        ;TableRelation=Zone.Code WHERE (Location Code=FIELD(Location Code));
                                                   CaptionML=[ENU=Zone Code;
                                                              PTG=C�d. Zona] }
    { 7305;   ;Action Type         ;Option        ;CaptionML=[ENU=Action Type;
                                                              PTG=Tipo A��o];
                                                   OptionCaptionML=[ENU=" ,Take,Place";
                                                                    PTG=" ,Retirar,Colocar"];
                                                   OptionString=[ ,Take,Place];
                                                   Editable=No }
    { 7312;   ;Special Equipment Code;Code10      ;TableRelation="Special Equipment";
                                                   CaptionML=[ENU=Special Equipment Code;
                                                              PTG=C�d. Equipamento Especial] }
  }
  KEYS
  {
    {    ;No.,Line No.                            ;Clustered=Yes }
    {    ;No.,Sorting Sequence No.                 }
    {    ;Source Type,Source Subtype,Source No.,Source Line No.,Source Subline No. }
    { No ;Lot No.                                  }
    { No ;Serial No.                               }
  }
  FIELDGROUPS
  {
  }
  CODE
  {
    VAR
      ItemTrackingMgt@1000 : Codeunit 6500;

    LOCAL PROCEDURE CalcBaseQty@14(Qty@1000 : Decimal) : Decimal;
    BEGIN
      TESTFIELD("Qty. per Unit of Measure");
      EXIT(ROUND(Qty * "Qty. per Unit of Measure",0.00001));
    END;

    BEGIN
    END.
  }
}

