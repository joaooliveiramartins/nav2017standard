OBJECT Page 96 Sales Cr. Memo Subform
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0,NAVPTSS93.00;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Lines;
               PTG=Linhas];
    MultipleNewLines=Yes;
    LinksAllowed=No;
    SourceTable=Table37;
    DelayedInsert=Yes;
    SourceTableView=WHERE(Document Type=FILTER(Credit Memo));
    PageType=ListPart;
    AutoSplitKey=Yes;
    OnAfterGetRecord=BEGIN
                       ShowShortcutDimCode(ShortcutDimCode);
                       CLEAR(DocumentTotals);
                     END;

    OnNewRecord=VAR
                  ApplicationAreaSetup@1001 : Record 9178;
                BEGIN
                  IF ApplicationAreaSetup.IsFoundationEnabled THEN
                    Type := Type::Item
                  ELSE
                    InitType;

                  CLEAR(ShortcutDimCode);
                END;

    OnInsertRecord=VAR
                     ApplicationAreaSetup@1000 : Record 9178;
                   BEGIN
                     IF ApplicationAreaSetup.IsFoundationEnabled THEN
                       Type := Type::Item;
                   END;

    OnDeleteRecord=VAR
                     ReserveSalesLine@1000 : Codeunit 99000832;
                   BEGIN
                     IF (Quantity <> 0) AND ItemExists("No.") THEN BEGIN
                       COMMIT;
                       IF NOT ReserveSalesLine.DeleteLineConfirm(Rec) THEN
                         EXIT(FALSE);
                       ReserveSalesLine.DeleteLine(Rec);
                     END;
                   END;

    OnAfterGetCurrRecord=BEGIN
                           UpdateEditableOnRow;

                           IF SalesHeader.GET("Document Type","Document No.") THEN;

                           DocumentTotals.SalesUpdateTotalsControls(Rec,TotalSalesHeader,TotalSalesLine,RefreshMessageEnabled,
                             TotalAmountStyle,RefreshMessageText,InvDiscAmountEditable,CurrPage.EDITABLE,VATAmount);
                         END;

    ActionList=ACTIONS
    {
      { 1900000004;0 ;ActionContainer;
                      ActionContainerType=ActionItems }
      { 1902056104;1 ;Action    ;
                      Name=InsertExtTexts;
                      AccessByPermission=TableData 279=R;
                      CaptionML=[ENU=Insert &Ext. Texts;
                                 PTG=Inserir T&extos Adicionais];
                      ToolTipML=ENU=Insert an extended description for the sales document.;
                      ApplicationArea=#Suite;
                      Image=Text;
                      OnAction=BEGIN
                                 InsertExtendedText(TRUE);
                               END;
                                }
      { 1902740304;1 ;Action    ;
                      AccessByPermission=TableData 348=R;
                      ShortCutKey=Shift+Ctrl+D;
                      CaptionML=[ENU=Dimensions;
                                 PTG=Dimens�es];
                      ToolTipML=ENU=View or edits dimensions, such as area, project, or department, that you can assign to sales and purchase documents to distribute costs and analyze transaction history.;
                      ApplicationArea=#Suite;
                      Image=Dimensions;
                      OnAction=BEGIN
                                 ShowDimensions;
                               END;
                                }
      { 19      ;1   ;Action    ;
                      Name=DeferralSchedule;
                      CaptionML=[ENU=Deferral Schedule;
                                 PTG=Plano Diferimentos];
                      ToolTipML=ENU=View or edit the deferral schedule that governs how revenue made with this sales document is deferred to different accounting periods when the document is posted.;
                      ApplicationArea=#Suite;
                      Enabled="Deferral Code" <> '';
                      Image=PaymentPeriod;
                      OnAction=BEGIN
                                 TotalSalesHeader.GET("Document Type","Document No.");
                                 ShowDeferrals(TotalSalesHeader."Posting Date",TotalSalesHeader."Currency Code");
                               END;
                                }
      { 1906587504;1 ;ActionGroup;
                      CaptionML=[ENU=F&unctions;
                                 PTG=F&un��es];
                      Image=Action }
      { 1904522204;2 ;Action    ;
                      AccessByPermission=TableData 90=R;
                      CaptionML=[ENU=E&xplode BOM;
                                 PTG=E&xpandir L.M.];
                      Image=ExplodeBOM;
                      OnAction=BEGIN
                                 ExplodeBOM;
                               END;
                                }
      { 1901991804;2 ;Action    ;
                      AccessByPermission=TableData 6660=R;
                      Ellipsis=Yes;
                      CaptionML=[ENU=Get Return &Receipt Lines;
                                 PTG=Obter Linhas &Rec. Devolu��o];
                      Image=ReturnReceipt;
                      OnAction=BEGIN
                                 GetReturnReceipt;
                               END;
                                }
      { 1907935204;1 ;ActionGroup;
                      CaptionML=[ENU=&Line;
                                 PTG=&Linha];
                      Image=Line }
      { 1907981204;2 ;ActionGroup;
                      CaptionML=[ENU=Item Availability by;
                                 PTG=Disponibilidade Produto por];
                      Image=ItemAvailability }
      { 5       ;3   ;Action    ;
                      CaptionML=[ENU=Event;
                                 PTG=Ocorr�ncia];
                      Image=Event;
                      OnAction=BEGIN
                                 ItemAvailFormsMgt.ShowItemAvailFromSalesLine(Rec,ItemAvailFormsMgt.ByEvent)
                               END;
                                }
      { 1903587104;3 ;Action    ;
                      CaptionML=[ENU=Period;
                                 PTG=Per�odo];
                      Image=Period;
                      OnAction=BEGIN
                                 ItemAvailFormsMgt.ShowItemAvailFromSalesLine(Rec,ItemAvailFormsMgt.ByPeriod)
                               END;
                                }
      { 1903193004;3 ;Action    ;
                      CaptionML=[ENU=Variant;
                                 PTG=Variante];
                      Image=ItemVariant;
                      OnAction=BEGIN
                                 ItemAvailFormsMgt.ShowItemAvailFromSalesLine(Rec,ItemAvailFormsMgt.ByVariant)
                               END;
                                }
      { 1901743104;3 ;Action    ;
                      AccessByPermission=TableData 14=R;
                      CaptionML=[ENU=Location;
                                 PTG=Localiza��o];
                      Image=Warehouse;
                      OnAction=BEGIN
                                 ItemAvailFormsMgt.ShowItemAvailFromSalesLine(Rec,ItemAvailFormsMgt.ByLocation)
                               END;
                                }
      { 7       ;3   ;Action    ;
                      CaptionML=[ENU=BOM Level;
                                 PTG=N�vel L.M.];
                      Image=BOMLevel;
                      OnAction=BEGIN
                                 ItemAvailFormsMgt.ShowItemAvailFromSalesLine(Rec,ItemAvailFormsMgt.ByBOM)
                               END;
                                }
      { 1907838004;2 ;Action    ;
                      CaptionML=[ENU=Co&mments;
                                 PTG=Co&ment�rios];
                      Image=ViewComments;
                      OnAction=BEGIN
                                 ShowLineComments;
                               END;
                                }
      { 1907184504;2 ;Action    ;
                      AccessByPermission=TableData 5800=R;
                      CaptionML=[ENU=Item Charge &Assignment;
                                 PTG=&Atribui��o Encargo Produto];
                      Image=ItemCosts;
                      OnAction=BEGIN
                                 ItemChargeAssgnt;
                               END;
                                }
      { 1905987604;2 ;Action    ;
                      Name=ItemTrackingLines;
                      ShortCutKey=Shift+Ctrl+I;
                      CaptionML=[ENU=Item &Tracking Lines;
                                 PTG=Linhas Ras&treio Produto];
                      Image=ItemTrackingLines;
                      OnAction=BEGIN
                                 OpenItemTrackingLines;
                               END;
                                }
      { 31022893;2   ;Action    ;
                      Name=CalcWithholdingTax;
                      CaptionML=[ENU=Calculate Retention;
                                 PTG=Calcular Reten��o na Fonte];
                      OnAction=BEGIN
                                 CalcWithholdingTax;
                               END;
                                }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 2   ;2   ;Field     ;
                ToolTipML=ENU=Specifies the type of entity that will be posted for this sales line, such as Item, Resource, or G/L Account.;
                SourceExpr=Type;
                OnValidate=BEGIN
                             NoOnAfterValidate;

                             IF xRec."No." <> '' THEN
                               RedistributeTotalsOnAfterValidate;
                           END;
                            }

    { 4   ;2   ;Field     ;
                ToolTipML=ENU=Specifies the number of a general ledger account, item, resource, additional cost, or fixed asset, depending on the contents of the Type field.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="No.";
                OnValidate=BEGIN
                             ShowShortcutDimCode(ShortcutDimCode);
                             NoOnAfterValidate;
                             UpdateEditableOnRow;
                             IF xRec."No." <> '' THEN
                               RedistributeTotalsOnAfterValidate;
                           END;
                            }

    { 34  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the cross-referenced item number. If you enter a cross reference between yours and your vendor's or customer's item number, then this number will override the standard item number when you enter the cross-reference number on a sales or purchase document.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Cross-Reference No.";
                Visible=FALSE;
                OnValidate=BEGIN
                             CrossReferenceNoOnAfterValidat;
                             NoOnAfterValidate;
                           END;

                OnLookup=BEGIN
                           CrossReferenceNoLookUp;
                           InsertExtendedText(FALSE);
                           NoOnAfterValidate;
                         END;
                          }

    { 46  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the IC partner code of the partner to whom you want to distribute the revenue of the sales line.;
                SourceExpr="IC Partner Code";
                Visible=FALSE }

    { 40  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the item or account in your IC partner's company that corresponds to the item or account on the line.;
                SourceExpr="IC Partner Ref. Type";
                Visible=FALSE }

    { 42  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the item or account in your IC partner's company that corresponds to the item or account on the line.;
                SourceExpr="IC Partner Reference";
                Visible=FALSE }

    { 18  ;2   ;Field     ;
                ToolTipML=ENU=Specifies a variant code for the item.;
                SourceExpr="Variant Code";
                Visible=FALSE }

    { 38  ;2   ;Field     ;
                ToolTipML=ENU=Specifies that this item is a nonstock item.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr=Nonstock;
                Visible=FALSE }

    { 58  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the code for the VAT product posting group of the item, resource, or general ledger account on this line.;
                SourceExpr="VAT Prod. Posting Group";
                Visible=FALSE;
                OnValidate=BEGIN
                             RedistributeTotalsOnAfterValidate;
                           END;
                            }

    { 6   ;2   ;Field     ;
                ToolTipML=ENU=Specifies a description of the entry, which is based on the contents of the Type and No. fields.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr=Description;
                OnValidate=BEGIN
                             UpdateEditableOnRow;

                             IF "No." = xRec."No." THEN
                               EXIT;

                             ShowShortcutDimCode(ShortcutDimCode);
                             NoOnAfterValidate;

                             IF xRec."No." <> '' THEN
                               RedistributeTotalsOnAfterValidate;
                           END;

                ShowMandatory=TRUE }

    { 24  ;2   ;Field     ;
                ToolTipML=ENU=Specifies a code that explains why the item is returned.;
                SourceExpr="Return Reason Code";
                Visible=FALSE }

    { 72  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the inventory location from which the items sold should be picked and where the inventory decrease is registered.;
                SourceExpr="Location Code" }

    { 50  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the bin from where items on the sales order line are taken from when they are shipped.;
                SourceExpr="Bin Code";
                Visible=FALSE }

    { 28  ;2   ;Field     ;
                ToolTipML=ENU=Specifies whether a reservation can be made for items on this line.;
                SourceExpr=Reserve;
                Visible=FALSE;
                OnValidate=BEGIN
                             ReserveOnAfterValidate;
                           END;
                            }

    { 8   ;2   ;Field     ;
                ToolTipML=ENU=Specifies how many units are being sold.;
                ApplicationArea=#Basic,#Suite;
                BlankZero=Yes;
                SourceExpr=Quantity;
                Enabled=NOT RowIsText;
                Editable=NOT RowIsText;
                OnValidate=BEGIN
                             QuantityOnAfterValidate;
                             RedistributeTotalsOnAfterValidate;
                           END;

                ShowMandatory="No." <> '' }

    { 26  ;2   ;Field     ;
                ToolTipML=ENU=Specifies how many units of the item on the line have been reserved.;
                BlankZero=Yes;
                SourceExpr="Reserved Quantity";
                Visible=FALSE;
                OnDrillDown=BEGIN
                              CurrPage.SAVERECORD;
                              COMMIT;
                              ShowReservationEntries(TRUE);
                              UpdateForm(TRUE);
                            END;
                             }

    { 22  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the unit of measure that is used to determine the value in the Unit Price field on the sales line.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Unit of Measure Code";
                Enabled=UnitofMeasureCodeIsChangeable;
                Editable=UnitofMeasureCodeIsChangeable;
                OnValidate=BEGIN
                             UnitofMeasureCodeOnAfterValida;
                             RedistributeTotalsOnAfterValidate;
                           END;
                            }

    { 10  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the unit of measure for the item or resource on the sales line.;
                SourceExpr="Unit of Measure";
                Visible=FALSE }

    { 1110002;2;Field     ;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Not in Vat Report" }

    { 70  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the unit cost of the item on the line.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Unit Cost (LCY)";
                Visible=FALSE }

    { 12  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the price for one unit on the sales line.;
                ApplicationArea=#Basic,#Suite;
                BlankZero=Yes;
                SourceExpr="Unit Price";
                Enabled=NOT RowIsText;
                Editable=NOT RowIsText;
                OnValidate=BEGIN
                             RedistributeTotalsOnAfterValidate;
                           END;

                ShowMandatory="No." <> '' }

    { 92  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the net amount (before subtracting the invoice discount amount) that must be paid for the items on the line.;
                ApplicationArea=#Basic,#Suite;
                BlankZero=Yes;
                SourceExpr="Line Amount";
                Enabled=NOT RowIsText;
                Editable=NOT RowIsText;
                OnValidate=BEGIN
                             RedistributeTotalsOnAfterValidate;
                           END;
                            }

    { 16  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the line discount percentage that is valid for the item quantity on the line.;
                ApplicationArea=#Basic,#Suite;
                BlankZero=Yes;
                SourceExpr="Line Discount %";
                Enabled=NOT RowIsText;
                Editable=NOT RowIsText;
                OnValidate=BEGIN
                             RedistributeTotalsOnAfterValidate;
                           END;
                            }

    { 56  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the amount of the discount that will be given on the invoice line.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Line Discount Amount";
                Visible=FALSE;
                OnValidate=BEGIN
                             RedistributeTotalsOnAfterValidate;
                           END;
                            }

    { 54  ;2   ;Field     ;
                ToolTipML=ENU=Specifies whether the invoice line is included when the invoice discount is calculated.;
                SourceExpr="Allow Invoice Disc.";
                Visible=FALSE }

    { 20  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the invoice discount amount for the line.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Inv. Discount Amount";
                Visible=FALSE }

    { 14  ;2   ;Field     ;
                ToolTipML=ENU=Specifies that you can assign item charges to this line.;
                SourceExpr="Allow Item Charge Assignment";
                Visible=FALSE }

    { 52  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the quantity of the item charge that will be assigned to a specified item when you post this sales line.;
                BlankZero=Yes;
                SourceExpr="Qty. to Assign";
                OnDrillDown=BEGIN
                              CurrPage.SAVERECORD;
                              ShowItemChargeAssgnt;
                              UpdateForm(FALSE);
                            END;
                             }

    { 60  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the quantity of the item charge that was assigned to a specified item when you posted this sales line.;
                BlankZero=Yes;
                SourceExpr="Qty. Assigned";
                OnDrillDown=BEGIN
                              CurrPage.SAVERECORD;
                              ShowItemChargeAssgnt;
                              UpdateForm(FALSE);
                            END;
                             }

    { 44  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the job number that the sales line is linked to.;
                SourceExpr="Job No.";
                Visible=FALSE;
                OnValidate=BEGIN
                             ShowShortcutDimCode(ShortcutDimCode);
                           END;
                            }

    { 3   ;2   ;Field     ;
                ToolTipML=ENU=Specifies the number of the job task that the sales line is linked to.;
                SourceExpr="Job Task No.";
                Visible=FALSE }

    { 21  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the VAT category in connection with electronic document sending. For example, when you send sales documents through the PEPPOL service, the value in this field is used to populate several fields, such as the ClassifiedTaxCategory element in the Item group. It is also used to populate the TaxCategory element in both the TaxSubtotal and AllowanceCharge group. The number is based on the UNCL5305 standard.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Tax Category";
                Visible=FALSE }

    { 62  ;2   ;Field     ;
                ToolTipML=ENU=Belongs to the Job application area.;
                SourceExpr="Work Type Code";
                Visible=FALSE }

    { 30  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the document number of the blanket order from which this sales line originates.;
                SourceExpr="Blanket Order No.";
                Visible=FALSE }

    { 32  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the line number of the blanket order line from which this sales line originates.;
                SourceExpr="Blanket Order Line No.";
                Visible=FALSE }

    { 5810;2   ;Field     ;
                ToolTipML=ENU=Specifies the number of the item ledger entry that the sales credit memo line is applied from.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Appl.-from Item Entry";
                Visible=FALSE }

    { 48  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the item ledger entry number the line should be applied to.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Appl.-to Item Entry";
                Visible=FALSE }

    { 23  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the deferral template that governs how revenue earned with this sales document is deferred to the different accounting periods when the good or service was delivered.;
                ApplicationArea=#Suite;
                SourceExpr="Deferral Code";
                TableRelation="Deferral Template"."Deferral Code";
                Visible=FALSE;
                Enabled=(Type <> Type::"Fixed Asset") AND (Type <> Type::" ") }

    { 76  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the code for Shortcut Dimension 1.;
                ApplicationArea=#Suite;
                SourceExpr="Shortcut Dimension 1 Code";
                Visible=FALSE }

    { 74  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the code for Shortcut Dimension 2.;
                ApplicationArea=#Suite;
                SourceExpr="Shortcut Dimension 2 Code";
                Visible=FALSE }

    { 300 ;2   ;Field     ;
                SourceExpr=ShortcutDimCode[3];
                CaptionClass='1,2,3';
                TableRelation="Dimension Value".Code WHERE (Global Dimension No.=CONST(3),
                                                            Dimension Value Type=CONST(Standard),
                                                            Blocked=CONST(No));
                Visible=FALSE;
                OnValidate=BEGIN
                             ValidateSaveShortcutDimCode(3,ShortcutDimCode[3]);
                           END;
                            }

    { 302 ;2   ;Field     ;
                SourceExpr=ShortcutDimCode[4];
                CaptionClass='1,2,4';
                TableRelation="Dimension Value".Code WHERE (Global Dimension No.=CONST(4),
                                                            Dimension Value Type=CONST(Standard),
                                                            Blocked=CONST(No));
                Visible=FALSE;
                OnValidate=BEGIN
                             ValidateSaveShortcutDimCode(4,ShortcutDimCode[4]);
                           END;
                            }

    { 304 ;2   ;Field     ;
                SourceExpr=ShortcutDimCode[5];
                CaptionClass='1,2,5';
                TableRelation="Dimension Value".Code WHERE (Global Dimension No.=CONST(5),
                                                            Dimension Value Type=CONST(Standard),
                                                            Blocked=CONST(No));
                Visible=FALSE;
                OnValidate=BEGIN
                             ValidateSaveShortcutDimCode(5,ShortcutDimCode[5]);
                           END;
                            }

    { 306 ;2   ;Field     ;
                SourceExpr=ShortcutDimCode[6];
                CaptionClass='1,2,6';
                TableRelation="Dimension Value".Code WHERE (Global Dimension No.=CONST(6),
                                                            Dimension Value Type=CONST(Standard),
                                                            Blocked=CONST(No));
                Visible=FALSE;
                OnValidate=BEGIN
                             ValidateSaveShortcutDimCode(6,ShortcutDimCode[6]);
                           END;
                            }

    { 308 ;2   ;Field     ;
                SourceExpr=ShortcutDimCode[7];
                CaptionClass='1,2,7';
                TableRelation="Dimension Value".Code WHERE (Global Dimension No.=CONST(7),
                                                            Dimension Value Type=CONST(Standard),
                                                            Blocked=CONST(No));
                Visible=FALSE;
                OnValidate=BEGIN
                             ValidateSaveShortcutDimCode(7,ShortcutDimCode[7]);
                           END;
                            }

    { 310 ;2   ;Field     ;
                SourceExpr=ShortcutDimCode[8];
                CaptionClass='1,2,8';
                TableRelation="Dimension Value".Code WHERE (Global Dimension No.=CONST(8),
                                                            Dimension Value Type=CONST(Standard),
                                                            Blocked=CONST(No));
                Visible=FALSE;
                OnValidate=BEGIN
                             ValidateSaveShortcutDimCode(8,ShortcutDimCode[8]);
                           END;
                            }

    { 1110000;2;Field     ;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="DRF Code";
                Visible=FALSE }

    { 1000000000;2;Field  ;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Credit - to Doc. No.";
                Visible=isCrMemoSerie }

    { 1000000001;2;Field  ;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Credit - to Doc. Line No.";
                Visible=isCrMemoSerie }

    { 31022890;2;Field    ;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Withholding Tax Code";
                Visible=FALSE }

    { 31022894;2;Field    ;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Withholding Tax %";
                Visible=FALSE }

    { 31022891;2;Field    ;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Withholding Tax Amount";
                Visible=FALSE }

    { 31022892;2;Field    ;
                SourceExpr="Stamp Duty Code";
                Visible=FALSE }

    { 39  ;1   ;Group     ;
                GroupType=Group }

    { 35  ;2   ;Group     ;
                GroupType=Group }

    { 33  ;3   ;Field     ;
                Name=Invoice Discount Amount;
                CaptionML=[ENU=Invoice Discount Amount;
                           PTG=Valor Desconto Fatura];
                ToolTipML=ENU=Specifies a discount amount that is deducted from the value in the Total Incl. VAT field. You can enter or change the amount manually.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr=TotalSalesLine."Inv. Discount Amount";
                AutoFormatType=1;
                AutoFormatExpr=TotalSalesHeader."Currency Code";
                Editable=InvDiscAmountEditable;
                Style=Subordinate;
                StyleExpr=RefreshMessageEnabled;
                OnValidate=BEGIN
                             SalesHeader.GET("Document Type","Document No.");
                             SalesCalcDiscByType.ApplyInvDiscBasedOnAmt(TotalSalesLine."Inv. Discount Amount",SalesHeader);
                             CurrPage.UPDATE(FALSE);
                           END;
                            }

    { 31  ;3   ;Field     ;
                Name=Invoice Disc. Pct.;
                CaptionML=[ENU=Invoice Discount %;
                           PTG=Valor Desconto %];
                ToolTipML=ENU=Specifies a discount percentage that is granted if criteria that you have set up for the customer are met.;
                ApplicationArea=#Basic,#Suite;
                DecimalPlaces=0:2;
                SourceExpr=SalesCalcDiscByType.GetCustInvoiceDiscountPct(Rec);
                Editable=FALSE;
                Style=Subordinate;
                StyleExpr=RefreshMessageEnabled }

    { 17  ;2   ;Group     ;
                GroupType=Group }

    { 15  ;3   ;Field     ;
                Name=Total Amount Excl. VAT;
                DrillDown=No;
                CaptionML=[ENU=Total Amount Excl. VAT;
                           PTG=Valor Total Excl.IVA];
                ToolTipML=ENU=Specifies the sum of the value in the Line Amount Excl. VAT field on all lines in the document minus any discount amount in the Invoice Discount Amount field.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr=TotalSalesLine.Amount;
                AutoFormatType=1;
                AutoFormatExpr=TotalSalesHeader."Currency Code";
                CaptionClass=DocumentTotals.GetTotalExclVATCaption(SalesHeader."Currency Code");
                Editable=FALSE;
                Style=Subordinate;
                StyleExpr=RefreshMessageEnabled }

    { 13  ;3   ;Field     ;
                Name=Total VAT Amount;
                CaptionML=[ENU=Total VAT;
                           PTG=Total IVA];
                ToolTipML=ENU=Specifies the sum of VAT amounts on all lines in the document.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr=VATAmount;
                AutoFormatType=1;
                AutoFormatExpr=TotalSalesHeader."Currency Code";
                CaptionClass=DocumentTotals.GetTotalVATCaption(SalesHeader."Currency Code");
                Editable=FALSE;
                Style=Subordinate;
                StyleExpr=RefreshMessageEnabled }

    { 11  ;3   ;Field     ;
                Name=Total Amount Incl. VAT;
                CaptionML=[ENU=Total Amount Incl. VAT;
                           PTG=Valor Total Incl. IVA];
                ToolTipML=ENU=Specifies the sum of the value in the Line Amount Incl. VAT field on all lines in the document minus any discount amount in the Invoice Discount Amount field.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr=TotalSalesLine."Amount Including VAT";
                AutoFormatType=1;
                AutoFormatExpr=TotalSalesHeader."Currency Code";
                CaptionClass=DocumentTotals.GetTotalInclVATCaption(SalesHeader."Currency Code");
                Editable=FALSE;
                StyleExpr=TotalAmountStyle }

    { 9   ;3   ;Field     ;
                Name=RefreshTotals;
                DrillDown=Yes;
                SourceExpr=RefreshMessageText;
                Enabled=RefreshMessageEnabled;
                Editable=FALSE;
                OnDrillDown=BEGIN
                              DocumentTotals.SalesRedistributeInvoiceDiscountAmounts(Rec,VATAmount,TotalSalesLine);
                              DocumentTotals.SalesUpdateTotalsControls(Rec,TotalSalesHeader,TotalSalesLine,RefreshMessageEnabled,
                                TotalAmountStyle,RefreshMessageText,InvDiscAmountEditable,CurrPage.EDITABLE,VATAmount);
                            END;

                ShowCaption=No }

  }
  CODE
  {
    VAR
      SalesHeader@1011 : Record 36;
      TotalSalesHeader@1021 : Record 36;
      TotalSalesLine@1020 : Record 37;
      TransferExtendedText@1000 : Codeunit 378;
      ItemAvailFormsMgt@1004 : Codeunit 353;
      SalesCalcDiscByType@1015 : Codeunit 56;
      DocumentTotals@1014 : Codeunit 57;
      VATAmount@1013 : Decimal;
      ShortcutDimCode@1001 : ARRAY [8] OF Code[20];
      InvDiscAmountEditable@1010 : Boolean;
      TotalAmountStyle@1009 : Text;
      RefreshMessageEnabled@1008 : Boolean;
      RefreshMessageText@1007 : Text;
      RowIsText@1002 : Boolean;
      UnitofMeasureCodeIsChangeable@1003 : Boolean;
      "//--soft-global--//"@9000000 : Integer;
      isCrMemoSerie@1000000001 : Boolean;

    PROCEDURE ApproveCalcInvDisc@1();
    BEGIN
      CODEUNIT.RUN(CODEUNIT::"Sales-Disc. (Yes/No)",Rec);
    END;

    LOCAL PROCEDURE ExplodeBOM@3();
    BEGIN
      CODEUNIT.RUN(CODEUNIT::"Sales-Explode BOM",Rec);
    END;

    LOCAL PROCEDURE GetReturnReceipt@2();
    BEGIN
      CODEUNIT.RUN(CODEUNIT::"Sales-Get Return Receipts",Rec);
    END;

    LOCAL PROCEDURE InsertExtendedText@5(Unconditionally@1000 : Boolean);
    BEGIN
      IF TransferExtendedText.SalesCheckIfAnyExtText(Rec,Unconditionally) THEN BEGIN
        CurrPage.SAVERECORD;
        COMMIT;
        TransferExtendedText.InsertSalesExtText(Rec);
      END;
      IF TransferExtendedText.MakeUpdate THEN
        UpdateForm(TRUE);
    END;

    LOCAL PROCEDURE OpenItemTrackingLines@6500();
    BEGIN
      OpenItemTrackingLines;
    END;

    LOCAL PROCEDURE ItemChargeAssgnt@5800();
    BEGIN
      ShowItemChargeAssgnt;
    END;

    PROCEDURE UpdateForm@12(SetSaveRecord@1000 : Boolean);
    BEGIN
      CurrPage.UPDATE(SetSaveRecord);
    END;

    LOCAL PROCEDURE ShowLineComments@10();
    BEGIN
      ShowLineComments;
    END;

    LOCAL PROCEDURE NoOnAfterValidate@19066594();
    BEGIN
      InsertExtendedText(FALSE);
      IF (Type = Type::"Charge (Item)") AND ("No." <> xRec."No.") AND
         (xRec."No." <> '')
      THEN
        CurrPage.SAVERECORD;
    END;

    LOCAL PROCEDURE CrossReferenceNoOnAfterValidat@19048248();
    BEGIN
      InsertExtendedText(FALSE);
    END;

    LOCAL PROCEDURE ReserveOnAfterValidate@19004502();
    BEGIN
      IF (Reserve = Reserve::Always) AND ("Outstanding Qty. (Base)" <> 0) THEN BEGIN
        CurrPage.SAVERECORD;
        AutoReserve;
      END;
    END;

    LOCAL PROCEDURE QuantityOnAfterValidate@19032465();
    BEGIN
      IF Reserve = Reserve::Always THEN BEGIN
        CurrPage.SAVERECORD;
        AutoReserve;
      END;
    END;

    LOCAL PROCEDURE UnitofMeasureCodeOnAfterValida@19057939();
    BEGIN
      IF Reserve = Reserve::Always THEN BEGIN
        CurrPage.SAVERECORD;
        AutoReserve;
      END;
    END;

    LOCAL PROCEDURE RedistributeTotalsOnAfterValidate@11();
    BEGIN
      CurrPage.SAVERECORD;

      SalesHeader.GET("Document Type","Document No.");
      IF DocumentTotals.SalesCheckNumberOfLinesLimit(SalesHeader) THEN
        DocumentTotals.SalesRedistributeInvoiceDiscountAmounts(Rec,VATAmount,TotalSalesLine);
      CurrPage.UPDATE;
    END;

    LOCAL PROCEDURE ValidateSaveShortcutDimCode@6(FieldNumber@1001 : Integer;VAR ShortcutDimCode@1000 : Code[20]);
    BEGIN
      ValidateShortcutDimCode(FieldNumber,ShortcutDimCode);
      CurrPage.SAVERECORD;
    END;

    LOCAL PROCEDURE UpdateEditableOnRow@19();
    BEGIN
      RowIsText := ("No." = '') AND (Description <> '');
      IF NOT RowIsText THEN
        UnitofMeasureCodeIsChangeable := CanEditUnitOfMeasureCode
      ELSE
        UnitofMeasureCodeIsChangeable := FALSE;
    END;

    PROCEDURE "//--soft-func--//"@9000005();
    BEGIN
    END;

    PROCEDURE ControlCreditInvoice@1000000002(NoSeriesCode@1000000002 : Code[20]);
    VAR
      SalesHeader@1000000000 : Record 36;
      NoSeries@1000000001 : Record 308;
    BEGIN
      //soft,sn
      NoSeries.GET(NoSeriesCode);
      IF NoSeries."Credit Invoice" THEN
        isCrMemoSerie := TRUE
      ELSE
        isCrMemoSerie := FALSE;
      //soft,en
    END;

    BEGIN
    {
      V93.00#00018 - Reten��o a Clientes e Fornecedores -  - 2016.06.29
      V93.00#00023 - Imposto Selo - SAFT - IRC - 2016.03.31
    }
    END.
  }
}

