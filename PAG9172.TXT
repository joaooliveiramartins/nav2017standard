OBJECT Page 9172 User Personalization Card
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0;
  }
  PROPERTIES
  {
    CaptionML=[ENU=User Personalization Card;
               PTG=Ficha Personaliza��o do Utilizador];
    SourceTable=Table2000000073;
    DataCaptionExpr="User ID";
    DelayedInsert=Yes;
    PageType=Card;
    OnOpenPage=BEGIN
                 HideExternalUsers;
               END;

    OnInsertRecord=BEGIN
                     TESTFIELD("User SID");
                   END;

    OnModifyRecord=BEGIN
                     TESTFIELD("User SID");
                   END;

    ActionList=ACTIONS
    {
      { 1900000003;0 ;ActionContainer;
                      ActionContainerType=RelatedInformation }
      { 13      ;1   ;ActionGroup;
                      CaptionML=[ENU=User &Personalization;
                                 PTG=&Personaliza��o Utilizador];
                      Image=Grid }
      { 14      ;2   ;Action    ;
                      ShortCutKey=Shift+Ctrl+L;
                      CaptionML=[ENU=List;
                                 PTG=Lista];
                      ToolTipML=[ENU=View or edit a list of all users who have personalized their user interface by customizing one or more pages.;
                                 PTG=""];
                      ApplicationArea=#Basic,#Suite;
                      Image=OpportunitiesList;
                      OnAction=VAR
                                 UserPersList@1102601000 : Page 9173;
                               BEGIN
                                 UserPersList.LOOKUPMODE := TRUE;
                                 UserPersList.SETRECORD(Rec);
                                 IF UserPersList.RUNMODAL = ACTION::LookupOK THEN
                                   UserPersList.GETRECORD(Rec);
                               END;
                                }
      { 1900000004;0 ;ActionContainer;
                      ActionContainerType=ActionItems }
      { 20      ;1   ;ActionGroup;
                      CaptionML=[ENU=F&unctions;
                                 PTG=F&un��es];
                      Image=Action }
      { 21      ;2   ;Action    ;
                      CaptionML=[ENU=C&lear Personalized Pages;
                                 PTG=&Limpar P�ginas Personalizadas];
                      ToolTipML=[ENU=Delete all personalizations made by the selected user.;
                                 PTG=""];
                      ApplicationArea=#Basic,#Suite;
                      Image=Cancel;
                      OnAction=BEGIN
                                 ConfPersMgt.ClearUserPersonalization(Rec);
                               END;
                                }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                CaptionML=[ENU=General;
                           PTG=Geral] }

    { 4   ;2   ;Field     ;
                DrillDown=No;
                CaptionML=[ENU=User ID;
                           PTG=ID Utilizador];
                ToolTipML=[ENU=Specifies the user ID of a user who is using Database Server Authentication to log on to Dynamics NAV.;
                           PTG=""];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="User ID";
                Editable=FALSE;
                OnAssistEdit=VAR
                               UserPersonalization@1005 : Record 2000000073;
                               UserMgt@1002 : Codeunit 418;
                               SID@1006 : GUID;
                               UserID@1001 : Code[50];
                             BEGIN
                               UserMgt.LookupUser(UserID,SID);

                               IF (SID <> "User SID") AND NOT ISNULLGUID(SID) THEN BEGIN
                                 IF UserPersonalization.GET(SID) THEN BEGIN
                                   UserPersonalization.CALCFIELDS("User ID");
                                   ERROR(Text000,TABLECAPTION,UserPersonalization."User ID");
                                 END;

                                 VALIDATE("User SID",SID);
                                 CALCFIELDS("User ID");

                                 CurrPage.UPDATE;
                               END;
                             END;
                              }

    { 6   ;2   ;Field     ;
                CaptionML=[ENU=Profile ID;
                           PTG=ID Perfil];
                ToolTipML=[ENU=Specifies the ID of the profile that is associated with the current user.;
                           PTG=""];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Profile ID";
                LookupPageID=Profile List }

    { 8   ;2   ;Field     ;
                CaptionML=[ENU=Language ID;
                           PTG=ID Idioma];
                ToolTipML=[ENU=Specifies the ID of the language that Microsoft Windows is set up to run for the selected user.;
                           PTG=""];
                BlankZero=Yes;
                SourceExpr="Language ID";
                OnValidate=VAR
                             ApplicationManagement@1001 : Codeunit 1;
                           BEGIN
                             ApplicationManagement.ValidateApplicationlLanguage("Language ID");
                           END;

                OnLookup=VAR
                           ApplicationManagement@1002 : Codeunit 1;
                         BEGIN
                           ApplicationManagement.LookupApplicationlLanguage("Language ID");

                           IF "Language ID" <> xRec."Language ID" THEN
                             VALIDATE("Language ID","Language ID");
                         END;
                          }

    { 2   ;2   ;Field     ;
                CaptionML=[ENU=Locale ID;
                           PTG=ID Local];
                ToolTipML=[ENU=Specifies the ID of the locale that Microsoft Windows is set up to run for the selected user.;
                           PTG=Nota: Esta defini��o n�o afeta o cliente windows.];
                ApplicationArea=#Basic,#Suite;
                BlankZero=Yes;
                SourceExpr="Locale ID";
                TableRelation="Windows Language"."Language ID";
                Importance=Additional }

    { 3   ;2   ;Field     ;
                CaptionML=[ENU=Time Zone;
                           PTG=Fuso Hor�rio];
                ToolTipML=[ENU=Select the Time Zone setting that controls time and date. This setting does not apply to the Windows client.;
                           PTG=selecionar o Fuso Hor�rio que controla a data hora. Esta defini��o n�o se aplica ao cliente Windows.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Time Zone";
                Importance=Additional;
                OnValidate=BEGIN
                             ConfPersMgt.ValidateTimeZone("Time Zone")
                           END;

                OnLookup=BEGIN
                           EXIT(ConfPersMgt.LookupTimeZone(Text))
                         END;
                          }

    { 10  ;2   ;Field     ;
                CaptionML=[ENU=Company;
                           PTG=Empresa];
                ToolTipML=[ENU=Specifies the company that is associated with the user.;
                           PTG=""];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=Company }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {
    VAR
      ConfPersMgt@1000 : Codeunit 9170;
      Text000@1008 : TextConst '@@@=User Personalization User1 already exists.;ENU=%1 %2 already exists.;PTG=%1 %2 j� existem.';

    LOCAL PROCEDURE HideExternalUsers@5();
    VAR
      PermissionManager@1001 : Codeunit 9002;
      OriginalFilterGroup@1000 : Integer;
    BEGIN
      IF NOT PermissionManager.SoftwareAsAService THEN
        EXIT;

      OriginalFilterGroup := FILTERGROUP;
      FILTERGROUP := 2;
      CALCFIELDS("License Type");
      SETFILTER("License Type",'<>%1',"License Type"::"External User");
      FILTERGROUP := OriginalFilterGroup;
    END;

    BEGIN
    END.
  }
}

