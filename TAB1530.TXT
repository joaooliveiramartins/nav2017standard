OBJECT Table 1530 Workflow Step Instance Archive
{
  OBJECT-PROPERTIES
  {
    Date=15/09/15;
    Time=12:00:00;
    Version List=NAVW19.00;
  }
  PROPERTIES
  {
    Permissions=TableData 1531=d;
    OnDelete=VAR
               WorkflowStepArgumentArchive@1000 : Record 1531;
             BEGIN
               IF WorkflowStepArgumentArchive.GET(Argument) THEN
                 WorkflowStepArgumentArchive.DELETE(TRUE);
             END;

    CaptionML=[ENU=Workflow Step Instance Archive;
               PTG=Arquivo Inst�ncia Etapa Workflow];
  }
  FIELDS
  {
    { 1   ;   ;ID                  ;GUID          ;CaptionML=[ENU=ID;
                                                              PTG=ID] }
    { 2   ;   ;Workflow Code       ;Code20        ;CaptionML=[ENU=Workflow Code;
                                                              PTG=C�digo Workflow] }
    { 3   ;   ;Workflow Step ID    ;Integer       ;CaptionML=[ENU=Workflow Step ID;
                                                              PTG=ID Passo Workflow] }
    { 4   ;   ;Description         ;Text100       ;CaptionML=[ENU=Description;
                                                              PTG=Descri��o] }
    { 9   ;   ;Entry Point         ;Boolean       ;CaptionML=[ENU=Entry Point;
                                                              PTG=Porto/Aeroporto Entrada] }
    { 11  ;   ;Record ID           ;RecordID      ;CaptionML=[ENU=Record ID;
                                                              PTG=ID Registo] }
    { 12  ;   ;Created Date-Time   ;DateTime      ;CaptionML=[ENU=Created Date-Time;
                                                              PTG=Data-Hora Cria��o];
                                                   Editable=No }
    { 13  ;   ;Created By User ID  ;Code50        ;CaptionML=[ENU=Created By User ID;
                                                              PTG=Criado por Utilizador ID];
                                                   Editable=No }
    { 14  ;   ;Last Modified Date-Time;DateTime   ;CaptionML=[ENU=Last Modified Date-Time;
                                                              PTG=Data-Hora �lt. Modif.];
                                                   Editable=No }
    { 15  ;   ;Last Modified By User ID;Code50    ;TableRelation=User."User Name";
                                                   CaptionML=[ENU=Last Modified By User ID;
                                                              PTG=�ltima Modifica��o Por ID Utilizador];
                                                   Editable=No }
    { 17  ;   ;Status              ;Option        ;CaptionML=[ENU=Status;
                                                              PTG=Estado];
                                                   OptionCaptionML=[ENU=Inactive,Active,Completed,Ignored,Processing;
                                                                    PTG=Inativo,Ativo,Completado,Ignorado,A Processar];
                                                   OptionString=Inactive,Active,Completed,Ignored,Processing }
    { 18  ;   ;Previous Workflow Step ID;Integer  ;CaptionML=[ENU=Previous Workflow Step ID;
                                                              PTG=ID Passo Workflow Anterior] }
    { 19  ;   ;Next Workflow Step ID;Integer      ;CaptionML=[ENU=Next Workflow Step ID;
                                                              PTG=ID Passo Workflow Seguinte] }
    { 21  ;   ;Type                ;Option        ;CaptionML=[ENU=Type;
                                                              PTG=Tipo];
                                                   OptionCaptionML=[ENU=Event,Response;
                                                                    PTG=Evento,Resposta];
                                                   OptionString=Event,Response }
    { 22  ;   ;Function Name       ;Code128       ;CaptionML=[ENU=Function Name;
                                                              PTG=Nome Fun��o] }
    { 23  ;   ;Argument            ;GUID          ;CaptionML=[ENU=Argument;
                                                              PTG=Argumento] }
    { 30  ;   ;Original Workflow Code;Code20      ;CaptionML=[ENU=Original Workflow Code;
                                                              PTG=C�digo Workflow Original] }
    { 31  ;   ;Original Workflow Step ID;Integer  ;CaptionML=[ENU=Original Workflow Step ID;
                                                              PTG=ID Passo Workflow Original] }
    { 32  ;   ;Sequence No.        ;Integer       ;CaptionML=[ENU=Sequence No.;
                                                              PTG=N� Sequ�ncia] }
  }
  KEYS
  {
    {    ;ID,Workflow Code,Workflow Step ID       ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    END.
  }
}

