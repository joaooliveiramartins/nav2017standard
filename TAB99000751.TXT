OBJECT Table 99000751 Shop Calendar
{
  OBJECT-PROPERTIES
  {
    Date=07/09/12;
    Time=12:00:00;
    Version List=NAVW17.00;
  }
  PROPERTIES
  {
    DataCaptionFields=Code,Description;
    OnDelete=BEGIN
               ShopCalendarWorkDays.SETRANGE("Shop Calendar Code",Code);
               ShopCalendarWorkDays.DELETEALL;

               ShopCalHoliday.SETRANGE("Shop Calendar Code",Code);
               ShopCalHoliday.DELETEALL;
             END;

    CaptionML=[ENU=Shop Calendar;
               PTG=Calend�rio F�brica];
    LookupPageID=Page99000751;
    DrillDownPageID=Page99000751;
  }
  FIELDS
  {
    { 1   ;   ;Code                ;Code10        ;CaptionML=[ENU=Code;
                                                              PTG=C�digo];
                                                   NotBlank=Yes }
    { 2   ;   ;Description         ;Text50        ;CaptionML=[ENU=Description;
                                                              PTG=Descri��o] }
  }
  KEYS
  {
    {    ;Code                                    ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {
    VAR
      ShopCalendarWorkDays@1000 : Record 99000752;
      ShopCalHoliday@1001 : Record 99000753;

    BEGIN
    END.
  }
}

