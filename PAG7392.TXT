OBJECT Page 7392 Posted Invt. Pick
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0;
  }
  PROPERTIES
  {
    Editable=No;
    CaptionML=[ENU=Posted Invt. Pick;
               PTG=Recolha Invt. Registada];
    SaveValues=Yes;
    SourceTable=Table7342;
    PageType=Document;
    RefreshOnActivate=Yes;
    OnDeleteRecord=BEGIN
                     CurrPage.UPDATE;
                   END;

    ActionList=ACTIONS
    {
      { 1900000003;0 ;ActionContainer;
                      ActionContainerType=RelatedInformation }
      { 100     ;1   ;ActionGroup;
                      CaptionML=[ENU=P&ick;
                                 PTG=R&ecolha];
                      Image=CreateInventoryPickup }
      { 30      ;2   ;Action    ;
                      CaptionML=[ENU=Co&mments;
                                 PTG=Co&ment rios];
                      RunObject=Page 5776;
                      RunPageLink=Table Name=CONST(Posted Invt. Pick),
                                  Type=CONST(" "),
                                  No.=FIELD(No.);
                      Image=ViewComments }
      { 1900000004;0 ;ActionContainer;
                      ActionContainerType=ActionItems }
      { 7       ;1   ;Action    ;
                      CaptionML=[ENU=&Navigate;
                                 PTG=&Navegar];
                      Promoted=Yes;
                      Image=Navigate;
                      PromotedCategory=Process;
                      OnAction=BEGIN
                                 Navigate;
                               END;
                                }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                CaptionML=[ENU=General;
                           PTG=Geral] }

    { 2   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the posted inventory pick number.;
                           PTG=""];
                SourceExpr="No." }

    { 8   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the location code for where the posted inventory pick occurred.;
                           PTG=""];
                SourceExpr="Location Code" }

    { 11  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the number of the posted source document that the inventory pick is based upon.;
                           PTG=""];
                SourceExpr="Source No." }

    { 17  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the number or the code of the customer, vendor, location, item, family, or sales order linked to the posted inventory pick.;
                           PTG=""];
                SourceExpr="Destination No.";
                CaptionClass=FORMAT(WMSMgt.GetCaption("Destination Type","Source Document",0));
                Editable=FALSE }

    { 5   ;2   ;Field     ;
                CaptionML=[ENU=Name;
                           PTG=Nome];
                SourceExpr=WMSMgt.GetDestinationName("Destination Type","Destination No.");
                CaptionClass=FORMAT(WMSMgt.GetCaption("Destination Type","Source Document",1));
                Editable=FALSE }

    { 15  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the posting date from the inventory pick.;
                           PTG=""];
                SourceExpr="Posting Date" }

    { 4   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the date on which the items on the posted inventory pick were expected to be shipped.;
                           PTG=""];
                SourceExpr="Shipment Date" }

    { 21  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the external document number from the source document associated with the posted inventory pick.;
                           PTG=""];
                SourceExpr="External Document No.";
                CaptionClass=FORMAT(WMSMgt.GetCaption("Destination Type","Source Document",2)) }

    { 20  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies a second external document number to be used for reference.;
                           PTG=""];
                SourceExpr="External Document No.2";
                CaptionClass=FORMAT(WMSMgt.GetCaption("Destination Type","Source Document",3)) }

    { 97  ;1   ;Part      ;
                Name=WhseActivityLines;
                SubPageView=SORTING(No.,Sorting Sequence No.);
                SubPageLink=No.=FIELD(No.);
                PagePartID=Page7393 }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                Visible=TRUE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {
    VAR
      WMSMgt@1001 : Codeunit 7302;

    BEGIN
    END.
  }
}

