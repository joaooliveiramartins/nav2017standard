OBJECT Page 375 Bank Account Statistics
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0,NAVPTSS81.00;
  }
  PROPERTIES
  {
    Editable=No;
    CaptionML=[ENU=Bank Account Statistics;
               PTG=Estat�sticas Conta Banc�ria];
    InsertAllowed=No;
    DeleteAllowed=No;
    LinksAllowed=No;
    SourceTable=Table270;
    DataCaptionFields=No.,Name;
    PageType=Card;
    OnAfterGetRecord=BEGIN
                       //soft,sn
                       WITH BankAcc DO BEGIN
                         COPY(Rec);
                       //soft,en
                         IF CurrentDate <> WORKDATE THEN BEGIN
                           CurrentDate := WORKDATE;
                           DateFilterCalc.CreateAccountingPeriodFilter(BankAccDateFilter[1],BankAccDateName[1],CurrentDate,0);
                           DateFilterCalc.CreateFiscalYearFilter(BankAccDateFilter[2],BankAccDateName[2],CurrentDate,0);
                           DateFilterCalc.CreateFiscalYearFilter(BankAccDateFilter[3],BankAccDateName[3],CurrentDate,-1);
                         END;

                         SETRANGE("Date Filter",0D,CurrentDate);
                         CALCFIELDS(Balance,"Balance (LCY)");
                         //soft,sn
                         SETRANGE("Date Filter");
                         SETRANGE("Dealing Type Filter",1); //Discount
                         CALCFIELDS("Posted Receiv. Bills Rmg. Amt.");
                         DocsForDiscRmgAmt := "Posted Receiv. Bills Rmg. Amt.";
                         IF "Credit Limit for Discount" = 0 THEN
                           IF DocsForDiscRmgAmt = 0 THEN
                             RiskPerc := 0
                           ELSE
                             RiskPerc := 100
                         ELSE
                           RiskPerc := DocsForDiscRmgAmt / "Credit Limit for Discount" * 100;

                         SETRANGE("Dealing Type Filter",0); //Collection
                         CALCFIELDS("Posted Receiv. Bills Rmg. Amt.","Posted Pay. Bills Rmg. Amt.","Posted Pay. Inv. Rmg. Amt.");
                         DocsForCollRmgAmt := "Posted Receiv. Bills Rmg. Amt.";
                         PayableDocsRmgAmt := "Posted Pay. Bills Rmg. Amt." + "Posted Pay. Inv. Rmg. Amt.";
                         //soft,en

                         FOR i := 1 TO 4 DO BEGIN
                           SETFILTER("Date Filter",BankAccDateFilter[i]);
                           CALCFIELDS("Net Change","Net Change (LCY)");
                           BankAccNetChange[i] := "Net Change";
                           BankAccNetChangeLCY[i] := "Net Change (LCY)";
                         END;
                         SETRANGE("Date Filter",0D,CurrentDate);
                         //soft,sn
                         FOR i := 1 TO 4 DO BEGIN
                           SETFILTER("Honored/Rejtd. at Date Filter",BankAccDateFilter[i]);
                           SETRANGE("Status Filter","Status Filter"::Honored);
                           CALCFIELDS("Closed Receiv. Bills Amt.");
                           CALCFIELDS("Posted Receiv. Bills Amt.");
                           CALCFIELDS("Clos. Receivable Inv. Amt.");
                           CALCFIELDS("Post. Receivable Inv. Amt.");
                           CALCFIELDS("Closed Pay. Bills Amt.");
                           CALCFIELDS("Posted Pay. Bills Amt.");
                           CALCFIELDS("Closed Pay. Invoices Amt.");
                           CALCFIELDS("Posted Pay. Invoices Amt.");
                           TotalHonoredDocs[i] := "Closed Receiv. Bills Amt." + "Posted Receiv. Bills Amt.";
                           TotalHonoredInvoices[i] := "Clos. Receivable Inv. Amt." + "Post. Receivable Inv. Amt.";
                           TotalHonoredPayableDoc[i] := "Closed Pay. Bills Amt." + "Closed Pay. Invoices Amt." +
                             "Posted Pay. Bills Amt." + "Posted Pay. Invoices Amt.";
                           SETRANGE("Status Filter","Status Filter"::Rejected);
                           CALCFIELDS("Closed Receiv. Bills Amt.");
                           CALCFIELDS("Posted Receiv. Bills Amt.");
                           CALCFIELDS("Clos. Receivable Inv. Amt.");
                           CALCFIELDS("Post. Receivable Inv. Amt.");
                           CALCFIELDS("Closed Pay. Bills Amt.");
                           CALCFIELDS("Posted Pay. Bills Amt.");
                           CALCFIELDS("Closed Pay. Invoices Amt.");
                           CALCFIELDS("Posted Pay. Invoices Amt.");
                           TotalRejectedInvoices[i] := "Clos. Receivable Inv. Amt." + "Post. Receivable Inv. Amt.";
                           TotalRejectedDocs[i] := "Closed Receiv. Bills Amt." + "Posted Receiv. Bills Amt.";
                           TotalRejectedPayableDoc[i] := "Closed Pay. Bills Amt." + "Closed Pay. Invoices Amt." +
                             "Posted Pay. Bills Amt." + "Posted Pay. Invoices Amt.";
                         END;

                         FOR i := 1 TO 4 DO BEGIN
                           DocCollExpAmt[i] := CollectionFeesTotalAmt(BankAccDateFilter[i]);
                           DocDiscExpAmt[i] := ServicesFeesTotalAmt(BankAccDateFilter[i]);
                           DocDiscIntAmt[i] := DiscInterestsTotalAmt(BankAccDateFilter[i]);
                           DocRejExpAmt[i] := RejExpensesAmt(BankAccDateFilter[i]);
                           FactDiscIntAmt[i] := DiscInterestFactTotalAmt(BankAccDateFilter[i]);
                           RiskFactExpAmt[i] := RiskFactFeesTotalAmt(BankAccDateFilter[i]);
                           UnriskFactExpAmt[i] := UnriskFactFeesTotalAmt(BankAccDateFilter[i]);
                           PmtOrdExpAmt[i] := PaymentOrderFeesTotalAmt(BankAccDateFilter[i]);
                         END;
                       END;
                       //soft,en
                     END;

  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                CaptionML=[ENU=Balance;
                           PTG=Saldo] }

    { 4   ;2   ;Field     ;
                CaptionML=[ENU=Balance (LCY);
                           PTG=Saldo (DL)];
                ToolTipML=[ENU=Specifies the bank account's current balance in LCY.;
                           PTG=Especifica o saldo atual da conta bancaria na divisa local.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Balance (LCY)";
                AutoFormatType=1 }

    { 3   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the bank account's current balance denominated in the applicable foreign currency.;
                           PTG=Especifica o saldo da conta bancaria expressona divisa estrangeira aplicavel.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=Balance;
                AutoFormatType=1;
                AutoFormatExpr="Currency Code" }

    { 12  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies a minimum balance for the bank account.;
                           PTG=Especifica um saldo m�nimo para a conta bancaria.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Min. Balance";
                AutoFormatType=1;
                AutoFormatExpr="Currency Code" }

    { 2   ;2   ;Field     ;
                Lookup=No;
                CaptionML=[ENU=Currency;
                           PTG=Divisa];
                ToolTipML=[ENU=Specifies the currency code for the bank account.;
                           PTG=Especifica o c�digo de divisa da conta bancaria.];
                ApplicationArea=#Suite;
                SourceExpr="Currency Code" }

    { 1907573401;1;Group  ;
                CaptionML=[ENU=Net Change;
                           PTG=Saldo Per�odo] }

    { 1904230801;2;Group  ;
                GroupType=FixedLayout }

    { 1903099001;3;Group  ;
                CaptionML=[ENU=This Period;
                           PTG=Per�odo Atual] }

    { 20  ;4   ;Field     ;
                CaptionML=[ENU=Date Name;
                           PTG=Nome Data];
                ToolTipML=[ENU=Specifies the date.;
                           PTG=Especifica a data.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=BankAccDateName[1] }

    { 6   ;4   ;Field     ;
                CaptionML=[ENU=Net Change;
                           PTG=Saldo Per�odo];
                ToolTipML=[ENU=Specifies the net value of entries in LCY on the bank account for the periods: Current Month, This Year, Last Year and To Date.;
                           PTG=Especifica o valor l�quido dos movimentos (na divisa local) da conta banc�ria para os seguintes per�odos: M�s Atual, Ano Atual, Ano Anterior e At� � Data.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=BankAccNetChange[1];
                AutoFormatType=1;
                AutoFormatExpr="Currency Code" }

    { 11  ;4   ;Field     ;
                CaptionML=[ENU=Net Change (LCY);
                           PTG=Saldo Per�odo (DL)];
                ToolTipML=[ENU=Specifies the net value of entries in LCY on the bank account for the periods: Current Month, This Year, Last Year, and To Date;
                           PTG=Especifica o valor l�quido dos movimentos (na divisa local) da conta banc�ria para os seguintes per�odos: M�s Atual, Ano Atual, Ano Anterior e At� � Data.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=BankAccNetChangeLCY[1];
                AutoFormatType=1 }

    { 1901313401;3;Group  ;
                CaptionML=[ENU=This Year;
                           PTG=Ano Atual] }

    { 26  ;4   ;Field     ;
                SourceExpr=Text000;
                Visible=FALSE }

    { 14  ;4   ;Field     ;
                CaptionML=[ENU=Net Change;
                           PTG=Saldo Per�odo];
                ToolTipML=[ENU=Specifies the net value of entries in LCY on the bank account for the periods: Current Month, This Year, Last Year and To Date.;
                           PTG=Especifica o valor l�quido dos movimentos (na divisa local) da conta banc�ria para os seguintes per�odos: M�s Atual, Ano Atual, Ano Anterior e At� � Data.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=BankAccNetChange[2];
                AutoFormatType=1;
                AutoFormatExpr="Currency Code" }

    { 15  ;4   ;Field     ;
                CaptionML=[ENU=Net Change (LCY);
                           PTG=Saldo Per�odo (DL)];
                ToolTipML=[ENU=Specifies the net value of entries in LCY on the bank account for the periods: Current Month, This Year, Last Year, and To Date.;
                           PTG=Especifica o valor l�quido dos movimentos (na divisa local) da conta banc�ria para os seguintes per�odos: M�s Atual, Ano Atual, Ano Anterior e At� � Data.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=BankAccNetChangeLCY[2];
                AutoFormatType=1 }

    { 1902759801;3;Group  ;
                CaptionML=[ENU=Last Year;
                           PTG=Ano Anterior] }

    { 27  ;4   ;Field     ;
                SourceExpr=Text000;
                Visible=FALSE }

    { 16  ;4   ;Field     ;
                CaptionML=[ENU=Net Change;
                           PTG=Saldo Per�odo];
                ToolTipML=[ENU=Specifies the net value of entries in LCY on the bank account for the periods: Current Month, This Year, Last Year and To Date.;
                           PTG=Especifica o valor l�quido dos movimentos (na divisa local) da conta banc�ria para os seguintes per�odos: M�s Atual, Ano Atual, Ano Anterior e At� � Data.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=BankAccNetChange[3];
                AutoFormatType=1;
                AutoFormatExpr="Currency Code" }

    { 17  ;4   ;Field     ;
                CaptionML=[ENU=Net Change (LCY);
                           PTG=Saldo Per�odo (DL)];
                ToolTipML=[ENU=Specifies the net value of entries in LCY on the bank account for the periods: Current Month, This Year, Last Year, and To Date.;
                           PTG=Especifica o valor l�quido dos movimentos (na divisa local) da conta banc�ria para os seguintes per�odos: M�s Atual, Ano Atual, Ano Anterior e At� � Data.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=BankAccNetChangeLCY[3];
                AutoFormatType=1 }

    { 1900206201;3;Group  ;
                CaptionML=[ENU=To Date;
                           PTG=� Data] }

    { 28  ;4   ;Field     ;
                SourceExpr=Text000;
                Visible=FALSE }

    { 18  ;4   ;Field     ;
                CaptionML=[ENU=Net Change;
                           PTG=Saldo Per�odo];
                ToolTipML=[ENU=Specifies the net value of entries in LCY on the bank account for the periods: Current Month, This Year, Last Year and To Date.;
                           PTG=Especifica o valor l�quido dos movimentos (na divisa local) da conta banc�ria para os seguintes per�odos: M�s Atual, Ano Atual, Ano Anterior e At� � Data.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=BankAccNetChange[4];
                AutoFormatType=1;
                AutoFormatExpr="Currency Code" }

    { 19  ;4   ;Field     ;
                CaptionML=[ENU=Net Change (LCY);
                           PTG=Saldo Per�odo (DL)];
                ToolTipML=[ENU=Specifies the net value of entries in LCY on the bank account for the periods: Current Month, This Year, Last Year, and To Date.;
                           PTG=Especifica o valor l�quido dos movimentos (na divisa local) da conta banc�ria para os seguintes per�odos: M�s Atual, Ano Atual, Ano Anterior e At� � Data.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=BankAccNetChangeLCY[4];
                AutoFormatType=1 }

    { 31022956;1;Group    ;
                CaptionML=[ENU=Receivable Bills;
                           PTG=T�tulos � Cobran�a] }

    { 31022955;2;Group    ;
                GroupType=FixedLayout }

    { 31022927;3;Group    ;
                CaptionML=[ENU=This Period;
                           PTG=Per�odo Atual] }

    { 31022900;4;Field    ;
                SourceExpr=Text000;
                Visible=FALSE }

    { 31022926;4;Field    ;
                ToolTipML=[ENU=Shows the amount of the bills, included in the bill groups, posted and delivered to this bank.;
                           PTG=Mostra o valor dos t�tulos, inclu�dos em remesas, registados e entregues a este banco.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Posted Receiv. Bills Amt.";
                AutoFormatType=1;
                AutoFormatExpr="Currency Code" }

    { 31022953;4;Field    ;
                ToolTipML=[ENU=Shows the amount of the closed bills delivered to this bank.;
                           PTG=Mostra o valor dos t�tulos fechados entregues a este banco.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Closed Receiv. Bills Amt.";
                AutoFormatType=1;
                AutoFormatExpr="Currency Code" }

    { 1110105;4;Field     ;
                CaptionML=[ENU=Total Honored Bills Amt.;
                           PTG=Valor Total T�tulos Pagos];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=TotalHonoredDocs[1];
                AutoFormatType=1;
                AutoFormatExpr="Currency Code" }

    { 1110112;4;Field     ;
                CaptionML=[ENU=Total Rejected Bills Amt.;
                           PTG=Valor Total T�tulos Devolvidos];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=TotalRejectedDocs[1];
                AutoFormatType=1;
                AutoFormatExpr="Currency Code" }

    { 1110116;4;Field     ;
                CaptionML=[ENU=Discount Interests Amt.;
                           PTG=Valor Juros Desconto];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=DocDiscIntAmt[1];
                AutoFormatType=1;
                AutoFormatExpr="Currency Code" }

    { 1110120;4;Field     ;
                CaptionML=[ENU=Discount Expenses Amt.;
                           PTG=Valos Despesas Desconto];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=DocDiscExpAmt[1];
                AutoFormatType=1;
                AutoFormatExpr="Currency Code" }

    { 1110124;4;Field     ;
                CaptionML=[ENU=Collection Expenses Amt.;
                           PTG=Valor Despesas Cobran�a];
                SourceExpr=DocCollExpAmt[1];
                AutoFormatType=1;
                AutoFormatExpr="Currency Code" }

    { 1110128;4;Field     ;
                CaptionML=[ENU=Rejection Expenses Amt.;
                           PTG=Valor Despesas Devolu��o];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=DocRejExpAmt[1];
                AutoFormatType=1;
                AutoFormatExpr="Currency Code" }

    { 31022892;3;Group    ;
                CaptionML=[ENU=This Year;
                           PTG=Ano Atual] }

    { 31022937;4;Field    ;
                SourceExpr=Text000;
                Visible=FALSE }

    { 31022958;4;Field    ;
                SourceExpr=Text000;
                Visible=FALSE }

    { 1110106;4;Field     ;
                ApplicationArea=#Basic,#Suite;
                SourceExpr=TotalHonoredDocs[2];
                AutoFormatType=1;
                AutoFormatExpr="Currency Code" }

    { 1110109;4;Field     ;
                ApplicationArea=#Basic,#Suite;
                SourceExpr=TotalRejectedDocs[2];
                AutoFormatType=1;
                AutoFormatExpr="Currency Code" }

    { 1110113;4;Field     ;
                ApplicationArea=#Basic,#Suite;
                SourceExpr=DocDiscIntAmt[2];
                AutoFormatType=1;
                AutoFormatExpr="Currency Code" }

    { 1110119;4;Field     ;
                ApplicationArea=#Basic,#Suite;
                SourceExpr=DocDiscExpAmt[2];
                AutoFormatType=1;
                AutoFormatExpr="Currency Code" }

    { 1110123;4;Field     ;
                ApplicationArea=#Basic,#Suite;
                SourceExpr=DocCollExpAmt[2];
                AutoFormatType=1;
                AutoFormatExpr="Currency Code" }

    { 1110127;4;Field     ;
                ApplicationArea=#Basic,#Suite;
                SourceExpr=DocRejExpAmt[2];
                AutoFormatType=1;
                AutoFormatExpr="Currency Code" }

    { 31022899;3;Group    ;
                CaptionML=[ENU=Last Year;
                           PTG=Ano Anterior] }

    { 31022890;4;Field    ;
                SourceExpr=Text000;
                Visible=FALSE }

    { 31022938;4;Field    ;
                SourceExpr=Text000;
                Visible=FALSE }

    { 31022898;4;Field    ;
                ApplicationArea=#Basic,#Suite;
                SourceExpr=TotalHonoredDocs[3];
                AutoFormatType=1;
                AutoFormatExpr="Currency Code" }

    { 31022897;4;Field    ;
                ApplicationArea=#Basic,#Suite;
                SourceExpr=TotalRejectedDocs[3];
                AutoFormatType=1;
                AutoFormatExpr="Currency Code" }

    { 31022896;4;Field    ;
                ApplicationArea=#Basic,#Suite;
                SourceExpr=DocDiscIntAmt[3];
                AutoFormatType=1;
                AutoFormatExpr="Currency Code" }

    { 31022895;4;Field    ;
                ApplicationArea=#Basic,#Suite;
                SourceExpr=DocDiscExpAmt[3];
                AutoFormatType=1;
                AutoFormatExpr="Currency Code" }

    { 31022894;4;Field    ;
                ApplicationArea=#Basic,#Suite;
                SourceExpr=DocCollExpAmt[3];
                AutoFormatType=1;
                AutoFormatExpr="Currency Code" }

    { 31022893;4;Field    ;
                ApplicationArea=#Basic,#Suite;
                SourceExpr=DocRejExpAmt[3];
                AutoFormatType=1;
                AutoFormatExpr="Currency Code" }

    { 31022903;3;Group    ;
                CaptionML=[ENU=To Date;
                           PTG=At� � Data] }

    { 31022902;4;Field    ;
                SourceExpr=Text000;
                Visible=FALSE }

    { 31022940;4;Field    ;
                SourceExpr=Text000;
                Visible=FALSE }

    { 1110108;4;Field     ;
                ApplicationArea=#Basic,#Suite;
                SourceExpr=TotalHonoredDocs[4];
                AutoFormatType=1;
                AutoFormatExpr="Currency Code" }

    { 1110111;4;Field     ;
                ApplicationArea=#Basic,#Suite;
                SourceExpr=TotalRejectedDocs[4];
                AutoFormatType=1;
                AutoFormatExpr="Currency Code" }

    { 1110115;4;Field     ;
                ApplicationArea=#Basic,#Suite;
                SourceExpr=DocDiscIntAmt[4];
                AutoFormatType=1;
                AutoFormatExpr="Currency Code" }

    { 1110118;4;Field     ;
                ApplicationArea=#Basic,#Suite;
                SourceExpr=DocDiscExpAmt[4];
                AutoFormatType=1;
                AutoFormatExpr="Currency Code" }

    { 1110122;4;Field     ;
                ApplicationArea=#Basic,#Suite;
                SourceExpr=DocCollExpAmt[4];
                AutoFormatType=1;
                AutoFormatExpr="Currency Code" }

    { 1110125;4;Field     ;
                ApplicationArea=#Basic,#Suite;
                SourceExpr=DocRejExpAmt[4];
                AutoFormatType=1;
                AutoFormatExpr="Currency Code" }

    { 1904371301;1;Group  ;
                CaptionML=[ENU=Factoring;
                           PTG=Factoring] }

    { 31022908;2;Group    ;
                GroupType=FixedLayout }

    { 31022907;3;Group    ;
                CaptionML=[ENU=This Period;
                           PTG=Per�odo Atual] }

    { 66  ;4   ;Field     ;
                ApplicationArea=#Basic,#Suite;
                SourceExpr=BankAccDateName[1] }

    { 1110134;4;Field     ;
                CaptionML=[ENU=Total Honored Invoice Amt.;
                           PTG=Valor Total Pago Faturas];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=TotalHonoredInvoices[1];
                AutoFormatType=1;
                AutoFormatExpr="Currency Code" }

    { 1110135;4;Field     ;
                CaptionML=[ENU=Total Rejected Inv. Amt.;
                           PTG=Valor Total Devolvido Faturas];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=TotalRejectedInvoices[1];
                AutoFormatType=1;
                AutoFormatExpr="Currency Code" }

    { 1110136;4;Field     ;
                CaptionML=[ENU=Discount Interests Amt.;
                           PTG=Valor Juros Desconto];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=FactDiscIntAmt[1];
                AutoFormatType=1;
                AutoFormatExpr="Currency Code" }

    { 1110137;4;Field     ;
                CaptionML=[ENU=Risked Factoring Exp. Amt.;
                           PTG=Valor Serv. Factoring c/Risco];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=RiskFactExpAmt[1];
                AutoFormatType=1;
                AutoFormatExpr="Currency Code" }

    { 1110138;4;Field     ;
                CaptionML=[ENU=Unrisked Factoring Exp. Amt.;
                           PTG=Valor Serv. Factoring s/Risco];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=UnriskFactExpAmt[1];
                AutoFormatType=1;
                AutoFormatExpr="Currency Code" }

    { 31022905;4;Field    ;
                ToolTipML=[ENU=Shows the amount of the invoices included in bill groups, posted and delivered to this bank.;
                           PTG=Mostrao valor de faturas, inclu�das em remessas, registadas e entregues a este banco.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Post. Receivable Inv. Amt.";
                AutoFormatType=1;
                AutoFormatExpr="Currency Code" }

    { 31022901;4;Field    ;
                ToolTipML=[ENU=Shows the amount of the closed invoices delivered to this bank.;
                           PTG=Mostra o valor das faturas fechadas, entregues a este banco.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Clos. Receivable Inv. Amt.";
                AutoFormatType=1;
                AutoFormatExpr="Currency Code" }

    { 31022909;3;Group    ;
                CaptionML=[ENU=This Year;
                           PTG=Ano Atual] }

    { 31022942;4;Field    ;
                SourceExpr=Text000;
                Visible=FALSE }

    { 1110139;4;Field     ;
                ApplicationArea=#Basic,#Suite;
                SourceExpr=TotalHonoredInvoices[2];
                AutoFormatType=1;
                AutoFormatExpr="Currency Code" }

    { 1110140;4;Field     ;
                ApplicationArea=#Basic,#Suite;
                SourceExpr=TotalRejectedInvoices[2];
                AutoFormatType=1;
                AutoFormatExpr="Currency Code" }

    { 1110145;4;Field     ;
                ApplicationArea=#Basic,#Suite;
                SourceExpr=FactDiscIntAmt[2];
                AutoFormatType=1;
                AutoFormatExpr="Currency Code" }

    { 1110146;4;Field     ;
                ApplicationArea=#Basic,#Suite;
                SourceExpr=RiskFactExpAmt[2];
                AutoFormatType=1;
                AutoFormatExpr="Currency Code" }

    { 1110144;4;Field     ;
                ApplicationArea=#Basic,#Suite;
                SourceExpr=UnriskFactExpAmt[2];
                AutoFormatType=1;
                AutoFormatExpr="Currency Code" }

    { 31022962;4;Field    ;
                SourceExpr=Text000;
                Visible=FALSE }

    { 31022917;4;Field    ;
                SourceExpr=Text000;
                Visible=FALSE }

    { 31022919;3;Group    ;
                CaptionML=[ENU=Last Year;
                           PTG=Ano Anterior] }

    { 31022943;4;Field    ;
                SourceExpr=Text000;
                Visible=FALSE }

    { 31022915;4;Field    ;
                ApplicationArea=#Basic,#Suite;
                SourceExpr=TotalHonoredInvoices[3];
                AutoFormatType=1;
                AutoFormatExpr="Currency Code" }

    { 31022914;4;Field    ;
                ApplicationArea=#Basic,#Suite;
                SourceExpr=TotalRejectedInvoices[3];
                AutoFormatType=1;
                AutoFormatExpr="Currency Code" }

    { 31022913;4;Field    ;
                ApplicationArea=#Basic,#Suite;
                SourceExpr=FactDiscIntAmt[3];
                AutoFormatType=1;
                AutoFormatExpr="Currency Code" }

    { 31022912;4;Field    ;
                ApplicationArea=#Basic,#Suite;
                SourceExpr=RiskFactExpAmt[3];
                AutoFormatType=1;
                AutoFormatExpr="Currency Code" }

    { 31022911;4;Field    ;
                ApplicationArea=#Basic,#Suite;
                SourceExpr=UnriskFactExpAmt[3];
                AutoFormatType=1;
                AutoFormatExpr="Currency Code" }

    { 31022963;4;Field    ;
                SourceExpr=Text000;
                Visible=FALSE }

    { 31022947;4;Field    ;
                SourceExpr=Text000;
                Visible=FALSE }

    { 31022921;3;Group    ;
                CaptionML=[ENU=To Date;
                           PTG=At� � Data] }

    { 31022967;4;Field    ;
                SourceExpr=Text000;
                Visible=FALSE }

    { 1110143;4;Field     ;
                ApplicationArea=#Basic,#Suite;
                SourceExpr=TotalHonoredInvoices[4];
                AutoFormatType=1;
                AutoFormatExpr="Currency Code" }

    { 1110243;4;Field     ;
                ApplicationArea=#Basic,#Suite;
                SourceExpr=TotalRejectedInvoices[4];
                AutoFormatType=1;
                AutoFormatExpr="Currency Code" }

    { 1110151;4;Field     ;
                ApplicationArea=#Basic,#Suite;
                SourceExpr=FactDiscIntAmt[4];
                AutoFormatType=1;
                AutoFormatExpr="Currency Code" }

    { 1110152;4;Field     ;
                ApplicationArea=#Basic,#Suite;
                SourceExpr=RiskFactExpAmt[4];
                AutoFormatType=1;
                AutoFormatExpr="Currency Code" }

    { 1110150;4;Field     ;
                ApplicationArea=#Basic,#Suite;
                SourceExpr=UnriskFactExpAmt[4];
                AutoFormatType=1;
                AutoFormatExpr="Currency Code" }

    { 31022950;4;Field    ;
                SourceExpr=Text000;
                Visible=FALSE }

    { 31022923;4;Field    ;
                SourceExpr=Text000;
                Visible=FALSE }

    { 1901422401;1;Group  ;
                CaptionML=[ENU=Payable Documents;
                           PTG=Documentos a Pagar] }

    { 31022925;2;Group    ;
                GroupType=FixedLayout }

    { 31022924;3;Group    ;
                CaptionML=[ENU=This Period;
                           PTG=Per�odo Atual] }

    { 91  ;4   ;Field     ;
                ApplicationArea=#Basic,#Suite;
                SourceExpr=BankAccDateName[1] }

    { 1110154;4;Field     ;
                CaptionML=[ENU=Total Honored Amt.;
                           PTG=Valor Total Pago];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=TotalHonoredPayableDoc[1];
                AutoFormatType=1;
                AutoFormatExpr="Currency Code" }

    { 1110155;4;Field     ;
                CaptionML=[ENU=Payment Order Expenses Amt.;
                           PTG=Valor Despesas Ordem Pagamento];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=PmtOrdExpAmt[1];
                AutoFormatType=1;
                AutoFormatExpr="Currency Code" }

    { 31022918;4;Field    ;
                CaptionML=[ENU=Posted Documents;
                           PTG=Documentos Registados];
                ToolTipML=[ENU=Shows the value of the pending amount, for payable documents posted to this bank.;
                           PTG=Mostra o valor pendente  dos documentos a pagar registados neste banco.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Posted Pay. Documents Amt.";
                AutoFormatType=1;
                AutoFormatExpr="Currency Code" }

    { 31022916;4;Field    ;
                CaptionML=[ENU=Closed Documents;
                           PTG=Documentos Fechados];
                ToolTipML=[ENU=Shows the amount of the closed payable documents delivered to this bank.;
                           PTG=Mostra o valor dos documentos a pagar fechados entregues a este banco.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Closed Pay. Documents Amt.";
                AutoFormatType=1;
                AutoFormatExpr="Currency Code" }

    { 31022931;3;Group    ;
                CaptionML=[ENU=This Year;
                           PTG=Ano Atual] }

    { 31022954;4;Field    ;
                SourceExpr=Text000;
                Visible=FALSE }

    { 1110159;4;Field     ;
                ApplicationArea=#Basic,#Suite;
                SourceExpr=TotalHonoredPayableDoc[2];
                AutoFormatType=1;
                AutoFormatExpr="Currency Code" }

    { 1110156;4;Field     ;
                ApplicationArea=#Basic,#Suite;
                SourceExpr=PmtOrdExpAmt[2];
                AutoFormatType=1;
                AutoFormatExpr="Currency Code" }

    { 31022959;4;Field    ;
                SourceExpr=Text000;
                Visible=FALSE }

    { 31022929;4;Field    ;
                SourceExpr=Text000;
                Visible=FALSE }

    { 31022935;3;Group    ;
                CaptionML=[ENU=Last Year;
                           PTG=Ano Anterior] }

    { 31022928;4;Field    ;
                SourceExpr=Text000;
                Visible=FALSE }

    { 31022933;4;Field    ;
                ApplicationArea=#Basic,#Suite;
                SourceExpr=TotalHonoredPayableDoc[3];
                AutoFormatType=1;
                AutoFormatExpr="Currency Code" }

    { 31022932;4;Field    ;
                ApplicationArea=#Basic,#Suite;
                SourceExpr=PmtOrdExpAmt[3];
                AutoFormatType=1;
                AutoFormatExpr="Currency Code" }

    { 31022960;4;Field    ;
                SourceExpr=Text000;
                Visible=FALSE }

    { 31022934;4;Field    ;
                SourceExpr=Text000;
                Visible=FALSE }

    { 31022939;3;Group    ;
                CaptionML=[ENU=To Date;
                           PTG=At� � Data] }

    { 31022930;4;Field    ;
                SourceExpr=Text000;
                Visible=FALSE }

    { 1110161;4;Field     ;
                ApplicationArea=#Basic,#Suite;
                SourceExpr=TotalHonoredPayableDoc[4];
                AutoFormatType=1;
                AutoFormatExpr="Currency Code" }

    { 1110158;4;Field     ;
                ApplicationArea=#Basic,#Suite;
                SourceExpr=PmtOrdExpAmt[4];
                AutoFormatType=1;
                AutoFormatExpr="Currency Code" }

    { 31022922;2;Field    ;
                SourceExpr=Text000;
                Visible=FALSE }

    { 31022946;2;Field    ;
                SourceExpr=Text000;
                Visible=FALSE }

    { 1903727601;1;Group  ;
                CaptionML=[ENU=Bill Groups;
                           PTG=Remessas] }

    { 1100062;2;Field     ;
                SourceExpr=Text000;
                Visible=FALSE }

    { 1100063;2;Field     ;
                SourceExpr=Text000;
                Visible=FALSE }

    { 31022945;2;Group    ;
                GroupType=FixedLayout }

    { 31022944;3;Group     }

    { 1110162;4;Field     ;
                ToolTipML=[ENU=Specifies the number of the last posted bill group sent to this bank.;
                           PTG=Especifica o n�mero da �ltima remessa registada que se enviou a este banco.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Last Bill Gr. No." }

    { 1110164;4;Field     ;
                ToolTipML=[ENU=Shows the posting date of the last bill group sent to this bank.;
                           PTG=Mostra a data de registo da �ltima remessa enviada a este banco.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Date of Last Post. Bill Gr." }

    { 1110166;4;Field     ;
                ToolTipML=[ENU=Specifies the credit limit for the discount of bills available at this particular bank.;
                           PTG=Especifica o limite de cr�dito para o desconto de t�tulos dispon�vel neste banco em particular.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Credit Limit for Discount";
                AutoFormatType=1;
                AutoFormatExpr="Currency Code" }

    { 1110026;4;Field     ;
                CaptionML=[ENU=Bills for Disc. Remg. Amt.;
                           PTG=Valor Pendente T�tulos ao Desconto];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=DocsForDiscRmgAmt;
                AutoFormatType=1;
                AutoFormatExpr="Currency Code" }

    { 1110028;4;Field     ;
                CaptionML=[ENU=Bills for Coll. Remg. Amt.;
                           PTG=Valor Pendente T�tulos � Cobran�a];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=DocsForCollRmgAmt;
                AutoFormatType=1;
                AutoFormatExpr="Currency Code" }

    { 31022951;3;Group     }

    { 31022949;4;Field    ;
                SourceExpr=Text000;
                Visible=FALSE }

    { 31022891;4;Field    ;
                SourceExpr=Text000;
                Visible=FALSE }

    { 31022948;4;Field    ;
                SourceExpr=Text000;
                Visible=FALSE }

    { 1110168;4;Field     ;
                ExtendedDatatype=Ratio;
                ApplicationArea=#Basic,#Suite;
                SourceExpr=RiskPerc;
                MinValue=0;
                MaxValue=100 }

    { 31022952;4;Field    ;
                SourceExpr=Text000;
                Visible=FALSE }

  }
  CODE
  {
    VAR
      DateFilterCalc@1000 : Codeunit 358;
      BankAccDateFilter@1001 : ARRAY [4] OF Text[30];
      BankAccDateName@1002 : ARRAY [4] OF Text[30];
      CurrentDate@1003 : Date;
      BankAccNetChange@1004 : ARRAY [4] OF Decimal;
      BankAccNetChangeLCY@1005 : ARRAY [4] OF Decimal;
      i@1006 : Integer;
      Text000@1007 : TextConst 'ENU=Placeholder;PTG=Marcador de posi��o';
      "//--soft-global--//"@9000000 : Integer;
      BankAcc@1110000 : Record 270;
      DocsForDiscRmgAmt@1110008 : Decimal;
      DocsForCollRmgAmt@1110009 : Decimal;
      PayableDocsRmgAmt@1110010 : Decimal;
      DocCollExpAmt@1110011 : ARRAY [4] OF Decimal;
      DocDiscIntAmt@1110012 : ARRAY [4] OF Decimal;
      DocDiscExpAmt@1110013 : ARRAY [4] OF Decimal;
      DocRejExpAmt@1110014 : ARRAY [4] OF Decimal;
      TotalHonoredDocs@1110015 : ARRAY [4] OF Decimal;
      TotalRejectedDocs@1110016 : ARRAY [4] OF Decimal;
      RiskPerc@1110028 : Decimal;
      TotalHonoredInvoices@1110018 : ARRAY [4] OF Decimal;
      TotalRejectedInvoices@1110027 : ARRAY [4] OF Decimal;
      FactDiscIntAmt@1110020 : ARRAY [4] OF Decimal;
      RiskFactExpAmt@1110021 : ARRAY [4] OF Decimal;
      UnriskFactExpAmt@1110022 : ARRAY [4] OF Decimal;
      TotalHonoredPayableDoc@1110023 : ARRAY [4] OF Decimal;
      TotalRejectedPayableDoc@1110024 : ARRAY [4] OF Decimal;
      PmtOrdExpAmt@1110025 : ARRAY [4] OF Decimal;
      "//--soft-text--//"@1011 : TextConst;
      Text31022890@1010 : TextConst 'ENU=This Year;PTG=Este Ano';
      Text31022891@1009 : TextConst 'ENU=Last Year;PTG=Ano Anterior';
      Text31022892@1008 : TextConst 'ENU=To Date;PTG=� Data';

    BEGIN
    END.
  }
}

