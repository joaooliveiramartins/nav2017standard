OBJECT Page 2109 O365 Customer Sales Documents
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0;
  }
  PROPERTIES
  {
    Editable=No;
    CaptionML=[ENU=Invoices for Customer;
               PTG=Faturas para Cliente];
    InsertAllowed=No;
    DeleteAllowed=No;
    ModifyAllowed=No;
    SourceTable=Table2103;
    SourceTableView=SORTING(Sell-to Customer Name);
    PageType=List;
    SourceTableTemporary=Yes;
    OnFindRecord=BEGIN
                   EXIT(OnFind(Which));
                 END;

    OnNextRecord=BEGIN
                   EXIT(OnNext(Steps));
                 END;

    ActionList=ACTIONS
    {
      { 16      ;    ;ActionContainer;
                      ActionContainerType=ActionItems }
      { 17      ;1   ;Action    ;
                      ShortCutKey=Return;
                      CaptionML=[ENU=View;
                                 PTG=Ver];
                      ToolTipML=[ENU=Open the card for the selected record.;
                                 PTG=Abrir a ficha para o registo selecionado.];
                      ApplicationArea=#Basic, #Suite;
                      Image=DocumentEdit;
                      Scope=Repeater;
                      OnAction=VAR
                                 SalesHeader@1000 : Record 36;
                                 SalesInvoiceHeader@1001 : Record 112;
                               BEGIN
                                 IF Posted THEN BEGIN
                                   IF NOT SalesInvoiceHeader.GET("No.") THEN
                                     EXIT;
                                   SalesInvoiceHeader.SETRECFILTER;
                                   PAGE.RUN(PAGE::"O365 Posted Sales Invoice",SalesInvoiceHeader);
                                 END ELSE BEGIN
                                   IF NOT SalesHeader.GET("Document Type","No.") THEN
                                     EXIT;
                                   SalesHeader.SETRECFILTER;
                                   PAGE.RUN(PAGE::"O365 Sales Invoice",SalesHeader);
                                 END;
                               END;
                                }
      { 22      ;1   ;ActionGroup;
                      CaptionML=[ENU=Payments;
                                 PTG=Pagamentos] }
      { 20      ;2   ;Action    ;
                      Name=ShowPayments;
                      CaptionML=[ENU=Show Payments;
                                 PTG=Mostrar Pagamentos];
                      ToolTipML=[ENU=Show a list of payments made for this invoice.;
                                 PTG=Mostrar lista de pagamentos feitos para esta fatura.];
                      ApplicationArea=#Basic,#Suite;
                      Visible=Posted;
                      Image=Payment;
                      Scope=Repeater;
                      OnAction=BEGIN
                                 O365SalesInvoicePayment.ShowHistory("No.");
                               END;
                                }
      { 19      ;2   ;Action    ;
                      Name=MarkAsPaid;
                      CaptionML=[ENU=Mark as paid;
                                 PTG=Marcar como pago];
                      ToolTipML=[ENU=Pay the invoice as specified in the default Payment Registration Setup.;
                                 PTG=Pagar a fatura como especificado na Configura��o Registo Pagamentos por defeito.];
                      ApplicationArea=#Basic,#Suite;
                      Visible=Posted AND ("outstanding amount" > 0);
                      Image=Payment;
                      Scope=Repeater;
                      OnAction=BEGIN
                                 O365SalesInvoicePayment.MarkAsPaid("No.");
                               END;

                      Gesture=RightSwipe }
      { 18      ;2   ;Action    ;
                      Name=MarkAsUnpaid;
                      CaptionML=[ENU=Mark as unpaid;
                                 PTG=Marcar como n�o pago.];
                      ToolTipML=[ENU=Mark the invoice as unpaid.;
                                 PTG=Marcar a fatura como n�o paga.];
                      ApplicationArea=#Basic,#Suite;
                      Visible=Posted AND ("outstanding amount" = 0);
                      Image=Cancel;
                      Scope=Repeater;
                      OnAction=BEGIN
                                 O365SalesInvoicePayment.CancelSalesInvoicePayment("No.");
                               END;

                      Gesture=RightSwipe }
    }
  }
  CONTROLS
  {
    { 1   ;    ;Container ;
                ContainerType=ContentArea }

    { 15  ;1   ;Group     ;
                GroupType=Repeater }

    { 14  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the type of the document.;
                           PTG=Especifica o tipo de documento.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Document Type" }

    { 13  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the document number that references the document.;
                           PTG=Especifica o n�mero de documento.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="No." }

    { 12  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the number of the customer who will receive the products and be billed by default.;
                           PTG=Especifica o n�mero do cliente que vai receber os produtos e vai ser faturado por defeito.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Sell-to Customer No." }

    { 11  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the name of the customer who will receive the products and be billed by default.;
                           PTG=Especifica o nome do cliente que vai receber os produtos e vai ser faturado por defeito.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Sell-to Customer Name" }

    { 10  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the name of the person to contact at the customer that the items were sold to.;
                           PTG=Especifica o nome da pessoa de contacto no cliente ao qual os produtos foram vendidos.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Sell-to Contact" }

    { 9   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the date on which you created the sales document.;
                           PTG=Especifica a data em que criou o documento de venda];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Document Date" }

    { 7   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the currency of amounts on the sales document.;
                           PTG=Especifica o valor da moeda nos documentos de venda.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Currency Code" }

    { 8   ;2   ;Field     ;
                ToolTipML=[ENU="Specifies the currency with its symbol, such as $ for Dollar. ";
                           PTG=Especifica a moeda com o seu s�mbolo, tal como $ para d�lar.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Currency Symbol" }

    { 6   ;2   ;Field     ;
                ApplicationArea=#Basic,#Suite;
                SourceExpr=Posted }

    { 5   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the status of the document that represents the forecast entry.;
                           PTG=Especifica o estado do documento que representa o movimento de previs�o.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Document Status" }

    { 4   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the total invoices amount, displayed in Brick view.;
                           PTG=Especifica o valor total de faturas, mostrado na vista em Bloco.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Brick Total" }

    { 3   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the outstanding amount, meaning the amount not paid, displayed in Brick view.;
                           PTG=Especifica o valor pendente, correspondente ao valor n�o pago, mostrado na vista em Bloco.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Brick Outstanding" }

    { 2   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the visual identifier of the document format.;
                           PTG=Especifica o identificador visual do formato do documento.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Document Icon" }

  }
  CODE
  {
    VAR
      O365SalesInvoicePayment@1000 : Codeunit 2105;

    BEGIN
    END.
  }
}

