OBJECT Page 31023031 Cartera Receiv. Statistics FB
{
  OBJECT-PROPERTIES
  {
    Date=01/02/15;
    Time=13:00:00;
    Version List=NAVPTSS81.00;
  }
  PROPERTIES
  {
    Editable=No;
    CaptionML=[ENU=Cartera Receivables Statistics;
               PTG=Estat�sticas Cobran�as em Carteira];
    SourceTable=Table18;
    PageType=CardPart;
    OnAfterGetRecord=VAR
                       CostCalcMgt@1000 : Codeunit 5836;
                     BEGIN
                       IF CurrentDate <> WORKDATE THEN BEGIN
                         CurrentDate := WORKDATE;
                         DateFilterCalc.CreateAccountingPeriodFilter(CustDateFilter[1],CustDateName[1],CurrentDate,0);
                         DateFilterCalc.CreateFiscalYearFilter(CustDateFilter[2],CustDateName[2],CurrentDate,0);
                         DateFilterCalc.CreateFiscalYearFilter(CustDateFilter[3],CustDateName[3],CurrentDate,-1);
                       END;

                       SETRANGE("Date Filter",0D,CurrentDate);
                       CALCFIELDS(
                         Balance,"Balance (LCY)","Balance Due","Balance Due (LCY)",
                         "Outstanding Orders (LCY)","Shipped Not Invoiced (LCY)");

                       TotalAmountLCY := "Balance (LCY)" + "Outstanding Orders (LCY)" + "Shipped Not Invoiced (LCY)" + "Outstanding Invoices (LCY)";

                       FOR i := 1 TO 4 DO BEGIN
                         SETFILTER("Date Filter",CustDateFilter[i]);
                         CALCFIELDS(
                           "Sales (LCY)","Profit (LCY)","Inv. Discounts (LCY)","Inv. Amounts (LCY)","Pmt. Discounts (LCY)",
                           "Pmt. Disc. Tolerance (LCY)","Pmt. Tolerance (LCY)",
                           "Fin. Charge Memo Amounts (LCY)","Cr. Memo Amounts (LCY)","Payments (LCY)",
                           "Reminder Amounts (LCY)","Refunds (LCY)","Other Amounts (LCY)");
                         CustSalesLCY[i] := "Sales (LCY)";
                         CustProfit[i] := "Profit (LCY)" + CostCalcMgt.NonInvtblCostAmt(Rec);
                         AdjmtCostLCY[i] := CostCalcMgt.CalcCustAdjmtCostLCY(Rec);
                         AdjCustProfit[i] := CustProfit[i] + AdjmtCostLCY[i];

                         IF "Sales (LCY)" <> 0 THEN BEGIN
                           ProfitPct[i] := ROUND(100 * CustProfit[i] / "Sales (LCY)",0.1);
                           AdjProfitPct[i] := ROUND(100 * AdjCustProfit[i] / "Sales (LCY)",0.1);
                         END ELSE BEGIN
                           ProfitPct[i] := 0;
                           AdjProfitPct[i] := 0;
                         END;

                         InvAmountsLCY[i] := "Inv. Amounts (LCY)";
                         CustInvDiscAmountLCY[i] := "Inv. Discounts (LCY)";
                         CustPaymentDiscLCY[i] := "Pmt. Discounts (LCY)";
                         CustPaymentDiscTolLCY[i] := "Pmt. Disc. Tolerance (LCY)";
                         CustPaymentTolLCY[i] := "Pmt. Tolerance (LCY)";
                         CustReminderChargeAmtLCY[i] := "Reminder Amounts (LCY)";
                         CustFinChargeAmtLCY[i] := "Fin. Charge Memo Amounts (LCY)";
                         CustCrMemoAmountsLCY[i] := "Cr. Memo Amounts (LCY)";
                         CustPaymentsLCY[i] := "Payments (LCY)";
                         CustRefundsLCY[i] := "Refunds (LCY)";
                         CustOtherAmountsLCY[i] := "Other Amounts (LCY)";
                       END;
                       SETRANGE("Date Filter",0D,CurrentDate);

                       UpdateDocStatistics;
                     END;

  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1110032;1;Group     ;
                CaptionML=[ENU=No. of Bills;
                           PTG=N� de T�tulos] }

    { 1110029;2;Field     ;
                CaptionML=[ENU=Open Bills;
                           PTG=T�tulos Pendentes];
                ApplicationArea=#All;
                SourceExpr=NoOpen[1];
                Editable=FALSE }

    { 1110020;2;Field     ;
                CaptionML=[ENU=Open Bills in Bill Gr.;
                           PTG=T�tulos Pendentes da Remessa];
                ApplicationArea=#All;
                SourceExpr=NoOpen[2];
                Editable=FALSE }

    { 1110017;2;Field     ;
                CaptionML=[ENU=Open Bills in Post. Bill Gr.;
                           PTG=T�tulos Pendentes de Remessa Registada];
                ApplicationArea=#All;
                SourceExpr=NoOpen[3];
                Editable=FALSE }

    { 1110008;2;Field     ;
                CaptionML=[ENU=Hon. Bills in Post. Bill Gr.;
                           PTG=T�tulos Pagos de Remessa];
                ApplicationArea=#All;
                SourceExpr=NoHonored[3];
                Editable=FALSE }

    { 1110011;2;Field     ;
                CaptionML=[ENU=Rej. Bills in Post. Bill Gr.;
                           PTG=T�tulos Devolvidos de Remessa];
                ApplicationArea=#All;
                SourceExpr=NoRejected[3];
                Editable=FALSE }

    { 1110012;2;Field     ;
                CaptionML=[ENU=Redr. Bills in Post. Bill Gr.;
                           PTG=T�tulos Reformados de Remessa];
                ApplicationArea=#All;
                SourceExpr=NoRedrawn[3];
                Editable=FALSE }

    { 1110013;2;Field     ;
                CaptionML=[ENU=Hon. Closed Bills;
                           PTG=T�tulos Fechados];
                ApplicationArea=#All;
                SourceExpr=NoHonored[4];
                Editable=FALSE }

    { 1110014;2;Field     ;
                CaptionML=[ENU=Rej. Closed Bills;
                           PTG=T�tulos Fechados Devolvidos];
                ApplicationArea=#All;
                SourceExpr=NoRejected[4];
                Editable=FALSE }

    { 1110034;1;Group     ;
                CaptionML=[ENU=Remaining Amt.  (LCY);
                           PTG=Valor Pendente (DL)] }

    { 1110031;2;Field     ;
                CaptionML=[ENU=Open Bills;
                           PTG=T�tulos Pendentes];
                ApplicationArea=#All;
                SourceExpr=OpenRemainingAmtLCY[1];
                AutoFormatType=1;
                Editable=FALSE;
                OnDrillDown=BEGIN
                              DrillDownOpen(4,1); //Cartera
                            END;
                             }

    { 1110018;2;Field     ;
                CaptionML=[ENU=Open Bills in Bill Gr.;
                           PTG=T�tulos Pendentes de Remessa];
                ApplicationArea=#All;
                SourceExpr=OpenRemainingAmtLCY[2];
                AutoFormatType=1;
                Editable=FALSE;
                OnDrillDown=BEGIN
                              DrillDownOpen(3,1); //Bill Group;
                            END;
                             }

    { 1110015;2;Field     ;
                CaptionML=[ENU=Open Bills in Post. Bill Gr.;
                           PTG=T�tulos Pendentes de Remessa Registada];
                ApplicationArea=#All;
                SourceExpr=OpenRemainingAmtLCY[3];
                AutoFormatType=1;
                Editable=FALSE;
                OnDrillDown=BEGIN
                              DrillDownOpen(1,1); // Posted Bill Group
                            END;
                             }

    { 1110010;2;Field     ;
                CaptionML=[ENU=Hon. Bills in Post. Bill Gr.;
                           PTG=T�tulos Pagos de Remessa];
                ApplicationArea=#All;
                SourceExpr=HonoredRemainingAmtLCY[3];
                AutoFormatType=1;
                Editable=FALSE;
                OnDrillDown=BEGIN
                              DrillDownHonored(1,1); // Posted Bill Group
                            END;
                             }

    { 1110007;2;Field     ;
                CaptionML=[ENU=Rej. Bills in Post. Bill Gr.;
                           PTG=T�tulos Devolvidos e Remessa];
                ApplicationArea=#All;
                SourceExpr=RejectedRemainingAmtLCY[3];
                AutoFormatType=1;
                Editable=FALSE;
                OnDrillDown=BEGIN
                              DrillDownRejected(1,1); // Posted Bill Group
                            END;
                             }

    { 1110005;2;Field     ;
                CaptionML=[ENU=Redr. Bills in Post. Bill Gr.;
                           PTG=T�tulos Reformados de Remessa];
                ApplicationArea=#All;
                SourceExpr=RedrawnRemainingAmtLCY[3];
                AutoFormatType=1;
                Editable=FALSE;
                OnDrillDown=BEGIN
                              DrillDownRedrawn(1,1); // Posted Bill Group
                            END;
                             }

    { 1110001;2;Field     ;
                CaptionML=[ENU=Hon. Closed Bills;
                           PTG=T�tulos Fechados Pagos];
                ApplicationArea=#All;
                SourceExpr=HonoredRemainingAmtLCY[4];
                AutoFormatType=1;
                Editable=FALSE;
                OnDrillDown=BEGIN
                              DrillDownHonored(5,1); //Closed Bills
                            END;
                             }

    { 1110000;2;Field     ;
                CaptionML=[ENU=Rej. Closed Bills;
                           PTG=T�tulos Fechados Devolvidos];
                ApplicationArea=#All;
                SourceExpr=RejectedRemainingAmtLCY[4];
                AutoFormatType=1;
                Editable=FALSE;
                OnDrillDown=BEGIN
                              DrillDownRejected(5,1); //Closed Bills
                            END;
                             }

  }
  CODE
  {
    VAR
      Text000@1000 : TextConst 'ENU=Overdue Amounts (LCY) as of %1;PTG=Valores Vencidos (DL) a %1';
      ValueEntry@1017 : Record 5802;
      DateFilterCalc@1001 : Codeunit 358;
      CustDateFilter@1002 : ARRAY [4] OF Text[30];
      CustDateName@1003 : ARRAY [4] OF Text[30];
      TotalAmountLCY@1004 : Decimal;
      CurrentDate@1005 : Date;
      CustSalesLCY@1006 : ARRAY [4] OF Decimal;
      AdjmtCostLCY@1022 : ARRAY [4] OF Decimal;
      CustProfit@1007 : ARRAY [4] OF Decimal;
      ProfitPct@1008 : ARRAY [4] OF Decimal;
      AdjCustProfit@1024 : ARRAY [4] OF Decimal;
      AdjProfitPct@1023 : ARRAY [4] OF Decimal;
      CustInvDiscAmountLCY@1009 : ARRAY [4] OF Decimal;
      CustPaymentDiscLCY@1010 : ARRAY [4] OF Decimal;
      CustPaymentDiscTolLCY@1019 : ARRAY [4] OF Decimal;
      CustPaymentTolLCY@1018 : ARRAY [4] OF Decimal;
      CustReminderChargeAmtLCY@1011 : ARRAY [4] OF Decimal;
      CustFinChargeAmtLCY@1012 : ARRAY [4] OF Decimal;
      CustCrMemoAmountsLCY@1013 : ARRAY [4] OF Decimal;
      CustPaymentsLCY@1014 : ARRAY [4] OF Decimal;
      CustRefundsLCY@1020 : ARRAY [4] OF Decimal;
      CustOtherAmountsLCY@1021 : ARRAY [4] OF Decimal;
      i@1015 : Integer;
      InvAmountsLCY@1016 : ARRAY [4] OF Decimal;
      Text001@1025 : TextConst 'ENU=Placeholder;PTG=Marcador de posi��o';
      DocumentSituationFilter@1110010 : ARRAY [3] OF ' ,Posted BG/PO,Closed BG/PO,BG/PO,Cartera,Closed Documents';
      NoOpen@1110011 : ARRAY [5] OF Integer;
      NoHonored@1110012 : ARRAY [5] OF Integer;
      NoRejected@1110013 : ARRAY [5] OF Integer;
      NoRedrawn@1110014 : ARRAY [5] OF Integer;
      OpenAmtLCY@1110015 : ARRAY [5] OF Decimal;
      HonoredAmtLCY@1110016 : ARRAY [5] OF Decimal;
      RejectedAmtLCY@1110017 : ARRAY [5] OF Decimal;
      RedrawnAmtLCY@1110018 : ARRAY [5] OF Decimal;
      OpenRemainingAmtLCY@1110019 : ARRAY [5] OF Decimal;
      RejectedRemainingAmtLCY@1110020 : ARRAY [5] OF Decimal;
      HonoredRemainingAmtLCY@1110021 : ARRAY [5] OF Decimal;
      RedrawnRemainingAmtLCY@1110022 : ARRAY [5] OF Decimal;
      j@1110023 : Integer;

    PROCEDURE UpdateDocStatistics@1110100();
    VAR
      CustLedgEntry@1110000 : Record 21;
    BEGIN
      DocumentSituationFilter[1] := DocumentSituationFilter::Cartera;
      DocumentSituationFilter[2] := DocumentSituationFilter::"BG/PO";
      DocumentSituationFilter[3] := DocumentSituationFilter::"Posted BG/PO";

      WITH CustLedgEntry DO BEGIN
        SETCURRENTKEY("Customer No.","Document Type","Document Situation","Document Status");
        SETRANGE("Customer No.",Rec."No.");
        FOR j := 1 TO 5 DO BEGIN
          CASE j OF
            4: // Closed Bill Group and Closed Documents
              BEGIN
                SETRANGE("Document Type","Document Type"::Bill);
                SETFILTER("Document Situation",'%1|%2',
                  "Document Situation"::"Closed BG/PO",
                  "Document Situation"::"Closed Documents");
              END;
            5: // Invoices
              BEGIN
                SETRANGE("Document Type","Document Type"::Invoice);
                SETFILTER("Document Situation",'<>0');
              END;
            ELSE BEGIN
              SETRANGE("Document Type","Document Type"::Bill);
              SETRANGE("Document Situation",DocumentSituationFilter[j]);
            END;
          END;
          SETRANGE("Document Status","Document Status"::Open);
          CALCSUMS("Amount (LCY) stats.","Remaining Amount (LCY) stats.");
          OpenAmtLCY[j] := "Amount (LCY) stats.";
          OpenRemainingAmtLCY[j] := "Remaining Amount (LCY) stats.";
          NoOpen[j] := COUNT;
          SETRANGE("Document Status");

          SETRANGE("Document Status","Document Status"::Honored);
          CALCSUMS("Amount (LCY) stats.","Remaining Amount (LCY) stats.");
          HonoredAmtLCY[j] := "Amount (LCY) stats.";
          HonoredRemainingAmtLCY[j] := "Remaining Amount (LCY) stats.";
          NoHonored[j] := COUNT;
          SETRANGE("Document Status");

          SETRANGE("Document Status","Document Status"::Rejected);
          CALCSUMS("Amount (LCY) stats.","Remaining Amount (LCY) stats.");
          RejectedAmtLCY[j] := "Amount (LCY) stats.";
          RejectedRemainingAmtLCY[j] := "Remaining Amount (LCY) stats.";
          NoRejected[j] := COUNT;
          SETRANGE("Document Status");

          SETRANGE("Document Status","Document Status"::Redrawn);
          CALCSUMS("Amount (LCY) stats.","Remaining Amount (LCY) stats.");
          RedrawnAmtLCY[j] := "Amount (LCY) stats.";
          RedrawnRemainingAmtLCY[j] := "Remaining Amount (LCY) stats.";
          NoRedrawn[j] := COUNT;
          SETRANGE("Document Status");

          SETRANGE("Document Situation");
        END;
      END;
    END;

    PROCEDURE DrillDownOpen@1110101(Situation@1110000 : ' ,Posted BG/PO,Closed BG/PO,BG/PO,Cartera,Closed Documents';DocType@1110001 : 'Invoice,Bill');
    VAR
      CustLedgEntry@1110002 : Record 21;
      CustLedgEntriesPage@1110003 : Page 25;
    BEGIN
      WITH CustLedgEntry DO BEGIN
        SETCURRENTKEY("Customer No.","Document Type","Document Situation","Document Status");
        SETRANGE("Customer No.",Rec."No.");
        CASE Situation OF
          Situation::Cartera:
            SETRANGE("Document Situation","Document Situation"::Cartera);
          Situation::"BG/PO":
            SETRANGE("Document Situation","Document Situation"::"BG/PO");
          Situation::"Posted BG/PO":
            SETRANGE("Document Situation","Document Situation"::"Posted BG/PO");
          Situation::"Closed BG/PO":
            SETFILTER("Document Situation",'%1|%2',
              "Document Situation"::"Closed BG/PO",
              "Document Situation"::"Closed Documents");
          ELSE
            SETFILTER("Document Situation",'<>0');
        END;
        CASE DocType OF
          DocType::Invoice:
            SETRANGE("Document Type","Document Type"::Invoice);
          DocType::Bill:
            SETRANGE("Document Type","Document Type"::Bill);
        END;

        SETRANGE("Document Status","Document Status"::Open);
        CustLedgEntriesPage.SETTABLEVIEW(CustLedgEntry);
        CustLedgEntriesPage.SETRECORD(CustLedgEntry);
        CustLedgEntriesPage.RUNMODAL;
        SETRANGE("Document Status");
        SETRANGE("Document Situation");
      END;
    END;

    PROCEDURE DrillDownHonored@1110102(Situation@1110000 : ' ,Posted BG/PO,Closed BG/PO,BG/PO,Cartera,Closed Documents';DocType@1110001 : 'Invoice,Bill');
    VAR
      CustLedgEntry@1110002 : Record 21;
      CustLedgEntriesPage@1110003 : Page 25;
    BEGIN
      WITH CustLedgEntry DO BEGIN
        SETCURRENTKEY("Customer No.","Document Type","Document Situation","Document Status");
        SETRANGE("Customer No.",Rec."No.");
        CASE Situation OF
          Situation::Cartera:
            SETRANGE("Document Situation","Document Situation"::Cartera);
          Situation::"BG/PO":
            SETRANGE("Document Situation","Document Situation"::"BG/PO");
          Situation::"Posted BG/PO":
            SETRANGE("Document Situation","Document Situation"::"Posted BG/PO");
          Situation::"Closed BG/PO":
            SETFILTER("Document Situation",'%1|%2',
              "Document Situation"::"Closed BG/PO",
              "Document Situation"::"Closed Documents");
          ELSE
            SETFILTER("Document Situation",'<>0');
        END;
        CASE DocType OF
          DocType::Invoice:
            SETRANGE("Document Type","Document Type"::Invoice);
          DocType::Bill:
            SETRANGE("Document Type","Document Type"::Bill);
        END;

        SETRANGE("Document Status","Document Status"::Honored);
        CustLedgEntriesPage.SETTABLEVIEW(CustLedgEntry);
        CustLedgEntriesPage.SETRECORD(CustLedgEntry);
        CustLedgEntriesPage.RUNMODAL;
        SETRANGE("Document Status");
        SETRANGE("Document Situation");
      END;
    END;

    PROCEDURE DrillDownRejected@1110103(Situation@1110000 : ' ,Posted BG/PO,Closed BG/PO,BG/PO,Cartera,Closed Documents';DocType@1110001 : 'Invoice,Bill');
    VAR
      CustLedgEntry@1110002 : Record 21;
      CustLedgEntriesPage@1110003 : Page 25;
    BEGIN
      WITH CustLedgEntry DO BEGIN
        SETCURRENTKEY("Customer No.","Document Type","Document Situation","Document Status");
        SETRANGE("Customer No.",Rec."No.");
        CASE Situation OF
          Situation::Cartera:
            SETRANGE("Document Situation","Document Situation"::Cartera);
          Situation::"BG/PO":
            SETRANGE("Document Situation","Document Situation"::"BG/PO");
          Situation::"Posted BG/PO":
            SETRANGE("Document Situation","Document Situation"::"Posted BG/PO");
          Situation::"Closed BG/PO":
            SETFILTER("Document Situation",'%1|%2',
              "Document Situation"::"Closed BG/PO",
              "Document Situation"::"Closed Documents");
          ELSE
            SETFILTER("Document Situation",'<>0');
        END;
        CASE DocType OF
          DocType::Invoice:
            SETRANGE("Document Type","Document Type"::Invoice);
          DocType::Bill:
            SETRANGE("Document Type","Document Type"::Bill);
        END;

        SETRANGE("Document Status","Document Status"::Rejected);
        CustLedgEntriesPage.SETTABLEVIEW(CustLedgEntry);
        CustLedgEntriesPage.SETRECORD(CustLedgEntry);
        CustLedgEntriesPage.RUNMODAL;
        SETRANGE("Document Status");
        SETRANGE("Document Situation");
      END;
    END;

    PROCEDURE DrillDownRedrawn@1110104(Situation@1110000 : ' ,Posted BG/PO,Closed BG/PO,BG/PO,Cartera,Closed Documents';DocType@1110001 : 'Invoice,Bill');
    VAR
      CustLedgEntry@1110002 : Record 21;
      CustLedgEntriesPage@1110003 : Page 25;
    BEGIN
      WITH CustLedgEntry DO BEGIN
        SETCURRENTKEY("Customer No.","Document Type","Document Situation","Document Status");
        SETRANGE("Customer No.",Rec."No.");
        CASE Situation OF
          Situation::Cartera:
            SETRANGE("Document Situation","Document Situation"::Cartera);
          Situation::"BG/PO":
            SETRANGE("Document Situation","Document Situation"::"BG/PO");
          Situation::"Posted BG/PO":
            SETRANGE("Document Situation","Document Situation"::"Posted BG/PO");
          Situation::"Closed BG/PO",Situation::"Closed Documents":
            SETFILTER("Document Situation",'%1|%2',
              "Document Situation"::"Closed BG/PO",
              "Document Situation"::"Closed Documents");
          ELSE
            SETFILTER("Document Situation",'<>0');
        END;
        CASE DocType OF
          DocType::Invoice:
            SETRANGE("Document Type","Document Type"::Invoice);
          DocType::Bill:
            SETRANGE("Document Type","Document Type"::Bill);
        END;
        SETRANGE("Document Status","Document Status"::Redrawn);
        CustLedgEntriesPage.SETTABLEVIEW(CustLedgEntry);
        CustLedgEntriesPage.SETRECORD(CustLedgEntry);
        CustLedgEntriesPage.RUNMODAL;
        SETRANGE("Document Status");
        SETRANGE("Document Situation");
      END;
    END;

    BEGIN
    END.
  }
}

