OBJECT Codeunit 31022903 BG/PO-Post and Print
{
  OBJECT-PROPERTIES
  {
    Date=01/02/15;
    Time=13:00:00;
    Version List=NAVPTSS81.00;
  }
  PROPERTIES
  {
    Permissions=TableData 31022938=rm,
                TableData 31022939=rm,
                TableData 31022940=rm,
                TableData 31022952=rm,
                TableData 31022953=rm,
                TableData 31022954=rm;
    OnRun=BEGIN
          END;

  }
  CODE
  {
    VAR
      Text31022890@1000 : TextConst 'ENU=This Bill Group has not been printed. Do you want to continue?;PTG=A Remessa n�o foi impressa. Confirma que deseja continuar?';
      Text31022891@1001 : TextConst 'ENU=The posting process has been cancelled by the user.;PTG=O processo de registo foi cancelado pelo utilizador.';
      Text31022892@1002 : TextConst 'ENU=Do you want to post the Bill Group?;PTG=Confirma que deseja registar a Remessa?';
      Text31022893@1003 : TextConst 'ENU=This Payment Order has not been printed. Do you want to continue?;PTG=A Ordem Pagto. n�o foi impressa. Confirma que deseja continuar?';
      Text31022894@1004 : TextConst 'ENU=Do you want to post the Payment Order?;PTG=Confirma que deseja registar a Ordem de Pagamento?';
      CarteraReportSelection@1110000 : Record 31022946;

    PROCEDURE ReceivablePostOnly@2(BillGr@1110000 : Record 31022938);
    BEGIN
      IF BillGr."No. Printed" = 0  THEN
        IF NOT CONFIRM(Text31022890) THEN
          ERROR(Text31022891)
      ELSE
        IF NOT CONFIRM(Text31022892,FALSE) THEN
          ERROR(Text31022891);

      BillGr.SETRECFILTER;
      REPORT.RUNMODAL(REPORT::"Post Bill Group",
        (BillGr."Dealing Type" = BillGr."Dealing Type"::Discount),
        FALSE,BillGr);
    END;

    PROCEDURE ReceivablePostAndPrint@3(BillGr@1110000 : Record 31022938);
    VAR
      PostedBillGr@1110001 : Record 31022939;
    BEGIN
      BillGr.SETRECFILTER;
      REPORT.RUNMODAL(REPORT::"Post Bill Group",
        (BillGr."Dealing Type" = BillGr."Dealing Type"::Discount),
        FALSE,BillGr);

      COMMIT;

      IF PostedBillGr.GET(BillGr."No.") THEN BEGIN
        PostedBillGr.SETRECFILTER;
        CarteraReportSelection.RESET;
        CarteraReportSelection.SETRANGE(Usage,CarteraReportSelection.Usage::"Posted Bill Group");
        CarteraReportSelection.FINDSET;
        REPEAT
          CarteraReportSelection.TESTFIELD("Report ID");
          REPORT.RUN(CarteraReportSelection."Report ID",FALSE,FALSE,PostedBillGr);
        UNTIL CarteraReportSelection.NEXT = 0;
      END;
    END;

    PROCEDURE PayablePostOnly@4(PmtOrd@1110000 : Record 31022952);
    BEGIN
      IF PmtOrd."No. Printed" = 0  THEN
        IF NOT CONFIRM(Text31022893) THEN
          ERROR(Text31022891)
      ELSE
        IF NOT CONFIRM(Text31022894,FALSE) THEN
          ERROR(Text31022891);

      PmtOrd.SETRECFILTER;
      REPORT.RUNMODAL(REPORT::"Post Payment Order",FALSE,FALSE,PmtOrd);
    END;

    PROCEDURE PayablePostAndPrint@1(PmtOrd@1110000 : Record 31022952);
    VAR
      PostedPmtOrd@1110001 : Record 31022953;
    BEGIN
      PmtOrd.SETRECFILTER;
      REPORT.RUNMODAL(REPORT::"Post Payment Order",FALSE,FALSE,PmtOrd);

      COMMIT;

      IF PostedPmtOrd.GET(PmtOrd."No.") THEN BEGIN
        PostedPmtOrd.SETRECFILTER;
        CarteraReportSelection.RESET;
        CarteraReportSelection.SETRANGE(Usage,CarteraReportSelection.Usage::"Posted Payment Order");
        CarteraReportSelection.FINDSET;
        REPEAT
          CarteraReportSelection.TESTFIELD("Report ID");
          REPORT.RUN(CarteraReportSelection."Report ID",FALSE,FALSE,PostedPmtOrd);
        UNTIL CarteraReportSelection.NEXT = 0;
      END;
    END;

    PROCEDURE PrintCounter@1170000000(Table@1170000004 : Integer;Number@1170000005 : Code[20]);
    VAR
      PostedBillGr@1170000003 : Record 31022939;
      ClosedBillGr@1170000002 : Record 31022940;
      PostedPaymentOrder@1170000001 : Record 31022953;
      ClosedPaymentOrder@1170000000 : Record 31022954;
      BillGr@1000 : Record 31022938;
      PaymentOrder@1001 : Record 31022952;
    BEGIN
      CASE TRUE OF
        Table = DATABASE::"Bill Group":
          BEGIN
            BillGr.GET(Number);
            BillGr."No. Printed" := BillGr."No. Printed" + 1;
            BillGr.MODIFY;
          END;
        Table = DATABASE::"Payment Order":
          BEGIN
            PaymentOrder.GET(Number);
            PaymentOrder."No. Printed" := PaymentOrder."No. Printed" + 1;
            PaymentOrder.MODIFY;
          END;
        Table = DATABASE::"Posted Bill Group":
          BEGIN
            PostedBillGr.GET(Number);
            PostedBillGr."No. Printed" := PostedBillGr."No. Printed" + 1;
            PostedBillGr.MODIFY;
          END;
        Table = DATABASE::"Closed Bill Group":
          BEGIN
            ClosedBillGr.GET(Number);
            ClosedBillGr."No. Printed" := ClosedBillGr."No. Printed" + 1;
            ClosedBillGr.MODIFY;
          END;
        Table = DATABASE::"Posted Payment Order":
          BEGIN
            PostedPaymentOrder.GET(Number);
            PostedPaymentOrder."No. Printed" := PostedPaymentOrder."No. Printed" + 1;
            PostedPaymentOrder.MODIFY;
          END;
        Table = DATABASE::"Closed Payment Order":
          BEGIN
            ClosedPaymentOrder.GET(Number);
            ClosedPaymentOrder."No. Printed" := ClosedPaymentOrder."No. Printed" + 1;
            ClosedPaymentOrder.MODIFY;
          END;
        END;
      COMMIT;
    END;

    BEGIN
    END.
  }
}

