OBJECT Table 2000000169 NAV App Tenant Add-In
{
  OBJECT-PROPERTIES
  {
    Date=13/04/17;
    Time=14:36:28;
    Version List=NAVW110.0;
  }
  PROPERTIES
  {
    DataPerCompany=No;
    CaptionML=[ENU=NAV App Tenant Add-In;
               PTG=Add-in NAV App Tenant];
  }
  FIELDS
  {
    { 1   ;   ;App ID              ;GUID          ;CaptionML=[ENU=App ID;
                                                              PTG=ID App] }
    { 2   ;   ;Add-In Name         ;Text220       ;CaptionML=[ENU=Add-In Name;
                                                              PTG=Nome Add-In] }
    { 3   ;   ;Public Key Token    ;Text20        ;CaptionML=[ENU=Public Key Token;
                                                              PTG=Public Key Token] }
    { 4   ;   ;Version             ;Text25        ;CaptionML=[ENU=Version;
                                                              PTG=Vers�o] }
    { 5   ;   ;Category            ;Option        ;CaptionML=[ENU=Category;
                                                              PTG=Categoria];
                                                   OptionCaptionML=[ENU=JavaScript Control Add-in,DotNet Control Add-in,DotNet Interoperability,Language Resource;
                                                                    PTG=Add-in Controlo JavaScript,Add-in Controlo DotNet,DotNet Interoperability,Recurso L�ngua];
                                                   OptionString=JavaScript Control Add-in,DotNet Control Add-in,DotNet Interoperability,Language Resource }
    { 6   ;   ;Description         ;Text250       ;CaptionML=[ENU=Description;
                                                              PTG=Descri��o] }
    { 7   ;   ;Resource            ;BLOB          ;CaptionML=[ENU=Resource;
                                                              PTG=Recurso] }
  }
  KEYS
  {
    {    ;App ID,Add-In Name,Public Key Token,Version;
                                                   Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    END.
  }
}

