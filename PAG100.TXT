OBJECT Page 100 Accounting Periods
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Accounting Periods;
               PTG=Per�odos Contabil�sticos];
    SourceTable=Table50;
    PageType=List;
    RefreshOnActivate=Yes;
    ActionList=ACTIONS
    {
      { 1900000004;0 ;ActionContainer;
                      ActionContainerType=ActionItems }
      { 17      ;1   ;Action    ;
                      CaptionML=[ENU=&Inventory Period;
                                 PTG=Per�odo &Invent�rio];
                      ToolTipML=ENU=Create an inventory period. An inventory period defines a period of time in which you can post changes to the inventory value.;
                      ApplicationArea=#Basic,#Suite;
                      RunObject=Page 5828;
                      Promoted=Yes;
                      Image=ShowInventoryPeriods;
                      PromotedCategory=Process }
      { 14      ;1   ;Action    ;
                      Ellipsis=Yes;
                      CaptionML=[ENU=&Create Year;
                                 PTG=&Criar Exerc�cio];
                      ToolTipML=ENU=Open a new fiscal year and define its accounting periods so you can start posting documents.;
                      ApplicationArea=#Basic,#Suite;
                      RunObject=Report 93;
                      Promoted=Yes;
                      Image=CreateYear;
                      PromotedCategory=Process }
      { 15      ;1   ;Action    ;
                      CaptionML=[ENU=C&lose Year;
                                 PTG=Fechar E&xerc�cio];
                      ToolTipML=ENU=Close the current fiscal year. A confirmation message will display that tells you which year will be closed. You cannot reopen the year after it has been closed.;
                      ApplicationArea=#Basic,#Suite;
                      RunObject=Codeunit 6;
                      Promoted=Yes;
                      Image=CloseYear;
                      PromotedCategory=Process }
      { 1900000006;0 ;ActionContainer;
                      ActionContainerType=Reports }
      { 1902174606;1 ;Action    ;
                      CaptionML=[ENU=Trial Balance by Period;
                                 PTG=Balancete por Per�odo];
                      ToolTipML=ENU=Show the opening balance by general ledger account, the movements in the selected period of month, quarter, or year, and the resulting closing balance.;
                      ApplicationArea=#Basic,#Suite;
                      RunObject=Report 38;
                      Promoted=Yes;
                      Image=Report;
                      PromotedCategory=Report }
      { 1904082706;1 ;Action    ;
                      CaptionML=[ENU=Trial Balance;
                                 PTG=Balancete];
                      ToolTipML=ENU=Show the chart of accounts with balances and net changes. You can use the report at the close of an accounting period or fiscal year.;
                      ApplicationArea=#Suite;
                      RunObject=Report 6;
                      Promoted=No;
                      Image=Report;
                      PromotedCategory=Report }
      { 1905089406;1 ;Action    ;
                      CaptionML=[ENU=Fiscal Year Balance;
                                 PTG=Saldo Exerc�cio];
                      ToolTipML=ENU=View balance sheet movements for a selected period. The report is useful at the close of an accounting period or fiscal year.;
                      ApplicationArea=#Basic,#Suite;
                      RunObject=Report 36;
                      Promoted=No;
                      Image=Report;
                      PromotedCategory=Report }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 2   ;2   ;Field     ;
                ToolTipML=ENU=Specifies the date that the accounting period will begin.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Starting Date" }

    { 4   ;2   ;Field     ;
                ToolTipML=ENU=Specifies the name of the accounting period.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr=Name }

    { 6   ;2   ;Field     ;
                ToolTipML=ENU=Specifies whether to use the accounting period to start a fiscal year.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="New Fiscal Year" }

    { 8   ;2   ;Field     ;
                ToolTipML=ENU=Specifies if the accounting period belongs to a closed fiscal year.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr=Closed }

    { 10  ;2   ;Field     ;
                ToolTipML=ENU=Specifies if you can change the starting date for the accounting period.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Date Locked" }

    { 22  ;2   ;Field     ;
                CaptionML=[ENU=Inventory Period Closed;
                           PTG=Per�odo Invent�rio Fechado];
                ToolTipML=ENU=Specifies that the inventory period has been closed.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr=InvtPeriod.IsInvtPeriodClosed("Starting Date");
                Editable=FALSE }

    { 20  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the period type that was used in the accounting period to calculate the average cost.;
                SourceExpr="Average Cost Period";
                Visible=FALSE }

    { 18  ;2   ;Field     ;
                ToolTipML=ENU=Specifies how the average cost for items in the accounting period was calculated.;
                SourceExpr="Average Cost Calc. Type";
                Visible=FALSE }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {
    VAR
      InvtPeriod@1000 : Record 5814;

    BEGIN
    END.
  }
}

