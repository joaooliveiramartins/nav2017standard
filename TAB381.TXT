OBJECT Table 381 VAT Registration No. Format
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0,NAVPTSS91.00;
  }
  PROPERTIES
  {
    CaptionML=[ENU=VAT Registration No. Format;
               PTG=Format N� Contribuinte];
  }
  FIELDS
  {
    { 1   ;   ;Country/Region Code ;Code10        ;TableRelation=Country/Region;
                                                   CaptionML=[ENU=Country/Region Code;
                                                              PTG=C�d. Pa�s/Regi�o];
                                                   NotBlank=Yes;
                                                   Editable=No }
    { 2   ;   ;Line No.            ;Integer       ;CaptionML=[ENU=Line No.;
                                                              PTG=N� Linha] }
    { 3   ;   ;Format              ;Text20        ;CaptionML=[ENU=Format;
                                                              PTG=Formato] }
    { 31022890;;Check PT Format    ;Boolean       ;CaptionML=[ENU=Check PT Format;
                                                              PTG=Valida Formato PT];
                                                   Description=soft }
  }
  KEYS
  {
    {    ;Country/Region Code,Line No.            ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {
    VAR
      Text000@1000 : TextConst 'ENU=The entered VAT Registration number is not in agreement with the format specified for Country/Region Code %1.\;PTG=O N� Contribuinte introduzido n�o est� de acordo com o formato especificado para o C�d. Pa�s/Regi�o %1.\';
      Text001@1001 : TextConst 'ENU=The following formats are acceptable: %2;PTG=Os seguintes formatos s�o aceites: %2';
      Text002@1002 : TextConst 'ENU=This VAT registration number has already been entered for the following customers:\ %1;PTG=Este N� Contribuinte j� foi introduzido para os seguintes clientes:\ %1';
      Text003@1003 : TextConst 'ENU=This VAT registration number has already been entered for the following vendors:\ %1;PTG=Este N� Contribuinte j� foi introduzido para os seguintes fornecedores:\ %1';
      Text004@1004 : TextConst 'ENU=This VAT registration number has already been entered for the following contacts:\ %1;PTG=Este N� Contribuinte j� foi introduzido para os seguintes contactos:\ %1';
      Text005@1005 : TextConst 'ENU=ABCDEFGHIJKLMNOPQRSTUVWXYZ;PTG=ABCDEFGHIJKLMNOPQRSTUVWXYZ';

    PROCEDURE Test@1(VATRegNo@1000 : Text[20];CountryCode@1001 : Code[10];Number@1002 : Code[20];TableID@1003 : Option) : Boolean;
    VAR
      CompanyInfo@1005 : Record 79;
      Check@1004 : Boolean;
      t@1006 : Text[250];
    BEGIN
      IF VATRegNo = '' THEN
        EXIT;
      Check := TRUE;

      IF CountryCode = '' THEN BEGIN
        IF NOT CompanyInfo.GET THEN
          EXIT;
        SETRANGE("Country/Region Code",CompanyInfo."Country/Region Code");
      END ELSE
        SETRANGE("Country/Region Code",CountryCode);
      SETFILTER(Format,'<> %1','');
      IF FIND('-') THEN
        REPEAT
          IF t = '' THEN
            t := Format
          ELSE
            IF STRLEN(t) + STRLEN(Format) + 5 <= MAXSTRLEN(t) THEN
              t := t + ', ' + Format
            ELSE
              t := t + '...';
          Check := Compare(VATRegNo,Format);
        UNTIL Check OR (NEXT = 0)
      //V91.00#00019,sn
      ELSE
       IF Format <> '' THEN  //V91.00#00025,n
        Check := CheckPT(VATRegNo);
      //V91.00#00019,en
      IF NOT Check THEN
        ERROR(Text000 + Text001,"Country/Region Code",t);

      CASE TableID OF
        DATABASE::Customer:
          CheckCust(VATRegNo,Number);
        DATABASE::Vendor:
          CheckVendor(VATRegNo,Number);
        DATABASE::Contact:
          CheckContact(VATRegNo,Number);
      END;
      EXIT(TRUE);
    END;

    LOCAL PROCEDURE CheckCust@3(VATRegNo@1000 : Text[20];Number@1002 : Code[20]);
    VAR
      Cust@1003 : Record 18;
      Check@1004 : Boolean;
      Finish@1001 : Boolean;
      t@1005 : Text[250];
    BEGIN
      Check := TRUE;
      t := '';
      Cust.SETCURRENTKEY("VAT Registration No.");
      Cust.SETRANGE("VAT Registration No.",VATRegNo);
      Cust.SETFILTER("No.",'<>%1',Number);
      IF Cust.FIND('-') THEN BEGIN
        Check := FALSE;
        Finish := FALSE;
        REPEAT
          IF Cust."No." <> Number THEN
            IF t = '' THEN
              t := Cust."No."
            ELSE
              IF STRLEN(t) + STRLEN(Cust."No.") + 5 <= MAXSTRLEN(t) THEN
                t := t + ', ' + Cust."No."
              ELSE BEGIN
                t := t + '...';
                Finish := TRUE;
              END;
        UNTIL (Cust.NEXT = 0) OR Finish;
      END;
      IF Check = FALSE THEN
        MESSAGE(Text002,t);
    END;

    LOCAL PROCEDURE CheckVendor@4(VATRegNo@1000 : Text[20];Number@1002 : Code[20]);
    VAR
      Vend@1003 : Record 23;
      Check@1004 : Boolean;
      Finish@1001 : Boolean;
      t@1005 : Text[250];
    BEGIN
      Check := TRUE;
      t := '';
      Vend.SETCURRENTKEY("VAT Registration No.");
      Vend.SETRANGE("VAT Registration No.",VATRegNo);
      Vend.SETFILTER("No.",'<>%1',Number);
      IF Vend.FIND('-') THEN BEGIN
        Check := FALSE;
        Finish := FALSE;
        REPEAT
          IF Vend."No." <> Number THEN
            IF t = '' THEN
              t := Vend."No."
            ELSE
              IF STRLEN(t) + STRLEN(Vend."No.") + 5 <= MAXSTRLEN(t) THEN
                t := t + ', ' + Vend."No."
              ELSE BEGIN
                t := t + '...';
                Finish := TRUE;
              END;
        UNTIL (Vend.NEXT = 0) OR Finish;
      END;
      IF Check = FALSE THEN
        MESSAGE(Text003,t);
    END;

    LOCAL PROCEDURE CheckContact@5(VATRegNo@1000 : Text[20];Number@1002 : Code[20]);
    VAR
      Cont@1003 : Record 5050;
      Check@1004 : Boolean;
      Finish@1001 : Boolean;
      t@1005 : Text[250];
    BEGIN
      Check := TRUE;
      t := '';
      Cont.SETCURRENTKEY("VAT Registration No.");
      Cont.SETRANGE("VAT Registration No.",VATRegNo);
      Cont.SETFILTER("No.",'<>%1',Number);
      IF Cont.FIND('-') THEN BEGIN
        Check := FALSE;
        Finish := FALSE;
        REPEAT
          IF Cont."No." <> Number THEN
            IF t = '' THEN
              t := Cont."No."
            ELSE
              IF STRLEN(t) + STRLEN(Cont."No.") + 5 <= MAXSTRLEN(t) THEN
                t := t + ', ' + Cont."No."
              ELSE BEGIN
                t := t + '...';
                Finish := TRUE;
              END;
        UNTIL (Cont.NEXT = 0) OR Finish;
      END;
      IF Check = FALSE THEN
        MESSAGE(Text004,t);
    END;

    PROCEDURE Compare@2(VATRegNo@1000 : Text[20];Format@1001 : Text[20]) : Boolean;
    VAR
      i@1002 : Integer;
      Cf@1003 : Text[1];
      Ce@1004 : Text[1];
      Check@1005 : Boolean;
    BEGIN
      //soft,o Check := TRUE;
      //soft,sn
      IF "Check PT Format" THEN
         EXIT(CheckPT(VATRegNo))
      ELSE
      //soft,en
      //V91.00#00025,sn
      BEGIN
      Check := TRUE;
      //V91.00#00025,en
      IF STRLEN(VATRegNo) = STRLEN(Format) THEN
        FOR i := 1 TO STRLEN(VATRegNo) DO BEGIN
          Cf := COPYSTR(Format,i,1);
          Ce := COPYSTR(VATRegNo,i,1);
          CASE Cf OF
            '#':
              IF NOT ((Ce >= '0') AND (Ce <= '9')) THEN
                Check := FALSE;
            '@':
              IF STRPOS(Text005,UPPERCASE(Ce)) = 0 THEN
                Check := FALSE;
            ELSE
              IF NOT ((Cf = Ce) OR (Cf = '?')) THEN
                Check := FALSE
          END;
        END
      ELSE
        Check := FALSE;
      END; //V91.00#00025,n
      EXIT(Check);
    END;

    LOCAL PROCEDURE CheckPT@6(VatRegNo@1000 : Text[20]) : Boolean;
    VAR
      VatNumber@1001 : Integer;
      Count@1002 : Integer;
      CheckDigit@1003 : Integer;
      Value@1004 : Integer;
    BEGIN
      //soft,sn
      EVALUATE(VatNumber,VatRegNo);
      IF VatNumber = 999999999 THEN
        EXIT (TRUE);
      IF (VatNumber < 10000000) OR (VatNumber > 999999999) THEN
        EXIT(FALSE);
      IF NOT((COPYSTR(VatRegNo,1,1)='1') OR
             (COPYSTR(VatRegNo,1,1)='2') OR
             (COPYSTR(VatRegNo,1,1)='5') OR
             (COPYSTR(VatRegNo,1,1)='6') OR
             (COPYSTR(VatRegNo,1,1)='7') OR
             (COPYSTR(VatRegNo,1,1)='9')) THEN
        EXIT(FALSE);

      FOR Count := 1 TO 8 DO BEGIN
        EVALUATE(Value,COPYSTR(VatRegNo,Count,1));
        CheckDigit := CheckDigit + (Value * (10 - Count));
      END;
      CheckDigit := CheckDigit - (ROUND(CheckDigit / 11,1,'<') * 11);
      IF (CheckDigit = 0) OR (CheckDigit = 1) THEN
        CheckDigit := 0
      ELSE
        CheckDigit := 11 - CheckDigit;
      EVALUATE(Value,COPYSTR(VatRegNo, 9, 1));

      IF CheckDigit = Value THEN
        EXIT(TRUE)
      ELSE
        EXIT(FALSE);
      //soft,en
    END;

    BEGIN
    {
      V91.00#00019 - Valida��o N� Contribuinte no Cliente - IRC - 2015.12.17
      V91.00#00025 - Formato de N� Contribuinte Estrangeiro - IRC - 2016.01.19
    }
    END.
  }
}

