OBJECT Table 5199 Attendee
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0;
  }
  PROPERTIES
  {
    OnInsert=VAR
               Todo2@1005 : Record 5080;
             BEGIN
               ValidateAttendee(Rec,Attendee);
               Todo2.GET("To-do No.");
               Todo2.CreateSubTodo(Rec,Todo2);
             END;

    OnModify=VAR
               Todo@1000 : Record 5080;
             BEGIN
               ValidateAttendee(Rec,Attendee);
               IF xRec."Attendance Type" = "Attendance Type"::"To-do Organizer" THEN BEGIN
                 IF "Attendance Type" <> xRec."Attendance Type" THEN
                   ERROR(Text006,FIELDCAPTION("Attendance Type"));
                 IF "Attendee No." <> xRec."Attendee No." THEN
                   ERROR(Text008);
               END ELSE
                 IF "Attendee No." <> xRec."Attendee No." THEN BEGIN
                   Todo.DeleteAttendeeTodo(xRec);
                   Todo.GET("To-do No.");
                   Todo.CreateSubTodo(Rec,Todo);
                 END ELSE
                   IF (xRec."Invitation Response Type" <> "Invitation Response Type") OR
                      (xRec."Invitation Sent" <> "Invitation Sent")
                   THEN
                     EXIT;
             END;

    OnDelete=BEGIN
               IF "Attendance Type" = "Attendance Type"::"To-do Organizer" THEN
                 ERROR(Text005);
               Todo.DeleteAttendeeTodo(Rec);

               Todo.GET("To-do No.");
             END;

    CaptionML=[ENU=Attendee;
               PTG=Participante];
  }
  FIELDS
  {
    { 1   ;   ;To-do No.           ;Code20        ;TableRelation=To-do;
                                                   CaptionML=[ENU=To-do No.;
                                                              PTG=N� A��o Efetuar] }
    { 2   ;   ;Line No.            ;Integer       ;CaptionML=[ENU=Line No.;
                                                              PTG=N� Linha] }
    { 3   ;   ;Attendance Type     ;Option        ;OnValidate=VAR
                                                                Cont@1001 : Record 5050;
                                                                Salesperson@1000 : Record 13;
                                                              BEGIN
                                                                IF "Attendance Type" = "Attendance Type"::"To-do Organizer" THEN
                                                                  "Send Invitation" := TRUE
                                                                ELSE BEGIN
                                                                  IF "Attendee Type" = "Attendee Type"::Contact THEN BEGIN
                                                                    IF Cont.GET("Attendee No.") THEN
                                                                      "Send Invitation" := Cont."E-Mail" <> '';
                                                                  END ELSE
                                                                    IF Salesperson.GET("Attendee No.") THEN
                                                                      "Send Invitation" := Salesperson."E-Mail" <> '';
                                                                END
                                                              END;

                                                   CaptionML=[ENU=Attendance Type;
                                                              PTG=Tipo Participa��o];
                                                   OptionCaptionML=[ENU=Required,Optional,To-do Organizer;
                                                                    PTG=Necess�rio,Opcional,Organizador A��o Efetuar];
                                                   OptionString=Required,Optional,To-do Organizer }
    { 4   ;   ;Attendee Type       ;Option        ;OnValidate=BEGIN
                                                                IF "Attendee Type" <> xRec."Attendee Type" THEN BEGIN
                                                                  "Attendee No." := '';
                                                                  "Attendee Name" := '';
                                                                  "Send Invitation" := FALSE;
                                                                  IF "Attendance Type" = "Attendance Type"::"To-do Organizer" THEN
                                                                    "Send Invitation" := TRUE;
                                                                END;
                                                              END;

                                                   CaptionML=[ENU=Attendee Type;
                                                              PTG=Tipo Participante];
                                                   OptionCaptionML=[ENU=Contact,Salesperson;
                                                                    PTG=Contacto,Vendedor];
                                                   OptionString=Contact,Salesperson }
    { 5   ;   ;Attendee No.        ;Code20        ;TableRelation=IF (Attendee Type=CONST(Contact)) Contact WHERE (No.=FIELD(Attendee No.))
                                                                 ELSE IF (Attendee Type=CONST(Salesperson)) Salesperson/Purchaser WHERE (Code=FIELD(Attendee No.));
                                                   OnValidate=VAR
                                                                Cont@1000 : Record 5050;
                                                                Salesperson@1001 : Record 13;
                                                              BEGIN
                                                                TESTFIELD("Attendee No.");
                                                                IF "Attendee Type" = "Attendee Type"::Contact THEN BEGIN
                                                                  Cont.GET("Attendee No.");
                                                                  "Attendee Name" := Cont.Name;
                                                                  IF CurrFieldNo <> 0 THEN
                                                                    "Send Invitation" := Cont."E-Mail" <> '';
                                                                END ELSE BEGIN
                                                                  Salesperson.GET("Attendee No.");
                                                                  "Attendee Name" := Salesperson.Name;
                                                                  IF CurrFieldNo <> 0 THEN
                                                                    IF "Attendance Type" <> "Attendance Type"::"To-do Organizer" THEN
                                                                      "Send Invitation" := Salesperson."E-Mail" <> '';
                                                                END;
                                                              END;

                                                   CaptionML=[ENU=Attendee No.;
                                                              PTG=N� Participante] }
    { 6   ;   ;Attendee Name       ;Text50        ;CaptionML=[ENU=Attendee Name;
                                                              PTG=Nome Participante];
                                                   Editable=No }
    { 7   ;   ;Send Invitation     ;Boolean       ;OnValidate=VAR
                                                                Cont@1000 : Record 5050;
                                                                Salesperson@1001 : Record 13;
                                                                ToDo@1002 : Record 5080;
                                                              BEGIN
                                                                IF NOT "Send Invitation" AND
                                                                   ("Attendance Type" = "Attendance Type"::"To-do Organizer")
                                                                THEN
                                                                  ERROR(Text007,FIELDCAPTION("Send Invitation"));

                                                                ToDo.INIT;
                                                                IF ToDo.GET("To-do No.") THEN;
                                                                IF "Send Invitation" AND (ToDo.Type <> ToDo.Type::"Phone Call") THEN
                                                                  IF "Attendee Type" = "Attendee Type"::Salesperson THEN BEGIN
                                                                    IF Salesperson.GET("Attendee No.") AND (Salesperson."E-Mail" = '') THEN
                                                                      ERROR(Text004,FIELDCAPTION("Send Invitation"),Salesperson.Name);
                                                                  END ELSE
                                                                    IF Cont.GET("Attendee No.") AND (Cont."E-Mail" = '') THEN
                                                                      ERROR(Text004,FIELDCAPTION("Send Invitation"),Cont.Name);
                                                              END;

                                                   CaptionML=[ENU=Send Invitation;
                                                              PTG=Enviar Convite] }
    { 8   ;   ;Invitation Response Type;Option    ;CaptionML=[ENU=Invitation Response Type;
                                                              PTG=Tipo Resposta Convite];
                                                   OptionCaptionML=[ENU=None,Accepted,Declined,Tentative;
                                                                    PTG=Nenhuma,Aceite,Recusada,Tentativa];
                                                   OptionString=None,Accepted,Declined,Tentative }
    { 9   ;   ;Invitation Sent     ;Boolean       ;CaptionML=[ENU=Invitation Sent;
                                                              PTG=Convite Enviado] }
  }
  KEYS
  {
    {    ;To-do No.,Line No.                      ;Clustered=Yes }
    {    ;To-do No.,Attendee Type,Attendee No.     }
    {    ;To-do No.,Attendance Type                }
  }
  FIELDGROUPS
  {
  }
  CODE
  {
    VAR
      Text001@1002 : TextConst 'ENU=A to-do organizer must always be a salesperson.;PTG=Um organizador de a��o a efetuar tem de ser sempre um vendedor.';
      Text002@1001 : TextConst 'ENU=You cannot have more than one to-do organizer.;PTG=N�o pode ter mais de um organizador de a��o a efetuar.';
      Text003@1000 : TextConst 'ENU=This attendee already exists.;PTG=Este participante j� existe.';
      Attendee@1003 : Record 5199;
      Todo@1004 : Record 5080;
      Text004@1005 : TextConst '@@@="%1 = field caption for Send Invitation, %2 = Salesperson Name";ENU=You cannot select the %1 for %2 because he/she does not have an email address.;PTG=N�o pode selecionar o %1 para %2 porque ele/ela n�o possui um endere�o de email.';
      Text005@1006 : TextConst 'ENU=You cannot delete a to-do organizer.;PTG=N�o pode eliminar um organizador de a��o a efetuar.';
      Text006@1007 : TextConst 'ENU=You cannot change an %1 for a to-do organizer.;PTG=N�o pode modificar um %1 para um organizador de a��o a efetuar.';
      Text007@1008 : TextConst 'ENU=The %1 option is not available for a to-do organizer.;PTG=A op��o %1 n�o est� dispon�vel para um organizador de a��o a efetuar.';
      Text008@1010 : TextConst 'ENU=You cannot change the to-do organizer.;PTG=N�o pode modificar o organizador da a��o a efetuar.';
      Text011@1009 : TextConst '@@@="%1 = Sales / Purchaseer person name";ENU=You cannot set %1 as organizer because he/she does not have email address.;PTG=N�o pode definir %1 como organizador porque ele/ela n�o tem um endere�o de email.';

    PROCEDURE ValidateAttendee@4(AttendeeRec@1000 : Record 5199;VAR Attendee@1001 : Record 5199);
    BEGIN
      AttendeeRec.TESTFIELD("Attendee No.");
      ValidateOrganizer(AttendeeRec."Attendee No.",AttendeeRec."Attendance Type",AttendeeRec."Attendee Type",AttendeeRec."To-do No.");

      IF AttendeeRec."Attendance Type" = "Attendance Type"::"To-do Organizer" THEN BEGIN
        IF AttendeeRec."Attendee Type" = "Attendee Type"::Contact THEN
          ERROR(Text001);

        Attendee.SETRANGE("To-do No.",AttendeeRec."To-do No.");
        Attendee.SETRANGE("Attendance Type","Attendance Type"::"To-do Organizer");
        IF Attendee.FIND('-') THEN
          IF Attendee."Line No." <> AttendeeRec."Line No." THEN BEGIN
            Attendee.RESET;
            ERROR(Text002);
          END;
        Attendee.RESET;
      END;

      Attendee.SETRANGE("To-do No.",AttendeeRec."To-do No.");
      Attendee.SETFILTER("Attendee No.",AttendeeRec."Attendee No.");
      IF Attendee.FIND('-') THEN
        IF Attendee."Line No." <> AttendeeRec."Line No." THEN BEGIN
          Attendee.RESET;
          ERROR(Text003);
        END;
      Attendee.RESET;
    END;

    PROCEDURE CreateAttendee@5(VAR Attendee@1005 : Record 5199;TodoNo@1006 : Code[20];LineNo@1003 : Integer;AttendanceType@1002 : Integer;AttendeeType@1001 : Integer;AttendeeNo@1000 : Code[20];SendInvitation@1004 : Boolean);
    BEGIN
      ValidateOrganizer(AttendeeNo,AttendanceType,AttendeeType,TodoNo);

      Attendee.INIT;
      Attendee."To-do No." := TodoNo;
      Attendee."Line No." := LineNo;
      Attendee."Attendance Type" := AttendanceType;
      Attendee.VALIDATE("Attendee Type",AttendeeType);
      Attendee.VALIDATE("Attendee No.",AttendeeNo);
      IF Attendee."Attendance Type" <> Attendee."Attendance Type"::"To-do Organizer" THEN
        Attendee.VALIDATE("Send Invitation",SendInvitation)
      ELSE
        Attendee.VALIDATE("Send Invitation",TRUE);
      IF NOT Attendee.GET(Attendee."To-do No.",Attendee."Line No.") THEN
        Attendee.INSERT;
    END;

    LOCAL PROCEDURE ValidateOrganizer@1(AttendeeNo@1001 : Code[20];AttendanceType@1002 : Integer;AttendeeType@1003 : Integer;TodoNo@1004 : Code[20]);
    VAR
      SalesPurchPerson@1000 : Record 13;
      ToDo@1005 : Record 5080;
    BEGIN
      IF AttendanceType <> Attendee."Attendance Type"::"To-do Organizer" THEN
        EXIT;

      IF AttendeeType = "Attendee Type"::Contact THEN
        ERROR(Text001);

      SalesPurchPerson.GET(AttendeeNo);
      ToDo.INIT;
      IF ToDo.GET(TodoNo) THEN;
      IF (SalesPurchPerson."E-Mail" = '') AND (ToDo.Type <> ToDo.Type::"Phone Call") THEN
        ERROR(Text011,SalesPurchPerson.Name);
    END;

    BEGIN
    END.
  }
}

