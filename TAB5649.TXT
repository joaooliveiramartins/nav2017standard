OBJECT Table 5649 FA Posting Group Buffer
{
  OBJECT-PROPERTIES
  {
    Date=05/08/11;
    Time=12:00:00;
    Version List=NAVW16.00;
  }
  PROPERTIES
  {
    CaptionML=[ENU=FA Posting Group Buffer;
               PTG=Mem. Int. Gr. ContabilĄstico Imob.];
  }
  FIELDS
  {
    { 1   ;   ;FA Posting Group    ;Code10        ;CaptionML=[ENU=FA Posting Group;
                                                              PTG=Gr. ContabilĄstico Imob.] }
    { 2   ;   ;Posting Type        ;Option        ;CaptionML=[ENU=Posting Type;
                                                              PTG=Tipo Registo];
                                                   OptionCaptionML=[ENU=Acq,Depr,WD,Appr,C1,C2,DeprExp,Maint,Disp,GL,BV,DispAcq,DispDepr,DispWD,DispAppr,DispC1,DispC2,BalWD,BalAppr,BalC1,BalC2;
                                                                    PTG=Acq,Depr,WD,Appr,C1,C2,DeprExp,Maint,Disp,GL,BV,DispAcq,DispDepr,DispWD,DispAppr,DispC1,DispC2,BalWD,BalAppr,BalC1,BalC2];
                                                   OptionString=Acq,Depr,WD,Appr,C1,C2,DeprExp,Maint,Disp,GL,BV,DispAcq,DispDepr,DispWD,DispAppr,DispC1,DispC2,BalWD,BalAppr,BalC1,BalC2 }
    { 3   ;   ;Account No.         ;Code20        ;CaptionML=[ENU=Account No.;
                                                              PTG=N§ Conta] }
    { 4   ;   ;Amount              ;Decimal       ;CaptionML=[ENU=Amount;
                                                              PTG=Valor] }
    { 5   ;   ;Account Name        ;Text50        ;CaptionML=[ENU=Account Name;
                                                              PTG=Nome Conta] }
    { 6   ;   ;FA FieldCaption     ;Text50        ;CaptionML=[ENU=FA FieldCaption;
                                                              PTG=Imob. LegendaCampo] }
  }
  KEYS
  {
    {    ;FA Posting Group,Posting Type,Account No.;
                                                   Clustered=Yes }
    {    ;Account No.                             ;SumIndexFields=Amount }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    END.
  }
}

