OBJECT Table 50045 Field Permissions
{
  OBJECT-PROPERTIES
  {
    Date=15/01/15;
    Time=17:38:12;
    Modified=Yes;
    Version List=SGG01.00;
  }
  PROPERTIES
  {
    DataPerCompany=No;
  }
  FIELDS
  {
    { 1   ;   ;Company Name        ;Text30        ;TableRelation=Company }
    { 2   ;   ;Table No.           ;Integer       ;TableRelation=Object.ID WHERE (Type=CONST(TableData)) }
    { 3   ;   ;Field No.           ;Integer       ;TableRelation=Field.No. WHERE (TableNo=FIELD(Table No.));
                                                   OnLookup=BEGIN
                                                              AssistEdit;
                                                            END;
                                                             }
    { 4   ;   ;Type                ;Option        ;OptionString=[ ,Group,User,Login,Windows Access Control] }
    { 5   ;   ;No.                 ;Text65        ;ValidateTableRelation=No;
                                                   TestTableRelation=No }
    { 6   ;   ;Enabled             ;Boolean        }
    { 7   ;   ;Editable            ;Boolean        }
    { 8   ;   ;Modifiable          ;Boolean        }
    { 9   ;   ;Visible             ;Boolean        }
    { 10  ;   ;Deletable           ;Boolean        }
    { 15  ;   ;Blocked             ;Boolean        }
    { 50000;  ;Table Name          ;Text50        ;FieldClass=FlowField;
                                                   CalcFormula=Lookup("Table Information"."Table Name" WHERE (Table No.=FIELD(Table No.))) }
    { 50001;  ;Field Name          ;Text50        ;FieldClass=FlowField;
                                                   CalcFormula=Lookup(Field.FieldName WHERE (TableNo=FIELD(Table No.),
                                                                                             No.=FIELD(Field No.))) }
    { 50002;  ;User Filter         ;Code20        ;FieldClass=FlowFilter }
    { 50003;  ;Windows Login Filter;Text65        ;FieldClass=FlowFilter }
    { 50004;  ;Belongs To WAC      ;Boolean        }
    { 50005;  ;Belongs To Group    ;Boolean        }
  }
  KEYS
  {
    {    ;Company Name,Table No.,Field No.,Type,No.;
                                                   Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {
    VAR
      recSession@1000000001 : Record 2000000009;
      TempField@1000000002 : TEMPORARY Record 2000000041;
      x@1000000003 : Text[500];

    PROCEDURE ControlPermissionWithRaiseErro@1000000001(pTable@1000000004 : Integer;pField@1000000003 : Integer);
    VAR
      lEditable@1000000000 : Boolean;
      lEnabled@1000000001 : Boolean;
      errPermission@1000000002 : TextConst 'PTG=N�o pode editar a tabela %1 (campo %2).';
      lModifiable@1000000005 : Boolean;
      lVisible@1000000007 : Boolean;
      lDeletable@1000000006 : Boolean;
    BEGIN
      IF GetPermission(pTable, pField, '', lEnabled, lEditable, lModifiable,lVisible, lDeletable) THEN
        IF NOT lModifiable THEN
          ERROR(errPermission, pTable, pField);
    END;

    PROCEDURE GetPermission@1000000000(pTable@1000000000 : Integer;pField@1000000001 : Integer;pUser@1000000002 : Text[60];VAR pEnabled@1000000006 : Boolean;VAR pEditable@1000000007 : Boolean;VAR pModifiable@1000000008 : Boolean;VAR pVisible@1000000009 : Boolean;VAR pDeletable@1000000010 : Boolean) : Boolean;
    VAR
      IsWindowsAuthentication@1000000003 : Boolean;
      IsDatabaseAuthentication@1000000004 : Boolean;
      FullLoginName@1000000005 : Text[65];
    BEGIN
      // Check if anything is defined for Table and Field

      RESET;
      SETRANGE("Table No.", pTable);
      SETRANGE("Field No.", pField);
      SETRANGE(Blocked, FALSE);

      IF NOT FIND('-') THEN EXIT(FALSE);

      // Definitions
      // Table/Field Mandatory
      // more detail -> more generic
      //

      IsWindowsAuthentication := WindowsAuthentication;
      IsDatabaseAuthentication := DatabaseAuthentication;

      FullLoginName := pUser;
      IF FullLoginName = '' THEN FullLoginName := GetFullLoginName;

      IF IsDatabaseAuthentication THEN BEGIN
        SETRANGE("Company Name", COMPANYNAME);
        SETRANGE(Type, Type::User);
        SETFILTER("No.", STRSUBSTNO('@%1', FullLoginName));
        IF FIND('-') THEN BEGIN
          pEnabled := Enabled;
          pEditable := Editable;
          pModifiable := Modifiable;
          pVisible   := Visible;
          pDeletable := Deletable;
          EXIT(TRUE);
        END;
        SETFILTER("Company Name", '''''');
        IF FIND('-') THEN BEGIN
          pEnabled := Enabled;
          pEditable := Editable;
          pModifiable := Modifiable;
          pVisible   := Visible;
          pDeletable := Deletable;
          EXIT(TRUE);
        END;

        SETFILTER("No.", '');
        SETRANGE("Company Name", COMPANYNAME);
        SETRANGE(Type, Type::Group);
        SETFILTER("User Filter", STRSUBSTNO('@%1', FullLoginName));
        SETRANGE("Belongs To Group", TRUE);
        IF FIND('-') THEN BEGIN
          pEnabled := Enabled;
          pEditable := Editable;
          pModifiable := Modifiable;
          pVisible   := Visible;
          pDeletable := Deletable;
          EXIT(TRUE);
        END;
        SETFILTER("Company Name", '''''');
        IF FIND('-') THEN BEGIN
          pEnabled := Enabled;
          pEditable := Editable;
          pModifiable := Modifiable;
          pVisible   := Visible;
          pDeletable := Deletable;
          EXIT(TRUE);
        END;
      END;

      IF IsWindowsAuthentication THEN BEGIN
        SETRANGE("Company Name", COMPANYNAME);
        SETRANGE(Type, Type::Login);
        SETFILTER("No.", STRSUBSTNO('@%1', FullLoginName));
        IF FIND('-') THEN BEGIN
          pEnabled := Enabled;
          pEditable := Editable;
          pModifiable := Modifiable;
          pVisible   := Visible;
          pDeletable := Deletable;
          EXIT(TRUE);
        END;
        SETFILTER("Company Name", '''''');
        IF FIND('-') THEN BEGIN
          pEnabled := Enabled;
          pEditable := Editable;
          pModifiable := Modifiable;
          pVisible   := Visible;
          pDeletable := Deletable;
          EXIT(TRUE);
        END;

        SETFILTER("No.", '');
        SETRANGE("Company Name", COMPANYNAME);
        SETRANGE(Type, Type::"Windows Access Control");
        SETFILTER("Windows Login Filter", STRSUBSTNO('@%1', FullLoginName));
        SETRANGE("Belongs To WAC", TRUE);
        IF FIND('-') THEN BEGIN
          pEnabled := Enabled;
          pEditable := Editable;
          pModifiable := Modifiable;
          pVisible   := Visible;
          pDeletable := Deletable;
          EXIT(TRUE);
        END;
        SETFILTER("Company Name", '''''');
        x := GETFILTERS();
      //  MESSAGE('%1\%2',GETFILTERS, COUNT);
        IF FIND('-') THEN BEGIN
          pEnabled := Enabled;
          pEditable := Editable;
          pModifiable := Modifiable;
          pVisible   := Visible;
          pDeletable := Deletable;
          EXIT(TRUE);
        END;
      END;

      //IF pField <> 0 THEN
      //  EXIT( GetPermission(pTable, 0, FullLoginName, pEnabled, pEditable));

      EXIT (FALSE);
    END;

    PROCEDURE WindowsAuthentication@1000000002() : Boolean;
    BEGIN
      EXIT(GetLoginType = 1);
    END;

    PROCEDURE DatabaseAuthentication@1000000003() : Boolean;
    BEGIN
      EXIT(GetLoginType = 0);
    END;

    PROCEDURE GetLoginType@1000000007() : Integer;
    BEGIN
      CLEAR(recSession);
      recSession.SETRANGE("My Session",TRUE);
      IF recSession.FIND('-') THEN
        EXIT(recSession."Login Type");
    END;

    PROCEDURE GetFullLoginName@1000000010() : Code[20];
    BEGIN
      IF NOT recSession."My Session" THEN BEGIN
        CLEAR(recSession);
        recSession.SETRANGE("My Session",TRUE);
        IF recSession.FIND('-') THEN ;
      END;
      EXIT(recSession."User ID");
    END;

    PROCEDURE ControlModification@3(VAR RecRef@1000 : RecordRef;VAR xRecRef@1001 : RecordRef);
    VAR
      FldRef@1003 : FieldRef;
      xFldRef@1004 : FieldRef;
      i@1002 : Integer;
      cduChangeLogMGM@1000000000 : Codeunit 423;
    BEGIN
      ControlPermissionWithRaiseErro(RecRef.NUMBER, 0);

      FOR i := 1 TO RecRef.FIELDCOUNT DO BEGIN
        FldRef := RecRef.FIELDINDEX(i);
        xFldRef := xRecRef.FIELDINDEX(i);
        IF IsNormalField(RecRef.NUMBER,FldRef.NUMBER) THEN
          IF FORMAT(FldRef.VALUE) <> FORMAT(xFldRef.VALUE) THEN BEGIN
      // DTT.SS
            ControlPermissionWithRaiseErro(RecRef.NUMBER, FldRef.NUMBER);
          END;
      END;
    END;

    LOCAL PROCEDURE IsNormalField@12(TableNumber@1001 : Integer;FieldNumber@1002 : Integer) : Boolean;
    VAR
      Field@1000 : Record 2000000041;
    BEGIN
      GetField(TableNumber,FieldNumber,Field);
      EXIT(Field.Class = TempField.Class::Normal);
    END;

    LOCAL PROCEDURE GetField@7(TableNumber@1001 : Integer;FieldNumber@1002 : Integer;VAR Field2@1003 : Record 2000000041);
    VAR
      Field@1000 : Record 2000000041;
    BEGIN
      IF NOT TempField.GET(TableNumber,FieldNumber) THEN BEGIN
        Field.GET(TableNumber,FieldNumber);
        TempField := Field;
        TempField.INSERT;
      END;
      Field2 := TempField;
    END;

    LOCAL PROCEDURE AssistEdit@1();
    VAR
      Field@1001 : Record 2000000041;
      ChangeLogSetupFieldList@1002 : Page 594;
    BEGIN
      Field.FILTERGROUP(2);
      Field.SETRANGE(TableNo, "Table No.");
      Field.FILTERGROUP(0);
      ChangeLogSetupFieldList.SelectColumn(FALSE, FALSE, FALSE);
      IF Field.GET("Table No.", "Field No.") THEN ;
      ChangeLogSetupFieldList.SETTABLEVIEW(Field);
      ChangeLogSetupFieldList.SETRECORD(Field);
      ChangeLogSetupFieldList.LOOKUPMODE := TRUE;
      //ChangeLogSetupFieldList.RUN;
      IF ChangeLogSetupFieldList.RUNMODAL = ACTION::LookupOK THEN BEGIN
        ChangeLogSetupFieldList.GETRECORD(Field);
        MESSAGE('%1', Field."No.");
        RENAME(Rec."Company Name", Rec."Table No.", Field."No.", Rec.Type, Rec."No.");
      END;
    END;

    BEGIN
    {
      SGG01.00 SSOUSA 27-02-2007 Valida��o de Permiss�es Campo a Campo
    }
    END.
  }
}

