OBJECT Page 5740 Transfer Order
{
  OBJECT-PROPERTIES
  {
    Date=07/03/17;
    Time=17:59:06;
    Modified=Yes;
    Version List=NAVW110.0,NAVPT10.00#00018;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Transfer Order;
               PTG=Ordem Transfer�ncia];
    SourceTable=Table5740;
    PageType=Document;
    RefreshOnActivate=Yes;
    OnDeleteRecord=BEGIN
                     TESTFIELD(Status,Status::Open);
                   END;

    OnAfterGetCurrRecord=BEGIN
                           //V92.00#00033,sn
                           SetEntityEnabled;
                           CurrPage.UPDATE(FALSE);
                           //V92.00#00033,en
                         END;

    ActionList=ACTIONS
    {
      { 1900000003;0 ;ActionContainer;
                      ActionContainerType=RelatedInformation }
      { 57      ;1   ;ActionGroup;
                      CaptionML=[ENU=O&rder;
                                 PTG=O&rdem];
                      Image=Order }
      { 27      ;2   ;Action    ;
                      ShortCutKey=F7;
                      CaptionML=[ENU=Statistics;
                                 PTG=Estat�sticas];
                      RunObject=Page 5755;
                      RunPageLink=No.=FIELD(No.);
                      Promoted=Yes;
                      Image=Statistics;
                      PromotedCategory=Process }
      { 28      ;2   ;Action    ;
                      CaptionML=[ENU=Co&mments;
                                 PTG=Co&ment�rios];
                      RunObject=Page 5750;
                      RunPageLink=Document Type=CONST(Transfer Order),
                                  No.=FIELD(No.);
                      Image=ViewComments }
      { 65      ;2   ;Action    ;
                      Name=Dimensions;
                      AccessByPermission=TableData 348=R;
                      ShortCutKey=Shift+Ctrl+D;
                      CaptionML=[ENU=Dimensions;
                                 PTG=Dimens�es];
                      ToolTipML=[ENU=View or edit dimensions, such as area, project, or department, that you can assign to sales and purchase documents to distribute costs and analyze transaction history.;
                                 PTG=""];
                      Image=Dimensions;
                      OnAction=BEGIN
                                 ShowDocDim;
                                 CurrPage.SAVERECORD;
                               END;
                                }
      { 5       ;1   ;ActionGroup;
                      CaptionML=[ENU=Documents;
                                 PTG=Documentos];
                      Image=Documents }
      { 81      ;2   ;Action    ;
                      CaptionML=[ENU=S&hipments;
                                 PTG=E&nvios];
                      RunObject=Page 5752;
                      RunPageLink=Transfer Order No.=FIELD(No.);
                      Image=Shipment }
      { 82      ;2   ;Action    ;
                      CaptionML=[ENU=Re&ceipts;
                                 PTG=Re&ce��es];
                      RunObject=Page 5753;
                      RunPageLink=Transfer Order No.=FIELD(No.);
                      Image=PostedReceipts }
      { 7       ;1   ;ActionGroup;
                      CaptionML=[ENU=Warehouse;
                                 PTG=Armaz�m];
                      Image=Warehouse }
      { 98      ;2   ;Action    ;
                      CaptionML=[ENU=Whse. Shi&pments;
                                 PTG=Envios Armaz�&m];
                      RunObject=Page 7341;
                      RunPageView=SORTING(Source Type,Source Subtype,Source No.,Source Line No.);
                      RunPageLink=Source Type=CONST(5741),
                                  Source Subtype=CONST(0),
                                  Source No.=FIELD(No.);
                      Image=Shipment }
      { 97      ;2   ;Action    ;
                      CaptionML=[ENU=&Whse. Receipts;
                                 PTG=Rece��es Arma&z�m];
                      RunObject=Page 7342;
                      RunPageView=SORTING(Source Type,Source Subtype,Source No.,Source Line No.);
                      RunPageLink=Source Type=CONST(5741),
                                  Source Subtype=CONST(1),
                                  Source No.=FIELD(No.);
                      Image=Receipt }
      { 85      ;2   ;Action    ;
                      CaptionML=[ENU=In&vt. Put-away/Pick Lines;
                                 PTG=Linhas Arrum��o / Recolha In&vent�rio];
                      RunObject=Page 5774;
                      RunPageView=SORTING(Source Document,Source No.,Location Code);
                      RunPageLink=Source Document=FILTER(Inbound Transfer|Outbound Transfer),
                                  Source No.=FIELD(No.);
                      Image=PickLines }
      { 1900000004;0 ;ActionContainer;
                      ActionContainerType=ActionItems }
      { 69      ;1   ;Action    ;
                      Ellipsis=Yes;
                      CaptionML=[ENU=&Print;
                                 PTG=&Imprimir];
                      Promoted=Yes;
                      Image=Print;
                      PromotedCategory=Process;
                      OnAction=VAR
                                 DocPrint@1001 : Codeunit 229;
                               BEGIN
                                 DocPrint.PrintTransferHeader(Rec);
                               END;
                                }
      { 3       ;1   ;ActionGroup;
                      CaptionML=[ENU=Release;
                                 PTG=Libertar];
                      Image=ReleaseDoc }
      { 59      ;2   ;Action    ;
                      ShortCutKey=Ctrl+F9;
                      CaptionML=[ENU=Re&lease;
                                 PTG=&Libertar];
                      RunObject=Codeunit 5708;
                      Promoted=Yes;
                      Image=ReleaseDoc;
                      PromotedCategory=Process }
      { 48      ;2   ;Action    ;
                      CaptionML=[ENU=Reo&pen;
                                 PTG=Rea&brir];
                      Promoted=Yes;
                      Image=ReOpen;
                      PromotedCategory=Process;
                      OnAction=VAR
                                 ReleaseTransferDoc@1001 : Codeunit 5708;
                               BEGIN
                                 ReleaseTransferDoc.Reopen(Rec);
                               END;
                                }
      { 58      ;1   ;ActionGroup;
                      CaptionML=[ENU=F&unctions;
                                 PTG=F&un��es];
                      Image=Action }
      { 5778    ;2   ;Action    ;
                      AccessByPermission=TableData 7320=R;
                      CaptionML=[ENU=Create Whse. S&hipment;
                                 PTG=Criar Envio Arma&z�m];
                      Image=NewShipment;
                      OnAction=VAR
                                 GetSourceDocOutbound@1001 : Codeunit 5752;
                               BEGIN
                                 GetSourceDocOutbound.CreateFromOutbndTransferOrder(Rec);
                               END;
                                }
      { 84      ;2   ;Action    ;
                      AccessByPermission=TableData 7316=R;
                      CaptionML=[ENU=Create &Whse. Receipt;
                                 PTG=Criar Rece��o Arma&z�m];
                      Image=NewReceipt;
                      OnAction=VAR
                                 GetSourceDocInbound@1001 : Codeunit 5751;
                               BEGIN
                                 GetSourceDocInbound.CreateFromInbndTransferOrder(Rec);
                               END;
                                }
      { 94      ;2   ;Action    ;
                      Ellipsis=Yes;
                      CaptionML=[ENU=Create Inventor&y Put-away/Pick;
                                 PTG=Criar Arruma��o / Recolha Invent�ri&o];
                      Image=CreateInventoryPickup;
                      OnAction=BEGIN
                                 CreateInvtPutAwayPick;
                               END;
                                }
      { 95      ;2   ;Action    ;
                      AccessByPermission=TableData 7302=R;
                      Ellipsis=Yes;
                      CaptionML=[ENU=Get Bin Content;
                                 PTG=Obter Conte�do Posi��o];
                      Image=GetBinContent;
                      OnAction=VAR
                                 BinContent@1002 : Record 7302;
                                 GetBinContent@1000 : Report 7391;
                               BEGIN
                                 BinContent.SETRANGE("Location Code","Transfer-from Code");
                                 GetBinContent.SETTABLEVIEW(BinContent);
                                 GetBinContent.InitializeTransferHeader(Rec);
                                 GetBinContent.RUNMODAL;
                               END;
                                }
      { 62      ;1   ;ActionGroup;
                      CaptionML=[ENU=P&osting;
                                 PTG=&Registo];
                      Image=Post }
      { 66      ;2   ;Action    ;
                      Name=Post;
                      ShortCutKey=F9;
                      Ellipsis=Yes;
                      CaptionML=[ENU=P&ost;
                                 PTG=R&egistar];
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=PostOrder;
                      PromotedCategory=Process;
                      OnAction=BEGIN
                                 //V92.00#00033,sn
                                 CODEUNIT.RUN(CODEUNIT :: "TransferOrder-Post (Yes/No)",Rec);
                                 CurrPage.UPDATE(FALSE);
                                 //V92.00#00033,en
                               END;
                                }
      { 67      ;2   ;Action    ;
                      Name=PostAndPrint;
                      ShortCutKey=Shift+F9;
                      CaptionML=[ENU=Post and &Print;
                                 PTG=Registar e &Imprimir];
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=PostPrint;
                      PromotedCategory=Process;
                      OnAction=BEGIN
                                 //V92.00#00033,sn
                                 CODEUNIT.RUN(CODEUNIT ::"TransferOrder-Post + Print");
                                 CurrPage.UPDATE(FALSE);
                                 //V92.00#00033,en
                               END;
                                }
      { 1900000006;0 ;ActionContainer;
                      ActionContainerType=Reports }
      { 1901320106;1 ;Action    ;
                      CaptionML=[ENU=Inventory - Inbound Transfer;
                                 PTG=Invent�rio - Transf. Entrada];
                      RunObject=Report 5702;
                      Promoted=Yes;
                      Image=Report;
                      PromotedCategory=Report }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                CaptionML=[ENU=General;
                           PTG=Geral] }

    { 2   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the number of the transfer order.;
                           PTG=""];
                SourceExpr="No.";
                Importance=Promoted;
                OnAssistEdit=BEGIN
                               IF AssistEdit(xRec) THEN
                                 CurrPage.UPDATE;
                             END;
                              }

    { 14  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the code of the location from which items are transferred.;
                           PTG=""];
                SourceExpr="Transfer-from Code";
                Importance=Promoted }

    { 34  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the code of the location that you are transferring items to.;
                           PTG=""];
                SourceExpr="Transfer-to Code";
                Importance=Promoted;
                OnValidate=BEGIN
                             //V92.00#00033,en
                             SetEntityEnabled;
                             //V10.00#00018,o CurrPage.UPDATE(FALSE);
                             //V92.00#00033,en
                           END;
                            }

    { 8   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the in-transit code that identifies this transfer.;
                           PTG=""];
                SourceExpr="In-Transit Code" }

    { 4   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the posting date of the transfer order.;
                           PTG=""];
                SourceExpr="Posting Date";
                OnValidate=BEGIN
                             PostingDateOnAfterValidate;
                           END;
                            }

    { 10  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the dimension value code for the dimension that has been chosen as Global Dimension 1.;
                           PTG=""];
                SourceExpr="Shortcut Dimension 1 Code" }

    { 12  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the dimension value code for the dimension that has been chosen as Global Dimension 2.;
                           PTG=""];
                SourceExpr="Shortcut Dimension 2 Code" }

    { 106 ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the ID of the user who is responsible for the document.;
                           PTG=""];
                SourceExpr="Assigned User ID" }

    { 6   ;2   ;Field     ;
                ToolTipML=[ENU=Indicates whether the transfer order is open or has been released for the next stage of processing.;
                           PTG=""];
                SourceExpr=Status;
                Importance=Promoted }

    { 11  ;2   ;Field     ;
                SourceExpr="Location Type";
                Editable=FALSE }

    { 13  ;2   ;Field     ;
                Name=<Location Type>;
                SourceExpr="External Entity No.";
                Editable=SetEnabled }

    { 15  ;2   ;Field     ;
                SourceExpr="Ship-to Code";
                Editable=SetEnabled }

    { 55  ;1   ;Part      ;
                Name=TransferLines;
                SubPageLink=Document No.=FIELD(No.),
                            Derived From Line No.=CONST(0);
                PagePartID=Page5741 }

    { 1904655901;1;Group  ;
                CaptionML=[ENU=Transfer-from;
                           PTG=Transferir-de] }

    { 16  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the name of the location from which items are transferred.;
                           PTG=""];
                SourceExpr="Transfer-from Name" }

    { 18  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies an additional part of the name of the location from which items are transferred.;
                           PTG=""];
                SourceExpr="Transfer-from Name 2" }

    { 20  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the address of the location from which items are transferred.;
                           PTG=""];
                SourceExpr="Transfer-from Address" }

    { 22  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies an additional part of the address.;
                           PTG=""];
                SourceExpr="Transfer-from Address 2" }

    { 24  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the postal code of the location from which items are transferred.;
                           PTG=""];
                SourceExpr="Transfer-from Post Code" }

    { 26  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the city of the location from which items are transferred.;
                           PTG=""];
                SourceExpr="Transfer-from City" }

    { 1110001;2;Field     ;
                SourceExpr="Transfer-from County" }

    { 49  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the name of the contact person at the transfer-from location.;
                           PTG=""];
                SourceExpr="Transfer-from Contact" }

    { 32  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the date the order is expected to be shipped.;
                           PTG=""];
                SourceExpr="Shipment Date";
                Importance=Promoted;
                OnValidate=BEGIN
                             ShipmentDateOnAfterValidate;
                           END;
                            }

    { 1000000001;2;Field  ;
                SourceExpr="Shipment Start Time";
                Importance=Promoted }

    { 89  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the time it takes for the Transfer-from location to prepare the shipment to the Transfer-to location.;
                           PTG=""];
                SourceExpr="Outbound Whse. Handling Time";
                OnValidate=BEGIN
                             OutboundWhseHandlingTimeOnAfte;
                           END;
                            }

    { 79  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the shipment method code that you have entered for this order.;
                           PTG=""];
                SourceExpr="Shipment Method Code" }

    { 72  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the code for the shipping agent you are using to ship the items on this transfer order.;
                           PTG=""];
                SourceExpr="Shipping Agent Code";
                Importance=Promoted;
                OnValidate=BEGIN
                             ShippingAgentCodeOnAfterValida;
                           END;
                            }

    { 74  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the code for the shipping agent service that you are using to ship the items on this transfer order.;
                           PTG=""];
                SourceExpr="Shipping Agent Service Code";
                OnValidate=BEGIN
                             ShippingAgentServiceCodeOnAfte;
                           END;
                            }

    { 76  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the shipping time, used to calculate the receipt date.;
                           PTG=""];
                SourceExpr="Shipping Time";
                OnValidate=BEGIN
                             ShippingTimeOnAfterValidate;
                           END;
                            }

    { 70  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies advice for the warehouse sending the items, about whether a partial delivery is acceptable.;
                           PTG=""];
                SourceExpr="Shipping Advice";
                Importance=Promoted;
                OnValidate=BEGIN
                             IF "Shipping Advice" <> xRec."Shipping Advice" THEN
                               IF NOT CONFIRM(Text000,FALSE,FIELDCAPTION("Shipping Advice")) THEN
                                 ERROR('');
                           END;
                            }

    { 1901454601;1;Group  ;
                CaptionML=[ENU=Transfer-to;
                           PTG=Transferir-para] }

    { 36  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the name of the location that you are transferring items to.;
                           PTG=""];
                SourceExpr="Transfer-to Name" }

    { 38  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies an additional part of the transfer-to name of the location that you are transferring items to.;
                           PTG=""];
                SourceExpr="Transfer-to Name 2" }

    { 40  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the address of the location that you are transferring items to.;
                           PTG=""];
                SourceExpr="Transfer-to Address" }

    { 42  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies an additional part of the address of the location to which items are transferred.;
                           PTG=""];
                SourceExpr="Transfer-to Address 2" }

    { 44  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the postal code of the location that you are transferring items to.;
                           PTG=""];
                SourceExpr="Transfer-to Post Code" }

    { 46  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the city of the location that you are transferring items to.;
                           PTG=""];
                SourceExpr="Transfer-to City" }

    { 1110200;2;Field     ;
                SourceExpr="Transfer-to County" }

    { 51  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the name of the contact person at the transfer-to location.;
                           PTG=""];
                SourceExpr="Transfer-to Contact" }

    { 52  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the date that you expect the transfer-to location to receive the shipment.;
                           PTG=""];
                SourceExpr="Receipt Date";
                OnValidate=BEGIN
                             ReceiptDateOnAfterValidate;
                           END;
                            }

    { 91  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the time it takes to make items part of available inventory, after the items have been posted as received.;
                           PTG=""];
                SourceExpr="Inbound Whse. Handling Time";
                OnValidate=BEGIN
                             InboundWhseHandlingTimeOnAfter;
                           END;
                            }

    { 9   ;2   ;Field     ;
                SourceExpr="Transfer-to VAT Reg. No." }

    { 1907468901;1;Group  ;
                CaptionML=[ENU=Foreign Trade;
                           PTG=Com�rcio Externo] }

    { 104 ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the transaction type of the transfer.;
                           PTG=""];
                SourceExpr="Transaction Type";
                Importance=Promoted }

    { 102 ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the transaction specification code that was used in the transfer.;
                           PTG=""];
                SourceExpr="Transaction Specification" }

    { 100 ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the code for the transport method used for the item on this line.;
                           PTG=""];
                SourceExpr="Transport Method";
                Importance=Promoted }

    { 96  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the code for an area at the customer or vendor with which you are trading the items on the line.;
                           PTG=""];
                SourceExpr=Area }

    { 83  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the code of either the port of entry at which the items passed into your country/region, or the port of exit.;
                           PTG=""];
                SourceExpr="Entry/Exit Point" }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                Visible=TRUE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {
    VAR
      Text000@1000 : TextConst 'ENU=Do you want to change %1 in all related records in the warehouse?;PTG=Deseja alterar %1 em todos os registos de armaz�m associados?';
      SetEnabled@1003 : Boolean;
      Location@1002 : Record 14;

    LOCAL PROCEDURE PostingDateOnAfterValidate@19003005();
    BEGIN
      CurrPage.TransferLines.PAGE.UpdateForm(TRUE);
    END;

    LOCAL PROCEDURE ShipmentDateOnAfterValidate@19068710();
    BEGIN
      CurrPage.TransferLines.PAGE.UpdateForm(FALSE);
    END;

    LOCAL PROCEDURE ShippingAgentServiceCodeOnAfte@19046563();
    BEGIN
      CurrPage.TransferLines.PAGE.UpdateForm(FALSE);
    END;

    LOCAL PROCEDURE ShippingAgentCodeOnAfterValida@19008956();
    BEGIN
      CurrPage.TransferLines.PAGE.UpdateForm(FALSE);
    END;

    LOCAL PROCEDURE ShippingTimeOnAfterValidate@19029567();
    BEGIN
      CurrPage.TransferLines.PAGE.UpdateForm(FALSE);
    END;

    LOCAL PROCEDURE OutboundWhseHandlingTimeOnAfte@19078070();
    BEGIN
      CurrPage.TransferLines.PAGE.UpdateForm(FALSE);
    END;

    LOCAL PROCEDURE ReceiptDateOnAfterValidate@19074743();
    BEGIN
      CurrPage.TransferLines.PAGE.UpdateForm(FALSE);
    END;

    LOCAL PROCEDURE InboundWhseHandlingTimeOnAfter@19076948();
    BEGIN
      CurrPage.TransferLines.PAGE.UpdateForm(FALSE);
    END;

    LOCAL PROCEDURE SetEntityEnabled@1();
    BEGIN
      //V92.00#00033,en
      SetEnabled := FALSE;
      IF ("Location Type" <> "Location Type"::Internal) THEN
        IF Location.GET("Transfer-to Code") AND (Location."External Entity No." = '') THEN
          SetEnabled := TRUE;
      //V92.00#00033,en
    END;

    BEGIN
    {
      V92.00#00033 - NF - Ordens Transfer�ncia a Terceiros - EFD - 2016.03.14
      20170307 V10.00#00018 : Ordem Transfer�ncia apagar campos transferir
    }
    END.
  }
}

