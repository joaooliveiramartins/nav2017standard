OBJECT Page 7348 Warehouse Employee List
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0;
  }
  PROPERTIES
  {
    Editable=No;
    CaptionML=[ENU=Warehouse Employee List;
               PTG=Lista Empregados Armaz�m];
    SourceTable=Table7301;
    DelayedInsert=Yes;
    PageType=List;
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 2   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the user ID of the warehouse employee.;
                           PTG=""];
                SourceExpr="User ID" }

    { 4   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the code of the location in which the employee works.;
                           PTG=""];
                SourceExpr="Location Code" }

    { 8   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies that the location code that is defined as the default location for this employee's activities.;
                           PTG=""];
                SourceExpr=Default }

    { 16  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the ADCS user name of a warehouse employee.;
                           PTG=""];
                SourceExpr="ADCS User" }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {

    BEGIN
    END.
  }
}

