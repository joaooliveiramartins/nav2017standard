OBJECT Report 9170 Copy Profile
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Copy Profile;
               PTG=Copiar Perfil];
    ProcessingOnly=Yes;
  }
  DATASET
  {
    { 3203;    ;DataItem;                    ;
               DataItemTable=Table2000000072;
               DataItemTableView=SORTING(Profile ID);
               OnAfterGetRecord=VAR
                                  ConfPersMgt@1000 : Codeunit 9170;
                                BEGIN
                                  ConfPersMgt.CopyProfile(Profile,NewProfileID);
                                END;
                                 }

  }
  REQUESTPAGE
  {
    PROPERTIES
    {
    }
    CONTROLS
    {
      { 1900000001;0;Container;
                  ContainerType=ContentArea }

      { 1900000002;1;Group  ;
                  CaptionML=[ENU=Options;
                             PTG=Op��es] }

      { 1   ;2   ;Field     ;
                  Name=NewProfileID;
                  CaptionML=[ENU=New Profile ID;
                             PTG=Novo ID do Perfil];
                  ToolTipML=[ENU=Specifies the new ID of the profile after copying.;
                             PTG=Especifica o novo ID do perfil de depois da c�pia.];
                  ApplicationArea=#Basic,#Suite;
                  NotBlank=Yes;
                  SourceExpr=NewProfileID }

    }
  }
  LABELS
  {
  }
  CODE
  {
    VAR
      NewProfileID@1002 : Code[30];

    PROCEDURE GetProfileID@1005() : Code[30];
    BEGIN
      EXIT(NewProfileID);
    END;

    BEGIN
    END.
  }
  RDLDATA
  {
  }
}

