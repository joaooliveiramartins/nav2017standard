OBJECT Page 249 VAT Registration Log
{
  OBJECT-PROPERTIES
  {
    Date=13/04/17;
    Time=15:12:35;
    Modified=Yes;
    Version List=NAVW110.0.00.15140;
  }
  PROPERTIES
  {
    CaptionML=[ENU=VAT Registration Log;
               PTG=Registo N� Contribuinte];
    InsertAllowed=No;
    DeleteAllowed=No;
    ModifyAllowed=No;
    SourceTable=Table249;
    SourceTableView=SORTING(Entry No.)
                    ORDER(Descending);
    DataCaptionFields=Account Type,Account No.;
    PageType=List;
    OnOpenPage=BEGIN
                 IF FINDFIRST THEN;
               END;

    ActionList=ACTIONS
    {
      { 12      ;    ;ActionContainer;
                      ActionContainerType=ActionItems }
      { 13      ;1   ;Action    ;
                      CaptionML=[ENU=Verify VAT Registration No.;
                                 PTG=Verificar N� Contribuinte];
                      ToolTipML=ENU=Verify a Tax registration number. If the number is verified the status field contains the value Valid.;
                      ApplicationArea=#Basic,#Suite;
                      RunObject=Codeunit 248;
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=Start;
                      PromotedCategory=Process }
    }
  }
  CONTROLS
  {
    { 1   ;0   ;Container ;
                ContainerType=ContentArea }

    { 2   ;1   ;Group     ;
                Name=Group;
                GroupType=Repeater }

    { 11  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the number of the verification action.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Entry No.";
                Editable=FALSE }

    { 4   ;2   ;Field     ;
                ToolTipML=ENU=Specifies the country/region of the address.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Country/Region Code";
                Editable=FALSE }

    { 3   ;2   ;Field     ;
                ToolTipML=ENU=Specifies the VAT registration number that you entered in the VAT Registration No. field on a customer, vendor, or contact card.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="VAT Registration No." }

    { 8   ;2   ;Field     ;
                ToolTipML=ENU=Specifies the account type of the customer or vendor whose VAT registration number is verified.;
                SourceExpr="Account Type";
                Visible=FALSE;
                Editable=FALSE }

    { 9   ;2   ;Field     ;
                ToolTipML=ENU=Specifies the account number of the customer or vendor whose VAT registration number is verified.;
                SourceExpr="Account No.";
                Visible=FALSE;
                Editable=FALSE }

    { 7   ;2   ;Field     ;
                ToolTipML=ENU=Specifies the status of the verification action.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr=Status }

    { 10  ;2   ;Field     ;
                ToolTipML=ENU=Specifies when the VAT registration number was verified.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Verified Date";
                Editable=FALSE }

    { 5   ;2   ;Field     ;
                ToolTipML=ENU=Specifies the name of the customer, vendor, or contact whose VAT registration number was verified.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Verified Name";
                Editable=FALSE }

    { 6   ;2   ;Field     ;
                ToolTipML=ENU=Specifies the address of the customer, vendor, or contact whose VAT registration number was verified.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Verified Address";
                Editable=FALSE }

    { 14  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the ID of the user who verified the VAT registration number.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="User ID" }

  }
  CODE
  {

    BEGIN
    END.
  }
}

