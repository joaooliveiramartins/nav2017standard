OBJECT Page 7303 Bin List
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0;
  }
  PROPERTIES
  {
    Editable=No;
    CaptionML=[ENU=Bin List;
               PTG=Lista Posi��es];
    SourceTable=Table7354;
    DataCaptionFields=Zone Code;
    PageType=List;
    ActionList=ACTIONS
    {
      { 1900000003;0 ;ActionContainer;
                      ActionContainerType=RelatedInformation }
      { 29      ;1   ;ActionGroup;
                      CaptionML=[ENU=&Bin;
                                 PTG=&Posi��o];
                      Image=Bins }
      { 7380    ;2   ;Action    ;
                      CaptionML=[ENU=&Contents;
                                 PTG=&Conte�do];
                      RunObject=Page 7305;
                      RunPageLink=Location Code=FIELD(Location Code),
                                  Zone Code=FIELD(Zone Code),
                                  Bin Code=FIELD(Code);
                      Image=BinContent }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 2   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the location from which you opened the Bins window.;
                           PTG=""];
                SourceExpr="Location Code";
                Visible=FALSE }

    { 4   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the code of the zone in which the bin is located.;
                           PTG=""];
                SourceExpr="Zone Code";
                Visible=FALSE }

    { 6   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies a code that uniquely describes the bin.;
                           PTG=""];
                SourceExpr=Code }

    { 14  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies a description of the bin.;
                           PTG=""];
                SourceExpr=Description }

    { 24  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies that the bin Specifies no items.;
                           PTG=""];
                SourceExpr=Empty }

    { 30  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies if the bin is the default bin for an item.;
                           PTG=""];
                SourceExpr=Default }

    { 8   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the code of the bin type that applies to the bin.;
                           PTG=""];
                SourceExpr="Bin Type Code";
                Visible=FALSE }

    { 10  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the code of the warehouse class that applies to the bin.;
                           PTG=""];
                SourceExpr="Warehouse Class Code";
                Visible=FALSE }

    { 12  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies how the movement of an item, or bin content, into or out of this bin, is blocked.;
                           PTG=""];
                SourceExpr="Block Movement";
                Visible=FALSE }

    { 16  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the code of the equipment needed when working in the bin.;
                           PTG=""];
                SourceExpr="Special Equipment Code";
                Visible=FALSE }

    { 18  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the ranking of the bin. Items in the highest-ranking bins (with the highest number in the field) will be picked first.;
                           PTG=""];
                SourceExpr="Bin Ranking";
                Visible=FALSE }

    { 20  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the maximum cubage (volume) that the bin can hold.;
                           PTG=""];
                SourceExpr="Maximum Cubage";
                Visible=FALSE }

    { 22  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the maximum weight that this bin can hold.;
                           PTG=""];
                SourceExpr="Maximum Weight";
                Visible=FALSE }

    { 32  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies that quantities in the bin are protected from being picked for other demands.;
                           PTG=""];
                SourceExpr=Dedicated }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {

    BEGIN
    END.
  }
}

