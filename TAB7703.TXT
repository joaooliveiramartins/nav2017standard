OBJECT Table 7703 Miniform Function
{
  OBJECT-PROPERTIES
  {
    Date=05/08/11;
    Time=12:00:00;
    Version List=NAVW16.00;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Miniform Function;
               PTG=Fun��o Miniform];
  }
  FIELDS
  {
    { 1   ;   ;Miniform Code       ;Code20        ;TableRelation="Miniform Header".Code;
                                                   CaptionML=[ENU=Miniform Code;
                                                              PTG=C�d. Miniform] }
    { 2   ;   ;Function Code       ;Code20        ;TableRelation="Miniform Function Group".Code;
                                                   CaptionML=[ENU=Function Code;
                                                              PTG=C�d. Fun��o] }
  }
  KEYS
  {
    {    ;Miniform Code,Function Code             ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    END.
  }
}

