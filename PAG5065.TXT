OBJECT Page 5065 Mailing Group Contacts
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Mailing Group Contacts;
               PTG=Grupos Destino Contactos];
    SourceTable=Table5056;
    DataCaptionFields=Mailing Group Code;
    PageType=List;
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 2   ;2   ;Field     ;
                ToolTipML=ENU=Specifies the number of the contact to which you assign a mailing group.;
                ApplicationArea=#All;
                SourceExpr="Contact No." }

    { 4   ;2   ;Field     ;
                DrillDown=No;
                ToolTipML=ENU=Specifies the name of the contact company. If the contact you assign the mailing group is a person, this field contains the name of the company for which the contact works.;
                ApplicationArea=#RelationshipMgmt;
                SourceExpr="Contact Company Name" }

    { 6   ;2   ;Field     ;
                DrillDown=No;
                ToolTipML=ENU=Specifies the name of the contact to which you assign a mailing group.;
                ApplicationArea=#RelationshipMgmt;
                SourceExpr="Contact Name" }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {

    BEGIN
    END.
  }
}

