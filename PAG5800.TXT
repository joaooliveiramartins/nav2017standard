OBJECT Page 5800 Item Charges
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Item Charges;
               PTG=Encargos Produto];
    SourceTable=Table5800;
    PageType=List;
    ActionList=ACTIONS
    {
      { 1900000003;0 ;ActionContainer;
                      ActionContainerType=RelatedInformation }
      { 15      ;1   ;ActionGroup;
                      CaptionML=[ENU=&Item Charge;
                                 PTG=Encar&go Produto];
                      Image=Add }
      { 17      ;2   ;Action    ;
                      ShortCutKey=Ctrl+F7;
                      CaptionML=[ENU=Value E&ntries;
                                 PTG=Mo&vs. Valor];
                      RunObject=Page 5802;
                      RunPageView=SORTING(Item Charge No.);
                      RunPageLink=Entry Type=CONST(Direct Cost),
                                  Item Charge No.=FIELD(No.);
                      Image=ValueLedger }
      { 19      ;2   ;Action    ;
                      ShortCutKey=Shift+Ctrl+D;
                      CaptionML=[ENU=Dimensions;
                                 PTG=Dimens�es];
                      ToolTipML=[ENU=View or edit dimensions, such as area, project, or department, that you can assign to sales and purchase documents to distribute costs and analyze transaction history.;
                                 PTG=""];
                      RunObject=Page 540;
                      RunPageLink=Table ID=CONST(5800),
                                  No.=FIELD(No.);
                      Image=Dimensions }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 2   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the number used for identifying a specific kind of item charge.;
                           PTG=""];
                SourceExpr="No." }

    { 4   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies a description of the item charge number that you are setting up.;
                           PTG=""];
                SourceExpr=Description }

    { 6   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the general product posting group to which this item charge belongs.;
                           PTG=""];
                SourceExpr="Gen. Prod. Posting Group" }

    { 8   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the sales tax group code that this item charge belongs to.;
                           PTG=""];
                SourceExpr="Tax Group Code";
                Visible=FALSE }

    { 10  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the VAT product posting group to which this item charge belongs.;
                           PTG=""];
                SourceExpr="VAT Prod. Posting Group" }

    { 16  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies text to search for when you do not know the number of the item.;
                           PTG=""];
                SourceExpr="Search Description" }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {

    BEGIN
    END.
  }
}

