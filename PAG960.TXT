OBJECT Page 960 Time Sheet Archive List
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Time Sheet Archive List;
               PTG=Lista Hist. Folha Horas];
    InsertAllowed=No;
    ModifyAllowed=No;
    SourceTable=Table954;
    SourceTableView=SORTING(Resource No.,Starting Date);
    PageType=List;
    OnOpenPage=BEGIN
                 IF UserSetup.GET(USERID) THEN
                   CurrPage.EDITABLE := UserSetup."Time Sheet Admin.";
                 TimeSheetMgt.FilterTimeSheetsArchive(Rec,FIELDNO("Owner User ID"));
               END;

    ActionList=ACTIONS
    {
      { 3       ;    ;ActionContainer;
                      ActionContainerType=ActionItems }
      { 5       ;1   ;Action    ;
                      ShortCutKey=Return;
                      CaptionML=[ENU=&View Time Sheet;
                                 PTG=&Ver Folha Horas];
                      ToolTipML=ENU=Open the time sheet.;
                      ApplicationArea=#Jobs;
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=OpenJournal;
                      PromotedCategory=Process;
                      OnAction=BEGIN
                                 ViewTimeSheet;
                               END;
                                }
      { 7       ;    ;ActionContainer;
                      ActionContainerType=RelatedInformation }
      { 8       ;1   ;ActionGroup;
                      CaptionML=[ENU=&Time Sheet;
                                 PTG=Folha Horas];
                      Image=Timesheet }
      { 9       ;2   ;Action    ;
                      Name=Comments;
                      CaptionML=[ENU=Co&mments;
                                 PTG=Co&ment rios];
                      RunObject=Page 963;
                      RunPageLink=No.=FIELD(No.),
                                  Time Sheet Line No.=CONST(0);
                      Image=ViewComments }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 11  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the number of the archived time sheet.;
                ApplicationArea=#Jobs;
                SourceExpr="No." }

    { 2   ;2   ;Field     ;
                ToolTipML=ENU=Specifies the start date for the archived time sheet.;
                ApplicationArea=#Jobs;
                SourceExpr="Starting Date" }

    { 4   ;2   ;Field     ;
                ToolTipML=ENU=Specifies the end date for an archived time sheet.;
                ApplicationArea=#Jobs;
                SourceExpr="Ending Date" }

    { 6   ;2   ;Field     ;
                ToolTipML=ENU=Specifies the list of resource numbers associated with an archived time sheet.;
                ApplicationArea=#Jobs;
                SourceExpr="Resource No." }

  }
  CODE
  {
    VAR
      UserSetup@1001 : Record 91;
      TimeSheetMgt@1000 : Codeunit 950;

    LOCAL PROCEDURE ViewTimeSheet@1();
    VAR
      TimeSheetLineArchive@1001 : Record 955;
    BEGIN
      TimeSheetMgt.SetTimeSheetArchiveNo("No.",TimeSheetLineArchive);
      PAGE.RUN(PAGE::"Time Sheet Archive",TimeSheetLineArchive);
    END;

    BEGIN
    END.
  }
}

