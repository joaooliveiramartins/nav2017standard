OBJECT Table 91 User Setup
{
  OBJECT-PROPERTIES
  {
    Date=15/06/17;
    Time=14:15:44;
    Modified=Yes;
    Version List=NAVW110.0,NAVPTSS85.00,SGG;
  }
  PROPERTIES
  {
    OnInsert=BEGIN
               IF (NOT IsOwner(USERID, COMPANYNAME)) AND (NOT IsMemberOfGroup(txtSuperUserGroup, USERID, COMPANYNAME)) THEN
                 ERROR(errNotAuthorized);
             END;

    OnModify=BEGIN
               IF (NOT IsOwner(USERID, COMPANYNAME)) AND (NOT IsMemberOfGroup(txtSuperUserGroup, USERID, COMPANYNAME)) THEN
                 ERROR(errNotAuthorized);
             END;

    OnDelete=VAR
               NotificationSetup@1000 : Record 1512;
             BEGIN
               IF (NOT IsOwner(USERID, COMPANYNAME)) AND (NOT IsMemberOfGroup(txtSuperUserGroup, USERID, COMPANYNAME)) THEN
                 ERROR(errNotAuthorized);

               NotificationSetup.SETRANGE("User ID","User ID");
               NotificationSetup.DELETEALL(TRUE);
             END;

    CaptionML=[ENU=User Setup;
               PTG=Configura��o Utilizador];
    LookupPageID=Page119;
    DrillDownPageID=Page119;
  }
  FIELDS
  {
    { 1   ;   ;User ID             ;Code50        ;TableRelation=User."User Name";
                                                   OnValidate=VAR
                                                                UserMgt@1000 : Codeunit 418;
                                                              BEGIN
                                                                UserMgt.ValidateUserID("User ID");
                                                              END;

                                                   OnLookup=VAR
                                                              UserMgt@1000 : Codeunit 418;
                                                            BEGIN
                                                              UserMgt.LookupUserID("User ID");
                                                            END;

                                                   ValidateTableRelation=No;
                                                   CaptionML=[ENU=User ID;
                                                              PTG=ID Utilizador];
                                                   NotBlank=Yes }
    { 2   ;   ;Allow Posting From  ;Date          ;CaptionML=[ENU=Allow Posting From;
                                                              PTG=Permite Registo Desde] }
    { 3   ;   ;Allow Posting To    ;Date          ;CaptionML=[ENU=Allow Posting To;
                                                              PTG=Permite Registo At�] }
    { 4   ;   ;Register Time       ;Boolean       ;CaptionML=[ENU=Register Time;
                                                              PTG=Regista Tempo Conex�o] }
    { 10  ;   ;Salespers./Purch. Code;Code10      ;TableRelation=Salesperson/Purchaser.Code;
                                                   OnValidate=VAR
                                                                UserSetup@1000 : Record 91;
                                                              BEGIN
                                                                IF "Salespers./Purch. Code" <> '' THEN BEGIN
                                                                  UserSetup.SETCURRENTKEY("Salespers./Purch. Code");
                                                                  UserSetup.SETRANGE("Salespers./Purch. Code","Salespers./Purch. Code");
                                                                  IF UserSetup.FINDFIRST THEN
                                                                    ERROR(Text001,"Salespers./Purch. Code",UserSetup."User ID");
                                                                END;
                                                              END;

                                                   CaptionML=[ENU=Salespers./Purch. Code;
                                                              PTG=C�d. Vendedor/Comprador] }
    { 11  ;   ;Approver ID         ;Code50        ;TableRelation="User Setup"."User ID";
                                                   OnValidate=BEGIN
                                                                IF "Approver ID" = "User ID" THEN
                                                                  FIELDERROR("Approver ID");
                                                              END;

                                                   OnLookup=VAR
                                                              UserSetup@1000 : Record 91;
                                                            BEGIN
                                                              UserSetup.SETFILTER("User ID",'<>%1',"User ID");
                                                              IF PAGE.RUNMODAL(PAGE::"Approval User Setup",UserSetup) = ACTION::LookupOK THEN
                                                                VALIDATE("Approver ID",UserSetup."User ID");
                                                            END;

                                                   CaptionML=[ENU=Approver ID;
                                                              PTG=ID Aprovador] }
    { 12  ;   ;Sales Amount Approval Limit;Integer;OnValidate=BEGIN
                                                                IF "Unlimited Sales Approval" AND ("Sales Amount Approval Limit" <> 0) THEN
                                                                  ERROR(Text003,FIELDCAPTION("Sales Amount Approval Limit"),FIELDCAPTION("Unlimited Sales Approval"));
                                                                IF "Sales Amount Approval Limit" < 0 THEN
                                                                  ERROR(Text005);
                                                              END;

                                                   CaptionML=[ENU=Sales Amount Approval Limit;
                                                              PTG=Limite Aprova��o Valor Vendas];
                                                   BlankZero=Yes }
    { 13  ;   ;Purchase Amount Approval Limit;Integer;
                                                   OnValidate=BEGIN
                                                                IF "Unlimited Purchase Approval" AND ("Purchase Amount Approval Limit" <> 0) THEN
                                                                  ERROR(Text003,FIELDCAPTION("Purchase Amount Approval Limit"),FIELDCAPTION("Unlimited Purchase Approval"));
                                                                IF "Purchase Amount Approval Limit" < 0 THEN
                                                                  ERROR(Text005);
                                                              END;

                                                   CaptionML=[ENU=Purchase Amount Approval Limit;
                                                              PTG=Limite Aprova��o Valor Compra];
                                                   BlankZero=Yes }
    { 14  ;   ;Unlimited Sales Approval;Boolean   ;OnValidate=BEGIN
                                                                IF "Unlimited Sales Approval" THEN
                                                                  "Sales Amount Approval Limit" := 0;
                                                              END;

                                                   CaptionML=[ENU=Unlimited Sales Approval;
                                                              PTG=Aprova��o Vendas Ilimitada] }
    { 15  ;   ;Unlimited Purchase Approval;Boolean;OnValidate=BEGIN
                                                                IF "Unlimited Purchase Approval" THEN
                                                                  "Purchase Amount Approval Limit" := 0;
                                                              END;

                                                   CaptionML=[ENU=Unlimited Purchase Approval;
                                                              PTG=Aprova��o Compra Ilimitada] }
    { 16  ;   ;Substitute          ;Code50        ;TableRelation="User Setup"."User ID";
                                                   OnValidate=BEGIN
                                                                IF Substitute = "User ID" THEN
                                                                  FIELDERROR(Substitute);
                                                              END;

                                                   OnLookup=VAR
                                                              UserSetup@1000 : Record 91;
                                                            BEGIN
                                                              UserSetup.SETFILTER("User ID",'<>%1',"User ID");
                                                              IF PAGE.RUNMODAL(PAGE::"Approval User Setup",UserSetup) = ACTION::LookupOK THEN
                                                                VALIDATE(Substitute,UserSetup."User ID");
                                                            END;

                                                   CaptionML=[ENU=Substitute;
                                                              PTG=Substituir] }
    { 17  ;   ;E-Mail              ;Text100       ;ExtendedDatatype=E-Mail;
                                                   CaptionML=[ENU=Email;
                                                              PTG=Email] }
    { 19  ;   ;Request Amount Approval Limit;Integer;
                                                   OnValidate=BEGIN
                                                                IF "Unlimited Request Approval" AND ("Request Amount Approval Limit" <> 0) THEN
                                                                  ERROR(Text003,FIELDCAPTION("Request Amount Approval Limit"),FIELDCAPTION("Unlimited Request Approval"));
                                                                IF "Request Amount Approval Limit" < 0 THEN
                                                                  ERROR(Text005);
                                                              END;

                                                   CaptionML=[ENU=Request Amount Approval Limit;
                                                              PTG=Limite Aprova��o Valor Pedido];
                                                   BlankZero=Yes }
    { 20  ;   ;Unlimited Request Approval;Boolean ;OnValidate=BEGIN
                                                                IF "Unlimited Request Approval" THEN
                                                                  "Request Amount Approval Limit" := 0;
                                                              END;

                                                   CaptionML=[ENU=Unlimited Request Approval;
                                                              PTG=Aprova��o Pedido Ilimitada] }
    { 21  ;   ;Approval Administrator;Boolean     ;OnValidate=VAR
                                                                UserSetup@1000 : Record 91;
                                                              BEGIN
                                                                IF "Approval Administrator" THEN BEGIN
                                                                  UserSetup.SETRANGE("Approval Administrator",TRUE);
                                                                  IF NOT UserSetup.ISEMPTY THEN
                                                                    FIELDERROR("Approval Administrator");
                                                                END;
                                                              END;

                                                   CaptionML=[ENU=Approval Administrator;
                                                              PTG=Aprova��o Administrador] }
    { 31  ;   ;License Type        ;Option        ;FieldClass=FlowField;
                                                   CalcFormula=Lookup(User."License Type" WHERE (User Name=FIELD(User ID)));
                                                   CaptionML=[ENU=License Type;
                                                              PTG=Tipo Licen�a];
                                                   OptionCaptionML=[ENU=Full User,Limited User,Device Only User,Windows Group,External User;
                                                                    PTG=Full User,Limited User,Device Only User,Windows Group,External User];
                                                   OptionString=Full User,Limited User,Device Only User,Windows Group,External User }
    { 950 ;   ;Time Sheet Admin.   ;Boolean       ;CaptionML=[ENU=Time Sheet Admin.;
                                                              PTG=Admin. Folha Horas] }
    { 5600;   ;Allow FA Posting From;Date         ;CaptionML=[ENU=Allow FA Posting From;
                                                              PTG=Permite Reg. Imob. De] }
    { 5601;   ;Allow FA Posting To ;Date          ;CaptionML=[ENU=Allow FA Posting To;
                                                              PTG=Permite Reg. Imob. At�] }
    { 5700;   ;Sales Resp. Ctr. Filter;Code10     ;TableRelation="Responsibility Center".Code;
                                                   CaptionML=[ENU=Sales Resp. Ctr. Filter;
                                                              PTG=Filtro Cent. Resp. Venda] }
    { 5701;   ;Purchase Resp. Ctr. Filter;Code10  ;TableRelation="Responsibility Center";
                                                   CaptionML=[ENU=Purchase Resp. Ctr. Filter;
                                                              PTG=Filtro Cent. Resp. Compra] }
    { 5900;   ;Service Resp. Ctr. Filter;Code10   ;TableRelation="Responsibility Center";
                                                   CaptionML=[ENU=Service Resp. Ctr. Filter;
                                                              PTG=Filtro Cent. Resp. Servi�o] }
    { 31022911;;AT User ID         ;Code20        ;CaptionML=[ENU=AT User ID;
                                                              PTG=ID Utilizador AT];
                                                   Description=soft }
    { 31022912;;AT Password PF     ;Text50        ;ExtendedDatatype=Masked;
                                                   CaptionML=[ENU=AT Password PF;
                                                              PTG=Password PF AT];
                                                   Description=soft }
    { 31022913;;Sales Series Group ;Code10        ;TableRelation="Series Groups" WHERE (Type=CONST(Sales));
                                                   CaptionML=[ENU=Sales Series Group;
                                                              PTG=Sequ�ncia S�ries Venda];
                                                   Description=soft }
    { 31022914;;Purchase Series Group;Code10      ;TableRelation="Series Groups" WHERE (Type=CONST(Purchase));
                                                   CaptionML=[ENU=Purchase Series Group;
                                                              PTG=Sequ�ncia S�ries Compra];
                                                   Description=soft }
    { 31022915;;Service Series Group;Code10       ;TableRelation="Series Groups" WHERE (Type=CONST(Services));
                                                   CaptionML=[ENU=Service Series Group;
                                                              PTG=Sequ�ncia S�ries Servi�o];
                                                   Description=soft }
  }
  KEYS
  {
    {    ;User ID                                 ;Clustered=Yes }
    {    ;Salespers./Purch. Code                   }
  }
  FIELDGROUPS
  {
  }
  CODE
  {
    VAR
      Text001@1000 : TextConst 'ENU=The %1 Salesperson/Purchaser code is already assigned to another User ID %2.;PTG=O c�digo de Vendedor/Comprador %1 j� est� atribu�do a outro ID Utilizador %2.';
      Text003@1002 : TextConst 'ENU="You cannot have both a %1 and %2. ";PTG=N�o � poss�vel ter %1 e %2.';
      Text005@1004 : TextConst 'ENU=You cannot have approval limits less than zero.;PTG=N�o � poss�vel ter limites de aprova��o inferiores a zero.';
      "///sgg.text"@31022890 : TextConst;
      errNotAuthorized@31022893 : TextConst 'PTG=N�o est� autorizado a modificar a lista de utilizadores autorizados.';
      txtSuperUserGroup@31022892 : TextConst 'PTG=SUPER';

    PROCEDURE CreateApprovalUserSetup@3(User@1000 : Record 2000000120);
    VAR
      UserSetup@1001 : Record 91;
      ApprovalUserSetup@1002 : Record 91;
    BEGIN
      ApprovalUserSetup.INIT;
      ApprovalUserSetup.VALIDATE("User ID",User."User Name");
      ApprovalUserSetup.VALIDATE("Sales Amount Approval Limit",GetDefaultSalesAmountApprovalLimit);
      ApprovalUserSetup.VALIDATE("Purchase Amount Approval Limit",GetDefaultPurchaseAmountApprovalLimit);
      ApprovalUserSetup.VALIDATE("E-Mail",User."Contact Email");
      UserSetup.SETRANGE("Sales Amount Approval Limit",UserSetup.GetDefaultSalesAmountApprovalLimit);
      IF UserSetup.FINDFIRST THEN
        ApprovalUserSetup.VALIDATE("Approver ID",UserSetup."Approver ID");
      IF ApprovalUserSetup.INSERT THEN;
    END;

    PROCEDURE GetDefaultSalesAmountApprovalLimit@1() : Integer;
    VAR
      UserSetup@1001 : Record 91;
      DefaultApprovalLimit@1000 : Integer;
      LimitedApprovers@1002 : Integer;
    BEGIN
      UserSetup.SETRANGE("Unlimited Sales Approval",FALSE);

      IF UserSetup.FINDFIRST THEN BEGIN
        DefaultApprovalLimit := UserSetup."Sales Amount Approval Limit";
        LimitedApprovers := UserSetup.COUNT;
        UserSetup.SETRANGE("Sales Amount Approval Limit",DefaultApprovalLimit);
        IF LimitedApprovers = UserSetup.COUNT THEN
          EXIT(DefaultApprovalLimit);
      END;

      // Return 0 if no user setup exists or no default value is found
      EXIT(0);
    END;

    PROCEDURE GetDefaultPurchaseAmountApprovalLimit@2() : Integer;
    VAR
      UserSetup@1002 : Record 91;
      DefaultApprovalLimit@1001 : Integer;
      LimitedApprovers@1000 : Integer;
    BEGIN
      UserSetup.SETRANGE("Unlimited Purchase Approval",FALSE);

      IF UserSetup.FINDFIRST THEN BEGIN
        DefaultApprovalLimit := UserSetup."Purchase Amount Approval Limit";
        LimitedApprovers := UserSetup.COUNT;
        UserSetup.SETRANGE("Purchase Amount Approval Limit",DefaultApprovalLimit);
        IF LimitedApprovers = UserSetup.COUNT THEN
          EXIT(DefaultApprovalLimit);
      END;

      // Return 0 if no user setup exists or no default value is found
      EXIT(0);
    END;

    PROCEDURE HideExternalUsers@5();
    VAR
      PermissionManager@1001 : Codeunit 9002;
      OriginalFilterGroup@1000 : Integer;
    BEGIN
      IF NOT PermissionManager.SoftwareAsAService THEN
        EXIT;

      OriginalFilterGroup := FILTERGROUP;
      FILTERGROUP := 2;
      CALCFIELDS("License Type");
      SETFILTER("License Type",'<>%1',"License Type"::"External User");
      FILTERGROUP := OriginalFilterGroup;
    END;

    LOCAL PROCEDURE "//sgg.function"@31022890();
    BEGIN
    END;

    PROCEDURE IsMemberOfGroup@1000000000(pGroup@1000000000 : Code[50];pUser@1000000001 : Code[50];pCompanyName@1000000002 : Text[250]) : Boolean;
    VAR
      recAccessControl@1000000003 : Record 2000000053;
    BEGIN

      recAccessControl.SETRANGE("User Name", pUser);
      recAccessControl.SETRANGE("Role ID", pGroup);
      recAccessControl.SETRANGE("Company Name", '');
      IF recAccessControl.FINDFIRST() THEN
         EXIT(TRUE);

      recAccessControl.SETRANGE("User Name", pUser);
      recAccessControl.SETRANGE("Role ID", pGroup);
      recAccessControl.SETRANGE("Company Name", pCompanyName);
      IF recAccessControl.FINDFIRST() THEN
         EXIT(TRUE);

      EXIT(FALSE);
    END;

    PROCEDURE IsOwner@1000000001(pUserName@1000000000 : Code[50];pCompanyName@1000000001 : Text[250]) : Boolean;
    VAR
      recCompanyInfo@1000000002 : Record 79;
    BEGIN
      recCompanyInfo.CHANGECOMPANY(pCompanyName);
      recCompanyInfo.GET;
      EXIT (recCompanyInfo."Company Owner User ID" = pUserName);
    END;

    PROCEDURE canLogin@1000000003(pUserName@1000000000 : Code[50];pCompanyName@1000000001 : Text[250]) : Boolean;
    VAR
      recCompanyInfo@1000000002 : Record 79;
    BEGIN
      CHANGECOMPANY(pCompanyName);
      IF GET(pUserName) THEN
        EXIT (TRUE);
      IF IsOwner(pUserName, pCompanyName) THEN
        EXIT (TRUE);
      IF IsOwner('', pCompanyName) THEN
        EXIT (TRUE);
      IF IsMemberOfGroup(txtSuperUserGroup, pUserName, pCompanyName) THEN
        EXIT (TRUE);

      EXIT(FALSE);
    END;

    BEGIN
    {
      DTT.SS01.00 JOMARTINS 04-03-16 Acrescentadas novas fun��es IsMemberOfGroup, IsOwner, canLogin
    }
    END.
  }
}

