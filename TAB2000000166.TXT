OBJECT Table 2000000166 Tenant Permission
{
  OBJECT-PROPERTIES
  {
    Date=13/04/17;
    Time=14:36:28;
    Version List=NAVW110.0;
  }
  PROPERTIES
  {
    DataPerCompany=No;
    CaptionML=[ENU=Tenant Permission;
               PTG=Permiss�o Tenant];
  }
  FIELDS
  {
    { 1   ;   ;App ID              ;GUID          ;CaptionML=[ENU=App ID;
                                                              PTG=ID App] }
    { 2   ;   ;Role ID             ;Code20        ;TableRelation="Tenant Permission Set"."Role ID" WHERE (App ID=FIELD(App ID));
                                                   CaptionML=[ENU=Role ID;
                                                              PTG=ID Perfil] }
    { 3   ;   ;Role Name           ;Text30        ;FieldClass=FlowField;
                                                   CalcFormula=Lookup("Tenant Permission Set".Name WHERE (App ID=FIELD(App ID),
                                                                                                          Role ID=FIELD(Role ID)));
                                                   CaptionML=[ENU=Role Name;
                                                              PTG=Nome Perfil] }
    { 4   ;   ;Object Type         ;Option        ;CaptionML=[ENU=Object Type;
                                                              PTG=Tipo Objeto];
                                                   OptionCaptionML=[ENU=Table Data,Table,,Report,,Codeunit,XMLport,MenuSuite,Page,Query,System;
                                                                    PTG=Dados Tabela,Tabela,,Mapa,,Codeunit,XMLport,MenuSuite,Page,Query,Sistema];
                                                   OptionString=Table Data,Table,,Report,,Codeunit,XMLport,MenuSuite,Page,Query,System }
    { 5   ;   ;Object ID           ;Integer       ;TableRelation=IF (Object Type=CONST(Table Data)) AllObjWithCaption."Object ID" WHERE (Object Type=CONST(Table))
                                                                 ELSE IF (Object Type=CONST(Table)) AllObjWithCaption."Object ID" WHERE (Object Type=CONST(Table))
                                                                 ELSE IF (Object Type=CONST(Report)) AllObjWithCaption."Object ID" WHERE (Object Type=CONST(Report))
                                                                 ELSE IF (Object Type=CONST(Codeunit)) AllObjWithCaption."Object ID" WHERE (Object Type=CONST(Codeunit))
                                                                 ELSE IF (Object Type=CONST(XMLport)) AllObjWithCaption."Object ID" WHERE (Object Type=CONST(XMLport))
                                                                 ELSE IF (Object Type=CONST(MenuSuite)) AllObjWithCaption."Object ID" WHERE (Object Type=CONST(MenuSuite))
                                                                 ELSE IF (Object Type=CONST(Page)) AllObjWithCaption."Object ID" WHERE (Object Type=CONST(Page))
                                                                 ELSE IF (Object Type=CONST(Query)) AllObjWithCaption."Object ID" WHERE (Object Type=CONST(Query))
                                                                 ELSE IF (Object Type=CONST(System)) AllObjWithCaption."Object ID" WHERE (Object Type=CONST(System));
                                                   CaptionML=[ENU=Object ID;
                                                              PTG=ID Objeto] }
    { 6   ;   ;Object Name         ;Text249       ;FieldClass=FlowField;
                                                   CalcFormula=Lookup(AllObjWithCaption."Object Caption" WHERE (Object Type=FIELD(Object Type),
                                                                                                                Object ID=FIELD(Object ID)));
                                                   CaptionML=[ENU=Object Name;
                                                              PTG=Nome Objeto] }
    { 7   ;   ;Read Permission     ;Option        ;InitValue=Yes;
                                                   CaptionML=[ENU=Read Permission;
                                                              PTG=Permiss�o Ler];
                                                   OptionCaptionML=[ENU=" ,Yes,Indirect";
                                                                    PTG=" ,Sim,Indireto"];
                                                   OptionString=[ ,Yes,Indirect] }
    { 8   ;   ;Insert Permission   ;Option        ;InitValue=Yes;
                                                   CaptionML=[ENU=Insert Permission;
                                                              PTG=Permiss�o Inserir];
                                                   OptionCaptionML=[ENU=" ,Yes,Indirect";
                                                                    PTG=" ,Sim,Indireto"];
                                                   OptionString=[ ,Yes,Indirect] }
    { 9   ;   ;Modify Permission   ;Option        ;InitValue=Yes;
                                                   CaptionML=[ENU=Modify Permission;
                                                              PTG=Permiss�o Modificar];
                                                   OptionCaptionML=[ENU=" ,Yes,Indirect";
                                                                    PTG=" ,Sim,Indireto"];
                                                   OptionString=[ ,Yes,Indirect] }
    { 10  ;   ;Delete Permission   ;Option        ;InitValue=Yes;
                                                   CaptionML=[ENU=Delete Permission;
                                                              PTG=Permiss�o Apagar];
                                                   OptionCaptionML=[ENU=" ,Yes,Indirect";
                                                                    PTG=" ,Sim,Indireto"];
                                                   OptionString=[ ,Yes,Indirect] }
    { 11  ;   ;Execute Permission  ;Option        ;InitValue=Yes;
                                                   CaptionML=[ENU=Execute Permission;
                                                              PTG=Permiss�o Executar];
                                                   OptionCaptionML=[ENU=" ,Yes,Indirect";
                                                                    PTG=" ,Sim,Indireto"];
                                                   OptionString=[ ,Yes,Indirect] }
    { 12  ;   ;Security Filter     ;TableFilter   ;TableIDExpr="Object ID";
                                                   CaptionML=[ENU=Security Filter;
                                                              PTG=Filtro Seguran�a] }
  }
  KEYS
  {
    {    ;App ID,Role ID,Object Type,Object ID    ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    END.
  }
}

