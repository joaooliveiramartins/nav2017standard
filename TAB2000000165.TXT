OBJECT Table 2000000165 Tenant Permission Set
{
  OBJECT-PROPERTIES
  {
    Date=13/04/17;
    Time=14:36:28;
    Version List=NAVW19.00;
  }
  PROPERTIES
  {
    DataPerCompany=No;
    CaptionML=[ENU=Tenant Permission Set;
               PTG=Conjunto Permissäes Tenant];
  }
  FIELDS
  {
    { 1   ;   ;App ID              ;GUID          ;CaptionML=[ENU=App ID;
                                                              PTG=ID App] }
    { 2   ;   ;Role ID             ;Code20        ;CaptionML=[ENU=Role ID;
                                                              PTG=ID Perfil] }
    { 3   ;   ;Name                ;Text30        ;CaptionML=[ENU=Name;
                                                              PTG=Nome] }
  }
  KEYS
  {
    {    ;App ID,Role ID                          ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    END.
  }
}

