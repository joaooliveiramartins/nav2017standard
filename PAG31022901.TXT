OBJECT Page 31022901 Third-party company-Intrastat
{
  OBJECT-PROPERTIES
  {
    Date=01/02/15;
    Time=13:00:00;
    Version List=NAVPTSS81.00;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Third-party company-Intrastat;
               PTG=Intrastat-Terceiro declarante];
    SourceTable=Table31022900;
    PageType=List;
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1110000;1;Group     ;
                GroupType=Repeater }

    { 1110001;2;Field     ;
                SourceExpr="No." }

    { 1110003;2;Field     ;
                SourceExpr=Name }

    { 1110005;2;Field     ;
                SourceExpr=Address }

    { 1110024;2;Field     ;
                SourceExpr="Address 2" }

    { 1110007;2;Field     ;
                SourceExpr="Post code" }

    { 1110009;2;Field     ;
                SourceExpr=City }

    { 1110011;2;Field     ;
                SourceExpr="Phone No." }

    { 1110013;2;Field     ;
                SourceExpr="Fax No." }

    { 1110015;2;Field     ;
                SourceExpr="E-mail" }

    { 1110017;2;Field     ;
                SourceExpr="Company contact name" }

    { 1110019;2;Field     ;
                SourceExpr="VAT Registration No." }

  }
  CODE
  {

    BEGIN
    END.
  }
}

