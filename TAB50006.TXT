OBJECT Table 50006 Accruals and Deferrals
{
  OBJECT-PROPERTIES
  {
    Date=30/10/15;
    Time=11:25:14;
    Modified=Yes;
    Version List=SGG12.04;
  }
  PROPERTIES
  {
    OnInsert=BEGIN
               //-----comentado em 01/02/2002 Os Campos Obrigat�rios passaram a ser outros----------------------------
               //IF ("Natureza movimento"="Natureza movimento"::"Acr�scimos custos") OR
               //   ("Natureza movimento"="Natureza movimento"::"Acr�scimos proveitos") THEN
               //      BEGIN
               //         IF "N� documento"='' THEN
               //              ERROR('N� documento tem que estar preenchido em acr�scimos.');
               //      END;
               //-----------------------------------------------------------------------------------------------------

                 TESTFIELD("Document Type");

                 IF "Entry Type" = 0 THEN
                   ERROR(Text50000,FIELDCAPTION("Entry Type"));

                 TESTFIELD("Pre-Signed No.");

                 IF "Document No." = '' THEN
                   ERROR(Text50000,FIELDCAPTION("Document No."));

                 IF "Begin Date" = 0D THEN
                   ERROR(Text50000,FIELDCAPTION("Begin Date"));

               //  TESTFIELD("N� meses");                                               //D3PDS002.o

               //D3PDS002.sn
                 IF "End Date" = 0D THEN
                   ERROR(Text001, "End Date");

                 IF "End Date" <= "Begin Date" THEN
                   ERROR(Text002,FIELDCAPTION("End Date"),
                                 "End Date",
                                 FIELDCAPTION("Begin Date"),
                                 "Begin Date");

               //D3PDS002.en

                 IF Amount <= 0 THEN
                   ERROR(Text50001,Amount);

                 IF "Gen. Ledger Acc. to Debit" = '' THEN
                   ERROR(Text50000,FIELDCAPTION("Gen. Ledger Acc. to Debit"));

                 IF "Gen. Ledger Acc. to Credit" = '' THEN
                   ERROR(Text50000,FIELDCAPTION("Gen. Ledger Acc. to Credit"));
             END;

    OnModify=BEGIN
               //-----comentado em 01/02/2002 Os Campos Obrigat�rios passaram a ser outros----------------------------
               //IF ("Natureza movimento"="Natureza movimento"::"Acr�scimos custos") OR
               //   ("Natureza movimento"="Natureza movimento"::"Acr�scimos proveitos") THEN
               //      BEGIN
               //         IF "N� documento"='' THEN
               //              ERROR('N� documento tem que estar preenchido em acr�scimos.');
               //      END;
               //-----------------------------------------------------------------------------------------------------
                 TESTFIELD("Document Type");

                 IF "Entry Type" = 0 THEN
                   ERROR(Text50000,FIELDCAPTION("Entry Type"));

                 TESTFIELD("Pre-Signed No.");

                 IF "Document No." = '' THEN
                   ERROR(Text50000,FIELDCAPTION("Document No."));

                 IF "Begin Date" = 0D THEN
                   ERROR(Text50000,FIELDCAPTION("Begin Date"));

               //  TESTFIELD("N� meses");                                        //D3PDS002.o

               //D3PDS002.sn
                 IF "End Date" = 0D THEN
                   ERROR(Text001, "End Date");

                 IF "End Date" <= "Begin Date" THEN
                   ERROR(Text002,FIELDCAPTION("End Date"),
                                 "End Date",
                                 FIELDCAPTION("Begin Date"),
                                 "Begin Date");

               //D3PDS002.en

                 IF Amount <= 0 THEN
                   ERROR(Text50001,Amount);

                 IF "Gen. Ledger Acc. to Debit" = '' THEN
                   ERROR(Text50000,FIELDCAPTION("Gen. Ledger Acc. to Debit"));

                 IF "Gen. Ledger Acc. to Credit" = '' THEN
                   ERROR(Text50000,FIELDCAPTION("Gen. Ledger Acc. to Credit"));

               ValidateShortcutDimCode(1,"Global Dimension 1 to Debit", FALSE);
               ValidateShortcutDimCode(2,"Global Dimension 2 to Debit", FALSE);
               ValidateShortcutDimCode(1,"Global Dimension 1 to Credit", TRUE);
               ValidateShortcutDimCode(2,"Global Dimension 2 to Credit", TRUE);
             END;

    OnDelete=BEGIN
               {
               rec357.SETRANGE(rec357."Table ID", DATABASE::"Accruals and Deferrals");
               rec357.SETRANGE(rec357."Document Type", "Document Type");
               rec357.SETRANGE(rec357."Document No.", "Pre-Signed No.");
               rec357.DELETEALL
               }
             END;

    OnRename=BEGIN
               //-----comentado em 01/02/2002 Os Campos Obrigat�rios passaram a ser outros----------------------------
               //IF ("Natureza movimento"="Natureza movimento"::"Acr�scimos custos") OR
               //   ("Natureza movimento"="Natureza movimento"::"Acr�scimos proveitos") THEN
               //      BEGIN
               //         IF "N� documento"='' THEN
               //              ERROR('N� documento tem que estar preenchido em acr�scimos.');
               //      END;
               //-----------------------------------------------------------------------------------------------------

                 IF "Entry Type" = 0 THEN
                   ERROR(Text50000,FIELDCAPTION("Entry Type"));

                 IF "Document No." = '' THEN
                   ERROR(Text50000,FIELDCAPTION("Document No."));

                 IF Amount <= 0 THEN
                   ERROR(Text50001,Amount);

                 IF "Begin Date" = 0D THEN
                   ERROR(Text50000,FIELDCAPTION("Begin Date"));

               //D3PDS002.sn
                 IF "End Date" = 0D THEN
                   ERROR(Text001, "End Date");

                 IF "End Date" <= "Begin Date" THEN
                   ERROR(Text002,FIELDCAPTION("End Date"),
                                 "End Date",
                                 FIELDCAPTION("Begin Date"),
                                 "Begin Date");

               //D3PDS002.en

                 IF "Gen. Ledger Acc. to Debit" = '' THEN
                   ERROR(Text50000,FIELDCAPTION("Gen. Ledger Acc. to Debit"));

                 IF "Gen. Ledger Acc. to Credit" = '' THEN
                   ERROR(Text50000,FIELDCAPTION("Gen. Ledger Acc. to Credit"));
             END;

    CaptionML=[ENU=Accruals and Deferrals;
               PTG=Acr�scimos e Diferimentos];
  }
  FIELDS
  {
    { 1   ;   ;Entry Type          ;Option        ;CaptionML=[ENU=Entry Type;
                                                              PTG=Natureza Movimento];
                                                   OptionCaptionML=[ENU=,Accrued Income,Deferred Costs,Accrued Costs,Deferred Income;
                                                                    PTG=,Acr�scimos Proveitos,Diferimento Custos,Acr�scimos Custos,Diferimento Proveitos];
                                                   OptionString=,Accrued Income,Deferred Costs,Accrued Costs,Deferred Income;
                                                   BlankNumbers=BlankZero }
    { 2   ;   ;Description         ;Text50        ;CaptionML=[ENU=Description;
                                                              PTG=Descri��o] }
    { 3   ;   ;Document No.        ;Code20        ;CaptionML=[ENU=Document No.;
                                                              PTG=N� Documento] }
    { 4   ;   ;Amount              ;Decimal       ;CaptionML=[ENU=Amount;
                                                              PTG=Valor] }
    { 5   ;   ;Begin Date          ;Date          ;CaptionML=[ENU=Begin Date;
                                                              PTG=Data In�cio] }
    { 6   ;   ;Month No.           ;Integer       ;CaptionML=[ENU=Month No.;
                                                              PTG=N� Meses];
                                                   MinValue=1;
                                                   Editable=No }
    { 7   ;   ;Gen. Ledger Acc. to Debit;Code20   ;TableRelation="G/L Account";
                                                   OnValidate=VAR
                                                                "---SGG12----"@1000000000 : Integer;
                                                                GLAcc@1000000001 : Record 15;
                                                              BEGIN
                                                                //SGG12.03 BEGIN
                                                                IF "Gen. Ledger Acc. to Debit" <> '' THEN
                                                                  IF GLAcc.GET("Gen. Ledger Acc. to Debit") THEN
                                                                    GLAcc.TESTFIELD("Account Type",GLAcc."Account Type"::Posting);
                                                                //SGG12.03 END
                                                              END;

                                                   CaptionML=[ENU=Gen. Ledger Acc. to Debit;
                                                              PTG=Cta. Plano Geral a Debitar] }
    { 8   ;   ;Global Dimension 1 to Debit;Code20 ;TableRelation="Dimension Value".Code WHERE (Global Dimension No.=CONST(1));
                                                   OnValidate=BEGIN
                                                                ValidateShortcutDimCode(1,"Global Dimension 1 to Debit", FALSE);
                                                              END;

                                                   CaptionML=[ENU=Global Dimension 1 to Debit;
                                                              PTG=C�d. Departamento a Debitar];
                                                   Description=SGG12.02 }
    { 9   ;   ;Global Dimension 2 to Debit;Code20 ;TableRelation="Dimension Value".Code WHERE (Global Dimension No.=CONST(2));
                                                   OnValidate=BEGIN
                                                                ValidateShortcutDimCode(2,"Global Dimension 2 to Debit", FALSE);
                                                              END;

                                                   CaptionML=[ENU=Global Dimension 2 to Debit;
                                                              PTG=C�d. Programa a Debitar];
                                                   Description=SGG12.02 }
    { 10  ;   ;Gen. Ledger Acc. to Credit;Code20  ;TableRelation="G/L Account";
                                                   OnValidate=VAR
                                                                "---SGG12----"@1000000001 : Integer;
                                                                GLAcc@1000000000 : Record 15;
                                                              BEGIN
                                                                //SGG12.03 BEGIN
                                                                IF "Gen. Ledger Acc. to Credit" <> '' THEN
                                                                  IF GLAcc.GET("Gen. Ledger Acc. to Credit") THEN
                                                                    GLAcc.TESTFIELD("Account Type",GLAcc."Account Type"::Posting);
                                                                //SGG12.03 END
                                                              END;

                                                   CaptionML=[ENU=Gen. Ledger Acc. to Credit;
                                                              PTG=Cta. Plano Geral a Creditar] }
    { 11  ;   ;Global Dimension 1 to Credit;Code20;TableRelation="Dimension Value".Code WHERE (Global Dimension No.=CONST(1));
                                                   OnValidate=BEGIN
                                                                ValidateShortcutDimCode(1,"Global Dimension 1 to Credit", TRUE);
                                                              END;

                                                   CaptionML=[ENU=Global Dimension 1 to Credit;
                                                              PTG=C�d. Departamento a Creditar];
                                                   Description=SGG12.02 }
    { 12  ;   ;Global Dimension 2 to Credit;Code20;TableRelation="Dimension Value".Code WHERE (Global Dimension No.=CONST(2));
                                                   OnValidate=BEGIN
                                                                ValidateShortcutDimCode(2,"Global Dimension 2 to Credit", TRUE);
                                                              END;

                                                   CaptionML=[ENU=Global Dimension 2 to Credit;
                                                              PTG=C�d. Programa a Creditar];
                                                   Description=SGG12.02 }
    { 13  ;   ;Document Type       ;Option        ;CaptionML=[ENU=Document Type;
                                                              PTG=Tipo Documento];
                                                   OptionCaptionML=[ENU=" ,Invoice,Credit Memo";
                                                                    PTG=" ,Factura,Nota cr�dito"];
                                                   OptionString=[ ,Invoice,Credit Memo] }
    { 14  ;   ;Pre-Signed No.      ;Code20        ;CaptionML=[ENU=Pre-Signed No.;
                                                              PTG=N� Pr�-Assinado] }
    { 15  ;   ;Document Date       ;Date          ;CaptionML=[ENU=Document Date;
                                                              PTG=Data Documento] }
    { 16  ;   ;DRF Code to Debit   ;Code10        ;TableRelation=Table52076149;
                                                   CaptionML=[ENU=DRF Code to Debit;
                                                              PTG=C�d. DRF a Debitar] }
    { 17  ;   ;DRF Code to Credit  ;Code10        ;TableRelation=Table52076149;
                                                   CaptionML=[ENU=DRF Code to Credit;
                                                              PTG=C�d. DRF a Creditar] }
    { 18  ;   ;Regular             ;Boolean       ;CaptionML=[ENU=Regular;
                                                              PTG=Regularizado] }
    { 19  ;   ;End Date            ;Date          ;CaptionML=[ENU=End Date;
                                                              PTG=Data Fim] }
    { 480 ;   ;Dimension Set ID Debit;Integer     ;TableRelation="Dimension Set Entry";
                                                   OnLookup=BEGIN
                                                              ShowDimensions(FALSE);
                                                            END;

                                                   CaptionML=[ENU=Dimension Set ID (Debit);
                                                              PTG=ID Conj. Dimens�es (D�bito)];
                                                   Editable=No }
    { 481 ;   ;Dimension Set ID Credit;Integer    ;TableRelation="Dimension Set Entry";
                                                   OnLookup=BEGIN
                                                              ShowDimensions(TRUE);
                                                            END;

                                                   CaptionML=[ENU=Dimension Set ID (Credit);
                                                              PTG=ID Conj. Dimens�es (Cr�dito)];
                                                   Editable=No }
  }
  KEYS
  {
    {    ;Document Type,Pre-Signed No.            ;Clustered=Yes }
    {    ;Entry Type,Gen. Ledger Acc. to Debit     }
    {    ;Entry Type,Gen. Ledger Acc. to Credit    }
    {    ;Entry Type,Amount,Regular                }
  }
  FIELDGROUPS
  {
  }
  CODE
  {
    VAR
      Text001@1000000000 : TextConst 'ENU=%1 is not valid.;PTG=A Data %1 � Inv�lida.';
      Text002@1000000001 : TextConst 'ENU=%1 (%2) is equal or less than %3 (%4).;PTG=%1 (%2) � menor ou igual a %3 (%4).';
      "---SGG12.00---"@1000000002 : TextConst;
      Text50000@1000000003 : TextConst 'ENU=%1 must be filled.;PTG=Preencha o campo %1.';
      Text50001@1000000004 : TextConst 'ENU=%1 must not be equal or less than 0.;PTG=%1 n�o pode ser igual ou inferior a 0.';
      DimMgt@1000000005 : Codeunit 408;
      GLSetup@1000000006 : Record 98;

    PROCEDURE CreateDim@13(Type1@1000 : Integer;No1@1001 : Code[20];Type2@1002 : Integer;No2@1003 : Code[20];Type3@1004 : Integer;No3@1005 : Code[20];Type4@1006 : Integer;No4@1007 : Code[20];Type5@1008 : Integer;No5@1009 : Code[20]);
    VAR
      TableID@1010 : ARRAY [10] OF Integer;
      No@1011 : ARRAY [10] OF Code[20];
    BEGIN
      {TableID[1] := Type1;
      No[1] := No1;
      TableID[2] := Type2;
      No[2] := No2;
      TableID[3] := Type3;
      No[3] := No3;
      TableID[4] := Type4;
      No[4] := No4;
      TableID[5] := Type5;
      No[5] := No5;
      "Shortcut Dimension 1 Code" := '';
      "Shortcut Dimension 2 Code" := '';
      "Dimension Set ID" :=
        DimMgt.GetDefaultDimID(
          TableID,No,"Source Code","Shortcut Dimension 1 Code","Shortcut Dimension 2 Code",0,0);
      }
    END;

    PROCEDURE ValidateShortcutDimCode@14(FieldNumber@1000 : Integer;VAR ShortcutDimCode@1001 : Code[20];pCredit@1000000000 : Boolean);
    BEGIN
      GLSetup.GET;
      IF pCredit THEN BEGIN
        DimMgt.ValidateShortcutDimValues(FieldNumber,ShortcutDimCode,"Dimension Set ID Credit");
      END ELSE BEGIN
        DimMgt.ValidateShortcutDimValues(FieldNumber,ShortcutDimCode,"Dimension Set ID Debit");
      END;
    END;

    PROCEDURE LookupShortcutDimCode@18(FieldNumber@1000 : Integer;VAR ShortcutDimCode@1001 : Code[20];pCredit@1000000000 : Boolean);
    BEGIN
      IF pCredit THEN BEGIN
        DimMgt.LookupDimValueCode(FieldNumber,ShortcutDimCode);
        DimMgt.ValidateShortcutDimValues(FieldNumber,ShortcutDimCode,"Dimension Set ID Credit");
      END ELSE BEGIN
        DimMgt.LookupDimValueCode(FieldNumber,ShortcutDimCode);
        DimMgt.ValidateShortcutDimValues(FieldNumber,ShortcutDimCode,"Dimension Set ID Debit");
      END;
    END;

    PROCEDURE ShowShortcutDimCode@15(VAR ShortcutDimCode@1000 : ARRAY [8] OF Code[20];pCredit@1000000000 : Boolean);
    BEGIN
      IF pCredit THEN BEGIN
        DimMgt.GetShortcutDimensions("Dimension Set ID Credit",ShortcutDimCode);
      END ELSE BEGIN
        DimMgt.GetShortcutDimensions("Dimension Set ID Debit",ShortcutDimCode);
      END;
    END;

    PROCEDURE ShowDimensions@26(pCredit@1000000000 : Boolean);
    BEGIN
      IF pCredit THEN BEGIN
        "Dimension Set ID Credit" :=
          DimMgt.EditDimensionSet2(
            "Dimension Set ID Credit",STRSUBSTNO('%1 %2 %3',"Document Type", "Pre-Signed No.", FIELDCAPTION("Dimension Set ID Credit")),
            "Global Dimension 1 to Credit", "Global Dimension 2 to Credit");

      END ELSE BEGIN
        "Dimension Set ID Debit" :=
          DimMgt.EditDimensionSet2(
            "Dimension Set ID Debit",STRSUBSTNO('%1 %2 %3',"Document Type", "Pre-Signed No.", FIELDCAPTION("Dimension Set ID Debit")),
            "Global Dimension 1 to Debit", "Global Dimension 2 to Debit");

      END;
    END;

    BEGIN
    {
      SGG12.00 NPEREIRA 06-09-2006 Acr�scimos e Diferimentos - correc��o de vari�veis, tabelas,...
      SGG12.01 NPEREIRA 18-10-2006 Acr�scimos e Diferimentos - correc��o de tabelas a ser chamadas em alguns campos
      SGG12.02 NPEREIRA 20-10-2006 Acr�scimos e Diferimentos - aumentar tamanho dos campos dimens�o
                                                               carregar o c�digo valor dimens�o para a tabela em vez do nome da dimens�o
      SGG12.03 NPEREIRA 21-11-2006 Acr�scimos e Diferimentos - validar inser��o de contas maior
      SGG12.04 NPEREIRA 29-01-2007 Acr�scimos e Diferimentos - nova chave para ser utilizada em rotinas de verifica��o
    }
    END.
  }
}

