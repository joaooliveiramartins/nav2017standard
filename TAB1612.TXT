OBJECT Table 1612 Office Admin. Credentials
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0;
  }
  PROPERTIES
  {
    Permissions=TableData 1612=r;
    OnInsert=BEGIN
               IF Endpoint = '' THEN
                 VALIDATE(Endpoint,DefaultEndpoint);
             END;

    OnModify=BEGIN
               IF Endpoint = '' THEN
                 VALIDATE(Endpoint,DefaultEndpoint);
             END;

    CaptionML=[ENU=Office Admin. Credentials;
               PTG=Credenciais Admin. Office];
  }
  FIELDS
  {
    { 1   ;   ;Primary Key         ;Code10        ;CaptionML=[ENU=Primary Key;
                                                              PTG=Chave Prim ria] }
    { 2   ;   ;Email               ;Text80        ;ExtendedDatatype=E-Mail;
                                                   CaptionML=[ENU=Email;
                                                              PTG=Email];
                                                   NotBlank=Yes }
    { 3   ;   ;Password            ;Text50        ;ExtendedDatatype=Masked;
                                                   CaptionML=[ENU=Password;
                                                              PTG=Password];
                                                   NotBlank=Yes }
    { 4   ;   ;Endpoint            ;Text250       ;CaptionML=[ENU=Endpoint;
                                                              PTG=Ponto Final] }
  }
  KEYS
  {
    {    ;Primary Key                             ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    PROCEDURE DefaultEndpoint@1() : Text[250];
    BEGIN
      EXIT('https://ps.outlook.com/powershell-liveid');
    END;

    BEGIN
    END.
  }
}

