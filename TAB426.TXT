OBJECT Table 426 IC Outbox Sales Header
{
  OBJECT-PROPERTIES
  {
    Date=09/09/14;
    Time=12:00:00;
    Version List=NAVW18.00;
  }
  PROPERTIES
  {
    OnDelete=VAR
               ICOutboxSalesLine@1000 : Record 427;
               DimMgt@1002 : Codeunit 408;
             BEGIN
               ICOutboxSalesLine.SETRANGE("IC Partner Code","IC Partner Code");
               ICOutboxSalesLine.SETRANGE("IC Transaction No.","IC Transaction No.");
               ICOutboxSalesLine.SETRANGE("Transaction Source","Transaction Source");
               IF ICOutboxSalesLine.FINDFIRST THEN
                 ICOutboxSalesLine.DELETEALL(TRUE);
               DimMgt.DeleteICDocDim(
                 DATABASE::"IC Outbox Sales Header","IC Transaction No.","IC Partner Code","Transaction Source",0);
             END;

    CaptionML=[ENU=IC Outbox Sales Header;
               PTG=Cabe�alho Vendas Envio IE];
  }
  FIELDS
  {
    { 1   ;   ;Document Type       ;Option        ;CaptionML=[ENU=Document Type;
                                                              PTG=Tipo Documento];
                                                   OptionCaptionML=[ENU=" ,Order,Invoice,Credit Memo,,Return Order";
                                                                    PTG=" ,Encomenda,Fatura,Nota Cr�dito,,Devolu��o"];
                                                   OptionString=[ ,Order,Invoice,Credit Memo,,Return Order];
                                                   Editable=No }
    { 2   ;   ;Sell-to Customer No.;Code20        ;TableRelation=Customer;
                                                   CaptionML=[ENU=Sell-to Customer No.;
                                                              PTG=Venda-a N� Cliente];
                                                   Editable=No }
    { 3   ;   ;No.                 ;Code20        ;CaptionML=[ENU=No.;
                                                              PTG=N�];
                                                   Editable=No }
    { 4   ;   ;Bill-to Customer No.;Code20        ;TableRelation=Customer;
                                                   CaptionML=[ENU=Bill-to Customer No.;
                                                              PTG=Fatura-a N� Cliente];
                                                   Editable=No }
    { 13  ;   ;Ship-to Name        ;Text50        ;CaptionML=[ENU=Ship-to Name;
                                                              PTG=Envio-a Nome];
                                                   Editable=No }
    { 15  ;   ;Ship-to Address     ;Text50        ;CaptionML=[ENU=Ship-to Address;
                                                              PTG=Envio-a Endere�o];
                                                   Editable=No }
    { 16  ;   ;Ship-to Address 2   ;Text50        ;CaptionML=[ENU=Ship-to Address 2;
                                                              PTG=Envio-a Endere�o 2];
                                                   Editable=No }
    { 17  ;   ;Ship-to City        ;Text30        ;CaptionML=[ENU=Ship-to City;
                                                              PTG=envio-a Cidade];
                                                   Editable=No }
    { 20  ;   ;Posting Date        ;Date          ;CaptionML=[ENU=Posting Date;
                                                              PTG=Data Registo] }
    { 24  ;   ;Due Date            ;Date          ;CaptionML=[ENU=Due Date;
                                                              PTG=Data Vencimento] }
    { 25  ;   ;Payment Discount %  ;Decimal       ;CaptionML=[ENU=Payment Discount %;
                                                              PTG=% Desconto P.P.];
                                                   Editable=No }
    { 26  ;   ;Pmt. Discount Date  ;Date          ;CaptionML=[ENU=Pmt. Discount Date;
                                                              PTG=Data Desconto P.P.];
                                                   Editable=No }
    { 32  ;   ;Currency Code       ;Code10        ;TableRelation=Currency;
                                                   TestTableRelation=No;
                                                   CaptionML=[ENU=Currency Code;
                                                              PTG=C�d. Divisa];
                                                   Editable=No }
    { 35  ;   ;Prices Including VAT;Boolean       ;CaptionML=[ENU=Prices Including VAT;
                                                              PTG=Pre�os Incluindo IVA] }
    { 44  ;   ;Order No.           ;Code20        ;CaptionML=[ENU=Order No.;
                                                              PTG=N� Encomenda] }
    { 91  ;   ;Ship-to Post Code   ;Code20        ;CaptionML=[ENU=Ship-to Post Code;
                                                              PTG=Envio-a C�d. Postal];
                                                   Editable=No }
    { 99  ;   ;Document Date       ;Date          ;CaptionML=[ENU=Document Date;
                                                              PTG=Data Documento] }
    { 100 ;   ;External Document No.;Code35       ;CaptionML=[ENU=External Document No.;
                                                              PTG=N� Documento Externo] }
    { 125 ;   ;IC Partner Code     ;Code20        ;TableRelation="IC Partner";
                                                   CaptionML=[ENU=IC Partner Code;
                                                              PTG=C�d. Parceiro IE];
                                                   Editable=No }
    { 201 ;   ;IC Transaction No.  ;Integer       ;CaptionML=[ENU=IC Transaction No.;
                                                              PTG=N� Transa��o IE];
                                                   Editable=No }
    { 202 ;   ;Transaction Source  ;Option        ;CaptionML=[ENU=Transaction Source;
                                                              PTG=Origem Transa��o];
                                                   OptionCaptionML=[ENU=Rejected by Current Company,Created by Current Company;
                                                                    PTG=Rejeitado por Empresa Atual,Criado por Empresa Atual];
                                                   OptionString=Rejected by Current Company,Created by Current Company;
                                                   Editable=No }
    { 5790;   ;Requested Delivery Date;Date       ;AccessByPermission=TableData 99000880=R;
                                                   CaptionML=[ENU=Requested Delivery Date;
                                                              PTG=Data Entrega Requerida];
                                                   Editable=No }
    { 5791;   ;Promised Delivery Date;Date        ;CaptionML=[ENU=Promised Delivery Date;
                                                              PTG=Data Entrega Prometida];
                                                   Editable=No }
  }
  KEYS
  {
    {    ;IC Transaction No.,IC Partner Code,Transaction Source;
                                                   Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    END.
  }
}

