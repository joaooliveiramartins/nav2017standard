OBJECT Page 206 Resource Journal Templates
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Resource Journal Templates;
               PTG=Livros Di�rio Recurso];
    SourceTable=Table206;
    PageType=List;
    ActionList=ACTIONS
    {
      { 1900000003;0 ;ActionContainer;
                      ActionContainerType=RelatedInformation }
      { 29      ;1   ;ActionGroup;
                      CaptionML=[ENU=Te&mplate;
                                 PTG=&Livro];
                      Image=Template }
      { 30      ;2   ;Action    ;
                      CaptionML=[ENU=Batches;
                                 PTG=Sec��es];
                      ToolTipML=ENU=Set up multiple resource journals for a specific template. You can use batches when you need multiple journals of a certain type.;
                      ApplicationArea=#Jobs;
                      RunObject=Page 272;
                      RunPageLink=Journal Template Name=FIELD(Name);
                      Image=Description }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 2   ;2   ;Field     ;
                ToolTipML=ENU=Specifies the name of this journal.;
                ApplicationArea=#Jobs;
                SourceExpr=Name }

    { 4   ;2   ;Field     ;
                ToolTipML=ENU=Specifies a description of the template for easy identification.;
                ApplicationArea=#Jobs;
                SourceExpr=Description }

    { 10  ;2   ;Field     ;
                ToolTipML=ENU=Specifies if this journal will contain recurring entries.;
                ApplicationArea=#Jobs;
                SourceExpr=Recurring }

    { 31  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the number series code used to assign document numbers to journal lines in this resource journal template.;
                ApplicationArea=#Jobs;
                SourceExpr="No. Series" }

    { 33  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the number series code used to assign document numbers to ledger entries that are posted from journals using this template.;
                ApplicationArea=#Jobs;
                SourceExpr="Posting No. Series" }

    { 21  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the source code that is linked to the journal template.;
                ApplicationArea=#Jobs;
                SourceExpr="Source Code" }

    { 8   ;2   ;Field     ;
                ToolTipML=ENU=Specifies a reason code that is inserted on the journal lines.;
                ApplicationArea=#Jobs;
                SourceExpr="Reason Code" }

    { 15  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the number of the page that you want to be displayed when the user selects this journal.;
                ApplicationArea=#Jobs;
                SourceExpr="Page ID";
                Visible=FALSE;
                LookupPageID=Objects }

    { 23  ;2   ;Field     ;
                DrillDown=No;
                ToolTipML=ENU=Specifies the name of the page you selected in the Page ID field.;
                ApplicationArea=#Jobs;
                SourceExpr="Page Caption";
                Visible=FALSE }

    { 17  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the test report that is printed when, on the Actions tab in the Posting group, you choose Test Report.;
                ApplicationArea=#Jobs;
                SourceExpr="Test Report ID";
                Visible=FALSE;
                LookupPageID=Objects }

    { 25  ;2   ;Field     ;
                DrillDown=No;
                ToolTipML=ENU=Specifies the name of the test report that you selected in the Test Report ID field.;
                ApplicationArea=#Jobs;
                SourceExpr="Test Report Caption";
                Visible=FALSE }

    { 19  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the posting report that you want associated with this journal.;
                ApplicationArea=#Jobs;
                SourceExpr="Posting Report ID";
                Visible=FALSE;
                LookupPageID=Objects }

    { 27  ;2   ;Field     ;
                DrillDown=No;
                ToolTipML=ENU=Specifies the name of the posting report you selected in the Posting Report ID field.;
                ApplicationArea=#Jobs;
                SourceExpr="Posting Report Caption";
                Visible=FALSE }

    { 6   ;2   ;Field     ;
                ToolTipML=ENU=Specifies that an updated posting report is printed each time entries in this journal are posted.;
                ApplicationArea=#Jobs;
                SourceExpr="Force Posting Report";
                Visible=FALSE }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {

    BEGIN
    END.
  }
}

