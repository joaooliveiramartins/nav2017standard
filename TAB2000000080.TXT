OBJECT Table 2000000080 Page Data Personalization
{
  OBJECT-PROPERTIES
  {
    Date=13/04/17;
    Time=14:36:26;
    Version List=NAVW17.10;
  }
  PROPERTIES
  {
    DataPerCompany=No;
    CaptionML=[ENU=Page Data Personalization;
               PTG=Personaliza��o Informa��o P�gina];
  }
  FIELDS
  {
    { 3   ;   ;User SID            ;GUID          ;TableRelation=User."User Security ID";
                                                   CaptionML=[ENU=User SID;
                                                              PTG=SID Utilizador] }
    { 6   ;   ;User ID             ;Code50        ;FieldClass=FlowField;
                                                   CalcFormula=Lookup(User."User Name" WHERE (User Security ID=FIELD(User SID)));
                                                   CaptionML=[ENU=User ID;
                                                              PTG=ID Utilizador] }
    { 9   ;   ;Object Type         ;Option        ;CaptionML=[ENU=Object Type;
                                                              PTG=Tipo Objeto];
                                                   OptionCaptionML=[ENU=,,,Report,,,XMLport,,Page;
                                                                    PTG=,,,Mapa,,,XMLport,,Page];
                                                   OptionString=,,,Report,,,XMLport,,Page }
    { 12  ;   ;Object ID           ;Integer       ;TableRelation=Object.ID WHERE (Type=FIELD(Object Type));
                                                   CaptionML=[ENU=Object ID;
                                                              PTG=ID Objeto] }
    { 15  ;   ;Date                ;Date          ;CaptionML=[ENU=Date;
                                                              PTG=Data] }
    { 18  ;   ;Time                ;Time          ;CaptionML=[ENU=Time;
                                                              PTG=Tempo] }
    { 21  ;   ;Personalization ID  ;Code40        ;CaptionML=[ENU=Personalization ID;
                                                              PTG=ID Personaliza��o] }
    { 24  ;   ;ValueName           ;Code40        ;CaptionML=[ENU=ValueName;
                                                              PTG=NomeValor] }
    { 27  ;   ;Value               ;BLOB          ;CaptionML=[ENU=Value;
                                                              PTG=Valor] }
  }
  KEYS
  {
    {    ;User SID,Object Type,Object ID,Personalization ID,ValueName;
                                                   Clustered=Yes }
    {    ;Date                                     }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    END.
  }
}

