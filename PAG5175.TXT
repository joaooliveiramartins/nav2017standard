OBJECT Page 5175 Sales Cycle Statistics FactBox
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Sales Cycle Statistics FactBox;
               PTG=FactBox Estatísticas Ciclo Vendas];
    InsertAllowed=No;
    DeleteAllowed=No;
    LinksAllowed=No;
    SourceTable=Table5090;
    PageType=ListPart;
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                CaptionML=[ENU=General;
                           PTG=Geral] }

    { 2   ;2   ;Field     ;
                ToolTipML=ENU=Specifies the number of opportunities that you have created using the sales cycle. This field is not editable.;
                ApplicationArea=#RelationshipMgmt;
                SourceExpr="No. of Opportunities" }

    { 4   ;2   ;Field     ;
                ToolTipML=ENU=Specifies the estimated value of all the open opportunities that you have assigned to the sales cycle. This field is not editable.;
                ApplicationArea=#RelationshipMgmt;
                SourceExpr="Estimated Value (LCY)" }

    { 6   ;2   ;Field     ;
                ToolTipML=ENU=Specifies the calculated current value of all the open opportunities that you have assigned to the sales cycle. This field is not editable.;
                ApplicationArea=#RelationshipMgmt;
                SourceExpr="Calcd. Current Value (LCY)" }

  }
  CODE
  {

    BEGIN
    END.
  }
}

