OBJECT Table 1281 Bank Data Conversion Pmt. Type
{
  OBJECT-PROPERTIES
  {
    Date=09/09/14;
    Time=12:00:00;
    Version List=NAVW18.00;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Bank Data Conversion Pmt. Type;
               PTG=Convers�o Tipo Pagamento Dados Banc�rios];
    LookupPageID=Page1281;
  }
  FIELDS
  {
    { 1   ;   ;Code                ;Text50        ;CaptionML=[ENU=Code;
                                                              PTG=C�digo] }
    { 2   ;   ;Description         ;Text80        ;CaptionML=[ENU=Description;
                                                              PTG=Descri��o] }
  }
  KEYS
  {
    {    ;Code                                    ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    END.
  }
}

