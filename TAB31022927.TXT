OBJECT Table 31022927 Municipalities
{
  OBJECT-PROPERTIES
  {
    Date=01/02/15;
    Time=13:00:00;
    Version List=NAVPTSS81.00;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Municipalities;
               PTG=Munic�pios];
    LookupPageID=Page31022938;
  }
  FIELDS
  {
    { 1   ;   ;Municipality        ;Code4         ;CaptionML=[ENU=Municipality;
                                                              PTG=Munic�pio] }
    { 2   ;   ;Description         ;Text30        ;CaptionML=[ENU=Description;
                                                              PTG=Descri��o] }
    { 3   ;   ;Salary Mass         ;Decimal       ;OnValidate=BEGIN
                                                                CalcProduct;
                                                              END;

                                                   CaptionML=[ENU=Salary Mass;
                                                              PTG=Massa Salarial];
                                                   MaxValue=9�999�999�999,99 }
    { 4   ;   ;Municipality Tax    ;Decimal       ;OnValidate=BEGIN
                                                                CalcProduct;
                                                              END;

                                                   CaptionML=[ENU=Municipality Tax;
                                                              PTG=Imposto Municipal];
                                                   DecimalPlaces=2:2;
                                                   MaxValue=99,99 }
    { 5   ;   ;Product             ;Decimal       ;CaptionML=[ENU=Product;
                                                              PTG=Produto];
                                                   MaxValue=9�999�999�999,99;
                                                   Editable=No }
    { 6   ;   ;Active              ;Boolean       ;CaptionML=[ENU=Active;
                                                              PTG=Ativo] }
  }
  KEYS
  {
    {    ;Municipality                            ;Clustered=Yes }
    {    ;Active                                   }
  }
  FIELDGROUPS
  {
    { 1   ;DropDown            ;Municipality,Description,Salary Mass,Municipality Tax,Product,Active }
  }
  CODE
  {

    PROCEDURE CalcProduct@1110001();
    BEGIN
      Product := ("Salary Mass" * "Municipality Tax") / 100;
    END;

    BEGIN
    END.
  }
}

