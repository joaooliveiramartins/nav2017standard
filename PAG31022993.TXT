OBJECT Page 31022993 Posted Payment Orders
{
  OBJECT-PROPERTIES
  {
    Date=12/04/17;
    Time=14:49:23;
    Modified=Yes;
    Version List=NAVPTSS10.00;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Posted Payment Orders;
               PTG=Ordens Pagamento Registadas];
    InsertAllowed=No;
    DeleteAllowed=No;
    SourceTable=Table31022953;
    DataCaptionExpr=Caption;
    PageType=Document;
    RefreshOnActivate=Yes;
    ActionList=ACTIONS
    {
      { 1900000003;0 ;ActionContainer;
                      ActionContainerType=RelatedInformation }
      { 35      ;1   ;ActionGroup;
                      CaptionML=[ENU=Payment O&rder;
                                 PTG=&Ordem Pagamento] }
      { 43      ;2   ;Action    ;
                      CaptionML=[ENU=Co&mments;
                                 PTG=Co&ment rios];
                      ApplicationArea=#Basic,#Suite;
                      RunObject=Page 31022969;
                      RunPageLink=BG/PO No.=FIELD(No.),
                                  Type=CONST(Payable);
                      Image=ViewComments }
      { 20      ;2   ;Separator  }
      { 51      ;2   ;Action    ;
                      CaptionML=[ENU=Analysis;
                                 PTG=An lise];
                      ApplicationArea=#Basic,#Suite;
                      RunObject=Page 31022997;
                      RunPageLink=No.=FIELD(No.),
                                  Due Date Filter=FIELD(Due Date Filter),
                                  Global Dimension 1 Filter=FIELD(Global Dimension 1 Filter),
                                  Global Dimension 2 Filter=FIELD(Global Dimension 2 Filter),
                                  Category Filter=FIELD(Category Filter);
                      Image=Report }
      { 58      ;2   ;Separator  }
      { 16      ;2   ;Action    ;
                      Ellipsis=Yes;
                      CaptionML=[ENU=Listing;
                                 PTG=Listagem];
                      ApplicationArea=#Basic,#Suite;
                      Promoted=Yes;
                      Image=List;
                      PromotedCategory=Report;
                      OnAction=BEGIN
                                 IF FIND THEN BEGIN
                                   PostedPmtOrd.COPY(Rec);
                                   PostedPmtOrd.SETRECFILTER;
                                   REPORT.RUN(REPORT::"Posted Payment Order Listing",TRUE,FALSE,PostedPmtOrd);
                                 END;
                               END;
                                }
      { 1900000004;0 ;ActionContainer;
                      ActionContainerType=ActionItems }
      { 21      ;1   ;Action    ;
                      Name=Navigate;
                      CaptionML=[ENU=&Navigate;
                                 PTG=&Navegar];
                      ApplicationArea=#Basic,#Suite;
                      Promoted=Yes;
                      Image=Navigate;
                      PromotedCategory=Process;
                      OnAction=VAR
                                 Option@1110001 : Integer;
                               BEGIN
                                 Navigate.SetDoc("Posting Date","No.");
                                 Navigate.RUN;
                               END;
                                }
      { 1903561204;1 ;Action    ;
                      CaptionML=[ENU=Posted Payment Orders Maturity;
                                 PTG=Vencimento Ordens Pagto. Registadas];
                      ApplicationArea=#Basic,#Suite;
                      RunObject=Page 31022995;
                      RunPageLink=No.=FIELD(No.),
                                  Global Dimension 1 Filter=FIELD(Global Dimension 1 Filter),
                                  Global Dimension 2 Filter=FIELD(Global Dimension 2 Filter),
                                  Category Filter=FIELD(Category Filter);
                      Promoted=Yes;
                      Image=DocumentsMaturity;
                      PromotedCategory=Process }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                CaptionML=[ENU=General;
                           PTG=Geral] }

    { 2   ;2   ;Field     ;
                SourceExpr="No.";
                Editable=FALSE }

    { 6   ;2   ;Field     ;
                SourceExpr="Bank Account No.";
                Editable=FALSE }

    { 4   ;2   ;Field     ;
                SourceExpr="Bank Account Name";
                Editable=FALSE }

    { 28  ;2   ;Field     ;
                SourceExpr="Posting Date";
                Importance=Promoted;
                Editable=FALSE }

    { 42  ;2   ;Field     ;
                SourceExpr="Currency Code" }

    { 13  ;2   ;Field     ;
                SourceExpr="Amount Grouped" }

    { 30  ;2   ;Field     ;
                SourceExpr="Remaining Amount" }

    { 45  ;2   ;Field     ;
                SourceExpr="Amount Grouped (LCY)";
                Importance=Promoted }

    { 47  ;2   ;Field     ;
                SourceExpr="Remaining Amount (LCY)";
                Importance=Promoted }

    { 32  ;1   ;Part      ;
                Name=Docs;
                CaptionML=[ENU=Docs. in Posted PO;
                           PTG=Docs. em Ordem Pagamento Reg.];
                SubPageView=SORTING(Bill Gr./Pmt. Order No.)
                            WHERE(Type=CONST(Payable));
                SubPageLink=Bill Gr./Pmt. Order No.=FIELD(No.);
                PagePartID=Page31023013 }

    { 1906223801;1;Group  ;
                CaptionML=[ENU=Expenses;
                           PTG=Despesas] }

    { 49  ;2   ;Field     ;
                SourceExpr="Payment Order Expenses Amt.";
                Editable=FALSE }

    { 1904361101;1;Group  ;
                CaptionML=[ENU=Auditing;
                           PTG=Auditoria] }

    { 10  ;2   ;Field     ;
                SourceExpr="Reason Code";
                Editable=FALSE }

    { 14  ;2   ;Field     ;
                SourceExpr="No. Printed";
                Editable=FALSE }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1907055107;1;Part   ;
                SubPageLink=No.=FIELD(No.);
                PagePartID=Page31023026;
                Visible=TRUE;
                PartType=Page }

    { 1901839807;1;Part   ;
                SubPageLink=No.=FIELD(No.);
                PagePartID=Page31023027;
                Visible=FALSE;
                PartType=Page }

    { 1905767507;1;Part   ;
                Visible=TRUE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {
    VAR
      PostedPmtOrd@1110000 : Record 31022953;
      Navigate@1000000000 : Page 344;

    BEGIN
    END.
  }
}

