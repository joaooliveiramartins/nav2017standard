OBJECT Table 9050 Warehouse Basic Cue
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Warehouse Basic Cue;
               PTG=Fila B�sica de Armaz�m];
  }
  FIELDS
  {
    { 1   ;   ;Primary Key         ;Code10        ;CaptionML=[ENU=Primary Key;
                                                              PTG=Chave Prim�ria] }
    { 2   ;   ;Rlsd. Sales Orders Until Today;Integer;
                                                   FieldClass=FlowField;
                                                   CalcFormula=Count("Sales Header" WHERE (Document Type=FILTER(Order),
                                                                                           Status=FILTER(Released),
                                                                                           Shipment Date=FIELD(Date Filter),
                                                                                           Location Code=FIELD(Location Filter)));
                                                   AccessByPermission=TableData 110=R;
                                                   CaptionML=[ENU=Rlsd. Sales Orders Until Today;
                                                              PTG=Encomendas Venda Libertas At� Hoje];
                                                   Editable=No }
    { 3   ;   ;Posted Sales Shipments - Today;Integer;
                                                   FieldClass=FlowField;
                                                   CalcFormula=Count("Sales Shipment Header" WHERE (Posting Date=FIELD(Date Filter2),
                                                                                                    Location Code=FIELD(Location Filter)));
                                                   CaptionML=[ENU=Posted Sales Shipments - Today;
                                                              PTG=Guias Remessa Vendas Registadas - Hoje];
                                                   Editable=No }
    { 4   ;   ;Exp. Purch. Orders Until Today;Integer;
                                                   FieldClass=FlowField;
                                                   CalcFormula=Count("Purchase Header" WHERE (Document Type=FILTER(Order),
                                                                                              Status=FILTER(Released),
                                                                                              Expected Receipt Date=FIELD(Date Filter),
                                                                                              Location Code=FIELD(Location Filter)));
                                                   AccessByPermission=TableData 120=R;
                                                   CaptionML=[ENU=Exp. Purch. Orders Until Today;
                                                              PTG=Encomendas Compra Esperadas At� Hoje];
                                                   Editable=No }
    { 5   ;   ;Posted Purch. Receipts - Today;Integer;
                                                   FieldClass=FlowField;
                                                   CalcFormula=Count("Purch. Rcpt. Header" WHERE (Posting Date=FIELD(Date Filter2),
                                                                                                  Location Code=FIELD(Location Filter)));
                                                   CaptionML=[ENU=Posted Purch. Receipts - Today;
                                                              PTG=Guias Remessa Compra Registadas - Hoje];
                                                   Editable=No }
    { 6   ;   ;Invt. Picks Until Today;Integer    ;FieldClass=FlowField;
                                                   CalcFormula=Count("Warehouse Activity Header" WHERE (Type=FILTER(Invt. Pick),
                                                                                                        Shipment Date=FIELD(Date Filter),
                                                                                                        Location Code=FIELD(Location Filter)));
                                                   CaptionML=[ENU=Invt. Picks Until Today;
                                                              PTG=Recolhas de Invent�rio At� Hoje];
                                                   Editable=No }
    { 7   ;   ;Invt. Put-aways Until Today;Integer;FieldClass=FlowField;
                                                   CalcFormula=Count("Warehouse Activity Header" WHERE (Type=FILTER(Invt. Put-away),
                                                                                                        Shipment Date=FIELD(Date Filter),
                                                                                                        Location Code=FIELD(Location Filter)));
                                                   CaptionML=[ENU=Invt. Put-aways Until Today;
                                                              PTG=Arruma��es Invent�rio At� Hoje];
                                                   Editable=No }
    { 20  ;   ;Date Filter         ;Date          ;FieldClass=FlowFilter;
                                                   CaptionML=[ENU=Date Filter;
                                                              PTG=Filtro Data];
                                                   Editable=No }
    { 21  ;   ;Date Filter2        ;Date          ;FieldClass=FlowFilter;
                                                   CaptionML=[ENU=Date Filter2;
                                                              PTG=Filtro Data2];
                                                   Editable=No }
    { 22  ;   ;Location Filter     ;Code10        ;FieldClass=FlowFilter;
                                                   CaptionML=[ENU=Location Filter;
                                                              PTG=Filtro Localiza��o] }
  }
  KEYS
  {
    {    ;Primary Key                             ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    END.
  }
}

