OBJECT Table 5057 Industry Group
{
  OBJECT-PROPERTIES
  {
    Date=07/09/12;
    Time=12:00:00;
    Version List=NAVW17.00;
  }
  PROPERTIES
  {
    DataCaptionFields=Code,Description;
    CaptionML=[ENU=Industry Group;
               PTG=Grupo Ind�stria];
    LookupPageID=Page5066;
  }
  FIELDS
  {
    { 1   ;   ;Code                ;Code10        ;CaptionML=[ENU=Code;
                                                              PTG=C�digo];
                                                   NotBlank=Yes }
    { 2   ;   ;Description         ;Text50        ;CaptionML=[ENU=Description;
                                                              PTG=Descri��o] }
    { 3   ;   ;No. of Contacts     ;Integer       ;FieldClass=FlowField;
                                                   CalcFormula=Count("Contact Industry Group" WHERE (Industry Group Code=FIELD(Code)));
                                                   CaptionML=[ENU=No. of Contacts;
                                                              PTG=N� de Contactos];
                                                   Editable=No }
  }
  KEYS
  {
    {    ;Code                                    ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    END.
  }
}

