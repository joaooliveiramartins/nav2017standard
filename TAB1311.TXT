OBJECT Table 1311 Last Used Chart
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Last Used Chart;
               PTG=�ltimo Gr�fico Usado];
  }
  FIELDS
  {
    { 1   ;   ;UID                 ;Code50        ;CaptionML=[ENU=UID;
                                                              PTG=UID] }
    { 2   ;   ;Code Unit ID        ;Integer       ;CaptionML=[ENU=Code Unit ID;
                                                              PTG=CodeUnitID] }
    { 3   ;   ;Chart Name          ;Text60        ;CaptionML=[ENU=Chart Name;
                                                              PTG=Nome Gr�fico] }
  }
  KEYS
  {
    {    ;UID                                     ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    END.
  }
}

