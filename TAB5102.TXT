OBJECT Table 5102 RM Matrix Management
{
  OBJECT-PROPERTIES
  {
    Date=05/08/11;
    Time=12:00:00;
    Version List=NAVW16.00;
  }
  PROPERTIES
  {
    CaptionML=[ENU=RM Matrix Management;
               PTG=Modelo Gest�o GR];
  }
  FIELDS
  {
    { 1   ;   ;Company Name        ;Text50        ;CaptionML=[ENU=Company Name;
                                                              PTG=Nome Empresa] }
    { 2   ;   ;No.                 ;Code20        ;CaptionML=[ENU=No.;
                                                              PTG=N�];
                                                   NotBlank=Yes }
    { 3   ;   ;Name                ;Text50        ;CaptionML=[ENU=Name;
                                                              PTG=Nome] }
    { 4   ;   ;Type                ;Option        ;CaptionML=[ENU=Type;
                                                              PTG=Tipo];
                                                   OptionCaptionML=[ENU=Company,Person;
                                                                    PTG=Empresa,Pessoa];
                                                   OptionString=Company,Person }
    { 5   ;   ;No. of Opportunities;Integer       ;FieldClass=FlowField;
                                                   CalcFormula=Count("Opportunity Entry" WHERE (Active=CONST(Yes),
                                                                                                Salesperson Code=FIELD(Salesperson Filter),
                                                                                                Campaign No.=FIELD(Campaign Filter),
                                                                                                Contact No.=FIELD(Contact Filter),
                                                                                                Contact Company No.=FIELD(Contact Company Filter),
                                                                                                Estimated Close Date=FIELD(Date Filter),
                                                                                                Action Taken=FIELD(Action Taken Filter),
                                                                                                Sales Cycle Code=FIELD(Sales Cycle Filter),
                                                                                                Sales Cycle Stage=FIELD(Sales Cycle Stage Filter),
                                                                                                Probability %=FIELD(Probability % Filter),
                                                                                                Completed %=FIELD(Completed % Filter),
                                                                                                Chances of Success %=FIELD(Chances of Success % Filter),
                                                                                                Close Opportunity Code=FIELD(Close Opportunity Filter),
                                                                                                Estimated Value (LCY)=FIELD(Estimated Value Filter),
                                                                                                Calcd. Current Value (LCY)=FIELD(Calcd. Current Value Filter)));
                                                   CaptionML=[ENU=No. of Opportunities;
                                                              PTG=N� de Oportunidades];
                                                   Editable=No }
    { 6   ;   ;Estimated Value (LCY);Decimal      ;FieldClass=FlowField;
                                                   CalcFormula=Sum("Opportunity Entry"."Estimated Value (LCY)" WHERE (Active=CONST(Yes),
                                                                                                                      Salesperson Code=FIELD(Salesperson Filter),
                                                                                                                      Campaign No.=FIELD(Campaign Filter),
                                                                                                                      Contact No.=FIELD(Contact Filter),
                                                                                                                      Contact Company No.=FIELD(Contact Company Filter),
                                                                                                                      Estimated Close Date=FIELD(Date Filter),
                                                                                                                      Action Taken=FIELD(Action Taken Filter),
                                                                                                                      Sales Cycle Code=FIELD(Sales Cycle Filter),
                                                                                                                      Sales Cycle Stage=FIELD(Sales Cycle Stage Filter),
                                                                                                                      Probability %=FIELD(Probability % Filter),
                                                                                                                      Completed %=FIELD(Completed % Filter),
                                                                                                                      Chances of Success %=FIELD(Chances of Success % Filter),
                                                                                                                      Close Opportunity Code=FIELD(Close Opportunity Filter),
                                                                                                                      Estimated Value (LCY)=FIELD(Estimated Value Filter),
                                                                                                                      Calcd. Current Value (LCY)=FIELD(Calcd. Current Value Filter)));
                                                   CaptionML=[ENU=Estimated Value (LCY);
                                                              PTG=Valor Estimado (DL)];
                                                   Editable=No;
                                                   AutoFormatType=1 }
    { 7   ;   ;Calcd. Current Value (LCY);Decimal ;FieldClass=FlowField;
                                                   CalcFormula=Sum("Opportunity Entry"."Calcd. Current Value (LCY)" WHERE (Active=CONST(Yes),
                                                                                                                           Salesperson Code=FIELD(Salesperson Filter),
                                                                                                                           Campaign No.=FIELD(Campaign Filter),
                                                                                                                           Contact No.=FIELD(Contact Filter),
                                                                                                                           Contact Company No.=FIELD(Contact Company Filter),
                                                                                                                           Estimated Close Date=FIELD(Date Filter),
                                                                                                                           Action Taken=FIELD(Action Taken Filter),
                                                                                                                           Sales Cycle Code=FIELD(Sales Cycle Filter),
                                                                                                                           Sales Cycle Stage=FIELD(Sales Cycle Stage Filter),
                                                                                                                           Probability %=FIELD(Probability % Filter),
                                                                                                                           Completed %=FIELD(Completed % Filter),
                                                                                                                           Chances of Success %=FIELD(Chances of Success % Filter),
                                                                                                                           Close Opportunity Code=FIELD(Close Opportunity Filter),
                                                                                                                           Estimated Value (LCY)=FIELD(Estimated Value Filter),
                                                                                                                           Calcd. Current Value (LCY)=FIELD(Calcd. Current Value Filter)));
                                                   CaptionML=[ENU=Calcd. Current Value (LCY);
                                                              PTG=Atual Valor Calc. (DL)];
                                                   Editable=No;
                                                   AutoFormatType=1 }
    { 8   ;   ;Avg. Estimated Value (LCY);Decimal ;FieldClass=FlowField;
                                                   CalcFormula=Average("Opportunity Entry"."Estimated Value (LCY)" WHERE (Active=CONST(Yes),
                                                                                                                          Salesperson Code=FIELD(Salesperson Filter),
                                                                                                                          Campaign No.=FIELD(Campaign Filter),
                                                                                                                          Contact No.=FIELD(Contact Filter),
                                                                                                                          Contact Company No.=FIELD(Contact Company Filter),
                                                                                                                          Estimated Close Date=FIELD(Date Filter),
                                                                                                                          Action Taken=FIELD(Action Taken Filter),
                                                                                                                          Sales Cycle Code=FIELD(Sales Cycle Filter),
                                                                                                                          Sales Cycle Stage=FIELD(Sales Cycle Stage Filter),
                                                                                                                          Probability %=FIELD(Probability % Filter),
                                                                                                                          Completed %=FIELD(Completed % Filter),
                                                                                                                          Chances of Success %=FIELD(Chances of Success % Filter),
                                                                                                                          Close Opportunity Code=FIELD(Close Opportunity Filter),
                                                                                                                          Estimated Value (LCY)=FIELD(Estimated Value Filter),
                                                                                                                          Calcd. Current Value (LCY)=FIELD(Calcd. Current Value Filter)));
                                                   CaptionML=[ENU=Avg. Estimated Value (LCY);
                                                              PTG=Valor M�dio Estimado (DL)];
                                                   Editable=No;
                                                   AutoFormatType=1 }
    { 9   ;   ;Avg.Calcd. Current Value (LCY);Decimal;
                                                   FieldClass=FlowField;
                                                   CalcFormula=Average("Opportunity Entry"."Calcd. Current Value (LCY)" WHERE (Active=CONST(Yes),
                                                                                                                               Salesperson Code=FIELD(Salesperson Filter),
                                                                                                                               Campaign No.=FIELD(Campaign Filter),
                                                                                                                               Contact No.=FIELD(Contact Filter),
                                                                                                                               Contact Company No.=FIELD(Contact Company Filter),
                                                                                                                               Estimated Close Date=FIELD(Date Filter),
                                                                                                                               Action Taken=FIELD(Action Taken Filter),
                                                                                                                               Sales Cycle Code=FIELD(Sales Cycle Filter),
                                                                                                                               Sales Cycle Stage=FIELD(Sales Cycle Stage Filter),
                                                                                                                               Probability %=FIELD(Probability % Filter),
                                                                                                                               Completed %=FIELD(Completed % Filter),
                                                                                                                               Close Opportunity Code=FIELD(Close Opportunity Filter),
                                                                                                                               Chances of Success %=FIELD(Chances of Success % Filter),
                                                                                                                               Estimated Value (LCY)=FIELD(Estimated Value Filter),
                                                                                                                               Calcd. Current Value (LCY)=FIELD(Calcd. Current Value Filter)));
                                                   CaptionML=[ENU=Avg.Calcd. Current Value (LCY);
                                                              PTG=Atual Valor M�dio Calc. (DL)];
                                                   Editable=No;
                                                   AutoFormatType=1 }
    { 10  ;   ;Salesperson Filter  ;Code10        ;FieldClass=FlowFilter;
                                                   TableRelation=Salesperson/Purchaser;
                                                   CaptionML=[ENU=Salesperson Filter;
                                                              PTG=Filtro vendedor] }
    { 11  ;   ;Campaign Filter     ;Code20        ;FieldClass=FlowFilter;
                                                   TableRelation=Campaign;
                                                   CaptionML=[ENU=Campaign Filter;
                                                              PTG=Filtro Campanha] }
    { 12  ;   ;Contact Filter      ;Code20        ;FieldClass=FlowFilter;
                                                   TableRelation=Contact;
                                                   CaptionML=[ENU=Contact Filter;
                                                              PTG=Filtro Contacto] }
    { 13  ;   ;Date Filter         ;Date          ;FieldClass=FlowFilter;
                                                   CaptionML=[ENU=Date Filter;
                                                              PTG=Filtro Data] }
    { 14  ;   ;Action Taken Filter ;Option        ;FieldClass=FlowFilter;
                                                   CaptionML=[ENU=Action Taken Filter;
                                                              PTG=Filtro A��o Tomada];
                                                   OptionCaptionML=[ENU=" ,Next,Previous,Updated,Jumped,Won,Lost";
                                                                    PTG=" ,Pr�ximo,Anterior,Atualizado,Saltado,Ganho,Perdido"];
                                                   OptionString=[ ,Next,Previous,Updated,Jumped,Won,Lost] }
    { 15  ;   ;Sales Cycle Filter  ;Code10        ;FieldClass=FlowFilter;
                                                   TableRelation="Sales Cycle";
                                                   CaptionML=[ENU=Sales Cycle Filter;
                                                              PTG=Filtro Ciclo Vendas] }
    { 16  ;   ;Sales Cycle Stage Filter;Integer   ;FieldClass=FlowFilter;
                                                   TableRelation="Sales Cycle Stage".Stage WHERE (Sales Cycle Code=FIELD(Sales Cycle Filter));
                                                   CaptionML=[ENU=Sales Cycle Stage Filter;
                                                              PTG=Filtro Est�dio Ciclo Vendas] }
    { 17  ;   ;Probability % Filter;Decimal       ;FieldClass=FlowFilter;
                                                   CaptionML=[ENU=Probability % Filter;
                                                              PTG=Filtro % Probabilidade];
                                                   DecimalPlaces=1:1;
                                                   MinValue=0;
                                                   MaxValue=100 }
    { 18  ;   ;Completed % Filter  ;Decimal       ;FieldClass=FlowFilter;
                                                   CaptionML=[ENU=Completed % Filter;
                                                              PTG=Filtro % Finaliza��o];
                                                   DecimalPlaces=1:1;
                                                   MinValue=0;
                                                   MaxValue=100 }
    { 19  ;   ;Company No.         ;Code20        ;TableRelation=Contact WHERE (Type=CONST(Company));
                                                   CaptionML=[ENU=Company No.;
                                                              PTG=N� Empresa] }
    { 20  ;   ;Contact Company Filter;Code20      ;FieldClass=FlowFilter;
                                                   TableRelation=Contact WHERE (Type=CONST(Company));
                                                   CaptionML=[ENU=Contact Company Filter;
                                                              PTG=Filtro Companhia - Contacto] }
    { 21  ;   ;To-do Status Filter ;Option        ;FieldClass=FlowFilter;
                                                   CaptionML=[ENU=To-do Status Filter;
                                                              PTG=Filtro Estado A��o Efetuar];
                                                   OptionCaptionML=[ENU=Not Started,In Progress,Completed,Waiting,Postponed;
                                                                    PTG=N�o iniciado,Em Execu��o,Completado,Em Espera,Adiado];
                                                   OptionString=Not Started,In Progress,Completed,Waiting,Postponed }
    { 22  ;   ;To-do Closed Filter ;Boolean       ;FieldClass=FlowFilter;
                                                   CaptionML=[ENU=To-do Closed Filter;
                                                              PTG=Filtro A��o Efet. Fechadas] }
    { 23  ;   ;Priority Filter     ;Option        ;FieldClass=FlowFilter;
                                                   CaptionML=[ENU=Priority Filter;
                                                              PTG=Filtro Prioridade];
                                                   OptionCaptionML=[ENU=Low,Normal,High;
                                                                    PTG=Baixo,Normal,Alto];
                                                   OptionString=Low,Normal,High }
    { 24  ;   ;Team Filter         ;Code10        ;FieldClass=FlowFilter;
                                                   TableRelation=Team;
                                                   CaptionML=[ENU=Team Filter;
                                                              PTG=Filtro Equipa] }
    { 25  ;   ;No. of To-dos       ;Integer       ;FieldClass=FlowField;
                                                   CalcFormula=Count(To-do WHERE (Date=FIELD(Date Filter),
                                                                                  Salesperson Code=FIELD(Salesperson Filter),
                                                                                  Team Code=FIELD(Team Filter),
                                                                                  Campaign No.=FIELD(Campaign Filter),
                                                                                  Contact No.=FIELD(Contact Filter),
                                                                                  Contact Company No.=FIELD(Contact Company Filter),
                                                                                  Status=FIELD(To-do Status Filter),
                                                                                  Closed=FIELD(To-do Closed Filter),
                                                                                  Priority=FIELD(Priority Filter),
                                                                                  System To-do Type=FIELD(System To-do Type Filter)));
                                                   CaptionML=[ENU=No. of To-dos;
                                                              PTG=N� A��es Efetuar];
                                                   Editable=No }
    { 26  ;   ;Estimated Value Filter;Decimal     ;FieldClass=FlowFilter;
                                                   CaptionML=[ENU=Estimated Value Filter;
                                                              PTG=Filtro Valor Estimado];
                                                   AutoFormatType=1 }
    { 27  ;   ;Calcd. Current Value Filter;Decimal;FieldClass=FlowFilter;
                                                   CaptionML=[ENU=Calcd. Current Value Filter;
                                                              PTG=Filtro Atual Valor Calc.];
                                                   AutoFormatType=1 }
    { 28  ;   ;Chances of Success % Filter;Decimal;FieldClass=FlowFilter;
                                                   CaptionML=[ENU=Chances of Success % Filter;
                                                              PTG=Filtro % Hip�teses de Sucesso];
                                                   DecimalPlaces=0:0;
                                                   MinValue=0;
                                                   MaxValue=100 }
    { 29  ;   ;Close Opportunity Filter;Code10    ;FieldClass=FlowFilter;
                                                   TableRelation="Close Opportunity Code";
                                                   CaptionML=[ENU=Close Opportunity Filter;
                                                              PTG=Filtro Fecho Oportunidade] }
    { 30  ;   ;System To-do Type Filter;Option    ;FieldClass=FlowFilter;
                                                   CaptionML=[ENU=System To-do Type Filter;
                                                              PTG=Filtro Tipo A��o de Sistema];
                                                   OptionCaptionML=[ENU=Organizer,Salesperson Attendee,Contact Attendee,Team;
                                                                    PTG=Organizador,Vendedor Participante,Contacto Participante,Equipa];
                                                   OptionString=Organizer,Salesperson Attendee,Contact Attendee,Team }
  }
  KEYS
  {
    {    ;Company Name,Type,Name,No.              ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    END.
  }
}

