OBJECT Report 31022982 Batch Settl. Posted Bill Grs.
{
  OBJECT-PROPERTIES
  {
    Date=13/02/15;
    Time=13:00:00;
    Version List=NAVPTSS82.00;
  }
  PROPERTIES
  {
    Permissions=TableData 21=imd,
                TableData 25=imd,
                TableData 31022936=imd,
                TableData 31022937=imd,
                TableData 31022939=imd,
                TableData 31022940=imd;
    CaptionML=[ENU=Batch Settl. Posted Bill Grs.;
               PTG=Liquida��o Remessas Registadas em Lote];
    ProcessingOnly=Yes;
    OnInitReport=BEGIN
                   PostingDate := WORKDATE;
                 END;

    OnPreReport=BEGIN
                  GLSetup.GET;
                END;

  }
  DATASET
  {
    { 2392;    ;DataItem;PostedBillGr        ;
               DataItemTable=Table31022939;
               DataItemTableView=SORTING(No.)
                                 ORDER(Ascending);
               OnPreDataItem=BEGIN
                               DocPost.CheckPostingDate(PostingDate);

                               SourceCodeSetup.GET;
                               SourceCode := SourceCodeSetup."Cartera Journal";

                               GroupAmountLCY := 0;
                               BillGrCount := 0;
                               DocCount := 0;
                               TotalDocCount := 0;
                               TotalBillGr := PostedBillGr.COUNT;
                               Window.OPEN(
                                 Text31022890 +
                                 Text31022891 +
                                 Text31022892);
                             END;

               OnAfterGetRecord=BEGIN
                                  BillGrCount := BillGrCount + 1;
                                  Window.UPDATE(1,ROUND(BillGrCount / TotalBillGr * 10000,1));
                                  Window.UPDATE(2,STRSUBSTNO('%1',"No."));
                                  Window.UPDATE(3,0);
                                  GroupAmount := 0;

                                  TotalDisctdAmt := 0;
                                END;

               OnPostDataItem=BEGIN
                                Window.CLOSE;

                                COMMIT;

                                MESSAGE(
                                  Text31022893,
                                  TotalDocCount,BillGrCount,GroupAmountLCY);
                              END;
                               }

    { 2883;1   ;DataItem;PostedDoc           ;
               DataItemTable=Table31022936;
               DataItemTableView=SORTING(Bill Gr./Pmt. Order No.,Status,Category Code,Redrawn,Due Date)
                                 WHERE(Status=CONST(Open),
                                       Type=CONST(Receivable));
               OnPreDataItem=BEGIN
                               SumLCYAmt := 0;
                               GenJnlLineNextNo := 0;
                               TotalDoc := PostedDoc.COUNT;
                               ExistVATEntry := FALSE;
                             END;

               OnAfterGetRecord=BEGIN
                                  IsRedrawn := CarteraManagement.CheckFromRedrawnDoc(PostedDoc."No.");
                                  BankAcc.GET(PostedBillGr."Bank Account No.");
                                  Delay := BankAcc."Delay for Notices";

                                  IF DueOnly AND (PostingDate < "Due Date" + Delay) THEN
                                    CurrReport.SKIP;

                                  TotalDocCount := TotalDocCount + 1;
                                  DocCount := DocCount + 1;
                                  Window.UPDATE(3,ROUND(DocCount / TotalDoc * 10000,1));
                                  Window.UPDATE(4,STRSUBSTNO('%1 %2',"Document Type","Document No."));

                                  WITH TempGenJnlLine DO BEGIN
                                    GenJnlLineNextNo := GenJnlLineNextNo + 10000;
                                    CLEAR(TempGenJnlLine);
                                    INIT;
                                    "Line No." := GenJnlLineNextNo;
                                    "Posting Date" := PostingDate;
                                    "Document Type" := "Document Type"::Payment;
                                    "Document No." := PostedBillGr."No.";
                                    VALIDATE("Account Type","Account Type"::Customer);
                                    CustLedgEntry.GET(PostedDoc."Entry No.");
                                    IF PostedDoc."Document Type" = PostedDoc."Document Type"::Bill THEN BEGIN
                                      Description := COPYSTR(
                                        STRSUBSTNO(Text31022894,PostedDoc."Document No.",PostedDoc."No."),
                                        1,MAXSTRLEN(Description));
                                      IF GLSetup."Unrealized VAT" THEN BEGIN
                                        ExistVATEntry := CarteraManagement.FindCustVATSetup(VATPostingSetup,CustLedgEntry);
                                      END;
                                    END ELSE
                                      Description := COPYSTR(
                                        STRSUBSTNO(Text31022895,PostedDoc."Document No."),
                                        1,MAXSTRLEN(Description));
                                    VALIDATE("Account No.",CustLedgEntry."Customer No.");
                                    VALIDATE("Currency Code",PostedDoc."Currency Code");
                                    VALIDATE(Amount,-PostedDoc."Remaining Amount");
                                    "Applies-to Doc. Type" := CustLedgEntry."Document Type";
                                    "Applies-to Doc. No." := CustLedgEntry."Document No.";
                                    "Applies-to Bill No." := CustLedgEntry."Bill No.";
                                    "Source Code" := SourceCode;
                                    "System-Created Entry" := TRUE;
                                    "Shortcut Dimension 1 Code" := CustLedgEntry."Global Dimension 1 Code";
                                    "Shortcut Dimension 1 Code" := CustLedgEntry."Global Dimension 2 Code";
                                    "Dimension Set ID" := CustLedgEntry."Dimension Set ID";
                                    INSERT;
                                    SumLCYAmt := SumLCYAmt + "Amount (LCY)";
                                  END;

                                  IF ("Document Type" = "Document Type"::Bill) AND
                                     GLSetup."Unrealized VAT" AND
                                     ExistVATEntry AND
                                     (NOT IsRedrawn) THEN BEGIN
                                    CustLedgEntry.CALCFIELDS("Remaining Amount","Remaining Amt. (LCY)");
                                    CarteraManagement.CustUnrealizedVAT2(
                                      CustLedgEntry,
                                      CustLedgEntry."Remaining Amt. (LCY)",
                                      TempGenJnlLine,
                                      ExistVATEntry,
                                      FirstVATEntryNo,
                                      LastVATEntryNo,
                                      TempNoRealVATBuffer);

                                      IF TempNoRealVATBuffer.FINDSET THEN BEGIN
                                        REPEAT
                                          BEGIN
                                            InsertGenJournalLine(
                                              TempGenJnlLine."Account Type"::"G/L Account",
                                              TempNoRealVATBuffer.Account,
                                              TempNoRealVATBuffer.Amount);
                                            InsertGenJournalLine(
                                              TempGenJnlLine."Account Type"::"G/L Account",
                                              TempNoRealVATBuffer."Balance Account",
                                              -TempNoRealVATBuffer.Amount);
                                          END;
                                        UNTIL TempNoRealVATBuffer.NEXT = 0;
                                        TempNoRealVATBuffer.DELETEALL;
                                      END;
                                  END;

                                  GroupAmount := GroupAmount + "Remaining Amount";
                                  GroupAmountLCY := GroupAmountLCY + "Remaining Amt. (LCY)";
                                  CustLedgEntry."Document Status" := CustLedgEntry."Document Status"::Honored;
                                  CustLedgEntry.MODIFY;

                                  IF TempBGPOPostBuffer.GET('','',CustLedgEntry."Entry No.") THEN BEGIN
                                    TempBGPOPostBuffer.Amount := TempBGPOPostBuffer.Amount + "Remaining Amount";
                                    TempBGPOPostBuffer.MODIFY;
                                  END ELSE BEGIN
                                    TempBGPOPostBuffer.INIT;
                                    TempBGPOPostBuffer."Entry No." := CustLedgEntry."Entry No.";
                                    TempBGPOPostBuffer.Amount := TempBGPOPostBuffer.Amount + "Remaining Amount";
                                    TempBGPOPostBuffer.INSERT;
                                  END;


                                  IF  (PostedBillGr."Dealing Type" = PostedBillGr."Dealing Type"::Discount)  AND
                                      (PostedBillGr.Factoring <> PostedBillGr.Factoring::" ") THEN BEGIN
                                    DiscFactLiabs(PostedDoc);
                                  END;
                                END;

               OnPostDataItem=VAR
                                CustLedgEntry2@1110001 : Record 21;
                              BEGIN
                                IF (DocCount = 0) OR (GroupAmount = 0) THEN BEGIN
                                  IF DueOnly THEN
                                    ERROR(
                                      Text31022896 +
                                      Text31022897,
                                      PostedBillGr."No.")
                                  ELSE
                                    ERROR(
                                      Text31022896 +
                                      Text31022898,
                                      PostedBillGr."No.");
                                END;

                                IF PostedBillGr.Factoring = PostedBillGr.Factoring::" " THEN BEGIN

                                  WITH TempGenJnlLine DO BEGIN
                                    IF TempBGPOPostBuffer.FINDSET THEN BEGIN
                                      REPEAT
                                        CustLedgEntry2.GET(TempBGPOPostBuffer."Entry No.");
                                        CLEAR(TempGenJnlLine);
                                        INIT;
                                        GenJnlLineNextNo := GenJnlLineNextNo + 10000;
                                        "Line No." := GenJnlLineNextNo;
                                        "Posting Date" := PostingDate;
                                        "Document Type" := "Document Type"::Payment;
                                        "Document No." := PostedBillGr."No.";
                                        IF "Dealing Type" = "Dealing Type"::Discount THEN BEGIN
                                          BankAcc.TESTFIELD("Bank Acc. Posting Group");
                                          BankAccPostingGr.GET(BankAcc."Bank Acc. Posting Group");
                                          VALIDATE("Account Type","Account Type"::"G/L Account");
                                          BankAccPostingGr.TESTFIELD("Liabs. for Disc. Bills Acc.");
                                          VALIDATE("Account No.",BankAccPostingGr."Liabs. for Disc. Bills Acc.");
                                        END ELSE BEGIN
                                          VALIDATE("Account Type",TempGenJnlLine."Account Type"::"Bank Account");
                                          VALIDATE("Account No.",BankAcc."No.");
                                        END;
                                        Description := COPYSTR(STRSUBSTNO(Text31022899,PostedBillGr."No."),1,MAXSTRLEN(Description));
                                        VALIDATE("Currency Code",PostedBillGr."Currency Code");
                                        VALIDATE(Amount,TempBGPOPostBuffer.Amount);
                                        "Shortcut Dimension 1 Code" := TempBGPOPostBuffer."Global Dimension 1 Code";
                                        "Shortcut Dimension 2 Code" := TempBGPOPostBuffer."Global Dimension 2 Code";
                                        "Dimension Set ID" := CustLedgEntry2."Dimension Set ID";
                                        "Source Code" := SourceCode;
                                        "System-Created Entry" := TRUE;
                                        INSERT;
                                        SumLCYAmt := SumLCYAmt + "Amount (LCY)";
                                      UNTIL TempBGPOPostBuffer.NEXT <= 0;
                                    END;
                                  TempBGPOPostBuffer.DELETEALL;
                                  END;
                                END ELSE
                                  FactBankAccounting;

                                IF PostedBillGr."Currency Code" <> '' THEN BEGIN
                                  IF SumLCYAmt <> 0 THEN BEGIN
                                    Currency.GET(PostedBillGr."Currency Code");
                                    IF SumLCYAmt > 0 THEN BEGIN
                                      Currency.TESTFIELD("Residual Gains Account");
                                      Acct := Currency."Residual Gains Account";
                                    END ELSE BEGIN
                                      Currency.TESTFIELD("Residual Losses Account");
                                      Acct := Currency."Residual Losses Account";
                                    END;
                                    GenJnlLineNextNo := GenJnlLineNextNo + 10000;
                                    WITH TempGenJnlLine DO BEGIN
                                      CLEAR(TempGenJnlLine);
                                      INIT;
                                      "Line No." := GenJnlLineNextNo;
                                      "Posting Date" := PostingDate;
                                      "Document Type" := "Document Type"::Payment;
                                      "Document No." := PostedBillGr."No.";
                                      VALIDATE("Account Type","Account Type"::"G/L Account");
                                      VALIDATE("Account No.",Acct);
                                      Description := Text31022900;
                                      VALIDATE("Currency Code",'');
                                      VALIDATE(Amount,-SumLCYAmt);
                                      "Shortcut Dimension 1 Code" := BankAcc."Global Dimension 1 Code";
                                      "Shortcut Dimension 2 Code" := BankAcc."Global Dimension 2 Code";
                                      "Dimension Set ID" := CustLedgEntry2."Dimension Set ID";
                                      "Source Code" := SourceCode;
                                      "System-Created Entry" := TRUE;
                                      INSERT;
                                    END;
                                  END;
                                END;

                                IF TempGenJnlLine.FINDSET THEN
                                  REPEAT
                                    TempGenJnlLine2 := TempGenJnlLine;
                                    GenJnlPostLine.SetFromSettlement(TRUE);
                                    GenJnlPostLine.RunWithCheck(TempGenJnlLine2);
                                  UNTIL TempGenJnlLine.NEXT = 0;
                                TempGenJnlLine.DELETEALL;

                                DocPost.CloseBillGroupIfEmpty(PostedBillGr,PostingDate);

                                IF ExistVATEntry THEN BEGIN
                                  GLReg.FINDLAST;
                                  GLReg."From VAT Entry No." := FirstVATEntryNo;
                                  GLReg."To VAT Entry No." := LastVATEntryNo;
                                  GLReg.MODIFY;
                                END;
                              END;

               DataItemLink=Bill Gr./Pmt. Order No.=FIELD(No.) }

  }
  REQUESTPAGE
  {
    PROPERTIES
    {
      SaveValues=Yes;
    }
    CONTROLS
    {
      { 1900000001;0;Container;
                  ContainerType=ContentArea }

      { 1900000002;1;Group  ;
                  CaptionML=[ENU=Options;
                             PTG=Op��es] }

      { 5   ;2   ;Field     ;
                  CaptionML=[ENU=Posting Date;
                             PTG=Data Registo];
                  NotBlank=Yes;
                  SourceExpr=PostingDate }

      { 6   ;2   ;Field     ;
                  CaptionML=[ENU=Due bills only;
                             PTG=Apenas Letras Vencidas];
                  SourceExpr=DueOnly }

    }
  }
  LABELS
  {
  }
  CODE
  {
    VAR
      Text31022890@1000 : TextConst 'ENU=Settling           @1@@@@@@@@@@@@@@@@@@@@@@@\\;PTG=A Liquidar           @1@@@@@@@@@@@@@@@@@@@@@@@\\';
      Text31022891@1001 : TextConst 'ENU=Bill Groups        #2######  @3@@@@@@@@@@@@@\;PTG=Remessas        #2######  @3@@@@@@@@@@@@@\';
      Text31022892@1002 : TextConst 'ENU=Receiv. Documents  #4######;PTG=Documentos a receber  #4######';
      Text31022893@1003 : TextConst 'ENU=%1 Documents in %2 Bill Groups totaling %3 (LCY) have been settled.;PTG=%1 Documentos em %2 Remessas que totalizam %3 (DL) foram liquidados.';
      Text31022894@1004 : TextConst 'ENU=Receivable bill settlement %1/%2;PTG=Letras a receber liquidadas %1/%2';
      Text31022895@1005 : TextConst 'ENU=Receivable document settlement %1;PTG=Documentos a receber liquidados %1';
      Text31022896@1006 : TextConst 'ENU=No receivable documents have been found that can be settled in Posted Bill Group No. %1.;PTG=N�o foram encontrados documentos a receber que possam ser liquidados na Remessa Registada N� %1.';
      Text31022897@1007 : TextConst 'ENU=Please check that the selection is not empty and at least one receivable document is open and due.;PTG=Por favor verifique que a sele��o n�o est� vazia e que pelo menos um documento a receber est� aberto e vencido.';
      Text31022898@1008 : TextConst 'ENU=Please check that the selection is not empty and at least one receivable document is open.;PTG=Por favor verifique que a sele��o n�o est� vazia e que pelo menos um documento a receber est� aberto.';
      Text31022899@1009 : TextConst 'ENU=Bill Group settlement %1;PTG=Liquida��o Remessa %1';
      Text31022900@1010 : TextConst 'ENU=Residual adjust generated by rounding Amount;PTG=Ajuste residual gerado pelo arredondamento do Valor';
      Text31022901@1011 : TextConst 'ENU=Bill Group settlement %1 Customer No. %2;PTG=Liquida��o Remessa %1 Cliente N� %2';
      Text31022902@1012 : TextConst 'ENU=Receivable document settlement %1/%2;PTG=Liquida��o de documento a receber %1/%2';
      SourceCodeSetup@1110000 : Record 242;
      TempGenJnlLine@1110001 : TEMPORARY Record 81;
      TempGenJnlLine2@1110002 : TEMPORARY Record 81;
      CustLedgEntry@1110003 : Record 21;
      BankAccPostingGr@1110004 : Record 277;
      BankAcc@1110005 : Record 270;
      CurrExchRate@1110006 : Record 330;
      Currency@1110007 : Record 4;
      GLReg@1110008 : Record 45;
      GLSetup@1110009 : Record 98;
      VATPostingSetup@1110010 : Record 325;
      DocPost@1110012 : Codeunit 31022906;
      CarteraManagement@1110013 : Codeunit 31022901;
      GenJnlPostLine@1110014 : Codeunit 12;
      Window@1110015 : Dialog;
      PostingDate@1110016 : Date;
      DueOnly@1110017 : Boolean;
      Delay@1110018 : Decimal;
      SourceCode@1110019 : Code[10];
      Acct@1110020 : Code[20];
      DocCount@1110021 : Integer;
      TotalDocCount@1110022 : Integer;
      GroupAmount@1110023 : Decimal;
      GroupAmountLCY@1110024 : Decimal;
      GenJnlLineNextNo@1110025 : Integer;
      SumLCYAmt@1110026 : Decimal;
      TotalDisctdAmt@1110027 : Decimal;
      BillGrCount@1110028 : Integer;
      TotalBillGr@1110029 : Integer;
      TotalDoc@1110030 : Integer;
      ExistVATEntry@1110031 : Boolean;
      FirstVATEntryNo@1110032 : Integer;
      LastVATEntryNo@1110033 : Integer;
      IsRedrawn@1110034 : Boolean;
      TempBGPOPostBuffer@1110039 : TEMPORARY Record 31022945;
      TempNoRealVATBuffer@1110042 : TEMPORARY Record 31022945;
      ExistsNoRealVAT@1110041 : Boolean;

    PROCEDURE DiscFactLiabs@1(PostedDoc2@1110000 : Record 31022936);
    VAR
      Currency2@1110001 : Record 4;
      DisctedAmt@1110002 : Decimal;
      RoundingPrec@1110003 : Decimal;
    BEGIN
      DisctedAmt := 0;
      GenJnlLineNextNo := GenJnlLineNextNo + 10000;

      IF PostedDoc2."Currency Code" <> '' THEN BEGIN
        Currency2.GET(PostedDoc2."Currency Code");
        RoundingPrec := Currency2."Amount Rounding Precision";
      END ELSE
        RoundingPrec := GLSetup."Amount Rounding Precision";

      DisctedAmt := ROUND(DocPost.FindDisctdAmt(
        PostedDoc2."Remaining Amount",
        PostedDoc2."Account No.",
        PostedDoc2."Bank Account No."),RoundingPrec);

      TotalDisctdAmt := TotalDisctdAmt + DisctedAmt;

        WITH TempGenJnlLine DO BEGIN
          CLEAR(TempGenJnlLine);
          INIT;
          "Line No." := GenJnlLineNextNo;
          "Posting Date" := PostingDate;
          "Document Type" := "Document Type"::Payment;
          "Document No." := PostedBillGr."No.";
          BankAcc.TESTFIELD("Bank Acc. Posting Group");
          BankAccPostingGr.GET(BankAcc."Bank Acc. Posting Group");
          VALIDATE("Account Type","Account Type"::"G/L Account");
          BankAccPostingGr.TESTFIELD("Liabs. for Factoring Acc.");
          VALIDATE("Account No.",BankAccPostingGr."Liabs. for Factoring Acc.");
          Description := COPYSTR(
            STRSUBSTNO(Text31022901,
            PostedBillGr."No.",
            PostedDoc2."Account No."),1,MAXSTRLEN(Description));
          VALIDATE("Currency Code",PostedBillGr."Currency Code");
          VALIDATE(Amount,DisctedAmt);
          "Source Code" := SourceCode;
          "System-Created Entry" := TRUE;
          "Shortcut Dimension 1 Code" := PostedDoc2."Global Dimension 1 Code";
          "Shortcut Dimension 2 Code" := PostedDoc2."Global Dimension 2 Code";
          "Dimension Set ID" := PostedDoc2."Dimension Set ID";
          TempBGPOPostBuffer."Gain - Loss Amount (LCY)" := TempBGPOPostBuffer."Gain - Loss Amount (LCY)" + Amount;
          TempBGPOPostBuffer.MODIFY;
          INSERT;
          SumLCYAmt := SumLCYAmt + "Amount (LCY)";
        END;
    END;

    PROCEDURE FactBankAccounting@11();
    VAR
      CustLedgEntry2@1110000 : Record 21;
    BEGIN
      CASE TRUE OF
        PostedDoc."Dealing Type" = PostedDoc."Dealing Type"::Discount:
          BEGIN
            WITH TempGenJnlLine DO BEGIN
              IF TempBGPOPostBuffer.FINDSET THEN BEGIN
                REPEAT
                  CustLedgEntry2.GET(TempBGPOPostBuffer."Entry No.");
                  GenJnlLineNextNo := GenJnlLineNextNo + 10000;
                  CLEAR(TempGenJnlLine);
                  INIT;
                  "Line No." := GenJnlLineNextNo;
                  "Posting Date" := PostingDate;
                  "Document Type" := "Document Type"::Payment;
                  "Document No." := PostedBillGr."No.";
                  BankAcc.TESTFIELD("Bank Acc. Posting Group");
                  BankAccPostingGr.GET(BankAcc."Bank Acc. Posting Group");
                  VALIDATE("Account Type",TempGenJnlLine."Account Type"::"Bank Account");
                  VALIDATE("Account No.",BankAcc."No.");
                  Description := COPYSTR(STRSUBSTNO(Text31022899,PostedBillGr."No."),1,MAXSTRLEN(Description));
                  VALIDATE("Currency Code",PostedBillGr."Currency Code");
                  VALIDATE(Amount,TempBGPOPostBuffer.Amount - TempBGPOPostBuffer."Gain - Loss Amount (LCY)");
                  "Shortcut Dimension 1 Code" := TempBGPOPostBuffer."Global Dimension 1 Code";
                  "Shortcut Dimension 2 Code" := TempBGPOPostBuffer."Global Dimension 2 Code";
                  "Dimension Set ID" := CustLedgEntry2."Dimension Set ID";
                  "Source Code" := SourceCode;
                  "System-Created Entry" := TRUE;
                  INSERT;
                  SumLCYAmt := SumLCYAmt + "Amount (LCY)";
                UNTIL TempBGPOPostBuffer.NEXT <= 0;
              END;
            END;
          END;
        ELSE
          BEGIN
            WITH TempGenJnlLine DO BEGIN
              IF TempBGPOPostBuffer.FINDSET THEN BEGIN
                REPEAT
                  CustLedgEntry2.GET(TempBGPOPostBuffer."Entry No.");
                  GenJnlLineNextNo := GenJnlLineNextNo + 10000;
                  CLEAR(TempGenJnlLine);
                  INIT;
                  "Line No." := GenJnlLineNextNo;
                  "Posting Date" := PostingDate;
                  "Document Type" := "Document Type"::Payment;
                  "Document No." := PostedBillGr."No.";
                  BankAcc.TESTFIELD("Bank Acc. Posting Group");
                  BankAccPostingGr.GET(BankAcc."Bank Acc. Posting Group");
                  VALIDATE("Account Type",TempGenJnlLine."Account Type"::"Bank Account");
                  VALIDATE("Account No.",BankAcc."No.");
                  Description := COPYSTR(STRSUBSTNO(Text31022899,PostedBillGr."No."),1,MAXSTRLEN(Description));
                  VALIDATE("Currency Code",PostedBillGr."Currency Code");
                  VALIDATE(Amount,TempBGPOPostBuffer.Amount);
                  "Shortcut Dimension 1 Code" := TempBGPOPostBuffer."Global Dimension 1 Code";
                  "Shortcut Dimension 2 Code" := TempBGPOPostBuffer."Global Dimension 2 Code";
                  "Dimension Set ID" := CustLedgEntry2."Dimension Set ID";
                  "Source Code" := SourceCode;
                  "System-Created Entry" := TRUE;
                  INSERT;
                  SumLCYAmt := SumLCYAmt + "Amount (LCY)";
                UNTIL TempBGPOPostBuffer.NEXT <= 0;
              END;
            END;
          END;
      END;
    END;

    LOCAL PROCEDURE InsertGenJournalLine@4(AccType@1110000 : Integer;AccNo@1110001 : Code[20];Amount2@1110002 : Decimal);
    BEGIN
      GenJnlLineNextNo :=  GenJnlLineNextNo + 10000;
      WITH TempGenJnlLine DO BEGIN
        CLEAR(TempGenJnlLine);
        INIT;
        "Line No." := GenJnlLineNextNo;
        "Posting Date" := PostingDate;
        "Document Type" := "Document Type"::Payment;
        "Document No." := PostedBillGr."No.";
        "Account Type" := AccType;
        "Account No." := AccNo;
        Description := COPYSTR(
          STRSUBSTNO(Text31022902,PostedDoc."Document No.",PostedDoc."No."),
          1,MAXSTRLEN(Description));
        VALIDATE("Currency Code",PostedDoc."Currency Code");
        VALIDATE(Amount,-Amount2);
        "Applies-to Doc. Type" := CustLedgEntry."Document Type";
        "Applies-to Doc. No." := '';
        "Applies-to Bill No." := CustLedgEntry."Bill No.";
        "Source Code" := SourceCode;
        "System-Created Entry" := TRUE;
        "Shortcut Dimension 1 Code" := CustLedgEntry."Global Dimension 1 Code";
        "Shortcut Dimension 2 Code" := CustLedgEntry."Global Dimension 2 Code";
        "Dimension Set ID" := CustLedgEntry."Dimension Set ID";
        INSERT;
      END;
    END;

    BEGIN
    END.
  }
  RDLDATA
  {
  }
}

