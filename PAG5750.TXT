OBJECT Page 5750 Inventory Comment Sheet
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Comment Sheet;
               PTG=Folha Coment rios];
    MultipleNewLines=Yes;
    LinksAllowed=No;
    SourceTable=Table5748;
    DelayedInsert=Yes;
    DataCaptionFields=Document Type,No.;
    PageType=List;
    AutoSplitKey=Yes;
    OnNewRecord=BEGIN
                  SetUpNewLine;
                END;

  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 2   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies when the comment was created.;
                           PTG=""];
                SourceExpr=Date }

    { 4   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the actual comment text.;
                           PTG=""];
                SourceExpr=Comment }

    { 6   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the code of the record.;
                           PTG=""];
                SourceExpr=Code;
                Visible=FALSE }

  }
  CODE
  {

    BEGIN
    END.
  }
}

