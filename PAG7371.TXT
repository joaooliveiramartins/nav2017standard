OBJECT Page 7371 Bin Content Creation Worksheet
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Bin Content Creation Worksheet;
               PTG=Folha Cria��o Conte�do Posi��o];
    SaveValues=Yes;
    SourceTable=Table7338;
    DelayedInsert=Yes;
    SourceTableView=SORTING(Worksheet Template Name,Name,Location Code,Line No.)
                    WHERE(Type=CONST(Bin Content));
    DataCaptionFields=Name;
    PageType=Worksheet;
    AutoSplitKey=Yes;
    OnOpenPage=VAR
                 WkshSelected@1000 : Boolean;
               BEGIN
                 OpenedFromBatch := (Name <> '') AND ("Worksheet Template Name" = '');
                 IF OpenedFromBatch THEN BEGIN
                   CurrentJnlBatchName := Name;
                   CurrentLocationCode := "Location Code";
                   BinCreateLine.OpenWksh(CurrentJnlBatchName,CurrentLocationCode,Rec);
                   EXIT;
                 END;
                 BinCreateLine.TemplateSelection(PAGE::"Bin Content Creation Worksheet",1,Rec,WkshSelected);
                 IF NOT WkshSelected THEN
                   ERROR('');
                 BinCreateLine.OpenWksh(CurrentJnlBatchName,CurrentLocationCode,Rec);
               END;

    OnNewRecord=BEGIN
                  SetUpNewLine(GETRANGEMAX("Worksheet Template Name"));
                END;

    OnAfterGetCurrRecord=BEGIN
                           BinCreateLine.GetItemDescr("Item No.","Variant Code",ItemDescription);
                           BinCreateLine.GetUnitOfMeasureDescr("Unit of Measure Code",UOMDescription);
                           BinCode := "Bin Code";
                         END;

    ActionList=ACTIONS
    {
      { 1900000004;0 ;ActionContainer;
                      ActionContainerType=ActionItems }
      { 23      ;1   ;ActionGroup;
                      CaptionML=[ENU=F&unctions;
                                 PTG=F&un��es];
                      Image=Action }
      { 25      ;2   ;Action    ;
                      Name=CreateBinContent;
                      AccessByPermission=TableData 7302=R;
                      ShortCutKey=F9;
                      CaptionML=[ENU=&Create Bin Content;
                                 PTG=&Criar Conte�do Posi��o];
                      Image=CreateBinContent;
                      OnAction=BEGIN
                                 BinCreateLine.COPY(Rec);
                                 CODEUNIT.RUN(CODEUNIT::"Bin Content Create",Rec);
                                 BinCreateLine.RESET;
                                 COPY(BinCreateLine);
                                 FILTERGROUP(2);
                                 SETRANGE("Worksheet Template Name","Worksheet Template Name");
                                 SETRANGE(Name,Name);
                                 SETRANGE("Location Code",CurrentLocationCode);
                                 FILTERGROUP(0);
                                 CurrPage.UPDATE(FALSE);
                               END;
                                }
      { 45      ;1   ;Action    ;
                      Ellipsis=Yes;
                      CaptionML=[ENU=&Print;
                                 PTG=&Imprimir];
                      Promoted=Yes;
                      Image=Print;
                      PromotedCategory=Process;
                      OnAction=BEGIN
                                 BinCreateLine.SETRANGE("Worksheet Template Name","Worksheet Template Name");
                                 BinCreateLine.SETRANGE(Name,Name);
                                 BinCreateLine.SETRANGE("Location Code","Location Code");
                                 BinCreateLine.SETRANGE(Type,BinCreateLine.Type::"Bin Content");
                                 REPORT.RUN(REPORT::"Bin Content Create Wksh Report",TRUE,FALSE,BinCreateLine);
                               END;
                                }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 35  ;1   ;Field     ;
                Lookup=Yes;
                CaptionML=[ENU=Name;
                           PTG=Nome];
                SourceExpr=CurrentJnlBatchName;
                OnValidate=BEGIN
                             BinCreateLine.CheckName(CurrentJnlBatchName,CurrentLocationCode,Rec);
                             CurrentJnlBatchNameOnAfterVali;
                           END;

                OnLookup=BEGIN
                           CurrPage.SAVERECORD;
                           BinCreateLine.LookupBinCreationName(CurrentJnlBatchName,CurrentLocationCode,Rec);
                           CurrPage.UPDATE(FALSE);
                         END;
                          }

    { 4   ;1   ;Field     ;
                Lookup=Yes;
                CaptionML=[ENU=Location Code;
                           PTG=C�d. Localiza��o];
                SourceExpr=CurrentLocationCode;
                TableRelation=Location;
                Editable=FALSE }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 19  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the code of the zone where the bin on the worksheet will be located.;
                           PTG=""];
                SourceExpr="Zone Code";
                Visible=FALSE }

    { 17  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the code of the bin on the line of the worksheet.;
                           PTG=""];
                SourceExpr="Bin Code";
                OnValidate=BEGIN
                             BinCodeOnAfterValidate;
                           END;
                            }

    { 15  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the bin type or bin content that should be created.;
                           PTG=""];
                SourceExpr="Bin Type Code";
                Visible=FALSE;
                Editable=FALSE }

    { 26  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the warehouse class of the bin or bin content that should be created.;
                           PTG=""];
                SourceExpr="Warehouse Class Code";
                Visible=FALSE;
                Editable=FALSE }

    { 33  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the ranking of the bin or bin content that should be created.;
                           PTG=""];
                SourceExpr="Bin Ranking";
                Visible=FALSE;
                Editable=FALSE }

    { 5   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the item number for which bin content should be created.;
                           PTG=""];
                SourceExpr="Item No.";
                OnValidate=BEGIN
                             BinCreateLine.GetItemDescr("Item No.","Variant Code",ItemDescription);
                             BinCreateLine.GetUnitOfMeasureDescr("Unit of Measure Code",UOMDescription);
                           END;
                            }

    { 11  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the variant code of the bin content that should be created.;
                           PTG=""];
                SourceExpr="Variant Code";
                Visible=FALSE;
                OnValidate=BEGIN
                             VariantCodeOnAfterValidate;
                           END;
                            }

    { 13  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the unit of measure code that you would like to use for bin contents in this particular bin.;
                           PTG=""];
                SourceExpr="Unit of Measure Code";
                Visible=FALSE;
                OnValidate=BEGIN
                             BinCreateLine.GetUnitOfMeasureDescr("Unit of Measure Code",UOMDescription);
                           END;
                            }

    { 7   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the minimum quantity for the bin content that should be created.;
                           PTG=""];
                SourceExpr="Min. Qty.";
                Visible=FALSE }

    { 9   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the maximum quantity for the bin content that should be created.;
                           PTG=""];
                SourceExpr="Max. Qty.";
                Visible=FALSE }

    { 21  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies how the movement of a particular item, or bin content, into or out of this bin, is blocked.;
                           PTG=""];
                SourceExpr="Block Movement";
                Visible=FALSE }

    { 29  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies whether the bin content that is to be created will be fixed for the item.;
                           PTG=""];
                SourceExpr=Fixed }

    { 43  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies if the bin is to be the default bin for the item on the bin worksheet line.;
                           PTG=""];
                SourceExpr=Default }

    { 41  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies new cross-dock bins.;
                           PTG=""];
                SourceExpr="Cross-Dock Bin";
                Visible=FALSE }

    { 2   ;1   ;Group      }

    { 1900116601;2;Group  ;
                GroupType=FixedLayout }

    { 1901742101;3;Group  ;
                CaptionML=[ENU=Bin Code;
                           PTG=C�d. Posi��o] }

    { 40  ;4   ;Field     ;
                SourceExpr=BinCode;
                Editable=FALSE;
                ShowCaption=No }

    { 1900545301;3;Group  ;
                CaptionML=[ENU=Item Description;
                           PTG=Descri��o Produto] }

    { 31  ;4   ;Field     ;
                CaptionML=[ENU=Item Description;
                           PTG=Descri��o Produto];
                SourceExpr=ItemDescription;
                Editable=FALSE }

    { 1901991701;3;Group  ;
                CaptionML=[ENU=Unit Of Measure Description;
                           PTG=Descri��o Unidade Medida] }

    { 38  ;4   ;Field     ;
                CaptionML=[ENU=Unit Of Measure Description;
                           PTG=Descri��o Unidade Medida];
                SourceExpr=UOMDescription;
                Editable=FALSE }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {
    VAR
      BinCreateLine@1005 : Record 7338;
      CurrentLocationCode@1006 : Code[10];
      CurrentJnlBatchName@1002 : Code[10];
      BinCode@1008 : Code[20];
      ItemDescription@1000 : Text[50];
      UOMDescription@1007 : Text[50];
      OpenedFromBatch@1001 : Boolean;

    LOCAL PROCEDURE BinCodeOnAfterValidate@19073508();
    BEGIN
      BinCreateLine.GetItemDescr("Item No.","Variant Code",ItemDescription);
      BinCreateLine.GetUnitOfMeasureDescr("Unit of Measure Code",UOMDescription);
      BinCode := "Bin Code";
    END;

    LOCAL PROCEDURE VariantCodeOnAfterValidate@19003320();
    BEGIN
      BinCreateLine.GetItemDescr("Item No.","Variant Code",ItemDescription);
    END;

    LOCAL PROCEDURE CurrentJnlBatchNameOnAfterVali@19002411();
    BEGIN
      CurrPage.SAVERECORD;
      BinCreateLine.SetName(CurrentJnlBatchName,CurrentLocationCode,Rec);
      CurrPage.UPDATE(FALSE);
    END;

    BEGIN
    END.
  }
}

