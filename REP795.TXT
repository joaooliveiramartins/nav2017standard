OBJECT Report 795 Adjust Cost - Item Entries
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0;
  }
  PROPERTIES
  {
    Permissions=TableData 32=rimd,
                TableData 339=r,
                TableData 5802=rimd,
                TableData 5804=rimd;
    CaptionML=[ENU=Adjust Cost - Item Entries;
               PTG=Ajustar Custo - Movs. Produto];
    ProcessingOnly=Yes;
    OnPreReport=VAR
                  ItemLedgEntry@1000 : Record 32;
                  ValueEntry@1001 : Record 5802;
                  ItemApplnEntry@1002 : Record 339;
                  AvgCostAdjmtEntryPoint@1003 : Record 5804;
                  Item@1006 : Record 27;
                  UpdateItemAnalysisView@1004 : Codeunit 7150;
                BEGIN
                  ItemApplnEntry.LOCKTABLE;
                  IF NOT ItemApplnEntry.FINDLAST THEN
                    EXIT;
                  ItemLedgEntry.LOCKTABLE;
                  IF NOT ItemLedgEntry.FINDLAST THEN
                    EXIT;
                  AvgCostAdjmtEntryPoint.LOCKTABLE;
                  IF AvgCostAdjmtEntryPoint.FINDLAST THEN;
                  ValueEntry.LOCKTABLE;
                  IF NOT ValueEntry.FINDLAST THEN
                    EXIT;

                  IF (ItemNoFilter <> '') AND (ItemCategoryFilter <> '') THEN
                    ERROR(Text005);

                  IF ItemNoFilter <> '' THEN
                    Item.SETFILTER("No.",ItemNoFilter);
                  IF ItemCategoryFilter <> '' THEN
                    Item.SETFILTER("Item Category Code",ItemCategoryFilter);

                  InvtAdjmt.SetProperties(FALSE,PostToGL);
                  InvtAdjmt.SetFilterItem(Item);
                  InvtAdjmt.MakeMultiLevelAdjmt;

                  UpdateItemAnalysisView.UpdateAll(0,TRUE);
                END;

  }
  DATASET
  {
  }
  REQUESTPAGE
  {
    PROPERTIES
    {
      SaveValues=Yes;
      OnInit=BEGIN
               FilterItemCategoryEditable := TRUE;
               FilterItemNoEditable := TRUE;
               PostEnable := TRUE;
             END;

      OnOpenPage=BEGIN
                   InvtSetup.GET;
                   PostToGL := InvtSetup."Automatic Cost Posting";
                   PostEnable := PostToGL;
                 END;

    }
    CONTROLS
    {
      { 1900000001;0;Container;
                  ContainerType=ContentArea }

      { 1900000002;1;Group  ;
                  CaptionML=[ENU=Options;
                             PTG=Op��es] }

      { 3   ;2   ;Field     ;
                  Name=FilterItemNo;
                  CaptionML=[ENU=Item No. Filter;
                             PTG=Filtro N� Produto];
                  ToolTipML=ENU=Specifies a filter to run the Adjust Cost - Item Entries batch job for only certain items. You can leave this field blank to run the batch job for all items.;
                  ApplicationArea=#Basic,#Suite;
                  SourceExpr=ItemNoFilter;
                  TableRelation=Item;
                  Editable=FilterItemNoEditable }

      { 5   ;2   ;Field     ;
                  Name=FilterItemCategory;
                  CaptionML=[ENU=Item Category Filter;
                             PTG=Filtro Categoria Produto];
                  ToolTipML=ENU=Specifies a filter to run the Adjust Cost - Item Entries batch job for only certain item categories. You can leave this field blank to run the batch job for all item categories.;
                  ApplicationArea=#Basic,#Suite;
                  SourceExpr=ItemCategoryFilter;
                  TableRelation="Item Category";
                  Editable=FilterItemCategoryEditable }

      { 7   ;2   ;Field     ;
                  Name=Post;
                  CaptionML=[ENU=Post to G/L;
                             PTG=Registo na C/G];
                  ToolTipML=ENU=Specifies that inventory values created during the Adjust Cost - Item Entries batch job are posted to the inventory accounts in the general ledger. The option is only available if the Automatic Cost Posting check box is selected in the Inventory Setup window.;
                  ApplicationArea=#Basic,#Suite;
                  SourceExpr=PostToGL;
                  Enabled=PostEnable;
                  OnValidate=VAR
                               ObjTransl@1002 : Record 377;
                             BEGIN
                               IF NOT PostToGL THEN
                                 MESSAGE(
                                   ResynchronizeInfoMsg,
                                   ObjTransl.TranslateObject(ObjTransl."Object Type"::Report,REPORT::"Post Inventory Cost to G/L"));
                             END;
                              }

    }
  }
  LABELS
  {
  }
  CODE
  {
    VAR
      ResynchronizeInfoMsg@1008 : TextConst 'ENU=Your general and item ledgers will no longer be synchronized after running the cost adjustment. You must run the %1 report to synchronize them again.;PTG=Aten��o que os di�rios geral e de produtos j� n�o ser�o sincronizados depois de executar o ajuste de custo! Deve executar o mapa %1 para sincroniz�-los novamente.';
      InvtSetup@1006 : Record 313;
      InvtAdjmt@1001 : Codeunit 5895;
      ItemNoFilter@1004 : Text[250];
      ItemCategoryFilter@1005 : Text[250];
      Text005@1003 : TextConst 'ENU=You must not use Item No. Filter and Item Category Filter at the same time.;PTG=N�o deve usar o Filtro N� Produto e Filtro Categ. Produto ao mesmo tempo.';
      PostToGL@1007 : Boolean;
      PostEnable@19010160 : Boolean INDATASET;
      FilterItemNoEditable@19034962 : Boolean INDATASET;
      FilterItemCategoryEditable@19002320 : Boolean INDATASET;

    PROCEDURE InitializeRequest@2(NewItemNoFilter@1000 : Text[250];NewItemCategoryFilter@1001 : Text[250]);
    BEGIN
      ItemNoFilter := NewItemNoFilter;
      ItemCategoryFilter := NewItemCategoryFilter;
    END;

    BEGIN
    END.
  }
  RDLDATA
  {
  }
}

