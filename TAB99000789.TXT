OBJECT Table 99000789 Production Matrix  BOM Entry
{
  OBJECT-PROPERTIES
  {
    Date=05/08/11;
    Time=12:00:00;
    Version List=NAVW16.00;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Production Matrix  BOM Entry;
               PTG=Mov. L.M. Matriz de Produ��o];
  }
  FIELDS
  {
    { 1   ;   ;Item No.            ;Code20        ;TableRelation=Item;
                                                   CaptionML=[ENU=Item No.;
                                                              PTG=N� Produto] }
    { 2   ;   ;Variant Code        ;Code10        ;TableRelation="Item Variant".Code WHERE (Item No.=FIELD(Item No.));
                                                   CaptionML=[ENU=Variant Code;
                                                              PTG=C�d. Variante] }
    { 3   ;   ;ID                  ;Code20        ;CaptionML=[ENU=ID;
                                                              PTG=ID] }
    { 20  ;   ;Quantity            ;Decimal       ;CaptionML=[ENU=Quantity;
                                                              PTG=Quantidade];
                                                   DecimalPlaces=0:5 }
  }
  KEYS
  {
    {    ;Item No.,Variant Code,ID                ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    END.
  }
}

