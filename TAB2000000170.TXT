OBJECT Table 2000000170 Configuration Package File
{
  OBJECT-PROPERTIES
  {
    Date=13/04/17;
    Time=14:36:28;
    Version List=NAVW110.0;
  }
  PROPERTIES
  {
    DataPerCompany=No;
    CaptionML=[ENU=Configuration Package File;
               PTG=Pacote de Configura��o];
  }
  FIELDS
  {
    { 1   ;   ;Code                ;Code20        ;CaptionML=[ENU=Code;
                                                              PTG=C�d.] }
    { 2   ;   ;Name                ;Text50        ;CaptionML=[ENU=Name;
                                                              PTG=Nome] }
    { 3   ;   ;Language ID         ;Integer       ;CaptionML=[ENU=Language ID;
                                                              PTG=ID L�ngua] }
    { 4   ;   ;Setup Type          ;Option        ;CaptionML=[ENU=Setup Type;
                                                              PTG=Tipo Config.];
                                                   OptionCaptionML=[ENU=Company,Application,Other;
                                                                    PTG=Empresa,Aplica��o,Outro];
                                                   OptionString=Company,Application,Other }
    { 5   ;   ;Processing Order    ;Integer       ;CaptionML=[ENU=Processing Order;
                                                              PTG=Ordem Processamento] }
    { 6   ;   ;Package             ;BLOB          ;CaptionML=[ENU=Package;
                                                              PTG=Pacote] }
  }
  KEYS
  {
    {    ;Code                                    ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    END.
  }
}

