OBJECT Table 305 Issued Fin. Charge Memo Line
{
  OBJECT-PROPERTIES
  {
    Date=21/12/16;
    Time=13:00:00;
    Version List=NAVW17.10,NAVPTSS93.00;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Issued Fin. Charge Memo Line;
               PTG=Linha Nota Juros Emitida];
  }
  FIELDS
  {
    { 1   ;   ;Finance Charge Memo No.;Code20     ;TableRelation="Issued Fin. Charge Memo Header";
                                                   CaptionML=[ENU=Finance Charge Memo No.;
                                                              PTG=N� Nota Juros] }
    { 2   ;   ;Line No.            ;Integer       ;CaptionML=[ENU=Line No.;
                                                              PTG=N� Linha] }
    { 3   ;   ;Attached to Line No.;Integer       ;TableRelation="Issued Fin. Charge Memo Line"."Line No." WHERE (Finance Charge Memo No.=FIELD(Finance Charge Memo No.));
                                                   CaptionML=[ENU=Attached to Line No.;
                                                              PTG=Ligado � Linha N�] }
    { 4   ;   ;Type                ;Option        ;CaptionML=[ENU=Type;
                                                              PTG=Tipo];
                                                   OptionCaptionML=[ENU=" ,G/L Account,Customer Ledger Entry";
                                                                    PTG=" ,Conta C/G,Mov. Cliente"];
                                                   OptionString=[ ,G/L Account,Customer Ledger Entry] }
    { 5   ;   ;Entry No.           ;Integer       ;TableRelation="Cust. Ledger Entry";
                                                   OnLookup=BEGIN
                                                              IF Type <> Type::"Customer Ledger Entry" THEN
                                                                EXIT;
                                                              IssuedFinChrgMemoHeader.GET("Finance Charge Memo No.");
                                                              CustLedgEntry.SETCURRENTKEY("Customer No.");
                                                              CustLedgEntry.SETRANGE("Customer No.",IssuedFinChrgMemoHeader."Customer No.");
                                                              IF CustLedgEntry.GET("Entry No.") THEN;
                                                              PAGE.RUNMODAL(0,CustLedgEntry);
                                                            END;

                                                   CaptionML=[ENU=Entry No.;
                                                              PTG=N� Mov.];
                                                   BlankZero=Yes }
    { 7   ;   ;Posting Date        ;Date          ;CaptionML=[ENU=Posting Date;
                                                              PTG=Data Registo] }
    { 8   ;   ;Document Date       ;Date          ;CaptionML=[ENU=Document Date;
                                                              PTG=Data Documento] }
    { 9   ;   ;Due Date            ;Date          ;CaptionML=[ENU=Due Date;
                                                              PTG=Data Vencimento] }
    { 10  ;   ;Document Type       ;Option        ;CaptionML=[ENU=Document Type;
                                                              PTG=Tipo Documento];
                                                   OptionCaptionML=[ENU=" ,Payment,Invoice,Credit Memo,Finance Charge Memo,Reminder,Refund,,,,,,Bill";
                                                                    PTG=" ,Pagamento,Fatura,Nota Cr�dito,Nota Juros,Carta Aviso,Reembolso,,,,,,T�tulo"];
                                                   OptionString=[ ,Payment,Invoice,Credit Memo,Finance Charge Memo,Reminder,Refund,,,,,,Bill];
                                                   Description=soft }
    { 11  ;   ;Document No.        ;Code20        ;OnLookup=BEGIN
                                                              IF Type <> Type::"Customer Ledger Entry" THEN
                                                                EXIT;
                                                              IssuedFinChrgMemoHeader.GET("Finance Charge Memo No.");
                                                              CustLedgEntry.SETCURRENTKEY("Customer No.");
                                                              CustLedgEntry.SETRANGE("Customer No.",IssuedFinChrgMemoHeader."Customer No.");
                                                              IF CustLedgEntry.GET("Entry No.") THEN;
                                                              PAGE.RUNMODAL(0,CustLedgEntry);
                                                            END;

                                                   CaptionML=[ENU=Document No.;
                                                              PTG=N� Documento] }
    { 12  ;   ;Description         ;Text100       ;CaptionML=[ENU=Description;
                                                              PTG=Descri��o] }
    { 13  ;   ;Original Amount     ;Decimal       ;CaptionML=[ENU=Original Amount;
                                                              PTG=Valor Original];
                                                   BlankZero=Yes;
                                                   AutoFormatType=1;
                                                   AutoFormatExpr=GetCurrencyCode }
    { 14  ;   ;Remaining Amount    ;Decimal       ;CaptionML=[ENU=Remaining Amount;
                                                              PTG=Valor Pendente];
                                                   BlankZero=Yes;
                                                   AutoFormatType=1;
                                                   AutoFormatExpr=GetCurrencyCode }
    { 15  ;   ;No.                 ;Code20        ;TableRelation=IF (Type=CONST(" ")) "Standard Text"
                                                                 ELSE IF (Type=CONST(G/L Account)) "G/L Account";
                                                   CaptionML=[ENU=No.;
                                                              PTG=N�] }
    { 16  ;   ;Amount              ;Decimal       ;CaptionML=[ENU=Amount;
                                                              PTG=Valor];
                                                   BlankZero=Yes;
                                                   AutoFormatType=1;
                                                   AutoFormatExpr=GetCurrencyCode }
    { 17  ;   ;Interest Rate       ;Decimal       ;CaptionML=[ENU=Interest Rate;
                                                              PTG=Taxa Juros];
                                                   DecimalPlaces=0:5;
                                                   BlankZero=Yes }
    { 18  ;   ;Gen. Prod. Posting Group;Code10    ;TableRelation="Gen. Product Posting Group";
                                                   CaptionML=[ENU=Gen. Prod. Posting Group;
                                                              PTG=Gr. Contabil�stico Produto] }
    { 19  ;   ;VAT %               ;Decimal       ;CaptionML=[ENU=VAT %;
                                                              PTG=% IVA];
                                                   DecimalPlaces=0:5 }
    { 20  ;   ;VAT Calculation Type;Option        ;CaptionML=[ENU=VAT Calculation Type;
                                                              PTG=Tipo C�lculo IVA];
                                                   OptionCaptionML=[ENU=Normal VAT,Reverse Charge VAT,Full VAT,Sales Tax,,,,,,No Taxable VAT,Stamp Duty;
                                                                    PTG=IVA Normal,IVA Conta Autoliquida��o,IVA Total,Impostos Vendas,,,,,,N�o Sujeito,Imposto Selo];
                                                   OptionString=Normal VAT,Reverse Charge VAT,Full VAT,Sales Tax,,,,,,No Taxable VAT,Stamp Duty;
                                                   Description=V93.00#00023 }
    { 21  ;   ;VAT Amount          ;Decimal       ;CaptionML=[ENU=VAT Amount;
                                                              PTG=Valor IVA];
                                                   AutoFormatType=1;
                                                   AutoFormatExpr=GetCurrencyCode }
    { 22  ;   ;Tax Group Code      ;Code10        ;TableRelation="Tax Group";
                                                   CaptionML=[ENU=Tax Group Code;
                                                              PTG=C�d. Grupo Imposto] }
    { 23  ;   ;VAT Prod. Posting Group;Code10     ;TableRelation="VAT Product Posting Group";
                                                   CaptionML=[ENU=VAT Prod. Posting Group;
                                                              PTG=Gr. Registo IVA Produto] }
    { 24  ;   ;VAT Identifier      ;Code10        ;CaptionML=[ENU=VAT Identifier;
                                                              PTG=Identificador IVA];
                                                   Editable=No }
    { 26  ;   ;VAT Clause Code     ;Code10        ;TableRelation="VAT Clause";
                                                   CaptionML=[ENU=VAT Clause Code;
                                                              PTG=C�d. Norma Isen��o Legal IVA] }
    { 101 ;   ;System-Created Entry;Boolean       ;CaptionML=[ENU=System-Created Entry;
                                                              PTG=Lan�amento Autom�tico];
                                                   Editable=No }
    { 31022897;;DRF Code           ;Code10        ;TableRelation="DRF Codes"."DRF Code";
                                                   CaptionML=[ENU=DRF Code;
                                                              PTG=C�d. DRF] }
  }
  KEYS
  {
    {    ;Finance Charge Memo No.,Line No.        ;SumIndexFields=Amount,VAT Amount,Remaining Amount;
                                                   MaintainSIFTIndex=No;
                                                   Clustered=Yes }
    {    ;Finance Charge Memo No.,Type            ;SumIndexFields=Amount,VAT Amount,Remaining Amount;
                                                   MaintainSQLIndex=No;
                                                   MaintainSIFTIndex=No }
  }
  FIELDGROUPS
  {
  }
  CODE
  {
    VAR
      IssuedFinChrgMemoHeader@1000 : Record 304;
      CustLedgEntry@1001 : Record 21;

    PROCEDURE GetCurrencyCode@1() : Code[10];
    VAR
      IssuedFinChrgMemoHeader@1000 : Record 304;
    BEGIN
      IF "Finance Charge Memo No." = IssuedFinChrgMemoHeader."No." THEN
        EXIT(IssuedFinChrgMemoHeader."Currency Code");

      IF IssuedFinChrgMemoHeader.GET("Finance Charge Memo No.") THEN
        EXIT(IssuedFinChrgMemoHeader."Currency Code");

      EXIT('');
    END;

    BEGIN
    {
      V93.00#00023 - Imposto Selo - SAFT - IRC - 2016.03.31
    }
    END.
  }
}

