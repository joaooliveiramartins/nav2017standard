OBJECT Page 5615 FA Classes
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0;
  }
  PROPERTIES
  {
    CaptionML=[ENU=FA Classes;
               PTG=Classes Imobilizado];
    SourceTable=Table5607;
    PageType=List;
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 2   ;2   ;Field     ;
                ToolTipML=ENU=Specifies a code for the class that the fixed asset belongs to.;
                ApplicationArea=#FixedAssets;
                SourceExpr=Code }

    { 4   ;2   ;Field     ;
                ToolTipML=ENU=Specifies the name of the fixed asset class.;
                ApplicationArea=#FixedAssets;
                SourceExpr=Name }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {

    BEGIN
    END.
  }
}

