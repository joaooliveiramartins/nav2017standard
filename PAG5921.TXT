OBJECT Page 5921 Available Loaners
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Available Loaners;
               PTG=Empr�stimos Dispon�veis];
    SourceTable=Table5913;
    PageType=List;
    OnOpenPage=BEGIN
                 SETRANGE(Blocked,FALSE);
                 SETRANGE(Lent,FALSE);
               END;

    ActionList=ACTIONS
    {
      { 1900000003;0 ;ActionContainer;
                      ActionContainerType=RelatedInformation }
      { 20      ;1   ;ActionGroup;
                      CaptionML=[ENU=L&oaner;
                                 PTG=Empr�stim&o];
                      Image=Loaners }
      { 22      ;2   ;Action    ;
                      ShortCutKey=Shift+F7;
                      CaptionML=[ENU=Card;
                                 PTG=Ficha];
                      RunObject=Page 5922;
                      RunPageLink=No.=FIELD(No.);
                      Image=EditLines }
      { 23      ;2   ;Action    ;
                      CaptionML=[ENU=Co&mments;
                                 PTG=Co&ment�rios];
                      RunObject=Page 5911;
                      RunPageLink=Table Name=CONST(Loaner),
                                  No.=FIELD(No.);
                      Image=ViewComments }
      { 24      ;2   ;Action    ;
                      ShortCutKey=Ctrl+F7;
                      CaptionML=[ENU=Loaner E&ntries;
                                 PTG=Mo&vs. Empr�stimo];
                      RunObject=Page 5924;
                      RunPageView=SORTING(Loaner No.)
                                  ORDER(Ascending);
                      RunPageLink=Loaner No.=FIELD(No.);
                      Image=Entries }
      { 1900000004;0 ;ActionContainer;
                      ActionContainerType=ActionItems }
      { 21      ;1   ;ActionGroup;
                      CaptionML=[ENU=F&unctions;
                                 PTG=F&un��es];
                      Image=Action }
      { 25      ;2   ;Action    ;
                      CaptionML=[ENU=Receive;
                                 PTG=Receber];
                      Image=ReceiveLoaner;
                      OnAction=VAR
                                 LoanerEntry@1001 : Record 5914;
                                 ServItemLine@1002 : Record 5901;
                                 ServLoanerMgt@1003 : Codeunit 5901;
                               BEGIN
                                 IF Lent THEN  BEGIN
                                   CLEAR(LoanerEntry);
                                   LoanerEntry.SETCURRENTKEY("Document Type","Document No.","Loaner No.",Lent);
                                   LoanerEntry.SETRANGE("Document Type","Document Type");
                                   LoanerEntry.SETRANGE("Document No.","Document No.");
                                   LoanerEntry.SETRANGE("Loaner No.","No.");
                                   LoanerEntry.SETRANGE(Lent,TRUE);
                                   IF LoanerEntry.FINDFIRST THEN BEGIN
                                     ServItemLine.GET(LoanerEntry."Document Type" - 1,LoanerEntry."Document No.",LoanerEntry."Service Item Line No.");
                                     ServLoanerMgt.ReceiveLoaner(ServItemLine);
                                   END;
                                 END ELSE
                                   ERROR(Text000,TABLECAPTION,"No.");
                               END;
                                }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                Editable=FALSE;
                GroupType=Repeater }

    { 2   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the number of the loaner.;
                           PTG=""];
                SourceExpr="No." }

    { 19  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the serial number for the loaner for the service item.;
                           PTG=""];
                SourceExpr="Serial No." }

    { 4   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies a description of the loaner.;
                           PTG=""];
                SourceExpr=Description }

    { 17  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies that there is a comment for this loaner.;
                           PTG=""];
                SourceExpr=Comment }

    { 8   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies that the loaner is blocked and cannot be lent to customers.;
                           PTG=""];
                SourceExpr=Blocked }

    { 6   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies that the loaner has been lent to a customer.;
                           PTG=""];
                SourceExpr=Lent }

    { 27  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the document type of the loaner entry.;
                           PTG=""];
                SourceExpr="Document Type" }

    { 13  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the number of the service document for the service item that was lent.;
                           PTG=""];
                SourceExpr="Document No." }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                Visible=TRUE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {
    VAR
      Text000@1000 : TextConst '@@@=You cannot receive Loaner L00001 because it has not been lent.;ENU=You cannot receive %1 %2 because it has not been lent.;PTG=N�o pode receber %1 %2 porque n�o foi emprestado.';

    BEGIN
    END.
  }
}

