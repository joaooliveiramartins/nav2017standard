OBJECT Page 5660 Depreciation Table Lines
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Lines;
               PTG=Linhas];
    LinksAllowed=No;
    SourceTable=Table5643;
    DelayedInsert=Yes;
    PageType=ListPart;
    OnNewRecord=BEGIN
                  NewRecord;
                END;

  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 2   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the number of the depreciation period that this line applies to.;
                           PTG=""];
                ApplicationArea=#FixedAssets;
                SourceExpr="Period No." }

    { 4   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the depreciation percentage to apply to the period for this line.;
                           PTG=""];
                ApplicationArea=#FixedAssets;
                SourceExpr="Period Depreciation %" }

    { 6   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the units produced by the asset this depreciation table applies to, during the period when this line applies.;
                           PTG=""];
                ApplicationArea=#FixedAssets;
                SourceExpr="No. of Units in Period" }

  }
  CODE
  {

    BEGIN
    END.
  }
}

