OBJECT Page 6055 Service Contract Template
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Service Contract Template;
               PTG=Modelo Contrato Servi�o];
    SourceTable=Table5968;
    PageType=Card;
    OnInit=BEGIN
             InvoiceAfterServiceEnable := TRUE;
             PrepaidEnable := TRUE;
           END;

    OnOpenPage=BEGIN
                 ActivateFields;
               END;

    OnAfterGetCurrRecord=BEGIN
                           ActivateFields;
                         END;

    ActionList=ACTIONS
    {
      { 1900000003;0 ;ActionContainer;
                      ActionContainerType=RelatedInformation }
      { 37      ;1   ;ActionGroup;
                      CaptionML=[ENU=&Contract Template;
                                 PTG=Modelo &Contrato];
                      Image=Template }
      { 21      ;2   ;Action    ;
                      ShortCutKey=Shift+Ctrl+D;
                      CaptionML=[ENU=Dimensions;
                                 PTG=Dimens�es];
                      ToolTipML=[ENU=View or edit dimensions, such as area, project, or department, that you can assign to sales and purchase documents to distribute costs and analyze transaction history.;
                                 PTG=""];
                      RunObject=Page 540;
                      RunPageLink=Table ID=CONST(5968),
                                  No.=FIELD(No.);
                      Image=Dimensions }
      { 17      ;2   ;Separator ;
                      CaptionML=[ENU="";
                                 PTG=""] }
      { 52      ;2   ;Action    ;
                      CaptionML=[ENU=Service Dis&counts;
                                 PTG=Des&contos Servi�o];
                      RunObject=Page 6058;
                      RunPageLink=Contract Type=CONST(Template),
                                  Contract No.=FIELD(No.);
                      Image=Discount }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                CaptionML=[ENU=General;
                           PTG=Geral] }

    { 2   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the number of the service contract template.;
                           PTG=""];
                SourceExpr="No.";
                OnAssistEdit=BEGIN
                               AssistEdit(Rec);
                             END;
                              }

    { 58  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies a description of the service contract.;
                           PTG=""];
                SourceExpr=Description }

    { 40  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the contract group code of the service contract.;
                           PTG=""];
                SourceExpr="Contract Group Code" }

    { 10  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the service order type assigned to service orders linked to this service contract.;
                           PTG=""];
                SourceExpr="Service Order Type" }

    { 51  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the default service period for the items in the contract.;
                           PTG=""];
                SourceExpr="Default Service Period" }

    { 8   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the price update period for this service contract.;
                           PTG=""];
                SourceExpr="Price Update Period" }

    { 67  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the default response time for the service contract created from this service contract template.;
                           PTG=""];
                SourceExpr="Default Response Time (Hours)" }

    { 65  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the maximum unit price that can be set for a resource on lines for service orders associated with the service contract.;
                           PTG=""];
                SourceExpr="Max. Labor Unit Price" }

    { 1904200701;1;Group  ;
                CaptionML=[ENU=Invoice;
                           PTG=Fatura] }

    { 24  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the code associated with the service contract account group.;
                           PTG=""];
                SourceExpr="Serv. Contract Acc. Gr. Code" }

    { 14  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the invoice period for the service contract.;
                           PTG=""];
                SourceExpr="Invoice Period" }

    { 22  ;2   ;Field     ;
                CaptionML=[ENU=Contract Increase Text;
                           PTG=Texto Aumento Contrato];
                ToolTipML=[ENU=Specifies all billable prices for the job task, expressed in the local currency.;
                           PTG=""];
                SourceExpr="Price Inv. Increase Code" }

    { 6   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies that this service contract is prepaid.;
                           PTG=""];
                SourceExpr=Prepaid;
                Enabled=PrepaidEnable;
                OnValidate=BEGIN
                             PrepaidOnAfterValidate;
                           END;
                            }

    { 19  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies if the contents of the Calcd. Annual Amount field are copied into the Annual Amount field in the service contract or contract quote.;
                           PTG=""];
                SourceExpr="Allow Unbalanced Amounts" }

    { 12  ;2   ;Field     ;
                SourceExpr="Combine Invoices" }

    { 66  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies that a credit memo is created when you remove a contract line from the service contract under certain conditions.;
                           PTG=""];
                SourceExpr="Automatic Credit Memos" }

    { 16  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies you want contract lines to appear as text on the invoice.;
                           PTG=""];
                SourceExpr="Contract Lines on Invoice" }

    { 42  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies you can only invoice the contract if you have posted a service order linked to the contract since you last invoiced the contract.;
                           PTG=""];
                SourceExpr="Invoice after Service";
                Enabled=InvoiceAfterServiceEnable;
                OnValidate=BEGIN
                             InvoiceafterServiceOnAfterVali;
                           END;
                            }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {
    VAR
      PrepaidEnable@19025160 : Boolean INDATASET;
      InvoiceAfterServiceEnable@19024761 : Boolean INDATASET;

    LOCAL PROCEDURE ActivateFields@2();
    BEGIN
      PrepaidEnable := (NOT "Invoice after Service" OR Prepaid);
      InvoiceAfterServiceEnable := (NOT Prepaid OR "Invoice after Service");
    END;

    LOCAL PROCEDURE InvoiceafterServiceOnAfterVali@19065496();
    BEGIN
      ActivateFields;
    END;

    LOCAL PROCEDURE PrepaidOnAfterValidate@19004759();
    BEGIN
      ActivateFields;
    END;

    BEGIN
    END.
  }
}

