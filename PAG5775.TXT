OBJECT Page 5775 Warehouse Setup
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Warehouse Setup;
               PTG=Config. Gest�o Armaz�m];
    InsertAllowed=No;
    DeleteAllowed=No;
    SourceTable=Table5769;
    PageType=Card;
    OnOpenPage=BEGIN
                 RESET;
                 IF NOT GET THEN BEGIN
                   INIT;
                   INSERT;
                 END;
               END;

  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                CaptionML=[ENU=General;
                           PTG=Geral] }

    { 22  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies whether you require users to use the receive activity.;
                           PTG=""];
                SourceExpr="Require Receive" }

    { 30  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies whether you require users to use the put-away activity.;
                           PTG=""];
                SourceExpr="Require Put-away" }

    { 10  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies if warehouse shipments are required in warehouse work flows.;
                           PTG=""];
                SourceExpr="Require Shipment" }

    { 32  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies whether you require users to use the pick activity.;
                           PTG=""];
                SourceExpr="Require Pick" }

    { 26  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies that the document reference of the last warehouse posting will be shown.;
                           PTG=""];
                SourceExpr="Last Whse. Posting Ref. No." }

    { 28  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies what should happen if errors occur when warehouse receipts are posted.;
                           PTG=""];
                SourceExpr="Receipt Posting Policy" }

    { 34  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies what should happen if errors occur when warehouse shipments are posted.;
                           PTG=""];
                SourceExpr="Shipment Posting Policy" }

    { 1904569201;1;Group  ;
                CaptionML=[ENU=Numbering;
                           PTG=Numera��o] }

    { 2   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the number series code to use when you assign numbers to warehouse receipt journals.;
                           PTG=""];
                SourceExpr="Whse. Receipt Nos." }

    { 24  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the number series code you want used when you assign numbers to warehouse shipment journals.;
                           PTG=""];
                SourceExpr="Whse. Ship Nos." }

    { 44  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the number series code used to assign numbers to internal put-always.;
                           PTG=""];
                SourceExpr="Whse. Internal Put-away Nos." }

    { 48  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the number series code used to assign numbers to internal picks.;
                           PTG=""];
                SourceExpr="Whse. Internal Pick Nos." }

    { 4   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the number series code you want used when you assign numbers to warehouse put-away documents.;
                           PTG=""];
                SourceExpr="Whse. Put-away Nos." }

    { 8   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the number series code you want used when you assign numbers to warehouse pick documents.;
                           PTG=""];
                SourceExpr="Whse. Pick Nos." }

    { 38  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the number series code used to assign numbers to posted warehouse receipts.;
                           PTG=""];
                SourceExpr="Posted Whse. Receipt Nos." }

    { 42  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the number series code used to assign numbers to posted warehouse shipments.;
                           PTG=""];
                SourceExpr="Posted Whse. Shipment Nos." }

    { 18  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the number series code used when numbers are assigned to registered put-away documents.;
                           PTG=""];
                SourceExpr="Registered Whse. Put-away Nos." }

    { 12  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the number series code you want to be used to assign numbers to registered pick documents.;
                           PTG=""];
                SourceExpr="Registered Whse. Pick Nos." }

    { 52  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the number series code used to assign numbers to warehouse movements.;
                           PTG=""];
                SourceExpr="Whse. Movement Nos." }

    { 6   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the number series code used to assign numbers to registered warehouse movements.;
                           PTG=""];
                SourceExpr="Registered Whse. Movement Nos." }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {

    BEGIN
    END.
  }
}

