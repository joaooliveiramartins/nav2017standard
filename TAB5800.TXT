OBJECT Table 5800 Item Charge
{
  OBJECT-PROPERTIES
  {
    Date=15/09/15;
    Time=12:00:00;
    Version List=NAVW19.00;
  }
  PROPERTIES
  {
    DataCaptionFields=No.,Description;
    OnModify=BEGIN
               DimMgt.UpdateDefaultDim(
                 DATABASE::"Item Charge","No.",
                 "Global Dimension 1 Code","Global Dimension 2 Code");
             END;

    OnDelete=BEGIN
               DimMgt.DeleteDefaultDim(DATABASE::"Item Charge","No.");
             END;

    OnRename=VAR
               SalesLine@1000 : Record 37;
               PurchLine@1001 : Record 39;
             BEGIN
               SalesLine.RenameNo(SalesLine.Type::"Charge (Item)",xRec."No.","No.");
               PurchLine.RenameNo(PurchLine.Type::"Charge (Item)",xRec."No.","No.");
             END;

    CaptionML=[ENU=Item Charge;
               PTG=Encargo Produto];
    LookupPageID=Page5800;
    DrillDownPageID=Page5800;
  }
  FIELDS
  {
    { 1   ;   ;No.                 ;Code20        ;AltSearchField=Search Description;
                                                   CaptionML=[ENU=No.;
                                                              PTG=N�];
                                                   NotBlank=Yes }
    { 2   ;   ;Description         ;Text50        ;OnValidate=BEGIN
                                                                IF ("Search Description" = UPPERCASE(xRec.Description)) OR ("Search Description" = '') THEN
                                                                  "Search Description" := Description;
                                                              END;

                                                   CaptionML=[ENU=Description;
                                                              PTG=Descri��o] }
    { 3   ;   ;Gen. Prod. Posting Group;Code10    ;TableRelation="Gen. Product Posting Group";
                                                   OnValidate=VAR
                                                                GenProdPostingGrp@1000 : Record 251;
                                                              BEGIN
                                                                IF xRec."Gen. Prod. Posting Group" <> "Gen. Prod. Posting Group" THEN
                                                                  IF GenProdPostingGrp.ValidateVatProdPostingGroup(GenProdPostingGrp,"Gen. Prod. Posting Group") THEN
                                                                    VALIDATE("VAT Prod. Posting Group",GenProdPostingGrp."Def. VAT Prod. Posting Group");
                                                              END;

                                                   CaptionML=[ENU=Gen. Prod. Posting Group;
                                                              PTG=Gr. Contabil�stico Produto] }
    { 4   ;   ;Tax Group Code      ;Code10        ;TableRelation="Tax Group";
                                                   CaptionML=[ENU=Tax Group Code;
                                                              PTG=C�d. Grupo Imposto] }
    { 5   ;   ;VAT Prod. Posting Group;Code10     ;TableRelation="VAT Product Posting Group";
                                                   CaptionML=[ENU=VAT Prod. Posting Group;
                                                              PTG=Gr. Registo IVA Produto] }
    { 6   ;   ;Search Description  ;Code50        ;CaptionML=[ENU=Search Description;
                                                              PTG=Alias Descri��o] }
    { 7   ;   ;Global Dimension 1 Code;Code20     ;TableRelation="Dimension Value".Code WHERE (Global Dimension No.=CONST(1));
                                                   OnValidate=BEGIN
                                                                ValidateShortcutDimCode(1,"Global Dimension 1 Code");
                                                              END;

                                                   CaptionML=[ENU=Global Dimension 1 Code;
                                                              PTG=C�d. Dimens�o 1 Global];
                                                   CaptionClass='1,1,1' }
    { 8   ;   ;Global Dimension 2 Code;Code20     ;TableRelation="Dimension Value".Code WHERE (Global Dimension No.=CONST(2));
                                                   OnValidate=BEGIN
                                                                ValidateShortcutDimCode(2,"Global Dimension 2 Code");
                                                              END;

                                                   CaptionML=[ENU=Global Dimension 2 Code;
                                                              PTG=C�d. Dimens�o 2 Global];
                                                   CaptionClass='1,1,2' }
  }
  KEYS
  {
    {    ;No.                                     ;Clustered=Yes }
    {    ;Search Description                       }
    {    ;Gen. Prod. Posting Group                 }
    {    ;Description                              }
  }
  FIELDGROUPS
  {
  }
  CODE
  {
    VAR
      DimMgt@1000 : Codeunit 408;

    LOCAL PROCEDURE ValidateShortcutDimCode@29(FieldNumber@1000 : Integer;VAR ShortcutDimCode@1001 : Code[20]);
    BEGIN
      DimMgt.ValidateDimValueCode(FieldNumber,ShortcutDimCode);
      DimMgt.SaveDefaultDim(DATABASE::"Item Charge","No.",FieldNumber,ShortcutDimCode);
      MODIFY;
    END;

    BEGIN
    END.
  }
}

