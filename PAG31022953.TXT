OBJECT Page 31022953 Docs. in Closed BG Subform
{
  OBJECT-PROPERTIES
  {
    Date=11/04/17;
    Time=12:24:31;
    Modified=Yes;
    Version List=NAVPT10.00#00015;
  }
  PROPERTIES
  {
    Editable=No;
    CaptionML=[ENU=Docs. in Closed BG Subform;
               PTG=Subform Documentos Remessa Fechados];
    SourceTable=Table31022937;
    SourceTableView=WHERE(Bill Gr./Pmt. Order No.=FILTER(<>''),
                          Type=CONST(Receivable));
    PageType=ListPart;
    ActionList=ACTIONS
    {
      { 1900000004;0 ;ActionContainer;
                      ActionContainerType=ActionItems }
      { 1907380404;1 ;ActionGroup;
                      CaptionML=[ENU=&Documents;
                                 PTG=&Documentos] }
      { 1901991804;2 ;Action    ;
                      Name=Redraw;
                      Ellipsis=Yes;
                      CaptionML=[ENU=Redraw;
                                 PTG=Reforma];
                      ApplicationArea=#Basic,#Suite;
                      Image=RefreshVoucher;
                      OnAction=BEGIN
                                 Redraw;
                               END;
                                }
      { 1906734104;2 ;Action    ;
                      Name=Dimensions;
                      ShortCutKey=Shift+Ctrl+D;
                      CaptionML=[ENU=Dime&nsions;
                                 PTG=Dime&ns�es];
                      ApplicationArea=#Basic,#Suite;
                      Image=Dimensions;
                      OnAction=BEGIN
                                 ShowDimension;
                               END;
                                }
      { 31022890;2   ;Action    ;
                      Name=Navigate;
                      CaptionML=[ENU=&Navigate;
                                 PTG=&Navegar];
                      Image=Navigate;
                      OnAction=BEGIN
                                 Navigate;
                               END;
                                }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 38  ;2   ;Field     ;
                SourceExpr="Document Type" }

    { 2   ;2   ;Field     ;
                SourceExpr="Posting Date";
                Visible=FALSE }

    { 4   ;2   ;Field     ;
                SourceExpr="Due Date" }

    { 22  ;2   ;Field     ;
                SourceExpr=Status }

    { 28  ;2   ;Field     ;
                SourceExpr="Honored/Rejtd. at Date";
                Visible=FALSE }

    { 18  ;2   ;Field     ;
                SourceExpr="Payment Method Code" }

    { 6   ;2   ;Field     ;
                SourceExpr="Document No." }

    { 8   ;2   ;Field     ;
                SourceExpr="No." }

    { 10  ;2   ;Field     ;
                SourceExpr=Description }

    { 30  ;2   ;Field     ;
                SourceExpr="Original Amount" }

    { 32  ;2   ;Field     ;
                SourceExpr="Original Amount (LCY)";
                Visible=FALSE }

    { 20  ;2   ;Field     ;
                SourceExpr="Amount for Collection" }

    { 34  ;2   ;Field     ;
                SourceExpr="Amt. for Collection (LCY)";
                Visible=FALSE }

    { 12  ;2   ;Field     ;
                SourceExpr="Remaining Amount" }

    { 36  ;2   ;Field     ;
                SourceExpr="Remaining Amt. (LCY)";
                Visible=FALSE }

    { 26  ;2   ;Field     ;
                SourceExpr=Redrawn }

    { 16  ;2   ;Field     ;
                SourceExpr=Place;
                Visible=FALSE }

    { 14  ;2   ;Field     ;
                SourceExpr="Account No.";
                Visible=FALSE }

    { 24  ;2   ;Field     ;
                SourceExpr="Entry No." }

  }
  CODE
  {
    VAR
      Text31022890@1000 : TextConst 'ENU=Only bills can be redrawn.;PTG=S� podem ser reformados t�tulos.';
      ClosedDoc@1110000 : Record 31022937;
      CustLedgEntry@1110001 : Record 21;
      VendLedgEntry@1110002 : Record 25;
      CarteraManagement@1110003 : Codeunit 31022901;

    PROCEDURE Navigate@1();
    BEGIN
      CarteraManagement.NavigateClosedDoc(Rec);
    END;

    PROCEDURE Redraw@5();
    BEGIN
      CurrPage.SETSELECTIONFILTER(ClosedDoc);
      IF ClosedDoc.ISEMPTY THEN
        EXIT;

      ClosedDoc.SETFILTER("Document Type",'<>%1',ClosedDoc."Document Type"::Bill);
      IF NOT ClosedDoc.ISEMPTY THEN
        ERROR(Text31022890);

      ClosedDoc.SETRANGE("Document Type");

      CustLedgEntry.RESET;
      ClosedDoc.FINDSET;
      REPEAT
        CustLedgEntry.GET(ClosedDoc."Entry No.");
        CustLedgEntry.MARK(TRUE);
      UNTIL ClosedDoc.NEXT = 0;

      CustLedgEntry.MARKEDONLY(TRUE);
      REPORT.RUNMODAL(REPORT::"Redraw Receivable Bills",TRUE,FALSE,CustLedgEntry);

      CurrPage.UPDATE(FALSE);
    END;

    PROCEDURE ShowDimension@1110000();
    VAR
      Status@1110001 : 'Open,Posted,Closed';
      DimMgmt@1000000001 : Codeunit 408;
    BEGIN
      DimMgmt.ShowDimensionSet("Dimension Set ID",STRSUBSTNO('%1 %2 %3',"Document Type","Document No.",Description));
    END;

    BEGIN
    {
      20170306 V10.00#00015 : Subform BG/PO
    }
    END.
  }
}

