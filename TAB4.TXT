OBJECT Table 4 Currency
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0,NAVPTSS81.00;
  }
  PROPERTIES
  {
    OnModify=BEGIN
               "Last Date Modified" := TODAY;
             END;

    OnDelete=VAR
               CustLedgEntry@1000 : Record 21;
               VendLedgEntry@1001 : Record 25;
             BEGIN
               CustLedgEntry.SETRANGE(Open,TRUE);
               CustLedgEntry.SETRANGE("Currency Code",Code);
               IF NOT CustLedgEntry.ISEMPTY THEN
                 ERROR(Text002,CustLedgEntry.TABLECAPTION,TABLECAPTION,Code);

               VendLedgEntry.SETRANGE(Open,TRUE);
               VendLedgEntry.SETRANGE("Currency Code",Code);
               IF NOT VendLedgEntry.ISEMPTY THEN
                 ERROR(Text002,VendLedgEntry.TABLECAPTION,TABLECAPTION,Code);

               CurrExchRate.SETRANGE("Currency Code",Code);
               CurrExchRate.DELETEALL;
             END;

    OnRename=BEGIN
               "Last Date Modified" := TODAY;
             END;

    CaptionML=[ENU=Currency;
               PTG=Divisa];
    LookupPageID=Page5;
  }
  FIELDS
  {
    { 1   ;   ;Code                ;Code10        ;OnValidate=BEGIN
                                                                IF Symbol = '' THEN
                                                                  Symbol := ResolveCurrencySymbol(Code);
                                                              END;

                                                   CaptionML=[ENU=Code;
                                                              PTG=C�digo];
                                                   NotBlank=Yes }
    { 2   ;   ;Last Date Modified  ;Date          ;CaptionML=[ENU=Last Date Modified;
                                                              PTG=Data �ltima Modifica��o];
                                                   Editable=No }
    { 3   ;   ;Last Date Adjusted  ;Date          ;CaptionML=[ENU=Last Date Adjusted;
                                                              PTG=Data �ltimo Ajuste];
                                                   Editable=No }
    { 6   ;   ;Unrealized Gains Acc.;Code20       ;TableRelation="G/L Account";
                                                   OnValidate=BEGIN
                                                                CheckGLAcc("Unrealized Gains Acc.");
                                                              END;

                                                   CaptionML=[ENU=Unrealized Gains Acc.;
                                                              PTG=Conta Ganhos N�o Realiz.] }
    { 7   ;   ;Realized Gains Acc. ;Code20        ;TableRelation="G/L Account";
                                                   OnValidate=BEGIN
                                                                CheckGLAcc("Realized Gains Acc.");
                                                              END;

                                                   CaptionML=[ENU=Realized Gains Acc.;
                                                              PTG=Conta Ganhos Realizados] }
    { 8   ;   ;Unrealized Losses Acc.;Code20      ;TableRelation="G/L Account";
                                                   OnValidate=BEGIN
                                                                CheckGLAcc("Unrealized Losses Acc.");
                                                              END;

                                                   CaptionML=[ENU=Unrealized Losses Acc.;
                                                              PTG=Conta Perdas N�o Realiz.] }
    { 9   ;   ;Realized Losses Acc.;Code20        ;TableRelation="G/L Account";
                                                   OnValidate=BEGIN
                                                                CheckGLAcc("Realized Losses Acc.");
                                                              END;

                                                   CaptionML=[ENU=Realized Losses Acc.;
                                                              PTG=Conta Perdas Realizados] }
    { 10  ;   ;Invoice Rounding Precision;Decimal ;InitValue=0,01;
                                                   OnValidate=BEGIN
                                                                IF "Amount Rounding Precision" <> 0 THEN
                                                                  IF "Invoice Rounding Precision" <> ROUND("Invoice Rounding Precision","Amount Rounding Precision") THEN
                                                                    FIELDERROR(
                                                                      "Invoice Rounding Precision",
                                                                      STRSUBSTNO(Text000,"Amount Rounding Precision"));
                                                              END;

                                                   CaptionML=[ENU=Invoice Rounding Precision;
                                                              PTG=Precis�o Arred. Fatura];
                                                   AutoFormatType=1;
                                                   AutoFormatExpr=Code }
    { 12  ;   ;Invoice Rounding Type;Option       ;CaptionML=[ENU=Invoice Rounding Type;
                                                              PTG=Tipo Arredondamento Fatura];
                                                   OptionCaptionML=[ENU=Nearest,Up,Down;
                                                                    PTG=Pr�ximo,Superior,Inferior];
                                                   OptionString=Nearest,Up,Down }
    { 13  ;   ;Amount Rounding Precision;Decimal  ;InitValue=0,01;
                                                   OnValidate=BEGIN
                                                                IF "Amount Rounding Precision" <> 0 THEN BEGIN
                                                                  "Invoice Rounding Precision" := ROUND("Invoice Rounding Precision","Amount Rounding Precision");
                                                                  IF "Amount Rounding Precision" > "Invoice Rounding Precision" THEN
                                                                    "Invoice Rounding Precision" := "Amount Rounding Precision";
                                                                END;
                                                              END;

                                                   CaptionML=[ENU=Amount Rounding Precision;
                                                              PTG=Precis�o Arred. Valor];
                                                   DecimalPlaces=2:5;
                                                   MinValue=0 }
    { 14  ;   ;Unit-Amount Rounding Precision;Decimal;
                                                   InitValue=0,00001;
                                                   CaptionML=[ENU=Unit-Amount Rounding Precision;
                                                              PTG=Precis�o Arred. Pre�o-Produto];
                                                   DecimalPlaces=0:9;
                                                   MinValue=0 }
    { 15  ;   ;Description         ;Text30        ;CaptionML=[ENU=Description;
                                                              PTG=Descri��o] }
    { 17  ;   ;Amount Decimal Places;Text5        ;InitValue=2:2;
                                                   OnValidate=BEGIN
                                                                GLSetup.CheckDecimalPlacesFormat("Amount Decimal Places");
                                                              END;

                                                   CaptionML=[ENU=Amount Decimal Places;
                                                              PTG=N� Decimais Valores];
                                                   NotBlank=Yes }
    { 18  ;   ;Unit-Amount Decimal Places;Text5   ;InitValue=2:5;
                                                   OnValidate=BEGIN
                                                                GLSetup.CheckDecimalPlacesFormat("Unit-Amount Decimal Places");
                                                              END;

                                                   CaptionML=[ENU=Unit-Amount Decimal Places;
                                                              PTG=N� Decimais Pre�o-Produto];
                                                   NotBlank=Yes }
    { 19  ;   ;Customer Filter     ;Code20        ;FieldClass=FlowFilter;
                                                   TableRelation=Customer;
                                                   CaptionML=[ENU=Customer Filter;
                                                              PTG=Filtro Cliente] }
    { 20  ;   ;Vendor Filter       ;Code20        ;FieldClass=FlowFilter;
                                                   TableRelation=Vendor;
                                                   CaptionML=[ENU=Vendor Filter;
                                                              PTG=Filtro Fornecedor] }
    { 21  ;   ;Global Dimension 1 Filter;Code20   ;FieldClass=FlowFilter;
                                                   TableRelation="Dimension Value".Code WHERE (Global Dimension No.=CONST(1));
                                                   CaptionML=[ENU=Global Dimension 1 Filter;
                                                              PTG=Filtro Dimens�o Global 1];
                                                   CaptionClass='1,3,1' }
    { 22  ;   ;Global Dimension 2 Filter;Code20   ;FieldClass=FlowFilter;
                                                   TableRelation="Dimension Value".Code WHERE (Global Dimension No.=CONST(2));
                                                   CaptionML=[ENU=Global Dimension 2 Filter;
                                                              PTG=Filtro Dimens�o Global 2];
                                                   CaptionClass='1,3,2' }
    { 23  ;   ;Date Filter         ;Date          ;FieldClass=FlowFilter;
                                                   CaptionML=[ENU=Date Filter;
                                                              PTG=Filtro Data] }
    { 24  ;   ;Cust. Ledg. Entries in Filter;Boolean;
                                                   FieldClass=FlowField;
                                                   CalcFormula=Exist("Cust. Ledger Entry" WHERE (Customer No.=FIELD(Customer Filter),
                                                                                                 Currency Code=FIELD(Code)));
                                                   CaptionML=[ENU=Cust. Ledg. Entries in Filter;
                                                              PTG=Movs. Cliente no Filtro];
                                                   Editable=No }
    { 25  ;   ;Customer Balance    ;Decimal       ;FieldClass=FlowField;
                                                   CalcFormula=Sum("Detailed Cust. Ledg. Entry".Amount WHERE (Customer No.=FIELD(Customer Filter),
                                                                                                              Initial Entry Global Dim. 1=FIELD(Global Dimension 1 Filter),
                                                                                                              Initial Entry Global Dim. 2=FIELD(Global Dimension 2 Filter),
                                                                                                              Posting Date=FIELD(Date Filter),
                                                                                                              Currency Code=FIELD(Code)));
                                                   CaptionML=[ENU=Customer Balance;
                                                              PTG=Saldo Cliente];
                                                   Editable=No;
                                                   AutoFormatType=1;
                                                   AutoFormatExpr=Code }
    { 26  ;   ;Customer Outstanding Orders;Decimal;FieldClass=FlowField;
                                                   CalcFormula=Sum("Sales Line"."Outstanding Amount" WHERE (Document Type=CONST(Order),
                                                                                                            Bill-to Customer No.=FIELD(Customer Filter),
                                                                                                            Currency Code=FIELD(Code),
                                                                                                            Shortcut Dimension 1 Code=FIELD(Global Dimension 1 Filter),
                                                                                                            Shortcut Dimension 2 Code=FIELD(Global Dimension 2 Filter)));
                                                   AccessByPermission=TableData 110=R;
                                                   CaptionML=[ENU=Customer Outstanding Orders;
                                                              PTG=Encomendas Pendentes Cliente];
                                                   Editable=No;
                                                   AutoFormatType=1;
                                                   AutoFormatExpr=Code }
    { 27  ;   ;Customer Shipped Not Invoiced;Decimal;
                                                   FieldClass=FlowField;
                                                   CalcFormula=Sum("Sales Line"."Shipped Not Invoiced" WHERE (Document Type=CONST(Order),
                                                                                                              Bill-to Customer No.=FIELD(Customer Filter),
                                                                                                              Currency Code=FIELD(Code),
                                                                                                              Shortcut Dimension 1 Code=FIELD(Global Dimension 1 Filter),
                                                                                                              Shortcut Dimension 2 Code=FIELD(Global Dimension 2 Filter)));
                                                   AccessByPermission=TableData 110=R;
                                                   CaptionML=[ENU=Customer Shipped Not Invoiced;
                                                              PTG=Enviado e n�o Faturado a Cliente];
                                                   Editable=No;
                                                   AutoFormatType=1;
                                                   AutoFormatExpr=Code }
    { 28  ;   ;Customer Balance Due;Decimal       ;FieldClass=FlowField;
                                                   CalcFormula=Sum("Detailed Cust. Ledg. Entry".Amount WHERE (Customer No.=FIELD(Customer Filter),
                                                                                                              Initial Entry Global Dim. 1=FIELD(Global Dimension 1 Filter),
                                                                                                              Initial Entry Global Dim. 2=FIELD(Global Dimension 2 Filter),
                                                                                                              Initial Entry Due Date=FIELD(Date Filter),
                                                                                                              Posting Date=FIELD(UPPERLIMIT(Date Filter)),
                                                                                                              Currency Code=FIELD(Code)));
                                                   CaptionML=[ENU=Customer Balance Due;
                                                              PTG=Saldo Vencido Cliente];
                                                   Editable=No;
                                                   AutoFormatType=1;
                                                   AutoFormatExpr=Code }
    { 29  ;   ;Vendor Ledg. Entries in Filter;Boolean;
                                                   FieldClass=FlowField;
                                                   CalcFormula=Exist("Vendor Ledger Entry" WHERE (Vendor No.=FIELD(Vendor Filter),
                                                                                                  Currency Code=FIELD(Code)));
                                                   CaptionML=[ENU=Vendor Ledg. Entries in Filter;
                                                              PTG=Movs. Fornecedor no Filtro];
                                                   Editable=No }
    { 30  ;   ;Vendor Balance      ;Decimal       ;FieldClass=FlowField;
                                                   CalcFormula=-Sum("Detailed Vendor Ledg. Entry".Amount WHERE (Vendor No.=FIELD(Vendor Filter),
                                                                                                                Initial Entry Global Dim. 1=FIELD(Global Dimension 1 Filter),
                                                                                                                Initial Entry Global Dim. 2=FIELD(Global Dimension 2 Filter),
                                                                                                                Posting Date=FIELD(Date Filter),
                                                                                                                Currency Code=FIELD(Code)));
                                                   CaptionML=[ENU=Vendor Balance;
                                                              PTG=Saldo Fornecedor];
                                                   Editable=No;
                                                   AutoFormatType=1;
                                                   AutoFormatExpr=Code }
    { 31  ;   ;Vendor Outstanding Orders;Decimal  ;FieldClass=FlowField;
                                                   CalcFormula=Sum("Purchase Line"."Outstanding Amount" WHERE (Document Type=CONST(Order),
                                                                                                               Pay-to Vendor No.=FIELD(Vendor Filter),
                                                                                                               Currency Code=FIELD(Code),
                                                                                                               Shortcut Dimension 1 Code=FIELD(Global Dimension 1 Filter),
                                                                                                               Shortcut Dimension 2 Code=FIELD(Global Dimension 2 Filter)));
                                                   AccessByPermission=TableData 120=R;
                                                   CaptionML=[ENU=Vendor Outstanding Orders;
                                                              PTG=Encomendas Pendentes Fornecedor];
                                                   Editable=No;
                                                   AutoFormatType=1;
                                                   AutoFormatExpr=Code }
    { 32  ;   ;Vendor Amt. Rcd. Not Invoiced;Decimal;
                                                   FieldClass=FlowField;
                                                   CalcFormula=Sum("Purchase Line"."Amt. Rcd. Not Invoiced" WHERE (Document Type=CONST(Order),
                                                                                                                   Pay-to Vendor No.=FIELD(Vendor Filter),
                                                                                                                   Currency Code=FIELD(Code),
                                                                                                                   Shortcut Dimension 1 Code=FIELD(Global Dimension 1 Filter),
                                                                                                                   Shortcut Dimension 2 Code=FIELD(Global Dimension 2 Filter)));
                                                   AccessByPermission=TableData 120=R;
                                                   CaptionML=[ENU=Vendor Amt. Rcd. Not Invoiced;
                                                              PTG=Valor Rec. N�o Faturado Forn.];
                                                   Editable=No;
                                                   AutoFormatType=1;
                                                   AutoFormatExpr=Code }
    { 33  ;   ;Vendor Balance Due  ;Decimal       ;FieldClass=FlowField;
                                                   CalcFormula=-Sum("Detailed Vendor Ledg. Entry".Amount WHERE (Vendor No.=FIELD(Vendor Filter),
                                                                                                                Initial Entry Global Dim. 1=FIELD(Global Dimension 1 Filter),
                                                                                                                Initial Entry Global Dim. 2=FIELD(Global Dimension 2 Filter),
                                                                                                                Initial Entry Due Date=FIELD(Date Filter),
                                                                                                                Posting Date=FIELD(UPPERLIMIT(Date Filter)),
                                                                                                                Currency Code=FIELD(Code)));
                                                   CaptionML=[ENU=Vendor Balance Due;
                                                              PTG=Saldo Vencido Fornecedor];
                                                   Editable=No;
                                                   AutoFormatType=1;
                                                   AutoFormatExpr=Code }
    { 34  ;   ;Customer Balance (LCY);Decimal     ;FieldClass=FlowField;
                                                   CalcFormula=Sum("Detailed Cust. Ledg. Entry"."Amount (LCY)" WHERE (Customer No.=FIELD(Customer Filter),
                                                                                                                      Initial Entry Global Dim. 1=FIELD(Global Dimension 1 Filter),
                                                                                                                      Initial Entry Global Dim. 2=FIELD(Global Dimension 2 Filter),
                                                                                                                      Posting Date=FIELD(Date Filter),
                                                                                                                      Currency Code=FIELD(Code)));
                                                   CaptionML=[ENU=Customer Balance (LCY);
                                                              PTG=Saldo Cliente (DL)];
                                                   Editable=No;
                                                   AutoFormatType=1 }
    { 35  ;   ;Vendor Balance (LCY);Decimal       ;FieldClass=FlowField;
                                                   CalcFormula=-Sum("Detailed Vendor Ledg. Entry"."Amount (LCY)" WHERE (Vendor No.=FIELD(Vendor Filter),
                                                                                                                        Initial Entry Global Dim. 1=FIELD(Global Dimension 1 Filter),
                                                                                                                        Initial Entry Global Dim. 2=FIELD(Global Dimension 2 Filter),
                                                                                                                        Posting Date=FIELD(Date Filter),
                                                                                                                        Currency Code=FIELD(Code)));
                                                   CaptionML=[ENU=Vendor Balance (LCY);
                                                              PTG=Saldo Fornecedor (DL)];
                                                   Editable=No;
                                                   AutoFormatType=1 }
    { 40  ;   ;Realized G/L Gains Account;Code20  ;TableRelation="G/L Account";
                                                   OnValidate=BEGIN
                                                                CheckGLAcc("Realized G/L Gains Account");
                                                              END;

                                                   CaptionML=[ENU=Realized G/L Gains Account;
                                                              PTG=Conta Aj. Favs. Realiz.] }
    { 41  ;   ;Realized G/L Losses Account;Code20 ;TableRelation="G/L Account";
                                                   OnValidate=BEGIN
                                                                CheckGLAcc("Realized G/L Losses Account");
                                                              END;

                                                   CaptionML=[ENU=Realized G/L Losses Account;
                                                              PTG=Conta Aj. Desf. Realiz.] }
    { 44  ;   ;Appln. Rounding Precision;Decimal  ;CaptionML=[ENU=Appln. Rounding Precision;
                                                              PTG=Precis�o Arred. Liquida��o];
                                                   MinValue=0;
                                                   AutoFormatType=1;
                                                   AutoFormatExpr=Code }
    { 45  ;   ;EMU Currency        ;Boolean       ;CaptionML=[ENU=EMU Currency;
                                                              PTG=Divisa UME] }
    { 46  ;   ;Currency Factor     ;Decimal       ;CaptionML=[ENU=Currency Factor;
                                                              PTG=Factor Divisa];
                                                   DecimalPlaces=0:5;
                                                   Editable=No }
    { 47  ;   ;Residual Gains Account;Code20      ;TableRelation="G/L Account";
                                                   CaptionML=[ENU=Residual Gains Account;
                                                              PTG=Conta Aj. Residual Favs.] }
    { 48  ;   ;Residual Losses Account;Code20     ;TableRelation="G/L Account";
                                                   CaptionML=[ENU=Residual Losses Account;
                                                              PTG=Conta Aj. Residual Desf.] }
    { 50  ;   ;Conv. LCY Rndg. Debit Acc.;Code20  ;TableRelation="G/L Account";
                                                   CaptionML=[ENU=Conv. LCY Rndg. Debit Acc.;
                                                              PTG=Conta D�bito Arred. Convers�o DL] }
    { 51  ;   ;Conv. LCY Rndg. Credit Acc.;Code20 ;TableRelation="G/L Account";
                                                   CaptionML=[ENU=Conv. LCY Rndg. Credit Acc.;
                                                              PTG=Conta Cr�dito Arred. Convers�o DL] }
    { 52  ;   ;Max. VAT Difference Allowed;Decimal;OnValidate=BEGIN
                                                                IF "Max. VAT Difference Allowed" <> ROUND("Max. VAT Difference Allowed","Amount Rounding Precision") THEN
                                                                  ERROR(
                                                                    Text001,
                                                                    FIELDCAPTION("Max. VAT Difference Allowed"),"Amount Rounding Precision");

                                                                "Max. VAT Difference Allowed" := ABS("Max. VAT Difference Allowed");
                                                              END;

                                                   CaptionML=[ENU=Max. VAT Difference Allowed;
                                                              PTG=Dif. M�xima IVA Permitida];
                                                   AutoFormatType=1;
                                                   AutoFormatExpr=Code }
    { 53  ;   ;VAT Rounding Type   ;Option        ;CaptionML=[ENU=VAT Rounding Type;
                                                              PTG=Tipo Arredondamento IVA];
                                                   OptionCaptionML=[ENU=Nearest,Up,Down;
                                                                    PTG=Pr�ximo,Superior,Inferior];
                                                   OptionString=Nearest,Up,Down }
    { 54  ;   ;Payment Tolerance % ;Decimal       ;CaptionML=[ENU=Payment Tolerance %;
                                                              PTG=% Toler�ncia Pagamento];
                                                   DecimalPlaces=0:5;
                                                   MinValue=0;
                                                   MaxValue=100;
                                                   Editable=No }
    { 55  ;   ;Max. Payment Tolerance Amount;Decimal;
                                                   CaptionML=[ENU=Max. Payment Tolerance Amount;
                                                              PTG=Valor Max. Toler�ncia Pagamento];
                                                   MinValue=0;
                                                   Editable=No;
                                                   AutoFormatType=1;
                                                   AutoFormatExpr=Code }
    { 56  ;   ;Symbol              ;Text10        ;CaptionML=[ENU=Symbol;
                                                              PTG=S�mbolo] }
    { 31022890;;Decimal Currency Text;Text30      ;CaptionML=[ENU=Decimal Currency Text;
                                                              PTG=Descri��o Unidade Decimal];
                                                   Description=soft }
    { 31022891;;Decimal places decimal Curr.;Integer;
                                                   CaptionML=[ENU=Decimal places decimal Curr.;
                                                              PTG=N� Casas Unidade Decimal];
                                                   Description=soft }
    { 31022913;;Bill Groups - Collection;Boolean  ;CaptionML=[ENU=Bill Groups - Collection;
                                                              PTG=Remessas - Cobran�a];
                                                   Description=soft }
    { 31022914;;Bill Groups - Discount;Boolean    ;CaptionML=[ENU=Bill Groups - Discount;
                                                              PTG=Remessas - Desconto];
                                                   Description=soft }
    { 31022915;;Payment Orders     ;Boolean       ;CaptionML=[ENU=Payment Orders;
                                                              PTG=Ordens Pagamento];
                                                   Description=soft }
  }
  KEYS
  {
    {    ;Code                                    ;Clustered=Yes }
  }
  FIELDGROUPS
  {
    { 1   ;Brick               ;Code,Description                         }
  }
  CODE
  {
    VAR
      Text000@1000 : TextConst 'ENU=must be rounded to the nearest %1;PTG=tem de ser arredondado ao %1 mais pr�ximo';
      Text001@1001 : TextConst 'ENU=%1 must be rounded to the nearest %2.;PTG=%1 tem de ser arredondado ao mais pr�ximo %2.';
      CurrExchRate@1002 : Record 330;
      GLSetup@1003 : Record 98;
      Text002@1004 : TextConst '@@@=1 either customer or vendor ledger entry table 2 name co currency table 3 currency code;ENU=There is one or more opened entries in the %1 table using %2 %3.;PTG=Existe um ou mais movimentos pendentes na tabela %1 a utilizar %2 %3.';
      IncorrectEntryTypeErr@1005 : TextConst 'ENU=Incorrect Entry Type %1.;PTG=Tipo Movimento %1 incorreto.';

    PROCEDURE InitRoundingPrecision@2();
    BEGIN
      GLSetup.GET;
      IF GLSetup."Amount Rounding Precision" <> 0 THEN
        "Amount Rounding Precision" := GLSetup."Amount Rounding Precision"
      ELSE
        "Amount Rounding Precision" := 0.01;
      IF GLSetup."Unit-Amount Rounding Precision" <> 0 THEN
        "Unit-Amount Rounding Precision" := GLSetup."Unit-Amount Rounding Precision"
      ELSE
        "Unit-Amount Rounding Precision" := 0.00001;
      "Max. VAT Difference Allowed" := GLSetup."Max. VAT Difference Allowed";
      "VAT Rounding Type" := GLSetup."VAT Rounding Type";
      "Invoice Rounding Precision" := GLSetup."Inv. Rounding Precision (LCY)";
      "Invoice Rounding Type" := GLSetup."Inv. Rounding Type (LCY)";
    END;

    LOCAL PROCEDURE CheckGLAcc@1(AccNo@1000 : Code[20]);
    VAR
      GLAcc@1001 : Record 15;
    BEGIN
      IF AccNo <> '' THEN BEGIN
        GLAcc.GET(AccNo);
        GLAcc.CheckGLAcc;
      END;
    END;

    PROCEDURE VATRoundingDirection@3() : Text[1];
    BEGIN
      CASE "VAT Rounding Type" OF
        "VAT Rounding Type"::Nearest:
          EXIT('=');
        "VAT Rounding Type"::Up:
          EXIT('>');
        "VAT Rounding Type"::Down:
          EXIT('<');
      END;
    END;

    PROCEDURE InvoiceRoundingDirection@4() : Text[1];
    BEGIN
      CASE "Invoice Rounding Type" OF
        "Invoice Rounding Type"::Nearest:
          EXIT('=');
        "Invoice Rounding Type"::Up:
          EXIT('>');
        "Invoice Rounding Type"::Down:
          EXIT('<');
      END;
    END;

    PROCEDURE CheckAmountRoundingPrecision@5();
    BEGIN
      TESTFIELD("Unit-Amount Rounding Precision");
      TESTFIELD("Amount Rounding Precision");
    END;

    PROCEDURE GetGainLossAccount@6(DtldCVLedgEntryBuf@1000 : Record 383) : Code[20];
    BEGIN
      CASE DtldCVLedgEntryBuf."Entry Type" OF
        DtldCVLedgEntryBuf."Entry Type"::"Unrealized Loss":
          BEGIN
            TESTFIELD("Unrealized Losses Acc.");
            EXIT("Unrealized Losses Acc.");
          END;
        DtldCVLedgEntryBuf."Entry Type"::"Unrealized Gain":
          BEGIN
            TESTFIELD("Unrealized Gains Acc.");
            EXIT("Unrealized Gains Acc.");
          END;
        DtldCVLedgEntryBuf."Entry Type"::"Realized Loss":
          BEGIN
            TESTFIELD("Realized Losses Acc.");
            EXIT("Realized Losses Acc.");
          END;
        DtldCVLedgEntryBuf."Entry Type"::"Realized Gain":
          BEGIN
            TESTFIELD("Realized Gains Acc.");
            EXIT("Realized Gains Acc.");
          END;
        ELSE
          ERROR(IncorrectEntryTypeErr,DtldCVLedgEntryBuf."Entry Type");
      END;
    END;

    PROCEDURE GetCurrencySymbol@8() : Text[10];
    BEGIN
      IF Symbol <> '' THEN
        EXIT(Symbol);

      EXIT(Code);
    END;

    PROCEDURE ResolveCurrencySymbol@9(CurrencyCode@1000 : Code[10]) : Text[10];
    VAR
      Currency@1001 : Record 4;
      PoundChar@1002 : Char;
      EuroChar@1003 : Char;
      YenChar@1004 : Char;
    BEGIN
      IF Currency.GET(CurrencyCode) THEN
        IF Currency.Symbol <> '' THEN
          EXIT(Currency.Symbol);

      PoundChar := 163;
      YenChar := 165;
      EuroChar := 8364;

      CASE CurrencyCode OF
        'AUD','BND','CAD','FJD','HKD','MXN','NZD','SBD','SGD','USD':
          EXIT('$');
        'GBP':
          EXIT(FORMAT(PoundChar));
        'DKK','ISK','NOK','SEK':
          EXIT('kr');
        'EUR':
          EXIT(FORMAT(EuroChar));
        'CNY','JPY':
          EXIT(FORMAT(YenChar));
      END;

      EXIT('');
    END;

    PROCEDURE Initialize@7(CurrencyCode@1000 : Code[10]);
    BEGIN
      IF CurrencyCode <> '' THEN
        GET(CurrencyCode)
      ELSE
        InitRoundingPrecision;
    END;

    BEGIN
    END.
  }
}

