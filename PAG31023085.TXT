OBJECT Page 31023085 Income Type List
{
  OBJECT-PROPERTIES
  {
    Date=10/10/16;
    Time=13:00:00;
    Version List=NAVPTSS93.00;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Income Type List;
               PTG=Lista Categoria Rendimento];
    SourceTable=Table31022976;
    PageType=List;
  }
  CONTROLS
  {
    { 1000000000;0;Container;
                ContainerType=ContentArea }

    { 1000000001;1;Group  ;
                Name=Group;
                GroupType=Repeater }

    { 1000000002;2;Field  ;
                ApplicationArea=#Basic,#Suite;
                SourceExpr=Code }

    { 1000000003;2;Field  ;
                ApplicationArea=#Basic,#Suite;
                SourceExpr=Description }

  }
  CODE
  {

    BEGIN
    {
      V93.00#00018 - Reten��o a Clientes e Fornecedores -  - 2016.06.29
    }
    END.
  }
}

