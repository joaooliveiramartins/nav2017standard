OBJECT Page 9024 Security Admin Role Center
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0;
  }
  PROPERTIES
  {
    Editable=No;
    CaptionML=[ENU=Security Admin Role Center;
               PTG=Centro Perfil Administrador Seguran�a];
    Description=Manage users, users groups and permissions;
    PageType=RoleCenter;
    ActionList=ACTIONS
    {
      { 5       ;    ;ActionContainer;
                      Name=HomeItemsContainer;
                      ActionContainerType=HomeItems }
      { 10      ;1   ;Action    ;
                      Name=User Groups;
                      CaptionML=[ENU=User Groups;
                                 PTG=Grupos Utilizador];
                      ToolTipML=ENU=Set up or modify user groups as a fast way of giving users access to the functionality that is relevant to their work.;
                      ApplicationArea=#Basic,#Suite;
                      RunObject=Page 9830 }
      { 9       ;1   ;Action    ;
                      Name=Users;
                      CaptionML=[ENU=Users;
                                 PTG=Utilizadores];
                      ApplicationArea=#Basic,#Suite;
                      RunObject=Page 9800 }
      { 13      ;1   ;Action    ;
                      Name=User Review Log;
                      CaptionML=[ENU=User Review Log;
                                 PTG=Registo Revis�o Utilizador];
                      ToolTipML=ENU=View a log of users' activities in the database.;
                      ApplicationArea=#Basic,#Suite;
                      RunObject=Page 710;
                      RunPageView=WHERE(Table No Filter=FILTER(9062)) }
      { 2       ;1   ;Action    ;
                      Name=Permission Sets;
                      CaptionML=[ENU=Permission Sets;
                                 PTG=Conjuntos Permiss�es];
                      ToolTipML=ENU=View or edit which feature objects that users need to access and set up the related permissions in permission sets that you can assign to the users of the database.;
                      ApplicationArea=#Basic,#Suite;
                      RunObject=Page 9802 }
      { 11      ;1   ;Action    ;
                      Name=Plans;
                      CaptionML=[ENU=Plans;
                                 PTG=Planos];
                      ToolTipML=ENU=View subscription plans.;
                      ApplicationArea=#Basic,#Suite;
                      RunObject=Page 9824;
                      RunPageMode=View }
      { 18      ;0   ;ActionContainer;
                      ActionContainerType=ActivityButtons }
      { 24      ;1   ;ActionGroup;
                      CaptionML=[ENU=Self-Service;
                                 PTG=Self-Service];
                      ToolTipML=ENU=Manage your time sheets and assignments.;
                      Image=HumanResources }
      { 21      ;2   ;Action    ;
                      CaptionML=[ENU=Time Sheets;
                                 PTG=Folhas Tempo];
                      ToolTipML=ENU=View all time sheets.;
                      ApplicationArea=#Suite;
                      RunObject=Page 951;
                      Gesture=None }
    }
  }
  CONTROLS
  {
    { 1   ;0   ;Container ;
                ContainerType=RoleCenterArea }

    { 6   ;1   ;Group     ;
                Name=CueGroup;
                CaptionML=[ENU=Cue Group;
                           PTG=Grupo Sugest�es];
                GroupType=Group }

    { 7   ;2   ;Part      ;
                ApplicationArea=#Basic,#Suite;
                PagePartID=Page9062;
                PartType=Page }

    { 15  ;2   ;Part      ;
                ApplicationArea=#Suite;
                PagePartID=Page9042;
                PartType=Page }

    { 8   ;1   ;Group     ;
                Name=Lists;
                CaptionML=[ENU=Lists;
                           PTG=Listas];
                GroupType=Group }

    { 12  ;2   ;Part      ;
                PagePartID=Page773;
                PartType=Page }

    { 3   ;2   ;Part      ;
                CaptionML=[ENU=Subscription Plans;
                           PTG=Planos Subscri��o];
                ApplicationArea=#Basic,#Suite;
                PagePartID=Page9825;
                Editable=FALSE;
                PartType=Page }

    { 4   ;2   ;Part      ;
                ApplicationArea=#Basic,#Suite;
                PagePartID=Page9829;
                Editable=FALSE;
                PartType=Page }

    { 14  ;2   ;Part      ;
                CaptionML=[ENU=Plan Permission Set;
                           PTG=Conjuto Permiss�es Plano];
                ToolTipML=ENU=Specifies the permission sets included in plans.;
                ApplicationArea=#Basic,#Suite;
                PagePartID=Page9844;
                Editable=FALSE;
                PartType=Page }

  }
  CODE
  {

    BEGIN
    END.
  }
}

