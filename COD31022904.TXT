OBJECT Codeunit 31022904 Document-Move
{
  OBJECT-PROPERTIES
  {
    Date=01/02/15;
    Time=13:00:00;
    Version List=NAVPTSS81.00;
  }
  PROPERTIES
  {
    Permissions=TableData 31022937=imd,
                TableData 31022940=imd;
    OnRun=BEGIN
          END;

  }
  CODE
  {
    VAR
      Text31022890@1000 : TextConst 'ENU=You cannot delete a bank account with bill groups in preparation.;PTG=N�o � poss�vel eliminar uma conta banc�ria com remessas em prepara��o.';
      Text31022891@1001 : TextConst 'ENU=You cannot delete a bank account with bill groups.;PTG=N�o � poss�vel eliminar uma conta banc�ria com remessas pendentes.';
      Text31022892@1002 : TextConst 'ENU=You cannot delete a bank account with closed bill groups in a fiscal year that has not been closed yet.;PTG=N�o � poss�vel eliminar uma conta banc�ria com remessas encerradas num per�odo contabil�stico ainda n�o encerrado.';
      Text31022893@1003 : TextConst 'ENU=You cannot delete a bank account with payment orders in preparation.;PTG=N�o � poss�vel eliminar uma conta banc�ria com ordens de pagto. em prepara��o.';
      Text31022894@1004 : TextConst 'ENU=You cannot delete a bank account with payment orders.;PTG=N�o � poss�vel eliminar uma conta banc�ria com ordens de pagamento.';
      Text31022895@1005 : TextConst 'ENU=You cannot delete a bank account with closed payment orders in a fiscal year that has not been closed yet.;PTG=N�o � poss�vel eliminar uma conta banc�ria com ordens de pagto. encerradas num per�odo contabil�stico ainda n�o encerrado.';
      AccountingPeriod@1110000 : Record 50;
      BillGr@1110001 : Record 31022938;
      BillGr2@1110002 : Record 31022938;
      PostedBillGr@1110003 : Record 31022939;
      PostedBillGr2@1110004 : Record 31022939;
      ClosedBillGr@1110005 : Record 31022940;
      ClosedBillGr2@1110006 : Record 31022940;
      ClosedDoc@1110007 : Record 31022937;
      PmtOrd@1110008 : Record 31022952;
      PmtOrd2@1110009 : Record 31022952;
      PostedPmtOrd@1110010 : Record 31022953;
      PostedPmtOrd2@1110011 : Record 31022953;
      ClosedPmtOrd@1110012 : Record 31022954;
      ClosedPmtOrd2@1110013 : Record 31022954;

    PROCEDURE MoveBankAccDocs@5(BankAcc@1110000 : Record 270);
    BEGIN
      WITH BillGr DO BEGIN
        LOCKTABLE;
        IF RECORDLEVELLOCKING THEN
          IF BillGr2.FINDLAST THEN;
        RESET;
        SETCURRENTKEY("Bank Account No.");
        SETRANGE("Bank Account No.",BankAcc."No.");
        IF NOT ISEMPTY THEN
          ERROR(Text31022890);
      END;

      WITH PostedBillGr DO BEGIN
        LOCKTABLE;
        IF RECORDLEVELLOCKING THEN
          IF PostedBillGr2.FINDLAST THEN;
        RESET;
        SETCURRENTKEY("Bank Account No.");
        SETRANGE("Bank Account No.",BankAcc."No.");
        IF NOT ISEMPTY THEN
          ERROR(Text31022891);
      END;

      WITH ClosedBillGr DO BEGIN
        LOCKTABLE;
        IF RECORDLEVELLOCKING THEN
          IF ClosedBillGr2.FINDLAST THEN;
        RESET;
        SETCURRENTKEY("Bank Account No.");
        SETRANGE("Bank Account No.",BankAcc."No.");
        AccountingPeriod.SETRANGE(Closed,FALSE);
        IF AccountingPeriod.FINDFIRST THEN
          SETFILTER("Closing Date",'>=%1',AccountingPeriod."Starting Date");

        IF NOT ISEMPTY THEN
          ERROR(Text31022892);
        SETRANGE("Closing Date");
        MODIFYALL("Bank Account No.",'');
      END;

      WITH PmtOrd DO BEGIN
        LOCKTABLE;
        IF RECORDLEVELLOCKING THEN
          IF PmtOrd2.FINDLAST THEN;
        RESET;
        SETCURRENTKEY("Bank Account No.");
        SETRANGE("Bank Account No.",BankAcc."No.");
        IF NOT ISEMPTY THEN
          ERROR(Text31022893);
      END;

      WITH PostedPmtOrd DO BEGIN
        LOCKTABLE;
        IF RECORDLEVELLOCKING THEN
          IF PostedPmtOrd2.FINDLAST THEN;
        RESET;
        SETCURRENTKEY("Bank Account No.");
        SETRANGE("Bank Account No.",BankAcc."No.");
        IF NOT ISEMPTY THEN
          ERROR(Text31022894);
      END;

      WITH ClosedPmtOrd DO BEGIN
        LOCKTABLE;
        IF RECORDLEVELLOCKING THEN
          IF ClosedPmtOrd2.FINDLAST THEN;
        RESET;
        SETCURRENTKEY("Bank Account No.");
        SETRANGE("Bank Account No.",BankAcc."No.");
        AccountingPeriod.SETRANGE(Closed,FALSE);
        IF AccountingPeriod.FINDFIRST THEN
          SETFILTER("Closing Date",'>=%1',AccountingPeriod."Starting Date");
        IF NOT ISEMPTY THEN
          ERROR(Text31022895);
        SETRANGE("Closing Date");
        MODIFYALL("Bank Account No.",'');
      END;
    END;

    PROCEDURE MoveReceivableDocs@1(Cust@1110000 : Record 18);
    BEGIN
      WITH ClosedDoc DO BEGIN
        RESET;
        SETCURRENTKEY("Account No.","Honored/Rejtd. at Date");
        SETRANGE("Account No.",Cust."No.");
        MODIFYALL("Account No.",'');
      END;
    END;

    PROCEDURE MovePayableDocs@3(Vend@1110000 : Record 23);
    BEGIN
      WITH ClosedDoc DO BEGIN
        RESET;
        SETCURRENTKEY("Account No.","Honored/Rejtd. at Date");
        SETRANGE("Account No.",Vend."No.");
        MODIFYALL("Account No.",'');
      END;
    END;

    BEGIN
    END.
  }
}

