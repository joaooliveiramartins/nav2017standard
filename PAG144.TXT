OBJECT Page 144 Posted Sales Credit Memos
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0.00.15601;
  }
  PROPERTIES
  {
    Editable=No;
    CaptionML=[ENU=Posted Sales Credit Memos;
               PTG=Notas Cr�dito Venda Registadas];
    SourceTable=Table114;
    SourceTableView=SORTING(Posting Date)
                    ORDER(Descending);
    PageType=List;
    CardPageID=Posted Sales Credit Memo;
    PromotedActionCategoriesML=[ENU=New,Process,Report,Cr. Memo,Cancel;
                                PTG=Novo,Processar,Mapas,Nota Cr�dito,Cancelar];
    OnOpenPage=VAR
                 OfficeMgt@1000 : Codeunit 1630;
               BEGIN
                 SetSecurityFilterOnRespCenter;
                 IF FINDFIRST THEN;
                 IsOfficeAddin := OfficeMgt.IsAvailable;
               END;

    OnAfterGetRecord=VAR
                       SalesCrMemoHeader@1000 : Record 114;
                     BEGIN
                       DocExchStatusStyle := GetDocExchStatusStyle;

                       SalesCrMemoHeader.COPYFILTERS(Rec);
                       SalesCrMemoHeader.SETFILTER("Document Exchange Status",'<>%1',"Document Exchange Status"::"Not Sent");
                       DocExchStatusVisible := NOT SalesCrMemoHeader.ISEMPTY;
                     END;

    OnAfterGetCurrRecord=BEGIN
                           DocExchStatusStyle := GetDocExchStatusStyle;
                           CurrPage.IncomingDocAttachFactBox.PAGE.LoadDataFromRecord(Rec);
                         END;

    ActionList=ACTIONS
    {
      { 1900000004;0 ;ActionContainer;
                      ActionContainerType=ActionItems }
      { 19      ;1   ;ActionGroup;
                      CaptionML=[ENU=&Credit Memo;
                                 PTG=&Nota Cr�dito];
                      Image=CreditMemo }
      { 26      ;2   ;Action    ;
                      ShortCutKey=Shift+F7;
                      CaptionML=[ENU=Card;
                                 PTG=Ficha];
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=EditLines;
                      PromotedCategory=Category4;
                      OnAction=BEGIN
                                 PAGE.RUN(PAGE::"Posted Sales Credit Memo",Rec)
                               END;
                                }
      { 31      ;2   ;Action    ;
                      Name=Statistics;
                      ShortCutKey=F7;
                      CaptionML=[ENU=Statistics;
                                 PTG=Estat�sticas];
                      RunObject=Page 398;
                      RunPageLink=No.=FIELD(No.);
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=Statistics;
                      PromotedCategory=Category4 }
      { 32      ;2   ;Action    ;
                      CaptionML=[ENU=Co&mments;
                                 PTG=Co&ment�rios];
                      RunObject=Page 67;
                      RunPageLink=Document Type=CONST(Posted Credit Memo),
                                  No.=FIELD(No.);
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=ViewComments;
                      PromotedCategory=Category4 }
      { 1102601000;2 ;Action    ;
                      AccessByPermission=TableData 348=R;
                      ShortCutKey=Shift+Ctrl+D;
                      CaptionML=[ENU=Dimensions;
                                 PTG=Dimens�es];
                      ToolTipML=ENU=View or edit dimensions, such as area, project, or department, that you can assign to sales and purchase documents to distribute costs and analyze transaction history.;
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=Dimensions;
                      PromotedCategory=Category4;
                      OnAction=BEGIN
                                 ShowDimensions;
                               END;
                                }
      { 10      ;2   ;Action    ;
                      Name=IncomingDoc;
                      AccessByPermission=TableData 130=R;
                      CaptionML=[ENU=Incoming Document;
                                 PTG=Documento Recebido];
                      ToolTipML=ENU=View or create an incoming document record that is linked to the entry or document.;
                      ApplicationArea=#Basic,#Suite;
                      Promoted=Yes;
                      Image=Document;
                      PromotedCategory=Category4;
                      OnAction=VAR
                                 IncomingDocument@1000 : Record 130;
                               BEGIN
                                 IncomingDocument.ShowCard("No.","Posting Date");
                               END;
                                }
      { 12      ;2   ;Action    ;
                      ShortCutKey=Shift+F7;
                      CaptionML=[ENU=Customer;
                                 PTG=Cliente];
                      ToolTipML=ENU=View or edit detailed information about the customer.;
                      ApplicationArea=#Basic,#Suite;
                      RunObject=Page 21;
                      RunPageLink=No.=FIELD(Sell-to Customer No.);
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=Customer;
                      PromotedCategory=Category4;
                      Scope=Repeater }
      { 25      ;2   ;Action    ;
                      CaptionML=[ENU=&Navigate;
                                 PTG=&Navegar];
                      ToolTipML=ENU=Find all entries and documents that exist for the document number and posting date on the selected posted sales document.;
                      ApplicationArea=#Basic,#Suite;
                      Promoted=Yes;
                      Visible=NOT IsOfficeAddin;
                      PromotedIsBig=Yes;
                      Image=Navigate;
                      PromotedCategory=Category4;
                      Scope=Repeater;
                      OnAction=BEGIN
                                 Navigate;
                               END;
                                }
      { 36      ;1   ;ActionGroup;
                      CaptionML=[ENU=Cancel;
                                 PTG=Cancelar] }
      { 28      ;2   ;Action    ;
                      Name=CancelCrMemo;
                      CaptionML=[ENU=Cancel;
                                 PTG=Cancelar];
                      ToolTipML=ENU=Create and post a sales invoice that reverses this posted sales credit memo. This posted sales credit memo will be canceled.;
                      ApplicationArea=#Basic,#Suite;
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=Cancel;
                      PromotedCategory=Category5;
                      Scope=Repeater;
                      OnAction=BEGIN
                                 CODEUNIT.RUN(CODEUNIT::"Cancel PstdSalesCrM (Yes/No)",Rec);
                               END;
                                }
      { 24      ;2   ;Action    ;
                      Name=ShowInvoice;
                      CaptionML=[ENU=Show Canceled/Corrective Invoice;
                                 PTG=Mostrar Cancelado/Fatura Corre��o];
                      ToolTipML=ENU=Open the posted sales invoice that was created when you canceled the posted sales credit memo. If the posted sales credit memo is the result of a canceled sales invoice, then canceled invoice will open.;
                      ApplicationArea=#Basic,#Suite;
                      Promoted=Yes;
                      Enabled=Cancelled OR Corrective;
                      PromotedIsBig=Yes;
                      Image=Invoice;
                      PromotedCategory=Category5;
                      Scope=Repeater;
                      OnAction=BEGIN
                                 ShowCanceledOrCorrInvoice;
                               END;
                                }
      { 98      ;1   ;ActionGroup;
                      CaptionML=[ENU=Send;
                                 PTG=Enviar] }
      { 5       ;2   ;Action    ;
                      Name=SendCustom;
                      Ellipsis=Yes;
                      CaptionML=[ENU=Send;
                                 PTG=Enviar];
                      ToolTipML=ENU=Prepare to send the document according to the customer's sending profile, such as attached to an email. The Send document to window opens where you can confirm or select a sending profile.;
                      ApplicationArea=#Basic,#Suite;
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=SendToMultiple;
                      PromotedCategory=Process;
                      Scope=Repeater;
                      OnAction=VAR
                                 SalesCrMemoHeader@1000 : Record 114;
                               BEGIN
                                 SalesCrMemoHeader := Rec;
                                 CurrPage.SETSELECTIONFILTER(SalesCrMemoHeader);
                                 SalesCrMemoHeader.SendRecords;
                               END;
                                }
      { 20      ;2   ;Action    ;
                      Ellipsis=Yes;
                      CaptionML=[ENU=&Print;
                                 PTG=&Imprimir];
                      ToolTipML=ENU=Prepare to print the document. A report request window for the document opens where you can specify what to include on the print-out.;
                      ApplicationArea=#Basic,#Suite;
                      Visible=NOT IsOfficeAddin;
                      Image=Print;
                      Scope=Repeater;
                      OnAction=VAR
                                 SalesCrMemoHeader@1102 : Record 114;
                               BEGIN
                                 SalesCrMemoHeader := Rec;
                                 CurrPage.SETSELECTIONFILTER(SalesCrMemoHeader);
                                 SalesCrMemoHeader.PrintRecords(TRUE);
                               END;
                                }
      { 3       ;2   ;Action    ;
                      CaptionML=[ENU=Send by &Email;
                                 PTG=Enviar por &Email];
                      ToolTipML=ENU=Prepare to send the document by email. The Send Email window opens prefilled for the customer where you can add or change information before you send the email.;
                      ApplicationArea=#Basic,#Suite;
                      Image=Email;
                      Scope=Repeater;
                      OnAction=VAR
                                 SalesCrMemoHeader@1000 : Record 114;
                               BEGIN
                                 SalesCrMemoHeader := Rec;
                                 CurrPage.SETSELECTIONFILTER(SalesCrMemoHeader);
                                 SalesCrMemoHeader.EmailRecords(TRUE);
                               END;
                                }
      { 9       ;1   ;Action    ;
                      Name=ActivityLog;
                      CaptionML=[ENU=Activity Log;
                                 PTG=Registo Atividade];
                      ToolTipML=ENU=View the status and any errors if the document was sent as an electronic document or OCR file through the document exchange service.;
                      ApplicationArea=#Basic,#Suite;
                      Image=Log;
                      OnAction=BEGIN
                                 ShowActivityLog;
                               END;
                                }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 2   ;2   ;Field     ;
                ToolTipML=ENU=Specifies the posted credit memo number.;
                ApplicationArea=#All;
                SourceExpr="No." }

    { 4   ;2   ;Field     ;
                ToolTipML=ENU=Specifies the customer number associated with the credit memo.;
                SourceExpr="Sell-to Customer No." }

    { 6   ;2   ;Field     ;
                CaptionML=[ENU=Customer Name;
                           PTG=Nome Cliente];
                ToolTipML=ENU=Specifies the name of the customer that you shipped the items on the credit memo to.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Sell-to Customer Name" }

    { 37  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the currency code of the credit memo.;
                SourceExpr="Currency Code" }

    { 14  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the date on which the shipment is due for payment.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Due Date" }

    { 13  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the total of the amounts on all the credit memo lines, in the currency of the credit memo. The amount does not include VAT.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr=Amount;
                OnDrillDown=BEGIN
                              SETRANGE("No.");
                              PAGE.RUNMODAL(PAGE::"Posted Sales Credit Memo",Rec)
                            END;
                             }

    { 15  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the total of the amounts in all the amount fields on the credit memo, in the currency of the credit memo. The amount includes VAT.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Amount Including VAT";
                OnDrillDown=BEGIN
                              SETRANGE("No.");
                              PAGE.RUNMODAL(PAGE::"Posted Sales Credit Memo",Rec)
                            END;
                             }

    { 16  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the amount that remains to be paid for the posted sales invoice.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Remaining Amount" }

    { 22  ;2   ;Field     ;
                ToolTipML=ENU=Specifies if the posted sales invoice that relates to this sales credit memo is paid.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr=Paid }

    { 18  ;2   ;Field     ;
                ToolTipML=ENU=Specifies if the posted sales invoice that relates to this sales credit memo has been either corrected or canceled.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr=Cancelled;
                HideValue=NOT Cancelled;
                Style=Unfavorable;
                StyleExpr=Cancelled;
                OnDrillDown=BEGIN
                              ShowCorrectiveInvoice;
                            END;
                             }

    { 34  ;2   ;Field     ;
                ToolTipML=ENU=Specifies if the posted sales invoice has been either corrected or canceled by this sales credit memo.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr=Corrective;
                HideValue=NOT Corrective;
                Style=Unfavorable;
                StyleExpr=Corrective;
                OnDrillDown=BEGIN
                              ShowCancelledInvoice;
                            END;
                             }

    { 27  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the postal code of the address.;
                SourceExpr="Sell-to Post Code";
                Visible=FALSE }

    { 23  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the country/region code of the address.;
                SourceExpr="Sell-to Country/Region Code";
                Visible=FALSE }

    { 35  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the name of the person to contact when you communicate with the customer that you shipped the items on the credit memo to.;
                SourceExpr="Sell-to Contact";
                Visible=FALSE }

    { 127 ;2   ;Field     ;
                ToolTipML=ENU=Specifies the number of the customer the credit memo was sent to.;
                SourceExpr="Bill-to Customer No.";
                Visible=FALSE }

    { 125 ;2   ;Field     ;
                ToolTipML=ENU=Specifies the name of the customer that the credit memo was sent to.;
                SourceExpr="Bill-to Name";
                Visible=FALSE }

    { 33  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the postal code of the address.;
                SourceExpr="Bill-to Post Code";
                Visible=FALSE }

    { 29  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the country/region code of the address.;
                SourceExpr="Bill-to Country/Region Code";
                Visible=FALSE }

    { 115 ;2   ;Field     ;
                ToolTipML=ENU=Specifies the name of the person you regularly contact when you communicate with the customer to whom the credit memo was sent.;
                SourceExpr="Bill-to Contact";
                Visible=FALSE }

    { 111 ;2   ;Field     ;
                ToolTipML=ENU=This field is used with shipments to customers with multiple ship-to addresses.;
                SourceExpr="Ship-to Code";
                Visible=FALSE }

    { 109 ;2   ;Field     ;
                ToolTipML=ENU=Specifies the name of the customer that the items were shipped to.;
                SourceExpr="Ship-to Name";
                Visible=FALSE }

    { 21  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the postal code of the address.;
                SourceExpr="Ship-to Post Code";
                Visible=FALSE }

    { 17  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the country/region code of the address.;
                SourceExpr="Ship-to Country/Region Code";
                Visible=FALSE }

    { 99  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the name of the person you regularly contact at the customer to whom the items were shipped.;
                SourceExpr="Ship-to Contact";
                Visible=FALSE }

    { 97  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the date when the credit memo was posted.;
                SourceExpr="Posting Date";
                Visible=FALSE }

    { 65  ;2   ;Field     ;
                ToolTipML=ENU=Specifies which salesperson is associated with the credit memo.;
                SourceExpr="Salesperson Code";
                Visible=FALSE }

    { 89  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the dimension value code associated with the credit memo.;
                SourceExpr="Shortcut Dimension 1 Code";
                Visible=FALSE }

    { 87  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the dimension value code associated with the credit memo.;
                SourceExpr="Shortcut Dimension 2 Code";
                Visible=FALSE }

    { 91  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the location where the credit memo was registered.;
                SourceExpr="Location Code" }

    { 8   ;2   ;Field     ;
                ToolTipML=ENU=Specifies how many times the credit memo has been printed.;
                SourceExpr="No. Printed" }

    { 1102601003;2;Field  ;
                ToolTipML=ENU=Specifies the date when you created the sales document.;
                SourceExpr="Document Date";
                Visible=FALSE }

    { 1102601005;2;Field  ;
                ToolTipML=ENU=Specifies whether the credit memo has been applied to an already-posted document.;
                SourceExpr="Applies-to Doc. Type";
                Visible=FALSE }

    { 501 ;2   ;Field     ;
                SourceExpr="Customer Posting Group";
                Importance=Standard;
                Visible=FALSE;
                Enabled=TRUE;
                Editable=FALSE }

    { 11  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the status of the document if you are using a document exchange service to send it as an electronic document. The status values are reported by the document exchange service.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Document Exchange Status";
                Visible=DocExchStatusVisible;
                StyleExpr=DocExchStatusStyle;
                OnDrillDown=VAR
                              DocExchServDocStatus@1000 : Codeunit 1420;
                            BEGIN
                              DocExchServDocStatus.DocExchStatusDrillDown(Rec);
                            END;
                             }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 7   ;1   ;Part      ;
                Name=IncomingDocAttachFactBox;
                ApplicationArea=#Basic,#Suite;
                PagePartID=Page193;
                Visible=NOT IsOfficeAddin;
                PartType=Page;
                ShowFilter=No }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {
    VAR
      DocExchStatusStyle@1111 : Text;
      DocExchStatusVisible@1000 : Boolean;
      IsOfficeAddin@1001 : Boolean;

    BEGIN
    END.
  }
}

