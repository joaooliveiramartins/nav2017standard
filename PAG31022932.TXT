OBJECT Page 31022932 Historic G/L Account List
{
  OBJECT-PROPERTIES
  {
    Date=01/02/15;
    Time=13:00:00;
    Version List=NAVPTSS81.00;
  }
  PROPERTIES
  {
    Editable=No;
    CaptionML=[ENU=Historic G/L Account List;
               PTG=Lista Contas C/G Hist�ricas];
    SourceTable=Table31022909;
    DataCaptionFields=Search Name;
    PageType=List;
    OnAfterGetRecord=BEGIN
                       NameIndent := 0;
                       NoOnFormat;
                       NameOnFormat;
                     END;

    ActionList=ACTIONS
    {
      { 1900000003;0 ;ActionContainer;
                      ActionContainerType=RelatedInformation }
      { 1100018 ;1   ;ActionGroup;
                      CaptionML=[ENU=A&ccount;
                                 PTG=&Conta] }
      { 1100019 ;2   ;Action    ;
                      ShortCutKey=Shift+F7;
                      CaptionML=[ENU=Card;
                                 PTG=Ficha];
                      RunObject=Page 31022909;
                      RunPageLink=No.=FIELD(No.),
                                  Date Filter=FIELD(Date Filter),
                                  Global Dimension 1 Filter=FIELD(Global Dimension 1 Filter),
                                  Global Dimension 2 Filter=FIELD(Global Dimension 2 Filter),
                                  Budget Filter=FIELD(Budget Filter),
                                  Business Unit Filter=FIELD(Business Unit Filter);
                      Image=EditLines }
      { 1100023 ;2   ;Action    ;
                      ShortCutKey=Ctrl+F7;
                      CaptionML=[ENU=Ledger E&ntries;
                                 PTG=Movime&ntos];
                      RunObject=Page 20;
                      RunPageView=SORTING(Old G/L Account No.);
                      RunPageLink=Old G/L Account No.=FIELD(No.) }
      { 1100020 ;2   ;Action    ;
                      CaptionML=[ENU=Co&mments;
                                 PTG=Co&ment�rios];
                      RunObject=Page 124;
                      RunPageLink=Table Name=CONST(Historic G/L Account),
                                  No.=FIELD(No.);
                      Image=ViewComments }
      { 1100084 ;2   ;Action    ;
                      ShortCutKey=Shift+Ctrl+D;
                      CaptionML=[ENU=Dimensions;
                                 PTG=Dimens�es];
                      RunObject=Page 540;
                      RunPageLink=Table ID=CONST(31022909),
                                  No.=FIELD(No.);
                      Image=Dimensions }
      { 1100132 ;2   ;Action    ;
                      CaptionML=[ENU=E&xtended Texts;
                                 PTG=Te&xtos Adicionais];
                      RunObject=Page 391;
                      RunPageView=SORTING(Table Name,No.,Language Code,All Language Codes,Starting Date,Ending Date);
                      RunPageLink=Table Name=CONST(Historic G/L Account),
                                  No.=FIELD(No.) }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1100000;1;Group     ;
                IndentationColumnName=NameIndent;
                IndentationControls=Name;
                GroupType=Repeater }

    { 1100002;2;Field     ;
                SourceExpr="No." }

    { 1100004;2;Field     ;
                SourceExpr=Name }

    { 1100006;2;Field     ;
                SourceExpr="Income/Balance" }

    { 1100008;2;Field     ;
                SourceExpr="Account Type";
                Visible=FALSE }

    { 1100010;2;Field     ;
                SourceExpr="Gen. Posting Type" }

    { 1100025;2;Field     ;
                SourceExpr="Gen. Bus. Posting Group" }

    { 1100012;2;Field     ;
                SourceExpr="Gen. Prod. Posting Group" }

    { 1100021;2;Field     ;
                SourceExpr="VAT Bus. Posting Group";
                Visible=FALSE }

    { 1100026;2;Field     ;
                SourceExpr="VAT Prod. Posting Group";
                Visible=FALSE }

    { 1100030;2;Field     ;
                SourceExpr="Direct Posting" }

    { 1100014;2;Field     ;
                SourceExpr="Reconciliation Account" }

    { 1100044;2;Field     ;
                SourceExpr="Income Stmt. Bal. Acc.";
                Visible=FALSE }

  }
  CODE
  {
    VAR
      "No.Emphasize"@19074284 : Boolean INDATASET;
      NameEmphasize@19018670 : Boolean INDATASET;
      NameIndent@19079073 : Integer INDATASET;

    PROCEDURE SetSelection@1110000(VAR HistoricGLAcc@1110000 : Record 31022909);
    BEGIN
      CurrPage.SETSELECTIONFILTER(HistoricGLAcc);
    END;

    PROCEDURE GetSelectionFilter@1110001() : Code[80];
    VAR
      HistoricGLAcc@1110000 : Record 31022909;
      FirstAcc@1110001 : Text[20];
      LastAcc@1110002 : Text[20];
      SelectionFilter@1110003 : Code[80];
      HistoricGLAccCount@1110004 : Integer;
      More@1110005 : Boolean;
    BEGIN
      CurrPage.SETSELECTIONFILTER(HistoricGLAcc);
      HistoricGLAcc.SETCURRENTKEY("No.");
      HistoricGLAccCount := HistoricGLAcc.COUNT;
      IF HistoricGLAccCount > 0 THEN BEGIN
        HistoricGLAcc.FINDFIRST;
        WHILE HistoricGLAccCount > 0 DO BEGIN
          HistoricGLAccCount := HistoricGLAccCount - 1;
          HistoricGLAcc.MARKEDONLY(FALSE);
          FirstAcc := HistoricGLAcc."No.";
          LastAcc := FirstAcc;
          More := (HistoricGLAccCount > 0);
          WHILE More DO
            IF HistoricGLAcc.NEXT = 0 THEN
              More := FALSE
            ELSE
              IF NOT HistoricGLAcc.MARK THEN
                More := FALSE
              ELSE BEGIN
                LastAcc := HistoricGLAcc."No.";
                HistoricGLAccCount := HistoricGLAccCount - 1;
                IF HistoricGLAccCount = 0 THEN
                  More := FALSE;
              END;
          IF SelectionFilter <> '' THEN
            SelectionFilter := SelectionFilter + '|';
          IF FirstAcc = LastAcc THEN
            SelectionFilter := SelectionFilter + FirstAcc
          ELSE
            SelectionFilter := SelectionFilter + FirstAcc + '..' + LastAcc;
          IF HistoricGLAccCount > 0 THEN BEGIN
            HistoricGLAcc.MARKEDONLY(TRUE);
            HistoricGLAcc.NEXT;
          END;
        END;
      END;
      EXIT(SelectionFilter);
    END;

    LOCAL PROCEDURE NoOnFormat@19031395();
    BEGIN
      "No.Emphasize" := "Account Type" <> "Account Type"::Posting;
    END;

    LOCAL PROCEDURE NameOnFormat@19039177();
    BEGIN
      NameIndent := Indentation;
      NameEmphasize := "Account Type" <> "Account Type"::Posting;
    END;

    BEGIN
    END.
  }
}

