OBJECT Page 5619 FA Depreciation Books
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0,NAVPTSS81.00;
  }
  PROPERTIES
  {
    CaptionML=[ENU=FA Depreciation Books;
               PTG=Livros Amortiza��o Imobilizado];
    SourceTable=Table5612;
    DataCaptionFields=FA No.,Depreciation Book Code;
    PageType=List;
    RefreshOnActivate=Yes;
    ActionList=ACTIONS
    {
      { 1900000003;0 ;ActionContainer;
                      ActionContainerType=RelatedInformation }
      { 43      ;1   ;ActionGroup;
                      CaptionML=[ENU=&Depr. Book;
                                 PTG=Livro &Amortiza��o];
                      Image=DepreciationsBooks }
      { 45      ;2   ;Action    ;
                      ShortCutKey=Ctrl+F7;
                      CaptionML=[ENU=Ledger E&ntries;
                                 PTG=Movime&ntos];
                      ToolTipML=ENU=View the ledger entries for the fixed asset.;
                      ApplicationArea=#FixedAssets;
                      RunObject=Page 5604;
                      RunPageView=SORTING(FA No.,Depreciation Book Code);
                      RunPageLink=FA No.=FIELD(FA No.),
                                  Depreciation Book Code=FIELD(Depreciation Book Code);
                      Promoted=No;
                      Image=FixedAssetLedger;
                      PromotedCategory=Process }
      { 46      ;2   ;Action    ;
                      CaptionML=[ENU=Error Ledger Entries;
                                 PTG=Movs. Anulados];
                      ToolTipML=ENU=View the entries that have been posted as a result of you using the Cancel function to cancel an entry.;
                      ApplicationArea=#FixedAssets;
                      RunObject=Page 5605;
                      RunPageView=SORTING(Canceled from FA No.,Depreciation Book Code);
                      RunPageLink=Canceled from FA No.=FIELD(FA No.),
                                  Depreciation Book Code=FIELD(Depreciation Book Code);
                      Image=ErrorFALedgerEntries }
      { 47      ;2   ;Action    ;
                      CaptionML=[ENU=Maintenance Ledger Entries;
                                 PTG=Movs. Manuten��o];
                      ToolTipML=ENU=View the maintenance ledger entries for the selected fixed asset.;
                      ApplicationArea=#FixedAssets;
                      RunObject=Page 5641;
                      RunPageView=SORTING(FA No.,Depreciation Book Code);
                      RunPageLink=FA No.=FIELD(FA No.),
                                  Depreciation Book Code=FIELD(Depreciation Book Code);
                      Image=MaintenanceLedgerEntries }
      { 65      ;2   ;Separator  }
      { 59      ;2   ;Action    ;
                      ShortCutKey=F7;
                      CaptionML=[ENU=Statistics;
                                 PTG=Estat�sticas];
                      ToolTipML=ENU=View detailed historical information about the fixed asset.;
                      ApplicationArea=#FixedAssets;
                      RunObject=Page 5602;
                      RunPageLink=FA No.=FIELD(FA No.),
                                  Depreciation Book Code=FIELD(Depreciation Book Code);
                      Promoted=Yes;
                      Image=Statistics;
                      PromotedCategory=Process }
      { 66      ;2   ;Action    ;
                      CaptionML=[ENU=Main &Asset Statistics;
                                 PTG=Estat�sticas Imob. Princip&al];
                      ToolTipML=ENU=View statistics for all the components that make up the main asset for the selected book. The left side of the General FastTab displays the main asset's book value, depreciable basis and any maintenance expenses posted to the components that comprise the main asset. The right side shows the number of components for the main asset, the first date on which an acquisition and/or disposal entry was posted to one of the assets that comprise the main asset.;
                      ApplicationArea=#FixedAssets;
                      RunObject=Page 5603;
                      RunPageLink=FA No.=FIELD(FA No.),
                                  Depreciation Book Code=FIELD(Depreciation Book Code);
                      Image=StatisticsDocument }
      { 60      ;2   ;Action    ;
                      CaptionML=[ENU=FA Posting Types Overview;
                                 PTG=Vis�o Geral Tipos Registo Imobilizado];
                      ToolTipML=ENU=View accumulated amounts for each field, such as book value, acquisition cost, and depreciation, and for each fixed asset. For every fixed asset, a separate line is shown for each depreciation book linked to the fixed asset.;
                      ApplicationArea=#FixedAssets;
                      RunObject=Page 5662;
                      Image=ShowMatrix }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 2   ;2   ;Field     ;
                ToolTipML=ENU="Specifies the number of the fixed asset. ";
                ApplicationArea=#FixedAssets;
                SourceExpr="FA No." }

    { 4   ;2   ;Field     ;
                ToolTipML=ENU=Specifies the depreciation book that is assigned to the fixed asset.;
                ApplicationArea=#FixedAssets;
                SourceExpr="Depreciation Book Code" }

    { 67  ;2   ;Field     ;
                CaptionML=[ENU=FA Add.-Currency Code;
                           PTG=C�digo Div.-Adic. Imob.];
                ToolTipML=ENU=Specifies the exchange rate to be used if you post in an additional currency.;
                ApplicationArea=#Suite;
                SourceExpr=GetAddCurrCode;
                Visible=FALSE;
                OnAssistEdit=BEGIN
                               ChangeExchangeRate.SetParameterFA("FA Add.-Currency Factor",GetAddCurrCode,WORKDATE);
                               IF ChangeExchangeRate.RUNMODAL = ACTION::OK THEN
                                 "FA Add.-Currency Factor" := ChangeExchangeRate.GetParameter;

                               CLEAR(ChangeExchangeRate);
                             END;
                              }

    { 6   ;2   ;Field     ;
                ToolTipML=ENU=Specifies which posting group is used for the depreciation book when posting fixed asset transactions.;
                ApplicationArea=#FixedAssets;
                SourceExpr="FA Posting Group" }

    { 8   ;2   ;Field     ;
                ToolTipML=ENU=Specifies how depreciation is calculated for the depreciation book.;
                ApplicationArea=#FixedAssets;
                SourceExpr="Depreciation Method" }

    { 14  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the length of the depreciation period, expressed in years.;
                ApplicationArea=#FixedAssets;
                SourceExpr="No. of Depreciation Years" }

    { 10  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the date on which depreciation of the fixed asset starts.;
                ApplicationArea=#FixedAssets;
                SourceExpr="Depreciation Starting Date" }

    { 16  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the length of the depreciation period, expressed in months.;
                ApplicationArea=#FixedAssets;
                SourceExpr="No. of Depreciation Months";
                Visible=FALSE }

    { 33  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the date on which depreciation of the fixed asset ends.;
                ApplicationArea=#FixedAssets;
                SourceExpr="Depreciation Ending Date" }

    { 1110034;2;Field     ;
                SourceExpr="Depreciation start year" }

    { 1110036;2;Field     ;
                SourceExpr="Depreciation start month" }

    { 12  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the percentage to depreciate the fixed asset by the straight-line principle, but with a fixed yearly percentage.;
                ApplicationArea=#FixedAssets;
                SourceExpr="Straight-Line %";
                Visible=FALSE }

    { 18  ;2   ;Field     ;
                ToolTipML=ENU=Specifies an amount to depreciate the fixed asset, by a fixed yearly amount.;
                ApplicationArea=#FixedAssets;
                SourceExpr="Fixed Depr. Amount";
                Visible=FALSE }

    { 20  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the percentage to depreciate the fixed asset by the declining-balance principle, but with a fixed yearly percentage.;
                ApplicationArea=#FixedAssets;
                SourceExpr="Declining-Balance %";
                Visible=FALSE }

    { 37  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the starting date for the user-defined depreciation table if you have entered a code in the Depreciation Table Code field.;
                ApplicationArea=#FixedAssets;
                SourceExpr="First User-Defined Depr. Date";
                Visible=FALSE }

    { 22  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the code of the depreciation table to use if you have selected the User-Defined option in the Depreciation Method field.;
                ApplicationArea=#FixedAssets;
                SourceExpr="Depreciation Table Code";
                Visible=FALSE }

    { 24  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the final rounding amount to use.;
                ApplicationArea=#FixedAssets;
                SourceExpr="Final Rounding Amount";
                Visible=FALSE }

    { 26  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the amount to use as the ending book value.;
                ApplicationArea=#FixedAssets;
                SourceExpr="Ending Book Value";
                Visible=FALSE }

    { 75  ;2   ;Field     ;
                ToolTipML=ENU=Specifies that the default ending book value is ignored, and the value in the Ending Book Value is used.;
                ApplicationArea=#FixedAssets;
                SourceExpr="Ignore Def. Ending Book Value";
                Visible=FALSE }

    { 31  ;2   ;Field     ;
                ToolTipML=ENU=Specifies a decimal number, which will be used as an exchange rate when duplicating journal lines to this depreciation book.;
                ApplicationArea=#FixedAssets;
                SourceExpr="FA Exchange Rate";
                Visible=FALSE }

    { 35  ;2   ;Field     ;
                ToolTipML=ENU=Specifies which checks to perform before posting a journal line.;
                ApplicationArea=#FixedAssets;
                SourceExpr="Use FA Ledger Check";
                Visible=FALSE }

    { 41  ;2   ;Field     ;
                ToolTipML=ENU=Specifies a percentage if you have selected the Allow Depr. below Zero field in the depreciation book.;
                ApplicationArea=#FixedAssets;
                SourceExpr="Depr. below Zero %";
                Visible=FALSE }

    { 39  ;2   ;Field     ;
                ToolTipML=ENU=Specifies a positive amount if you have selected the Allow Depr. below Zero field in the depreciation book.;
                ApplicationArea=#FixedAssets;
                SourceExpr="Fixed Depr. Amount below Zero";
                Visible=FALSE }

    { 61  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the date on which you want to dispose of the fixed asset.;
                ApplicationArea=#FixedAssets;
                SourceExpr="Projected Disposal Date";
                Visible=FALSE }

    { 63  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the expected proceeds from disposal of the fixed asset.;
                ApplicationArea=#FixedAssets;
                SourceExpr="Projected Proceeds on Disposal";
                Visible=FALSE }

    { 49  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the starting date for depreciation of custom 1 entries.;
                ApplicationArea=#FixedAssets;
                SourceExpr="Depr. Starting Date (Custom 1)";
                Visible=FALSE }

    { 51  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the ending date for depreciation of custom 1 entries.;
                ApplicationArea=#FixedAssets;
                SourceExpr="Depr. Ending Date (Custom 1)";
                Visible=FALSE }

    { 53  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the total percentage for depreciation of custom 1 entries.;
                ApplicationArea=#FixedAssets;
                SourceExpr="Accum. Depr. % (Custom 1)";
                Visible=FALSE }

    { 55  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the percentage for depreciation of custom 1 entries for the current year.;
                ApplicationArea=#FixedAssets;
                SourceExpr="Depr. This Year % (Custom 1)";
                Visible=FALSE }

    { 57  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the property class of the asset.;
                ApplicationArea=#FixedAssets;
                SourceExpr="Property Class (Custom 1)";
                Visible=FALSE }

    { 44  ;2   ;Field     ;
                ToolTipML=ENU=Specifies that the Half-Year Convention is to be applied to the selected depreciation method.;
                ApplicationArea=#FixedAssets;
                SourceExpr="Use Half-Year Convention";
                Visible=FALSE }

    { 69  ;2   ;Field     ;
                ToolTipML=ENU=Specifies that the depreciation methods DB1/SL and DB2/SL use the declining balance depreciation amount in the first fiscal year.;
                ApplicationArea=#FixedAssets;
                SourceExpr="Use DB% First Fiscal Year";
                Visible=FALSE }

    { 71  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the ending date of the period during which a temporary fixed depreciation amount will be used.;
                ApplicationArea=#FixedAssets;
                SourceExpr="Temp. Ending Date";
                Visible=FALSE }

    { 73  ;2   ;Field     ;
                ToolTipML=ENU=Specifies a temporary fixed depreciation amount.;
                ApplicationArea=#FixedAssets;
                SourceExpr="Temp. Fixed Depr. Amount";
                Visible=FALSE }

    { 1110002;2;Field     ;
                SourceExpr="Legal Document No." }

    { 1110000;2;Field     ;
                SourceExpr="Legal Tax Code" }

    { 1110006;2;Field     ;
                SourceExpr="Legal Tax" }

    { 1110004;2;Field     ;
                SourceExpr="Legal Tax Description" }

    { 1110008;2;Field     ;
                SourceExpr="Activity Coeficient Code" }

    { 1110010;2;Field     ;
                SourceExpr="Annual or Monthly Depreciation" }

    { 1110012;2;Field     ;
                SourceExpr="Fully depreciated" }

    { 1110014;2;Field     ;
                SourceExpr=Revaluated }

    { 1110016;2;Field     ;
                SourceExpr="Detail by Asset" }

    { 1110018;2;Field     ;
                SourceExpr="Max. Dep Bases" }

    { 1110020;2;Field     ;
                SourceExpr="Accum. Lost Tax" }

    { 1110022;2;Field     ;
                SourceExpr="Last Dep. Code" }

    { 1110024;2;Field     ;
                SourceExpr="Previous Dep. Code" }

    { 1110026;2;Field     ;
                SourceExpr="Expected Life" }

    { 1110028;2;Field     ;
                SourceExpr="Gain on Disposal Year" }

    { 1110030;2;Field     ;
                SourceExpr="Gain on Disposal Value" }

    { 1110032;2;Field     ;
                SourceExpr="Acquisition year" }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {
    VAR
      GLSetup@1000 : Record 98;
      ChangeExchangeRate@1001 : Page 511;
      AddCurrCodeIsFound@1002 : Boolean;

    LOCAL PROCEDURE GetAddCurrCode@1() : Code[10];
    BEGIN
      IF NOT AddCurrCodeIsFound THEN
        GLSetup.GET;
      EXIT(GLSetup."Additional Reporting Currency");
    END;

    BEGIN
    END.
  }
}

