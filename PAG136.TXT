OBJECT Page 136 Posted Purchase Receipt
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0.00.15052,NAVPTSS81.00;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Posted Purchase Receipt;
               PTG=Guia Remessa Compra Registada];
    InsertAllowed=No;
    ModifyAllowed=No;
    SourceTable=Table120;
    PageType=Document;
    OnOpenPage=BEGIN
                 SetSecurityFilterOnRespCenter;
               END;

    ActionList=ACTIONS
    {
      { 1900000003;0 ;ActionContainer;
                      ActionContainerType=RelatedInformation }
      { 45      ;1   ;ActionGroup;
                      CaptionML=[ENU=&Receipt;
                                 PTG=&Guia Remessa];
                      Image=Receipt }
      { 8       ;2   ;Action    ;
                      ShortCutKey=F7;
                      CaptionML=[ENU=Statistics;
                                 PTG=Estat�sticas];
                      RunObject=Page 399;
                      RunPageLink=No.=FIELD(No.);
                      Promoted=Yes;
                      Image=Statistics;
                      PromotedCategory=Process }
      { 72      ;2   ;Action    ;
                      CaptionML=[ENU=Co&mments;
                                 PTG=Co&ment�rios];
                      ApplicationArea=#Suite;
                      RunObject=Page 66;
                      RunPageLink=Document Type=CONST(Receipt),
                                  No.=FIELD(No.),
                                  Document Line No.=CONST(0);
                      Image=ViewComments }
      { 77      ;2   ;Action    ;
                      AccessByPermission=TableData 348=R;
                      ShortCutKey=Shift+Ctrl+D;
                      CaptionML=[ENU=Dimensions;
                                 PTG=Dimens�es];
                      ToolTipML=ENU=View or edit dimensions, such as area, project, or department, that you can assign to sales and purchase documents to distribute costs and analyze transaction history.;
                      ApplicationArea=#Suite;
                      Image=Dimensions;
                      OnAction=BEGIN
                                 ShowDimensions;
                               END;
                                }
      { 99      ;2   ;Action    ;
                      AccessByPermission=TableData 456=R;
                      CaptionML=[ENU=Approvals;
                                 PTG=Aprova��es];
                      ToolTipML=ENU=View a list of the records that are waiting to be approved. For example, you can see who requested the record to be approved, when it was sent, and when it is due to be approved.;
                      ApplicationArea=#Suite;
                      Image=Approvals;
                      OnAction=VAR
                                 ApprovalsMgmt@1000 : Codeunit 1535;
                               BEGIN
                                 ApprovalsMgmt.ShowPostedApprovalEntries(RECORDID);
                               END;
                                }
      { 1900000004;0 ;ActionContainer;
                      ActionContainerType=ActionItems }
      { 47      ;1   ;Action    ;
                      Ellipsis=Yes;
                      CaptionML=[ENU=&Print;
                                 PTG=&Imprimir];
                      Promoted=Yes;
                      Image=Print;
                      PromotedCategory=Process;
                      OnAction=BEGIN
                                 CurrPage.SETSELECTIONFILTER(PurchRcptHeader);
                                 PurchRcptHeader.PrintRecords(TRUE);
                               END;
                                }
      { 48      ;1   ;Action    ;
                      CaptionML=[ENU=&Navigate;
                                 PTG=&Navegar];
                      Promoted=Yes;
                      Image=Navigate;
                      PromotedCategory=Process;
                      OnAction=BEGIN
                                 Navigate;
                               END;
                                }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                CaptionML=[ENU=General;
                           PTG=Geral] }

    { 2   ;2   ;Field     ;
                ToolTipML=ENU=Specifies the number of a general ledger account, item, additional cost, or fixed asset, depending on what you selected in the Type field.;
                ApplicationArea=#Suite;
                SourceExpr="No.";
                Importance=Promoted;
                Editable=FALSE }

    { 4   ;2   ;Field     ;
                ToolTipML=ENU=Specifies the number of the vendor who will delivers the items.;
                ApplicationArea=#Suite;
                SourceExpr="Buy-from Vendor No.";
                Importance=Promoted;
                Editable=FALSE }

    { 102 ;2   ;Field     ;
                SourceExpr="Buy-from Contact No.";
                Editable=FALSE }

    { 50  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the name of the vendor who delivered the items.;
                ApplicationArea=#Suite;
                SourceExpr="Buy-from Vendor Name";
                Editable=FALSE }

    { 52  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the address of the vendor who delivered the items.;
                ApplicationArea=#Suite;
                SourceExpr="Buy-from Address";
                Editable=FALSE }

    { 54  ;2   ;Field     ;
                ToolTipML=ENU=Specifies an additional part of the address of the vendor who delivered the items.;
                ApplicationArea=#Suite;
                SourceExpr="Buy-from Address 2";
                Editable=FALSE }

    { 6   ;2   ;Field     ;
                ToolTipML=ENU=Specifies the post code of the vendor who delivered the items.;
                ApplicationArea=#Suite;
                SourceExpr="Buy-from Post Code";
                Editable=FALSE }

    { 56  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the city of the vendor who delivered the items.;
                ApplicationArea=#Suite;
                SourceExpr="Buy-from City";
                Editable=FALSE }

    { 1110004;2;Field     ;
                SourceExpr="Buy-from County";
                Editable=FALSE }

    { 58  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the contact person at the vendor who delivered the items.;
                ApplicationArea=#Suite;
                SourceExpr="Buy-from Contact";
                Editable=FALSE }

    { 16  ;2   ;Field     ;
                SourceExpr="No. Printed";
                Editable=FALSE }

    { 12  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the posting date of the record.;
                ApplicationArea=#Suite;
                SourceExpr="Posting Date";
                Importance=Promoted;
                Editable=FALSE }

    { 27  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the date when the purchase document was created.;
                ApplicationArea=#Suite;
                SourceExpr="Document Date";
                Editable=FALSE }

    { 82  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the date that you want the vendor to deliver to the ship-to address. The value in the field is used to calculate the latest date you can order the items to have them delivered on the requested receipt date. If you do not need delivery on a specific date, you can leave the field blank.;
                ApplicationArea=#Suite;
                SourceExpr="Requested Receipt Date" }

    { 84  ;2   ;Field     ;
                SourceExpr="Promised Receipt Date" }

    { 107 ;2   ;Field     ;
                SourceExpr="Quote No." }

    { 73  ;2   ;Field     ;
                SourceExpr="Order No.";
                Editable=FALSE }

    { 97  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the vendor's order number.;
                ApplicationArea=#Suite;
                SourceExpr="Vendor Order No.";
                Importance=Promoted;
                Editable=FALSE }

    { 14  ;2   ;Field     ;
                SourceExpr="Vendor Shipment No.";
                Editable=FALSE }

    { 7   ;2   ;Field     ;
                SourceExpr="Order Address Code";
                Editable=FALSE }

    { 10  ;2   ;Field     ;
                ToolTipML=ENU=Specifies which purchaser is associated with the receipt.;
                ApplicationArea=#Suite;
                SourceExpr="Purchaser Code";
                Editable=FALSE }

    { 80  ;2   ;Field     ;
                SourceExpr="Responsibility Center";
                Editable=FALSE }

    { 44  ;1   ;Part      ;
                Name=PurchReceiptLines;
                ApplicationArea=#Suite;
                SubPageLink=Document No.=FIELD(No.);
                PagePartID=Page137;
                PartType=Page }

    { 1905885101;1;Group  ;
                CaptionML=[ENU=Invoicing;
                           PTG=Fatura��o] }

    { 18  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the number of the vendor that you received the invoice from.;
                ApplicationArea=#Suite;
                SourceExpr="Pay-to Vendor No.";
                Importance=Promoted;
                Editable=FALSE }

    { 104 ;2   ;Field     ;
                SourceExpr="Pay-to Contact no.";
                Editable=FALSE }

    { 20  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the name of the vendor that you received the invoice from.;
                ApplicationArea=#Suite;
                SourceExpr="Pay-to Name";
                Editable=FALSE }

    { 22  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the address of the vendor that you received the invoice from.;
                ApplicationArea=#Suite;
                SourceExpr="Pay-to Address";
                Editable=FALSE }

    { 24  ;2   ;Field     ;
                ToolTipML=ENU=Specifies an additional part of the address of the vendor that the invoice was received from.;
                ApplicationArea=#Suite;
                SourceExpr="Pay-to Address 2";
                Editable=FALSE }

    { 67  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the post code of the vendor that you received the invoice from.;
                ApplicationArea=#Suite;
                SourceExpr="Pay-to Post Code";
                Editable=FALSE }

    { 26  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the city of the vendor that you received the invoice from.;
                ApplicationArea=#Suite;
                SourceExpr="Pay-to City";
                Editable=FALSE }

    { 1110002;2;Field     ;
                SourceExpr="Pay-to County";
                Editable=FALSE }

    { 28  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the contact person at the vendor that you received the invoice from.;
                ApplicationArea=#Suite;
                SourceExpr="Pay-to Contact";
                Editable=FALSE }

    { 61  ;2   ;Field     ;
                SourceExpr="Shortcut Dimension 1 Code";
                Editable=FALSE }

    { 63  ;2   ;Field     ;
                SourceExpr="Shortcut Dimension 2 Code";
                Editable=FALSE }

    { 1906801201;1;Group  ;
                CaptionML=[ENU=Shipping;
                           PTG=Envio] }

    { 30  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the name of the customer that items on the purchase order were shipped to, as a drop shipment.;
                ApplicationArea=#Suite;
                SourceExpr="Ship-to Name";
                Editable=FALSE }

    { 32  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the address that items on the purchase order were shipped to, as a drop shipment..;
                ApplicationArea=#Suite;
                SourceExpr="Ship-to Address";
                Editable=FALSE }

    { 34  ;2   ;Field     ;
                ToolTipML=ENU=Specifies an additional part of the address that items on the purchase order were shipped to, as a drop shipment.;
                ApplicationArea=#Suite;
                SourceExpr="Ship-to Address 2";
                Editable=FALSE }

    { 69  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the post code that items on the purchase order were shipped to, as a drop shipment.;
                ApplicationArea=#Suite;
                SourceExpr="Ship-to Post Code";
                Editable=FALSE }

    { 36  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the city that items on the purchase order were shipped to, as a drop shipment.;
                ApplicationArea=#Suite;
                SourceExpr="Ship-to City";
                Editable=FALSE }

    { 1110000;2;Field     ;
                SourceExpr="Ship-to County";
                Editable=FALSE }

    { 38  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the contact person at the customer that items on the purchase order were shipped to, as a drop shipment.;
                ApplicationArea=#Suite;
                SourceExpr="Ship-to Contact";
                Editable=FALSE }

    { 65  ;2   ;Field     ;
                SourceExpr="Location Code";
                Importance=Promoted;
                Editable=FALSE }

    { 88  ;2   ;Field     ;
                SourceExpr="Inbound Whse. Handling Time" }

    { 40  ;2   ;Field     ;
                ToolTipML=ENU=Specifies how the vendor must ship items to you. The shipment method code is copied from this field to purchase documents that you send to the vendor.;
                ApplicationArea=#Suite;
                SourceExpr="Shipment Method Code";
                Editable=FALSE }

    { 90  ;2   ;Field     ;
                SourceExpr="Lead Time Calculation" }

    { 42  ;2   ;Field     ;
                ToolTipML=ENU="Specifies the date you expect the items to be available in your warehouse. If you leave the field blank, it will be calculated as follows: Planned Receipt Date + Safety Lead Time + Inbound Warehouse Handling Time = Expected Receipt Date.";
                ApplicationArea=#Suite;
                SourceExpr="Expected Receipt Date";
                Importance=Promoted;
                Editable=FALSE }

    { 1901792801;1;Group  ;
                CaptionML=[ENU=Payment;
                           PTG=Pagamento] }

    { 1110006;2;Field     ;
                SourceExpr="Pay-at Code";
                Editable=FALSE }

    { 1110008;2;Field     ;
                SourceExpr="Vendor Bank Acc. Code";
                Editable=FALSE }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                Visible=TRUE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {
    VAR
      PurchRcptHeader@1000 : Record 120;

    BEGIN
    END.
  }
}

