OBJECT Table 550 VAT Rate Change Setup
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0;
  }
  PROPERTIES
  {
    CaptionML=[ENU=VAT Rate Change Setup;
               PTG=Configura��o Altera��o Taxa IVA];
  }
  FIELDS
  {
    { 1   ;   ;Primary Key         ;Code10        ;CaptionML=[ENU=Primary Key;
                                                              PTG=Chave Prim�ria] }
    { 10  ;   ;Update Gen. Prod. Post. Groups;Option;
                                                   InitValue=VAT Prod. Posting Group;
                                                   CaptionML=[ENU=Update Gen. Prod. Post. Groups;
                                                              PTG=Atualizar Grs. Contcos. Produto];
                                                   OptionCaptionML=[ENU=VAT Prod. Posting Group,,,No;
                                                                    PTG=Gr. Registo IVA Produto,,,N�o];
                                                   OptionString=VAT Prod. Posting Group,,,No }
    { 15  ;   ;Update G/L Accounts ;Option        ;InitValue=Both;
                                                   CaptionML=[ENU=Update G/L Accounts;
                                                              PTG=Atualizar Contas C/G];
                                                   OptionCaptionML=[ENU=VAT Prod. Posting Group,Gen. Prod. Posting Group,Both,No;
                                                                    PTG=Gr. Registo IVA Produto,Gr. Contabil�stico Produto,Ambos,N�o];
                                                   OptionString=VAT Prod. Posting Group,Gen. Prod. Posting Group,Both,No }
    { 17  ;   ;Update Items        ;Option        ;InitValue=Both;
                                                   AccessByPermission=TableData 27=R;
                                                   CaptionML=[ENU=Update Items;
                                                              PTG=Atualizar Produtos];
                                                   OptionCaptionML=[ENU=VAT Prod. Posting Group,Gen. Prod. Posting Group,Both,No;
                                                                    PTG=Gr. Registo IVA Produto,Gr. Contabil�stico Produto,Ambos,N�o];
                                                   OptionString=VAT Prod. Posting Group,Gen. Prod. Posting Group,Both,No }
    { 18  ;   ;Update Item Templates;Option       ;InitValue=Both;
                                                   AccessByPermission=TableData 27=R;
                                                   CaptionML=[ENU=Update Item Templates;
                                                              PTG=Atualizar Modelos Produto];
                                                   OptionCaptionML=[ENU=VAT Prod. Posting Group,Gen. Prod. Posting Group,Both,No;
                                                                    PTG=Gr. Registo IVA Produto,Gr. Contabil�stico Produto,Ambos,N�o];
                                                   OptionString=VAT Prod. Posting Group,Gen. Prod. Posting Group,Both,No }
    { 19  ;   ;Update Item Charges ;Option        ;InitValue=Both;
                                                   AccessByPermission=TableData 5800=R;
                                                   CaptionML=[ENU=Update Item Charges;
                                                              PTG=Atualizar Encargos Prod.];
                                                   OptionCaptionML=[ENU=VAT Prod. Posting Group,Gen. Prod. Posting Group,Both,No;
                                                                    PTG=Gr. Registo IVA Produto,Gr. Contabil�stico Produto,Ambos,N�o];
                                                   OptionString=VAT Prod. Posting Group,Gen. Prod. Posting Group,Both,No }
    { 21  ;   ;Update Resources    ;Option        ;InitValue=Both;
                                                   AccessByPermission=TableData 156=R;
                                                   CaptionML=[ENU=Update Resources;
                                                              PTG=Atualizar Recursos];
                                                   OptionCaptionML=[ENU=VAT Prod. Posting Group,Gen. Prod. Posting Group,Both,No;
                                                                    PTG=Gr. Registo IVA Produto,Gr. Contabil�stico Produto,Ambos,N�o];
                                                   OptionString=VAT Prod. Posting Group,Gen. Prod. Posting Group,Both,No }
    { 30  ;   ;Update Gen. Journal Lines;Option   ;InitValue=Both;
                                                   CaptionML=[ENU=Update Gen. Journal Lines;
                                                              PTG=Atualizar Linhas Di�rio Geral];
                                                   OptionCaptionML=[ENU=VAT Prod. Posting Group,Gen. Prod. Posting Group,Both,No;
                                                                    PTG=Gr. Registo IVA Produto,Gr. Contabil�stico Produto,Ambos,N�o];
                                                   OptionString=VAT Prod. Posting Group,Gen. Prod. Posting Group,Both,No }
    { 31  ;   ;Update Gen. Journal Allocation;Option;
                                                   InitValue=Both;
                                                   AccessByPermission=TableData 221=R;
                                                   CaptionML=[ENU=Update Gen. Journal Allocation;
                                                              PTG=Atualizar Ocupa��o Di�rio Geral];
                                                   OptionCaptionML=[ENU=VAT Prod. Posting Group,Gen. Prod. Posting Group,Both,No;
                                                                    PTG=Gr. Registo IVA Produto,Gr. Contabil�stico Produto,Ambos,N�o];
                                                   OptionString=VAT Prod. Posting Group,Gen. Prod. Posting Group,Both,No }
    { 32  ;   ;Update Std. Gen. Jnl. Lines;Option ;InitValue=Both;
                                                   CaptionML=[ENU=Update Std. Gen. Jnl. Lines;
                                                              PTG=Atualizar Linhas Di�rio Padr�o];
                                                   OptionCaptionML=[ENU=VAT Prod. Posting Group,Gen. Prod. Posting Group,Both,No;
                                                                    PTG=Gr. Registo IVA Produto,Gr. Contabil�stico Produto,Ambos,N�o];
                                                   OptionString=VAT Prod. Posting Group,Gen. Prod. Posting Group,Both,No }
    { 33  ;   ;Update Res. Journal Lines;Option   ;InitValue=Gen. Prod. Posting Group;
                                                   AccessByPermission=TableData 156=R;
                                                   CaptionML=[ENU=Update Res. Journal Lines;
                                                              PTG=Atualizar Linhas Di�rio Rec.];
                                                   OptionCaptionML=[ENU=,Gen. Prod. Posting Group,,No;
                                                                    PTG=,Gr. Contabil�stico Produto,,N�o];
                                                   OptionString=,Gen. Prod. Posting Group,,No }
    { 35  ;   ;Update Job Journal Lines;Option    ;InitValue=Gen. Prod. Posting Group;
                                                   AccessByPermission=TableData 167=R;
                                                   CaptionML=[ENU=Update Job Journal Lines;
                                                              PTG=Atualizar Linha Di�rio Projeto];
                                                   OptionCaptionML=[ENU=,Gen. Prod. Posting Group,,No;
                                                                    PTG=,Gr. Contabil�stico Produto,,N�o];
                                                   OptionString=,Gen. Prod. Posting Group,,No }
    { 37  ;   ;Update Requisition Lines;Option    ;InitValue=Gen. Prod. Posting Group;
                                                   AccessByPermission=TableData 244=R;
                                                   CaptionML=[ENU=Update Requisition Lines;
                                                              PTG=Atualizar Linhas Requisi��o];
                                                   OptionCaptionML=[ENU=,Gen. Prod. Posting Group,,No;
                                                                    PTG=,Gr. Contabil�stico Produto,,N�o];
                                                   OptionString=,Gen. Prod. Posting Group,,No }
    { 39  ;   ;Update Std. Item Jnl. Lines;Option ;InitValue=Gen. Prod. Posting Group;
                                                   AccessByPermission=TableData 27=R;
                                                   CaptionML=[ENU=Update Std. Item Jnl. Lines;
                                                              PTG=Atualizar Linhas Di�rio Prod. Padr�o];
                                                   OptionCaptionML=[ENU=,Gen. Prod. Posting Group,,No;
                                                                    PTG=,Gr. Contabil�stico Produto,,N�o];
                                                   OptionString=,Gen. Prod. Posting Group,,No }
    { 41  ;   ;Update Service Docs.;Option        ;InitValue=Both;
                                                   AccessByPermission=TableData 5900=R;
                                                   CaptionML=[ENU=Update Service Docs.;
                                                              PTG=Atualizar Docs. Servi�o];
                                                   OptionCaptionML=[ENU=VAT Prod. Posting Group,Gen. Prod. Posting Group,Both,No;
                                                                    PTG=Gr. Registo IVA Produto,Gr. Contabil�stico Produto,Ambos,N�o];
                                                   OptionString=VAT Prod. Posting Group,Gen. Prod. Posting Group,Both,No }
    { 43  ;   ;Update Serv. Price Adj. Detail;Option;
                                                   InitValue=Gen. Prod. Posting Group;
                                                   AccessByPermission=TableData 5900=R;
                                                   CaptionML=[ENU=Update Serv. Price Adj. Detail;
                                                              PTG=Atualizar Detalhe Ajuste Pre�o Serv.];
                                                   OptionCaptionML=[ENU=,Gen. Prod. Posting Group,,No;
                                                                    PTG=,Gr. Contabil�stico Produto,,N�o];
                                                   OptionString=,Gen. Prod. Posting Group,,No }
    { 50  ;   ;Update Sales Documents;Option      ;InitValue=Both;
                                                   AccessByPermission=TableData 36=R;
                                                   CaptionML=[ENU=Update Sales Documents;
                                                              PTG=Atualizar Docs. Venda];
                                                   OptionCaptionML=[ENU=VAT Prod. Posting Group,Gen. Prod. Posting Group,Both,No;
                                                                    PTG=Gr. Registo IVA Produto,Gr. Contabil�stico Produto,Ambos,N�o];
                                                   OptionString=VAT Prod. Posting Group,Gen. Prod. Posting Group,Both,No }
    { 55  ;   ;Update Purchase Documents;Option   ;InitValue=Both;
                                                   AccessByPermission=TableData 38=R;
                                                   CaptionML=[ENU=Update Purchase Documents;
                                                              PTG=Atualizar Docs. Compra];
                                                   OptionCaptionML=[ENU=VAT Prod. Posting Group,Gen. Prod. Posting Group,Both,No;
                                                                    PTG=Gr. Registo IVA Produto,Gr. Contabil�stico Produto,Ambos,N�o];
                                                   OptionString=VAT Prod. Posting Group,Gen. Prod. Posting Group,Both,No }
    { 60  ;   ;Update Production Orders;Option    ;InitValue=Gen. Prod. Posting Group;
                                                   AccessByPermission=TableData 5405=R;
                                                   CaptionML=[ENU=Update Production Orders;
                                                              PTG=Atualizar Ordens Produ��o];
                                                   OptionCaptionML=[ENU=,Gen. Prod. Posting Group,,No;
                                                                    PTG=,Gr. Contabil�stico Produto,,N�o];
                                                   OptionString=,Gen. Prod. Posting Group,,No }
    { 62  ;   ;Update Work Centers ;Option        ;InitValue=Gen. Prod. Posting Group;
                                                   AccessByPermission=TableData 99000758=R;
                                                   CaptionML=[ENU=Update Work Centers;
                                                              PTG=Atualizar Centros Trabalho];
                                                   OptionCaptionML=[ENU=,Gen. Prod. Posting Group,,No;
                                                                    PTG=,Gr. Contabil�stico Produto,,N�o];
                                                   OptionString=,Gen. Prod. Posting Group,,No }
    { 64  ;   ;Update Machine Centers;Option      ;InitValue=Gen. Prod. Posting Group;
                                                   AccessByPermission=TableData 99000758=R;
                                                   CaptionML=[ENU=Update Machine Centers;
                                                              PTG=Atualizar Centros M�quina];
                                                   OptionCaptionML=[ENU=,Gen. Prod. Posting Group,,No;
                                                                    PTG=,Gr. Contabil�stico Produto,,N�o];
                                                   OptionString=,Gen. Prod. Posting Group,,No }
    { 70  ;   ;Update Reminders    ;Option        ;InitValue=VAT Prod. Posting Group;
                                                   AccessByPermission=TableData 36=R;
                                                   CaptionML=[ENU=Update Reminders;
                                                              PTG=Atualizar Avisos];
                                                   OptionCaptionML=[ENU=VAT Prod. Posting Group,,,No;
                                                                    PTG=Grupo Registo IVA Produto,,,N�o];
                                                   OptionString=VAT Prod. Posting Group,,,No }
    { 75  ;   ;Update Finance Charge Memos;Option ;InitValue=VAT Prod. Posting Group;
                                                   CaptionML=[ENU=Update Finance Charge Memos;
                                                              PTG=Atualizar Notas de Juros];
                                                   OptionCaptionML=[ENU=VAT Prod. Posting Group,,,No;
                                                                    PTG=Grupo Registo IVA Produto,,,N�o];
                                                   OptionString=VAT Prod. Posting Group,,,No }
    { 90  ;   ;VAT Rate Change Tool Completed;Boolean;
                                                   InitValue=No;
                                                   CaptionML=[ENU=VAT Rate Change Tool Completed;
                                                              PTG=Ferramenta Altera��o Taxa IVA Conclu�da] }
    { 91  ;   ;Ignore Status on Sales Docs.;Boolean;
                                                   InitValue=Yes;
                                                   CaptionML=[ENU=Ignore Status on Sales Docs.;
                                                              PTG=Ignorar Estado em Docs. Venda] }
    { 92  ;   ;Ignore Status on Purch. Docs.;Boolean;
                                                   InitValue=Yes;
                                                   CaptionML=[ENU=Ignore Status on Purch. Docs.;
                                                              PTG=Ignorar Estado em Docs. Compra] }
    { 93  ;   ;Perform Conversion  ;Boolean       ;CaptionML=[ENU=Perform Conversion;
                                                              PTG=Efetuar Convers�o] }
    { 100 ;   ;Item Filter         ;Text250       ;ValidateTableRelation=No;
                                                   TestTableRelation=No;
                                                   CaptionML=[ENU=Item Filter;
                                                              PTG=Filtro Produto] }
    { 101 ;   ;Account Filter      ;Text250       ;ValidateTableRelation=No;
                                                   TestTableRelation=No;
                                                   CaptionML=[ENU=Account Filter;
                                                              PTG=Filtro Contas Raz�o] }
    { 102 ;   ;Resource Filter     ;Text250       ;ValidateTableRelation=No;
                                                   TestTableRelation=No;
                                                   CaptionML=[ENU=Resource Filter;
                                                              PTG=Filtro Recurso] }
  }
  KEYS
  {
    {    ;Primary Key                             ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    PROCEDURE LookUpItemFilter@57(VAR Text@1001 : Text[250]) : Boolean;
    VAR
      Item@1000 : Record 27;
      ItemList@1003 : Page 31;
    BEGIN
      ItemList.LOOKUPMODE(TRUE);
      ItemList.SETTABLEVIEW(Item);
      IF ItemList.RUNMODAL = ACTION::LookupOK THEN BEGIN
        ItemList.GETRECORD(Item);
        Text := ItemList.GetSelectionFilter;
        EXIT(TRUE);
      END;
      EXIT(FALSE)
    END;

    PROCEDURE LookUpResourceFilter@1(VAR Text@1001 : Text[250]) : Boolean;
    VAR
      Resource@1000 : Record 156;
      ResourceList@1003 : Page 77;
    BEGIN
      ResourceList.LOOKUPMODE(TRUE);
      ResourceList.SETTABLEVIEW(Resource);
      IF ResourceList.RUNMODAL = ACTION::LookupOK THEN BEGIN
        ResourceList.GETRECORD(Resource);
        Text := Resource."No.";
        EXIT(TRUE);
      END;
      EXIT(FALSE)
    END;

    PROCEDURE LookUpGLAccountFilter@2(VAR Text@1001 : Text[250]) : Boolean;
    VAR
      GLAccount@1000 : Record 15;
      GLAccountList@1003 : Page 18;
    BEGIN
      GLAccountList.LOOKUPMODE(TRUE);
      GLAccountList.SETTABLEVIEW(GLAccount);
      IF GLAccountList.RUNMODAL = ACTION::LookupOK THEN BEGIN
        GLAccountList.GETRECORD(GLAccount);
        Text := GLAccountList.GetSelectionFilter;
        EXIT(TRUE);
      END;
      EXIT(FALSE)
    END;

    BEGIN
    END.
  }
}

