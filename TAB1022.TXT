OBJECT Table 1022 Job Planning Line Invoice
{
  OBJECT-PROPERTIES
  {
    Date=07/09/12;
    Time=12:00:00;
    Version List=NAVW17.10;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Job Planning Line Invoice;
               PTG=Fatura Linha Planeamento Projeto];
    LookupPageID=Page1029;
    DrillDownPageID=Page1029;
  }
  FIELDS
  {
    { 1   ;   ;Job No.             ;Code20        ;TableRelation=Job;
                                                   CaptionML=[ENU=Job No.;
                                                              PTG=N� Projeto];
                                                   Editable=No }
    { 2   ;   ;Job Task No.        ;Code20        ;TableRelation="Job Task"."Job Task No." WHERE (Job No.=FIELD(Job No.));
                                                   CaptionML=[ENU=Job Task No.;
                                                              PTG=N� Tarefa Projeto];
                                                   Editable=No }
    { 3   ;   ;Job Planning Line No.;Integer      ;TableRelation="Job Planning Line"."Line No." WHERE (Job No.=FIELD(Job No.),
                                                                                                       Job Task No.=FIELD(Job Task No.));
                                                   CaptionML=[ENU=Job Planning Line No.;
                                                              PTG=N� Linha Planeamento Projeto];
                                                   Editable=No }
    { 4   ;   ;Document Type       ;Option        ;CaptionML=[ENU=Document Type;
                                                              PTG=Tipo Documento];
                                                   OptionCaptionML=[ENU=" ,Invoice,Credit Memo,Posted Invoice,Posted Credit Memo";
                                                                    PTG=" ,Fatura,Nota Cr�dito,Fatura Registada,Nota Cr�dito Registada"];
                                                   OptionString=[ ,Invoice,Credit Memo,Posted Invoice,Posted Credit Memo];
                                                   Editable=No }
    { 5   ;   ;Document No.        ;Code20        ;CaptionML=[ENU=Document No.;
                                                              PTG=N� Documento];
                                                   Editable=No }
    { 6   ;   ;Line No.            ;Integer       ;CaptionML=[ENU=Line No.;
                                                              PTG=N� Linha] }
    { 7   ;   ;Quantity Transferred;Decimal       ;CaptionML=[ENU=Quantity Transferred;
                                                              PTG=Qtd. Transferida];
                                                   Editable=No }
    { 8   ;   ;Transferred Date    ;Date          ;CaptionML=[ENU=Transferred Date;
                                                              PTG=Data Transferido];
                                                   Editable=No }
    { 9   ;   ;Invoiced Date       ;Date          ;CaptionML=[ENU=Invoiced Date;
                                                              PTG=Data Faturado];
                                                   Editable=No }
    { 10  ;   ;Invoiced Amount (LCY);Decimal      ;CaptionML=[ENU=Invoiced Amount (LCY);
                                                              PTG=Valor Faturado (DL)];
                                                   Editable=No }
    { 11  ;   ;Invoiced Cost Amount (LCY);Decimal ;CaptionML=[ENU=Invoiced Cost Amount (LCY);
                                                              PTG=Valor Custo Faturado (DL)];
                                                   Editable=No }
    { 12  ;   ;Job Ledger Entry No.;Integer       ;TableRelation="Job Ledger Entry";
                                                   CaptionML=[ENU=Job Ledger Entry No.;
                                                              PTG=N� Mov. Projeto];
                                                   BlankZero=Yes;
                                                   Editable=No }
  }
  KEYS
  {
    {    ;Job No.,Job Task No.,Job Planning Line No.,Document Type,Document No.,Line No.;
                                                   SumIndexFields=Quantity Transferred,Invoiced Amount (LCY),Invoiced Cost Amount (LCY);
                                                   Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    END.
  }
}

