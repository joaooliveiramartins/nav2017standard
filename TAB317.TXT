OBJECT Table 317 Payable Vendor Ledger Entry
{
  OBJECT-PROPERTIES
  {
    Date=05/08/11;
    Time=12:00:00;
    Version List=NAVW16.00;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Payable Vendor Ledger Entry;
               PTG=Mov. Fornecedor a Pagar];
  }
  FIELDS
  {
    { 1   ;   ;Priority            ;Integer       ;CaptionML=[ENU=Priority;
                                                              PTG=Prioridade] }
    { 2   ;   ;Vendor No.          ;Code20        ;TableRelation=Vendor;
                                                   CaptionML=[ENU=Vendor No.;
                                                              PTG=N� Fornecedor] }
    { 3   ;   ;Entry No.           ;Integer       ;CaptionML=[ENU=Entry No.;
                                                              PTG=N� Mov.] }
    { 4   ;   ;Vendor Ledg. Entry No.;Integer     ;TableRelation="Vendor Ledger Entry";
                                                   CaptionML=[ENU=Vendor Ledg. Entry No.;
                                                              PTG=N� Mov. fornecedor] }
    { 5   ;   ;Amount              ;Decimal       ;CaptionML=[ENU=Amount;
                                                              PTG=Valor];
                                                   AutoFormatType=1 }
    { 6   ;   ;Amount (LCY)        ;Decimal       ;CaptionML=[ENU=Amount (LCY);
                                                              PTG=Valor (DL)];
                                                   AutoFormatType=1 }
    { 7   ;   ;Currency Code       ;Code10        ;TableRelation=Currency;
                                                   CaptionML=[ENU=Currency Code;
                                                              PTG=C�d. Divisa] }
    { 8   ;   ;Positive            ;Boolean       ;CaptionML=[ENU=Positive;
                                                              PTG=Positivo] }
    { 9   ;   ;Future              ;Boolean       ;CaptionML=[ENU=Future;
                                                              PTG=Futuro] }
  }
  KEYS
  {
    {    ;Priority,Vendor No.,Currency Code,Positive,Future,Entry No.;
                                                   Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    END.
  }
}

