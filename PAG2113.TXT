OBJECT Page 2113 O365 Posted Sales Invoice
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0;
  }
  PROPERTIES
  {
    Editable=No;
    CaptionML=[ENU=Sent Invoice;
               PTG=Fatura Enviada];
    SourceTable=Table112;
    PageType=Document;
    OnAfterGetRecord=VAR
                       DummyCustLedgerEntry@1002 : Record 21;
                       O365SalesInvoicePayment@1001 : Codeunit 2105;
                     BEGIN
                       IsFullyPaid := O365SalesInvoicePayment.GetPaymentCustLedgerEntry(DummyCustLedgerEntry,"No.");
                       WorkDescription := GetWorkDescription;
                     END;

    ActionList=ACTIONS
    {
      { 22      ;    ;ActionContainer;
                      CaptionML=[ENU=Invoice;
                                 PTG=Fatura];
                      ActionContainerType=ActionItems }
      { 23      ;1   ;Action    ;
                      Name=Send;
                      CaptionML=[ENU=Resend by email;
                                 PTG=Reenviar por email];
                      ToolTipML=[ENU=Sends the invoice as pdf by email.;
                                 PTG=Enviar a fatura como pdf por email.];
                      ApplicationArea=#Basic,#Suite;
                      Promoted=Yes;
                      Image=Email;
                      PromotedCategory=Process;
                      OnAction=BEGIN
                                 SETRECFILTER;
                                 LOCKTABLE;
                                 FIND;
                                 EmailRecords(FALSE);
                                 FIND;
                               END;
                                }
      { 24      ;1   ;Action    ;
                      Name=ViewPdf;
                      CaptionML=[ENU=View Invoice;
                                 PTG=Ver Fatura];
                      ToolTipML=[ENU=View the final invoice as pdf.;
                                 PTG=Ver a fatura final como pdf.];
                      ApplicationArea=#Basic,#Suite;
                      Promoted=Yes;
                      Image=View;
                      PromotedCategory=Process;
                      OnAction=VAR
                                 ReportSelections@1001 : Record 77;
                                 ReportViewer@1000 : Page 2115;
                               BEGIN
                                 SETRECFILTER;
                                 LOCKTABLE;
                                 FIND;
                                 ReportViewer.SetDocument(Rec,ReportSelections.Usage::"S.Invoice","Sell-to Customer No.");
                                 ReportViewer.RUN;
                                 FIND;
                               END;
                                }
      { 27      ;1   ;Action    ;
                      Name=ShowPayments;
                      CaptionML=[ENU=Show Payments;
                                 PTG=Mostrar Pagamentos];
                      ToolTipML=[ENU=Show a list of payments made for this invoice.;
                                 PTG=Mostrar uma lista de pagamentos feitos para esta fatura.];
                      ApplicationArea=#Basic,#Suite;
                      Promoted=Yes;
                      Image=Payment;
                      PromotedCategory=Process;
                      OnAction=BEGIN
                                 O365SalesInvoicePayment.ShowHistory("No.");
                               END;
                                }
      { 26      ;1   ;Action    ;
                      Name=MarkAsPaid;
                      CaptionML=[ENU=Mark as paid;
                                 PTG=Marcar como paga.];
                      ToolTipML=[ENU=Pay the invoice as specified in the default Payment Registration Setup.;
                                 PTG=Pagar a fatura como especificado por defeito an Conf. Registo Pagamento];
                      ApplicationArea=#Basic,#Suite;
                      Promoted=Yes;
                      Visible=NOT IsFullyPaid;
                      Image=Payment;
                      PromotedCategory=Process;
                      OnAction=BEGIN
                                 O365SalesInvoicePayment.MarkAsPaid("No.");
                               END;
                                }
      { 25      ;1   ;Action    ;
                      Name=MarkAsUnpaid;
                      CaptionML=[ENU=Mark as unpaid;
                                 PTG=Marcar como n�o pago];
                      ToolTipML=[ENU=Mark the invoice as unpaid.;
                                 PTG=Marcar a fatura como n�o paga.];
                      ApplicationArea=#Basic,#Suite;
                      Promoted=Yes;
                      Visible=IsFullyPaid;
                      Image=Cancel;
                      PromotedCategory=Process;
                      OnAction=BEGIN
                                 O365SalesInvoicePayment.CancelSalesInvoicePayment("No.");
                               END;
                                }
    }
  }
  CONTROLS
  {
    { 1   ;0   ;Container ;
                ContainerType=ContentArea }

    { 2   ;1   ;Group     ;
                GroupType=Group }

    { 4   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the number of the record.;
                           PTG=Especifica o n�mero do registo.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="No.";
                Importance=Additional }

    { 3   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the number of the customer who will receive the products and be billed by default. When you fill this field, most of the other fields on the document are filled from the customer card.;
                           PTG=Especifica o n�mero de cliente que vai receber os produtos e ser faturado por defeito. Quando preencher este campo, muitos dos outros campos no documento s�o preenchidos atrav�s da ficha de cliente.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Sell-to Customer No.";
                Importance=Additional;
                Visible=FALSE }

    { 9   ;2   ;Field     ;
                CaptionML=[ENU=Customer Name;
                           PTG=Nome Cliente];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Sell-to Customer Name";
                Importance=Promoted }

    { 10  ;2   ;Field     ;
                CaptionML=[ENU=Address;
                           PTG=Endere�o];
                ToolTipML=[ENU=Specifies the customer's sell-to address.;
                           PTG=Especifica o endere�o vender-a cliente.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Sell-to Address";
                Importance=Additional }

    { 11  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies an additional part of the address of the customer that the items were sold to.;
                           PTG=Especifica uma parte adicional do endere�o do cliente ao qual foram vendidos os produtos.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Sell-to Address 2";
                Importance=Additional;
                Visible=FALSE }

    { 12  ;2   ;Field     ;
                CaptionML=[ENU=City;
                           PTG=Cidade];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Sell-to City";
                Importance=Additional }

    { 13  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the name of the person to contact at the customer that the items were sold to.;
                           PTG=Especifica o nome de pessoa a contactar no cliente ao qual foram vendidos os produtos.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Sell-to Contact";
                Visible=FALSE }

    { 14  ;2   ;Field     ;
                CaptionML=[ENU=" Post Code";
                           PTG=" C�d. Postal"];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Sell-to Post Code";
                Importance=Additional }

    { 15  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the county of the customer that the items were sold to.;
                           PTG=Especifica o distrito do cliente ao qual os produtos foram vendidos.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Sell-to County";
                Importance=Additional;
                Visible=FALSE }

    { 16  ;2   ;Field     ;
                CaptionML=[ENU=Country/Region Code;
                           PTG=C�d. Pa�s/Regi�o];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Sell-to Country/Region Code" }

    { 17  ;2   ;Field     ;
                CaptionML=[ENU=Invoice Date;
                           PTG=Data Fatura];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Document Date" }

    { 5   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the posting date of the record.;
                           PTG=Especifica a data de registo.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Posting Date";
                Importance=Additional;
                Visible=FALSE }

    { 6   ;2   ;Field     ;
                CaptionML=[ENU=Due Date;
                           PTG=Data Vencimento];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Due Date" }

    { 18  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the tax area code that is used to calculate and post sales tax.;
                           PTG=Especifica o c�d. �rea imposto que � usado para calcular e registar imposto venda.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Tax Area Code";
                Importance=Additional }

    { 19  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies if the sales invoice contains sales tax.;
                           PTG=Especifica se a fatura de venda cont�m imposto de venda.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Tax Liable";
                Importance=Additional }

    { 20  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the number of the sales quote that the sales order was created from. You can track the number to sales quote documents that you have printed, saved, or emailed.;
                           PTG=Especifica o n�mero da proposta de venda de qual a encomenda de venda foi criada. Pode associar o n�mero ao documento de proposta de venda que imprimiu, guardou ou envou por email.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Quote No.";
                Importance=Additional;
                Visible=FALSE }

    { 30  ;2   ;Group     ;
                CaptionML=[ENU=Work Description;
                           PTG=Descri��o de Tarefa];
                GroupType=Group }

    { 29  ;3   ;Field     ;
                ToolTipML=[ENU=Specifies the products or service being offered;
                           PTG=Especifica os produtos our servi�os a ser oferecidos];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=WorkDescription;
                Importance=Additional;
                Editable=FALSE;
                MultiLine=Yes;
                ShowCaption=No }

    { 21  ;1   ;Part      ;
                CaptionML=[ENU=Lines;
                           PTG=Linhas];
                ApplicationArea=#Basic,#Suite;
                SubPageLink=Document No.=FIELD(No.);
                PagePartID=Page2114;
                PartType=Page }

    { 7   ;1   ;Field     ;
                CaptionML=[ENU=Net Total;
                           PTG=Total L�quido];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=Amount;
                Importance=Promoted;
                Enabled=FALSE }

    { 8   ;1   ;Field     ;
                CaptionML=[ENU=Total Including Tax;
                           PTG=Totol Inclu�ndo Imposto];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Amount Including VAT";
                Importance=Promoted;
                Enabled=FALSE }

  }
  CODE
  {
    VAR
      O365SalesInvoicePayment@1002 : Codeunit 2105;
      IsFullyPaid@1000 : Boolean;
      WorkDescription@1001 : Text;

    BEGIN
    END.
  }
}

