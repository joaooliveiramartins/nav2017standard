OBJECT Page 579 Post Application
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Post Application;
               PTG=Registar Liquida��o];
    PageType=StandardDialog;
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 3   ;1   ;Group     ;
                CaptionML=[ENU=General;
                           PTG=Geral] }

    { 4   ;2   ;Field     ;
                CaptionML=[ENU=Document No.;
                           PTG=N� Documento];
                ToolTipML=ENU=Specifies the document number of the entry to be applied.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr=DocNo }

    { 7   ;2   ;Field     ;
                CaptionML=[ENU=Posting Date;
                           PTG=Data Registo];
                ToolTipML=ENU=Specifies the posting date of the entry to be applied.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr=PostingDate }

  }
  CODE
  {
    VAR
      DocNo@1000 : Code[20];
      PostingDate@1001 : Date;

    PROCEDURE SetValues@1(NewDocNo@1000 : Code[20];NewPostingDate@1001 : Date);
    BEGIN
      DocNo := NewDocNo;
      PostingDate := NewPostingDate;
    END;

    PROCEDURE GetValues@2(VAR NewDocNo@1000 : Code[20];VAR NewPostingDate@1001 : Date);
    BEGIN
      NewDocNo := DocNo;
      NewPostingDate := PostingDate;
    END;

    BEGIN
    END.
  }
}

