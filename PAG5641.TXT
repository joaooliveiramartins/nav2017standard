OBJECT Page 5641 Maintenance Ledger Entries
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0;
  }
  PROPERTIES
  {
    Editable=No;
    CaptionML=[ENU=Maintenance Ledger Entries;
               PTG=Movs. Manuten��o];
    SourceTable=Table5625;
    DataCaptionFields=FA No.,Depreciation Book Code;
    PageType=List;
    ActionList=ACTIONS
    {
      { 1900000003;0 ;ActionContainer;
                      ActionContainerType=RelatedInformation }
      { 23      ;1   ;ActionGroup;
                      CaptionML=[ENU=Ent&ry;
                                 PTG=M&ovimento];
                      Image=Entry }
      { 26      ;2   ;Action    ;
                      AccessByPermission=TableData 348=R;
                      ShortCutKey=Shift+Ctrl+D;
                      CaptionML=[ENU=Dimensions;
                                 PTG=Dimens�es];
                      ToolTipML=[ENU=View or edits dimensions, such as area, project, or department, that you can assign to sales and purchase documents to distribute costs and analyze transaction history.;
                                 PTG=""];
                      ApplicationArea=#Suite;
                      Image=Dimensions;
                      OnAction=BEGIN
                                 ShowDimensions;
                               END;
                                }
      { 1900000004;0 ;ActionContainer;
                      ActionContainerType=ActionItems }
      { 29      ;1   ;ActionGroup;
                      CaptionML=[ENU=F&unctions;
                                 PTG=F&un��es];
                      Image=Action }
      { 32      ;2   ;Action    ;
                      Name=ReverseTransaction;
                      Ellipsis=Yes;
                      CaptionML=[ENU=Reverse Transaction;
                                 PTG=Estornar Transa��o];
                      ToolTipML=[ENU=Undo an erroneous journal posting.;
                                 PTG=""];
                      ApplicationArea=#FixedAssets;
                      Image=ReverseRegister;
                      OnAction=VAR
                                 ReversalEntry@1000 : Record 179;
                               BEGIN
                                 CLEAR(ReversalEntry);
                                 IF Reversed THEN
                                   ReversalEntry.AlreadyReversedEntry(TABLECAPTION,"Entry No.");
                                 IF "Journal Batch Name" = '' THEN
                                   ReversalEntry.TestFieldError;
                                 IF "Transaction No." = 0 THEN
                                   ERROR(CannotUndoErr,"Entry No.","Depreciation Book Code");
                                 TESTFIELD("G/L Entry No.");
                                 ReversalEntry.ReverseTransaction("Transaction No.");
                               END;
                                }
      { 18      ;1   ;Action    ;
                      CaptionML=[ENU=&Navigate;
                                 PTG=&Navegar];
                      ToolTipML=[ENU=Find all entries and documents that exist for the document number and posting date on the selected entry or document.;
                                 PTG=""];
                      ApplicationArea=#FixedAssets;
                      Promoted=Yes;
                      Image=Navigate;
                      PromotedCategory=Process;
                      OnAction=BEGIN
                                 Navigate.SetDoc("Posting Date","Document No.");
                                 Navigate.RUN;
                               END;
                                }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 8   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the entry's FA posting date.;
                           PTG=""];
                ApplicationArea=#FixedAssets;
                SourceExpr="FA Posting Date" }

    { 12  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the document type that the entry belongs to.;
                           PTG=""];
                ApplicationArea=#FixedAssets;
                SourceExpr="Document Type" }

    { 16  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the document number on the entry.;
                           PTG=""];
                ApplicationArea=#FixedAssets;
                SourceExpr="Document No." }

    { 6   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the number of the fixed asset that the entry is linked to.;
                           PTG=""];
                ApplicationArea=#FixedAssets;
                SourceExpr="FA No." }

    { 27  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the code for the depreciation book that was used when the entry was posted.;
                           PTG=""];
                ApplicationArea=#FixedAssets;
                SourceExpr="Depreciation Book Code" }

    { 20  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies a description of the entry.;
                           PTG=""];
                ApplicationArea=#FixedAssets;
                SourceExpr=Description }

    { 24  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the amount of the entry.;
                           PTG=""];
                ApplicationArea=#FixedAssets;
                SourceExpr=Amount }

    { 19  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the maintenance code that the entry is linked to.;
                           PTG=""];
                ApplicationArea=#FixedAssets;
                SourceExpr="Maintenance Code" }

    { 52  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the dimension value code that the entry is linked to.;
                           PTG=""];
                ApplicationArea=#Suite;
                SourceExpr="Global Dimension 1 Code";
                Visible=FALSE }

    { 54  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the dimension value code that the entry is linked to.;
                           PTG=""];
                ApplicationArea=#Suite;
                SourceExpr="Global Dimension 2 Code";
                Visible=FALSE }

    { 84  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the type of balancing account used in the entry: G/L Account, Bank Account, Customer, Vendor, or Fixed Asset.;
                           PTG=""];
                ApplicationArea=#FixedAssets;
                SourceExpr="Bal. Account Type";
                Visible=FALSE }

    { 14  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the number of the balancing account used on the entry.;
                           PTG=""];
                ApplicationArea=#FixedAssets;
                SourceExpr="Bal. Account No.";
                Visible=FALSE }

    { 60  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the ID of the user that is associated with the entry.;
                           PTG=""];
                ApplicationArea=#FixedAssets;
                SourceExpr="User ID";
                Visible=FALSE }

    { 78  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the source code that is linked to the entry.;
                           PTG=""];
                ApplicationArea=#FixedAssets;
                SourceExpr="Source Code";
                Visible=FALSE }

    { 80  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the reason code on the entry.;
                           PTG=""];
                ApplicationArea=#FixedAssets;
                SourceExpr="Reason Code";
                Visible=FALSE }

    { 30  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies whether the entry has been part of a reverse transaction (correction) made by the Reverse function.;
                           PTG=""];
                ApplicationArea=#FixedAssets;
                SourceExpr=Reversed;
                Visible=FALSE }

    { 33  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the number of the correcting entry.;
                           PTG=""];
                ApplicationArea=#FixedAssets;
                SourceExpr="Reversed by Entry No.";
                Visible=FALSE }

    { 35  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the number of the original entry that was undone by the reverse transaction.;
                           PTG=""];
                ApplicationArea=#FixedAssets;
                SourceExpr="Reversed Entry No.";
                Visible=FALSE }

    { 10  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the entry's posting date.;
                           PTG=""];
                ApplicationArea=#FixedAssets;
                SourceExpr="Posting Date" }

    { 4   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the G/L number for the entry that was created for this maintenance transaction.;
                           PTG=""];
                ApplicationArea=#FixedAssets;
                SourceExpr="G/L Entry No." }

    { 2   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the entry number that the program has given the entry.;
                           PTG=""];
                ApplicationArea=#FixedAssets;
                SourceExpr="Entry No." }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {
    VAR
      Navigate@1000 : Page 344;
      CannotUndoErr@1001 : TextConst 'ENU=You cannot undo the Maintenance Ledger Entry No. %1 by using the Reverse Transaction function because Depreciation Book %2 does not have the appropriate G/L integration setup.;PTG=N�o pode anular o Mov. Contco. Manuten��o N� %1 usando a fun��o Estornar Transa��o porque o Livro Amortiza��o %2 n�o tem a configura��o de integra��o C/G apropriada.';

    BEGIN
    END.
  }
}

