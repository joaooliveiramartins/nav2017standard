OBJECT Page 31023026 Post. PO Analysis LCY Fact Box
{
  OBJECT-PROPERTIES
  {
    Date=01/02/15;
    Time=13:00:00;
    Version List=NAVPTSS81.00;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Post. PO Analysis LC;
               PTG=An�lise Ordens Pagamento DL Registadas];
    InsertAllowed=No;
    DeleteAllowed=No;
    ModifyAllowed=No;
    SourceTable=Table31022953;
    DataCaptionExpr=Caption;
    PageType=CardPart;
    OnAfterGetRecord=BEGIN
                       UpdateStatistics;
                     END;

  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 14  ;1   ;Field     ;
                ApplicationArea=#All;
                SourceExpr="Currency Code" }

    { 13  ;1   ;Field     ;
                ApplicationArea=#All;
                SourceExpr="Amount Grouped" }

    { 2   ;1   ;Field     ;
                ApplicationArea=#All;
                SourceExpr="Remaining Amount" }

    { 9   ;1   ;Group     ;
                CaptionML=[ENU=No. of Documents;
                           PTG=N� Documentos] }

    { 11  ;2   ;Field     ;
                CaptionML=[ENU=Open;
                           PTG=Pendente];
                ApplicationArea=#All;
                SourceExpr=NoOpen;
                Editable=FALSE }

    { 10  ;2   ;Field     ;
                CaptionML=[ENU=Honored;
                           PTG=Pago];
                ApplicationArea=#All;
                SourceExpr=NoHonored;
                Editable=FALSE }

    { 17  ;2   ;Field     ;
                CaptionML=[ENU=Rejected;
                           PTG=Devolvido];
                ApplicationArea=#All;
                SourceExpr=NoRejected;
                Editable=FALSE }

    { 28  ;2   ;Field     ;
                CaptionML=[ENU=Redrawn;
                           PTG=Reformado];
                ApplicationArea=#All;
                SourceExpr=NoRedrawn;
                Editable=FALSE }

    { 23  ;1   ;Group     ;
                CaptionML=[ENU=Amount (LCY);
                           PTG=Valor (DL)] }

    { 24  ;2   ;Field     ;
                CaptionML=[ENU=Open;
                           PTG=Pendente];
                ApplicationArea=#All;
                SourceExpr=OpenAmtLCY;
                AutoFormatType=1;
                Editable=FALSE }

    { 25  ;2   ;Field     ;
                CaptionML=[ENU=Honored;
                           PTG=Pago];
                ApplicationArea=#All;
                SourceExpr=HonoredAmtLCY;
                AutoFormatType=1;
                Editable=FALSE }

    { 26  ;2   ;Field     ;
                CaptionML=[ENU=Rejected;
                           PTG=Devolvido];
                ApplicationArea=#All;
                SourceExpr=RejectedAmtLCY;
                AutoFormatType=1;
                Editable=FALSE }

    { 30  ;2   ;Field     ;
                CaptionML=[ENU=Redrawn;
                           PTG=Reformado];
                ApplicationArea=#All;
                SourceExpr=RedrawnAmtLCY;
                AutoFormatType=1;
                Editable=FALSE }

  }
  CODE
  {
    VAR
      PostedDoc@1110000 : Record 31022936;
      OpenAmt@1110001 : Decimal;
      HonoredAmt@1110002 : Decimal;
      RejectedAmt@1110003 : Decimal;
      RedrawnAmt@1110004 : Decimal;
      OpenAmtLCY@1110005 : Decimal;
      HonoredAmtLCY@1110006 : Decimal;
      RejectedAmtLCY@1110007 : Decimal;
      RedrawnAmtLCY@1110008 : Decimal;
      NoOpen@1110009 : Integer;
      NoHonored@1110010 : Integer;
      NoRejected@1110011 : Integer;
      NoRedrawn@1110012 : Integer;

    LOCAL PROCEDURE UpdateStatistics@1();
    BEGIN
      WITH PostedDoc DO BEGIN
        SETCURRENTKEY("Bill Gr./Pmt. Order No.",Status,"Category Code",Redrawn,"Due Date");
        SETRANGE(Type,Type::Payable);
        SETRANGE("Bill Gr./Pmt. Order No.",Rec."No.");
        Rec.COPYFILTER("Due Date Filter","Due Date");
        Rec.COPYFILTER("Global Dimension 1 Filter","Global Dimension 1 Code");
        Rec.COPYFILTER("Global Dimension 2 Filter","Global Dimension 2 Code");
        Rec.COPYFILTER("Category Filter","Category Code");

        SETRANGE(Status,Status::Open);
        CALCSUMS("Amount for Collection","Amt. for Collection (LCY)");
        OpenAmt := "Amount for Collection";
        OpenAmtLCY := "Amt. for Collection (LCY)";
        NoOpen := COUNT;

        SETRANGE(Status);
        SETRANGE(Redrawn,TRUE);
        CALCSUMS("Amount for Collection","Amt. for Collection (LCY)");
        RedrawnAmt := "Amount for Collection";
        RedrawnAmtLCY := "Amt. for Collection (LCY)";
        NoRedrawn := COUNT;
        SETRANGE(Redrawn);

        SETRANGE(Status,Status::Honored);
        CALCSUMS("Amount for Collection","Amt. for Collection (LCY)");
        HonoredAmt := "Amount for Collection" - RedrawnAmt;
        HonoredAmtLCY := "Amt. for Collection (LCY)" - RedrawnAmtLCY;
        NoHonored := COUNT - NoRedrawn;

        SETRANGE(Status,Status::Rejected);
        CALCSUMS("Amount for Collection","Amt. for Collection (LCY)");
        RejectedAmt := "Amount for Collection";
        RejectedAmtLCY := "Amt. for Collection (LCY)";
        NoRejected := COUNT;
      END;
    END;

    BEGIN
    END.
  }
}

