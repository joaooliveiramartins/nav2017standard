OBJECT Table 1313 Activities Cue
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Activities Cue;
               PTG=Lista Atividades];
  }
  FIELDS
  {
    { 1   ;   ;Primary Key         ;Code10        ;CaptionML=[ENU=Primary Key;
                                                              PTG=Chave Prim�ria] }
    { 2   ;   ;Due Date Filter     ;Date          ;FieldClass=FlowFilter;
                                                   CaptionML=[ENU=Due Date Filter;
                                                              PTG=Filtro Data Vencimento];
                                                   Editable=No }
    { 3   ;   ;Overdue Date Filter ;Date          ;FieldClass=FlowFilter;
                                                   CaptionML=[ENU=Overdue Date Filter;
                                                              PTG=Filtro Data Vencida] }
    { 4   ;   ;Ongoing Sales Invoices;Integer     ;FieldClass=FlowField;
                                                   CalcFormula=Count("Sales Header" WHERE (Document Type=FILTER(Invoice)));
                                                   CaptionML=[ENU=Ongoing Sales Invoices;
                                                              PTG=Faturas Venda em Curso] }
    { 5   ;   ;Ongoing Purchase Invoices;Integer  ;FieldClass=FlowField;
                                                   CalcFormula=Count("Purchase Header" WHERE (Document Type=FILTER(Invoice)));
                                                   CaptionML=[ENU=Ongoing Purchase Invoices;
                                                              PTG=Faturas Compra em Curso] }
    { 6   ;   ;Sales This Month    ;Decimal       ;CaptionML=[ENU=Sales This Month;
                                                              PTG=Vendas Este M�s];
                                                   DecimalPlaces=0:0;
                                                   AutoFormatType=10;
                                                   AutoFormatExpr=GetAmountFormat }
    { 7   ;   ;Top 10 Customer Sales YTD;Decimal  ;CaptionML=[ENU=Top 10 Customer Sales YTD;
                                                              PTG=10 Melhores Vendas Cliente �lt. Ano];
                                                   AutoFormatType=10;
                                                   AutoFormatExpr='<Precision,1:1><Standard Format,9>%' }
    { 8   ;   ;Overdue Purch. Invoice Amount;Decimal;
                                                   CaptionML=[ENU=Overdue Purch. Invoice Amount;
                                                              PTG=Valor Vencido Fatura Compra];
                                                   DecimalPlaces=0:0;
                                                   AutoFormatType=10;
                                                   AutoFormatExpr=GetAmountFormat }
    { 9   ;   ;Overdue Sales Invoice Amount;Decimal;
                                                   CaptionML=[ENU=Overdue Sales Invoice Amount;
                                                              PTG=Valor Vencido Fatura Venda];
                                                   DecimalPlaces=0:0;
                                                   AutoFormatType=10;
                                                   AutoFormatExpr=GetAmountFormat }
    { 10  ;   ;Average Collection Days;Decimal    ;CaptionML=[ENU=Average Collection Days;
                                                              PTG=M�dia Dias Pagamento];
                                                   DecimalPlaces=1:1 }
    { 11  ;   ;Ongoing Sales Quotes;Integer       ;FieldClass=FlowField;
                                                   CalcFormula=Count("Sales Header" WHERE (Document Type=FILTER(Quote)));
                                                   CaptionML=[ENU=Ongoing Sales Quotes;
                                                              PTG=Propostas Venda em Curso] }
    { 12  ;   ;Requests to Approve ;Integer       ;FieldClass=FlowField;
                                                   CalcFormula=Count("Approval Entry" WHERE (Approver ID=FIELD(User ID Filter),
                                                                                             Status=FILTER(Open)));
                                                   CaptionML=[ENU=Requests to Approve;
                                                              PTG=Pedidos para Aprova��o] }
    { 13  ;   ;Sales Inv. - Pending Doc.Exch.;Integer;
                                                   FieldClass=FlowField;
                                                   CalcFormula=Count("Sales Invoice Header" WHERE (Document Exchange Status=FILTER(Sent to Document Exchange Service|Delivery Failed)));
                                                   CaptionML=[ENU=Sales Invoices - Pending Document Exchange;
                                                              PTG=Faturas Venda - Interc�mbio Documento Pendente];
                                                   Editable=No }
    { 14  ;   ;Sales CrM. - Pending Doc.Exch.;Integer;
                                                   FieldClass=FlowField;
                                                   CalcFormula=Count("Sales Cr.Memo Header" WHERE (Document Exchange Status=FILTER(Sent to Document Exchange Service|Delivery Failed)));
                                                   CaptionML=[ENU=Sales Credit Memos - Pending Document Exchange;
                                                              PTG=Notas Cr�dito Venda - Interc�mbio Documento Pendente];
                                                   Editable=No }
    { 15  ;   ;User ID Filter      ;Code50        ;FieldClass=FlowFilter;
                                                   CaptionML=[ENU=User ID Filter;
                                                              PTG=Filtro ID Utilizador] }
    { 17  ;   ;Due Next Week Filter;Date          ;FieldClass=FlowFilter;
                                                   CaptionML=[ENU=Due Next Week Filter;
                                                              PTG=Filtro Vencimento Pr�xima Semana] }
    { 20  ;   ;My Incoming Documents;Integer      ;FieldClass=FlowField;
                                                   CalcFormula=Count("Incoming Document" WHERE (Processed=CONST(No)));
                                                   CaptionML=[ENU=My Incoming Documents;
                                                              PTG=Meus Documentos Recebidos] }
    { 21  ;   ;Non-Applied Payments;Integer       ;FieldClass=FlowField;
                                                   CalcFormula=Count("Bank Acc. Reconciliation" WHERE (Statement Type=CONST(Payment Application)));
                                                   CaptionML=[ENU=Non-Applied Payments;
                                                              PTG=Pagamento Por Liquidar] }
    { 22  ;   ;Purch. Invoices Due Next Week;Integer;
                                                   FieldClass=FlowField;
                                                   CalcFormula=Count("Vendor Ledger Entry" WHERE (Document Type=FILTER(Invoice|Credit Memo),
                                                                                                  Due Date=FIELD(Due Next Week Filter),
                                                                                                  Open=CONST(Yes)));
                                                   CaptionML=[ENU=Purch. Invoices Due Next Week;
                                                              PTG=Faturas Compra a Vencer Pr�xima Semana];
                                                   Editable=No }
    { 23  ;   ;Sales Invoices Due Next Week;Integer;
                                                   FieldClass=FlowField;
                                                   CalcFormula=Count("Cust. Ledger Entry" WHERE (Document Type=FILTER(Invoice|Credit Memo),
                                                                                                 Due Date=FIELD(Due Next Week Filter),
                                                                                                 Open=CONST(Yes)));
                                                   CaptionML=[ENU=Sales Invoices Due Next Week;
                                                              PTG=Faturas Venda a Vencer Pr�xima Semana];
                                                   Editable=No }
    { 24  ;   ;Ongoing Sales Orders;Integer       ;FieldClass=FlowField;
                                                   CalcFormula=Count("Sales Header" WHERE (Document Type=FILTER(Order)));
                                                   CaptionML=[ENU=Ongoing Sales Orders;
                                                              PTG=Encomendas Venda Ativas] }
    { 25  ;   ;Inc. Doc. Awaiting Verfication;Integer;
                                                   FieldClass=FlowField;
                                                   CalcFormula=Count("Incoming Document" WHERE (OCR Status=CONST(Awaiting Verification)));
                                                   CaptionML=[ENU=Inc. Doc. Awaiting Verfication;
                                                              PTG=Doc. Recebido � Espera de Verfica��o] }
    { 26  ;   ;Purchase Orders     ;Integer       ;FieldClass=FlowField;
                                                   CalcFormula=Count("Purchase Header" WHERE (Document Type=FILTER(Order)));
                                                   CaptionML=[ENU=Purchase Orders;
                                                              PTG=Encomendas Compra] }
  }
  KEYS
  {
    {    ;Primary Key                             ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    LOCAL PROCEDURE GetAmountFormat@1() : Text;
    BEGIN
      EXIT('<Precision,0:0><Standard Format,0>');
    END;

    BEGIN
    END.
  }
}

