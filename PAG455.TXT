OBJECT Page 455 Fin. Charge Comment List
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0;
  }
  PROPERTIES
  {
    Editable=No;
    CaptionML=[ENU=Comment List;
               PTG=Lista Coment�rios];
    LinksAllowed=No;
    SourceTable=Table306;
    DataCaptionExpr=Caption(Rec);
    DelayedInsert=Yes;
    PageType=List;
    AutoSplitKey=Yes;
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 11  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the type of document the comment is attached to: either Finance Charge Memo or Issued Finance Charge Memo.;
                SourceExpr=Type }

    { 2   ;2   ;Field     ;
                ToolTipML=ENU=Specifies the document number of the finance charge memo to which the comment applies.;
                SourceExpr="No." }

    { 4   ;2   ;Field     ;
                ToolTipML=ENU=Specifies the date the comment was created.;
                SourceExpr=Date }

    { 6   ;2   ;Field     ;
                ToolTipML=ENU=Specifies the comment itself.;
                SourceExpr=Comment }

  }
  CODE
  {
    VAR
      Text000@1001 : TextConst '@@@=it is a caption for empty page;ENU=untitled;PTG=sem t��tulo';
      Text001@1000 : TextConst 'ENU=Fin. Charge Memo;PTG=Nota juros';

    LOCAL PROCEDURE Caption@1(FinChrgCommentLine@1000 : Record 306) : Text[110];
    BEGIN
      IF FinChrgCommentLine."No." = '' THEN
        EXIT(Text000);
      EXIT(Text001 + ' ' + FinChrgCommentLine."No." + ' ');
    END;

    BEGIN
    END.
  }
}

