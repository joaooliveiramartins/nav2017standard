OBJECT Page 6079 Contract Amount Distribution
{
  OBJECT-PROPERTIES
  {
    Date=07/09/12;
    Time=12:00:00;
    Version List=NAVW17.00;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Contract Amount Distribution;
               PTG=Distribui��o Valores Contrato];
    PageType=ConfirmationDialog;
    OnInit=BEGIN
             CurrPage.LOOKUPMODE := TRUE;
           END;

  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 3   ;1   ;Field     ;
                CaptionML=[ENU=Do you want the differences to be distributed to the contract lines by;
                           PTG=Pretende que as diferen�as sejam distribu�das pelas linhas do contrato por];
                OptionCaptionML=[ENU=Even Distribution?,Distribution Based on Profit?,Distribution Based on Line Amount?;
                                 PTG=Distribui��o Uniforme?,Distribui��o Baseada na Margem?,Distribui��o Baseada no Valor da Linha?];
                SourceExpr=Result }

    { 1   ;1   ;Group     ;
                CaptionML=[ENU=Details;
                           PTG=Detalhes];
                InstructionalTextML=[ENU=The Annual Amount and the Calcd. Annual Amount must be the same.;
                                     PTG=O Valor Anual e o Valor Anual Calculado devem ser o mesmo.] }

    { 2   ;2   ;Field     ;
                CaptionML=[ENU=Annual Amount;
                           PTG=Valor Anual];
                SourceExpr=AnnualAmount;
                Editable=FALSE }

    { 8   ;2   ;Field     ;
                CaptionML=[ENU=Calcd. Annual Amount;
                           PTG=Valor Anual Calculado];
                SourceExpr=CalcdAnnualAmount;
                Editable=FALSE }

    { 10  ;2   ;Field     ;
                CaptionML=[ENU=Difference;
                           PTG=Diferen�a];
                SourceExpr=Difference;
                Editable=FALSE }

  }
  CODE
  {
    VAR
      Result@1020 : '0,1,2';
      AnnualAmount@1001 : Decimal;
      CalcdAnnualAmount@1002 : Decimal;
      Difference@1003 : Decimal;

    PROCEDURE GetResult@1() : Integer;
    BEGIN
      EXIT(Result);
    END;

    PROCEDURE SetValues@2(AnnualAmount2@1000 : Decimal;CalcdAnnualAmount2@1001 : Decimal);
    BEGIN
      AnnualAmount := AnnualAmount2;
      CalcdAnnualAmount := CalcdAnnualAmount2;
      Difference := AnnualAmount2 - CalcdAnnualAmount2;
    END;

    PROCEDURE SetResult@3(Option@1000 : Option);
    BEGIN
      Result := Option;
    END;

    BEGIN
    END.
  }
}

