OBJECT Table 8383 Dimensions Field Map
{
  OBJECT-PROPERTIES
  {
    Date=07/09/12;
    Time=12:00:00;
    Version List=NAVW17.00;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Dimensions Field Map;
               PTG=Map. Campo Dimens�es];
  }
  FIELDS
  {
    { 1   ;   ;Table No.           ;Integer       ;CaptionML=[ENU=Table No.;
                                                              PTG=N� Tabela] }
    { 2   ;   ;Global Dim.1 Field No.;Integer     ;CaptionML=[ENU=Global Dim.1 Field No.;
                                                              PTG=N� Campo Dim.1 Global] }
    { 3   ;   ;Global Dim.2 Field No.;Integer     ;CaptionML=[ENU=Global Dim.2 Field No.;
                                                              PTG=N� Campo Dim.2 Global] }
    { 4   ;   ;ID Field No.        ;Integer       ;CaptionML=[ENU=ID Field No.;
                                                              PTG=N� Campo ID] }
  }
  KEYS
  {
    {    ;Table No.                               ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    END.
  }
}

