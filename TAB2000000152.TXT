OBJECT Table 2000000152 NAV App Data Archive
{
  OBJECT-PROPERTIES
  {
    Date=13/04/17;
    Time=14:36:27;
    Version List=NAVW19.00;
  }
  PROPERTIES
  {
    DataPerCompany=No;
    CaptionML=[ENU=NAV App Data Archive;
               PTG=Arquivo Dados NAV App];
  }
  FIELDS
  {
    { 1   ;   ;App ID              ;GUID          ;CaptionML=[ENU=App ID;
                                                              PTG=ID App] }
    { 2   ;   ;Table ID            ;Integer       ;CaptionML=[ENU=Table ID;
                                                              PTG=ID Tabela] }
    { 3   ;   ;Company Name        ;Text30        ;CaptionML=[ENU=Company Name;
                                                              PTG=Nome Empresa] }
    { 4   ;   ;Version Major       ;Integer       ;CaptionML=[ENU=Version Major;
                                                              PTG=Vers�o Major] }
    { 5   ;   ;Version Minor       ;Integer       ;CaptionML=[ENU=Version Minor;
                                                              PTG=Vers�o Minor] }
    { 6   ;   ;Version Build       ;Integer       ;CaptionML=[ENU=Version Build;
                                                              PTG=Vers�o Build] }
    { 7   ;   ;Version Revision    ;Integer       ;CaptionML=[ENU=Version Revision;
                                                              PTG=Vers�o Revis�o] }
    { 8   ;   ;Archive Table Name  ;Text128       ;CaptionML=[ENU=Archive Table Name;
                                                              PTG=Nome Tabela Arquivo] }
    { 9   ;   ;Metadata            ;BLOB          ;CaptionML=[ENU=Metadata;
                                                              PTG=Metadados] }
  }
  KEYS
  {
    {    ;App ID,Table ID,Company Name            ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    END.
  }
}

