OBJECT Page 914 Assemble-to-Order Lines
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Assemble-to-Order Lines;
               PTG=Linhas Montagem-para-Encomenda];
    SourceTable=Table901;
    DataCaptionExpr=GetCaption;
    DelayedInsert=Yes;
    PopulateAllFields=Yes;
    PageType=Worksheet;
    AutoSplitKey=Yes;
    OnAfterGetRecord=BEGIN
                       UpdateAvailWarning;
                     END;

    OnDeleteRecord=VAR
                     AssemblyLineReserve@1000 : Codeunit 926;
                   BEGIN
                     IF (Quantity <> 0) AND ItemExists("No.") THEN BEGIN
                       COMMIT;
                       IF NOT AssemblyLineReserve.DeleteLineConfirm(Rec) THEN
                         EXIT(FALSE);
                       AssemblyLineReserve.DeleteLine(Rec);
                     END;
                   END;

    ActionList=ACTIONS
    {
      { 27      ;    ;ActionContainer;
                      CaptionML=[ENU=Line;
                                 PTG=Linha];
                      ActionContainerType=ActionItems }
      { 37      ;1   ;Action    ;
                      Ellipsis=Yes;
                      CaptionML=[ENU=&Reserve;
                                 PTG=&Reserva];
                      Promoted=Yes;
                      Image=LineReserve;
                      PromotedCategory=Process;
                      OnAction=BEGIN
                                 ShowReservation;
                               END;
                                }
      { 26      ;1   ;Action    ;
                      CaptionML=[ENU=Select Item Substitution;
                                 PTG=Selecionar Subs. Produto];
                      Image=SelectItemSubstitution;
                      OnAction=BEGIN
                                 ShowItemSub;
                                 CurrPage.UPDATE;
                               END;
                                }
      { 14      ;1   ;Action    ;
                      CaptionML=[ENU=Explode BOM;
                                 PTG=Expandir L.M.];
                      Image=ExplodeBOM;
                      OnAction=BEGIN
                                 ExplodeAssemblyList;
                                 CurrPage.UPDATE;
                               END;
                                }
      { 16      ;1   ;Action    ;
                      CaptionML=[ENU=Assembly BOM;
                                 PTG=L.M. Montagem];
                      Image=BulletList;
                      OnAction=BEGIN
                                 ShowAssemblyList;
                               END;
                                }
      { 44      ;1   ;Action    ;
                      Ellipsis=Yes;
                      CaptionML=[ENU=Create Inventor&y Movement;
                                 PTG=Criar Movimento Invent�rio];
                      Image=CreatePutAway;
                      OnAction=VAR
                                 AssemblyHeader@1000 : Record 900;
                                 ATOMovementsCreated@1001 : Integer;
                                 TotalATOMovementsToBeCreated@1002 : Integer;
                               BEGIN
                                 AssemblyHeader.GET("Document Type","Document No.");
                                 AssemblyHeader.CreateInvtMovement(FALSE,FALSE,FALSE,ATOMovementsCreated,TotalATOMovementsToBeCreated);
                               END;
                                }
      { 45      ;    ;ActionContainer;
                      ActionContainerType=RelatedInformation }
      { 36      ;1   ;Action    ;
                      CaptionML=[ENU=Show Document;
                                 PTG=Mostrar Documento];
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=View;
                      PromotedCategory=Process;
                      OnAction=VAR
                                 ATOLink@1002 : Record 904;
                                 SalesLine@1000 : Record 37;
                               BEGIN
                                 ATOLink.GET("Document Type","Document No.");
                                 SalesLine.GET(ATOLink."Document Type",ATOLink."Document No.",ATOLink."Document Line No.");
                                 ATOLink.ShowAsm(SalesLine);
                               END;
                                }
      { 23      ;1   ;Action    ;
                      AccessByPermission=TableData 348=R;
                      ShortCutKey=Shift+Ctrl+D;
                      CaptionML=[ENU=Dimensions;
                                 PTG=Dimens�es];
                      ToolTipML=ENU=View or edit dimensions, such as area, project, or department, that you can assign to sales and purchase documents to distribute costs and analyze transaction history.;
                      Image=Dimensions;
                      OnAction=BEGIN
                                 ShowDimensions;
                               END;
                                }
      { 40      ;1   ;Action    ;
                      ShortCutKey=Shift+Ctrl+I;
                      CaptionML=[ENU=Item &Tracking Lines;
                                 PTG=Linhas Ras&treio Produto];
                      Promoted=Yes;
                      Image=ItemTrackingLines;
                      PromotedCategory=Process;
                      OnAction=BEGIN
                                 OpenItemTrackingLines;
                               END;
                                }
      { 25      ;1   ;ActionGroup;
                      CaptionML=[ENU=Item Availability by;
                                 PTG=Disponibilidade Produto por];
                      ActionContainerType=NewDocumentItems;
                      Image=ItemAvailability }
      { 39      ;2   ;Action    ;
                      CaptionML=[ENU=Event;
                                 PTG=Ocorr�ncia];
                      Image=Event;
                      OnAction=BEGIN
                                 ItemAvailFormsMgt.ShowItemAvailFromAsmLine(Rec,ItemAvailFormsMgt.ByEvent);
                               END;
                                }
      { 28      ;2   ;Action    ;
                      CaptionML=[ENU=Period;
                                 PTG=Per�odo];
                      Image=Period;
                      OnAction=BEGIN
                                 ItemAvailFormsMgt.ShowItemAvailFromAsmLine(Rec,ItemAvailFormsMgt.ByPeriod);
                               END;
                                }
      { 32      ;2   ;Action    ;
                      CaptionML=[ENU=Variant;
                                 PTG=Variante];
                      Image=ItemVariant;
                      OnAction=BEGIN
                                 ItemAvailFormsMgt.ShowItemAvailFromAsmLine(Rec,ItemAvailFormsMgt.ByVariant);
                               END;
                                }
      { 29      ;2   ;Action    ;
                      AccessByPermission=TableData 14=R;
                      CaptionML=[ENU=Location;
                                 PTG=Localiza��o];
                      Image=Warehouse;
                      OnAction=BEGIN
                                 ItemAvailFormsMgt.ShowItemAvailFromAsmLine(Rec,ItemAvailFormsMgt.ByLocation);
                               END;
                                }
      { 43      ;2   ;Action    ;
                      CaptionML=[ENU=BOM Level;
                                 PTG=N�vel L.M.];
                      Image=BOMLevel;
                      OnAction=BEGIN
                                 ItemAvailFormsMgt.ShowItemAvailFromAsmLine(Rec,ItemAvailFormsMgt.ByBOM);
                               END;
                                }
      { 21      ;1   ;Action    ;
                      CaptionML=[ENU=Comments;
                                 PTG=Coment�rios];
                      RunObject=Page 907;
                      RunPageLink=Document Type=FIELD(Document Type),
                                  Document No.=FIELD(Document No.),
                                  Document Line No.=FIELD(Line No.);
                      Image=ViewComments }
      { 34      ;1   ;Action    ;
                      Name=ShowWarning;
                      CaptionML=[ENU=Show Warning;
                                 PTG=Mostrar Aviso];
                      Image=ShowWarning;
                      OnAction=BEGIN
                                 ShowAvailabilityWarning;
                               END;
                                }
    }
  }
  CONTROLS
  {
    { 1   ;0   ;Container ;
                ContainerType=ContentArea }

    { 2   ;1   ;Group     ;
                Name=Group;
                GroupType=Repeater }

    { 33  ;2   ;Field     ;
                DrillDown=Yes;
                ToolTipML=ENU=Specifies Yes if the assembly component is not available in the quantity and on the due date of the assembly order line.;
                BlankZero=Yes;
                SourceExpr="Avail. Warning";
                OnDrillDown=BEGIN
                              ShowAvailabilityWarning;
                            END;
                             }

    { 3   ;2   ;Field     ;
                ToolTipML=ENU=Specifies if the assembly order line is of type Item or Resource.;
                SourceExpr=Type }

    { 35  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the item or resource that is represented by the assembly order line.;
                SourceExpr="No." }

    { 6   ;2   ;Field     ;
                ToolTipML=ENU=Specifies the description of the assembly component.;
                SourceExpr=Description }

    { 5   ;2   ;Field     ;
                ToolTipML=ENU=Specifies the second description of the assembly component.;
                SourceExpr="Description 2";
                Visible=FALSE }

    { 4   ;2   ;Field     ;
                ToolTipML=ENU=Specifies the code of the item variant of the assembly component.;
                SourceExpr="Variant Code";
                Visible=FALSE }

    { 12  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the location from which you want to post consumption of the assembly component.;
                SourceExpr="Location Code";
                Visible=FALSE }

    { 8   ;2   ;Field     ;
                ToolTipML=ENU=Specifies the unit of measure in which the assembly component is consumed on the assembly order.;
                SourceExpr="Unit of Measure Code" }

    { 13  ;2   ;Field     ;
                ToolTipML=ENU=Specifies how many units of the assembly component are required to assemble one assembly item.;
                SourceExpr="Quantity per" }

    { 15  ;2   ;Field     ;
                ToolTipML=ENU=Specifies how many units of the assembly component are expected to be consumed.;
                SourceExpr=Quantity }

    { 20  ;2   ;Field     ;
                ToolTipML=ENU=Specifies how many units of the assembly component have been reserved for this assembly order line.;
                SourceExpr="Reserved Quantity" }

    { 38  ;2   ;Field     ;
                ToolTipML=ENU=Specifies how many units of the assembly component have been posted as consumed during the assembly.;
                SourceExpr="Consumed Quantity";
                Visible=FALSE }

    { 41  ;2   ;Field     ;
                ToolTipML=ENU=Specifies how many units of the assembly component have been moved or picked for the assembly order line.;
                SourceExpr="Qty. Picked";
                Visible=FALSE }

    { 42  ;2   ;Field     ;
                ToolTipML=ENU=Specifies how many units of the assembly component are currently on warehouse pick lines.;
                SourceExpr="Pick Qty.";
                Visible=FALSE }

    { 24  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the date when the assembly component must be available for consumption by the assembly order.;
                SourceExpr="Due Date";
                Visible=FALSE }

    { 30  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the lead-time offset that is defined for the assembly component on the assembly BOM.;
                SourceExpr="Lead-Time Offset";
                Visible=FALSE }

    { 9   ;2   ;Field     ;
                ToolTipML=ENU=Specifies the code of the shortcut dimension 1 value that the assembly order line is linked to.;
                SourceExpr="Shortcut Dimension 1 Code";
                Visible=FALSE }

    { 10  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the code of the shortcut dimension 2 value that the assembly order line is linked to.;
                SourceExpr="Shortcut Dimension 2 Code";
                Visible=FALSE }

    { 11  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the code of the bin where assembly components must be placed prior to assembly and from where they are posted as consumed.;
                SourceExpr="Bin Code";
                Visible=FALSE }

    { 17  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the inventory posting group to which the item on this assembly order line is posted.;
                SourceExpr="Inventory Posting Group";
                Visible=FALSE }

    { 18  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the unit cost of the assembly component.;
                SourceExpr="Unit Cost";
                Visible=FALSE }

    { 19  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the cost of the assembly order line.;
                SourceExpr="Cost Amount" }

    { 46  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the quantity per unit of measure of the component item on the assembly order line.;
                SourceExpr="Qty. per Unit of Measure";
                Visible=FALSE }

    { 22  ;2   ;Field     ;
                ToolTipML=ENU=Specifies how the cost of the resource on the assembly order line is allocated to the assembly item.;
                SourceExpr="Resource Usage Type" }

    { 7   ;2   ;Field     ;
                ToolTipML=ENU=Specifies the number of a particular item ledger entry that the assembly order line is applied to.;
                SourceExpr="Appl.-to Item Entry";
                Visible=FALSE }

    { 31  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the number of the item ledger entry that the assembly order line is applied from.;
                SourceExpr="Appl.-from Item Entry";
                Visible=FALSE }

  }
  CODE
  {
    VAR
      ItemAvailFormsMgt@1000 : Codeunit 353;

    LOCAL PROCEDURE GetCaption@1() : Text[250];
    VAR
      ObjTransln@1009 : Record 377;
      AsmHeader@1003 : Record 900;
      SourceTableName@1002 : Text[250];
      SourceFilter@1001 : Text[200];
      Description@1000 : Text[100];
    BEGIN
      Description := '';

      IF AsmHeader.GET("Document Type","Document No.") THEN BEGIN
        SourceTableName := ObjTransln.TranslateObject(ObjTransln."Object Type"::Table,27);
        SourceFilter := AsmHeader."Item No.";
        Description := AsmHeader.Description;
      END;
      EXIT(STRSUBSTNO('%1 %2 %3',SourceTableName,SourceFilter,Description));
    END;

    BEGIN
    END.
  }
}

