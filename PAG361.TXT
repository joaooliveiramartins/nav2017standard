OBJECT Page 361 Res. Availability Lines
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0;
  }
  PROPERTIES
  {
    Editable=No;
    CaptionML=[ENU=Lines;
               PTG=Linhas];
    SaveValues=Yes;
    InsertAllowed=No;
    DeleteAllowed=No;
    LinksAllowed=No;
    SourceTable=Table2000000007;
    PageType=ListPart;
    OnOpenPage=BEGIN
                 RESET;
               END;

    OnFindRecord=BEGIN
                   EXIT(PeriodFormMgt.FindDate(Which,Rec,PeriodType));
                 END;

    OnNextRecord=BEGIN
                   EXIT(PeriodFormMgt.NextDate(Steps,Rec,PeriodType));
                 END;

    OnAfterGetRecord=BEGIN
                       SetDateFilter;
                       Res.CALCFIELDS(Capacity,"Qty. on Order (Job)","Qty. Quoted (Job)","Qty. on Service Order","Qty. on Assembly Order");
                       CapacityAfterOrders := Res.Capacity - Res."Qty. on Order (Job)";
                       CapacityAfterQuotes := CapacityAfterOrders - Res."Qty. Quoted (Job)";
                       NetAvailability := CapacityAfterQuotes - Res."Qty. on Service Order" - Res."Qty. on Assembly Order";
                     END;

  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 2   ;2   ;Field     ;
                CaptionML=[ENU=Period Start;
                           PTG=In�cio Per�odo];
                SourceExpr="Period Start" }

    { 14  ;2   ;Field     ;
                CaptionML=[ENU=Period Name;
                           PTG=Nome Per�odo];
                SourceExpr="Period Name" }

    { 4   ;2   ;Field     ;
                Name=Capacity;
                CaptionML=[ENU=Capacity;
                           PTG=Capacidade];
                DecimalPlaces=0:5;
                SourceExpr=Res.Capacity }

    { 6   ;2   ;Field     ;
                CaptionML=[ENU=Qty. on Order (Job);
                           PTG=Qtd. em Encomenda (Projeto)];
                DecimalPlaces=0:5;
                SourceExpr=Res."Qty. on Order (Job)" }

    { 8   ;2   ;Field     ;
                CaptionML=[ENU=Availability After Orders;
                           PTG=Disponibilidade Ap�s Encom.];
                DecimalPlaces=0:5;
                SourceExpr=CapacityAfterOrders }

    { 10  ;2   ;Field     ;
                CaptionML=[ENU=Job Quotes Allocation;
                           PTG=Aloca��o Propostas Proj.];
                DecimalPlaces=0:5;
                SourceExpr=Res."Qty. Quoted (Job)" }

    { 12  ;2   ;Field     ;
                CaptionML=[ENU=Availability After Quotes;
                           PTG=Disponibilidade ap�s Proposta];
                DecimalPlaces=0:5;
                SourceExpr=CapacityAfterQuotes }

    { 16  ;2   ;Field     ;
                CaptionML=[ENU=Qty. on Service Order;
                           PTG=Qtd. em Ordem Servi�o];
                SourceExpr=Res."Qty. on Service Order" }

    { 3   ;2   ;Field     ;
                Name=QtyOnAssemblyOrder;
                CaptionML=[ENU=Qty. on Assembly Order;
                           PTG=Qtd. em Ordem Montagem];
                SourceExpr=Res."Qty. on Assembly Order" }

    { 18  ;2   ;Field     ;
                CaptionML=[ENU=Net Availability;
                           PTG=Disponibilidade L�quida];
                SourceExpr=NetAvailability;
                AutoFormatType=1 }

  }
  CODE
  {
    VAR
      Res@1006 : Record 156;
      PeriodFormMgt@1003 : Codeunit 359;
      CapacityAfterOrders@1002 : Decimal;
      CapacityAfterQuotes@1001 : Decimal;
      NetAvailability@1000 : Decimal;
      PeriodType@1004 : 'Day,Week,Month,Quarter,Year,Accounting Period';
      AmountType@1005 : 'Net Change,Balance at Date';

    PROCEDURE Set@1(VAR NewRes@1000 : Record 156;NewPeriodType@1001 : Integer;NewAmountType@1002 : 'Net Change,Balance at Date');
    BEGIN
      Res.COPY(NewRes);
      PeriodType := NewPeriodType;
      AmountType := NewAmountType;
      CurrPage.UPDATE(FALSE);
    END;

    LOCAL PROCEDURE SetDateFilter@4();
    BEGIN
      IF AmountType = AmountType::"Net Change" THEN
        Res.SETRANGE("Date Filter","Period Start","Period End")
      ELSE
        Res.SETRANGE("Date Filter",0D,"Period End");
    END;

    BEGIN
    END.
  }
}

