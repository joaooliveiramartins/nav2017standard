OBJECT Page 7315 Warehouse Movement
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Warehouse Movement;
               PTG=Movs. Armaz�m];
    SaveValues=Yes;
    InsertAllowed=No;
    SourceTable=Table5766;
    SourceTableView=WHERE(Type=FILTER(Movement));
    PageType=Document;
    RefreshOnActivate=Yes;
    OnOpenPage=BEGIN
                 ErrorIfUserIsNotWhseEmployee;
               END;

    OnFindRecord=BEGIN
                   EXIT(FindFirstAllowedRec(Which));
                 END;

    OnNextRecord=BEGIN
                   EXIT(FindNextAllowedRec(Steps));
                 END;

    OnAfterGetRecord=BEGIN
                       CurrentLocationCode := "Location Code";
                     END;

    OnDeleteRecord=BEGIN
                     CurrPage.UPDATE;
                   END;

    ActionList=ACTIONS
    {
      { 1900000003;0 ;ActionContainer;
                      ActionContainerType=RelatedInformation }
      { 100     ;1   ;ActionGroup;
                      CaptionML=[ENU=&Movement;
                                 PTG=&Movimento];
                      Image=CreateMovement }
      { 101     ;2   ;Action    ;
                      ShortCutKey=Shift+Ctrl+L;
                      CaptionML=[ENU=List;
                                 PTG=Lista];
                      Image=OpportunitiesList;
                      OnAction=BEGIN
                                 LookupActivityHeader(CurrentLocationCode,Rec);
                               END;
                                }
      { 25      ;2   ;Action    ;
                      CaptionML=[ENU=Co&mments;
                                 PTG=Co&ment�rios];
                      RunObject=Page 5776;
                      RunPageLink=Table Name=CONST(Whse. Activity Header),
                                  Type=FIELD(Type),
                                  No.=FIELD(No.);
                      Image=ViewComments }
      { 11      ;2   ;Action    ;
                      CaptionML=[ENU=Registered Movements;
                                 PTG=Movs. Registados];
                      RunObject=Page 5797;
                      RunPageView=SORTING(Whse. Activity No.);
                      RunPageLink=Type=FIELD(Type),
                                  Whse. Activity No.=FIELD(No.);
                      Image=RegisteredDocs }
      { 1900000004;0 ;ActionContainer;
                      ActionContainerType=ActionItems }
      { 9       ;1   ;ActionGroup;
                      CaptionML=[ENU=F&unctions;
                                 PTG=F&un��es];
                      Image=Action }
      { 35      ;2   ;Action    ;
                      CaptionML=[ENU=&Autofill Qty. to Handle;
                                 PTG=Preencher &Aut. Qtd. a Processar];
                      Image=AutofillQtyToHandle;
                      OnAction=BEGIN
                                 AutofillQtyToHandle;
                               END;
                                }
      { 20      ;2   ;Action    ;
                      CaptionML=[ENU=&Delete Qty. to Handle;
                                 PTG=Eliminar Qt&d. a Processar];
                      Image=DeleteQtyToHandle;
                      OnAction=BEGIN
                                 DeleteQtyToHandle;
                               END;
                                }
      { 4       ;2   ;Separator  }
      { 16      ;1   ;ActionGroup;
                      CaptionML=[ENU=&Registering;
                                 PTG=&Registo];
                      Image=PostOrder }
      { 17      ;2   ;Action    ;
                      ShortCutKey=F9;
                      CaptionML=[ENU=&Register Movement;
                                 PTG=&Registar Movimento];
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=RegisterPutAway;
                      PromotedCategory=Process;
                      OnAction=BEGIN
                                 RegisterActivityYesNo;
                               END;
                                }
      { 6       ;1   ;Action    ;
                      Ellipsis=Yes;
                      CaptionML=[ENU=&Print;
                                 PTG=&Imprimir];
                      Promoted=Yes;
                      Image=Print;
                      PromotedCategory=Process;
                      OnAction=BEGIN
                                 WhseActPrint.PrintMovementHeader(Rec);
                               END;
                                }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                CaptionML=[ENU=General;
                           PTG=Geral] }

    { 2   ;2   ;Field     ;
                CaptionML=[ENU=No.;
                           PTG=N�];
                ToolTipML=[ENU=Specifies the number of the warehouse header.;
                           PTG=""];
                SourceExpr="No.";
                Editable=FALSE;
                OnAssistEdit=BEGIN
                               IF AssistEdit(xRec) THEN
                                 CurrPage.UPDATE;
                             END;
                              }

    { 28  ;2   ;Field     ;
                Lookup=No;
                CaptionML=[ENU=Location Code;
                           PTG=C�d. Localiza��o];
                SourceExpr=CurrentLocationCode;
                Editable=FALSE }

    { 18  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies that the intermediate Take and Place lines will not show as put-away, pick, or movement lines, when the quantity in the larger unit of measure is being put-away, picked or moved completely.;
                           PTG=""];
                SourceExpr="Breakbulk Filter";
                OnValidate=BEGIN
                             BreakbulkFilterOnAfterValidate;
                           END;
                            }

    { 8   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the ID of the user who is responsible for the document.;
                           PTG=""];
                SourceExpr="Assigned User ID" }

    { 15  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the date when the user was assigned the activity.;
                           PTG=""];
                SourceExpr="Assignment Date";
                Editable=FALSE }

    { 23  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the time when the user was assigned the activity.;
                           PTG=""];
                SourceExpr="Assignment Time";
                Editable=FALSE }

    { 12  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the method by which the lines are sorted on the warehouse header, such as Item or Document.;
                           PTG=""];
                OptionCaptionML=[ENU=" ,Item,,Bin Code,Due Date,,Bin Ranking,Action Type";
                                 PTG=" ,Produto,,C�d. Posi��o,Data Vencimento,,Classifica��o Posi��o,Tipo A��o"];
                SourceExpr="Sorting Method";
                OnValidate=BEGIN
                             SortingMethodOnAfterValidate;
                           END;
                            }

    { 97  ;1   ;Part      ;
                Name=WhseMovLines;
                SubPageView=SORTING(Activity Type,No.,Sorting Sequence No.)
                            WHERE(Breakbulk=CONST(No));
                SubPageLink=Activity Type=FIELD(Type),
                            No.=FIELD(No.);
                PagePartID=Page7316 }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1901796907;1;Part   ;
                SubPageLink=No.=FIELD(Item No.);
                PagePartID=Page9109;
                ProviderID=97;
                Visible=TRUE;
                PartType=Page }

    { 5   ;1   ;Part      ;
                SubPageLink=Item No.=FIELD(Item No.),
                            Variant Code=FIELD(Variant Code),
                            Location Code=FIELD(Location Code);
                PagePartID=Page9126;
                ProviderID=97;
                Visible=false;
                PartType=Page }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                Visible=TRUE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {
    VAR
      WhseActPrint@1000 : Codeunit 5776;
      CurrentLocationCode@1001 : Code[10];

    LOCAL PROCEDURE AutofillQtyToHandle@1();
    BEGIN
      CurrPage.WhseMovLines.PAGE.AutofillQtyToHandle;
    END;

    LOCAL PROCEDURE DeleteQtyToHandle@2();
    BEGIN
      CurrPage.WhseMovLines.PAGE.DeleteQtyToHandle;
    END;

    LOCAL PROCEDURE RegisterActivityYesNo@3();
    BEGIN
      CurrPage.WhseMovLines.PAGE.RegisterActivityYesNo;
    END;

    LOCAL PROCEDURE SortingMethodOnAfterValidate@19063061();
    BEGIN
      CurrPage.UPDATE;
    END;

    LOCAL PROCEDURE BreakbulkFilterOnAfterValidate@19055352();
    BEGIN
      CurrPage.UPDATE;
    END;

    BEGIN
    END.
  }
}

