OBJECT Table 44 Sales Comment Line
{
  OBJECT-PROPERTIES
  {
    Date=07/09/12;
    Time=12:00:00;
    Version List=NAVW17.00;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Sales Comment Line;
               PTG=Linha Coment�rio Venda];
    LookupPageID=Page69;
    DrillDownPageID=Page69;
  }
  FIELDS
  {
    { 1   ;   ;Document Type       ;Option        ;CaptionML=[ENU=Document Type;
                                                              PTG=Tipo Documento];
                                                   OptionCaptionML=[ENU=Quote,Order,Invoice,Credit Memo,Blanket Order,Return Order,Shipment,Posted Invoice,Posted Credit Memo,Posted Return Receipt;
                                                                    PTG=Proposta,Encomenda,Fatura,Nota Cr�dito,Enc. Aberta,Devolu��o,Guia Remessa Venda,Fatura Reg.,Nota Cr�dito Reg.,Envio Dev. Reg.];
                                                   OptionString=Quote,Order,Invoice,Credit Memo,Blanket Order,Return Order,Shipment,Posted Invoice,Posted Credit Memo,Posted Return Receipt }
    { 2   ;   ;No.                 ;Code20        ;CaptionML=[ENU=No.;
                                                              PTG=N�] }
    { 3   ;   ;Line No.            ;Integer       ;CaptionML=[ENU=Line No.;
                                                              PTG=N� Linha] }
    { 4   ;   ;Date                ;Date          ;CaptionML=[ENU=Date;
                                                              PTG=Data] }
    { 5   ;   ;Code                ;Code10        ;CaptionML=[ENU=Code;
                                                              PTG=C�digo] }
    { 6   ;   ;Comment             ;Text80        ;CaptionML=[ENU=Comment;
                                                              PTG=Coment�rio] }
    { 7   ;   ;Document Line No.   ;Integer       ;CaptionML=[ENU=Document Line No.;
                                                              PTG=N� Linha Documento] }
  }
  KEYS
  {
    {    ;Document Type,No.,Document Line No.,Line No.;
                                                   Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    PROCEDURE SetUpNewLine@1();
    VAR
      SalesCommentLine@1000 : Record 44;
    BEGIN
      SalesCommentLine.SETRANGE("Document Type","Document Type");
      SalesCommentLine.SETRANGE("No.","No.");
      SalesCommentLine.SETRANGE("Document Line No.","Document Line No.");
      SalesCommentLine.SETRANGE(Date,WORKDATE);
      IF NOT SalesCommentLine.FINDFIRST THEN
        Date := WORKDATE;
    END;

    BEGIN
    END.
  }
}

