OBJECT Page 9551 Document Service Config
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Microsoft SharePoint Connection Setup;
               PTG=Configura��o Liga��o Microsoft Sharepoint];
    InsertAllowed=No;
    SourceTable=Table2000000114;
    DelayedInsert=Yes;
    PageType=Card;
    OnInit=BEGIN
             DynamicEditable := FALSE;
           END;

    OnOpenPage=BEGIN
                 IF NOT FINDFIRST THEN BEGIN
                   INIT;
                   "Service ID" := 'Service 1';
                   INSERT(FALSE);
                 END;
               END;

    OnAfterGetCurrRecord=BEGIN
                           DynamicEditable := CurrPage.EDITABLE;
                         END;

    ActionList=ACTIONS
    {
      { 10      ;    ;ActionContainer;
                      Name=Container;
                      CaptionML=[ENU=Container;
                                 PTG=Recipiente];
                      ActionContainerType=ActionItems }
      { 11      ;1   ;Action    ;
                      Name=Test Connection;
                      CaptionML=[ENU=Test Connection;
                                 PTG=Testar Liga��o];
                      ToolTipML=[ENU=Test the configuration settings against the online document storage service.;
                                 PTG=Testar as configura��es do servi�o de armazenamento de documentos online.];
                      Promoted=Yes;
                      Image=ValidateEmailLoggingSetup;
                      PromotedCategory=Process;
                      OnAction=VAR
                                 DocumentServiceManagement@1000 : Codeunit 9510;
                               BEGIN
                                 // Save record to make sure the credentials are reset.
                                 MODIFY;
                                 DocumentServiceManagement.TestConnection;
                                 MESSAGE(ValidateSuccessMsg);
                               END;
                                }
      { 9       ;1   ;Action    ;
                      Name=Set Password;
                      CaptionML=[ENU=Set Password;
                                 PTG=Definir Palavra-Passe];
                      ToolTipML=[ENU=Set the password for the current User Name;
                                 PTG=Definir palavra-passe para o utilizador atual];
                      Promoted=Yes;
                      Enabled=DynamicEditable;
                      Image=EncryptionKeys;
                      PromotedCategory=Process;
                      OnAction=VAR
                                 DocumentServiceAccPwd@1000 : Page 9552;
                               BEGIN
                                 IF DocumentServiceAccPwd.RUNMODAL = ACTION::OK THEN BEGIN
                                   IF CONFIRM(ChangePwdQst) THEN
                                     Password := DocumentServiceAccPwd.GetData;
                                 END;
                               END;
                                }
    }
  }
  CONTROLS
  {
    { 1   ;0   ;Container ;
                ContainerType=ContentArea }

    { 2   ;1   ;Group     ;
                Name=General;
                CaptionML=[ENU=General;
                           PTG=Geral];
                GroupType=Group }

    { 3   ;2   ;Field     ;
                CaptionML=[ENU=Service ID;
                           PTG=ID Servi�o];
                ToolTipML=[ENU=Specifies a unique code for the service that you use for document storage and usage.;
                           PTG=""];
                SourceExpr="Service ID" }

    { 4   ;2   ;Field     ;
                CaptionML=[ENU=Description;
                           PTG=Descri��o];
                ToolTipML=[ENU=Specifies a description for the document service.;
                           PTG=""];
                SourceExpr=Description }

    { 5   ;2   ;Field     ;
                CaptionML=[ENU=Location;
                           PTG=Localiza��o];
                ToolTipML=[ENU=Specifies the URI for where your documents are stored, such as your site on SharePoint Online.;
                           PTG=""];
                SourceExpr=Location }

    { 7   ;2   ;Field     ;
                CaptionML=[ENU=Folder;
                           PTG=Pasta];
                ToolTipML=[ENU=Specifies the folder in the document repository for this document service that you want documents to be stored in.;
                           PTG=""];
                SourceExpr=Folder }

    { 12  ;1   ;Group     ;
                Name=Shared documents;
                CaptionML=[ENU=Shared Documents;
                           PTG=Documentos Partilhados];
                GroupType=Group }

    { 6   ;2   ;Field     ;
                CaptionML=[ENU=Document Repository;
                           PTG=Reposit�rio Documento];
                ToolTipML=[ENU=Specifies the location where your document service provider stores your documents, if you want to store documents in a shared document repository.;
                           PTG=""];
                SourceExpr="Document Repository" }

    { 8   ;2   ;Field     ;
                CaptionML=[ENU=User Name;
                           PTG=Nome Utilizador];
                ToolTipML=[ENU=Specifies the account that Microsoft Dynamics NAV Server must use to log on to the document service, if you want to use a shared document repository.;
                           PTG=""];
                SourceExpr="User Name" }

  }
  CODE
  {
    VAR
      ChangePwdQst@1002 : TextConst 'ENU=Are you sure that you want to change your password?;PTG=Confirma que pretende alterar a palavra-passe?';
      DynamicEditable@1001 : Boolean;
      ValidateSuccessMsg@1004 : TextConst 'ENU=The connection settings validated correctly, and the current configuration can connect to the document storage service.;PTG=As configura��es de conex�o foram validadas corretamente, e a configura��o atual pode ligar ao documento de armazenamento de  servi�o.';

    BEGIN
    END.
  }
}

