OBJECT Page 7154 Item Analy. View Budg. Entries
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0;
  }
  PROPERTIES
  {
    Editable=No;
    CaptionML=[ENU=Analysis View Budget Entries;
               PTG=Movs. Or�. Dados An�lise];
    SourceTable=Table7156;
    DataCaptionFields=Analysis View Code;
    PageType=List;
    OnAfterGetCurrRecord=BEGIN
                           IF "Analysis View Code" <> xRec."Analysis View Code" THEN;
                         END;

  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 4   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the name of the budget that the analysis view budget entries are linked to.;
                           PTG=""];
                SourceExpr="Budget Name" }

    { 6   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the code of the location to which the analysis view budget entry was posted.;
                           PTG=""];
                SourceExpr="Location Code" }

    { 8   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the number of the item that the analysis view budget entry is linked to.;
                           PTG=""];
                SourceExpr="Item No." }

    { 10  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies which dimension value you have selected for the analysis view dimension that you defined as Dimension 1 on the analysis view card.;
                           PTG=""];
                SourceExpr="Dimension 1 Value Code" }

    { 12  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies which dimension value you have selected for the analysis view dimension that you defined as Dimension 2 on the analysis view card.;
                           PTG=""];
                SourceExpr="Dimension 2 Value Code" }

    { 14  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies which dimension value you have selected for the analysis view dimension that you defined as Dimension 1 on the analysis view card.;
                           PTG=""];
                SourceExpr="Dimension 3 Value Code" }

    { 18  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the date on which the item budget entries in an analysis view budget entry were posted.;
                           PTG=""];
                SourceExpr="Posting Date" }

    { 20  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the item budget entry sales amount included in an analysis view budget entry.;
                           PTG=""];
                SourceExpr="Sales Amount";
                OnDrillDown=BEGIN
                              DrillDown;
                            END;
                             }

    { 2   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the item budget entry cost amount included in an analysis view budget entry.;
                           PTG=""];
                SourceExpr="Cost Amount";
                OnDrillDown=BEGIN
                              DrillDown;
                            END;
                             }

    { 16  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the item budget entry quantity included in an analysis view budget entry.;
                           PTG=""];
                SourceExpr=Quantity;
                OnDrillDown=BEGIN
                              DrillDown;
                            END;
                             }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {

    LOCAL PROCEDURE DrillDown@1();
    VAR
      ItemBudgetEntry@1000 : Record 7134;
    BEGIN
      ItemBudgetEntry.SETRANGE("Entry No.","Entry No.");
      PAGE.RUNMODAL(0,ItemBudgetEntry);
    END;

    BEGIN
    END.
  }
}

