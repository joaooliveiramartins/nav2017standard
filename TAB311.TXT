OBJECT Table 311 Sales & Receivables Setup
{
  OBJECT-PROPERTIES
  {
    Date=19/06/17;
    Time=12:41:13;
    Modified=Yes;
    Version List=NAVW110.0,NAVPTSS83.00;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Sales & Receivables Setup;
               PTG=Configura��o Vendas e Cobran�as];
    LookupPageID=Page459;
    DrillDownPageID=Page459;
  }
  FIELDS
  {
    { 1   ;   ;Primary Key         ;Code10        ;CaptionML=[ENU=Primary Key;
                                                              PTG=Chave Prim�ria] }
    { 2   ;   ;Discount Posting    ;Option        ;CaptionML=[ENU=Discount Posting;
                                                              PTG=Registo Desconto];
                                                   OptionCaptionML=[ENU=No Discounts,Invoice Discounts,Line Discounts,All Discounts;
                                                                    PTG=Nenhum,Desconto Fatura,Desconto Linha,Todos Descontos];
                                                   OptionString=No Discounts,Invoice Discounts,Line Discounts,All Discounts }
    { 4   ;   ;Credit Warnings     ;Option        ;CaptionML=[ENU=Credit Warnings;
                                                              PTG=Avisos Cr�dito];
                                                   OptionCaptionML=[ENU=Both Warnings,Credit Limit,Overdue Balance,No Warning;
                                                                    PTG=Ambos,Limite Cr�dito,D�vida Vencida,Nenhum];
                                                   OptionString=Both Warnings,Credit Limit,Overdue Balance,No Warning }
    { 5   ;   ;Stockout Warning    ;Boolean       ;InitValue=Yes;
                                                   CaptionML=[ENU=Stockout Warning;
                                                              PTG=Aviso Exist�ncias em Falta] }
    { 6   ;   ;Shipment on Invoice ;Boolean       ;OnValidate=VAR
                                                                "//--soft-local--//"@9000002 : Integer;
                                                                NoSeriesLine@111000 : Record 309;
                                                              BEGIN
                                                                //soft,sn
                                                                IF NOT "Shipment on Invoice" THEN BEGIN
                                                                  NoSeriesLine.SETRANGE("Series Code","Posted Shipment Nos.");
                                                                  IF NOT NoSeriesLine.ISEMPTY THEN BEGIN
                                                                    NoSeriesLine.FINDSET;
                                                                    REPEAT
                                                                      IF (NoSeriesLine."Last No. Used" <> NoSeriesLine."Last No. Posted") THEN
                                                                        ERROR(Text31022890,NoSeriesLine."Last No. Used",NoSeriesLine."Series Code");
                                                                    UNTIL NoSeriesLine.NEXT = 0;
                                                                  END;
                                                                END
                                                                //soft,sn
                                                              END;

                                                   CaptionML=[ENU=Shipment on Invoice;
                                                              PTG=Guia Remessa ao Faturar] }
    { 7   ;   ;Invoice Rounding    ;Boolean       ;CaptionML=[ENU=Invoice Rounding;
                                                              PTG=Arredondamento Fatura] }
    { 8   ;   ;Ext. Doc. No. Mandatory;Boolean    ;CaptionML=[ENU=Ext. Doc. No. Mandatory;
                                                              PTG=N� Doc. Externo Obrigat�rio] }
    { 9   ;   ;Customer Nos.       ;Code10        ;TableRelation="No. Series";
                                                   CaptionML=[ENU=Customer Nos.;
                                                              PTG=N� S�ries Cliente] }
    { 10  ;   ;Quote Nos.          ;Code10        ;TableRelation="No. Series";
                                                   CaptionML=[ENU=Quote Nos.;
                                                              PTG=N� S�ries Proposta] }
    { 11  ;   ;Order Nos.          ;Code10        ;TableRelation="No. Series";
                                                   AccessByPermission=TableData 110=R;
                                                   CaptionML=[ENU=Order Nos.;
                                                              PTG=N� S�ries Encomendas] }
    { 12  ;   ;Invoice Nos.        ;Code10        ;TableRelation="No. Series";
                                                   CaptionML=[ENU=Invoice Nos.;
                                                              PTG=N� S�ries Fatura] }
    { 13  ;   ;Posted Invoice Nos. ;Code10        ;TableRelation="No. Series";
                                                   CaptionML=[ENU=Posted Invoice Nos.;
                                                              PTG=N� S�ries Fatura Reg.] }
    { 14  ;   ;Credit Memo Nos.    ;Code10        ;TableRelation="No. Series";
                                                   CaptionML=[ENU=Credit Memo Nos.;
                                                              PTG=N� S�ries Nota Cr�dito] }
    { 15  ;   ;Posted Credit Memo Nos.;Code10     ;TableRelation="No. Series";
                                                   CaptionML=[ENU=Posted Credit Memo Nos.;
                                                              PTG=N� S�ries Nota Cr�dito Reg.] }
    { 16  ;   ;Posted Shipment Nos.;Code10        ;TableRelation="No. Series";
                                                   AccessByPermission=TableData 110=R;
                                                   CaptionML=[ENU=Posted Shipment Nos.;
                                                              PTG=N� S�ries Guia Remessa Reg.] }
    { 17  ;   ;Reminder Nos.       ;Code10        ;TableRelation="No. Series";
                                                   CaptionML=[ENU=Reminder Nos.;
                                                              PTG=N� S�ries Carta Aviso] }
    { 18  ;   ;Issued Reminder Nos.;Code10        ;TableRelation="No. Series";
                                                   CaptionML=[ENU=Issued Reminder Nos.;
                                                              PTG=N� S�ries Carta Aviso Emitida] }
    { 19  ;   ;Fin. Chrg. Memo Nos.;Code10        ;TableRelation="No. Series";
                                                   CaptionML=[ENU=Fin. Chrg. Memo Nos.;
                                                              PTG=N� S�ries Nota Juros] }
    { 20  ;   ;Issued Fin. Chrg. M. Nos.;Code10   ;TableRelation="No. Series";
                                                   CaptionML=[ENU=Issued Fin. Chrg. M. Nos.;
                                                              PTG=N� S�ries Nota Juro Emitida] }
    { 21  ;   ;Posted Prepmt. Inv. Nos.;Code10    ;TableRelation="No. Series";
                                                   CaptionML=[ENU=Posted Prepmt. Inv. Nos.;
                                                              PTG=N� S�ries Fatura Pr�-Pagm. Reg.] }
    { 22  ;   ;Posted Prepmt. Cr. Memo Nos.;Code10;TableRelation="No. Series";
                                                   CaptionML=[ENU=Posted Prepmt. Cr. Memo Nos.;
                                                              PTG=N� S�ries Nota Cr�dito Pr�-Pagm. Reg.] }
    { 23  ;   ;Blanket Order Nos.  ;Code10        ;TableRelation="No. Series";
                                                   AccessByPermission=TableData 110=R;
                                                   CaptionML=[ENU=Blanket Order Nos.;
                                                              PTG=N� S�ries Encomenda Aberta] }
    { 24  ;   ;Calc. Inv. Discount ;Boolean       ;CaptionML=[ENU=Calc. Inv. Discount;
                                                              PTG=Calcula Desconto Fatura] }
    { 25  ;   ;Appln. between Currencies;Option   ;AccessByPermission=TableData 4=R;
                                                   CaptionML=[ENU=Appln. between Currencies;
                                                              PTG=Liquida��o entre Divisas];
                                                   OptionCaptionML=[ENU=None,EMU,All;
                                                                    PTG=Nenhuma,UME,Todas];
                                                   OptionString=None,EMU,All }
    { 26  ;   ;Copy Comments Blanket to Order;Boolean;
                                                   InitValue=Yes;
                                                   AccessByPermission=TableData 110=R;
                                                   CaptionML=[ENU=Copy Comments Blanket to Order;
                                                              PTG=Copia Coment. Aberta p/ Encomenda] }
    { 27  ;   ;Copy Comments Order to Invoice;Boolean;
                                                   InitValue=Yes;
                                                   AccessByPermission=TableData 110=R;
                                                   CaptionML=[ENU=Copy Comments Order to Invoice;
                                                              PTG=Copia Coment. Encomenda p/ Fatura] }
    { 28  ;   ;Copy Comments Order to Shpt.;Boolean;
                                                   InitValue=Yes;
                                                   AccessByPermission=TableData 110=R;
                                                   CaptionML=[ENU=Copy Comments Order to Shpt.;
                                                              PTG=Copia Coment. Enc. p/ Guia Remessa] }
    { 29  ;   ;Allow VAT Difference;Boolean       ;CaptionML=[ENU=Allow VAT Difference;
                                                              PTG=Permite Dif. IVA] }
    { 30  ;   ;Calc. Inv. Disc. per VAT ID;Boolean;CaptionML=[ENU=Calc. Inv. Disc. per VAT ID;
                                                              PTG=Calc. Desconto Fatura por ID IVA] }
    { 31  ;   ;Logo Position on Documents;Option  ;CaptionML=[ENU=Logo Position on Documents;
                                                              PTG=Posi��o do Log�tipo nos Documentos];
                                                   OptionCaptionML=[ENU=No Logo,Left,Center,Right;
                                                                    PTG=Sem Log�tipo,Esquerda,Centro,Direita];
                                                   OptionString=No Logo,Left,Center,Right }
    { 32  ;   ;Check Prepmt. when Posting;Boolean ;CaptionML=[ENU=Check Prepmt. when Posting;
                                                              PTG=Verificar Pr�-Pagamento ao Registar] }
    { 35  ;   ;Default Posting Date;Option        ;CaptionML=[ENU=Default Posting Date;
                                                              PTG=Data Registo Padr�o];
                                                   OptionCaptionML=[ENU=Work Date,No Date;
                                                                    PTG=Data Trabalho,Sem Data];
                                                   OptionString=Work Date,No Date }
    { 36  ;   ;Default Quantity to Ship;Option    ;AccessByPermission=TableData 110=R;
                                                   CaptionML=[ENU=Default Quantity to Ship;
                                                              PTG=Quantidade Padr�o a Enviar];
                                                   OptionCaptionML=[ENU=Remainder,Blank;
                                                                    PTG=Restante,Vazio];
                                                   OptionString=Remainder,Blank }
    { 37  ;   ;Archive Quotes and Orders;Boolean  ;CaptionML=[ENU=Archive Quotes and Orders;
                                                              PTG=Arquivar Propostas e Encomendas] }
    { 38  ;   ;Post with Job Queue ;Boolean       ;CaptionML=[ENU=Post with Job Queue;
                                                              PTG=Registar com Fila de Tarefas] }
    { 39  ;   ;Job Queue Category Code;Code10     ;TableRelation="Job Queue Category";
                                                   CaptionML=[ENU=Job Queue Category Code;
                                                              PTG=C�d. Categoria Fila Tarefas] }
    { 40  ;   ;Job Queue Priority for Post;Integer;InitValue=1000;
                                                   OnValidate=BEGIN
                                                                IF "Job Queue Priority for Post" < 0 THEN
                                                                  ERROR(Text001);
                                                              END;

                                                   CaptionML=[ENU=Job Queue Priority for Post;
                                                              PTG=Prioridade para Registo de Fila Tarefas];
                                                   MinValue=0 }
    { 41  ;   ;Post & Print with Job Queue;Boolean;CaptionML=[ENU=Post & Print with Job Queue;
                                                              PTG=Registar & Imprimir com Fila Tarefas] }
    { 42  ;   ;Job Q. Prio. for Post & Print;Integer;
                                                   InitValue=1000;
                                                   OnValidate=BEGIN
                                                                IF "Job Queue Priority for Post" < 0 THEN
                                                                  ERROR(Text001);
                                                              END;

                                                   CaptionML=[ENU=Job Q. Prio. for Post & Print;
                                                              PTG=Prioridade Fila Tarefas para Registar & Imprimir];
                                                   MinValue=0 }
    { 43  ;   ;Notify On Success   ;Boolean       ;CaptionML=[ENU=Notify On Success;
                                                              PTG=Notifica��o de Sucesso] }
    { 44  ;   ;VAT Bus. Posting Gr. (Price);Code10;TableRelation="VAT Business Posting Group";
                                                   CaptionML=[ENU=VAT Bus. Posting Gr. (Price);
                                                              PTG=Gr. Registo IVA Neg�cio (Pre�o)] }
    { 45  ;   ;Direct Debit Mandate Nos.;Code10   ;TableRelation="No. Series";
                                                   CaptionML=[ENU=Direct Debit Mandate Nos.;
                                                              PTG=N� S�ries Autoriza��o D�bito Direto] }
    { 46  ;   ;Allow Document Deletion Before;Date;CaptionML=[ENU=Allow Document Deletion Before;
                                                              PTG=Permitir Elimina��o Documentos Antes] }
    { 50  ;   ;Default Item Quantity;Boolean      ;CaptionML=[ENU=Default Item Quantity;
                                                              PTG=Quantidade Produto Pr�-Definida] }
    { 51  ;   ;Create Item from Description;Boolean;
                                                   CaptionML=[ENU=Create Item from Description;
                                                              PTG=Criar Produto pela Descri��o] }
    { 5800;   ;Posted Return Receipt Nos.;Code10  ;TableRelation="No. Series";
                                                   AccessByPermission=TableData 6660=R;
                                                   CaptionML=[ENU=Posted Return Receipt Nos.;
                                                              PTG=N�s S�ries Rece��o Devolu��o Reg.] }
    { 5801;   ;Copy Cmts Ret.Ord. to Ret.Rcpt;Boolean;
                                                   InitValue=Yes;
                                                   AccessByPermission=TableData 6660=R;
                                                   CaptionML=[ENU=Copy Cmts Ret.Ord. to Ret.Rcpt;
                                                              PTG=Copia Coment. Dev. p/ Rec. Devolu��o] }
    { 5802;   ;Copy Cmts Ret.Ord. to Cr. Memo;Boolean;
                                                   InitValue=Yes;
                                                   AccessByPermission=TableData 6660=R;
                                                   CaptionML=[ENU=Copy Cmts Ret.Ord. to Cr. Memo;
                                                              PTG=Copia Coment. Dev. p/ Nota Cr�dito] }
    { 6600;   ;Return Order Nos.   ;Code10        ;TableRelation="No. Series";
                                                   AccessByPermission=TableData 6660=R;
                                                   CaptionML=[ENU=Return Order Nos.;
                                                              PTG=N�s S�ries Devolu��es] }
    { 6601;   ;Return Receipt on Credit Memo;Boolean;
                                                   AccessByPermission=TableData 6660=R;
                                                   CaptionML=[ENU=Return Receipt on Credit Memo;
                                                              PTG=Rece��o Dev. em Nota Cr�dito] }
    { 6602;   ;Exact Cost Reversing Mandatory;Boolean;
                                                   CaptionML=[ENU=Exact Cost Reversing Mandatory;
                                                              PTG=Custo Exato Revers�o Obrigat�rio] }
    { 7101;   ;Customer Group Dimension Code;Code20;
                                                   TableRelation=Dimension;
                                                   CaptionML=[ENU=Customer Group Dimension Code;
                                                              PTG=C�d. Dimens�o Grupo Cliente] }
    { 7102;   ;Salesperson Dimension Code;Code20  ;TableRelation=Dimension;
                                                   CaptionML=[ENU=Salesperson Dimension Code;
                                                              PTG=C�d. Dimens�o Vendedor] }
    { 7103;   ;Freight G/L Acc. No.;Code20        ;TableRelation="G/L Account";
                                                   CaptionML=[ENU=Freight G/L Acc. No.;
                                                              PTG=N� Conta C/G Transporte] }
    { 50000;  ;Receipt Nos.OLD     ;Code10        ;TableRelation="No. Series".Code WHERE (Code=FIELD(Receipt Nos.));
                                                   CaptionML=[ENU=Receipt Number Series;
                                                              PTG=N� s�rie recibo];
                                                   Description=SGG }
    { 50001;  ;Debit Note Nos.     ;Code10        ;TableRelation="No. Series".Code WHERE (Code=FIELD(Debit Note Nos.));
                                                   CaptionML=[ENU=Debit Note Nos.;
                                                              PTG=N� s�rie nota d�bito];
                                                   Description=SGG }
    { 50002;  ;Posted Debit Note Nos.;Code10      ;TableRelation="No. Series".Code WHERE (Code=FIELD(Posted Debit Note Nos.));
                                                   CaptionML=[ENU=Posted Debit Note Nos.;
                                                              PTG=N� s�rie nota d�bito registada];
                                                   Description=SGG }
    { 70001;  ;General Text1       ;Text30        ;Description=SGG }
    { 70002;  ;General Text2       ;Text30        ;Description=SGG }
    { 70003;  ;General Text3       ;Text50        ;Description=SGG }
    { 70004;  ;General Text4       ;Text50        ;Description=SGG }
    { 70005;  ;General Text5       ;Text250       ;Description=SGG }
    { 70011;  ;General Date1       ;Date          ;Description=SGG }
    { 70012;  ;General Date2       ;Date          ;Description=SGG }
    { 70013;  ;General Date3       ;Date          ;Description=SGG }
    { 70021;  ;General Int1        ;Integer       ;Description=SGG }
    { 70022;  ;General Int2        ;Integer       ;Description=SGG }
    { 70023;  ;General Int3        ;Integer       ;Description=SGG }
    { 70031;  ;General Bool1       ;Boolean       ;Description=tRUMPF - PERMITE EDIT VENDA A NOME }
    { 70032;  ;General Bool2       ;Boolean       ;Description=SGG }
    { 70033;  ;General Bool3       ;Boolean       ;Description=SGG }
    { 70034;  ;General Bool4       ;Boolean       ;Description=SGG }
    { 70035;  ;General Bool5       ;Boolean       ;Description=SGG }
    { 70036;  ;General Bool6       ;Boolean       ;Description=SGG }
    { 70041;  ;General Dec1        ;Decimal       ;Description=SGG }
    { 70042;  ;General Dec2        ;Decimal       ;Description=SGG }
    { 70051;  ;General Code1       ;Code20        ;Description=SGG }
    { 70052;  ;General Code2       ;Code20        ;Description=SGG }
    { 70053;  ;General Code3       ;Code20        ;Description=SGG }
    { 31022890;;Post Invoice Discount;Boolean     ;CaptionML=[ENU=Post Invoice Discount;
                                                              PTG=Registo Desconto Fatura] }
    { 31022891;;Post Line Discount ;Boolean       ;CaptionML=[ENU=Post Line Discount;
                                                              PTG=Registo Desconto Linha] }
    { 31022892;;Post Payment Discount;Boolean     ;OnValidate=BEGIN
                                                                IF "Post Payment Discount" THEN BEGIN
                                                                  GLSetup.GET;
                                                                  GLSetup.TESTFIELD("Payment Discount Type",GLSetup."Payment Discount Type"::"Calc. Pmt. Disc. on Lines");
                                                                END;
                                                              END;

                                                   CaptionML=[ENU=Post Payment Discount;
                                                              PTG=Registo Desconto P.P.] }
    { 31022893;;Posted Reminder Nos.;Code10       ;TableRelation="No. Series";
                                                   OnValidate=BEGIN
                                                                IF "Posted Reminder Nos." <> '' THEN BEGIN
                                                                  NoSeries.GET("Posted Reminder Nos.");
                                                                  NoSeries.TESTFIELD(NoSeries."Date Order");
                                                                END;
                                                              END;

                                                   CaptionML=[ENU=Posted Reminder Nos.;
                                                              PTG=N� S�ries Carta Aviso Enviada] }
    { 31022894;;Posted Fin. Chrg. M. Nos.;Code10  ;TableRelation="No. Series";
                                                   OnValidate=BEGIN
                                                                IF "Posted Fin. Chrg. M. Nos." <> '' THEN BEGIN
                                                                  NoSeries.GET("Posted Fin. Chrg. M. Nos.");
                                                                  NoSeries.TESTFIELD(NoSeries."Date Order");
                                                                END;
                                                              END;

                                                   CaptionML=[ENU=Posted Fin. Chrg. M. Nos.;
                                                              PTG=N� S�ries Nota Juros Enviada] }
    { 31022895;;Receipt Nos.       ;Code10        ;TableRelation="No. Series";
                                                   CaptionML=[ENU=Receipt Nos.;
                                                              PTG=N� S�ries Recibo] }
    { 31022896;;Debit Memo Nos.    ;Code10        ;TableRelation="No. Series";
                                                   CaptionML=[ENU=Debit Memo Nos.;
                                                              PTG=N� S�ries Notas D�bito];
                                                   Description=soft }
    { 31022897;;Posted Debit Memo Nos.;Code10     ;TableRelation="No. Series";
                                                   CaptionML=[ENU=Posted Debit Memo Nos.;
                                                              PTG=N� S�ries Notas D�bito Reg.];
                                                   Description=soft }
  }
  KEYS
  {
    {    ;Primary Key                             ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {
    VAR
      Text001@1000 : TextConst 'ENU=Job Queue Priority must be zero or positive.;PTG=A Prioridade da Fila de Tarefas deve ser zero ou positiva.';
      "//--soft-global--//"@9000000 : Integer;
      NoSeriesLine@111000 : Record 309;
      NoSeries@1110002 : Record 308;
      GLSetup@1110003 : Record 98;
      "//--soft-text--//"@9000001 : TextConst;
      Text31022890@111001 : TextConst 'ENU=Posted document no. %1 has been allocated but not posted yet. A new number cannot be allocated from the number series %2 unless document no. %1 is posted or deleted.;PTG=Documento registado N� %1 foi atribu�do, mas ainda n�o foi registado. N�o � possivel atribuir um novo N� s�rie %2 a n�o ser que o documento N� %1 seja registado ou eliminado.';

    PROCEDURE GetLegalStatement@11() : Text;
    BEGIN
      EXIT('');
    END;

    PROCEDURE JobQueueActive@1() : Boolean;
    BEGIN
      GET;
      EXIT("Post with Job Queue" OR "Post & Print with Job Queue");
    END;

    BEGIN
    END.
  }
}

