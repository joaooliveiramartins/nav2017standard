OBJECT Table 5846 Inventory Report Entry
{
  OBJECT-PROPERTIES
  {
    Date=09/09/14;
    Time=12:00:00;
    Version List=NAVW18.00;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Inventory Report Entry;
               PTG=Mov. Mapa Invent�rio];
    LookupPageID=Page5846;
    DrillDownPageID=Page5846;
  }
  FIELDS
  {
    { 1   ;   ;Entry No.           ;Integer       ;CaptionML=[ENU=Entry No.;
                                                              PTG=N� Mov.] }
    { 3   ;   ;No.                 ;Code20        ;CaptionML=[ENU=No.;
                                                              PTG=N�] }
    { 5   ;   ;Location Filter     ;Code10        ;FieldClass=FlowFilter;
                                                   CaptionML=[ENU=Location Filter;
                                                              PTG=Filtro Localiza��o] }
    { 6   ;   ;Posting Date Filter ;Date          ;FieldClass=FlowFilter;
                                                   CaptionML=[ENU=Posting Date Filter;
                                                              PTG=Data filtro registo] }
    { 8   ;   ;Inventory           ;Decimal       ;CaptionML=[ENU=Inventory;
                                                              PTG=Invent�rio] }
    { 9   ;   ;Inventory (Interim) ;Decimal       ;CaptionML=[ENU=Inventory (Interim);
                                                              PTG=Invent�rio (Provis.)] }
    { 10  ;   ;WIP Inventory       ;Decimal       ;CaptionML=[ENU=WIP Inventory;
                                                              PTG=Invent�rio Trab. Curso] }
    { 11  ;   ;Direct Cost Applied Actual;Decimal ;CaptionML=[ENU=Direct Cost Applied Actual;
                                                              PTG=Custo Direto Aplicado Atual] }
    { 12  ;   ;Overhead Applied Actual;Decimal    ;CaptionML=[ENU=Overhead Applied Actual;
                                                              PTG=Custos Gerais Aplicados Atuais] }
    { 13  ;   ;Purchase Variance   ;Decimal       ;CaptionML=[ENU=Purchase Variance;
                                                              PTG=Varia��o Compra] }
    { 14  ;   ;Inventory Adjmt.    ;Decimal       ;CaptionML=[ENU=Inventory Adjmt.;
                                                              PTG=Ajuste Invent�rio] }
    { 16  ;   ;Invt. Accrual (Interim);Decimal    ;CaptionML=[ENU=Invt. Accrual (Interim);
                                                              PTG=Ati. Invent�rio (Provis.)] }
    { 17  ;   ;COGS                ;Decimal       ;CaptionML=[ENU=COGS;
                                                              PTG=CMV] }
    { 18  ;   ;COGS (Interim)      ;Decimal       ;CaptionML=[ENU=COGS (Interim);
                                                              PTG=CMV (Provis.)] }
    { 19  ;   ;Material Variance   ;Decimal       ;CaptionML=[ENU=Material Variance;
                                                              PTG=Varia��o de Material] }
    { 20  ;   ;Capacity Variance   ;Decimal       ;CaptionML=[ENU=Capacity Variance;
                                                              PTG=Varia��o Capacidade] }
    { 21  ;   ;Subcontracted Variance;Decimal     ;AccessByPermission=TableData 99000758=R;
                                                   CaptionML=[ENU=Subcontracted Variance;
                                                              PTG=Varia��o Subcontrata��o] }
    { 22  ;   ;Capacity Overhead Variance;Decimal ;CaptionML=[ENU=Capacity Overhead Variance;
                                                              PTG=Varia��o Capacidade Geral] }
    { 23  ;   ;Mfg. Overhead Variance;Decimal     ;AccessByPermission=TableData 99000758=R;
                                                   CaptionML=[ENU=Mfg. Overhead Variance;
                                                              PTG=Varia��o Custos Gerais Prod.] }
    { 28  ;   ;Total               ;Decimal       ;CaptionML=[ENU=Total;
                                                              PTG=Total] }
    { 29  ;   ;G/L Total           ;Decimal       ;CaptionML=[ENU=G/L Total;
                                                              PTG=Total C/G] }
    { 30  ;   ;Difference          ;Decimal       ;CaptionML=[ENU=Difference;
                                                              PTG=Diferen�a] }
    { 31  ;   ;Type                ;Option        ;CaptionML=[ENU=Type;
                                                              PTG=Tipo];
                                                   OptionCaptionML=[ENU=" ,G/L Account,Item";
                                                                    PTG=" ,Conta C/G,Produto"];
                                                   OptionString=[ ,G/L Account,Item] }
    { 32  ;   ;Direct Cost Applied WIP;Decimal    ;CaptionML=[ENU=Direct Cost Applied WIP;
                                                              PTG=Custo Dir. Aplic. Trab. Curso] }
    { 33  ;   ;Overhead Applied WIP;Decimal       ;AccessByPermission=TableData 99000758=R;
                                                   CaptionML=[ENU=Overhead Applied WIP;
                                                              PTG=Custos Gerais Apls. Trab Curso] }
    { 35  ;   ;Inventory To WIP    ;Decimal       ;AccessByPermission=TableData 5405=R;
                                                   CaptionML=[ENU=Inventory To WIP;
                                                              PTG=Invent�rio para Trab. Curso] }
    { 36  ;   ;WIP To Interim      ;Decimal       ;AccessByPermission=TableData 5405=R;
                                                   CaptionML=[ENU=WIP To Interim;
                                                              PTG=Trab. Curso Para Provis�rio] }
    { 37  ;   ;Direct Cost Applied ;Decimal       ;CaptionML=[ENU=Direct Cost Applied;
                                                              PTG=Custo Direto Aplicado] }
    { 38  ;   ;Overhead Applied    ;Decimal       ;CaptionML=[ENU=Overhead Applied;
                                                              PTG=Custos Gerais Liquidados] }
    { 39  ;   ;Description         ;Text50        ;CaptionML=[ENU=Description;
                                                              PTG=Descri��o] }
    { 40  ;   ;Warning             ;Text50        ;CaptionML=[ENU=Warning;
                                                              PTG=Aviso] }
    { 61  ;   ;Cost is Posted to G/L Warning;Boolean;
                                                   CaptionML=[ENU=Cost is Posted to G/L Warning;
                                                              PTG=Aviso Custo Registado na C/G] }
    { 62  ;   ;Expected Cost Posting Warning;Boolean;
                                                   CaptionML=[ENU=Expected Cost Posting Warning;
                                                              PTG=Aviso Registo Custo Esperado] }
    { 63  ;   ;Compression Warning ;Boolean       ;CaptionML=[ENU=Compression Warning;
                                                              PTG=Aviso Compress�o] }
    { 64  ;   ;Posting Group Warning;Boolean      ;CaptionML=[ENU=Posting Group Warning;
                                                              PTG=Aviso Grupo Registo] }
    { 65  ;   ;Direct Postings Warning;Boolean    ;CaptionML=[ENU=Direct Postings Warning;
                                                              PTG=Aviso Entrada Direta] }
    { 66  ;   ;Posting Date Warning;Boolean       ;CaptionML=[ENU=Posting Date Warning;
                                                              PTG=Aviso Data Registo] }
    { 67  ;   ;Closing Period Overlap Warning;Boolean;
                                                   CaptionML=[ENU=Closing Period Overlap Warning;
                                                              PTG=Aviso Sobrep. Per�odo Fecho] }
    { 68  ;   ;Similar Accounts Warning;Boolean   ;CaptionML=[ENU=Similar Accounts Warning;
                                                              PTG=Aviso Contas Semelhantes] }
    { 69  ;   ;Deleted G/L Accounts Warning;Boolean;
                                                   CaptionML=[ENU=Deleted G/L Accounts Warning;
                                                              PTG=Aviso Contas C/G Eliminadas] }
  }
  KEYS
  {
    {    ;Type,Entry No.                          ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    END.
  }
}

