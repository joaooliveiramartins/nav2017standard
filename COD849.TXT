OBJECT Codeunit 849 Cash Flow Account - Indent
{
  OBJECT-PROPERTIES
  {
    Date=15/09/15;
    Time=12:00:00;
    Version List=NAVW19.00;
  }
  PROPERTIES
  {
    OnRun=BEGIN
            IF NOT
               CONFIRM(
                 Text1000 +
                 Text1003,TRUE)
            THEN
              EXIT;

            Indentation;
          END;

  }
  CODE
  {
    VAR
      Text1000@1000 : TextConst 'ENU=This function updates the indentation of all the cash flow accounts in the chart of cash flow accounts. All accounts between a Begin-Total and the matching End-Total are indented one level. The Totaling for each End-total is also updated.\\;PTG=Esta fun��o atualiza a indenta��o de todas a contas de cash flow no plano de contas de cash flow. Todas as contas entre um Total-In�cio e Total-Fim s�o indentadas um n�vel. O Somat�rio de cada Total-Fim tamb�m � atualizado.\\';
      Text1003@1003 : TextConst 'ENU=Do you want to indent the chart of accounts?;PTG=Deseja indentar o plano de contas?';
      Text1004@1004 : TextConst 'ENU=Indenting the Chart of Accounts #1##########;PTG=Indentando o Plano de Contas    #1##########';
      Text1005@1005 : TextConst 'ENU=End-Total %1 is missing a matching Begin-Total.;PTG=Falta um Total-Inicial correspondente ao Total-Final %1.';
      CFAccount@1006 : Record 841;
      Window@1007 : Dialog;
      AccNo@1008 : ARRAY [10] OF Code[20];
      i@1009 : Integer;

    LOCAL PROCEDURE Indentation@1000();
    BEGIN
      Window.OPEN(Text1004);

      WITH CFAccount DO
        IF FIND('-') THEN
          REPEAT
            Window.UPDATE(1,"No.");

            IF "Account Type" = "Account Type"::"End-Total" THEN BEGIN
              IF i < 1 THEN
                ERROR(
                  Text1005,
                  "No.");
              Totaling := AccNo[i] + '..' + "No.";
              i := i - 1;
            END;

            Indentation := i;
            MODIFY;

            IF "Account Type" = "Account Type"::"Begin-Total" THEN BEGIN
              i := i + 1;
              AccNo[i] := "No.";
            END;
          UNTIL NEXT = 0;

      Window.CLOSE;
    END;

    BEGIN
    END.
  }
}

