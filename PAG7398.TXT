OBJECT Page 7398 Internal Movement Subform
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Lines;
               PTG=Linhas];
    MultipleNewLines=Yes;
    LinksAllowed=No;
    SourceTable=Table7347;
    DelayedInsert=Yes;
    PageType=ListPart;
    OnNewRecord=BEGIN
                  SetUpNewLine(xRec);
                END;

    ActionList=ACTIONS
    {
      { 1900000004;0 ;ActionContainer;
                      ActionContainerType=ActionItems }
      { 1907935204;1 ;ActionGroup;
                      CaptionML=[ENU=&Line;
                                 PTG=&Linha];
                      Image=Line }
      { 1901742304;2 ;Action    ;
                      CaptionML=[ENU=Bin Contents List;
                                 PTG=Lista Conte�do Posi��o];
                      Image=BinContent;
                      OnAction=BEGIN
                                 ShowBinContent;
                               END;
                                }
      { 1903866904;2 ;Action    ;
                      ShortCutKey=Shift+Ctrl+I;
                      CaptionML=[ENU=Item &Tracking Lines;
                                 PTG=Linhas Ras&treio Produto];
                      Image=ItemTrackingLines;
                      OnAction=BEGIN
                                 OpenItemTrackingLinesForm;
                               END;
                                }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 10  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the number of the item that is available to move from the bin.;
                           PTG=""];
                SourceExpr="Item No.";
                OnValidate=BEGIN
                             ItemNoOnAfterValidate;
                           END;
                            }

    { 20  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the variant of the item on the line.;
                           PTG=""];
                SourceExpr="Variant Code";
                Visible=FALSE }

    { 22  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the same as the field with the same name in the Whse. Internal Put-away Line table.;
                           PTG=""];
                SourceExpr=Description }

    { 24  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the second description of the item.;
                           PTG=""];
                SourceExpr="Description 2";
                Visible=FALSE }

    { 2   ;2   ;Field     ;
                ToolTipML=[ENU=This field contains the location code that applies to the internal movement line.;
                           PTG=""];
                SourceExpr="Location Code" }

    { 4   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the shelf number that is recorded on the item card or on the stockkeeping unit card of the item that is being moved.;
                           PTG=""];
                SourceExpr="Shelf No.";
                Visible=FALSE }

    { 6   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the code of the bin that the items on the internal movement are picked from.;
                           PTG=""];
                SourceExpr="From Bin Code";
                OnValidate=BEGIN
                             FromBinCodeOnAfterValidate;
                           END;
                            }

    { 8   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the bin where you want items on this internal movement to be placed when they are picked.;
                           PTG=""];
                SourceExpr="To Bin Code" }

    { 12  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the quantity of items to be moved. The quantity must be lower than or equal to the bin content.;
                           PTG=""];
                SourceExpr=Quantity }

    { 14  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the quantity of units to be moved.;
                           PTG=""];
                SourceExpr="Qty. (Base)";
                Visible=FALSE }

    { 28  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the same as the field with the same name in the Whse. Internal Put-away Line table.;
                           PTG=""];
                SourceExpr="Due Date";
                OnValidate=BEGIN
                             DueDateOnAfterValidate;
                           END;
                            }

    { 16  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the same as the field with the same name in the Whse. Internal Put-away Line table.;
                           PTG=""];
                SourceExpr="Unit of Measure Code" }

    { 18  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the same as the field with the same name in the Whse. Internal Put-away Line table.;
                           PTG=""];
                SourceExpr="Qty. per Unit of Measure";
                Visible=FALSE }

  }
  CODE
  {
    VAR
      SortMethod@1000 : ' ,Item,Shelf/Bin No.,Due Date';

    LOCAL PROCEDURE GetActualSortMethod@1() : Integer;
    VAR
      InternalMovementHeader@1000 : Record 7346;
    BEGIN
      IF InternalMovementHeader.GET("No.") THEN
        EXIT(InternalMovementHeader."Sorting Method");
    END;

    LOCAL PROCEDURE ShowBinContent@2();
    VAR
      BinContent@1000 : Record 7302;
    BEGIN
      BinContent.ShowBinContents("Location Code","Item No.","Variant Code","From Bin Code");
    END;

    LOCAL PROCEDURE OpenItemTrackingLinesForm@4();
    BEGIN
      OpenItemTrackingLines;
    END;

    LOCAL PROCEDURE ItemNoOnAfterValidate@19061248();
    BEGIN
      IF GetActualSortMethod = SortMethod::Item THEN
        CurrPage.UPDATE;
    END;

    LOCAL PROCEDURE FromBinCodeOnAfterValidate@19011280();
    BEGIN
      IF GetActualSortMethod = SortMethod::"Shelf/Bin No." THEN
        CurrPage.UPDATE;
    END;

    LOCAL PROCEDURE DueDateOnAfterValidate@19011747();
    BEGIN
      IF GetActualSortMethod = SortMethod::"Due Date" THEN
        CurrPage.UPDATE;
    END;

    BEGIN
    END.
  }
}

