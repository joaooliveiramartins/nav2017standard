OBJECT Page 31022968 BG/PO Comment List
{
  OBJECT-PROPERTIES
  {
    Date=01/02/15;
    Time=13:00:00;
    Version List=NAVPTSS81.00;
  }
  PROPERTIES
  {
    Editable=No;
    CaptionML=[ENU=BG/PO Comment List;
               PTG=Lista Coment rio Rem./Ordem Pagto.];
    SourceTable=Table31022941;
    DataCaptionExpr=Caption;
    PageType=List;
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 2   ;2   ;Field     ;
                SourceExpr="BG/PO No." }

    { 4   ;2   ;Field     ;
                SourceExpr=Date }

    { 6   ;2   ;Field     ;
                SourceExpr=Comment }

  }
  CODE
  {

    BEGIN
    END.
  }
}

