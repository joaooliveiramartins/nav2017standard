OBJECT Table 304 Issued Fin. Charge Memo Header
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0,NAVPTSS81.00;
  }
  PROPERTIES
  {
    DataCaptionFields=No.,Name;
    OnDelete=BEGIN
               TESTFIELD("No. Printed");
               LOCKTABLE;
               FinChrgMemoIssue.DeleteIssuedFinChrgLines(Rec);

               FinChrgCommentLine.SETRANGE(Type,FinChrgCommentLine.Type::"Issued Finance Charge Memo");
               FinChrgCommentLine.SETRANGE("No.","No.");
               FinChrgCommentLine.DELETEALL;
             END;

    CaptionML=[ENU=Issued Fin. Charge Memo Header;
               PTG=Cab. Nota Juros Emitida];
    LookupPageID=Page452;
    DrillDownPageID=Page452;
  }
  FIELDS
  {
    { 1   ;   ;No.                 ;Code20        ;CaptionML=[ENU=No.;
                                                              PTG=N�];
                                                   NotBlank=Yes }
    { 2   ;   ;Customer No.        ;Code20        ;TableRelation=Customer;
                                                   CaptionML=[ENU=Customer No.;
                                                              PTG=N� Cliente] }
    { 3   ;   ;Name                ;Text50        ;CaptionML=[ENU=Name;
                                                              PTG=Nome] }
    { 4   ;   ;Name 2              ;Text50        ;CaptionML=[ENU=Name 2;
                                                              PTG=Nome 2] }
    { 5   ;   ;Address             ;Text50        ;CaptionML=[ENU=Address;
                                                              PTG=Endere�o] }
    { 6   ;   ;Address 2           ;Text50        ;CaptionML=[ENU=Address 2;
                                                              PTG=Endere�o 2] }
    { 7   ;   ;Post Code           ;Code20        ;TableRelation="Post Code";
                                                   ValidateTableRelation=No;
                                                   TestTableRelation=No;
                                                   CaptionML=[ENU=Post Code;
                                                              PTG=C�d. Postal] }
    { 8   ;   ;City                ;Text30        ;TableRelation="Post Code".City;
                                                   ValidateTableRelation=No;
                                                   TestTableRelation=No;
                                                   CaptionML=[ENU=City;
                                                              PTG=Cidade] }
    { 9   ;   ;County              ;Text30        ;CaptionML=[ENU=County;
                                                              PTG=Distrito] }
    { 10  ;   ;Country/Region Code ;Code10        ;TableRelation=Country/Region;
                                                   CaptionML=[ENU=Country/Region Code;
                                                              PTG=C�digo Pa�s/Regi�o] }
    { 11  ;   ;Language Code       ;Code10        ;TableRelation=Language;
                                                   CaptionML=[ENU=Language Code;
                                                              PTG=C�d. Idioma] }
    { 12  ;   ;Currency Code       ;Code10        ;TableRelation=Currency;
                                                   CaptionML=[ENU=Currency Code;
                                                              PTG=C�d. Divisa] }
    { 13  ;   ;Contact             ;Text50        ;CaptionML=[ENU=Contact;
                                                              PTG=Contacto] }
    { 14  ;   ;Your Reference      ;Text35        ;CaptionML=[ENU=Your Reference;
                                                              PTG=Sua Refer�ncia] }
    { 15  ;   ;Shortcut Dimension 1 Code;Code20   ;TableRelation="Dimension Value".Code WHERE (Global Dimension No.=CONST(1));
                                                   CaptionML=[ENU=Shortcut Dimension 1 Code;
                                                              PTG=C�d. Atalho Dimens�o 1];
                                                   CaptionClass='1,2,1' }
    { 16  ;   ;Shortcut Dimension 2 Code;Code20   ;TableRelation="Dimension Value".Code WHERE (Global Dimension No.=CONST(2));
                                                   CaptionML=[ENU=Shortcut Dimension 2 Code;
                                                              PTG=C�d. Atalho Dimens�o 2];
                                                   CaptionClass='1,2,2' }
    { 17  ;   ;Customer Posting Group;Code10      ;TableRelation="Customer Posting Group";
                                                   CaptionML=[ENU=Customer Posting Group;
                                                              PTG=Gr. Contabil�stico Cliente] }
    { 18  ;   ;Gen. Bus. Posting Group;Code10     ;TableRelation="Gen. Business Posting Group";
                                                   CaptionML=[ENU=Gen. Bus. Posting Group;
                                                              PTG=Gr. Contabil�stico Neg�cio] }
    { 19  ;   ;VAT Registration No.;Text20        ;CaptionML=[ENU=VAT Registration No.;
                                                              PTG=N� Contribuinte] }
    { 20  ;   ;Reason Code         ;Code10        ;TableRelation="Reason Code";
                                                   CaptionML=[ENU=Reason Code;
                                                              PTG=C�d. Auditoria] }
    { 21  ;   ;Posting Date        ;Date          ;CaptionML=[ENU=Posting Date;
                                                              PTG=Data Registo] }
    { 22  ;   ;Document Date       ;Date          ;CaptionML=[ENU=Document Date;
                                                              PTG=Data Documento] }
    { 23  ;   ;Due Date            ;Date          ;CaptionML=[ENU=Due Date;
                                                              PTG=Data Vencimento] }
    { 25  ;   ;Fin. Charge Terms Code;Code10      ;TableRelation="Finance Charge Terms";
                                                   CaptionML=[ENU=Fin. Charge Terms Code;
                                                              PTG=C�d. Termos Nota Juros] }
    { 26  ;   ;Interest Posted     ;Boolean       ;CaptionML=[ENU=Interest Posted;
                                                              PTG=Juro Registado] }
    { 27  ;   ;Additional Fee Posted;Boolean      ;CaptionML=[ENU=Additional Fee Posted;
                                                              PTG=Encargo Adicional Registado] }
    { 29  ;   ;Posting Description ;Text50        ;CaptionML=[ENU=Posting Description;
                                                              PTG=Texto Registo] }
    { 30  ;   ;Comment             ;Boolean       ;FieldClass=FlowField;
                                                   CalcFormula=Exist("Fin. Charge Comment Line" WHERE (Type=CONST(Issued Finance Charge Memo),
                                                                                                       No.=FIELD(No.)));
                                                   CaptionML=[ENU=Comment;
                                                              PTG=Coment�rio];
                                                   Editable=No }
    { 31  ;   ;Remaining Amount    ;Decimal       ;FieldClass=FlowField;
                                                   CalcFormula=Sum("Issued Fin. Charge Memo Line"."Remaining Amount" WHERE (Finance Charge Memo No.=FIELD(No.)));
                                                   CaptionML=[ENU=Remaining Amount;
                                                              PTG=Valor Pendente];
                                                   Editable=No;
                                                   AutoFormatType=1;
                                                   AutoFormatExpr="Currency Code" }
    { 32  ;   ;Interest Amount     ;Decimal       ;FieldClass=FlowField;
                                                   CalcFormula=Sum("Issued Fin. Charge Memo Line".Amount WHERE (Finance Charge Memo No.=FIELD(No.),
                                                                                                                Type=CONST(Customer Ledger Entry)));
                                                   CaptionML=[ENU=Interest Amount;
                                                              PTG=Valor Juros];
                                                   Editable=No;
                                                   AutoFormatType=1;
                                                   AutoFormatExpr="Currency Code" }
    { 33  ;   ;Additional Fee      ;Decimal       ;FieldClass=FlowField;
                                                   CalcFormula=Sum("Issued Fin. Charge Memo Line".Amount WHERE (Finance Charge Memo No.=FIELD(No.),
                                                                                                                Type=CONST(G/L Account)));
                                                   CaptionML=[ENU=Additional Fee;
                                                              PTG=Encargo Adicional];
                                                   Editable=No;
                                                   AutoFormatType=1;
                                                   AutoFormatExpr="Currency Code" }
    { 34  ;   ;VAT Amount          ;Decimal       ;FieldClass=FlowField;
                                                   CalcFormula=Sum("Issued Fin. Charge Memo Line"."VAT Amount" WHERE (Finance Charge Memo No.=FIELD(No.)));
                                                   CaptionML=[ENU=VAT Amount;
                                                              PTG=Valor IVA];
                                                   Editable=No;
                                                   AutoFormatType=1;
                                                   AutoFormatExpr="Currency Code" }
    { 35  ;   ;No. Printed         ;Integer       ;CaptionML=[ENU=No. Printed;
                                                              PTG=N� Impress�es] }
    { 36  ;   ;User ID             ;Code50        ;TableRelation=User."User Name";
                                                   OnLookup=VAR
                                                              UserMgt@1000 : Codeunit 418;
                                                            BEGIN
                                                              UserMgt.LookupUserID("User ID");
                                                            END;

                                                   TestTableRelation=No;
                                                   CaptionML=[ENU=User ID;
                                                              PTG=ID Utilizador] }
    { 37  ;   ;No. Series          ;Code10        ;TableRelation="No. Series";
                                                   CaptionML=[ENU=No. Series;
                                                              PTG=N� S�ries];
                                                   Editable=No }
    { 38  ;   ;Pre-Assigned No. Series;Code10     ;TableRelation="No. Series";
                                                   CaptionML=[ENU=Pre-Assigned No. Series;
                                                              PTG=N� S�ries Pr�-Atribu�do] }
    { 39  ;   ;Pre-Assigned No.    ;Code20        ;CaptionML=[ENU=Pre-Assigned No.;
                                                              PTG=N� Pr�-Atribu�do] }
    { 40  ;   ;Source Code         ;Code10        ;TableRelation="Source Code";
                                                   CaptionML=[ENU=Source Code;
                                                              PTG=C�d. Origem] }
    { 41  ;   ;Tax Area Code       ;Code20        ;TableRelation="Tax Area";
                                                   CaptionML=[ENU=Tax Area Code;
                                                              PTG=C�d. �rea Imposto] }
    { 42  ;   ;Tax Liable          ;Boolean       ;CaptionML=[ENU=Tax Liable;
                                                              PTG=Sujeito a Imposto] }
    { 43  ;   ;VAT Bus. Posting Group;Code10      ;TableRelation="VAT Business Posting Group";
                                                   CaptionML=[ENU=VAT Bus. Posting Group;
                                                              PTG=Gr. Registo IVA Neg�cio] }
    { 480 ;   ;Dimension Set ID    ;Integer       ;TableRelation="Dimension Set Entry";
                                                   OnLookup=BEGIN
                                                              ShowDimensions;
                                                            END;

                                                   CaptionML=[ENU=Dimension Set ID;
                                                              PTG=ID Combina��o Dimens�o];
                                                   Editable=No }
    { 31022893;;Signed             ;Boolean       ;CaptionML=[ENU=Signed;
                                                              PTG=Assinado];
                                                   Editable=No }
    { 31022894;;Creation Date      ;Date          ;CaptionML=[ENU=Creation Date;
                                                              PTG=Data Cria��o];
                                                   Editable=No }
    { 31022895;;Creation Time      ;Time          ;CaptionML=[ENU=Creation Time;
                                                              PTG=Hora Cria��o];
                                                   Editable=No }
    { 31022896;;Hash               ;Text172       ;CaptionML=[ENU=Hash;
                                                              PTG=Hash] }
    { 31022897;;Private Key Version;Text40        ;CaptionML=[ENU=Private Key Version;
                                                              PTG=Vers�o Chave Privada] }
    { 31022922;;Hash Doc. Type     ;Code10        ;CaptionML=[ENU=Hash Doc. Type;
                                                              PTG=Hash Tipo Doc.];
                                                   Editable=No }
    { 31022923;;Hash Doc. No.      ;Code20        ;CaptionML=[ENU=Hash Doc. No.;
                                                              PTG=Hash N� Doc.];
                                                   Editable=No }
    { 31022924;;Hash No. Series    ;Code10        ;CaptionML=[ENU=Hash No. Series;
                                                              PTG=Hash N�s S�ries];
                                                   Editable=No }
    { 31022925;;Hash Amount Including VAT;Decimal ;CaptionML=[ENU=Hash Amount Including VAT;
                                                              PTG=Hash Valor Incluindo IVA];
                                                   Editable=No }
    { 31022926;;Hash Last Hash Used;Text172       ;CaptionML=[ENU=Hash Last Hash Used;
                                                              PTG=Hash �ltimo Hash Utilizado];
                                                   Editable=No }
  }
  KEYS
  {
    {    ;No.                                     ;Clustered=Yes }
    {    ;Customer No.,Posting Date                }
  }
  FIELDGROUPS
  {
    { 1   ;DropDown            ;No.,Customer No.,Name,Posting Date       }
  }
  CODE
  {
    VAR
      FinChrgCommentLine@1001 : Record 306;
      FinChrgMemoIssue@1002 : Codeunit 395;
      DimMgt@1003 : Codeunit 408;
      FinanceChargeTxt@1004 : TextConst 'ENU=Issued Finance Charge Memo;PTG=Nota Juro Emitida';

    PROCEDURE PrintRecords@1(ShowRequestForm@1000 : Boolean;SendAsEmail@1002 : Boolean;HideDialog@1003 : Boolean);
    VAR
      DummyReportSelections@1001 : Record 77;
      DocumentSendingProfile@1004 : Record 60;
    BEGIN
      IF SendAsEmail THEN
        DocumentSendingProfile.TrySendToEMail(
          DummyReportSelections.Usage::"Fin.Charge",Rec,FIELDNO("No."),FinanceChargeTxt,FIELDNO("Customer No."),NOT HideDialog)
      ELSE
        DocumentSendingProfile.TrySendToPrinter(
          DummyReportSelections.Usage::"Fin.Charge",Rec,"Customer No.",ShowRequestForm)
    END;

    PROCEDURE Navigate@2();
    VAR
      NavigateForm@1000 : Page 344;
    BEGIN
      NavigateForm.SetDoc("Posting Date","No.");
      NavigateForm.RUN;
    END;

    PROCEDURE IncrNoPrinted@3();
    BEGIN
      FinChrgMemoIssue.IncrNoPrinted(Rec);
    END;

    PROCEDURE ShowDimensions@4();
    BEGIN
      DimMgt.ShowDimensionSet("Dimension Set ID",STRSUBSTNO('%1 %2',TABLECAPTION,"No."));
    END;

    BEGIN
    END.
  }
}

