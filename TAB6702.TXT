OBJECT Table 6702 Booking Sync
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0;
  }
  PROPERTIES
  {
    Permissions=TableData 1261=rimd;
    CaptionML=[ENU=Booking Sync;
               PTG=Sinc. Reserva];
  }
  FIELDS
  {
    { 1   ;   ;Primary Key         ;Code10        ;CaptionML=[ENU=Primary Key;
                                                              PTG=Chave Prim�ria] }
    { 2   ;   ;Booking Mailbox Address;Text80     ;CaptionML=[ENU=Booking Mailbox Address;
                                                              PTG=Endere�o Caixa de Correio Reserva] }
    { 3   ;   ;Booking Mailbox Name;Text250       ;CaptionML=[ENU=Booking Mailbox Name;
                                                              PTG=Nome Caixa de Correio Reserva] }
    { 4   ;   ;User ID             ;Code50        ;TableRelation=User."User Name";
                                                   ValidateTableRelation=No;
                                                   TestTableRelation=No;
                                                   CaptionML=[ENU=User ID;
                                                              PTG=ID Utilizador];
                                                   NotBlank=Yes }
    { 5   ;   ;Last Customer Sync  ;DateTime      ;CaptionML=[ENU=Last Customer Sync;
                                                              PTG=Sinc. �ltimo Cliente];
                                                   Editable=Yes }
    { 6   ;   ;Last Service Sync   ;DateTime      ;CaptionML=[ENU=Last Service Sync;
                                                              PTG=Sinc. �ltimo Servi�o] }
    { 7   ;   ;Enabled             ;Boolean       ;CaptionML=[ENU=Enabled;
                                                              PTG=Ativo] }
    { 8   ;   ;Sync Customers      ;Boolean       ;CaptionML=[ENU=Sync Customers;
                                                              PTG=Sinc. Clientes] }
    { 9   ;   ;Customer Filter     ;BLOB          ;CaptionML=[ENU=Customer Filter;
                                                              PTG=Filtro Clientes] }
    { 10  ;   ;Customer Template Code;Code10      ;TableRelation="Customer Template".Code;
                                                   CaptionML=[ENU=Customer Template Code;
                                                              PTG=C�d. Modelo Cliente] }
    { 12  ;   ;Sync Services       ;Boolean       ;CaptionML=[ENU=Sync Services;
                                                              PTG=Sinc. Servi�os] }
    { 13  ;   ;Item Filter         ;BLOB          ;CaptionML=[ENU=Item Filter;
                                                              PTG=Filtro Produto] }
    { 14  ;   ;Item Template Code  ;Code10        ;TableRelation="Config. Template Header".Code WHERE (Table ID=FILTER(27));
                                                   CaptionML=[ENU=Item Template Code;
                                                              PTG=C�d. Modelo Produto] }
  }
  KEYS
  {
    {    ;Primary Key                             ;Clustered=Yes }
    {    ;Booking Mailbox Address                  }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    PROCEDURE GetCustomerFilter@1() FilterText : Text;
    VAR
      ReadStream@1000 : InStream;
    BEGIN
      CALCFIELDS("Customer Filter");
      "Customer Filter".CREATEINSTREAM(ReadStream);
      ReadStream.READTEXT(FilterText);
    END;

    PROCEDURE GetItemFilter@2() FilterText : Text;
    VAR
      ReadStream@1000 : InStream;
    BEGIN
      CALCFIELDS("Item Filter");
      "Item Filter".CREATEINSTREAM(ReadStream);
      ReadStream.READTEXT(FilterText);
    END;

    PROCEDURE SaveCustomerFilter@6(FilterText@1000 : Text);
    VAR
      WriteStream@1001 : OutStream;
    BEGIN
      CLEAR("Customer Filter");
      "Customer Filter".CREATEOUTSTREAM(WriteStream);
      WriteStream.WRITETEXT(FilterText);
      CLEAR("Last Customer Sync");
      MODIFY;
    END;

    PROCEDURE SaveItemFilter@7(FilterText@1000 : Text);
    VAR
      WriteStream@1001 : OutStream;
    BEGIN
      CLEAR("Item Filter");
      "Item Filter".CREATEOUTSTREAM(WriteStream);
      WriteStream.WRITETEXT(FilterText);
      CLEAR("Last Service Sync");
      MODIFY;
    END;

    PROCEDURE IsSetup@3() : Boolean;
    BEGIN
      EXIT(GET AND (FORMAT("Last Customer Sync") <> '') AND (FORMAT("Last Service Sync") <> ''));
    END;

    BEGIN
    END.
  }
}

