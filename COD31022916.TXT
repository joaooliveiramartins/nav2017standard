OBJECT Codeunit 31022916 Imp Jnl.-Post Line
{
  OBJECT-PROPERTIES
  {
    Date=01/02/15;
    Time=13:00:00;
    Version List=NAVPTSS81.00;
  }
  PROPERTIES
  {
    OnRun=BEGIN
          END;

  }
  CODE
  {
    VAR
      Text000@1000 : TextConst 'ENU="%2 must not be %3 in %4 %5 = %6 for %1.";PTG="%2 n�o tem de ser %3 em %4 %5 = %6 para %1."';
      Text001@1001 : TextConst 'ENU="%2 = %3 must be canceled first for %1.";PTG="%2 = %3 tem de ser anulado primeiro para %1."';
      Text002@1002 : TextConst 'ENU=%1 is not a %2.;PTG=%1 n�o � um %2.';
      FA@1003 : Record 5600;
      FA2@1004 : Record 5600;
      DeprBook@1005 : Record 5611;
      FADeprBook@1006 : Record 5612;
      FALedgEntry@1007 : Record 5601;
      MaintenanceLedgEntry@1008 : Record 5625;
      FAInsertLedgEntry@1010 : Codeunit 5600;
      FAJnlCheckLine@1011 : Codeunit 5631;
      DuplicateDeprBook@1012 : Codeunit 5640;
      CalculateDisposal@1013 : Codeunit 5605;
      CalculateDepr@1014 : Codeunit 5610;
      CalculateAcqCostDepr@1015 : Codeunit 5613;
      MakeFALedgEntry@1016 : Codeunit 5604;
      MakeMaintenanceLedgEntry@1017 : Codeunit 5647;
      DimMgt@1018 : Codeunit 408;
      FANo@1019 : Code[20];
      BudgetNo@1020 : Code[20];
      DeprBookCode@1021 : Code[10];
      FAPostingType@1022 : 'Acquisition Cost,Depreciation,Write-Down,Appreciation,Custom 1,Custom 2,Disposal,Maintenance,Salvage Value';
      FAPostingDate@1023 : Date;
      Amount2@1024 : Decimal;
      SalvageValue@1025 : Decimal;
      DeprUntilDate@1026 : Boolean;
      DeprAcqCost@1027 : Boolean;
      ErrorEntryNo@1028 : Integer;
      ResultOnDisposal@1029 : Integer;
      Text003@1031 : TextConst 'ENU="%1 = %2 already exists for %5 (%3 = %4).";PTG="%1 = %2 j�� existe para %5 (%3 = %4)."';

    PROCEDURE ImpJnlPostLine@11(ImpJnlLine@1000 : Record 31022970;CheckLine@1001 : Boolean);
    VAR
      ImpJnlBatch@1102058000 : Record 31022969;
    BEGIN
      WITH ImpJnlLine DO BEGIN
        IF "Imparity Code" = '' THEN
          EXIT;
        IF "Posting Date" = 0D THEN
          "Posting Date" := "Posting Date";
          MakeImpLedgEntry(ImpJnlLine);
      END;
    END;

    PROCEDURE MakeImpLedgEntry@1102058001(ImpJnlLine@1102058000 : Record 31022970);
    VAR
      ImpLdgEntry@1102058001 : Record 31022967;
      EntryNo@1102058002 : BigInteger;
      Imparities@1102058003 : Record 31022965;
    BEGIN
      IF ImpLdgEntry.FINDLAST THEN
        EntryNo := ImpLdgEntry."Entry No." + 1
      ELSE
        EntryNo := 1;

      ImpLdgEntry.INIT;
      ImpLdgEntry."Entry No." := EntryNo;
      Imparities.GET(ImpJnlLine."Imparity Code");
      ImpLdgEntry."Imparity Code" := ImpJnlLine."Imparity Code";
      ImpLdgEntry.Description := ImpJnlLine.Description;
      ImpLdgEntry."Entry Type" := ImpJnlLine."Imparity Entry";
      ImpLdgEntry.Amount := ImpJnlLine.Amount;
      ImpLdgEntry."Imparity Type" := ImpJnlLine."Imparity Type";
      ImpLdgEntry."Imparity SubType" := ImpJnlLine."Imparity SubType";
      ImpLdgEntry."Tax behavior" := ImpJnlLine."Tax behavior";
      ImpLdgEntry."Imparity Posting Gr." := ImpJnlLine."Imparity Posting Gr.";
      ImpLdgEntry."Posting Date" := ImpJnlLine."Posting Date";
      ImpLdgEntry."User ID" := USERID;
      ImpLdgEntry."Document No." := ImpJnlLine."Document No.";
      ImpLdgEntry."Journal Template Name" := ImpJnlLine."Journal Template Name";
      ImpLdgEntry."Journal Batch Name" := ImpJnlLine."Journal Batch Name";
      ImpLdgEntry."Account No." := ImpJnlLine."Account No.";
      ImpLdgEntry."Bal. Account No." := ImpJnlLine."Bal. Account No.";
      ImpLdgEntry."DRF Code" := ImpJnlLine."DRF Code";
      ImpLdgEntry."Bal. DRF Code" := ImpJnlLine."Bal. DRF Code";
      ImpLdgEntry."Dimension Set ID" := ImpJnlLine."Dimension Set ID";
      ImpLdgEntry.INSERT;
    END;

    BEGIN
    END.
  }
}

