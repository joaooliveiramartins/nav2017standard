OBJECT Page 104 Account Schedule
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0,NAVPTSS81.00;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Account Schedule;
               PTG=Esquema Contas];
    MultipleNewLines=Yes;
    SourceTable=Table85;
    DataCaptionFields=Schedule Name;
    PageType=Worksheet;
    AutoSplitKey=Yes;
    OnOpenPage=BEGIN
                 AccSchedManagement.OpenAndCheckSchedule(CurrentSchedName,Rec);
               END;

    OnAfterGetRecord=BEGIN
                       IF NOT DimCaptionsInitialized THEN
                         DimCaptionsInitialized := TRUE;
                     END;

    ActionList=ACTIONS
    {
      { 1900000003;0 ;ActionContainer;
                      ActionContainerType=RelatedInformation }
      { 29      ;1   ;Action    ;
                      Name=Overview;
                      Ellipsis=Yes;
                      CaptionML=[ENU=Overview;
                                 PTG=Vis�o Geral];
                      ToolTipML=ENU=View an overview of the current account schedule.;
                      ApplicationArea=#Basic,#Suite;
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=ViewDetails;
                      PromotedCategory=Process;
                      OnAction=VAR
                                 AccSchedOverview@1001 : Page 490;
                               BEGIN
                                 AccSchedOverview.SetAccSchedName(CurrentSchedName);
                                 AccSchedOverview.RUN;
                               END;
                                }
      { 1900000004;0 ;ActionContainer;
                      ActionContainerType=ActionItems }
      { 11      ;1   ;Action    ;
                      Name=Indent;
                      CaptionML=[ENU=Indent;
                                 PTG=Indentar];
                      ToolTipML=[ENU=Make this row part of a group of rows. For example, indent rows that itemize a range of accounts, such as types of revenue.;
                                 PTG=Fazer desta linha, parte de um grupo de linhas. Por exemplo, indentar linhas que agrupam um intervalo de contas, tais como tipos de receita.];
                      ApplicationArea=#Basic,#Suite;
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=Indent;
                      PromotedCategory=Process;
                      Scope=Repeater;
                      OnAction=VAR
                                 AccScheduleLine@1000 : Record 85;
                               BEGIN
                                 CurrPage.SETSELECTIONFILTER(AccScheduleLine);
                                 IF AccScheduleLine.FINDSET THEN
                                   REPEAT
                                     AccScheduleLine.Indent;
                                     AccScheduleLine.MODIFY;
                                   UNTIL AccScheduleLine.NEXT = 0;
                                 CurrPage.UPDATE(FALSE);
                               END;
                                }
      { 13      ;1   ;Action    ;
                      Name=Outdent;
                      CaptionML=[ENU=Outdent;
                                 PTG=Anular Indenta��o];
                      ToolTipML=[ENU=Move this row out one level.;
                                 PTG=Recuar esta linha um nivel.];
                      ApplicationArea=#Basic,#Suite;
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=DecreaseIndent;
                      PromotedCategory=Process;
                      Scope=Repeater;
                      OnAction=VAR
                                 AccScheduleLine@1000 : Record 85;
                               BEGIN
                                 CurrPage.SETSELECTIONFILTER(AccScheduleLine);
                                 IF AccScheduleLine.FINDSET THEN
                                   REPEAT
                                     AccScheduleLine.Outdent;
                                     AccScheduleLine.MODIFY;
                                   UNTIL AccScheduleLine.NEXT = 0;
                                 CurrPage.UPDATE(FALSE);
                               END;
                                }
      { 15      ;1   ;ActionGroup;
                      CaptionML=[ENU=F&unctions;
                                 PTG=F&un��es];
                      Image=Action }
      { 7       ;2   ;Action    ;
                      Name=InsertGLAccounts;
                      Ellipsis=Yes;
                      CaptionML=[ENU=Insert G/L Accounts;
                                 PTG=Inserir Contas C/G];
                      ToolTipML=ENU=Open the list of general ledger accounts so you can add accounts to the account schedule.;
                      ApplicationArea=#Basic,#Suite;
                      Image=InsertAccount;
                      OnAction=VAR
                                 AccSchedLine@1001 : Record 85;
                               BEGIN
                                 CurrPage.UPDATE(TRUE);
                                 SetupAccSchedLine(AccSchedLine);
                                 AccSchedManagement.InsertGLAccounts(AccSchedLine);
                               END;
                                }
      { 5       ;2   ;Action    ;
                      Name=InsertCFAccounts;
                      Ellipsis=Yes;
                      CaptionML=[ENU=Insert CF Accounts;
                                 PTG=Inserir Contas FC];
                      Image=InsertAccount;
                      OnAction=VAR
                                 AccSchedLine@1001 : Record 85;
                               BEGIN
                                 CurrPage.UPDATE(TRUE);
                                 SetupAccSchedLine(AccSchedLine);
                                 AccSchedManagement.InsertCFAccounts(AccSchedLine);
                               END;
                                }
      { 3       ;2   ;Action    ;
                      Name=InsertCostTypes;
                      Ellipsis=Yes;
                      CaptionML=[ENU=Insert Cost Types;
                                 PTG=Inserir Tipos Custo];
                      Image=InsertAccount;
                      OnAction=VAR
                                 AccSchedLine@1001 : Record 85;
                               BEGIN
                                 CurrPage.UPDATE(TRUE);
                                 SetupAccSchedLine(AccSchedLine);
                                 AccSchedManagement.InsertCostTypes(AccSchedLine);
                               END;
                                }
      { 24      ;2   ;Action    ;
                      Name=EditColumnLayoutSetup;
                      Ellipsis=Yes;
                      CaptionML=[ENU=Edit Column Layout Setup;
                                 PTG=Editar Config. Formato Coluna];
                      ToolTipML=ENU=Create or change the column layout for the current account schedule name.;
                      ApplicationArea=#Basic,#Suite;
                      RunObject=Page 489;
                      Image=SetupColumns }
      { 14      ;    ;ActionContainer;
                      CaptionML=[ENU=Reports;
                                 PTG=Mapas];
                      ActionContainerType=Reports }
      { 22      ;1   ;Action    ;
                      Name=Print;
                      Ellipsis=Yes;
                      CaptionML=[ENU=&Print;
                                 PTG=&Imprimir];
                      ToolTipML=ENU=Prepare to print the document. A report request window for the document opens where you can specify what to include on the print-out.;
                      ApplicationArea=#Basic,#Suite;
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=Print;
                      PromotedCategory=Report;
                      OnAction=VAR
                                 AccScheduleName@1001 : Record 84;
                               BEGIN
                                 AccScheduleName.GET("Schedule Name");
                                 //soft,sn
                                 IF AccScheduleName.Standardized THEN
                                   REPORT.RUN(REPORT::"Normalized Account Schedule",TRUE,FALSE,AccScheduleName)
                                 ELSE
                                 //soft,en
                                 AccScheduleName.Print;
                               END;
                                }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 18  ;1   ;Field     ;
                Lookup=Yes;
                CaptionML=[ENU=Name;
                           PTG=Nome];
                ToolTipML=ENU=Specifies the name of the account schedule.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr=CurrentSchedName;
                OnValidate=BEGIN
                             AccSchedManagement.CheckName(CurrentSchedName);
                             CurrentSchedNameOnAfterValidat;
                           END;

                OnLookup=BEGIN
                           EXIT(AccSchedManagement.LookupName(CurrentSchedName,Text));
                         END;
                          }

    { 1   ;1   ;Group     ;
                IndentationColumnName=Indentation;
                IndentationControls=Description;
                GroupType=Repeater }

    { 2   ;2   ;Field     ;
                ToolTipML=ENU=Specifies a number for the account schedule line.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Row No." }

    { 4   ;2   ;Field     ;
                ToolTipML=ENU=Specifies text that will appear on the account schedule line.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr=Description;
                Style=Strong;
                StyleExpr=Bold }

    { 6   ;2   ;Field     ;
                ToolTipML=ENU=Specifies the totaling type for the account schedule line. The type determines which accounts within the totaling interval you specify in the Totaling field will be totaled.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Totaling Type" }

    { 8   ;2   ;Field     ;
                ToolTipML=ENU=Specifies which accounts will be totaled on this line.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr=Totaling;
                OnLookup=VAR
                           CostType@1001 : Record 1103;
                           AccSchedName@1002 : Record 84;
                           GLAccList@1000 : Page 18;
                           HistoricGLAccList@1003 : Page 31022932;
                         BEGIN
                           BEGIN
                             IF "Totaling Type" IN
                                ["Totaling Type"::"Posting Accounts","Totaling Type"::"Total Accounts"]
                             THEN BEGIN
                               IF AccSchedName.GET(CurrentSchedName) THEN
                                 IF AccSchedName."Acc. No. Referred to old Acc." THEN BEGIN
                                   HistoricGLAccList.LOOKUPMODE(TRUE);
                                   IF NOT (HistoricGLAccList.RUNMODAL = ACTION::LookupOK) THEN
                                     EXIT(FALSE)
                                   ELSE
                                     Text := HistoricGLAccList.GetSelectionFilter;
                                   EXIT(TRUE);
                                 END ELSE BEGIN
                                   GLAccList.LOOKUPMODE(TRUE);
                                   IF NOT (GLAccList.RUNMODAL = ACTION::LookupOK) THEN
                                     EXIT(FALSE)
                                   ELSE
                                     Text := GLAccList.GetSelectionFilter;
                                   EXIT(TRUE);
                                 END;
                             END;
                             IF "Totaling Type" IN ["Totaling Type"::"Cost Type","Totaling Type"::"Cost Type Total"] THEN BEGIN
                               CostType."No." := FORMAT(Text,-20);

                               IF "Totaling Type" = "Totaling Type"::"Cost Type Total" THEN
                                 CostType.SETFILTER(Type,'%1|%2',CostType.Type::Total,CostType.Type::"End-Total");

                               IF PAGE.RUNMODAL(0,CostType) = ACTION::LookupOK THEN BEGIN
                                 Text := CostType."No.";

                                 EXIT(TRUE);

                               END;
                             END;


                             EXIT(FALSE);
                           END;
                         END;
                          }

    { 33  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the row type for the account schedule row. The type determines how the amounts in the row are calculated.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Row Type" }

    { 35  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the type of entries that will be included in the amounts in the account schedule row.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Amount Type" }

    { 1110004;2;Field     ;
                ApplicationArea=#Basic,#Suite;
                SourceExpr=Type }

    { 31022892;2;Field    ;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Positive Only" }

    { 31022893;2;Field    ;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Reverse Sign" }

    { 31  ;2   ;Field     ;
                ToolTipML=ENU=Specifies whether to show debits in reports as negative amounts with a minus sign and credits as positive amounts.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Show Opposite Sign" }

    { 1110002;2;Field     ;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Totaling 2";
                OnLookup=VAR
                           AccSchedName@1110004 : Record 84;
                           GLAccList@1110000 : Page 18;
                           HistoricGLAccList@1110003 : Page 31022932;
                         BEGIN
                           IF "Totaling Type" IN
                              ["Totaling Type"::"Posting Accounts","Totaling Type"::"Total Accounts"]
                           THEN BEGIN
                             IF AccSchedName.GET(CurrentSchedName) THEN
                               IF AccSchedName."Acc. No. Referred to old Acc." THEN BEGIN
                                 HistoricGLAccList.LOOKUPMODE(TRUE);
                                 IF NOT (HistoricGLAccList.RUNMODAL = ACTION::LookupOK) THEN
                                   EXIT(FALSE)
                                 ELSE
                                   Text := HistoricGLAccList.GetSelectionFilter;
                                 EXIT(TRUE);
                               END ELSE BEGIN
                                 GLAccList.LOOKUPMODE(TRUE);
                                 IF NOT (GLAccList.RUNMODAL = ACTION::LookupOK) THEN
                                   EXIT(FALSE)
                                 ELSE
                                   Text := GLAccList.GetSelectionFilter;
                                 EXIT(TRUE);
                               END;
                           END;
                         END;
                          }

    { 31022894;2;Field    ;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Positive Only 2" }

    { 31022895;2;Field    ;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Reverse Sign 2" }

    { 41  ;2   ;Field     ;
                ToolTipML=ENU=Specifies which dimension value amounts will be totaled on this line.;
                ApplicationArea=#Suite;
                SourceExpr="Dimension 1 Totaling";
                Visible=FALSE;
                OnLookup=BEGIN
                           EXIT(LookUpDimFilter(1,Text));
                         END;
                          }

    { 43  ;2   ;Field     ;
                ToolTipML=ENU=Specifies which dimension value amounts will be totaled on this line.;
                ApplicationArea=#Suite;
                SourceExpr="Dimension 2 Totaling";
                Visible=FALSE;
                OnLookup=BEGIN
                           EXIT(LookUpDimFilter(2,Text));
                         END;
                          }

    { 45  ;2   ;Field     ;
                ToolTipML=ENU=Specifies which dimension value amounts will be totaled on this line.;
                ApplicationArea=#Suite;
                SourceExpr="Dimension 3 Totaling";
                Visible=FALSE;
                OnLookup=BEGIN
                           EXIT(LookUpDimFilter(3,Text));
                         END;
                          }

    { 47  ;2   ;Field     ;
                ToolTipML=ENU=Specifies which dimension value amounts will be totaled on this line.;
                ApplicationArea=#Suite;
                SourceExpr="Dimension 4 Totaling";
                Visible=FALSE;
                OnLookup=BEGIN
                           EXIT(LookUpDimFilter(4,Text));
                         END;
                          }

    { 31022890;2;Field    ;
                SourceExpr="Cost Center Totaling";
                OnLookup=BEGIN
                           EXIT(LookUpDimFilter(5,Text)); //soft,n
                         END;
                          }

    { 31022891;2;Field    ;
                SourceExpr="Cost Object Totaling";
                OnLookup=BEGIN
                           EXIT(LookUpDimFilter(6,Text)); //soft,n
                         END;
                          }

    { 12  ;2   ;Field     ;
                ToolTipML=ENU=Specifies whether the account schedule line will be printed on the report.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr=Show }

    { 20  ;2   ;Field     ;
                ToolTipML=ENU=Specifies whether to print the amounts in this row in bold.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr=Bold }

    { 25  ;2   ;Field     ;
                ToolTipML=ENU=Specifies whether to print the amounts in this row in italics.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr=Italic }

    { 28  ;2   ;Field     ;
                ToolTipML=ENU=Specifies whether to underline the amounts in this row.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr=Underline }

    { 9   ;2   ;Field     ;
                ToolTipML=ENU=Specifies whether to double underline the amounts in this row.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Double Underline";
                Visible=FALSE }

    { 10  ;2   ;Field     ;
                ToolTipML=ENU=Specifies whether there will be a page break after the current account when the account schedule is printed.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="New Page" }

    { 1110016;2;Field     ;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Column Value" }

    { 1110014;2;Field     ;
                ApplicationArea=#Basic,#Suite;
                SourceExpr=Indentation }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {
    VAR
      AccSchedManagement@1000 : Codeunit 8;
      CurrentSchedName@1001 : Code[10];
      DimCaptionsInitialized@1003 : Boolean;

    PROCEDURE SetAccSchedName@1(NewAccSchedName@1000 : Code[10]);
    BEGIN
      CurrentSchedName := NewAccSchedName;
    END;

    LOCAL PROCEDURE CurrentSchedNameOnAfterValidat@19053875();
    BEGIN
      CurrPage.SAVERECORD;
      AccSchedManagement.SetName(CurrentSchedName,Rec);
      CurrPage.UPDATE(FALSE);
    END;

    PROCEDURE SetupAccSchedLine@3(VAR AccSchedLine@1000 : Record 85);
    BEGIN
      AccSchedLine := Rec;
      IF "Line No." = 0 THEN BEGIN
        AccSchedLine := xRec;
        AccSchedLine.SETRANGE("Schedule Name",CurrentSchedName);
        IF AccSchedLine.NEXT = 0 THEN
          AccSchedLine."Line No." := xRec."Line No." + 10000
        ELSE BEGIN
          IF AccSchedLine.FINDLAST THEN
            AccSchedLine."Line No." += 10000;
          AccSchedLine.SETRANGE("Schedule Name");
        END;
      END;
    END;

    BEGIN
    END.
  }
}

