OBJECT Table 9154 My Job
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0;
  }
  PROPERTIES
  {
    CaptionML=[ENU=My Job;
               PTG=O Meu Projeto];
  }
  FIELDS
  {
    { 1   ;   ;User ID             ;Code50        ;TableRelation=User."User Name";
                                                   ValidateTableRelation=No;
                                                   TestTableRelation=No;
                                                   CaptionML=[ENU=User ID;
                                                              PTG=ID Utilizador] }
    { 2   ;   ;Job No.             ;Code20        ;TableRelation=Job;
                                                   CaptionML=[ENU=Job No.;
                                                              PTG=N� Projeto];
                                                   NotBlank=Yes }
    { 3   ;   ;Exclude from Business Chart;Boolean;CaptionML=[ENU=Exclude from Business Chart;
                                                              PTG=Excluir do Gr�fico] }
    { 4   ;   ;Description         ;Text50        ;CaptionML=[ENU=Description;
                                                              PTG=Descri��o] }
    { 5   ;   ;Status              ;Option        ;InitValue=Open;
                                                   CaptionML=[ENU=Status;
                                                              PTG=Estado];
                                                   OptionCaptionML=[ENU=Planning,Quote,Open,Completed;
                                                                    PTG=Planeamento,Proposta,Aberto,Completo];
                                                   OptionString=Planning,Quote,Open,Completed }
    { 6   ;   ;Bill-to Name        ;Text50        ;CaptionML=[ENU=Bill-to Name;
                                                              PTG=Faturar-a Nome] }
    { 7   ;   ;Percent Completed   ;Decimal       ;CaptionML=[ENU=Percent Completed;
                                                              PTG=Percentagem Completa] }
    { 8   ;   ;Percent Invoiced    ;Decimal       ;CaptionML=[ENU=Percent Invoiced;
                                                              PTG=Percentagem Faturada] }
  }
  KEYS
  {
    {    ;User ID,Job No.                         ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    END.
  }
}

