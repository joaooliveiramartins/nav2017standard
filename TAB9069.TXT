OBJECT Table 9069 O365 Sales Cue
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0;
  }
  PROPERTIES
  {
    CaptionML=[ENU=O365 Sales Cue;
               PTG=Fila Vendas O365];
  }
  FIELDS
  {
    { 1   ;   ;Primary Key         ;Code10        ;CaptionML=[ENU=Primary Key;
                                                              PTG=Chave Prim�ria] }
    { 2   ;   ;Overdue Sales Documents;Integer    ;FieldClass=FlowField;
                                                   CalcFormula=Count("Cust. Ledger Entry" WHERE (Document Type=FILTER(Invoice|Credit Memo),
                                                                                                 Due Date=FIELD(Overdue Date Filter),
                                                                                                 Open=CONST(Yes)));
                                                   CaptionML=[ENU=Overdue Sales Documents;
                                                              PTG=Documentos de Venda Vencidos] }
    { 3   ;   ;Customers - Blocked ;Integer       ;FieldClass=FlowField;
                                                   CalcFormula=Count(Customer WHERE (Blocked=FILTER(<>' ')));
                                                   CaptionML=[ENU=Customers - Blocked;
                                                              PTG=Clientes - Bloqueado] }
    { 4   ;   ;CM Date Filter      ;Date          ;FieldClass=FlowFilter;
                                                   CaptionML=[ENU=CM Date Filter;
                                                              PTG=Filtro Data M�s Corrente] }
    { 5   ;   ;YTD Date Filter     ;Date          ;FieldClass=FlowFilter;
                                                   CaptionML=[ENU=YTD Date Filter;
                                                              PTG=Filtro Data YTD] }
    { 6   ;   ;Due Date Filter     ;Date          ;FieldClass=FlowFilter;
                                                   CaptionML=[ENU=Due Date Filter;
                                                              PTG=Filtro Data Vencimento] }
    { 7   ;   ;Overdue Date Filter ;Date          ;FieldClass=FlowFilter;
                                                   CaptionML=[ENU=Overdue Date Filter;
                                                              PTG=Filtro Data Vencida] }
    { 8   ;   ;User ID Filter      ;Code50        ;FieldClass=FlowFilter;
                                                   CaptionML=[ENU=User ID Filter;
                                                              PTG=Filtro ID Utilizador] }
    { 9   ;   ;Non-Applied Payments;Integer       ;FieldClass=FlowField;
                                                   CalcFormula=Count("Bank Acc. Reconciliation" WHERE (Statement Type=CONST(Payment Application)));
                                                   CaptionML=[ENU=Non-Applied Payments;
                                                              PTG=Pagamentos N�o Liquidados] }
    { 10  ;   ;Invoiced YTD        ;Decimal       ;FieldClass=FlowField;
                                                   CalcFormula=Sum("Detailed Cust. Ledg. Entry"."Amount (LCY)" WHERE (Posting Date=FIELD(YTD Date Filter),
                                                                                                                      Entry Type=CONST(Initial Entry),
                                                                                                                      Document Type=FILTER(>Payment)));
                                                   CaptionML=[ENU=Invoiced YTD;
                                                              PTG=Faturado YTD] }
    { 11  ;   ;Invoiced CM         ;Decimal       ;FieldClass=FlowField;
                                                   CalcFormula=Sum("Detailed Cust. Ledg. Entry"."Amount (LCY)" WHERE (Posting Date=FIELD(CM Date Filter),
                                                                                                                      Entry Type=CONST(Initial Entry),
                                                                                                                      Document Type=FILTER(>Payment)));
                                                   CaptionML=[ENU=Invoiced CM;
                                                              PTG=Faturado M�s Corrente] }
    { 12  ;   ;Sales Invoices Outstanding;Decimal ;FieldClass=FlowField;
                                                   CalcFormula=Sum("Detailed Cust. Ledg. Entry"."Amount (LCY)");
                                                   CaptionML=[ENU=Sales Invoices Outstanding;
                                                              PTG=Faturas de Venda Pendentes] }
    { 13  ;   ;Sales Invoices Overdue;Decimal     ;FieldClass=FlowField;
                                                   CalcFormula=Sum("Detailed Cust. Ledg. Entry"."Amount (LCY)" WHERE (Initial Entry Due Date=FIELD(Due Date Filter)));
                                                   CaptionML=[ENU=Sales Invoices Overdue;
                                                              PTG=Faturas de Venda  Vencidas] }
  }
  KEYS
  {
    {    ;Primary Key                             ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    END.
  }
}

