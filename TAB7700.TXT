OBJECT Table 7700 Miniform Header
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0;
  }
  PROPERTIES
  {
    OnDelete=VAR
               MiniFormLine@1000 : Record 7701;
               MiniFormFunc@1001 : Record 7703;
             BEGIN
               MiniFormLine.RESET;
               MiniFormLine.SETRANGE("Miniform Code",Code);
               MiniFormLine.DELETEALL;

               MiniFormFunc.RESET;
               MiniFormFunc.SETRANGE("Miniform Code",Code);
               MiniFormFunc.DELETEALL;
             END;

    CaptionML=[ENU=Miniform Header;
               PTG=Cab. Miniform];
    LookupPageID=Page7703;
  }
  FIELDS
  {
    { 1   ;   ;Code                ;Code20        ;CaptionML=[ENU=Code;
                                                              PTG=C�digo];
                                                   NotBlank=Yes }
    { 11  ;   ;Description         ;Text30        ;CaptionML=[ENU=Description;
                                                              PTG=Descri��o] }
    { 12  ;   ;No. of Records in List;Integer     ;CaptionML=[ENU=No. of Records in List;
                                                              PTG=N� Registos na Lista] }
    { 13  ;   ;Form Type           ;Option        ;CaptionML=[ENU=Form Type;
                                                              PTG=Form Type];
                                                   OptionCaptionML=[@@@={Locked};
                                                                    ENU=Card,Selection List,Data List,Data List Input;
                                                                    PTG=Ficha,Lista Sele��o,Lista Dados,Lista Dados Input];
                                                   OptionString=Card,Selection List,Data List,Data List Input }
    { 15  ;   ;Start Miniform      ;Boolean       ;OnValidate=VAR
                                                                MiniformHeader@1000 : Record 7700;
                                                              BEGIN
                                                                MiniformHeader.SETFILTER(Code,'<>%1',Code);
                                                                MiniformHeader.SETRANGE("Start Miniform",TRUE);
                                                                IF NOT MiniformHeader.ISEMPTY THEN
                                                                  ERROR(Text002);
                                                              END;

                                                   CaptionML=[ENU=Start Miniform;
                                                              PTG=Iniciar Miniform] }
    { 20  ;   ;Handling Codeunit   ;Integer       ;TableRelation=AllObjWithCaption."Object ID" WHERE (Object Type=CONST(Codeunit));
                                                   CaptionML=[ENU=Handling Codeunit;
                                                              PTG=Processar Codeunit] }
    { 21  ;   ;Next Miniform       ;Code20        ;TableRelation="Miniform Header";
                                                   OnValidate=BEGIN
                                                                IF "Next Miniform" = Code THEN
                                                                  ERROR(Text000);

                                                                IF "Form Type" IN ["Form Type"::"Selection List","Form Type"::"Data List Input"] THEN
                                                                  ERROR(Text001,FIELDCAPTION("Form Type"),"Form Type");
                                                              END;

                                                   CaptionML=[ENU=Next Miniform;
                                                              PTG=Pr�ximo Miniform] }
    { 25  ;   ;XMLin               ;BLOB          ;CaptionML=[ENU=XMLin;
                                                              PTG=XMLin] }
  }
  KEYS
  {
    {    ;Code                                    ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {
    VAR
      Text000@1001 : TextConst 'ENU=Recursion is not allowed.;PTG=A recurs�o n�o � permitida.';
      Text001@1000 : TextConst 'ENU=%1 must not be %2.;PTG=%1 n�o deve ser %2.';
      Text002@1002 : TextConst 'ENU=There can only be one login form.;PTG=S� pode existir um formul�rio de login.';

    PROCEDURE SaveXMLin@1(DOMxmlin@1001 : DotNet "'System.Xml, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089'.System.Xml.XmlDocument");
    VAR
      InStrm@1000 : InStream;
    BEGIN
      XMLin.CREATEINSTREAM(InStrm);
      DOMxmlin.Save(InStrm);
    END;

    PROCEDURE LoadXMLin@2(VAR DOMxmlin@1001 : DotNet "'System.Xml, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089'.System.Xml.XmlDocument");
    VAR
      XMLDOMManagement@1002 : Codeunit 6224;
      OutStrm@1000 : OutStream;
    BEGIN
      XMLin.CREATEOUTSTREAM(OutStrm);
      XMLDOMManagement.LoadXMLDocumentFromOutStream(OutStrm,DOMxmlin);
    END;

    BEGIN
    END.
  }
}

