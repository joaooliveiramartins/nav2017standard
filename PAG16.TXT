OBJECT Page 16 Chart of Accounts
{
  OBJECT-PROPERTIES
  {
    Date=02/10/17;
    Time=17:11:11;
    Version List=NAVW110.0,NAVPT10.00#00003;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Chart of Accounts;
               PTG=Plano de Contas];
    SourceTable=Table15;
    PageType=List;
    CardPageID=G/L Account Card;
    RefreshOnActivate=Yes;
    PromotedActionCategoriesML=[ENU=New,Process,Report,Periodic Activities;
                                PTG=Novo,Processar,Mapas,Atividades Peri�dicas];
    OnAfterGetRecord=BEGIN
                       NoEmphasize := "Account Type" <> "Account Type"::Posting;
                       NameIndent := Indentation;
                       NameEmphasize := "Account Type" <> "Account Type"::Posting;
                     END;

    OnNewRecord=BEGIN
                  SetupNewGLAcc(xRec,BelowxRec);
                END;

    ActionList=ACTIONS
    {
      { 1900000003;0 ;ActionContainer;
                      ActionContainerType=RelatedInformation }
      { 22      ;1   ;ActionGroup;
                      CaptionML=[ENU=A&ccount;
                                 PTG=&Conta];
                      Image=ChartOfAccounts }
      { 28      ;2   ;Action    ;
                      ShortCutKey=Ctrl+F7;
                      CaptionML=[ENU=Ledger E&ntries;
                                 PTG=Movime&ntos];
                      ToolTipML=ENU=View the history of transactions that have been posted for the selected record.;
                      ApplicationArea=#Basic,#Suite;
                      RunObject=Page 20;
                      RunPageView=SORTING(G/L Account No.);
                      RunPageLink=G/L Account No.=FIELD(No.);
                      Promoted=No;
                      Image=GLRegisters;
                      PromotedCategory=Process }
      { 25      ;2   ;Action    ;
                      CaptionML=[ENU=Co&mments;
                                 PTG=Co&ment�rios];
                      ToolTipML=ENU=Show or add comments.;
                      RunObject=Page 124;
                      RunPageLink=Table Name=CONST(G/L Account),
                                  No.=FIELD(No.);
                      Image=ViewComments }
      { 34      ;2   ;ActionGroup;
                      CaptionML=[ENU=Dimensions;
                                 PTG=Dimens�es];
                      Image=Dimensions }
      { 84      ;3   ;Action    ;
                      ShortCutKey=Shift+Ctrl+D;
                      CaptionML=[ENU=Dimensions-Single;
                                 PTG=Dimens�es-Singular];
                      ToolTipML=ENU=View or edit the single set of dimensions that are set up for the selected record.;
                      ApplicationArea=#Suite;
                      RunObject=Page 540;
                      RunPageLink=Table ID=CONST(15),
                                  No.=FIELD(No.);
                      Image=Dimensions }
      { 33      ;3   ;Action    ;
                      AccessByPermission=TableData 348=R;
                      CaptionML=[ENU=Dimensions-&Multiple;
                                 PTG=Dimens�es-&M�ltipla];
                      ToolTipML=ENU=View or edit dimensions for a group of records. You can assign dimension codes to transactions to distribute costs and analyze historical information.;
                      ApplicationArea=#Suite;
                      Image=DimensionSets;
                      OnAction=VAR
                                 GLAcc@1001 : Record 15;
                                 DefaultDimMultiple@1002 : Page 542;
                               BEGIN
                                 CurrPage.SETSELECTIONFILTER(GLAcc);
                                 DefaultDimMultiple.SetMultiGLAcc(GLAcc);
                                 DefaultDimMultiple.RUNMODAL;
                               END;
                                }
      { 23      ;2   ;Action    ;
                      CaptionML=[ENU=E&xtended Texts;
                                 PTG=Te&xtos Adicionais];
                      ToolTipML=ENU=View additional information that has been added to the description for the current account.;
                      ApplicationArea=#Suite;
                      RunObject=Page 391;
                      RunPageView=SORTING(Table Name,No.,Language Code,All Language Codes,Starting Date,Ending Date);
                      RunPageLink=Table Name=CONST(G/L Account),
                                  No.=FIELD(No.);
                      Image=Text }
      { 27      ;2   ;Action    ;
                      CaptionML=[ENU=Receivables-Payables;
                                 PTG=Cobran�as-Pagamentos];
                      ToolTipML=ENU=Show a summary of receivables and payables.;
                      ApplicationArea=#Suite;
                      RunObject=Page 159;
                      Image=ReceivablesPayables }
      { 54      ;2   ;Action    ;
                      CaptionML=[ENU=Where-Used List;
                                 PTG=Lista Onde Usado];
                      ToolTipML=ENU=Show setup tables where the current account is used.;
                      ApplicationArea=#Basic,#Suite;
                      Image=Track;
                      OnAction=VAR
                                 CalcGLAccWhereUsed@1000 : Codeunit 100;
                               BEGIN
                                 CalcGLAccWhereUsed.CheckGLAcc("No.");
                               END;
                                }
      { 123     ;1   ;ActionGroup;
                      CaptionML=[ENU=&Balance;
                                 PTG=&Saldo];
                      Image=Balance }
      { 36      ;2   ;Action    ;
                      CaptionML=[ENU=G/L &Account Balance;
                                 PTG=S&aldo Conta C/G];
                      ToolTipML=ENU=View a summary of the debit and credit balances for different time periods for the current account.;
                      ApplicationArea=#Basic,#Suite;
                      RunObject=Page 415;
                      RunPageLink=No.=FIELD(No.),
                                  Global Dimension 1 Filter=FIELD(Global Dimension 1 Filter),
                                  Global Dimension 2 Filter=FIELD(Global Dimension 2 Filter),
                                  Business Unit Filter=FIELD(Business Unit Filter);
                      Image=GLAccountBalance }
      { 132     ;2   ;Action    ;
                      CaptionML=[ENU=G/L &Balance;
                                 PTG=&Saldo C/G];
                      ToolTipML=ENU=View a summary of the debit and credit balances for different time periods for all accounts.;
                      ApplicationArea=#Basic,#Suite;
                      RunObject=Page 414;
                      RunPageOnRec=Yes;
                      RunPageLink=Global Dimension 1 Filter=FIELD(Global Dimension 1 Filter),
                                  Global Dimension 2 Filter=FIELD(Global Dimension 2 Filter),
                                  Business Unit Filter=FIELD(Business Unit Filter);
                      Image=GLBalance }
      { 126     ;2   ;Action    ;
                      CaptionML=[ENU=G/L Balance by &Dimension;
                                 PTG=Saldo por &Dimens�o];
                      ToolTipML=ENU=View a summary of the debit and credit balances by dimensions for all accounts.;
                      ApplicationArea=#Suite;
                      RunObject=Page 408;
                      Image=GLBalanceDimension }
      { 52      ;2   ;Separator ;
                      CaptionML=[ENU="";
                                 PTG=""] }
      { 53      ;2   ;Action    ;
                      CaptionML=[ENU=G/L Account Balance/Bud&get;
                                 PTG=Saldo/Or�amento Conta C/&G];
                      ToolTipML=ENU=View a summary of the debit and credit balances and the budgeted amounts for different time periods for the current account.;
                      ApplicationArea=#Suite;
                      RunObject=Page 154;
                      RunPageLink=No.=FIELD(No.),
                                  Global Dimension 1 Filter=FIELD(Global Dimension 1 Filter),
                                  Global Dimension 2 Filter=FIELD(Global Dimension 2 Filter),
                                  Business Unit Filter=FIELD(Business Unit Filter),
                                  Budget Filter=FIELD(Budget Filter);
                      Image=Period }
      { 35      ;2   ;Action    ;
                      CaptionML=[ENU=G/L Balance/B&udget;
                                 PTG=Saldo/Or�amen&to C/G];
                      ToolTipML=ENU=View a summary of the debit and credit balances and the budgeted amounts for different time periods for the current account.;
                      ApplicationArea=#Suite;
                      RunObject=Page 422;
                      RunPageOnRec=Yes;
                      RunPageLink=Global Dimension 1 Filter=FIELD(Global Dimension 1 Filter),
                                  Global Dimension 2 Filter=FIELD(Global Dimension 2 Filter),
                                  Business Unit Filter=FIELD(Business Unit Filter),
                                  Budget Filter=FIELD(Budget Filter);
                      Image=ChartOfAccounts }
      { 55      ;2   ;Separator  }
      { 56      ;2   ;Action    ;
                      CaptionML=[ENU=Chart of Accounts &Overview;
                                 PTG=Vis�o Geral Plan&o Contas];
                      ToolTipML=ENU=View the chart of accounts with different levels of detail where you can expand or collapse a section of the chart of accounts.;
                      ApplicationArea=#Basic,#Suite;
                      RunObject=Page 634;
                      Image=Accounts }
      { 1900210203;1 ;Action    ;
                      CaptionML=[ENU=G/L Register;
                                 PTG=Regs. Movs. C/G];
                      ToolTipML=ENU=View posted G/L entries.;
                      ApplicationArea=#Basic,#Suite;
                      RunObject=Page 116;
                      Promoted=Yes;
                      Image=GLRegisters;
                      PromotedCategory=Process;
                      PromotedOnly=Yes }
      { 1900000004;0 ;ActionContainer;
                      ActionContainerType=ActionItems }
      { 122     ;1   ;ActionGroup;
                      CaptionML=[ENU=F&unctions;
                                 PTG=F&un��es];
                      Image=Action }
      { 30      ;2   ;Action    ;
                      Name=IndentChartOfAccounts;
                      CaptionML=[ENU=Indent Chart of Accounts;
                                 PTG=Indentar Plano de Contas];
                      ToolTipML=ENU=Indent accounts between a Begin-Total and the matching End-Total one level to make the chart of accounts easier to read.;
                      ApplicationArea=#Basic,#Suite;
                      RunObject=Codeunit 3;
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=IndentChartOfAccounts;
                      PromotedCategory=Process;
                      PromotedOnly=Yes }
      { 1110006 ;2   ;Action    ;
                      Name=CheckChart;
                      CaptionML=[ENU=Check Chart of Accounts;
                                 PTG=Verificar Plano de Contas];
                      ApplicationArea=#All;
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=IndentChartOfAccounts;
                      PromotedCategory=Process;
                      PromotedOnly=Yes;
                      OnAction=VAR
                                 CheckAccounts@1110001 : Codeunit 3;
                               BEGIN
                                 //soft,sn
                                 GLSetup.GET;
                                 GLSetup.TESTFIELD("Check Chart of Accounts");
                                 CheckAccounts.CheckChartAcc;
                                 //soft,en
                               END;
                                }
      { 40      ;1   ;ActionGroup;
                      CaptionML=[ENU=Periodic Activities;
                                 PTG=Atividades Peri�dicas] }
      { 70      ;2   ;Action    ;
                      CaptionML=[ENU=General Journal;
                                 PTG=Di�rio Geral];
                      ToolTipML=ENU=Open the general journal, for example, to record or post a payment that has no related document.;
                      RunObject=Page 39;
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=Journal;
                      PromotedCategory=Process;
                      PromotedOnly=Yes }
      { 20      ;2   ;Action    ;
                      CaptionML=[ENU=Close Income Statement;
                                 PTG=Lan�amento Regulariza��o];
                      ToolTipML=ENU=Start the transfer of the year's result to an account in the balance sheet and close the income statement accounts.;
                      ApplicationArea=#Basic,#Suite;
                      RunObject=Report 94;
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=CloseYear;
                      PromotedCategory=Process;
                      PromotedOnly=Yes }
      { 5       ;2   ;Action    ;
                      Name=DocsWithoutIC;
                      CaptionML=[ENU=Posted Documents without Incoming Document;
                                 PTG=Documentos Registados sem Documento Recebido];
                      ToolTipML=ENU=Show a list of posted purchase and sales documents under the G/L account that do not have related incoming document records.;
                      Image=Documents;
                      OnAction=VAR
                                 PostedDocsWithNoIncBuf@1001 : Record 134;
                               BEGIN
                                 IF "Account Type" = "Account Type"::Posting THEN
                                   PostedDocsWithNoIncBuf.SETRANGE("G/L Account No. Filter","No.")
                                 ELSE
                                   IF Totaling <> '' THEN
                                     PostedDocsWithNoIncBuf.SETFILTER("G/L Account No. Filter",Totaling)
                                   ELSE
                                     EXIT;
                                 PAGE.RUN(PAGE::"Posted Docs. With No Inc. Doc.",PostedDocsWithNoIncBuf);
                               END;
                                }
      { 1900000006;  ;ActionContainer;
                      ActionContainerType=Reports }
      { 1900670506;1 ;Action    ;
                      CaptionML=[ENU=Detail Trial Balance;
                                 PTG=Balancete Detalhado];
                      ToolTipML=ENU=View a detail trial balance for the general ledger accounts that you specify.;
                      ApplicationArea=#Basic,#Suite;
                      RunObject=Report 31022958;
                      Promoted=Yes;
                      Image=Report;
                      PromotedCategory=Report;
                      PromotedOnly=Yes }
      { 1904082706;1 ;Action    ;
                      CaptionML=[ENU=Trial Balance;
                                 PTG=Balancete];
                      ToolTipML=ENU=View the chart of accounts that have balances and net changes.;
                      ApplicationArea=#Basic,#Suite;
                      RunObject=Report 6;
                      Promoted=Yes;
                      Image=Report;
                      PromotedCategory=Report;
                      PromotedOnly=Yes }
      { 1902174606;1 ;Action    ;
                      CaptionML=[ENU=Trial Balance by Period;
                                 PTG=Balancete por Per�odo];
                      ToolTipML=ENU=View the opening balance by general ledger account, the movements in the selected period of month, quarter, or year, and the resulting closing balance.;
                      ApplicationArea=#Basic,#Suite;
                      RunObject=Report 38;
                      Image=Report;
                      PromotedCategory=Report }
      { 1900210206;1 ;Action    ;
                      CaptionML=[ENU=G/L Register;
                                 PTG=Regs. Movs. C/G];
                      ToolTipML=ENU=View posted G/L entries.;
                      ApplicationArea=#Basic,#Suite;
                      RunObject=Report 3;
                      Promoted=Yes;
                      Image=Report;
                      PromotedCategory=Report;
                      PromotedOnly=Yes }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                IndentationColumnName=NameIndent;
                IndentationControls=Name;
                GroupType=Repeater }

    { 2   ;2   ;Field     ;
                ToolTipML=ENU=Specifies the No. of the G/L Account you are setting up.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="No.";
                Style=Strong;
                StyleExpr=NoEmphasize }

    { 4   ;2   ;Field     ;
                ToolTipML=ENU=Specifies the name of the general ledger account.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr=Name;
                Style=Strong;
                StyleExpr=NameEmphasize }

    { 6   ;2   ;Field     ;
                ToolTipML=ENU=Specifies whether a general ledger account is an income statement account or a balance sheet account.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Income/Balance";
                Visible=FALSE }

    { 9   ;2   ;Field     ;
                ToolTipML=ENU=Specifies the category of the G/L account.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Account Category";
                Visible=FALSE }

    { 11  ;2   ;Field     ;
                CaptionML=[ENU=Account Subcategory;
                           PTG=Sub-Categoria Conta];
                ToolTipML=ENU=Specifies the subcategory of the account category of the G/L account.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Account Subcategory Descript." }

    { 8   ;2   ;Field     ;
                ToolTipML=ENU=Specifies the purpose of the account. Newly created accounts are automatically assigned the Posting account type, but you can change this.;
                OptionCaptionML=[ENU=Posting,,Total;
                                 PTG=Auxiliar,,Maior];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Account Type";
                Visible=FALSE }

    { 26  ;2   ;Field     ;
                ToolTipML=ENU=Specifies whether you will be able to post directly or only indirectly to this general ledger account.;
                SourceExpr="Direct Posting";
                Visible=FALSE }

    { 10  ;2   ;Field     ;
                ToolTipML=ENU=Specifies an account interval or a list of account numbers.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr=Totaling;
                Visible=FALSE;
                OnLookup=VAR
                           GLaccList@1000 : Page 18;
                         BEGIN
                           GLaccList.LOOKUPMODE(TRUE);
                           IF NOT (GLaccList.RUNMODAL = ACTION::LookupOK) THEN
                             EXIT(FALSE);

                           Text := GLaccList.GetSelectionFilter;
                           EXIT(TRUE);
                         END;
                          }

    { 12  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the general posting type to use when posting to this account.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Gen. Posting Type" }

    { 37  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the general business posting group that applies to the entry.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Gen. Bus. Posting Group" }

    { 14  ;2   ;Field     ;
                ToolTipML=ENU=Specifies a general product posting group code.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Gen. Prod. Posting Group" }

    { 32  ;2   ;Field     ;
                ToolTipML=ENU=Specifies a VAT Bus. Posting Group.;
                SourceExpr="VAT Bus. Posting Group";
                Visible=FALSE }

    { 44  ;2   ;Field     ;
                ToolTipML=ENU=Specifies a VAT Prod. Posting Group code.;
                SourceExpr="VAT Prod. Posting Group";
                Visible=FALSE }

    { 1110009;2;Field     ;
                SourceExpr="Debit Amount" }

    { 1110011;2;Field     ;
                SourceExpr="Credit Amount" }

    { 16  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the net change in the account balance during the time period in the Date Filter field.;
                ApplicationArea=#Basic,#Suite;
                BlankZero=Yes;
                SourceExpr="Net Change" }

    { 59  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the G/L account balance on the last date included in the Date Filter field.;
                BlankZero=Yes;
                SourceExpr="Balance at Date";
                Visible=FALSE }

    { 18  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the balance on this account.;
                ApplicationArea=#Basic,#Suite;
                BlankZero=Yes;
                SourceExpr=Balance }

    { 1110002;2;Field     ;
                SourceExpr="Add.-Currency Debit Amount";
                Visible=FALSE }

    { 1110004;2;Field     ;
                SourceExpr="Add.-Currency Credit Amount";
                Visible=FALSE }

    { 46  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the net change in the account balance.;
                BlankZero=Yes;
                SourceExpr="Additional-Currency Net Change";
                Visible=FALSE }

    { 48  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the G/L account balance (in the additional reporting currency) on the last date included in the Date Filter field.;
                BlankZero=Yes;
                SourceExpr="Add.-Currency Balance at Date";
                Visible=FALSE }

    { 50  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the balance on this account, in the additional reporting currency.;
                BlankZero=Yes;
                SourceExpr="Additional-Currency Balance";
                Visible=FALSE }

    { 39  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the account number in a consolidated company to transfer credit balances.;
                SourceExpr="Consol. Debit Acc.";
                Visible=FALSE }

    { 41  ;2   ;Field     ;
                ToolTipML=ENU=Specifies if amounts without any payment tolerance amount from the customer and vendor ledger entries are used.;
                SourceExpr="Consol. Credit Acc.";
                Visible=FALSE }

    { 3   ;2   ;Field     ;
                ToolTipML=ENU=Specifies a cost type number to establish which cost type a general ledger account belongs to.;
                SourceExpr="Cost Type No." }

    { 61  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the consolidation translation method that will be used for the account.;
                SourceExpr="Consol. Translation Method";
                Visible=FALSE }

    { 57  ;2   ;Field     ;
                ToolTipML=ENU=Specifies accounts that you often enter in the Bal. Account No. field on intercompany journal or document lines.;
                SourceExpr="Default IC Partner G/L Acc. No";
                Visible=FALSE }

    { 1110007;2;Field     ;
                CaptionML=[ENU=Specifies the adjustment account for the comercial posting accounts.;
                           PTG=Especifica a conta de regulariza��o para as contas de registo comercial.];
                SourceExpr="Income Stmt. Bal. Acc.";
                Visible=FALSE }

    { 7   ;2   ;Field     ;
                CaptionML=[ENU=Default Deferral Template;
                           PTG=Livro Diferimentos Padr�o];
                ToolTipML=ENU=Specifies the default deferral template that governs how to defer revenues and expenses to the periods when they occurred.;
                ApplicationArea=#Suite;
                SourceExpr="Default Deferral Template Code" }

    { 31022891;2;Field    ;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Taxonomy Code" }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1905532107;1;Part   ;
                SubPageLink=Table ID=CONST(15),
                            No.=FIELD(No.);
                PagePartID=Page9083;
                Visible=FALSE;
                PartType=Page }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                Visible=TRUE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {
    VAR
      NoEmphasize@1000 : Boolean INDATASET;
      NameEmphasize@1001 : Boolean INDATASET;
      NameIndent@1002 : Integer INDATASET;
      "//--soft-global--//"@9000000 : Integer;
      GLSetup@1110001 : Record 98;

    BEGIN
    {
      20170210 V10.00#00003 : Tradu��es
    }
    END.
  }
}

