OBJECT Page 7400 Internal Movement List
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0;
  }
  PROPERTIES
  {
    Editable=No;
    CaptionML=[ENU=Internal Movement List;
               PTG=Lista Movs. Internos];
    SourceTable=Table7346;
    PageType=List;
    CardPageID=Internal Movement;
    ActionList=ACTIONS
    {
      { 1900000003;0 ;ActionContainer;
                      ActionContainerType=RelatedInformation }
      { 17      ;1   ;ActionGroup;
                      CaptionML=[ENU=&Internal Movement;
                                 PTG=Movimento &Interno];
                      Image=CreateMovement }
      { 19      ;2   ;Action    ;
                      CaptionML=[ENU=Co&mments;
                                 PTG=Co&ment rios];
                      RunObject=Page 5776;
                      RunPageLink=Table Name=CONST(Internal Movement),
                                  Type=CONST(" "),
                                  No.=FIELD(No.);
                      Image=ViewComments }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 2   ;2   ;Field     ;
                ToolTipML=[ENU=There are a number of tables and fields that are not currently documented. There is no specific Help for these tables and fields.;
                           PTG=""];
                SourceExpr="No." }

    { 4   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the code of the location where the internal movement is being performed.;
                           PTG=""];
                SourceExpr="Location Code" }

    { 6   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the bin where you want items on this internal movement to be placed when they are picked.;
                           PTG=""];
                SourceExpr="To Bin Code" }

    { 8   ;2   ;Field     ;
                ToolTipML=[ENU=There are a number of tables and fields that are not currently documented. There is no specific Help for these tables and fields.;
                           PTG=""];
                SourceExpr="Due Date" }

    { 10  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the ID of the user who is responsible for the document.;
                           PTG=""];
                SourceExpr="Assigned User ID" }

    { 12  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the method by which the internal movements are sorted.;
                           PTG=""];
                SourceExpr="Sorting Method" }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                Visible=TRUE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {

    BEGIN
    END.
  }
}

