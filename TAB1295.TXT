OBJECT Table 1295 Posted Payment Recon. Hdr
{
  OBJECT-PROPERTIES
  {
    Date=15/09/15;
    Time=12:00:00;
    Version List=NAVW19.00;
  }
  PROPERTIES
  {
    DataCaptionFields=Bank Account No.,Statement No.;
    OnDelete=BEGIN
               IF NOT CONFIRM(HasBankEntriesQst,FALSE,"Bank Account No.","Statement No.") THEN
                 ERROR('');
               CODEUNIT.RUN(CODEUNIT::"BankPaymentApplLines-Delete",Rec);
             END;

    CaptionML=[ENU=Posted Payment Recon. Hdr;
               PTG=Hist. Cab. Pagamentos Reconcilia��o];
    LookupPageID=Page388;
  }
  FIELDS
  {
    { 1   ;   ;Bank Account No.    ;Code20        ;TableRelation="Bank Account";
                                                   CaptionML=[ENU=Bank Account No.;
                                                              PTG=N� Conta Banc�ria];
                                                   NotBlank=Yes }
    { 2   ;   ;Statement No.       ;Code20        ;CaptionML=[ENU=Statement No.;
                                                              PTG=N� Extrato];
                                                   NotBlank=Yes }
    { 3   ;   ;Statement Ending Balance;Decimal   ;CaptionML=[ENU=Statement Ending Balance;
                                                              PTG=Balan�o Final Extrato];
                                                   AutoFormatType=1;
                                                   AutoFormatExpr=GetCurrencyCode }
    { 4   ;   ;Statement Date      ;Date          ;CaptionML=[ENU=Statement Date;
                                                              PTG=Data Extrato] }
    { 6   ;   ;Bank Statement      ;BLOB          ;CaptionML=[ENU=Bank Statement;
                                                              PTG=Extrato Banc�rio] }
  }
  KEYS
  {
    {    ;Bank Account No.,Statement No.          ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {
    VAR
      HasBankEntriesQst@1001 : TextConst 'ENU=One or more bank account ledger entries in bank account %1 have been reconciled for bank account statement %2, and contain information about the bank statement. These bank ledger entries will not be modified if you delete bank account statement %2.\\Do you want to continue?;PTG=Um ou mais movimentos de conta banc�ria na conta banc�ria %1 foram reconciliados pelo extrato banc�rio %2, e cont�m informa��o sobre o extrato banc�rio. Estes movimentos de conta banc�ria n�o ser�o modificados se eliminar o extrato banc�rio %2.\\ Deseja continuar?';

    LOCAL PROCEDURE GetCurrencyCode@1() : Code[10];
    VAR
      BankAcc2@1000 : Record 270;
    BEGIN
      IF "Bank Account No." = BankAcc2."No." THEN
        EXIT(BankAcc2."Currency Code");

      IF BankAcc2.GET("Bank Account No.") THEN
        EXIT(BankAcc2."Currency Code");

      EXIT('');
    END;

    BEGIN
    END.
  }
}

