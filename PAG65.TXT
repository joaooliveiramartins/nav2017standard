OBJECT Page 65 Rounding Methods
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Rounding Methods;
               PTG=M�todos Arredondamento];
    SourceTable=Table42;
    PageType=List;
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 2   ;2   ;Field     ;
                ToolTipML=ENU=Specifies a code for the rounding method for item prices.;
                SourceExpr=Code }

    { 4   ;2   ;Field     ;
                ToolTipML=ENU=Specifies the minimum amount to round.;
                SourceExpr="Minimum Amount" }

    { 6   ;2   ;Field     ;
                ToolTipML=ENU=Specifies an amount to add before it is rounded.;
                SourceExpr="Amount Added Before" }

    { 8   ;2   ;Field     ;
                ToolTipML=ENU=Specifies how to round.;
                SourceExpr=Type }

    { 10  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the size of the interval that you want between rounded amounts.;
                SourceExpr=Precision }

    { 12  ;2   ;Field     ;
                ToolTipML=ENU=Specifies an amount to add, after the amount has been rounded.;
                SourceExpr="Amount Added After" }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {

    BEGIN
    END.
  }
}

