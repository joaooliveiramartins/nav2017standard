OBJECT Table 5942 Service Item Log
{
  OBJECT-PROPERTIES
  {
    Date=07/09/12;
    Time=12:00:00;
    Version List=NAVW17.00;
  }
  PROPERTIES
  {
    DataCaptionFields=Service Item No.;
    OnInsert=BEGIN
               ServItemLog.LOCKTABLE;
               ServItemLog.RESET;
               ServItemLog.SETRANGE("Service Item No.","Service Item No.");
               IF ServItemLog.FINDLAST THEN
                 "Entry No." := ServItemLog."Entry No." + 1
               ELSE
                 "Entry No." := 1;

               "Change Date" := TODAY;
               "Change Time" := TIME;
               "User ID" := USERID;
             END;

    CaptionML=[ENU=Service Item Log;
               PTG=Altera��es Produto Servi�o];
    LookupPageID=Page5989;
    DrillDownPageID=Page5989;
  }
  FIELDS
  {
    { 1   ;   ;Service Item No.    ;Code20        ;TableRelation="Service Item";
                                                   CaptionML=[ENU=Service Item No.;
                                                              PTG=N� Produto Servi�o];
                                                   NotBlank=Yes }
    { 2   ;   ;Entry No.           ;Integer       ;CaptionML=[ENU=Entry No.;
                                                              PTG=N� Mov.] }
    { 3   ;   ;Event No.           ;Integer       ;CaptionML=[ENU=Event No.;
                                                              PTG=N� Evento] }
    { 4   ;   ;Document No.        ;Code20        ;TableRelation=IF (Document Type=CONST(Quote)) "Service Header".No. WHERE (Document Type=CONST(Quote))
                                                                 ELSE IF (Document Type=CONST(Order)) "Service Header".No. WHERE (Document Type=CONST(Order))
                                                                 ELSE IF (Document Type=CONST(Contract)) "Service Contract Header"."Contract No." WHERE (Contract Type=CONST(Contract));
                                                   TestTableRelation=No;
                                                   CaptionML=[ENU=Document No.;
                                                              PTG=N� Documento] }
    { 5   ;   ;After               ;Text50        ;CaptionML=[ENU=After;
                                                              PTG=Ap�s] }
    { 6   ;   ;Before              ;Text50        ;CaptionML=[ENU=Before;
                                                              PTG=Antes] }
    { 7   ;   ;Change Date         ;Date          ;CaptionML=[ENU=Change Date;
                                                              PTG=Altera��o data] }
    { 8   ;   ;Change Time         ;Time          ;CaptionML=[ENU=Change Time;
                                                              PTG=Hora Altera��o] }
    { 9   ;   ;User ID             ;Code50        ;TableRelation=User."User Name";
                                                   OnLookup=VAR
                                                              UserMgt@1000 : Codeunit 418;
                                                            BEGIN
                                                              UserMgt.LookupUserID("User ID");
                                                            END;

                                                   TestTableRelation=No;
                                                   CaptionML=[ENU=User ID;
                                                              PTG=ID Utilizador] }
    { 10  ;   ;Document Type       ;Option        ;CaptionML=[ENU=Document Type;
                                                              PTG=Tipo Documento];
                                                   OptionCaptionML=[ENU=" ,Quote,Order,Contract";
                                                                    PTG=" ,Proposta,Ordem,Contrato"];
                                                   OptionString=[ ,Quote,Order,Contract] }
  }
  KEYS
  {
    {    ;Service Item No.,Entry No.              ;Clustered=Yes }
    {    ;Change Date                              }
  }
  FIELDGROUPS
  {
  }
  CODE
  {
    VAR
      ServItemLog@1000 : Record 5942;

    BEGIN
    END.
  }
}

