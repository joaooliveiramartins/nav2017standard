OBJECT Page 5874 BOM Warning Log
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0;
  }
  PROPERTIES
  {
    Editable=No;
    CaptionML=[ENU=BOM Warning Log;
               PTG=Registo Avisos LM];
    SourceTable=Table5874;
    PageType=List;
    ActionList=ACTIONS
    {
      { 7       ;0   ;ActionContainer;
                      ActionContainerType=ActionItems }
      { 6       ;1   ;Action    ;
                      CaptionML=[ENU=&Show;
                                 PTG=&Mostrar];
                      Promoted=Yes;
                      Image=View;
                      PromotedCategory=Process;
                      OnAction=BEGIN
                                 ShowWarning;
                               END;
                                }
    }
  }
  CONTROLS
  {
    { 1   ;0   ;Container ;
                ContainerType=ContentArea }

    { 2   ;1   ;Group     ;
                Name=Group;
                GroupType=Repeater }

    { 3   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the description of the warning associated with the entry.;
                           PTG=""];
                SourceExpr="Warning Description" }

    { 4   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the table ID associated with the entry.;
                           PTG=""];
                SourceExpr="Table ID";
                Visible=FALSE }

    { 5   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the table position associated with the entry.;
                           PTG=""];
                SourceExpr="Table Position";
                Visible=FALSE }

  }
  CODE
  {

    BEGIN
    END.
  }
}

