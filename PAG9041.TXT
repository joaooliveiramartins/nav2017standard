OBJECT Page 9041 Shop Supervisor Activities
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Activities;
               PTG=Atividades];
    SourceTable=Table9056;
    PageType=CardPart;
    RefreshOnActivate=Yes;
    OnOpenPage=BEGIN
                 RESET;
                 IF NOT GET THEN BEGIN
                   INIT;
                   INSERT;
                 END;
               END;

  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 8   ;1   ;Group     ;
                CaptionML=[ENU=Production Orders;
                           PTG=Ordens Produ��o];
                GroupType=CueGroup;
                ActionList=ACTIONS
                {
                  { 22      ;    ;Action    ;
                                  CaptionML=[ENU=Change Production Order Status;
                                             PTG=Alterar Estado Ordem Produ��o];
                                  RunObject=Page 99000914;
                                  Image=ChangeStatus }
                  { 2       ;    ;Action    ;
                                  CaptionML=[ENU=Update Unit Cost;
                                             PTG=Atualizar Custo Unit�rio];
                                  RunObject=Report 99001014;
                                  Image=UpdateUnitCost }
                  { 27      ;    ;Action    ;
                                  CaptionML=[ENU=Navigate;
                                             PTG=Navegar];
                                  ToolTipML=ENU=View and link to all entries that exist for the document number on the selected line.;
                                  RunObject=Page 344;
                                  Image=Navigate }
                }
                 }

    { 1   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the number of planned production orders that are displayed in the Manufacturing Cue on the Role Center. The documents are filtered by today's date.;
                           PTG=""];
                SourceExpr="Planned Prod. Orders - All";
                DrillDownPageID=Planned Production Orders }

    { 4   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the number of firm planned production orders that are displayed in the Manufacturing Cue on the Role Center. The documents are filtered by today's date.;
                           PTG=""];
                SourceExpr="Firm Plan. Prod. Orders - All";
                DrillDownPageID=Firm Planned Prod. Orders }

    { 6   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the number of released production orders that are displayed in the Manufacturing Cue on the Role Center. The documents are filtered by today's date.;
                           PTG=""];
                SourceExpr="Released Prod. Orders - All";
                DrillDownPageID=Released Production Orders }

    { 9   ;1   ;Group     ;
                CaptionML=[ENU=Operations;
                           PTG=Opera��es];
                GroupType=CueGroup;
                ActionList=ACTIONS
                {
                  { 19      ;    ;Action    ;
                                  CaptionML=[ENU=Consumption Journal;
                                             PTG=Di�rio Consumo];
                                  RunObject=Page 99000846;
                                  Image=ConsumptionJournal }
                  { 20      ;    ;Action    ;
                                  CaptionML=[ENU=Output Journal;
                                             PTG=Di�rio Sa�da];
                                  RunObject=Page 99000823;
                                  Image=OutputJournal }
                }
                 }

    { 11  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the number of production order routings in queue that are displayed in the Manufacturing Cue on the Role Center. The documents are filtered by today's date.;
                           PTG=""];
                SourceExpr="Prod. Orders Routings-in Queue";
                DrillDownPageID=Prod. Order Routing }

    { 13  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the number of inactive service orders that are displayed in the Service Cue on the Role Center. The documents are filtered by today's date.;
                           PTG=""];
                SourceExpr="Prod. Orders Routings-in Prog.";
                DrillDownPageID=Prod. Order Routing }

    { 10  ;1   ;Group     ;
                CaptionML=[ENU=Warehouse Documents;
                           PTG=Documentos Armaz�m];
                GroupType=CueGroup }

    { 21  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the number of inventory picks that are displayed in the Manufacturing Cue on the Role Center. The documents are filtered by today's date.;
                           PTG=""];
                SourceExpr="Invt. Picks to Production";
                DrillDownPageID=Inventory Picks }

    { 17  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the number of inventory put-always from production that are displayed in the Manufacturing Cue on the Role Center. The documents are filtered by today's date.;
                           PTG=""];
                SourceExpr="Invt. Put-aways from Prod.";
                DrillDownPageID=Inventory Put-aways }

  }
  CODE
  {

    BEGIN
    END.
  }
}

