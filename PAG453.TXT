OBJECT Page 453 Issued Fin. Charge Memo Stat.
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0;
  }
  PROPERTIES
  {
    Editable=No;
    CaptionML=[ENU=Issued Fin. Charge Memo Stat.;
               PTG=Estat�stica Nota Juros Emitida];
    LinksAllowed=No;
    SourceTable=Table304;
    PageType=Card;
    OnAfterGetRecord=VAR
                       CustPostingGr@1003 : Record 92;
                       GLAcc@1002 : Record 15;
                       VATPostingSetup@1001 : Record 325;
                       VATInterest@1000 : Decimal;
                     BEGIN
                       CALCFIELDS("Interest Amount","VAT Amount");
                       FinChrgMemoTotal := "Additional Fee" + "Interest Amount" + "VAT Amount";
                       CustPostingGr.GET("Customer Posting Group");
                       CustPostingGr.TESTFIELD("Interest Account");
                       GLAcc.GET(CustPostingGr."Interest Account");
                       VATPostingSetup.GET("VAT Bus. Posting Group",GLAcc."VAT Prod. Posting Group");
                       VATInterest := VATPostingSetup."VAT %";
                       GLAcc.GET(CustPostingGr."Additional Fee Account");
                       VATPostingSetup.GET("VAT Bus. Posting Group",GLAcc."VAT Prod. Posting Group");
                       Interest := (FinChrgMemoTotal - "Additional Fee" * (VATPostingSetup."VAT %" / 100 + 1)) /
                         (VATInterest / 100 + 1);
                       VatAmount := Interest * VATInterest / 100 +
                         "Additional Fee" * VATPostingSetup."VAT %" / 100;

                       IF Cust.GET("Customer No.") THEN
                         Cust.CALCFIELDS("Balance (LCY)")
                       ELSE
                         CLEAR(Cust);
                       IF Cust."Credit Limit (LCY)" = 0 THEN
                         CreditLimitLCYExpendedPct := 0
                       ELSE
                         CreditLimitLCYExpendedPct := ROUND(Cust."Balance (LCY)" / Cust."Credit Limit (LCY)" * 10000,1);
                     END;

  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 3   ;1   ;Group     ;
                CaptionML=[ENU=General;
                           PTG=Geral] }

    { 13  ;2   ;Field     ;
                DrillDown=No;
                CaptionML=[ENU=Interest Amount;
                           PTG=Valor Juros];
                SourceExpr=Interest }

    { 1   ;2   ;Field     ;
                DrillDown=No;
                ToolTipML=ENU=Specifies the total of the additional fee amounts on the finance charge memo lines.;
                SourceExpr="Additional Fee" }

    { 4   ;2   ;Field     ;
                DrillDown=No;
                CaptionML=[ENU=VAT Amount;
                           PTG=Valor IVA];
                SourceExpr=VatAmount }

    { 6   ;2   ;Field     ;
                CaptionML=[ENU=Total;
                           PTG=Total];
                SourceExpr=FinChrgMemoTotal;
                AutoFormatType=1;
                AutoFormatExpr="Currency Code" }

    { 1903289601;1;Group  ;
                CaptionML=[ENU=Customer;
                           PTG=Cliente] }

    { 11  ;2   ;Field     ;
                CaptionML=[ENU=Balance (LCY);
                           PTG=Saldo (DL)];
                SourceExpr=Cust."Balance (LCY)";
                AutoFormatType=1 }

    { 8   ;2   ;Field     ;
                CaptionML=[ENU=Credit Limit (LCY);
                           PTG=Limite Cr�dito (DL)];
                SourceExpr=Cust."Credit Limit (LCY)";
                AutoFormatType=1 }

    { 10  ;2   ;Field     ;
                ExtendedDatatype=Ratio;
                CaptionML=[ENU=Expended % of Credit Limit (LCY);
                           PTG=% Limite Cr�dito Usado (DL)];
                ToolTipML=[ENU=Specifies the expended percentage of the credit limit in (LCY).;
                           PTG=Especifica a percentagem gasta do limite de cr�dito em (DL).];
                SourceExpr=CreditLimitLCYExpendedPct }

  }
  CODE
  {
    VAR
      Cust@1000 : Record 18;
      FinChrgMemoTotal@1001 : Decimal;
      CreditLimitLCYExpendedPct@1002 : Decimal;
      Interest@1004 : Decimal;
      VatAmount@1003 : Decimal;

    BEGIN
    END.
  }
}

