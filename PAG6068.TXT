OBJECT Page 6068 Contract Gain/Loss (Reasons)
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Contract Gain/Loss (Reasons);
               PTG=Ganhos/Perdas Contrato (Raz�es)];
    SaveValues=Yes;
    InsertAllowed=No;
    DeleteAllowed=No;
    LinksAllowed=No;
    SourceTable=Table2000000007;
    DataCaptionExpr='';
    PageType=Card;
    RefreshOnActivate=Yes;
    OnOpenPage=BEGIN
                 IF PeriodStart = 0D THEN
                   PeriodStart := WORKDATE;
                 MATRIX_GenerateColumnCaptions(SetWanted::Initial);
               END;

    OnFindRecord=BEGIN
                   EXIT(TRUE);
                 END;

    ActionList=ACTIONS
    {
      { 1900000004;0 ;ActionContainer;
                      ActionContainerType=ActionItems }
      { 77      ;1   ;Action    ;
                      Name=ShowMatrix;
                      CaptionML=[ENU=&Show Matrix;
                                 PTG=&Mostrar Matriz];
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=ShowMatrix;
                      PromotedCategory=Process;
                      OnAction=VAR
                                 MatrixForm@1098 : Page 9263;
                               BEGIN
                                 IF PeriodStart = 0D THEN
                                   PeriodStart := WORKDATE;
                                 CLEAR(MatrixForm);

                                 MatrixForm.Load(MATRIX_CaptionSet,MatrixRecords,MATRIX_CurrentNoOfColumns,AmountType,PeriodType,
                                   ReasonFilter,PeriodStart);
                                 MatrixForm.RUNMODAL;
                               END;
                                }
      { 1112    ;1   ;Action    ;
                      CaptionML=[ENU=Next Set;
                                 PTG=Conjunto Seguinte];
                      ToolTipML=[ENU=Go to the next set of data.;
                                 PTG=Ir para o conjunto de dados seguintes.];
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=NextSet;
                      PromotedCategory=Process;
                      OnAction=BEGIN
                                 MATRIX_GenerateColumnCaptions(SetWanted::Next);
                               END;
                                }
      { 1110    ;1   ;Action    ;
                      CaptionML=[ENU=Previous Set;
                                 PTG=Conjunto Anterior];
                      ToolTipML=[ENU=Go to the previous set of data.;
                                 PTG=Ir para o conjunto de dados anterior.];
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=PreviousSet;
                      PromotedCategory=Process;
                      OnAction=BEGIN
                                 MATRIX_GenerateColumnCaptions(SetWanted::Previous);
                               END;
                                }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 22  ;1   ;Group     ;
                CaptionML=[ENU=General;
                           PTG=Geral] }

    { 15  ;2   ;Field     ;
                CaptionML=[ENU=Period Start;
                           PTG=In�cio Per�odo];
                SourceExpr=PeriodStart }

    { 1907524401;1;Group  ;
                CaptionML=[ENU=Filters;
                           PTG=Filtros] }

    { 8   ;2   ;Field     ;
                CaptionML=[ENU=Reason Code Filter;
                           PTG=Filtro C�d. Raz�o];
                SourceExpr=ReasonFilter;
                OnValidate=BEGIN
                             MATRIX_GenerateColumnCaptions(SetWanted::Initial);
                             ReasonFilterOnAfterValidate;
                           END;

                OnLookup=BEGIN
                           ReasonCode.RESET;
                           IF PAGE.RUNMODAL(0,ReasonCode) = ACTION::LookupOK THEN BEGIN
                             Text := ReasonCode.Code;
                             EXIT(TRUE);
                           END;

                           MATRIX_GenerateColumnCaptions(SetWanted::Initial);
                         END;
                          }

    { 1906098301;1;Group  ;
                CaptionML=[ENU=Matrix Options;
                           PTG=Op��es de Matriz] }

    { 1107;2   ;Field     ;
                CaptionML=[ENU=View by;
                           PTG=Ver por];
                ToolTipML=[ENU=Specifies by which period amounts are displayed.;
                           PTG=""];
                OptionCaptionML=[ENU=Day,Week,Month,Quarter,Year;
                                 PTG=Dia,Semana,M�s,Trimestre,Ano];
                SourceExpr=PeriodType }

    { 1108;2   ;Field     ;
                CaptionML=[ENU=View as;
                           PTG=Ver como];
                ToolTipML=[ENU=Specifies how amounts are displayed. Net Change: The net change in the balance for the selected period. Balance at Date: The balance as of the last day in the selected period.;
                           PTG=""];
                OptionCaptionML=[ENU=Net Change,Balance at Date;
                                 PTG=Saldo Per�odo,Saldo � Data];
                SourceExpr=AmountType }

    { 1109;2   ;Field     ;
                CaptionML=[ENU=Column Set;
                           PTG=Conjunto de Colunas];
                SourceExpr=MATRIX_CaptionRange;
                Editable=FALSE }

  }
  CODE
  {
    VAR
      MatrixRecords@1099 : ARRAY [32] OF Record 231;
      MatrixRecord@1019 : Record 231;
      ReasonCode@1002 : Record 231;
      MATRIX_CaptionSet@1100 : ARRAY [32] OF Text[1024];
      MATRIX_CaptionRange@1101 : Text[1024];
      PKFirstRecInCurrSet@1105 : Text[1024];
      MATRIX_CurrentNoOfColumns@1106 : Integer;
      AmountType@1004 : 'Net Change,Balance at Date';
      PeriodType@1005 : 'Day,Week,Month,Quarter,Year';
      ReasonFilter@1006 : Text[250];
      PeriodStart@1008 : Date;
      SetWanted@1021 : 'Initial,Previous,Same,Next';

    LOCAL PROCEDURE MATRIX_GenerateColumnCaptions@1107(SetWanted@1001 : 'First,Previous,Same,Next');
    VAR
      MatrixMgt@1003 : Codeunit 9200;
      RecRef@1002 : RecordRef;
      CurrentMatrixRecordOrdinal@1000 : Integer;
    BEGIN
      CLEAR(MATRIX_CaptionSet);
      CLEAR(MatrixRecords);
      CurrentMatrixRecordOrdinal := 1;
      IF ReasonFilter <> '' THEN
        MatrixRecord.SETFILTER(Code,ReasonFilter)
      ELSE
        MatrixRecord.SETRANGE(Code);
      RecRef.GETTABLE(MatrixRecord);
      RecRef.SETTABLE(MatrixRecord);

      MatrixMgt.GenerateMatrixData(RecRef,SetWanted,ARRAYLEN(MatrixRecords),1,PKFirstRecInCurrSet,
        MATRIX_CaptionSet,MATRIX_CaptionRange,MATRIX_CurrentNoOfColumns);
      IF MATRIX_CurrentNoOfColumns > 0 THEN BEGIN
        MatrixRecord.SETPOSITION(PKFirstRecInCurrSet);
        MatrixRecord.FIND;
        REPEAT
          MatrixRecords[CurrentMatrixRecordOrdinal].COPY(MatrixRecord);
          CurrentMatrixRecordOrdinal := CurrentMatrixRecordOrdinal + 1;
        UNTIL (CurrentMatrixRecordOrdinal > MATRIX_CurrentNoOfColumns) OR (MatrixRecord.NEXT <> 1);
      END;
    END;

    LOCAL PROCEDURE ReasonFilterOnAfterValidate@19066029();
    BEGIN
      CurrPage.UPDATE(TRUE);
    END;

    BEGIN
    END.
  }
}

