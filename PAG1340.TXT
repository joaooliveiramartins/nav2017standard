OBJECT Page 1340 Config Templates
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Templates;
               PTG=Modelos];
    InsertAllowed=No;
    DeleteAllowed=No;
    ModifyAllowed=No;
    SourceTable=Table8618;
    PageType=List;
    PromotedActionCategoriesML=[ENU=New,Process,Report,Manage;
                                PTG=Novo,Processar,Mapas,Gerir];
    OnOpenPage=VAR
                 FilterValue@1000 : Text;
               BEGIN
                 FilterValue := GETFILTER("Table ID");

                 IF NOT EVALUATE(FilteredTableId,FilterValue) THEN
                   FilteredTableId := 0;

                 UpdateActionsVisibility;
                 UpdatePageCaption;

                 IF NewMode THEN
                   UpdateSelection;
               END;

    OnDeleteRecord=BEGIN
                     CASE "Table ID" OF
                       DATABASE::Customer,
                       DATABASE::Item:
                         ConfigTemplateManagement.DeleteRelatedTemplates(Code,DATABASE::"Default Dimension");
                     END;
                   END;

    OnQueryClosePage=BEGIN
                       IF NewMode AND (CloseAction = ACTION::LookupOK) THEN
                         SaveSelection;
                     END;

    ActionList=ACTIONS
    {
      { 5       ;    ;ActionContainer;
                      Name=NewDocumentItems;
                      CaptionML=[ENU=New Document Items;
                                 PTG=Novos Documentos];
                      ActionContainerType=NewDocumentItems }
      { 6       ;1   ;Action    ;
                      Name=NewCustomerTemplate;
                      CaptionML=[ENU=New;
                                 PTG=Novo];
                      ToolTipML=[ENU=Create a new template for a customer card.;
                                 PTG=Criar um novo modelo para uma ficha de cliente. Criar um novo modelo de ficha de fornecedor. Criar um novo modelo de ficha de produto.];
                      ApplicationArea=#All;
                      RunObject=Page 1341;
                      Visible=CreateCustomerActionVisible;
                      Image=NewDocument;
                      RunPageMode=Create }
      { 3       ;1   ;Action    ;
                      Name=NewVendorTemplate;
                      CaptionML=[ENU=New;
                                 PTG=Novo];
                      ToolTipML=[ENU=Create a new template for a vendor card.;
                                 PTG=Criar um novo modelo para uma ficha de fornecedor.];
                      ApplicationArea=#All;
                      RunObject=Page 1344;
                      Visible=CreateVendorActionVisible;
                      Image=NewDocument;
                      RunPageMode=Create }
      { 8       ;1   ;Action    ;
                      Name=NewItemTemplate;
                      CaptionML=[ENU=New;
                                 PTG=Novo];
                      ToolTipML=[ENU=Create a new template for an item card.;
                                 PTG=Criar um novo modelo para uma ficha de produto.];
                      ApplicationArea=#All;
                      RunObject=Page 1342;
                      Visible=CreateItemActionVisible;
                      Image=NewDocument;
                      RunPageMode=Create }
      { 9       ;1   ;Action    ;
                      Name=NewConfigTemplate;
                      CaptionML=[ENU=New;
                                 PTG=Novo];
                      ToolTipML=ENU=Create a new configuration template.;
                      ApplicationArea=#All;
                      RunObject=Page 8618;
                      Visible=CreateConfigurationTemplateActionVisible;
                      Image=NewDocument;
                      RunPageMode=Create }
      { 7       ;    ;ActionContainer;
                      ActionContainerType=ActionItems }
      { 10      ;1   ;Action    ;
                      Name=Edit Template;
                      ShortCutKey=Return;
                      CaptionML=[ENU=Edit;
                                 PTG=Editar];
                      ToolTipML=[ENU=Edit the selected template.;
                                 PTG=Editar o modelo selecionado];
                      ApplicationArea=#Basic,#Suite;
                      Image=Edit;
                      Scope=Repeater;
                      OnAction=VAR
                                 TempMiniCustomerTemplate@1000 : TEMPORARY Record 1300;
                                 TempItemTemplate@1001 : TEMPORARY Record 1301;
                                 TempMiniVendorTemplate@1002 : TEMPORARY Record 1303;
                               BEGIN
                                 CASE "Table ID" OF
                                   DATABASE::Customer:
                                     BEGIN
                                       TempMiniCustomerTemplate.InitializeTempRecordFromConfigTemplate(TempMiniCustomerTemplate,Rec);
                                       PAGE.RUN(PAGE::"Cust. Template Card",TempMiniCustomerTemplate);
                                     END;
                                   DATABASE::Item:
                                     BEGIN
                                       TempItemTemplate.InitializeTempRecordFromConfigTemplate(TempItemTemplate,Rec);
                                       PAGE.RUN(PAGE::"Item Template Card",TempItemTemplate);
                                     END;
                                   DATABASE::Vendor:
                                     BEGIN
                                       TempMiniVendorTemplate.InitializeTempRecordFromConfigTemplate(TempMiniVendorTemplate,Rec);
                                       PAGE.RUN(PAGE::"Vendor Template Card",TempMiniVendorTemplate);
                                     END;
                                   ELSE
                                     PAGE.RUN(PAGE::"Config. Template Header",Rec);
                                 END;
                               END;
                                }
      { 13      ;1   ;Action    ;
                      Name=Delete;
                      CaptionML=[ENU=Delete;
                                 PTG=Eliminar];
                      ToolTipML=ENU=Delete the selected template.;
                      ApplicationArea=#Basic,#Suite;
                      Image=Delete;
                      OnAction=BEGIN
                                 IF CONFIRM(STRSUBSTNO(DeleteQst,Code)) THEN
                                   DELETE(TRUE);
                               END;
                                }
    }
  }
  CONTROLS
  {
    { 1   ;    ;Container ;
                ContainerType=ContentArea }

    { 2   ;1   ;Group     ;
                Name=Repeater;
                GroupType=Repeater }

    { 11  ;2   ;Field     ;
                Name=Template;
                ToolTipML=ENU=Specifies the configuration template.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr=Code }

    { 4   ;2   ;Field     ;
                Name=Template Name;
                ToolTipML=ENU=Specifies a description of the template.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr=Description }

    { 12  ;2   ;Field     ;
                ToolTipML=ENU=Specifies if the template is ready to be used;
                ApplicationArea=#Basic,#Suite;
                SourceExpr=Enabled;
                Visible=NOT NewMode }

  }
  CODE
  {
    VAR
      ConfigTemplateManagement@1007 : Codeunit 8612;
      CreateCustomerActionVisible@1000 : Boolean;
      CreateVendorActionVisible@1011 : Boolean;
      CreateItemActionVisible@1001 : Boolean;
      CreateConfigurationTemplateActionVisible@1002 : Boolean;
      NewMode@1014 : Boolean;
      FilteredTableId@1003 : Integer;
      ConfigurationTemplatesCap@1006 : TextConst 'ENU=Configuration Templates;PTG=Configura��o Modelos';
      CustomerTemplatesCap@1005 : TextConst 'ENU=Customer Templates;PTG=Modelos Clientes';
      VendorTemplatesCap@1013 : TextConst 'ENU=Vendor Templates;PTG=Modelos Fornecedor';
      ItemTemplatesCap@1004 : TextConst 'ENU=Item Templates;PTG=Modelos Produto';
      SelectConfigurationTemplatesCap@1010 : TextConst 'ENU=Select a template;PTG=Selecionar um modelo';
      SelectCustomerTemplatesCap@1009 : TextConst 'ENU=Select a template for a new customer;PTG=Selecionar um modelo para um novo cliente';
      SelectVendorTemplatesCap@1012 : TextConst 'ENU=Select a template for a new vendor;PTG=Selecionar um modelo para um novo fornecedor';
      SelectItemTemplatesCap@1008 : TextConst 'ENU=Select a template for a new item;PTG=Selecionar um modelo para um novo produto';
      DeleteQst@1015 : TextConst '@@@=%1 - configuration template code;ENU=Delete %1?;PTG=Eliminar %1?';

    LOCAL PROCEDURE UpdateActionsVisibility@3();
    BEGIN
      CreateCustomerActionVisible := FALSE;
      CreateItemActionVisible := FALSE;
      CreateConfigurationTemplateActionVisible := FALSE;
      CreateVendorActionVisible := FALSE;

      CASE FilteredTableId OF
        DATABASE::Customer:
          CreateCustomerActionVisible := TRUE;
        DATABASE::Item:
          CreateItemActionVisible := TRUE;
        DATABASE::Vendor:
          CreateVendorActionVisible := TRUE;
        ELSE
          CreateConfigurationTemplateActionVisible := TRUE;
      END;
    END;

    LOCAL PROCEDURE UpdatePageCaption@1();
    VAR
      PageCaption@1000 : Text;
    BEGIN
      IF NOT NewMode THEN
        CASE FilteredTableId OF
          DATABASE::Customer:
            PageCaption := CustomerTemplatesCap;
          DATABASE::Vendor:
            PageCaption := VendorTemplatesCap;
          DATABASE::Item:
            PageCaption := ItemTemplatesCap;
          ELSE
            PageCaption := ConfigurationTemplatesCap;
        END
      ELSE
        CASE FilteredTableId OF
          DATABASE::Customer:
            PageCaption := SelectCustomerTemplatesCap;
          DATABASE::Vendor:
            PageCaption := SelectVendorTemplatesCap;
          DATABASE::Item:
            PageCaption := SelectItemTemplatesCap;
          ELSE
            PageCaption := SelectConfigurationTemplatesCap;
        END;

      CurrPage.CAPTION(PageCaption);
    END;

    LOCAL PROCEDURE UpdateSelection@2();
    VAR
      ConfigTemplateHeader@1002 : Record 8618;
      TemplateSelectionMgt@1000 : Codeunit 1900;
      TemplateCode@1001 : Code[10];
    BEGIN
      CASE FilteredTableId OF
        DATABASE::Customer:
          TemplateSelectionMgt.GetLastCustTemplateSelection(TemplateCode);
        DATABASE::Vendor:
          TemplateSelectionMgt.GetLastVendorTemplateSelection(TemplateCode);
        DATABASE::Item:
          TemplateSelectionMgt.GetLastItemTemplateSelection(TemplateCode);
      END;

      IF NOT (TemplateCode = '') THEN
        IF ConfigTemplateHeader.GET(TemplateCode) THEN
          SETPOSITION(ConfigTemplateHeader.GETPOSITION);
    END;

    LOCAL PROCEDURE SaveSelection@4();
    VAR
      TemplateSelectionMgt@1000 : Codeunit 1900;
    BEGIN
      CASE FilteredTableId OF
        DATABASE::Customer:
          TemplateSelectionMgt.SaveCustTemplateSelectionForCurrentUser(Code);
        DATABASE::Vendor:
          TemplateSelectionMgt.SaveVendorTemplateSelectionForCurrentUser(Code);
        DATABASE::Item:
          TemplateSelectionMgt.SaveItemTemplateSelectionForCurrentUser(Code);
      END;
    END;

    PROCEDURE SetNewMode@5();
    BEGIN
      NewMode := TRUE;
    END;

    BEGIN
    END.
  }
}

