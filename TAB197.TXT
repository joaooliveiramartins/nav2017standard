OBJECT Table 197 Acc. Sched. KPI Buffer
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Acc. Sched. KPI Buffer;
               PTG=C�digo];
  }
  FIELDS
  {
    { 1   ;   ;No.                 ;Integer       ;CaptionML=[ENU=No.;
                                                              PTG=N�] }
    { 2   ;   ;Date                ;Date          ;CaptionML=[ENU=Date;
                                                              PTG=Data] }
    { 3   ;   ;Closed Period       ;Boolean       ;CaptionML=[ENU=Closed Period;
                                                              PTG=Per�odo fechado] }
    { 4   ;   ;Account Schedule Name;Code10       ;CaptionML=[ENU=Account Schedule Name;
                                                              PTG=Nome Esquema de Contas] }
    { 5   ;   ;KPI Code            ;Code10        ;CaptionML=[ENU=KPI Code;
                                                              PTG=C�d. KPI] }
    { 6   ;   ;KPI Name            ;Text50        ;CaptionML=[ENU=KPI Name;
                                                              PTG=Nome KPI] }
    { 7   ;   ;Net Change Actual   ;Decimal       ;CaptionML=[ENU=Net Change Actual;
                                                              PTG=Saldo Per�odo] }
    { 8   ;   ;Balance at Date Actual;Decimal     ;CaptionML=[ENU=Balance at Date Actual;
                                                              PTG=Saldo � Data] }
    { 9   ;   ;Net Change Budget   ;Decimal       ;CaptionML=[ENU=Net Change Budget;
                                                              PTG=Saldo Per�odo Or�amento] }
    { 10  ;   ;Balance at Date Budget;Decimal     ;CaptionML=[ENU=Balance at Date Budget;
                                                              PTG=Saldo � Data Or�amento] }
    { 11  ;   ;Net Change Actual Last Year;Decimal;CaptionML=[ENU=Net Change Actual Last Year;
                                                              PTG=Saldo Per�odo �lt. Ano] }
    { 12  ;   ;Balance at Date Act. Last Year;Decimal;
                                                   CaptionML=[ENU=Balance at Date Act. Last Year;
                                                              PTG=Saldo � Data �lt. Ano] }
    { 13  ;   ;Net Change Budget Last Year;Decimal;CaptionML=[ENU=Net Change Budget Last Year;
                                                              PTG=Saldo Per�odo Or�amento �lt. Ano] }
    { 14  ;   ;Balance at Date Bud. Last Year;Decimal;
                                                   CaptionML=[ENU=Balance at Date Bud. Last Year;
                                                              PTG=Saldo � Data Or�amento �lt. Ano] }
    { 15  ;   ;Net Change Forecast ;Decimal       ;CaptionML=[ENU=Net Change Forecast;
                                                              PTG=Saldo Ped�odo] }
    { 16  ;   ;Balance at Date Forecast;Decimal   ;CaptionML=[ENU=Balance at Date Forecast;
                                                              PTG=Previs�o Saldo � Data] }
    { 17  ;   ;Dimension Set ID    ;Integer       ;CaptionML=[ENU=Dimension Set ID;
                                                              PTG=ID Conjunto Dimen��es] }
  }
  KEYS
  {
    {    ;No.                                     ;Clustered=Yes }
    {    ;Account Schedule Name,KPI Code,Dimension Set ID }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    PROCEDURE AddColumnValue@1(ColumnLayout@1000 : Record 334;Value@1001 : Decimal);
    BEGIN
      IF ColumnLayout."Column Type" = ColumnLayout."Column Type"::"Net Change" THEN
        IF ColumnLayout."Ledger Entry Type" = ColumnLayout."Ledger Entry Type"::Entries THEN
          IF FORMAT(ColumnLayout."Comparison Date Formula") = '-1Y' THEN
            "Net Change Actual Last Year" += Value
          ELSE
            "Net Change Actual" += Value
        ELSE
          IF FORMAT(ColumnLayout."Comparison Date Formula") = '-1Y' THEN
            "Net Change Budget Last Year" += Value
          ELSE
            "Net Change Budget" += Value
      ELSE
        IF ColumnLayout."Ledger Entry Type" = ColumnLayout."Ledger Entry Type"::Entries THEN
          IF FORMAT(ColumnLayout."Comparison Date Formula") = '-1Y' THEN
            "Balance at Date Act. Last Year" += Value
          ELSE
            "Balance at Date Actual" += Value
        ELSE
          IF FORMAT(ColumnLayout."Comparison Date Formula") = '-1Y' THEN
            "Balance at Date Bud. Last Year" += Value
          ELSE
            "Balance at Date Budget" += Value;
    END;

    PROCEDURE GetColumnValue@2(ColumnLayout@1000 : Record 334) Result : Decimal;
    BEGIN
      IF ColumnLayout."Column Type" = ColumnLayout."Column Type"::"Net Change" THEN
        IF ColumnLayout."Ledger Entry Type" = ColumnLayout."Ledger Entry Type"::Entries THEN
          IF FORMAT(ColumnLayout."Comparison Date Formula") = '-1Y' THEN
            Result := "Net Change Actual Last Year"
          ELSE
            Result := "Net Change Actual"
        ELSE
          IF FORMAT(ColumnLayout."Comparison Date Formula") = '-1Y' THEN
            Result := "Net Change Budget Last Year"
          ELSE
            Result := "Net Change Budget"
      ELSE
        IF ColumnLayout."Ledger Entry Type" = ColumnLayout."Ledger Entry Type"::Entries THEN
          IF FORMAT(ColumnLayout."Comparison Date Formula") = '-1Y' THEN
            Result := "Balance at Date Act. Last Year"
          ELSE
            Result := "Balance at Date Actual"
        ELSE
          IF FORMAT(ColumnLayout."Comparison Date Formula") = '-1Y' THEN
            Result := "Balance at Date Bud. Last Year"
          ELSE
            Result := "Balance at Date Budget";
      EXIT(Result)
    END;

    BEGIN
    END.
  }
}

