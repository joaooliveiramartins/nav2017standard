OBJECT Table 313 Inventory Setup
{
  OBJECT-PROPERTIES
  {
    Date=15/09/15;
    Time=12:00:00;
    Version List=NAVW19.00;
  }
  PROPERTIES
  {
    Permissions=TableData 5896=m;
    CaptionML=[ENU=Inventory Setup;
               PTG=Configura��o Invent�rio];
  }
  FIELDS
  {
    { 1   ;   ;Primary Key         ;Code10        ;CaptionML=[ENU=Primary Key;
                                                              PTG=Chave Prim�ria] }
    { 2   ;   ;Automatic Cost Posting;Boolean     ;OnValidate=VAR
                                                                GLSetup@1000 : Record 98;
                                                              BEGIN
                                                                IF "Automatic Cost Posting" THEN BEGIN
                                                                  IF GLSetup.GET THEN
                                                                    IF NOT GLSetup."Use Legacy G/L Entry Locking" THEN
                                                                      MESSAGE(Text006,
                                                                        FIELDCAPTION("Automatic Cost Posting"),
                                                                        "Automatic Cost Posting",
                                                                        GLSetup.FIELDCAPTION("Use Legacy G/L Entry Locking"),
                                                                        GLSetup.TABLECAPTION,
                                                                        GLSetup."Use Legacy G/L Entry Locking");
                                                                END;
                                                              END;

                                                   CaptionML=[ENU=Automatic Cost Posting;
                                                              PTG=Varia��o Exist�ncias Autom�tica] }
    { 3   ;   ;Location Mandatory  ;Boolean       ;AccessByPermission=TableData 14=R;
                                                   CaptionML=[ENU=Location Mandatory;
                                                              PTG=Localiza��o Obrigat�ria] }
    { 4   ;   ;Item Nos.           ;Code10        ;TableRelation="No. Series";
                                                   CaptionML=[ENU=Item Nos.;
                                                              PTG=N� S�ries Produto] }
    { 30  ;   ;Automatic Cost Adjustment;Option   ;OnValidate=BEGIN
                                                                IF "Automatic Cost Adjustment" <> "Automatic Cost Adjustment"::Never THEN BEGIN
                                                                  Item.SETCURRENTKEY("Cost is Adjusted","Allow Online Adjustment");
                                                                  Item.SETRANGE("Cost is Adjusted",FALSE);
                                                                  Item.SETRANGE("Allow Online Adjustment",FALSE);

                                                                  UpdateInvtAdjmtEntryOrder;

                                                                  InvtAdjmtEntryOrder.SETCURRENTKEY("Cost is Adjusted","Allow Online Adjustment");
                                                                  InvtAdjmtEntryOrder.SETRANGE("Cost is Adjusted",FALSE);
                                                                  InvtAdjmtEntryOrder.SETRANGE("Allow Online Adjustment",FALSE);
                                                                  InvtAdjmtEntryOrder.SETRANGE("Is Finished",TRUE);

                                                                  IF NOT (Item.ISEMPTY AND InvtAdjmtEntryOrder.ISEMPTY) THEN
                                                                    MESSAGE(Text000);
                                                                END;
                                                              END;

                                                   CaptionML=[ENU=Automatic Cost Adjustment;
                                                              PTG=Ajuste Exist�ncias Autom�tico];
                                                   OptionCaptionML=[ENU=Never,Day,Week,Month,Quarter,Year,Always;
                                                                    PTG=Nunca,Dia,Semana,M�s,Trimestre,Ano,Sempre];
                                                   OptionString=Never,Day,Week,Month,Quarter,Year,Always }
    { 40  ;   ;Prevent Negative Inventory;Boolean ;CaptionML=[ENU=Prevent Negative Inventory;
                                                              PTG=Impedir Invent�rio Negativo] }
    { 5700;   ;Transfer Order Nos. ;Code10        ;TableRelation="No. Series";
                                                   AccessByPermission=TableData 5740=R;
                                                   CaptionML=[ENU=Transfer Order Nos.;
                                                              PTG=N� S�ries Ordem Transf.] }
    { 5701;   ;Posted Transfer Shpt. Nos.;Code10  ;TableRelation="No. Series";
                                                   AccessByPermission=TableData 5740=R;
                                                   CaptionML=[ENU=Posted Transfer Shpt. Nos.;
                                                              PTG=N� S�ries Envio Transf. Reg.] }
    { 5702;   ;Posted Transfer Rcpt. Nos.;Code10  ;TableRelation="No. Series";
                                                   AccessByPermission=TableData 5740=R;
                                                   CaptionML=[ENU=Posted Transfer Rcpt. Nos.;
                                                              PTG=N� S�ries Rece��o Transf. Reg.] }
    { 5703;   ;Copy Comments Order to Shpt.;Boolean;
                                                   InitValue=Yes;
                                                   AccessByPermission=TableData 5740=R;
                                                   CaptionML=[ENU=Copy Comments Order to Shpt.;
                                                              PTG=Copia Coment. Enc. p/ Envio] }
    { 5704;   ;Copy Comments Order to Rcpt.;Boolean;
                                                   InitValue=Yes;
                                                   AccessByPermission=TableData 5740=R;
                                                   CaptionML=[ENU=Copy Comments Order to Rcpt.;
                                                              PTG=Copia Coment. Enc. p/ Rece��o] }
    { 5718;   ;Nonstock Item Nos.  ;Code10        ;TableRelation="No. Series";
                                                   AccessByPermission=TableData 5718=R;
                                                   CaptionML=[ENU=Nonstock Item Nos.;
                                                              PTG=N�s S�ries Prod. N�o Armazenados] }
    { 5790;   ;Outbound Whse. Handling Time;DateFormula;
                                                   AccessByPermission=TableData 14=R;
                                                   CaptionML=[ENU=Outbound Whse. Handling Time;
                                                              PTG=Tempo Proc. Sa�da Armaz�m] }
    { 5791;   ;Inbound Whse. Handling Time;DateFormula;
                                                   AccessByPermission=TableData 14=R;
                                                   CaptionML=[ENU=Inbound Whse. Handling Time;
                                                              PTG=Tempo Proc. Entrada Armaz�m] }
    { 5800;   ;Expected Cost Posting to G/L;Boolean;
                                                   OnValidate=VAR
                                                                ChangeExpCostPostToGL@1000 : Codeunit 5811;
                                                              BEGIN
                                                                IF "Expected Cost Posting to G/L" <> xRec."Expected Cost Posting to G/L" THEN
                                                                  IF ItemLedgEntry.FINDFIRST THEN BEGIN
                                                                    ChangeExpCostPostToGL.ChangeExpCostPostingToGL(Rec,"Expected Cost Posting to G/L");
                                                                    FIND;
                                                                  END;
                                                              END;

                                                   CaptionML=[ENU=Expected Cost Posting to G/L;
                                                              PTG=Custo Esperado Reg. para C/G] }
    { 5804;   ;Average Cost Calc. Type;Option     ;InitValue=Item;
                                                   OnValidate=BEGIN
                                                                TESTFIELD("Average Cost Calc. Type");
                                                                IF "Average Cost Calc. Type" <> xRec."Average Cost Calc. Type" THEN
                                                                  UpdateAvgCostItemSettings(FIELDCAPTION("Average Cost Calc. Type"),FORMAT("Average Cost Calc. Type"));
                                                              END;

                                                   CaptionML=[ENU=Average Cost Calc. Type;
                                                              PTG=Tipo C�lculo Custo M�dio];
                                                   OptionCaptionML=[ENU=" ,Item,Item & Location & Variant";
                                                                    PTG=" ,Produto,Produto & Localiza��o & Variante"];
                                                   OptionString=[ ,Item,Item & Location & Variant];
                                                   NotBlank=Yes }
    { 5805;   ;Average Cost Period ;Option        ;InitValue=Day;
                                                   OnValidate=BEGIN
                                                                TESTFIELD("Average Cost Period");
                                                                IF "Average Cost Period" <> xRec."Average Cost Period" THEN
                                                                  UpdateAvgCostItemSettings(FIELDCAPTION("Average Cost Period"),FORMAT("Average Cost Period"));
                                                              END;

                                                   CaptionML=[ENU=Average Cost Period;
                                                              PTG=Per�odo Custo M�dio];
                                                   OptionCaptionML=[ENU=" ,Day,Week,Month,Quarter,Year,Accounting Period";
                                                                    PTG=" ,Dia,Semana,M�s,Trimestre,Ano,Per�odo Contabil�stico"];
                                                   OptionString=[ ,Day,Week,Month,Quarter,Year,Accounting Period];
                                                   NotBlank=Yes }
    { 7101;   ;Item Group Dimension Code;Code20   ;TableRelation=Dimension;
                                                   CaptionML=[ENU=Item Group Dimension Code;
                                                              PTG=C�digo Dimens�o Grupo Produto] }
    { 7300;   ;Inventory Put-away Nos.;Code10     ;TableRelation="No. Series";
                                                   AccessByPermission=TableData 7340=R;
                                                   CaptionML=[ENU=Inventory Put-away Nos.;
                                                              PTG=N� S�ries Arrumar Invent�rio] }
    { 7301;   ;Inventory Pick Nos. ;Code10        ;TableRelation="No. Series";
                                                   AccessByPermission=TableData 7342=R;
                                                   CaptionML=[ENU=Inventory Pick Nos.;
                                                              PTG=N� S�ries Recolha Invent�rio] }
    { 7302;   ;Posted Invt. Put-away Nos.;Code10  ;TableRelation="No. Series";
                                                   AccessByPermission=TableData 7340=R;
                                                   CaptionML=[ENU=Posted Invt. Put-away Nos.;
                                                              PTG=N� S�ries Arrumar Invent�rio Reg.] }
    { 7303;   ;Posted Invt. Pick Nos.;Code10      ;TableRelation="No. Series";
                                                   AccessByPermission=TableData 7342=R;
                                                   CaptionML=[ENU=Posted Invt. Pick Nos.;
                                                              PTG=N� S�ries Recolha Invent�rio Reg.] }
    { 7304;   ;Inventory Movement Nos.;Code10     ;TableRelation="No. Series";
                                                   AccessByPermission=TableData 7331=R;
                                                   CaptionML=[ENU=Inventory Movement Nos.;
                                                              PTG=N� S�ries Movs. Invent�rio] }
    { 7305;   ;Registered Invt. Movement Nos.;Code10;
                                                   TableRelation="No. Series";
                                                   AccessByPermission=TableData 7331=R;
                                                   CaptionML=[ENU=Registered Invt. Movement Nos.;
                                                              PTG=N� S�ries Movs. Invent�rio Reg.] }
    { 7306;   ;Internal Movement Nos.;Code10      ;TableRelation="No. Series";
                                                   AccessByPermission=TableData 7331=R;
                                                   CaptionML=[ENU=Internal Movement Nos.;
                                                              PTG=N� S�ries Movs. Internos] }
  }
  KEYS
  {
    {    ;Primary Key                             ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {
    VAR
      ItemLedgEntry@1000 : Record 32;
      Text000@1001 : TextConst 'ENU=Some unadjusted value entries will not be covered with the new setting. You must run the Adjust Cost - Item Entries batch job once to adjust these.;PTG=Alguns movimentos de valor n�o ser�o cobertos pela nova defini��o. Deve executar a rotina Ajustar Custo - Movs. Produto uma vez, para ajust�-los.';
      Item@1002 : Record 27;
      InvtAdjmtEntryOrder@1003 : Record 5896;
      Text001@1005 : TextConst 'ENU=If you change the %1, the program must adjust all item entries.;PTG=Se alterar %1, o programa ter� de ajustar todos os movimentos de produto.';
      Text002@1006 : TextConst 'ENU=The adjustment of all entries can take several hours.\;PTG=O ajuste de todos os movimentos poder� demorar v�rias horas.\';
      Text003@1007 : TextConst 'ENU=Do you really want to change the %1?;PTG=Pretende alterar %1?';
      Text004@1008 : TextConst 'ENU=The program has cancelled the change that would have caused an adjustment of all items.;PTG=O programa cancelou a altera��o que causaria o ajuste de todos os movimentos.';
      Text005@1009 : TextConst 'ENU=%1 has been changed to %2. You should now run %3.;PTG=%1 foi alterado para %2. Deve executar %3.';
      ObjTransl@1011 : Record 377;
      Text006@1004 : TextConst 'ENU=The field %1 should not be set to %2 if field %3 in %4 table is set to %5 because of possibility of deadlocks.;PTG=O campo %1 n�o deve ser %2 se o campo %3 na tabela %4 for %5, para evitar poss�veis bloqueios.';

    LOCAL PROCEDURE UpdateInvtAdjmtEntryOrder@2();
    VAR
      InvtAdjmtEntryOrder@1000 : Record 5896;
    BEGIN
      InvtAdjmtEntryOrder.SETCURRENTKEY("Cost is Adjusted","Allow Online Adjustment");
      InvtAdjmtEntryOrder.SETRANGE("Cost is Adjusted",FALSE);
      InvtAdjmtEntryOrder.SETRANGE("Allow Online Adjustment",FALSE);
      InvtAdjmtEntryOrder.SETRANGE("Is Finished",FALSE);
      InvtAdjmtEntryOrder.SETRANGE("Order Type",InvtAdjmtEntryOrder."Order Type"::Production);
      InvtAdjmtEntryOrder.MODIFYALL("Allow Online Adjustment",TRUE);
    END;

    LOCAL PROCEDURE UpdateAvgCostItemSettings@1(FieldCaption@1000 : Text[80];FieldValue@1001 : Text[80]);
    BEGIN
      IF NOT
         CONFIRM(
           STRSUBSTNO(
             Text001 +
             Text002 +
             Text003,FieldCaption),FALSE)
      THEN
        ERROR(Text004);

      CODEUNIT.RUN(CODEUNIT::"Change Average Cost Setting",Rec);

      MESSAGE(
        Text005,FieldCaption,FieldValue,
        ObjTransl.TranslateObject(ObjTransl."Object Type"::Report,REPORT::"Adjust Cost - Item Entries"));
    END;

    BEGIN
    END.
  }
}

