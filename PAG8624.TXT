OBJECT Page 8624 Config. Package Fields
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Config. Package Fields;
               PTG=Configurar Campos Pacote];
    InsertAllowed=No;
    DeleteAllowed=No;
    SourceTable=Table8616;
    DataCaptionExpr=FormCaption;
    SourceTableView=SORTING(Package Code,Table ID,Processing Order);
    PageType=List;
    OnAfterGetRecord=BEGIN
                       IncludedEnabled := NOT "Primary Key";
                       ValidateEnabled := NOT "Primary Key";
                     END;

    ActionList=ACTIONS
    {
      { 5       ;    ;ActionContainer;
                      ActionContainerType=ActionItems }
      { 14      ;1   ;ActionGroup;
                      CaptionML=[ENU=Fiel&ds;
                                 PTG=Campos] }
      { 3       ;2   ;Action    ;
                      CaptionML=[ENU=Set Included;
                                 PTG=Definir Inclu�dos];
                      ToolTipML=[ENU=Specify that the field is included in the package.;
                                 PTG=""];
                      ApplicationArea=#Basic,#Suite;
                      Promoted=Yes;
                      Image=Completed;
                      PromotedCategory=Process;
                      OnAction=VAR
                                 ConfigPackageField@1000 : Record 8616;
                                 ConfigPackageMgt@1004 : Codeunit 8611;
                               BEGIN
                                 ConfigPackageField.COPYFILTERS(Rec);
                                 ConfigPackageMgt.SelectAllPackageFields(ConfigPackageField,TRUE);
                               END;
                                }
      { 7       ;2   ;Action    ;
                      CaptionML=[ENU=Clear Included;
                                 PTG=Apagar Inclu�dos];
                      ToolTipML=[ENU=Deselect the included fields. To include all fields, choose the Set Included action.;
                                 PTG=""];
                      ApplicationArea=#Basic,#Suite;
                      Promoted=Yes;
                      Image=ResetStatus;
                      PromotedCategory=Process;
                      OnAction=VAR
                                 ConfigPackageField@1001 : Record 8616;
                                 ConfigPackageMgt@1000 : Codeunit 8611;
                               BEGIN
                                 ConfigPackageField.COPYFILTERS(Rec);
                                 ConfigPackageMgt.SelectAllPackageFields(ConfigPackageField,FALSE);
                               END;
                                }
      { 20      ;2   ;Action    ;
                      Ellipsis=Yes;
                      CaptionML=[ENU=Mapping;
                                 PTG=Mapeamento];
                      ToolTipML=[ENU=View the mapping of values from an existing ERP system into the Dynamics NAV implementation during the migration of data.;
                                 PTG=""];
                      ApplicationArea=#Basic,#Suite;
                      Promoted=Yes;
                      Image=MapAccounts;
                      PromotedCategory=Process;
                      OnAction=VAR
                                 ConfigPackageManagement@1000 : Codeunit 8611;
                               BEGIN
                                 ConfigPackageManagement.ShowFieldMapping(Rec);
                               END;
                                }
      { 16      ;2   ;Action    ;
                      CaptionML=[ENU=Move Up;
                                 PTG=Mover p/ Cima];
                      ToolTipML=[ENU=Change the sorting order of the lines.;
                                 PTG=""];
                      ApplicationArea=#Basic,#Suite;
                      Image=MoveUp;
                      OnAction=VAR
                                 ConfigPackageField@1000 : Record 8616;
                               BEGIN
                                 CurrPage.SAVERECORD;
                                 ConfigPackageField.SETCURRENTKEY("Package Code","Table ID","Processing Order");
                                 ConfigPackageField.SETRANGE("Package Code","Package Code");
                                 ConfigPackageField.SETRANGE("Table ID","Table ID");
                                 ConfigPackageField.SETFILTER("Processing Order",'..%1',"Processing Order" - 1);
                                 IF ConfigPackageField.FINDLAST THEN BEGIN
                                   ExchangeLines(Rec,ConfigPackageField);
                                   CurrPage.UPDATE(FALSE);
                                 END;
                               END;
                                }
      { 17      ;2   ;Action    ;
                      CaptionML=[ENU=Move Down;
                                 PTG=Mover p/ Baixo];
                      ToolTipML=[ENU=Change the sorting order of the lines.;
                                 PTG=""];
                      ApplicationArea=#Basic,#Suite;
                      Image=MoveDown;
                      OnAction=VAR
                                 ConfigPackageField@1000 : Record 8616;
                               BEGIN
                                 CurrPage.SAVERECORD;
                                 ConfigPackageField.SETCURRENTKEY("Package Code","Table ID","Processing Order");
                                 ConfigPackageField.SETRANGE("Package Code","Package Code");
                                 ConfigPackageField.SETRANGE("Table ID","Table ID");
                                 ConfigPackageField.SETFILTER("Processing Order",'%1..',"Processing Order" + 1);
                                 IF ConfigPackageField.FINDFIRST THEN BEGIN
                                   ExchangeLines(Rec,ConfigPackageField);
                                   CurrPage.UPDATE(FALSE);
                                 END;
                               END;
                                }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 2   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the ID for the table that is part of the migration process.;
                           PTG=""];
                SourceExpr="Table ID";
                Visible=FALSE;
                Editable=FALSE }

    { 13  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies whether the field in the table is part of the dimension definition set.;
                           PTG=""];
                ApplicationArea=#Suite;
                SourceExpr=Dimension }

    { 4   ;2   ;Field     ;
                CaptionML=PTG=ID Campo;
                ToolTipML=[ENU=Specifies the ID of the field for the table that is part of the migration process.;
                           PTG=""];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Field ID";
                Editable=FALSE }

    { 6   ;2   ;Field     ;
                DrillDown=No;
                ToolTipML=[ENU=Specifies the name of the field for the table that is part of the migration process. The name comes from the Name property for the field.;
                           PTG=""];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Field Name";
                Editable=FALSE }

    { 8   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the caption of the field for the table that is part of the migration process. The caption comes from the Caption property for the field.;
                           PTG=""];
                SourceExpr="Field Caption";
                Visible=FALSE;
                Editable=FALSE }

    { 15  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies whether the field is included in the migration. Select the check box to include the field in the migration process. By default, when you select the check box, the Field Caption check box is also selected. You can clear this check box if you do not want to enable validation for the field.;
                           PTG=""];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Include Field";
                Enabled=IncludedEnabled }

    { 10  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies whether the information in a field is to be validated during migration. Select the check box if you want to enable validation for the field. This is useful when you want to limit data to a prescribed set of options.;
                           PTG=""];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Validate Field";
                Enabled=ValidateEnabled }

    { 12  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the order in which the data from the fields in the package should be applied. If your business logic requires that a certain field be filled in before another field can contain data, you can use the Processing Order field to specify the appropriate order. To specify the order, use the Move Up and Move Down commands on the Actions tab in the Config. Package Fields window. When you export the configuration information to Excel, the order that you specify for processing is the order in which the fields will be listed in columns in Excel.;
                           PTG=""];
                ApplicationArea=#Basic,#Suite;
                BlankZero=Yes;
                SourceExpr="Processing Order";
                Editable=FALSE }

    { 18  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies whether the field is part of the definition of the primary key for the table.;
                           PTG=""];
                SourceExpr="Primary Key";
                Visible=FALSE }

    { 19  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies whether the field is to be localized.;
                           PTG=""];
                BlankZero=Yes;
                SourceExpr="Localize Field";
                Visible=FALSE }

    { 9   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the ID for the table that contains a field that is related to the one in the migration table. For example, the Post Code table has a relationship with the City field in the Company Information migration table.;
                           PTG=""];
                ApplicationArea=#Basic,#Suite;
                BlankZero=Yes;
                SourceExpr="Relation Table ID" }

    { 11  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the name of the table with a relationship to the migration field.;
                           PTG=""];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Relation Table Caption" }

    { 21  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies whether you can create additional values in the database during the configuration migration process. Select the check box to indicate that additional codes can be added to that field in Microsoft Dynamics NAV during the import of data from Excel.;
                           PTG=""];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Create Missing Codes" }

    { 22  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies whether the field has a mapping assigned to it that is to be used during data migration.;
                           PTG=""];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Mapping Exists";
                OnDrillDown=VAR
                              ConfigPackageManagement@1000 : Codeunit 8611;
                            BEGIN
                              ConfigPackageManagement.ShowFieldMapping(Rec);
                              CurrPage.UPDATE(FALSE);
                            END;
                             }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {
    VAR
      IncludedEnabled@1001 : Boolean;
      ValidateEnabled@1002 : Boolean;

    LOCAL PROCEDURE FormCaption@1() : Text[1024];
    VAR
      ConfigPackageTable@1000 : Record 8613;
    BEGIN
      IF ConfigPackageTable.GET("Package Code","Table ID") THEN
        ConfigPackageTable.CALCFIELDS("Table Caption");

      EXIT(ConfigPackageTable."Table Caption");
    END;

    LOCAL PROCEDURE ExchangeLines@3(VAR ConfigPackageField1@1000 : Record 8616;VAR ConfigPackageField2@1001 : Record 8616);
    VAR
      ProcessingOrder@1002 : Integer;
    BEGIN
      IF ConfigPackageField1."Primary Key" <> ConfigPackageField2."Primary Key" THEN
        EXIT;

      ProcessingOrder := ConfigPackageField1."Processing Order";
      ConfigPackageField1."Processing Order" := ConfigPackageField2."Processing Order";
      ConfigPackageField2."Processing Order" := ProcessingOrder;
      ConfigPackageField1.MODIFY;
      ConfigPackageField2.MODIFY;
    END;

    BEGIN
    END.
  }
}

