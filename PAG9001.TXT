OBJECT Page 9001 Accounting Manager Role Center
{
  OBJECT-PROPERTIES
  {
    Date=24/02/17;
    Time=15:19:27;
    Modified=Yes;
    Version List=NAVW110.0,NAVPTSS10.00;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Role Center;
               PTG=Centro Perfil];
    PageType=RoleCenter;
    ActionList=ACTIONS
    {
      { 1900000006;0 ;ActionContainer;
                      ActionContainerType=Reports }
      { 32      ;1   ;Action    ;
                      CaptionML=[ENU=Trial Balance;
                                 PTG=Balancete];
                      RunObject=Report 6;
                      Image=Report }
      { 1110001 ;1   ;Action    ;
                      CaptionML=[ENU=Trial Balance - Legal Books;
                                 PTG=Balancete - Valores Selados];
                      RunObject=Report 31022896;
                      Image=Report }
      { 1110002 ;1   ;Action    ;
                      CaptionML=[ENU=Trial Balance - 7/4 Columns;
                                 PTG=Balancete - 7/4 Colunas];
                      RunObject=Report 31022897;
                      Image=Report }
      { 33      ;1   ;Action    ;
                      CaptionML=[ENU=&Bank Detail Trial Balance;
                                 PTG=&Balancete Bancario Detalhado];
                      RunObject=Report 1404;
                      Image=Report }
      { 34      ;1   ;Action    ;
                      CaptionML=[ENU=&Account Schedule;
                                 PTG=Esquem&a de Contas];
                      RunObject=Report 25;
                      Image=Report }
      { 1110003 ;1   ;Action    ;
                      CaptionML=[ENU=Detail Account Statement;
                                 PTG=Extrato Conta Detalhado];
                      RunObject=Report 31022941;
                      Image=Report }
      { 35      ;1   ;Action    ;
                      CaptionML=[ENU=Bu&dget;
                                 PTG=&Or�amento];
                      RunObject=Report 8;
                      Image=Report }
      { 36      ;1   ;Action    ;
                      CaptionML=[ENU=Trial Bala&nce/Budget;
                                 PTG=Balancete/Or�ame&nto];
                      RunObject=Report 9;
                      Image=Report }
      { 37      ;1   ;Action    ;
                      CaptionML=[ENU=Trial Balance by &Period;
                                 PTG=Balancete por &Per�odo];
                      RunObject=Report 38;
                      Image=Report }
      { 1110004 ;1   ;Action    ;
                      CaptionML=[ENU=Trial Balance G. Dim./Account;
                                 PTG=Balancete Dim. G. / Conta];
                      RunObject=Report 31022898;
                      Image=Report }
      { 1110005 ;1   ;Action    ;
                      CaptionML=[ENU=Trial Balance Account/G. Dim.;
                                 PTG=Balancete Conta/Dim. G];
                      RunObject=Report 31022899;
                      Image=Report }
      { 46      ;1   ;Action    ;
                      CaptionML=[ENU=&Fiscal Year Balance;
                                 PTG=Saldo &Exerc�cio];
                      RunObject=Report 36;
                      Image=Report }
      { 47      ;1   ;Action    ;
                      CaptionML=[ENU=Balance Comp. - Prev. Y&ear;
                                 PTG=Comp. Saldo - Ano Ant&erior];
                      RunObject=Report 37;
                      Image=Report }
      { 48      ;1   ;Action    ;
                      CaptionML=[ENU=&Closing Trial Balance;
                                 PTG=Balancete Fecho];
                      RunObject=Report 10;
                      Image=Report }
      { 49      ;1   ;Separator  }
      { 104     ;1   ;Action    ;
                      CaptionML=[ENU=Cash Flow Date List;
                                 PTG=Listagem Data Cash Flow];
                      RunObject=Report 846;
                      Image=Report }
      { 115     ;1   ;Separator  }
      { 50      ;1   ;Action    ;
                      CaptionML=[ENU=Aged Accounts &Receivable;
                                 PTG=Antiguidade Contas  a Receber];
                      RunObject=Report 120;
                      Image=Report }
      { 51      ;1   ;Action    ;
                      CaptionML=[ENU=Aged Accounts Pa&yable;
                                 PTG=Antiguidade Contas  a Pagar];
                      RunObject=Report 322;
                      Image=Report }
      { 52      ;1   ;Action    ;
                      CaptionML=[ENU=Reconcile Cus&t. and Vend. Accs;
                                 PTG=Reconciliar Contas Clien&te e Forn.];
                      RunObject=Report 33;
                      Image=Report }
      { 53      ;1   ;Separator  }
      { 54      ;1   ;Action    ;
                      CaptionML=[ENU=&VAT Registration No. Check;
                                 PTG=Verifica��o N� Contribuinte];
                      RunObject=Report 32;
                      Image=Report }
      { 55      ;1   ;Action    ;
                      CaptionML=[ENU=VAT E&xceptions;
                                 PTG=E&xcep��es IVA];
                      RunObject=Report 31;
                      Image=Report }
      { 56      ;1   ;Action    ;
                      CaptionML=[ENU=VAT &Statement;
                                 PTG=&Declara��o IVA];
                      RunObject=Report 12;
                      Image=Report }
      { 57      ;1   ;Action    ;
                      CaptionML=[ENU=VAT - VIES Declaration Tax Aut&h;
                                 PTG=IVA - Declara��o Intracomunit�ria];
                      RunObject=Report 19;
                      Image=Report }
      { 58      ;1   ;Action    ;
                      CaptionML=[ENU=VAT - VIES Declaration Dis&k;
                                 PTG=IVA - Disc&o Declara��o Intracomunit�ria];
                      RunObject=Report 88;
                      Image=Report }
      { 59      ;1   ;Action    ;
                      CaptionML=[ENU=EU Sales &List;
                                 PTG=&Lista Vendas UE];
                      RunObject=Report 130;
                      Image=Report }
      { 1110008 ;1   ;Action    ;
                      CaptionML=[ENU=VAT Customers Report;
                                 PTG=Mapa Recapitulativo Clientes];
                      RunObject=Report 31022892;
                      Image=Report }
      { 60      ;1   ;Separator  }
      { 61      ;1   ;Action    ;
                      CaptionML=[ENU=&Intrastat - Checklist;
                                 PTG=&Intrastat - Verificar];
                      RunObject=Report 502;
                      Image=Report }
      { 62      ;1   ;Action    ;
                      CaptionML=[ENU=Intrastat - For&m;
                                 PTG=Intrastat - Formul�rio];
                      RunObject=Report 501;
                      Image=Report }
      { 4       ;1   ;Separator  }
      { 7       ;1   ;Action    ;
                      CaptionML=[ENU=Cost Accounting P/L Statement;
                                 PTG=Extrato G/P Contabilidade Custo];
                      RunObject=Report 1126;
                      Image=Report }
      { 15      ;1   ;Action    ;
                      CaptionML=[ENU=CA P/L Statement per Period;
                                 PTG=Extrato G/P CC por Per�odo];
                      RunObject=Report 1123;
                      Image=Report }
      { 21      ;1   ;Action    ;
                      CaptionML=[ENU=CA P/L Statement with Budget;
                                 PTG=Extrato G/P CC com Or�amento];
                      RunObject=Report 1133;
                      Image=Report }
      { 42      ;1   ;Action    ;
                      CaptionML=[ENU=Cost Accounting Analysis;
                                 PTG=An�lise Contabilidade Custo];
                      RunObject=Report 1127;
                      Image=Report }
      { 31022892;1   ;Separator  }
      { 31022891;1   ;Action    ;
                      CaptionML=[ENU=Stamp Duty List / Payment;
                                 PTG=Lista/Pagamento Imposto de Selo];
                      RunObject=Report 31022895;
                      Image=Report }
      { 1900000011;0 ;ActionContainer;
                      ActionContainerType=HomeItems }
      { 2       ;1   ;Action    ;
                      CaptionML=[ENU=Chart of Accounts;
                                 PTG=Plano de Contas];
                      RunObject=Page 16 }
      { 8       ;1   ;Action    ;
                      Name=Vendors;
                      CaptionML=[ENU=Vendors;
                                 PTG=Fornecedores];
                      RunObject=Page 27;
                      Image=Vendor }
      { 5       ;1   ;Action    ;
                      Name=VendorsBalance;
                      CaptionML=[ENU=Balance;
                                 PTG=Saldo];
                      RunObject=Page 27;
                      RunPageView=WHERE(Balance (LCY)=FILTER(<>0));
                      Image=Balance }
      { 6       ;1   ;Action    ;
                      CaptionML=[ENU=Purchase Orders;
                                 PTG=Encomendas Compra];
                      RunObject=Page 9307 }
      { 76      ;1   ;Action    ;
                      CaptionML=[ENU=Budgets;
                                 PTG=Or�amentos];
                      RunObject=Page 121 }
      { 9       ;1   ;Action    ;
                      CaptionML=[ENU=Bank Accounts;
                                 PTG=Contas Banc�rias];
                      RunObject=Page 371;
                      Image=BankAccount }
      { 10      ;1   ;Action    ;
                      CaptionML=[ENU=VAT Statements;
                                 PTG=Declara��es IVA];
                      RunObject=Page 320 }
      { 11      ;1   ;Action    ;
                      CaptionML=[ENU=Items;
                                 PTG=Produtos];
                      RunObject=Page 31;
                      Image=Item }
      { 12      ;1   ;Action    ;
                      Name=Customers;
                      CaptionML=[ENU=Customers;
                                 PTG=Clientes];
                      RunObject=Page 22;
                      Image=Customer }
      { 13      ;1   ;Action    ;
                      Name=CustomersBalance;
                      CaptionML=[ENU=Balance;
                                 PTG=Saldo];
                      RunObject=Page 22;
                      RunPageView=WHERE(Balance (LCY)=FILTER(<>0));
                      Image=Balance }
      { 14      ;1   ;Action    ;
                      CaptionML=[ENU=Sales Orders;
                                 PTG=Encomendas de Venda];
                      RunObject=Page 9305;
                      Image=Order }
      { 1102601003;1 ;Action    ;
                      CaptionML=[ENU=Reminders;
                                 PTG=Cartas Aviso];
                      RunObject=Page 436;
                      Image=Reminder }
      { 1102601004;1 ;Action    ;
                      CaptionML=[ENU=Finance Charge Memos;
                                 PTG=Notas Juros];
                      RunObject=Page 448;
                      Image=FinChargeMemo }
      { 119     ;1   ;Action    ;
                      CaptionML=[ENU=Incoming Documents;
                                 PTG=Documentos Recebidos];
                      RunObject=Page 190;
                      Image=Documents }
      { 1900000012;0 ;ActionContainer;
                      ActionContainerType=ActivityButtons }
      { 107     ;1   ;ActionGroup;
                      CaptionML=[ENU=Journals;
                                 PTG=Di�rios];
                      Image=Journals }
      { 117     ;2   ;Action    ;
                      Name=PurchaseJournals;
                      CaptionML=[ENU=Purchase Journals;
                                 PTG=Di�rios Compras];
                      RunObject=Page 251;
                      RunPageView=WHERE(Template Type=CONST(Purchases),
                                        Recurring=CONST(No)) }
      { 118     ;2   ;Action    ;
                      Name=SalesJournals;
                      CaptionML=[ENU=Sales Journals;
                                 PTG=Di�rios Vendas];
                      RunObject=Page 251;
                      RunPageView=WHERE(Template Type=CONST(Sales),
                                        Recurring=CONST(No)) }
      { 113     ;2   ;Action    ;
                      Name=CashReceiptJournals;
                      CaptionML=[ENU=Cash Receipt Journals;
                                 PTG=Di�rios Cobran�as];
                      RunObject=Page 251;
                      RunPageView=WHERE(Template Type=CONST(Cash Receipts),
                                        Recurring=CONST(No));
                      Image=Journals }
      { 114     ;2   ;Action    ;
                      Name=PaymentJournals;
                      CaptionML=[ENU=Payment Journals;
                                 PTG=Di�rios Pagamentos];
                      RunObject=Page 251;
                      RunPageView=WHERE(Template Type=CONST(Payments),
                                        Recurring=CONST(No));
                      Image=Journals }
      { 1102601000;2 ;Action    ;
                      Name=ICGeneralJournals;
                      CaptionML=[ENU=IC General Journals;
                                 PTG=Di�rios Gerais IE];
                      RunObject=Page 251;
                      RunPageView=WHERE(Template Type=CONST(Intercompany),
                                        Recurring=CONST(No)) }
      { 1102601001;2 ;Action    ;
                      Name=GeneralJournals;
                      CaptionML=[ENU=General Journals;
                                 PTG=Di�rios Gerais];
                      RunObject=Page 251;
                      RunPageView=WHERE(Template Type=CONST(General),
                                        Recurring=CONST(No));
                      Image=Journal }
      { 1102601002;2 ;Action    ;
                      CaptionML=[ENU=Intrastat Journals;
                                 PTG=Di�rios Intrastat];
                      RunObject=Page 327;
                      Image=Report }
      { 16      ;1   ;ActionGroup;
                      CaptionML=[ENU=Fixed Assets;
                                 PTG=Imobilizado];
                      Image=FixedAssets }
      { 17      ;2   ;Action    ;
                      CaptionML=[ENU=Fixed Assets;
                                 PTG=Imobilizado];
                      RunObject=Page 5601 }
      { 18      ;2   ;Action    ;
                      CaptionML=[ENU=Insurance;
                                 PTG=Seguro];
                      RunObject=Page 5645 }
      { 19      ;2   ;Action    ;
                      CaptionML=[ENU=Fixed Assets G/L Journals;
                                 PTG=Diarios Gerais Imobilizado];
                      RunObject=Page 251;
                      RunPageView=WHERE(Template Type=CONST(Assets),
                                        Recurring=CONST(No)) }
      { 24      ;2   ;Action    ;
                      CaptionML=[ENU=Fixed Assets Journals;
                                 PTG=Diarios Imobilidado];
                      RunObject=Page 5633;
                      RunPageView=WHERE(Recurring=CONST(No)) }
      { 20      ;2   ;Action    ;
                      CaptionML=[ENU=Fixed Assets Reclass. Journals;
                                 PTG=Diarios Reclassifica��o Imobilizado];
                      RunObject=Page 5640 }
      { 22      ;2   ;Action    ;
                      CaptionML=[ENU=Insurance Journals;
                                 PTG=Di�rios Seguros];
                      RunObject=Page 5655 }
      { 3       ;2   ;Action    ;
                      Name=<Action3>;
                      CaptionML=[ENU=Recurring General Journals;
                                 PTG=Di�rios Gerais Peri�dicos];
                      RunObject=Page 251;
                      RunPageView=WHERE(Template Type=CONST(General),
                                        Recurring=CONST(Yes)) }
      { 23      ;2   ;Action    ;
                      CaptionML=[ENU=Recurring Fixed Asset Journals;
                                 PTG=Di�rios Imobilizado Peri�dicos];
                      RunObject=Page 5633;
                      RunPageView=WHERE(Recurring=CONST(Yes)) }
      { 121     ;1   ;ActionGroup;
                      CaptionML=[ENU=Cash Flow;
                                 PTG=Cash Flow] }
      { 102     ;2   ;Action    ;
                      CaptionML=[ENU=Cash Flow Forecasts;
                                 PTG=Previs�es Cash Flow];
                      RunObject=Page 849 }
      { 142     ;2   ;Action    ;
                      CaptionML=[ENU=Chart of Cash Flow Accounts;
                                 PTG=Gr�fico de Contas de Cash Flow];
                      RunObject=Page 851 }
      { 174     ;2   ;Action    ;
                      CaptionML=[ENU=Cash Flow Manual Revenues;
                                 PTG=Manual Receitas Cash Flow];
                      RunObject=Page 857 }
      { 177     ;2   ;Action    ;
                      CaptionML=[ENU=Cash Flow Manual Expenses;
                                 PTG=Manual Despesas Cash Flow];
                      RunObject=Page 859 }
      { 84      ;1   ;ActionGroup;
                      CaptionML=[ENU=Cost Accounting;
                                 PTG=Contabilidade Custo] }
      { 77      ;2   ;Action    ;
                      CaptionML=[ENU=Cost Types;
                                 PTG=Tipos Custo];
                      RunObject=Page 1100 }
      { 75      ;2   ;Action    ;
                      CaptionML=[ENU=Cost Centers;
                                 PTG=Centros Custo];
                      RunObject=Page 1122 }
      { 74      ;2   ;Action    ;
                      CaptionML=[ENU=Cost Objects;
                                 PTG=Objetos Custo];
                      RunObject=Page 1123 }
      { 63      ;2   ;Action    ;
                      CaptionML=[ENU=Cost Allocations;
                                 PTG=Distribui��es Custo];
                      RunObject=Page 1102 }
      { 1       ;2   ;Action    ;
                      CaptionML=[ENU=Cost Budgets;
                                 PTG=Or�amentos de Custos];
                      RunObject=Page 1116 }
      { 39      ;1   ;ActionGroup;
                      CaptionML=[ENU=Posted Documents;
                                 PTG=Documentos Registados];
                      Image=FiledPosted }
      { 25      ;2   ;Action    ;
                      CaptionML=[ENU=Posted Sales Invoices;
                                 PTG=Faturas Venda Registadas];
                      RunObject=Page 143;
                      Image=PostedOrder }
      { 26      ;2   ;Action    ;
                      CaptionML=[ENU=Posted Sales Credit Memos;
                                 PTG=Notas Cr�dito Venda Registadas];
                      RunObject=Page 144;
                      Image=PostedOrder }
      { 111     ;2   ;Action    ;
                      Name=<Page Posted Sales Debit Memos>;
                      CaptionML=PTG=Notas D�bito Venda Registadas;
                      RunObject=Page 31023060 }
      { 27      ;2   ;Action    ;
                      CaptionML=[ENU=Posted Purchase Invoices;
                                 PTG=Faturas Compra Registadas];
                      RunObject=Page 146 }
      { 28      ;2   ;Action    ;
                      CaptionML=[ENU=Posted Purchase Credit Memos;
                                 PTG=Notas Cr�dito Compra Registadas];
                      RunObject=Page 147 }
      { 109     ;2   ;Action    ;
                      CaptionML=[ENU=Posted Debit Memos;
                                 PTG=Notas D�bito Compra Registadas];
                      RunObject=Page 31023072;
                      Image=PostedOrder }
      { 29      ;2   ;Action    ;
                      CaptionML=[ENU=Issued Reminders;
                                 PTG=Cartas Aviso Emitidas];
                      RunObject=Page 440;
                      Image=OrderReminder }
      { 30      ;2   ;Action    ;
                      CaptionML=[ENU=Issued Fin. Charge Memos;
                                 PTG=Notas de Juros Emitidas];
                      RunObject=Page 452;
                      Image=PostedMemo }
      { 92      ;2   ;Action    ;
                      CaptionML=[ENU=G/L Registers;
                                 PTG=Regs. Movs. G/L];
                      RunObject=Page 116;
                      Image=GLRegisters }
      { 83      ;2   ;Action    ;
                      CaptionML=[ENU=Cost Accounting Registers;
                                 PTG=Regs. Movs. Contabilidade];
                      RunObject=Page 1104 }
      { 91      ;2   ;Action    ;
                      CaptionML=[ENU=Cost Accounting Budget Registers;
                                 PTG=Regs. Movs. Or�amento Contabilidade Custo];
                      RunObject=Page 1121 }
      { 31      ;1   ;ActionGroup;
                      CaptionML=[ENU=Administration;
                                 PTG=Administra��o];
                      Image=Administration }
      { 38      ;2   ;Action    ;
                      CaptionML=[ENU=Currencies;
                                 PTG=Divisas];
                      RunObject=Page 5;
                      Image=Currency }
      { 40      ;2   ;Action    ;
                      CaptionML=[ENU=Accounting Periods;
                                 PTG=Per�odos Contabil�sticos];
                      RunObject=Page 100;
                      Image=AccountingPeriods }
      { 41      ;2   ;Action    ;
                      CaptionML=[ENU=Number Series;
                                 PTG=N�s S�rie];
                      RunObject=Page 456 }
      { 43      ;2   ;Action    ;
                      CaptionML=[ENU=Analysis Views;
                                 PTG=Dados An�lise];
                      RunObject=Page 556 }
      { 93      ;2   ;Action    ;
                      CaptionML=[ENU=Account Schedules;
                                 PTG=Esquemas Contas];
                      RunObject=Page 103 }
      { 44      ;2   ;Action    ;
                      CaptionML=[ENU=Dimensions;
                                 PTG=Dimens�es];
                      ToolTipML=[ENU=View or edit dimensions, such as area, project, or department, that you can assign to sales and purchase documents to distribute costs and analyze transaction history.;
                                 PTG=""];
                      RunObject=Page 536;
                      Image=Dimensions }
      { 45      ;2   ;Action    ;
                      CaptionML=[ENU=Bank Account Posting Groups;
                                 PTG=Grupos Contabs. Conta Banc�ria];
                      RunObject=Page 373 }
      { 105     ;0   ;ActionContainer;
                      ActionContainerType=NewDocumentItems }
      { 66      ;1   ;Action    ;
                      CaptionML=[ENU=Sales &Credit Memo;
                                 PTG=Nota &Cr�dito Venda];
                      RunObject=Page 44;
                      Promoted=No;
                      Image=CreditMemo;
                      PromotedCategory=Process;
                      RunPageMode=Create }
      { 65      ;1   ;Action    ;
                      CaptionML=[ENU=P&urchase Credit Memo;
                                 PTG=Nota Cr�dito Compra];
                      RunObject=Page 52;
                      Promoted=No;
                      Image=CreditMemo;
                      PromotedCategory=Process;
                      RunPageMode=Create }
      { 1900000004;0 ;ActionContainer;
                      ActionContainerType=ActionItems }
      { 64      ;1   ;Separator ;
                      CaptionML=[ENU=Tasks;
                                 PTG=Tarefas];
                      IsHeader=Yes }
      { 94      ;1   ;Action    ;
                      CaptionML=[ENU=Cas&h Receipt Journal;
                                 PTG=Di�rio Cobran�as];
                      RunObject=Page 255;
                      Image=CashReceiptJournal }
      { 95      ;1   ;Action    ;
                      CaptionML=[ENU=Pa&yment Journal;
                                 PTG=Di�rio Pagamentos];
                      RunObject=Page 256;
                      Image=PaymentJournal }
      { 67      ;1   ;Separator  }
      { 110     ;1   ;Action    ;
                      CaptionML=[ENU=Analysis &Views;
                                 PTG=&Visualizar e Analizar];
                      RunObject=Page 556;
                      Image=AnalysisView }
      { 98      ;1   ;Action    ;
                      CaptionML=[ENU=Analysis by &Dimensions;
                                 PTG=An�lises por &Dimens�o];
                      RunObject=Page 554;
                      Image=AnalysisViewDimension }
      { 68      ;1   ;Action    ;
                      Ellipsis=Yes;
                      CaptionML=[ENU=Calculate Deprec&iation;
                                 PTG=Calcular Amortiza��o];
                      RunObject=Report 5692;
                      Image=CalculateDepreciation }
      { 69      ;1   ;Action    ;
                      Ellipsis=Yes;
                      CaptionML=[ENU=Import Co&nsolidation from Database;
                                 PTG=Importar Consolida��o a partir da Base de Dados];
                      RunObject=Report 90;
                      Image=ImportDatabase }
      { 70      ;1   ;Action    ;
                      CaptionML=[ENU=Bank Account R&econciliation;
                                 PTG=Conta Reconcilia��o Bancaria];
                      RunObject=Page 379;
                      Image=BankAccountRec }
      { 31022890;1   ;Action    ;
                      CaptionML=[ENU=Payment Reconciliation Journals;
                                 PTG=Di�rios de Reconcilia��o de Pagamentos];
                      RunObject=Page 1294;
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=ApplyEntries;
                      RunPageMode=View }
      { 71      ;1   ;Action    ;
                      Ellipsis=Yes;
                      CaptionML=[ENU=Adjust E&xchange Rates;
                                 PTG=Ajustar Taxas C�mbio];
                      RunObject=Report 595;
                      Image=AdjustExchangeRates }
      { 72      ;1   ;Action    ;
                      CaptionML=[ENU=P&ost Inventory Cost to G/L;
                                 PTG=Registar C&usto Invent�rio na C/G];
                      RunObject=Report 1002;
                      Image=PostInventoryToGL }
      { 97      ;1   ;Separator  }
      { 78      ;1   ;Action    ;
                      Ellipsis=Yes;
                      CaptionML=[ENU=C&reate Reminders;
                                 PTG=Criar Aviso];
                      RunObject=Report 188;
                      Image=CreateReminders }
      { 79      ;1   ;Action    ;
                      Ellipsis=Yes;
                      CaptionML=[ENU=Create Finance Charge &Memos;
                                 PTG=Criar &Notas Juro];
                      RunObject=Report 191;
                      Image=CreateFinanceChargememo }
      { 73      ;1   ;Separator  }
      { 81      ;1   ;Action    ;
                      CaptionML=[ENU=Intrastat &Journal;
                                 PTG="Di�rios Intrastat "];
                      RunObject=Page 327;
                      Image=Journal }
      { 82      ;1   ;Action    ;
                      CaptionML=[ENU=Calc. and Pos&t VAT Settlement;
                                 PTG=Calcular e Regis&tar Liquida��o IVA];
                      RunObject=Report 20;
                      Image=SettleOpenTransactions }
      { 80      ;1   ;Separator ;
                      CaptionML=[ENU=Administration;
                                 PTG=Administra��o];
                      IsHeader=Yes }
      { 85      ;1   ;Action    ;
                      CaptionML=[ENU=General &Ledger Setup;
                                 PTG=Configura��o &Contabilidade Geral];
                      RunObject=Page 118;
                      Image=Setup }
      { 86      ;1   ;Action    ;
                      CaptionML=[ENU=&Sales && Receivables Setup;
                                 PTG=Configura��o Vendas e Cobran�as];
                      RunObject=Page 459;
                      Image=Setup }
      { 87      ;1   ;Action    ;
                      CaptionML=[ENU=&Purchases && Payables Setup;
                                 PTG=Configura��o Compras e Pagamentos];
                      RunObject=Page 460;
                      Image=Setup }
      { 88      ;1   ;Action    ;
                      CaptionML=[ENU=&Fixed Asset Setup;
                                 PTG=Con&figura��o Imobilizado];
                      RunObject=Page 5607;
                      Image=Setup }
      { 1110010 ;1   ;Action    ;
                      CaptionML=[ENU=Cartera Setup;
                                 PTG=Configura��o Carteira];
                      RunObject=Page 31022981;
                      Image=Setup }
      { 101     ;1   ;Action    ;
                      CaptionML=[ENU=Cash Flow Setup;
                                 PTG=Configura��o Cash Flow];
                      RunObject=Page 846;
                      Image=CashFlowSetup }
      { 96      ;1   ;Action    ;
                      CaptionML=[ENU=Cost Accounting Setup;
                                 PTG=Configura��o Contabilidade Custo];
                      RunObject=Page 1113;
                      Image=CostAccountingSetup }
      { 89      ;1   ;Separator ;
                      CaptionML=[ENU=History;
                                 PTG=Hist�rico];
                      IsHeader=Yes }
      { 90      ;1   ;Action    ;
                      CaptionML=[ENU=Navi&gate;
                                 PTG=Navegar];
                      RunObject=Page 344;
                      Image=Navigate }
    }
  }
  CONTROLS
  {
    { 1900000008;0;Container;
                ContainerType=RoleCenterArea }

    { 1900724808;1;Group   }

    { 99  ;2   ;Part      ;
                PagePartID=Page762;
                Visible=false;
                PartType=Page }

    { 1902304208;2;Part   ;
                PagePartID=Page9030;
                PartType=Page }

    { 1907692008;2;Part   ;
                PagePartID=Page9150;
                PartType=Page }

    { 1900724708;1;Group   }

    { 103 ;2   ;Part      ;
                PagePartID=Page760;
                Visible=FALSE;
                PartType=Page }

    { 106 ;2   ;Part      ;
                PagePartID=Page675;
                Visible=false;
                PartType=Page }

    { 100 ;2   ;Part      ;
                PagePartID=Page869;
                PartType=Page }

    { 1902476008;2;Part   ;
                PagePartID=Page9151;
                PartType=Page }

    { 108 ;2   ;Part      ;
                PagePartID=Page681;
                PartType=Page }

    { 1903012608;2;Part   ;
                PagePartID=Page9175;
                PartType=Page }

    { 1901377608;2;Part   ;
                PartType=System;
                SystemPartID=MyNotes }

  }
  CODE
  {

    BEGIN
    END.
  }
}

