OBJECT Page 99000845 Prod. Order Routing Personnel
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Prod. Order Routing Personnel;
               PTG=Pessoal Gama Operatória da OP];
    MultipleNewLines=Yes;
    SourceTable=Table5412;
    DataCaptionExpr=Caption;
    PageType=List;
    AutoSplitKey=Yes;
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 4   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies a number.;
                           PTG=""];
                SourceExpr="No." }

    { 6   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the description of the personnel.;
                           PTG=""];
                SourceExpr=Description }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {

    BEGIN
    END.
  }
}

