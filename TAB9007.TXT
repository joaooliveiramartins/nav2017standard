OBJECT Table 9007 User Group Plan
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0;
  }
  PROPERTIES
  {
    DataPerCompany=No;
    CaptionML=[ENU=User Group Plan;
               PTG=Plano Grupo Utilizadores];
  }
  FIELDS
  {
    { 1   ;   ;Plan ID             ;GUID          ;TableRelation=Plan."Plan ID";
                                                   CaptionML=[ENU=Plan ID;
                                                              PTG=ID Plano] }
    { 2   ;   ;User Group Code     ;Code20        ;TableRelation="User Group".Code;
                                                   CaptionML=[ENU=User Group Code;
                                                              PTG=C�d. Grupo Utilizadores] }
    { 10  ;   ;Plan Name           ;Text50        ;FieldClass=FlowField;
                                                   CalcFormula=Lookup(Plan.Name WHERE (Plan ID=FIELD(Plan ID)));
                                                   CaptionML=[ENU=Plan Name;
                                                              PTG=Nome Plano] }
    { 11  ;   ;User Group Name     ;Text50        ;FieldClass=FlowField;
                                                   CalcFormula=Lookup("User Group".Name WHERE (Code=FIELD(User Group Code)));
                                                   CaptionML=[ENU=User Group Name;
                                                              PTG=Nome Grupo Utilizador] }
  }
  KEYS
  {
    {    ;Plan ID,User Group Code                 ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    END.
  }
}

