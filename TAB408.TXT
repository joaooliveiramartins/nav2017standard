OBJECT Table 408 XBRL Line Constant
{
  OBJECT-PROPERTIES
  {
    Date=05/08/11;
    Time=12:00:00;
    Version List=NAVW16.00;
  }
  PROPERTIES
  {
    OnInsert=BEGIN
               TESTFIELD("XBRL Taxonomy Name");
             END;

    CaptionML=[ENU=XBRL Line Constant;
               PTG=Constante Linha XBRL];
  }
  FIELDS
  {
    { 1   ;   ;XBRL Taxonomy Name  ;Code20        ;TableRelation="XBRL Taxonomy";
                                                   CaptionML=[ENU=XBRL Taxonomy Name;
                                                              PTG=Nome Taxonomia XBRL] }
    { 2   ;   ;XBRL Taxonomy Line No.;Integer     ;TableRelation="XBRL Taxonomy Line"."Line No." WHERE (XBRL Taxonomy Name=FIELD(XBRL Taxonomy Name));
                                                   CaptionML=[ENU=XBRL Taxonomy Line No.;
                                                              PTG=N� Linha Taxonomia XBRL] }
    { 3   ;   ;Line No.            ;Integer       ;CaptionML=[ENU=Line No.;
                                                              PTG=N� Linha] }
    { 4   ;   ;Starting Date       ;Date          ;CaptionML=[ENU=Starting Date;
                                                              PTG=Data Inicial] }
    { 5   ;   ;Constant Amount     ;Decimal       ;CaptionML=[ENU=Constant Amount;
                                                              PTG=Valor Constante] }
    { 6   ;   ;Label Language Filter;Text10       ;FieldClass=FlowFilter;
                                                   CaptionML=[ENU=Label Language Filter;
                                                              PTG=Filtro Idioma Etiqueta] }
  }
  KEYS
  {
    {    ;XBRL Taxonomy Name,XBRL Taxonomy Line No.,Line No.;
                                                   Clustered=Yes }
    {    ;XBRL Taxonomy Name,XBRL Taxonomy Line No.,Starting Date }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    END.
  }
}

