OBJECT Page 5173 Answer Points List
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0;
  }
  PROPERTIES
  {
    Editable=No;
    CaptionML=[ENU=Answer Points List;
               PTG=Lista Pontua��o Resposta];
    SourceTable=Table5111;
    DataCaptionFields=Profile Questionnaire Line No.;
    PageType=List;
    OnAfterGetRecord=BEGIN
                       IF ProfileQuestionnaireLine.GET("Rating Profile Quest. Code","Rating Profile Quest. Line No.") THEN ;
                     END;

    OnAfterGetCurrRecord=BEGIN
                           IF NOT ProfileQuestionnaireLine.GET("Rating Profile Quest. Code","Rating Profile Quest. Line No.") THEN
                             CLEAR(ProfileQuestionnaireLine);
                         END;

  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 2   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the code for the profile questionnaire that contains the answer you use to create your rating.;
                           PTG=""];
                ApplicationArea=#All;
                SourceExpr="Rating Profile Quest. Code" }

    { 13  ;2   ;Field     ;
                CaptionML=[ENU=Question;
                           PTG=Quest�o];
                ToolTipML=[ENU=Specifies the question in the profile questionnaire.;
                           PTG=""];
                ApplicationArea=#RelationshipMgmt;
                SourceExpr=ProfileQuestionnaireLine.Question }

    { 11  ;2   ;Field     ;
                CaptionML=[ENU=Answer;
                           PTG=Resposta];
                ToolTipML=[ENU=Specifies answers to the questions in the profile questionnaire.;
                           PTG=""];
                ApplicationArea=#RelationshipMgmt;
                SourceExpr=ProfileQuestionnaireLine.Description;
                Editable=FALSE }

    { 6   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the number of points you have assigned to this answer.;
                           PTG=""];
                ApplicationArea=#RelationshipMgmt;
                SourceExpr=Points }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {
    VAR
      ProfileQuestionnaireLine@1000 : Record 5088;

    BEGIN
    END.
  }
}

