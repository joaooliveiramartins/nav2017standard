OBJECT Page 292 Req. Worksheet Template List
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0;
  }
  PROPERTIES
  {
    Editable=No;
    CaptionML=[ENU=Req. Worksheet Template List;
               PTG=Lista Livro Folha Requisi��o];
    SourceTable=Table244;
    PageType=List;
    RefreshOnActivate=Yes;
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 2   ;2   ;Field     ;
                ToolTipML=ENU=Specifies the name of the requisition worksheet template you are creating.;
                SourceExpr=Name }

    { 4   ;2   ;Field     ;
                ToolTipML=ENU=Specifies a description of the requisition worksheet template you are creating.;
                SourceExpr=Description }

    { 14  ;2   ;Field     ;
                ToolTipML=ENU=Specifies whether the requisition worksheet template will be a recurring requisition worksheet.;
                SourceExpr=Recurring;
                Visible=FALSE }

    { 12  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the window number the requisition worksheet template appears in.;
                SourceExpr="Page ID";
                Visible=FALSE;
                LookupPageID=Objects }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {

    BEGIN
    END.
  }
}

