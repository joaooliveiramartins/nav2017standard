OBJECT Table 6704 Booking Mailbox
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0;
  }
  PROPERTIES
  {
    TableType=Exchange;
    ExternalName=BookingMailbox;
    CaptionML=[ENU=Booking Mailbox;
               PTG=Mailbox Reserva];
  }
  FIELDS
  {
    { 1   ;   ;SmtpAddress         ;Text80        ;CaptionML=[ENU=SmtpAddress;
                                                              PTG=Endere�oSmtp] }
    { 2   ;   ;Name                ;Text250       ;CaptionML=[ENU=Name;
                                                              PTG=Nome] }
    { 3   ;   ;Display Name        ;Text250       ;ExternalName=DisplayName;
                                                   CaptionML=[ENU=Display Name;
                                                              PTG=Nome a Mostrar] }
  }
  KEYS
  {
    {    ;SmtpAddress                             ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    PROCEDURE LookupMailbox@1(VAR BookingMailbox@1001 : Record 6704) : Boolean;
    VAR
      BookingMailboxList@1000 : Page 6704;
    BEGIN
      BookingMailboxList.SETRECORD(Rec);
      BookingMailboxList.SETTABLEVIEW(Rec);
      BookingMailboxList.LOOKUPMODE(TRUE);
      IF BookingMailboxList.RUNMODAL IN [ACTION::OK,ACTION::LookupOK] THEN BEGIN
        BookingMailboxList.GETRECORD(BookingMailbox);
        EXIT(TRUE);
      END;
    END;

    BEGIN
    END.
  }
}

