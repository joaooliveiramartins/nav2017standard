OBJECT Page 31023021 Closed BG Analysis LCY FB
{
  OBJECT-PROPERTIES
  {
    Date=01/02/15;
    Time=13:00:00;
    Version List=NAVPTSS81.00;
  }
  PROPERTIES
  {
    Editable=No;
    CaptionML=[ENU=Closed BG Analysis LCY;
               PTG=" An�lise Remessa DL Fechada"];
    SourceTable=Table31022940;
    DataCaptionExpr=Caption;
    PageType=CardPart;
    OnAfterGetRecord=BEGIN
                       UpdateStatistics;
                     END;

  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1110008;1;Field     ;
                ApplicationArea=#All;
                SourceExpr="Currency Code" }

    { 1110004;1;Field     ;
                ApplicationArea=#All;
                SourceExpr="Amount Grouped" }

    { 1110005;1;Group     ;
                CaptionML=[ENU=No. of documents;
                           PTG=N� documentos] }

    { 1110006;2;Field     ;
                CaptionML=[ENU=Honored;
                           PTG=Pago];
                ApplicationArea=#All;
                SourceExpr=NoHonored;
                Editable=FALSE }

    { 1110007;2;Field     ;
                CaptionML=[ENU=Rejected;
                           PTG=Devolvido];
                ApplicationArea=#All;
                SourceExpr=NoRejected;
                Editable=FALSE }

    { 1110016;2;Field     ;
                CaptionML=[ENU=Redrawn;
                           PTG=Reformado];
                ApplicationArea=#All;
                SourceExpr=NoRedrawn;
                Editable=FALSE }

    { 1110012;1;Group     ;
                CaptionML=[ENU=Amount (LCY);
                           PTG=Valor (DL)] }

    { 1110013;2;Field     ;
                Name=Honored;
                CaptionML=[ENU=Honored;
                           PTG=Pago];
                ApplicationArea=#All;
                SourceExpr=HonoredAmtLCY;
                AutoFormatType=1;
                Editable=FALSE }

    { 1110014;2;Field     ;
                Name=Rejected;
                CaptionML=[ENU=Rejected;
                           PTG=Devolvido];
                ApplicationArea=#All;
                SourceExpr=RejectedAmtLCY;
                AutoFormatType=1;
                Editable=FALSE }

    { 1110018;2;Field     ;
                Name=Redrawn;
                CaptionML=[ENU=Redrawn;
                           PTG=Reformado];
                ApplicationArea=#All;
                SourceExpr=RedrawnAmtLCY;
                AutoFormatType=1;
                Editable=FALSE }

  }
  CODE
  {
    VAR
      ClosedDoc@1110000 : Record 31022937;
      HonoredAmt@1110001 : Decimal;
      RejectedAmt@1110002 : Decimal;
      RedrawnAmt@1110003 : Decimal;
      HonoredAmtLCY@1110004 : Decimal;
      RejectedAmtLCY@1110005 : Decimal;
      RedrawnAmtLCY@1110006 : Decimal;
      NoHonored@1110007 : Integer;
      NoRejected@1110008 : Integer;
      NoRedrawn@1110009 : Integer;

    LOCAL PROCEDURE UpdateStatistics@1();
    BEGIN
      WITH ClosedDoc DO BEGIN
        SETCURRENTKEY(Type,"Collection Agent","Bill Gr./Pmt. Order No.","Currency Code",Status,Redrawn);
        SETRANGE(Type,Type::Receivable);
        SETRANGE("Collection Agent","Collection Agent"::Bank);
        SETRANGE("Bill Gr./Pmt. Order No.",Rec."No.");
        Rec.COPYFILTER("Global Dimension 1 Filter","Global Dimension 1 Code");
        Rec.COPYFILTER("Global Dimension 2 Filter","Global Dimension 2 Code");

        SETRANGE(Status,Status::Honored);
        SETRANGE(Redrawn,TRUE);
        CALCSUMS("Amount for Collection","Amt. for Collection (LCY)");
        RedrawnAmt := "Amount for Collection";
        RedrawnAmtLCY := "Amt. for Collection (LCY)";
        NoRedrawn := COUNT;

        SETRANGE(Redrawn,FALSE);
        CALCSUMS("Amount for Collection","Amt. for Collection (LCY)");
        HonoredAmt := "Amount for Collection";
        HonoredAmtLCY := "Amt. for Collection (LCY)";
        NoHonored := COUNT;
        SETRANGE(Redrawn);

        SETRANGE(Status,Status::Rejected);
        CALCSUMS("Amount for Collection","Amt. for Collection (LCY)");
        RejectedAmt := "Amount for Collection";
        RejectedAmtLCY := "Amt. for Collection (LCY)";
        NoRejected := COUNT;
        SETRANGE(Status);
      END;
    END;

    BEGIN
    END.
  }
}

