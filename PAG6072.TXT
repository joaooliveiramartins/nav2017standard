OBJECT Page 6072 Filed Service Contract
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0;
  }
  PROPERTIES
  {
    Editable=No;
    CaptionML=[ENU=Filed Service Contract;
               PTG=Contrato Servi�o Guardado];
    InsertAllowed=No;
    ModifyAllowed=No;
    SourceTable=Table5970;
    DataCaptionExpr=FORMAT("Contract Type") + ' ' + "Contract No.";
    PageType=Document;
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                CaptionML=[ENU=General;
                           PTG=Geral] }

    { 2   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the number of the filed service contract or service contract quote.;
                           PTG=""];
                SourceExpr="Contract No.";
                Editable=FALSE }

    { 48  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies a description of the filed service contract or contract quote.;
                           PTG=""];
                SourceExpr=Description }

    { 161 ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the number of the customer who owns the items in the filed service contract or contract quote.;
                           PTG=""];
                SourceExpr="Customer No." }

    { 23  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the number of the contact who will receive the service contract delivery.;
                           PTG=""];
                SourceExpr="Contact No." }

    { 120 ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the name of the customer in the filed service contract or contract quote.;
                           PTG=""];
                SourceExpr=Name }

    { 50  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the address of the customer in the filed service contract or contract quote.;
                           PTG=""];
                SourceExpr=Address }

    { 163 ;2   ;Field     ;
                ToolTipML=[ENU=Specifies an additional address line.;
                           PTG=""];
                SourceExpr="Address 2" }

    { 84  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the postal code of the address.;
                           PTG=""];
                SourceExpr="Post Code" }

    { 86  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the city of the address.;
                           PTG=""];
                SourceExpr=City }

    { 165 ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the name of the person you regularly contact when you do business with the customer in the filed service contract or contract quote.;
                           PTG=""];
                SourceExpr="Contact Name" }

    { 14  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the customer phone number.;
                           PTG=""];
                SourceExpr="Phone No." }

    { 16  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the customer's email address.;
                           PTG=""];
                SourceExpr="E-Mail" }

    { 167 ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the contract group code of the filed service contract or contract quote.;
                           PTG=""];
                SourceExpr="Contract Group Code" }

    { 169 ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the code of the salesperson assigned to the filed service contract or contract quote.;
                           PTG=""];
                SourceExpr="Salesperson Code" }

    { 173 ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the starting date of the filed service contract or contract quote.;
                           PTG=""];
                SourceExpr="Starting Date" }

    { 175 ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the date when the filed service contract expires.;
                           PTG=""];
                SourceExpr="Expiration Date" }

    { 177 ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the status of the filed service contract or contract quote.;
                           PTG=""];
                SourceExpr=Status }

    { 8   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the code of the responsibility center associated with the customer in the filed service contract or contract quote, or with your company.;
                           PTG=""];
                SourceExpr="Responsibility Center" }

    { 25  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies if the service contract or the service contract quote was open or locked for changes at the moment of filing.;
                           PTG=""];
                SourceExpr="Change Status" }

    { 93  ;1   ;Part      ;
                SubPageView=SORTING(Entry No.,Line No.);
                SubPageLink=Entry No.=FIELD(Entry No.);
                PagePartID=Page6074;
                Editable=FALSE }

    { 1905885101;1;Group  ;
                CaptionML=[ENU=Invoicing;
                           PTG=Fatura��o] }

    { 56  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the number of the customer you send the invoice for the filed service contract or contract quote to.;
                           PTG=""];
                SourceExpr="Bill-to Customer No." }

    { 63  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the number of the contact you have chosen to send an invoice to.;
                           PTG=""];
                SourceExpr="Bill-to Contact No." }

    { 44  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the name of the customer you will send the invoice for the filed service contract or contract quote to.;
                           PTG=""];
                SourceExpr="Bill-to Name" }

    { 45  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the address of the customer you will send the invoice for the filed service contract or contract quote to.;
                           PTG=""];
                SourceExpr="Bill-to Address" }

    { 46  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies an additional line of the address.;
                           PTG=""];
                SourceExpr="Bill-to Address 2" }

    { 47  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the postal code of the address.;
                           PTG=""];
                SourceExpr="Bill-to Post Code" }

    { 43  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the city of the address.;
                           PTG=""];
                SourceExpr="Bill-to City" }

    { 65  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the name of the contact person from your customer's company who receives the invoice for the service order.;
                           PTG=""];
                SourceExpr="Bill-to Contact" }

    { 60  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the customer's reference number.;
                           PTG=""];
                SourceExpr="Your Reference" }

    { 58  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the code of the service contract account group that the filed service contract is associated with.;
                           PTG=""];
                SourceExpr="Serv. Contract Acc. Gr. Code" }

    { 37  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the dimension value code for the document.;
                           PTG=""];
                SourceExpr="Shortcut Dimension 1 Code" }

    { 36  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies a dimension value code for the document line.;
                           PTG=""];
                SourceExpr="Shortcut Dimension 2 Code" }

    { 35  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the payment terms code for the customer in the filed service contract or contract quote.;
                           PTG=""];
                SourceExpr="Payment Terms Code" }

    { 38  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the currency code of the amounts in the filed service contract or contract quote.;
                           PTG=""];
                SourceExpr="Currency Code" }

    { 1906801201;1;Group  ;
                CaptionML=[ENU=Shipping;
                           PTG=Envio] }

    { 33  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the ship-to code for the customer in the filed service contract or contract quote.;
                           PTG=""];
                SourceExpr="Ship-to Code" }

    { 31  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the name of the customer at the address where the service items in the filed contract or contract quote are located.;
                           PTG=""];
                SourceExpr="Ship-to Name" }

    { 27  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the address where the service items in the filed contract or contract quote are located.;
                           PTG=""];
                SourceExpr="Ship-to Address" }

    { 26  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies an additional line of the address.;
                           PTG=""];
                SourceExpr="Ship-to Address 2" }

    { 11  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the postal code of the address.;
                           PTG=""];
                SourceExpr="Ship-to Post Code" }

    { 10  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the city of the address.;
                           PTG=""];
                SourceExpr="Ship-to City" }

    { 1902138501;1;Group  ;
                CaptionML=[ENU=Service;
                           PTG=Servi�o] }

    { 18  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the code of the service zone of the customer's ship-to address.;
                           PTG=""];
                SourceExpr="Service Zone Code" }

    { 4   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the default service period for the service items in the filed service contract or contract quote.;
                           PTG=""];
                SourceExpr="Service Period" }

    { 189 ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the date of the first expected service for the service items in the filed service contract or contract quote.;
                           PTG=""];
                SourceExpr="First Service Date" }

    { 191 ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the default response time for the service items in the filed service contract or contract quote.;
                           PTG=""];
                SourceExpr="Response Time (Hours)" }

    { 193 ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the service order type assigned to service orders linked to this filed service contract or contract quote.;
                           PTG=""];
                SourceExpr="Service Order Type" }

    { 1905361901;1;Group  ;
                CaptionML=[ENU=Invoice Details;
                           PTG=Detalhes Fatura] }

    { 219 ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the amount that was invoiced annually before the service contract or contract quote was filed.;
                           PTG=""];
                SourceExpr="Annual Amount" }

    { 233 ;2   ;Field     ;
                ToolTipML=[ENU=Specifies whether the Annual Amount field on the contract or quote is modified automatically or manually.;
                           PTG=""];
                SourceExpr="Allow Unbalanced Amounts" }

    { 21  ;2   ;Field     ;
                Name=Calcd. Annual Amount;
                ToolTipML=[ENU=Specifies the sum of the Line Amount field values on all contract lines associated with the filed service contract or contract quote.;
                           PTG=""];
                SourceExpr="Calcd. Annual Amount" }

    { 223 ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the invoice period for the filed service contract or contract quote.;
                           PTG=""];
                SourceExpr="Invoice Period" }

    { 225 ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the next invoice date for this filed service contract or contract quote.;
                           PTG=""];
                SourceExpr="Next Invoice Date" }

    { 227 ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the amount that will be invoiced for each invoice period for the filed service contract or contract quote.;
                           PTG=""];
                SourceExpr="Amount per Period" }

    { 221 ;2   ;Field     ;
                CaptionML=[ENU=Next Invoice Period;
                           PTG=Pr�ximo Per�odo Fatura];
                SourceExpr=NextInvoicePeriod }

    { 229 ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the invoice date when this filed service contract was last invoiced.;
                           PTG=""];
                SourceExpr="Last Invoice Date" }

    { 245 ;2   ;Field     ;
                ToolTipML=[ENU=Specifies that this filed service contract or contract quote is prepaid.;
                           PTG=""];
                SourceExpr=Prepaid }

    { 287 ;2   ;Field     ;
                ToolTipML=[ENU=Specifies that you want a credit memo created when you remove a contract line from the filed service contract.;
                           PTG=""];
                SourceExpr="Automatic Credit Memos" }

    { 239 ;2   ;Field     ;
                ToolTipML=[ENU=Specifies you can only invoice the contract if you have posted a service order since last time you invoiced the contract.;
                           PTG=""];
                SourceExpr="Invoice after Service" }

    { 237 ;2   ;Field     ;
                ToolTipML=[ENU=Specifies you want to combine invoices for this filed service contract with invoices for other service contracts with the same bill-to customer.;
                           PTG=""];
                SourceExpr="Combine Invoices" }

    { 235 ;2   ;Field     ;
                ToolTipML=[ENU=Specifies that you want the contract lines for this service contract to appear as text on the invoice created when you invoice the contract.;
                           PTG=""];
                SourceExpr="Contract Lines on Invoice" }

    { 1904390801;1;Group  ;
                CaptionML=[ENU=Price Update;
                           PTG=Atualiza��o Pre�o] }

    { 6   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the price update period for this filed service contract or contract quote.;
                           PTG=""];
                SourceExpr="Price Update Period" }

    { 257 ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the next date when you want contract prices to be updated.;
                           PTG=""];
                SourceExpr="Next Price Update Date" }

    { 259 ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the price update percentage you used when you last updated the contract prices.;
                           PTG=""];
                SourceExpr="Last Price Update %" }

    { 261 ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the date when you last updated the service contract prices.;
                           PTG=""];
                SourceExpr="Last Price Update Date" }

    { 215 ;2   ;Field     ;
                ToolTipML=[ENU=Prints the price increase text specified on invoices for this contract, informing the customer which prices have been updated.;
                           PTG=""];
                SourceExpr="Print Increase Text" }

    { 213 ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the standard text code to print on service invoices for this filed service contract, informing the customer which prices have been updated.;
                           PTG=""];
                SourceExpr="Price Inv. Increase Code" }

    { 1906484701;1;Group  ;
                CaptionML=[ENU=Detail;
                           PTG=Detalhe] }

    { 12  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the cancel reason code specified in a service contract or a contract quote at the moment of filing.;
                           PTG=""];
                SourceExpr="Cancel Reason Code" }

    { 285 ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the maximum unit price that can be set for a resource on all service order lines for to the filed service contract or contract quote.;
                           PTG=""];
                SourceExpr="Max. Labor Unit Price" }

    { 1904882701;1;Group  ;
                CaptionML=[ENU=Filed Detail;
                           PTG=Detalhe Arquivo] }

    { 289 ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the user ID of the person who filed the copy of the service contract or contract quote.;
                           PTG=""];
                SourceExpr="Entry No." }

    { 291 ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the date when service contract or contract quote is filed.;
                           PTG=""];
                SourceExpr="File Date" }

    { 293 ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the time when the service contract or contract quote is filed.;
                           PTG=""];
                SourceExpr="File Time" }

    { 297 ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the reason for filing the service contract or contract quote.;
                           PTG=""];
                SourceExpr="Reason for Filing" }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {

    BEGIN
    END.
  }
}

