OBJECT Page 5758 Posted Transfer Shipment Lines
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0;
  }
  PROPERTIES
  {
    Editable=No;
    CaptionML=[ENU=Posted Transfer Shipment Lines;
               PTG=Hist. Linhas Envios Transf.];
    SourceTable=Table5745;
    PageType=List;
    OnAfterGetRecord=BEGIN
                       DocumentNoHideValue := FALSE;
                       DocumentNoOnFormat;
                     END;

    ActionList=ACTIONS
    {
      { 1900000003;0 ;ActionContainer;
                      ActionContainerType=RelatedInformation }
      { 17      ;1   ;ActionGroup;
                      CaptionML=[ENU=&Line;
                                 PTG=&Linha];
                      Image=Line }
      { 18      ;2   ;Action    ;
                      ShortCutKey=Shift+F7;
                      CaptionML=[ENU=Show Document;
                                 PTG=Mostrar Documento];
                      Image=View;
                      OnAction=VAR
                                 TransShptHeader@1001 : Record 5744;
                               BEGIN
                                 TransShptHeader.GET("Document No.");
                                 PAGE.RUN(PAGE::"Posted Transfer Shipment",TransShptHeader);
                               END;
                                }
      { 19      ;2   ;Action    ;
                      AccessByPermission=TableData 348=R;
                      ShortCutKey=Shift+Ctrl+D;
                      CaptionML=[ENU=Dimensions;
                                 PTG=Dimens�es];
                      ToolTipML=[ENU=View or edit dimensions, such as area, project, or department, that you can assign to sales and purchase documents to distribute costs and analyze transaction history.;
                                 PTG=""];
                      Image=Dimensions;
                      OnAction=BEGIN
                                 ShowDimensions;
                               END;
                                }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 2   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the document number associated with this transfer line.;
                           PTG=""];
                SourceExpr="Document No.";
                HideValue=DocumentNoHideValue;
                StyleExpr='Strong' }

    { 4   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the number of the item that will be transferred.;
                           PTG=""];
                SourceExpr="Item No." }

    { 6   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies a description of the entry.;
                           PTG=""];
                SourceExpr=Description }

    { 8   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the quantity of the item specified on the line.;
                           PTG=""];
                SourceExpr=Quantity }

    { 10  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the unit of measure code of the item.;
                           PTG=""];
                SourceExpr="Unit of Measure" }

    { 12  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the shipment date of the transfer shipment line.;
                           PTG=""];
                SourceExpr="Shipment Date" }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {
    VAR
      TempTransShptLine@1000 : TEMPORARY Record 5745;
      DocumentNoHideValue@19020538 : Boolean INDATASET;

    LOCAL PROCEDURE IsFirstLine@2(DocNo@1000 : Code[20];LineNo@1001 : Integer) : Boolean;
    VAR
      TransShptLine@1002 : Record 5745;
    BEGIN
      TempTransShptLine.RESET;
      TempTransShptLine.COPYFILTERS(Rec);
      TempTransShptLine.SETRANGE("Document No.",DocNo);
      IF NOT TempTransShptLine.FINDFIRST THEN BEGIN
        TransShptLine.COPYFILTERS(Rec);
        TransShptLine.SETRANGE("Document No.",DocNo);
        TransShptLine.FINDFIRST;
        TempTransShptLine := TransShptLine;
        TempTransShptLine.INSERT;
      END;
      IF TempTransShptLine."Line No." = LineNo THEN
        EXIT(TRUE);
    END;

    LOCAL PROCEDURE DocumentNoOnFormat@19001080();
    BEGIN
      IF NOT IsFirstLine("Document No.","Line No.") THEN
        DocumentNoHideValue := TRUE;
    END;

    BEGIN
    END.
  }
}

