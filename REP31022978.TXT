OBJECT Report 31022978 Settle Docs. in Posted PO
{
  OBJECT-PROPERTIES
  {
    Date=13/02/15;
    Time=13:00:00;
    Version List=NAVPTSS82.00;
  }
  PROPERTIES
  {
    Permissions=TableData 21=imd,
                TableData 25=imd,
                TableData 31022936=imd,
                TableData 31022937=imd,
                TableData 31022953=imd,
                TableData 31022954=imd;
    CaptionML=[ENU=Settle Docs. in Posted PO;
               PTG=Liquidar Documentos na OP Registadas];
    ProcessingOnly=Yes;
    OnInitReport=BEGIN
                   PostingDate := WORKDATE;
                 END;

    OnPreReport=BEGIN
                  GLSetup.GET;
                END;

  }
  DATASET
  {
    { 2883;    ;DataItem;PostedDoc           ;
               DataItemTable=Table31022936;
               DataItemTableView=SORTING(Bill Gr./Pmt. Order No.,Status,Category Code,Redrawn,Due Date)
                                 WHERE(Status=CONST(Open));
               OnPreDataItem=BEGIN
                               DocPost.CheckPostingDate(PostingDate);

                               SourceCodeSetup.GET;
                               SourceCode := SourceCodeSetup."Cartera Journal";
                               DocCount :=0;
                               SumLCYAmt := 0;
                               GenJnlLineNextNo := 0;
                               ExistInvoice := FALSE;
                               ExistVATEntry := FALSE;
                               HasRecords := NOT ISEMPTY;
                               Window.OPEN(
                                 Text31022890);
                               IF HasRecords AND GLSetup."Unrealized VAT" THEN BEGIN
                                 VATEntry.LOCKTABLE;
                                 IF VATEntry.FINDLAST THEN
                                   FromVATEntryNo := VATEntry."Entry No." + 1;
                               END;
                             END;

               OnAfterGetRecord=BEGIN
                                  IsRedrawn := CarteraManagement.CheckFromRedrawnDoc(PostedDoc."No.");
                                  IF PostedDoc."Document Type" = PostedDoc."Document Type"::Invoice THEN
                                    ExistInvoice := TRUE;

                                  PostedPmtOrd.GET(PostedDoc."Bill Gr./Pmt. Order No.");
                                  BankAcc.GET(PostedPmtOrd."Bank Account No.");
                                  Delay := BankAcc."Delay for Notices";

                                  IF DueOnly AND (PostingDate < "Due Date" + Delay) THEN
                                    CurrReport.SKIP;

                                  DocCount := DocCount + 1;
                                  Window.UPDATE(1,DocCount);

                                  CASE "Document Type" OF
                                    PostedDoc."Document Type"::Invoice,PostedDoc."Document Type"::"1":
                                      BEGIN
                                        WITH GenJnlLine DO BEGIN
                                          GenJnlLineNextNo := GenJnlLineNextNo + 10000;
                                          CLEAR(GenJnlLine);
                                          INIT;
                                          "Line No." := GenJnlLineNextNo;
                                          "Posting Date" := PostingDate;
                                          "Reason Code" := PostedPmtOrd."Reason Code";
                                          GenJnlLine."Document Date" := "Document Date";
                                          VALIDATE("Account Type","Account Type"::Vendor);
                                          VendLedgEntry.GET(PostedDoc."Entry No.");
                                          VALIDATE("Account No.",VendLedgEntry."Vendor No.");
                                          "Document Type" := "Document Type"::Payment;
                                          Description := COPYSTR(
                                            STRSUBSTNO(Text31022891,PostedDoc."Document No."),
                                            1,MAXSTRLEN(Description));
                                          "Document No." := PostedPmtOrd."No.";
                                          VALIDATE("Currency Code",PostedDoc."Currency Code");
                                          VALIDATE(Amount,PostedDoc."Remaining Amount" );
                                          "Applies-to Doc. Type" := VendLedgEntry."Document Type";
                                          "Applies-to Doc. No." := VendLedgEntry."Document No.";
                                          "Applies-to Bill No." := VendLedgEntry."Bill No.";
                                          "Source Code" := SourceCode;
                                          "Global Dimension 1 Code" := VendLedgEntry."Global Dimension 1 Code";
                                          "System-Created Entry" := TRUE;
                                          "Global Dimension 1 Code" := VendLedgEntry."Global Dimension 1 Code";
                                          "Global Dimension 2 Code" := VendLedgEntry."Global Dimension 2 Code";
                                          "Dimension Set ID" := VendLedgEntry."Dimension Set ID";
                                          INSERT;
                                          SumLCYAmt := SumLCYAmt + "Amount (LCY)";
                                        END;

                                        GroupAmount := GroupAmount + "Remaining Amount";
                                        CalcBankAccount("No.","Remaining Amount",VendLedgEntry."Entry No.");
                                        VendLedgEntry."Document Status" := VendLedgEntry."Document Status"::Honored;
                                        VendLedgEntry.MODIFY;
                                      END;
                                    PostedDoc."Document Type"::Bill:
                                      BEGIN
                                        WITH GenJnlLine DO BEGIN
                                          GenJnlLineNextNo := GenJnlLineNextNo + 10000;
                                          CLEAR(GenJnlLine);
                                          INIT;
                                          "Line No." := GenJnlLineNextNo;
                                          "Posting Date" := PostingDate;
                                          "Document Type" := "Document Type"::Payment;
                                          "Document No." := PostedPmtOrd."No.";
                                          "Reason Code" := PostedPmtOrd."Reason Code";
                                          VALIDATE("Account Type","Account Type"::Vendor);
                                          VendLedgEntry.GET(PostedDoc."Entry No.");

                                          IF GLSetup."Unrealized VAT" THEN BEGIN
                                            ExistsNoRealVAT := CarteraManagement.FindVendVATSetup(VATPostingSetup,VendLedgEntry);
                                          END;

                                          VALIDATE("Account No.",VendLedgEntry."Vendor No.");
                                          Description := COPYSTR(
                                            STRSUBSTNO(Text31022892,PostedDoc."Document No.",PostedDoc."No."),
                                            1,MAXSTRLEN(Description));
                                          VALIDATE("Currency Code",PostedDoc."Currency Code");
                                          VALIDATE(Amount,PostedDoc."Remaining Amount");
                                          "Applies-to Doc. Type" := VendLedgEntry."Document Type";
                                          "Applies-to Doc. No." := VendLedgEntry."Document No.";
                                          "Applies-to Bill No." := VendLedgEntry."Bill No.";
                                          "Source Code" := SourceCode;
                                          "System-Created Entry" := TRUE;
                                          "Global Dimension 1 Code" := VendLedgEntry."Global Dimension 1 Code";
                                          "Global Dimension 2 Code" := VendLedgEntry."Global Dimension 2 Code";
                                          "Dimension Set ID" := VendLedgEntry."Dimension Set ID";
                                          INSERT;
                                          SumLCYAmt := SumLCYAmt + "Amount (LCY)";
                                        END;
                                        IF GLSetup."Unrealized VAT" AND
                                          ExistsNoRealVAT AND
                                          (NOT IsRedrawn) THEN BEGIN
                                          VendLedgEntry.CALCFIELDS("Remaining Amount","Remaining Amt. (LCY)");
                                          CarteraManagement.VendUnrealizedVAT2(
                                            VendLedgEntry,
                                            VendLedgEntry."Remaining Amt. (LCY)",
                                            GenJnlLine,
                                            ExistVATEntry,
                                            FirstVATEntryNo,
                                            LastVATEntryNo,
                                            NoRealVATBuffer);
                                            TempCurrCode := "Currency Code";
                                            "Currency Code" := '';

                                          IF NoRealVATBuffer.FINDSET THEN BEGIN
                                            REPEAT
                                              InsertGenJournalLine(
                                                GenJnlLine."Account Type"::"G/L Account",
                                                NoRealVATBuffer.Account,
                                                NoRealVATBuffer.Amount,
                                                PostedDoc."Global Dimension 1 Code",
                                                PostedDoc."Global Dimension 2 Code",
                                                PostedDoc."Dimension Set ID");
                                              InsertGenJournalLine(
                                                GenJnlLine."Account Type"::"G/L Account",
                                                NoRealVATBuffer."Balance Account",
                                                -NoRealVATBuffer.Amount,
                                                PostedDoc."Global Dimension 1 Code",
                                                PostedDoc."Global Dimension 2 Code",
                                                PostedDoc."Dimension Set ID");
                                            UNTIL NoRealVATBuffer.NEXT = 0;
                                            NoRealVATBuffer.DELETEALL;
                                          END;

                                           "Currency Code" := TempCurrCode;
                                         END;
                                        GroupAmount := GroupAmount + "Remaining Amount";
                                        CalcBankAccount("No.","Remaining Amount",VendLedgEntry."Entry No.");
                                        VendLedgEntry."Document Status" := VendLedgEntry."Document Status"::Honored;
                                        VendLedgEntry.MODIFY;
                                      END;
                                  END;
                                END;

               OnPostDataItem=VAR
                                VendLedgEntry2@1110001 : Record 25;
                                PostedDoc2@1110002 : Record 31022936;
                              BEGIN
                                IF (DocCount = 0) OR (GroupAmount = 0) THEN BEGIN
                                  IF DueOnly THEN
                                    ERROR(
                                      Text31022893 +
                                      Text31022894)
                                  ELSE
                                    ERROR(
                                      Text31022893 +
                                      Text31022895);
                                END;
                                IF BankAccPostBuffer.FINDSET THEN
                                  REPEAT
                                    VendLedgEntry2.GET(BankAccPostBuffer."Entry No.");
                                    PostedDoc2.GET(1,VendLedgEntry2."Entry No.");
                                    PostedPmtOrd.GET(PostedDoc2."Bill Gr./Pmt. Order No.");
                                    BankAcc.GET(PostedPmtOrd."Bank Account No.");
                                    GenJnlLineNextNo := GenJnlLineNextNo + 10000;
                                    WITH GenJnlLine DO BEGIN
                                      CLEAR(GenJnlLine);
                                      INIT;
                                      "Line No." := GenJnlLineNextNo;
                                      "Posting Date" := PostingDate;
                                      "Document Type" := "Document Type"::Payment;
                                      "Document No." := PostedPmtOrd."No.";
                                      "Reason Code" := PostedPmtOrd."Reason Code";
                                      VALIDATE("Account Type",GenJnlLine."Account Type"::"Bank Account");
                                      VALIDATE("Account No.",BankAcc."No.");
                                      Description := COPYSTR(STRSUBSTNO(Text31022896,PostedPmtOrd."No."),1,MAXSTRLEN(Description));
                                      VALIDATE("Currency Code",PostedPmtOrd."Currency Code");
                                      VALIDATE(Amount,-BankAccPostBuffer.Amount);
                                      "Source Code" := SourceCode;
                                      "Global Dimension 1 Code" := BankAccPostBuffer."Global Dimension 1 Code";
                                      "Global Dimension 2 Code" := BankAccPostBuffer."Global Dimension 2 Code";
                                      "Dimension Set ID" := BankAccPostBuffer."Dimension Set ID";
                                      "System-Created Entry" := TRUE;
                                      INSERT;
                                      SumLCYAmt := SumLCYAmt + "Amount (LCY)";
                                    END;
                                  UNTIL BankAccPostBuffer.NEXT = 0;

                                IF PostedPmtOrd."Currency Code" <> '' THEN BEGIN
                                  IF SumLCYAmt <> 0 THEN BEGIN
                                    Currency.GET(PostedPmtOrd."Currency Code");
                                    IF SumLCYAmt > 0 THEN BEGIN
                                      Currency.TESTFIELD("Residual Gains Account");
                                      Acct := Currency."Residual Gains Account";
                                    END ELSE BEGIN
                                      Currency.TESTFIELD("Residual Losses Account");
                                      Acct := Currency."Residual Losses Account";
                                    END;
                                    GenJnlLineNextNo := GenJnlLineNextNo + 10000;
                                    WITH GenJnlLine DO BEGIN
                                      CLEAR(GenJnlLine);
                                      INIT;
                                      "Line No." := GenJnlLineNextNo;
                                      "Posting Date" := PostingDate;
                                      "Document Type" := "Document Type"::Payment;
                                      "Document No." := PostedPmtOrd."No.";
                                      "Reason Code" := PostedPmtOrd."Reason Code";
                                      VALIDATE("Account Type","Account Type"::"G/L Account");
                                      VALIDATE("Account No.",Acct);
                                      Description := Text31022897;
                                      VALIDATE("Currency Code",'');
                                      VALIDATE(Amount,-SumLCYAmt);
                                      "Source Code" := SourceCode;
                                      "System-Created Entry" := TRUE;
                                      INSERT;
                                    END;
                                  END;
                                END;
                                IF GenJnlLine.FINDSET THEN
                                  REPEAT
                                    GenJnlLine2 := GenJnlLine;
                                    GenJnlPostLine.SetFromSettlement(TRUE);
                                    GenJnlPostLine.RunWithCheck(GenJnlLine2);
                                    IF PostedPmtOrd.GET(GenJnlLine."Document No.") THEN
                                      DocPost.ClosePmtOrdIfEmpty(PostedPmtOrd,PostingDate);
                                  UNTIL GenJnlLine.NEXT = 0;

                                Window.CLOSE;

                                IF HasRecords AND GLSetup."Unrealized VAT" AND
                                  ExistVATEntry AND ExistInvoice THEN BEGIN
                                  IF VATEntry.FINDLAST THEN
                                    ToVATEntryNo := VATEntry."Entry No.";
                                  GLReg.FINDLAST;
                                  GLReg."From VAT Entry No." := FromVATEntryNo;
                                  GLReg."To VAT Entry No." := ToVATEntryNo;
                                  GLReg.MODIFY;
                                END ELSE BEGIN
                                  IF ExistVATEntry THEN BEGIN
                                    GLReg.FINDLAST;
                                    GLReg."From VAT Entry No." := FirstVATEntryNo;
                                    GLReg."To VAT Entry No." := LastVATEntryNo;
                                    GLReg.MODIFY;
                                  END;
                                END;

                                COMMIT;

                                IF NOT HidePrintDialog THEN
                                  MESSAGE(Text31022898,DocCount,GroupAmount,PostedPmtOrd."No.");
                              END;
                               }

  }
  REQUESTPAGE
  {
    PROPERTIES
    {
      SaveValues=Yes;
    }
    CONTROLS
    {
      { 1900000001;0;Container;
                  ContainerType=ContentArea }

      { 1900000002;1;Group  ;
                  CaptionML=[ENU=Options;
                             PTG=Op��es] }

      { 5   ;2   ;Field     ;
                  CaptionML=[ENU=Posting Date;
                             PTG=Data Registo];
                  NotBlank=Yes;
                  SourceExpr=PostingDate }

      { 6   ;2   ;Field     ;
                  CaptionML=[ENU=Due bills only;
                             PTG=Apenas Letras Vencidas];
                  SourceExpr=DueOnly }

    }
  }
  LABELS
  {
  }
  CODE
  {
    VAR
      Text31022890@1000 : TextConst 'ENU=Settling payable documents     #1######;PTG=A liquidar documentos a pagar     #1######';
      Text31022891@1001 : TextConst 'ENU=Payable document settlement %1;PTG=Liquida��o de documento a pagar %1';
      Text31022892@1002 : TextConst 'ENU=Payable bill settlement %1/%2;PTG=Liquida��o letra a pagar %1/%2';
      Text31022893@1003 : TextConst 'ENU=No payable documents have been found that can be settled.;PTG=N�o foram encontrados documentos a pagar que possam ser liquidados.';
      Text31022894@1004 : TextConst 'ENU=Please check that the selection is not empty and at least one payable document is open and due.;PTG=Por favor verifique que a sele��o n�o est� vazia e que pelo menos um documento a pagar est� aberto e � devido.';
      Text31022895@1005 : TextConst 'ENU=Please check that the selection is not empty and at least one payable document is open.;PTG=Por favor verifique que a sele��o n�o est� vazia e que pelo menos um documento a pagar est� aberto.';
      Text31022896@1006 : TextConst 'ENU=Payment Order settlement %1;PTG=Liquida��o de Ordem Pagamento %1';
      Text31022897@1007 : TextConst 'ENU=Residual adjust generated by rounding Amount;PTG=Ajuste residual gerado pelo arredondamento do Valor';
      Text31022898@1008 : TextConst 'ENU=%1 documents totaling %2 have been settled in Payment Order %3.;PTG=%1 documentos que totalizam %2 foram liquidados pela Ordem Pagamento %3.';
      Text31022899@1009 : TextConst 'ENU=Document settlement %1/%2;PTG=Liquida��o documento %1/%2';
      BankAccPostBuffer@1000000001 : TEMPORARY Record 31022945;
      NoRealVATBuffer@1000000000 : TEMPORARY Record 31022945;
      SourceCodeSetup@1110000 : Record 242;
      PostedPmtOrd@1110001 : Record 31022953;
      GenJnlLine@1110002 : TEMPORARY Record 81;
      GenJnlLine2@1110003 : Record 81;
      VendLedgEntry@1110004 : Record 25;
      BankAccPostingGr@1110005 : Record 277;
      BankAcc@1110006 : Record 270;
      CurrExchRate@1110007 : Record 330;
      Currency@1110008 : Record 4;
      GLReg@1110009 : Record 45;
      GLSetup@1110010 : Record 98;
      VATPostingSetup@1110011 : Record 325;
      VATEntry@1110012 : Record 254;
      DocPost@1110013 : Codeunit 31022906;
      CarteraManagement@1110014 : Codeunit 31022901;
      GenJnlPostLine@1110015 : Codeunit 12;
      DueOnly@1000000002 : Boolean;
      ExistVATEntry@1000000003 : Boolean;
      IsRedrawn@1000000005 : Boolean;
      ExistInvoice@1000000004 : Boolean;
      ExistsNoRealVAT@1000000007 : Boolean;
      HidePrintDialog@1000000006 : Boolean;
      HasRecords@1000000008 : Boolean;
      Window@1110017 : Dialog;
      PostingDate@1110018 : Date;
      Delay@1110020 : Decimal;
      SourceCode@1110021 : Code[10];
      Acct@1110022 : Code[20];
      DocCount@1110023 : Integer;
      GroupAmount@1110024 : Decimal;
      GenJnlLineNextNo@1110025 : Integer;
      SumLCYAmt@1110026 : Decimal;
      FirstVATEntryNo@1110028 : Integer;
      LastVATEntryNo@1110029 : Integer;
      FromVATEntryNo@1110032 : Integer;
      ToVATEntryNo@1110033 : Integer;
      TempCurrCode@1110035 : Code[10];

    LOCAL PROCEDURE InsertGenJournalLine@4(AccType@1110000 : Integer;AccNo@1110001 : Code[20];Amount2@1110002 : Decimal;Dep@1110003 : Code[20];Proj@1110004 : Code[20];DimSetId@1000000000 : Integer);
    BEGIN
      GenJnlLineNextNo :=  GenJnlLineNextNo + 10000;

      WITH GenJnlLine DO BEGIN
        CLEAR(GenJnlLine);
        INIT;
        "Line No." := GenJnlLineNextNo;
        "Posting Date" := PostingDate;
        "Document Type" := "Document Type"::Payment;
        "Document No." := PostedPmtOrd."No.";
        "Reason Code" := PostedPmtOrd."Reason Code";
        "Account Type" := AccType;
        "Account No." := AccNo;
        IF PostedDoc."Document Type" = PostedDoc."Document Type"::Bill THEN
          Description := COPYSTR(
            STRSUBSTNO(Text31022899,PostedDoc."Document No.",PostedDoc."No."),
            1,MAXSTRLEN(Description))
        ELSE
          Description := COPYSTR(
            STRSUBSTNO(Text31022899,PostedDoc."Document No.",PostedDoc."No."),
            1,MAXSTRLEN(Description));
        VALIDATE("Currency Code",PostedDoc."Currency Code");
        VALIDATE(Amount,-Amount2);
        "Applies-to Doc. Type" := VendLedgEntry."Document Type";
        "Applies-to Doc. No." := '';
        "Applies-to Bill No." := VendLedgEntry."Bill No.";
        "Source Code" := SourceCode;
        "System-Created Entry" := TRUE;
        "Shortcut Dimension 1 Code" := Dep;
        "Shortcut Dimension 2 Code" := Proj;
        SumLCYAmt := SumLCYAmt + "Amount (LCY)";
        "Dimension Set ID" := DimSetId;
        INSERT;
      END;
    END;

    PROCEDURE CalcBankAccount@5(BankAcc2@1110000 : Code[20];Amount2@1110001 : Decimal;EntryNo@1110002 : Integer);
    BEGIN
      IF BankAccPostBuffer.GET(BankAcc2,'',EntryNo) THEN BEGIN
        BankAccPostBuffer.Amount := BankAccPostBuffer.Amount + Amount2;
        BankAccPostBuffer.MODIFY;
      END ELSE BEGIN
        BankAccPostBuffer.INIT;
        BankAccPostBuffer.Account := BankAcc2;
        BankAccPostBuffer.Amount := Amount2;
        BankAccPostBuffer."Entry No." := EntryNo;
        BankAccPostBuffer."Global Dimension 1 Code" := VendLedgEntry."Global Dimension 1 Code";
        BankAccPostBuffer."Global Dimension 2 Code" := VendLedgEntry."Global Dimension 2 Code";
        BankAccPostBuffer."Dimension Set ID" := VendLedgEntry."Dimension Set ID"; //NAVPT,n
        BankAccPostBuffer.INSERT;
      END;
    END;

    PROCEDURE SetHidePrintDialog@1110100(NewHidePrintDialog@1110000 : Boolean);
    BEGIN
      HidePrintDialog := NewHidePrintDialog;
    END;

    BEGIN
    END.
  }
  RDLDATA
  {
  }
}

