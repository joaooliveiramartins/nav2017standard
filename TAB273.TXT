OBJECT Table 273 Bank Acc. Reconciliation
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0.00.15052;
  }
  PROPERTIES
  {
    Permissions=TableData 1220=rimd;
    DataCaptionFields=Bank Account No.,Statement No.;
    OnInsert=BEGIN
               TESTFIELD("Statement No.");
               TESTFIELD("Bank Account No.");
               BankAcc.GET("Bank Account No.");
               CASE "Statement Type" OF
                 "Statement Type"::"Bank Reconciliation":
                   BEGIN
                     IF PostedBankAccStmt.GET("Bank Account No.","Statement No.") THEN
                       ERROR(DuplicateStatementErr,"Statement No.");
                     BankAcc."Last Statement No." := "Statement No.";
                   END;
                 "Statement Type"::"Payment Application":
                   BEGIN
                     IF PostedPaymentReconHdr.GET("Bank Account No.","Statement No.") THEN
                       ERROR(DuplicateStatementErr,"Statement No.");
                     BankAcc."Last Payment Statement No." := "Statement No.";
                   END;
               END;

               BankAcc.MODIFY;
             END;

    OnDelete=BEGIN
               IF BankAccReconLine.LinesExist(Rec) THEN
                 BankAccReconLine.DELETEALL(TRUE);
             END;

    OnRename=BEGIN
               ERROR(RenameErr,TABLECAPTION);
             END;

    CaptionML=[ENU=Bank Acc. Reconciliation;
               PTG=Reconcilia��o Conta Banc�ria];
    LookupPageID=Page388;
  }
  FIELDS
  {
    { 1   ;   ;Bank Account No.    ;Code20        ;TableRelation="Bank Account";
                                                   OnValidate=BEGIN
                                                                IF "Statement No." = '' THEN BEGIN
                                                                  BankAcc.GET("Bank Account No.");

                                                                  IF "Statement Type" = "Statement Type"::"Payment Application" THEN BEGIN
                                                                    SetLastPaymentStatementNo(BankAcc);
                                                                    "Statement No." := INCSTR(BankAcc."Last Payment Statement No.");
                                                                  END ELSE BEGIN
                                                                    SetLastStatementNo(BankAcc);
                                                                    "Statement No." := INCSTR(BankAcc."Last Statement No.");
                                                                  END;

                                                                  "Balance Last Statement" := BankAcc."Balance Last Statement";
                                                                END;

                                                                CreateDim(DATABASE::"Bank Account",BankAcc."No.");
                                                              END;

                                                   CaptionML=[ENU=Bank Account No.;
                                                              PTG=N� Conta Banc�ria];
                                                   NotBlank=Yes }
    { 2   ;   ;Statement No.       ;Code20        ;OnValidate=BEGIN
                                                                TESTFIELD("Bank Account No.");
                                                              END;

                                                   CaptionML=[ENU=Statement No.;
                                                              PTG=N� Extrato];
                                                   NotBlank=Yes;
                                                   Editable=No }
    { 3   ;   ;Statement Ending Balance;Decimal   ;CaptionML=[ENU=Statement Ending Balance;
                                                              PTG=Saldo Final Extrato];
                                                   AutoFormatType=1;
                                                   AutoFormatExpr=GetCurrencyCode }
    { 4   ;   ;Statement Date      ;Date          ;CaptionML=[ENU=Statement Date;
                                                              PTG=Data Extrato] }
    { 5   ;   ;Balance Last Statement;Decimal     ;OnValidate=BEGIN
                                                                TESTFIELD("Statement Type","Statement Type"::"Bank Reconciliation");
                                                                BankAcc.GET("Bank Account No.");
                                                                IF "Balance Last Statement" <> BankAcc."Balance Last Statement" THEN
                                                                  IF NOT
                                                                     CONFIRM(
                                                                       BalanceQst,FALSE,
                                                                       FIELDCAPTION("Balance Last Statement"),BankAcc.FIELDCAPTION("Balance Last Statement"),
                                                                       BankAcc.TABLECAPTION)
                                                                  THEN
                                                                    "Balance Last Statement" := xRec."Balance Last Statement";
                                                              END;

                                                   CaptionML=[ENU=Balance Last Statement;
                                                              PTG=Saldo �ltimo Extrato];
                                                   AutoFormatType=1;
                                                   AutoFormatExpr=GetCurrencyCode }
    { 6   ;   ;Bank Statement      ;BLOB          ;CaptionML=[ENU=Bank Statement;
                                                              PTG=Extrato Conta] }
    { 7   ;   ;Total Balance on Bank Account;Decimal;
                                                   FieldClass=FlowField;
                                                   CalcFormula=Sum("Bank Account Ledger Entry".Amount WHERE (Bank Account No.=FIELD(Bank Account No.)));
                                                   CaptionML=[ENU=Total Balance on Bank Account;
                                                              PTG=Saldo Total na Conta Banc�ria];
                                                   Editable=No;
                                                   AutoFormatType=1;
                                                   AutoFormatExpr=GetCurrencyCode }
    { 8   ;   ;Total Applied Amount;Decimal       ;FieldClass=FlowField;
                                                   CalcFormula=Sum("Bank Acc. Reconciliation Line"."Applied Amount" WHERE (Statement Type=FIELD(Statement Type),
                                                                                                                           Bank Account No.=FIELD(Bank Account No.),
                                                                                                                           Statement No.=FIELD(Statement No.)));
                                                   CaptionML=[ENU=Total Applied Amount;
                                                              PTG=Total Valor Liquidado];
                                                   Editable=No;
                                                   AutoFormatExpr=GetCurrencyCode }
    { 9   ;   ;Total Transaction Amount;Decimal   ;FieldClass=FlowField;
                                                   CalcFormula=Sum("Bank Acc. Reconciliation Line"."Statement Amount" WHERE (Statement Type=FIELD(Statement Type),
                                                                                                                             Bank Account No.=FIELD(Bank Account No.),
                                                                                                                             Statement No.=FIELD(Statement No.)));
                                                   CaptionML=[ENU=Total Transaction Amount;
                                                              PTG=Total Valor Transa��o];
                                                   Editable=No;
                                                   AutoFormatExpr=GetCurrencyCode }
    { 10  ;   ;Total Unposted Applied Amount;Decimal;
                                                   FieldClass=FlowField;
                                                   CalcFormula=Sum("Bank Acc. Reconciliation Line"."Applied Amount" WHERE (Statement Type=FIELD(Statement Type),
                                                                                                                           Bank Account No.=FIELD(Bank Account No.),
                                                                                                                           Statement No.=FIELD(Statement No.),
                                                                                                                           Account Type=FILTER(<>Bank Account)));
                                                   CaptionML=[ENU=Total Unposted Applied Amount;
                                                              PTG=Total Valor por Liquidar por Registar];
                                                   Editable=No;
                                                   AutoFormatExpr=GetCurrencyCode }
    { 11  ;   ;Total Difference    ;Decimal       ;FieldClass=FlowField;
                                                   CalcFormula=Sum("Bank Acc. Reconciliation Line".Difference WHERE (Statement Type=FIELD(Statement Type),
                                                                                                                     Bank Account No.=FIELD(Bank Account No.),
                                                                                                                     Statement No.=FIELD(Statement No.)));
                                                   CaptionML=[ENU=Total Difference;
                                                              PTG=Total Diferen�a];
                                                   Editable=No;
                                                   AutoFormatExpr=GetCurrencyCode }
    { 20  ;   ;Statement Type      ;Option        ;CaptionML=[ENU=Statement Type;
                                                              PTG=Tipo Declara��o];
                                                   OptionCaptionML=[ENU=Bank Reconciliation,Payment Application;
                                                                    PTG=Reconcilia��o Banc�ria,Liquida��o Pagamento];
                                                   OptionString=Bank Reconciliation,Payment Application }
    { 21  ;   ;Shortcut Dimension 1 Code;Code20   ;TableRelation="Dimension Value".Code WHERE (Global Dimension No.=CONST(1));
                                                   OnValidate=BEGIN
                                                                ValidateShortcutDimCode(1,"Shortcut Dimension 1 Code");
                                                              END;

                                                   CaptionML=[ENU=Shortcut Dimension 1 Code;
                                                              PTG=C�d. Atalho Dimens�o 1];
                                                   CaptionClass='1,2,1' }
    { 22  ;   ;Shortcut Dimension 2 Code;Code20   ;TableRelation="Dimension Value".Code WHERE (Global Dimension No.=CONST(2));
                                                   OnValidate=BEGIN
                                                                ValidateShortcutDimCode(2,"Shortcut Dimension 2 Code");
                                                              END;

                                                   CaptionML=[ENU=Shortcut Dimension 2 Code;
                                                              PTG=C�d. Atalho Dimens�o 2];
                                                   CaptionClass='1,2,2' }
    { 23  ;   ;Post Payments Only  ;Boolean       ;CaptionML=[ENU=Post Payments Only;
                                                              PTG=Registar Apenas Pagamentos] }
    { 24  ;   ;Import Posted Transactions;Option  ;CaptionML=[ENU=Import Posted Transactions;
                                                              PTG=Importar Transa��es Registadas];
                                                   OptionCaptionML=[ENU=" ,Yes,No";
                                                                    PTG=" ,Sim,N�o"];
                                                   OptionString=[ ,Yes,No] }
    { 25  ;   ;Total Outstd Bank Transactions;Decimal;
                                                   FieldClass=FlowField;
                                                   CalcFormula=Sum("Bank Account Ledger Entry".Amount WHERE (Bank Account No.=FIELD(Bank Account No.),
                                                                                                             Open=CONST(Yes),
                                                                                                             Check Ledger Entries=CONST(0)));
                                                   CaptionML=[ENU=Total Outstd Bank Transactions;
                                                              PTG=Total Transa��es Banc�rias Pendentes];
                                                   Editable=No;
                                                   AutoFormatExpr=GetCurrencyCode }
    { 26  ;   ;Total Outstd Payments;Decimal      ;FieldClass=FlowField;
                                                   CalcFormula=Sum("Bank Account Ledger Entry".Amount WHERE (Bank Account No.=FIELD(Bank Account No.),
                                                                                                             Open=CONST(Yes),
                                                                                                             Check Ledger Entries=FILTER(>0)));
                                                   CaptionML=[ENU=Total Outstd Payments;
                                                              PTG=Total Pagamentos Pendentes];
                                                   Editable=No;
                                                   AutoFormatExpr=GetCurrencyCode }
    { 27  ;   ;Total Applied Amount Payments;Decimal;
                                                   FieldClass=FlowField;
                                                   CalcFormula=Sum("Bank Acc. Reconciliation Line"."Applied Amount" WHERE (Statement Type=FIELD(Statement Type),
                                                                                                                           Bank Account No.=FIELD(Bank Account No.),
                                                                                                                           Statement No.=FIELD(Statement No.),
                                                                                                                           Type=CONST(Check Ledger Entry)));
                                                   CaptionML=[ENU=Total Applied Amount Payments;
                                                              PTG=Total Valor Pagamentos Liquidados];
                                                   Editable=No;
                                                   AutoFormatExpr=GetCurrencyCode }
    { 28  ;   ;Bank Account Balance (LCY);Decimal ;FieldClass=FlowField;
                                                   CalcFormula=Sum("Bank Account Ledger Entry"."Amount (LCY)" WHERE (Bank Account No.=FIELD(Bank Account No.)));
                                                   CaptionML=[ENU=Bank Account Balance (LCY);
                                                              PTG=Saldo Conta Banc�ria (LCY)];
                                                   Editable=No }
    { 29  ;   ;Total Positive Adjustments;Decimal ;FieldClass=FlowField;
                                                   CalcFormula=Sum("Bank Acc. Reconciliation Line"."Applied Amount" WHERE (Statement Type=FIELD(Statement Type),
                                                                                                                           Bank Account No.=FIELD(Bank Account No.),
                                                                                                                           Statement No.=FIELD(Statement No.),
                                                                                                                           Account Type=FILTER(<>Bank Account),
                                                                                                                           Statement Amount=FILTER(>0)));
                                                   CaptionML=[ENU=Total Positive Adjustments;
                                                              PTG=Total Ajustes Positivos];
                                                   Editable=No;
                                                   AutoFormatExpr=GetCurrencyCode }
    { 30  ;   ;Total Negative Adjustments;Decimal ;FieldClass=FlowField;
                                                   CalcFormula=Sum("Bank Acc. Reconciliation Line"."Applied Amount" WHERE (Statement Type=FIELD(Statement Type),
                                                                                                                           Bank Account No.=FIELD(Bank Account No.),
                                                                                                                           Statement No.=FIELD(Statement No.),
                                                                                                                           Account Type=FILTER(<>Bank Account),
                                                                                                                           Statement Amount=FILTER(<0)));
                                                   CaptionML=[ENU=Total Negative Adjustments;
                                                              PTG=Total Ajustes Negativos];
                                                   Editable=No;
                                                   AutoFormatExpr=GetCurrencyCode }
    { 31  ;   ;Total Positive Difference;Decimal  ;FieldClass=FlowField;
                                                   CalcFormula=Sum("Bank Acc. Reconciliation Line"."Applied Amount" WHERE (Account Type=FIELD(Statement Type),
                                                                                                                           Bank Account No.=FIELD(Bank Account No.),
                                                                                                                           Statement No.=FIELD(Statement No.),
                                                                                                                           Type=CONST(Difference),
                                                                                                                           Applied Amount=FILTER(>0)));
                                                   CaptionML=[ENU=Total Positive Difference;
                                                              PTG=Total Diferen�a Positiva];
                                                   Editable=No;
                                                   AutoFormatExpr=GetCurrencyCode }
    { 32  ;   ;Total Negative Difference;Decimal  ;FieldClass=FlowField;
                                                   CalcFormula=Sum("Bank Acc. Reconciliation Line"."Applied Amount" WHERE (Account Type=FIELD(Statement Type),
                                                                                                                           Bank Account No.=FIELD(Bank Account No.),
                                                                                                                           Statement No.=FIELD(Statement No.),
                                                                                                                           Type=CONST(Difference),
                                                                                                                           Applied Amount=FILTER(<0)));
                                                   CaptionML=[ENU=Total Negative Difference;
                                                              PTG=Total Diferen�a Negativa];
                                                   Editable=No;
                                                   AutoFormatExpr=GetCurrencyCode }
    { 480 ;   ;Dimension Set ID    ;Integer       ;TableRelation="Dimension Set Entry";
                                                   OnLookup=BEGIN
                                                              ShowDocDim;
                                                            END;

                                                   CaptionML=[ENU=Dimension Set ID;
                                                              PTG=ID Combina��o Dimens�o];
                                                   Editable=No }
  }
  KEYS
  {
    {    ;Statement Type,Bank Account No.,Statement No.;
                                                   Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {
    VAR
      DuplicateStatementErr@1000 : TextConst '@@@="%1=Statement No. value";ENU=Statement %1 already exists.;PTG=O extrato %1 j� existe.';
      RenameErr@1001 : TextConst '@@@="%1=Table name caption";ENU=You cannot rename a %1.;PTG=N�o pode renomear %1';
      BalanceQst@1002 : TextConst '@@@="%1=Balance Last Statement field caption;%2=field caption;%3=table caption";ENU=%1 is different from %2 on the %3. Do you want to change the value?;PTG=%1 � diferente de %2 em %3. Deseja alterar o valor?';
      BankAcc@1003 : Record 270;
      BankAccReconLine@1005 : Record 274;
      PostedBankAccStmt@1006 : Record 275;
      PostedPaymentReconHdr@1007 : Record 1295;
      DimMgt@1008 : Codeunit 408;
      YouChangedDimQst@1009 : TextConst 'ENU=You may have changed a dimension.\\Do you want to update the lines?;PTG=Pode ter mudado uma dimens�o.\\Deseja continuar?';
      NoBankAccountsMsg@1010 : TextConst 'ENU=You have not set up a bank account.\To use the payments import process, set up a bank account.;PTG=N�o configurou uma conta banc�ria.\Configure uma conta banc�ria para usar o processo de impora��o de pagamentos.';
      NoBankAccWithFileFormatMsg@1011 : TextConst 'ENU=No bank account exists that is ready for import of bank statement files.\Fill the Bank Statement Import Format field on the card of the bank account that you want to use.;PTG=N�o existe conta banc�ria pronta para importar ficheiros de declara��o banc�ria.\Preencha o campo Formato Importa��o Declara��o Banc�ria na ficha da Conta Banc�ria que quer usar.';
      PostHighConfidentLinesQst@1012 : TextConst 'ENU=All imported bank statement lines were applied with high confidence level.\Do you want to post the payment applications?;PTG=Todas as linhas da declara��o banc�ria importadas foram aplicadas com alto n�vel de confian�a.\Deseja registar as liquida��o de pagamento?';
      MustHaveValueQst@1015 : TextConst 'ENU=The bank account must have a value in %1. Do you want to open the bank account card?;PTG=A conta banc�ria tem de ter um valor em %1. Deseja abrir a ficha da Conta Banc�ria?';
      NoTransactionsImportedMsg@1013 : TextConst 'ENU=No bank transactions were imported.;PTG=N�o foram importadas quaisquer transa��es banc�rias.';

    LOCAL PROCEDURE CreateDim@22(Type1@1000 : Integer;No1@1001 : Code[20]);
    VAR
      SourceCodeSetup@1010 : Record 242;
      TableID@1011 : ARRAY [10] OF Integer;
      No@1012 : ARRAY [10] OF Code[20];
      OldDimSetID@1013 : Integer;
    BEGIN
      SourceCodeSetup.GET;
      TableID[1] := Type1;
      No[1] := No1;
      "Shortcut Dimension 1 Code" := '';
      "Shortcut Dimension 2 Code" := '';
      OldDimSetID := "Dimension Set ID";
      "Dimension Set ID" :=
        DimMgt.GetDefaultDimID(
          TableID,No,SourceCodeSetup."Payment Reconciliation Journal","Shortcut Dimension 1 Code","Shortcut Dimension 2 Code",0,0);

      IF (OldDimSetID <> "Dimension Set ID") AND LinesExist THEN BEGIN
        MODIFY;
        UpdateAllLineDim("Dimension Set ID",OldDimSetID);
      END;
    END;

    LOCAL PROCEDURE GetCurrencyCode@1() : Code[10];
    VAR
      BankAcc2@1000 : Record 270;
    BEGIN
      IF "Bank Account No." = BankAcc2."No." THEN
        EXIT(BankAcc2."Currency Code");

      IF BankAcc2.GET("Bank Account No.") THEN
        EXIT(BankAcc2."Currency Code");

      EXIT('');
    END;

    PROCEDURE MatchSingle@5(DateRange@1008 : Integer);
    VAR
      MatchBankRecLines@1000 : Codeunit 1252;
    BEGIN
      MatchBankRecLines.MatchSingle(Rec,DateRange);
    END;

    PROCEDURE ImportBankStatement@6();
    VAR
      DataExch@1001 : Record 1220;
      ProcessBankAccRecLines@1000 : Codeunit 1248;
    BEGIN
      IF BankAccountCouldBeUsedForImport THEN BEGIN
        DataExch.INIT;
        ProcessBankAccRecLines.ImportBankStatement(Rec,DataExch);
      END;
    END;

    LOCAL PROCEDURE ValidateShortcutDimCode@7(FieldNumber@1000 : Integer;VAR ShortcutDimCode@1001 : Code[20]);
    VAR
      DimMgt@1002 : Codeunit 408;
      OldDimSetID@1003 : Integer;
    BEGIN
      OldDimSetID := "Dimension Set ID";
      DimMgt.ValidateShortcutDimValues(FieldNumber,ShortcutDimCode,"Dimension Set ID");

      IF OldDimSetID <> "Dimension Set ID" THEN BEGIN
        MODIFY;
        UpdateAllLineDim("Dimension Set ID",OldDimSetID);
      END;
    END;

    PROCEDURE ShowDocDim@3();
    VAR
      OldDimSetID@1000 : Integer;
    BEGIN
      OldDimSetID := "Dimension Set ID";
      "Dimension Set ID" :=
        DimMgt.EditDimensionSet2(
          "Dimension Set ID",STRSUBSTNO('%1 %2',TABLECAPTION,"Statement No."),
          "Shortcut Dimension 1 Code","Shortcut Dimension 2 Code");

      IF OldDimSetID <> "Dimension Set ID" THEN BEGIN
        MODIFY;
        UpdateAllLineDim("Dimension Set ID",OldDimSetID);
      END;
    END;

    LOCAL PROCEDURE UpdateAllLineDim@34(NewParentDimSetID@1000 : Integer;OldParentDimSetID@1001 : Integer);
    VAR
      BankAccReconciliationLine@1003 : Record 274;
      NewDimSetID@1002 : Integer;
    BEGIN
      // Update all lines with changed dimensions.
      IF NewParentDimSetID = OldParentDimSetID THEN
        EXIT;

      BankAccReconciliationLine.LOCKTABLE;
      IF BankAccReconciliationLine.LinesExist(Rec) THEN BEGIN
        IF NOT CONFIRM(YouChangedDimQst) THEN
          EXIT;

        REPEAT
          NewDimSetID :=
            DimMgt.GetDeltaDimSetID(BankAccReconciliationLine."Dimension Set ID",NewParentDimSetID,OldParentDimSetID);
          IF BankAccReconciliationLine."Dimension Set ID" <> NewDimSetID THEN BEGIN
            BankAccReconciliationLine."Dimension Set ID" := NewDimSetID;
            DimMgt.UpdateGlobalDimFromDimSetID(
              BankAccReconciliationLine."Dimension Set ID",
              BankAccReconciliationLine."Shortcut Dimension 1 Code",
              BankAccReconciliationLine."Shortcut Dimension 2 Code");
            BankAccReconciliationLine.MODIFY;
          END;
        UNTIL BankAccReconciliationLine.NEXT = 0;
      END;
    END;

    PROCEDURE OpenNewWorksheet@19();
    VAR
      BankAccount@1000 : Record 270;
      BankAccReconciliation@1002 : Record 273;
    BEGIN
      IF NOT SelectBankAccountToUse(BankAccount,FALSE) THEN
        EXIT;

      CreateNewBankPaymentAppBatch(BankAccount."No.",BankAccReconciliation);
      OpenWorksheet(BankAccReconciliation);
    END;

    PROCEDURE ImportAndProcessToNewStatement@8();
    VAR
      BankAccount@1000 : Record 270;
      BankAccReconciliation@1001 : Record 273;
      DataExch@1002 : Record 1220;
      DataExchDef@1003 : Record 1222;
      DummyBankAccReconciliationLine@1005 : Record 274;
      LastStatementNo@1004 : Code[20];
    BEGIN
      IF NOT SelectBankAccountToUse(BankAccount,TRUE) THEN
        EXIT;
      BankAccount.GetDataExchDef(DataExchDef);

      DataExch."Related Record" := BankAccount.RECORDID;
      IF NOT DataExch.ImportFileContent(DataExchDef) THEN
        EXIT;

      BankAccount.LOCKTABLE;
      LastStatementNo := BankAccount."Last Statement No.";
      CreateNewBankPaymentAppBatch(BankAccount."No.",BankAccReconciliation);

      IF NOT ImportStatement(BankAccReconciliation,DataExch) THEN BEGIN
        DeleteBankAccReconciliation(BankAccReconciliation,BankAccount,LastStatementNo);
        MESSAGE(NoTransactionsImportedMsg);
        EXIT;
      END;

      IF NOT DummyBankAccReconciliationLine.LinesExist(BankAccReconciliation) THEN BEGIN
        DeleteBankAccReconciliation(BankAccReconciliation,BankAccount,LastStatementNo);
        MESSAGE(NoTransactionsImportedMsg);
        EXIT;
      END;

      COMMIT;
      ProcessStatement(BankAccReconciliation);
    END;

    LOCAL PROCEDURE DeleteBankAccReconciliation@23(VAR BankAccReconciliation@1000 : Record 273;VAR BankAccount@1002 : Record 270;LastStatementNo@1001 : Code[20]);
    BEGIN
      BankAccReconciliation.DELETE;
      BankAccount.GET(BankAccount."No.");
      BankAccount."Last Statement No." := LastStatementNo;
      BankAccount.MODIFY;
      COMMIT;
    END;

    PROCEDURE ImportStatement@13(VAR BankAccReconciliation@1000 : Record 273;DataExch@1002 : Record 1220) : Boolean;
    VAR
      ProcessBankAccRecLines@1001 : Codeunit 1248;
    BEGIN
      EXIT(ProcessBankAccRecLines.ImportBankStatement(BankAccReconciliation,DataExch))
    END;

    PROCEDURE ProcessStatement@20(VAR BankAccReconciliation@1000 : Record 273);
    BEGIN
      CODEUNIT.RUN(CODEUNIT::"Match Bank Pmt. Appl.",BankAccReconciliation);

      IF ConfidenceLevelPermitToPost(BankAccReconciliation) THEN
        CODEUNIT.RUN(CODEUNIT::"Bank Acc. Reconciliation Post",BankAccReconciliation)
      ELSE
        IF GUIALLOWED THEN
          OpenWorksheet(BankAccReconciliation);
    END;

    PROCEDURE CreateNewBankPaymentAppBatch@16(BankAccountNo@1001 : Code[20];VAR BankAccReconciliation@1000 : Record 273);
    BEGIN
      BankAccReconciliation.INIT;
      BankAccReconciliation."Statement Type" := BankAccReconciliation."Statement Type"::"Payment Application";
      BankAccReconciliation.VALIDATE("Bank Account No.",BankAccountNo);
      BankAccReconciliation.INSERT(TRUE);
    END;

    LOCAL PROCEDURE SelectBankAccountToUse@4(VAR BankAccount@1000 : Record 270;OnlyWithImportFormatSet@1002 : Boolean) : Boolean;
    VAR
      TempBankAccount@1003 : TEMPORARY Record 270;
      TempLinkedBankAccount@1001 : TEMPORARY Record 270;
      NoOfAccounts@1004 : Integer;
    BEGIN
      IF OnlyWithImportFormatSet THEN BEGIN
        // copy to temp as we need OR filter
        BankAccount.SETFILTER("Bank Statement Import Format",'<>%1','');
        CopyBankAccountsToTemp(TempBankAccount,BankAccount);

        // clear filters
        BankAccount.SETRANGE("Bank Statement Import Format");
        TempLinkedBankAccount.SETRANGE("Bank Statement Import Format");

        BankAccount.GetLinkedBankAccounts(TempLinkedBankAccount);
        CopyBankAccountsToTemp(TempBankAccount,TempLinkedBankAccount);

        NoOfAccounts := TempBankAccount.COUNT;
      END ELSE
        NoOfAccounts := BankAccount.COUNT;

      CASE NoOfAccounts OF
        0:
          BEGIN
            IF NOT BankAccount.GET(CantFindBancAccToUseInPaymentFileImport) THEN
              EXIT(FALSE);

            EXIT(TRUE);
          END;
        1:
          BEGIN
            IF TempBankAccount.COUNT > 0 THEN BEGIN
              TempBankAccount.FINDFIRST;
              BankAccount.GET(TempBankAccount."No.");
            END ELSE
              BankAccount.FINDFIRST;
          END;
        ELSE BEGIN
          IF TempBankAccount.COUNT > 0 THEN BEGIN
            IF PAGE.RUNMODAL(PAGE::"Payment Bank Account List",TempBankAccount) = ACTION::LookupOK THEN BEGIN
              BankAccount.GET(TempBankAccount."No.");
              EXIT(TRUE)
            END;
            EXIT(FALSE);
          END;
          EXIT(PAGE.RUNMODAL(PAGE::"Payment Bank Account List",BankAccount) = ACTION::LookupOK);
        END;
      END;

      EXIT(TRUE);
    END;

    PROCEDURE OpenWorksheet@14(BankAccReconciliation@1001 : Record 273);
    VAR
      BankAccReconciliationLine@1000 : Record 274;
    BEGIN
      SetFiltersOnBankAccReconLineTable(BankAccReconciliation,BankAccReconciliationLine);
      PAGE.RUN(PAGE::"Payment Reconciliation Journal",BankAccReconciliationLine);
    END;

    PROCEDURE OpenList@12(BankAccReconciliation@1001 : Record 273);
    VAR
      BankAccReconciliationLine@1000 : Record 274;
    BEGIN
      SetFiltersOnBankAccReconLineTable(BankAccReconciliation,BankAccReconciliationLine);
      PAGE.RUN(PAGE::"Pmt. Recon. Journal Overview",BankAccReconciliationLine);
    END;

    LOCAL PROCEDURE CantFindBancAccToUseInPaymentFileImport@18() : Code[20];
    VAR
      BankAccount@1000 : Record 270;
    BEGIN
      IF BankAccount.COUNT = 0 THEN
        MESSAGE(NoBankAccountsMsg)
      ELSE
        MESSAGE(NoBankAccWithFileFormatMsg);

      IF PAGE.RUNMODAL(PAGE::"Payment Bank Account List",BankAccount) = ACTION::LookupOK THEN
        IF (BankAccount."Bank Statement Import Format" <> '') OR
           BankAccount.IsLinkedToBankStatementServiceProvider
        THEN
          EXIT(BankAccount."No.");

      EXIT('');
    END;

    LOCAL PROCEDURE SetLastPaymentStatementNo@9(VAR BankAccount@1000 : Record 270);
    BEGIN
      IF BankAccount."Last Payment Statement No." = '' THEN BEGIN
        BankAccount."Last Payment Statement No." := '0';
        BankAccount.MODIFY;
      END;
    END;

    LOCAL PROCEDURE SetLastStatementNo@17(VAR BankAccount@1000 : Record 270);
    BEGIN
      IF BankAccount."Last Statement No." = '' THEN BEGIN
        BankAccount."Last Statement No." := '0';
        BankAccount.MODIFY;
      END;
    END;

    PROCEDURE SetFiltersOnBankAccReconLineTable@10(BankAccReconciliation@1001 : Record 273;VAR BankAccReconciliationLine@1000 : Record 274);
    BEGIN
      BankAccReconciliationLine.FILTERGROUP := 2;
      BankAccReconciliationLine.SETRANGE("Statement Type",BankAccReconciliation."Statement Type");
      BankAccReconciliationLine.SETRANGE("Bank Account No.",BankAccReconciliation."Bank Account No.");
      BankAccReconciliationLine.SETRANGE("Statement No.",BankAccReconciliation."Statement No.");
      BankAccReconciliationLine.FILTERGROUP := 0;
    END;

    LOCAL PROCEDURE ConfidenceLevelPermitToPost@25(BankAccReconciliation@1000 : Record 273) : Boolean;
    VAR
      BankAccReconciliationLine@1001 : Record 274;
    BEGIN
      SetFiltersOnBankAccReconLineTable(BankAccReconciliation,BankAccReconciliationLine);
      IF BankAccReconciliationLine.COUNT = 0 THEN
        EXIT(FALSE);

      BankAccReconciliationLine.SETFILTER("Match Confidence",'<>%1',BankAccReconciliationLine."Match Confidence"::High);
      IF BankAccReconciliationLine.COUNT <> 0 THEN
        EXIT(FALSE);

      IF CONFIRM(PostHighConfidentLinesQst) THEN
        EXIT(TRUE);

      EXIT(FALSE);
    END;

    LOCAL PROCEDURE LinesExist@11() : Boolean;
    VAR
      BankAccReconciliationLine@1000 : Record 274;
    BEGIN
      EXIT(BankAccReconciliationLine.LinesExist(Rec));
    END;

    LOCAL PROCEDURE BankAccountCouldBeUsedForImport@2() : Boolean;
    VAR
      BankAccount@1000 : Record 270;
    BEGIN
      BankAccount.GET("Bank Account No.");
      IF BankAccount."Bank Statement Import Format" <> '' THEN
        EXIT(TRUE);

      IF BankAccount.IsLinkedToBankStatementServiceProvider THEN
        EXIT(TRUE);

      IF NOT CONFIRM(MustHaveValueQst,TRUE,BankAccount.FIELDCAPTION("Bank Statement Import Format")) THEN
        EXIT(FALSE);

      IF PAGE.RUNMODAL(PAGE::"Payment Bank Account Card",BankAccount) = ACTION::LookupOK THEN
        IF BankAccount."Bank Statement Import Format" <> '' THEN
          EXIT(TRUE);

      EXIT(FALSE);
    END;

    PROCEDURE DrillDownOnBalanceOnBankAccount@38();
    VAR
      BankAccountLedgerEntry@1000 : Record 271;
    BEGIN
      BankAccountLedgerEntry.SETRANGE(Open,TRUE);
      BankAccountLedgerEntry.SETRANGE("Bank Account No.","Bank Account No.");
      PAGE.RUN(PAGE::"Bank Account Ledger Entries",BankAccountLedgerEntry);
    END;

    LOCAL PROCEDURE CopyBankAccountsToTemp@21(VAR TempBankAccount@1000 : TEMPORARY Record 270;VAR FromBankAccount@1001 : Record 270);
    BEGIN
      IF FromBankAccount.FINDSET THEN
        REPEAT
          TempBankAccount := FromBankAccount;
          TempBankAccount.INSERT;
        UNTIL FromBankAccount.NEXT = 0;
    END;

    BEGIN
    END.
  }
}

