OBJECT Table 50034 IRS - Rendim. Pagos - Anexo J
{
  OBJECT-PROPERTIES
  {
    Date=22/02/02;
    Time=17:01:23;
    Version List=D3Porto;
  }
  PROPERTIES
  {
  }
  FIELDS
  {
    { 1   ;   ;Ano                 ;Integer        }
    { 2   ;   ;Tipo Anexo          ;Option        ;OptionString=J }
    { 3   ;   ;Trabalho dependente ;Decimal        }
    { 4   ;   ;Trabalho independente;Decimal       }
    { 5   ;   ;Comiss�es           ;Decimal        }
    { 6   ;   ;Capitais            ;Decimal        }
    { 7   ;   ;Saldos credores c/c ;Decimal        }
    { 8   ;   ;Prediais            ;Decimal        }
    { 9   ;   ;Mais-valias         ;Decimal        }
    { 10  ;   ;Pens�es             ;Decimal        }
    { 11  ;   ;Reten��es IRC       ;Decimal        }
    { 12  ;   ;Reten��es taxas liberat�rias;Decimal }
    { 13  ;   ;Compensa��es IRS/IRC;Decimal        }
  }
  KEYS
  {
    {    ;Ano,Tipo Anexo                          ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    END.
  }
}

