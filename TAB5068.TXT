OBJECT Table 5068 Salutation
{
  OBJECT-PROPERTIES
  {
    Date=07/09/12;
    Time=12:00:00;
    Version List=NAVW17.00;
  }
  PROPERTIES
  {
    OnDelete=VAR
               SalutationFormula@1000 : Record 5069;
             BEGIN
               SalutationFormula.SETRANGE("Salutation Code",Code);
               SalutationFormula.DELETEALL;
             END;

    CaptionML=[ENU=Salutation;
               PTG=Encabe�amento];
    LookupPageID=Page5153;
  }
  FIELDS
  {
    { 1   ;   ;Code                ;Code10        ;CaptionML=[ENU=Code;
                                                              PTG=C�digo];
                                                   NotBlank=Yes }
    { 2   ;   ;Description         ;Text50        ;CaptionML=[ENU=Description;
                                                              PTG=Descri��o] }
  }
  KEYS
  {
    {    ;Code                                    ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    END.
  }
}

