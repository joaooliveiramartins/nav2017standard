OBJECT Table 7313 Warehouse Register
{
  OBJECT-PROPERTIES
  {
    Date=07/09/12;
    Time=12:00:00;
    Version List=NAVW17.10;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Warehouse Register;
               PTG=Registos Armaz�m];
    LookupPageID=Page7325;
  }
  FIELDS
  {
    { 1   ;   ;No.                 ;Integer       ;CaptionML=[ENU=No.;
                                                              PTG=N�] }
    { 2   ;   ;From Entry No.      ;Integer       ;TableRelation="Warehouse Entry";
                                                   CaptionML=[ENU=From Entry No.;
                                                              PTG=De N� Mov.] }
    { 3   ;   ;To Entry No.        ;Integer       ;TableRelation="Warehouse Entry";
                                                   CaptionML=[ENU=To Entry No.;
                                                              PTG=At� N� Mov.] }
    { 4   ;   ;Creation Date       ;Date          ;CaptionML=[ENU=Creation Date;
                                                              PTG=Data Cria��o] }
    { 5   ;   ;Source Code         ;Code10        ;TableRelation="Source Code";
                                                   CaptionML=[ENU=Source Code;
                                                              PTG=C�d. Origem] }
    { 6   ;   ;User ID             ;Code50        ;TableRelation=User."User Name";
                                                   OnLookup=VAR
                                                              UserMgt@1000 : Codeunit 418;
                                                            BEGIN
                                                              UserMgt.LookupUserID("User ID");
                                                            END;

                                                   TestTableRelation=No;
                                                   CaptionML=[ENU=User ID;
                                                              PTG=ID Utilizador] }
    { 7   ;   ;Journal Batch Name  ;Code10        ;TableRelation="Item Journal Batch".Name;
                                                   TestTableRelation=No;
                                                   CaptionML=[ENU=Journal Batch Name;
                                                              PTG=Nome Sec��o Di�rio] }
  }
  KEYS
  {
    {    ;No.                                     ;Clustered=Yes }
    {    ;Source Code                              }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    END.
  }
}

