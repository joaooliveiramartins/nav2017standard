OBJECT Report 31022981 Partial Settl. - Payable
{
  OBJECT-PROPERTIES
  {
    Date=13/02/15;
    Time=13:00:00;
    Version List=NAVPTSS82.00;
  }
  PROPERTIES
  {
    Permissions=TableData 21=imd,
                TableData 25=imd,
                TableData 31022936=imd,
                TableData 31022937=imd,
                TableData 31022953=imd,
                TableData 31022954=imd;
    CaptionML=[ENU=Partial Settl. - Payable;
               PTG=Liquida��o Parcial - A pagar];
    ProcessingOnly=Yes;
    OnInitReport=BEGIN
                   PostingDate := WORKDATE;
                   RemainingAmt := 0;
                   AppliedAmt := 0;
                   CurrencyCode := '';
                 END;

    OnPreReport=BEGIN
                  GLSetup.GET;
                END;

  }
  DATASET
  {
    { 2883;    ;DataItem;PostedDoc           ;
               DataItemTable=Table31022936;
               DataItemTableView=SORTING(Bill Gr./Pmt. Order No.,Status,Category Code,Redrawn,Due Date)
                                 WHERE(Status=CONST(Open));
               OnPreDataItem=BEGIN
                               DocPost.CheckPostingDate(PostingDate);

                               SourceCodeSetup.GET;
                               SourceCode := SourceCodeSetup."Cartera Journal";
                               DocCount :=0;
                               SumLCYAmt := 0;
                               GenJnlLineNextNo := 0;
                               ExistVATEntry := FALSE;
                               Window.OPEN(
                                 Text31022890);
                             END;

               OnAfterGetRecord=BEGIN
                                  IsRedrawn := CarteraManagement.CheckFromRedrawnDoc(PostedDoc."No.");
                                  IF PostedPmtOrd."No." = '' THEN BEGIN
                                    PostedPmtOrd.GET(PostedDoc."Bill Gr./Pmt. Order No.");
                                    BankAcc.GET(PostedPmtOrd."Bank Account No.");
                                    Delay := BankAcc."Delay for Notices";
                                  END;

                                  RemainingAmt2 := "Remaining Amount" - AppliedAmt;

                                  DocCount := DocCount + 1;
                                  Window.UPDATE(1,DocCount);

                                  CASE "Document Type" OF
                                    PostedDoc."Document Type"::Invoice,PostedDoc."Document Type"::"1":
                                      BEGIN
                                        WITH GenJnlLine DO BEGIN
                                          GenJnlLineNextNo := GenJnlLineNextNo + 10000;
                                          CLEAR(GenJnlLine);
                                          INIT;
                                          "Line No." := GenJnlLineNextNo;
                                          "Posting Date" := PostingDate;
                                          "Reason Code" := PostedPmtOrd."Reason Code";
                                          GenJnlLine."Document Date" := "Document Date";
                                          VALIDATE("Account Type","Account Type"::Vendor);
                                          VendLedgEntry.GET(PostedDoc."Entry No.");
                                          VALIDATE("Account No.",VendLedgEntry."Vendor No.");
                                          "Document Type" := "Document Type"::Payment;
                                          Description := COPYSTR(
                                            STRSUBSTNO(Text31022891,PostedDoc."Document No."),
                                            1,MAXSTRLEN(Description));
                                          "Document No." := PostedPmtOrd."No.";
                                          VALIDATE("Currency Code",PostedDoc."Currency Code");
                                          VALIDATE(Amount,AppliedAmt);
                                          "Applies-to Doc. Type" := VendLedgEntry."Document Type";
                                          "Applies-to Doc. No." := VendLedgEntry."Document No.";
                                          "Applies-to Bill No." := VendLedgEntry."Bill No.";
                                          "Source Code" := SourceCode;
                                          "Global Dimension 1 Code" := VendLedgEntry."Global Dimension 1 Code";
                                          "System-Created Entry" := TRUE;
                                          "Global Dimension 1 Code" := VendLedgEntry."Global Dimension 1 Code";
                                          "Global Dimension 2 Code" := VendLedgEntry."Global Dimension 2 Code";
                                          "Dimension Set ID" := VendLedgEntry."Dimension Set ID";
                                          INSERT;
                                          SumLCYAmt := SumLCYAmt + "Amount (LCY)";
                                        END;

                                        IF AppliedAmt = RemainingAmt THEN BEGIN
                                          VendLedgEntry."Document Status" := VendLedgEntry."Document Status"::Honored;
                                          VendLedgEntry.MODIFY;
                                          RemainingAmt2 := 0;
                                        END;
                                      END;
                                    PostedDoc."Document Type"::Bill:
                                      BEGIN
                                        WITH GenJnlLine DO BEGIN
                                          GenJnlLineNextNo := GenJnlLineNextNo + 10000;
                                          CLEAR(GenJnlLine);
                                          INIT;
                                          "Line No." := GenJnlLineNextNo;
                                          "Posting Date" := PostingDate;
                                          "Document Type" := "Document Type"::Payment;
                                          "Document No." := PostedPmtOrd."No.";
                                          "Reason Code" := PostedPmtOrd."Reason Code";
                                          VALIDATE("Account Type","Account Type"::Vendor);
                                          VendLedgEntry.GET(PostedDoc."Entry No.");

                                          IF GLSetup."Unrealized VAT" THEN BEGIN
                                            ExistsNoRealVAT := CarteraManagement.FindVendVATSetup(VATPostingSetup,VendLedgEntry);
                                          END;

                                          VALIDATE("Account No.",VendLedgEntry."Vendor No.");
                                          Description := COPYSTR(
                                            STRSUBSTNO(Text31022892,PostedDoc."Document No.",PostedDoc."No."),
                                            1,MAXSTRLEN(Description));
                                          VALIDATE("Currency Code",PostedDoc."Currency Code");
                                          VALIDATE(Amount,AppliedAmt);
                                          "Applies-to Doc. Type" := VendLedgEntry."Document Type";
                                          "Applies-to Doc. No." := VendLedgEntry."Document No.";
                                          "Applies-to Bill No." := VendLedgEntry."Bill No.";
                                          "Source Code" := SourceCode;
                                          "System-Created Entry" := TRUE;
                                          "Global Dimension 1 Code" := VendLedgEntry."Global Dimension 1 Code";
                                          "Global Dimension 2 Code" := VendLedgEntry."Global Dimension 2 Code";
                                          "Dimension Set ID" := VendLedgEntry."Dimension Set ID";
                                          INSERT;
                                          SumLCYAmt := SumLCYAmt + "Amount (LCY)";
                                        END;
                                        IF GLSetup."Unrealized VAT" AND
                                           ExistsNoRealVAT AND
                                           (NOT IsRedrawn) THEN BEGIN
                                          CarteraManagement.VendUnrealizedVAT2(
                                            VendLedgEntry,
                                            AppliedAmt,
                                            GenJnlLine,
                                            ExistVATEntry,
                                            FirstVATEntryNo,
                                            LastVATEntryNo,
                                            NoRealVATBuffer);

                                          IF NoRealVATBuffer.FINDSET THEN BEGIN
                                            REPEAT
                                              BEGIN
                                                InsertGenJournalLine(
                                                  GenJnlLine."Account Type"::"G/L Account",
                                                  NoRealVATBuffer.Account,
                                                  -NoRealVATBuffer.Amount);
                                                InsertGenJournalLine(
                                                  GenJnlLine."Account Type"::"G/L Account",
                                                  NoRealVATBuffer."Balance Account",
                                                  NoRealVATBuffer.Amount);
                                              END;
                                            UNTIL NoRealVATBuffer.NEXT = 0;
                                          END;

                                        END;

                                        IF AppliedAmt =  PostedDoc."Remaining Amount" THEN BEGIN
                                          VendLedgEntry."Document Status" := VendLedgEntry."Document Status"::Honored;
                                          VendLedgEntry.MODIFY;
                                        END;
                                      END;
                                  END;

                                  Dep := "Global Dimension 1 Code";
                                  Proj := "Global Dimension 2 Code";
                                END;

               OnPostDataItem=BEGIN
                                IF DocCount = 0 THEN
                                  ERROR(
                                    Text31022893 +
                                    Text31022894);

                                GenJnlLineNextNo := GenJnlLineNextNo + 10000;
                                WITH GenJnlLine DO BEGIN
                                  CLEAR(GenJnlLine);
                                  INIT;
                                  "Line No." := GenJnlLineNextNo;
                                  "Posting Date" := PostingDate;
                                  "Document Type" := "Document Type"::Payment;
                                  "Document No." := PostedPmtOrd."No.";
                                  "Reason Code" := PostedPmtOrd."Reason Code";
                                  VALIDATE("Account Type",GenJnlLine."Account Type"::"Bank Account");
                                  VALIDATE("Account No.",BankAcc."No.");
                                  IF PostedDoc."Document Type" = PostedDoc."Document Type"::Bill THEN
                                    Description := COPYSTR(STRSUBSTNO(
                                      Text31022892,PostedDoc."Document No.",PostedDoc."No."),1,MAXSTRLEN(Description))
                                  ELSE
                                    Description := COPYSTR(STRSUBSTNO(
                                      Text31022891,PostedDoc."Document No."),1,MAXSTRLEN(Description));
                                  VALIDATE("Currency Code",PostedPmtOrd."Currency Code");
                                  VALIDATE(Amount,-AppliedAmt);
                                  "Source Code" := SourceCode;
                                  "System-Created Entry" := TRUE;
                                  "Global Dimension 1 Code" := Dep;
                                  "Global Dimension 2 Code" := Proj;
                                  "Dimension Set ID" := VendLedgEntry."Dimension Set ID";
                                  INSERT;
                                  SumLCYAmt := SumLCYAmt + "Amount (LCY)";
                                END;

                                IF PostedPmtOrd."Currency Code" <> '' THEN BEGIN
                                  IF SumLCYAmt <> 0 THEN BEGIN
                                    Currency.GET(PostedPmtOrd."Currency Code");
                                    IF SumLCYAmt > 0 THEN BEGIN
                                      Currency.TESTFIELD("Residual Gains Account");
                                      Acct := Currency."Residual Gains Account";
                                    END ELSE BEGIN
                                      Currency.TESTFIELD("Residual Losses Account");
                                      Acct := Currency."Residual Losses Account";
                                    END;
                                    GenJnlLineNextNo := GenJnlLineNextNo + 10000;
                                    WITH GenJnlLine DO BEGIN
                                      CLEAR(GenJnlLine);
                                      INIT;
                                      "Line No." := GenJnlLineNextNo;
                                      "Posting Date" := PostingDate;
                                      "Document Type" := "Document Type"::Payment;
                                      "Document No." := PostedPmtOrd."No.";
                                      "Reason Code" := PostedPmtOrd."Reason Code";
                                      VALIDATE("Account Type","Account Type"::"G/L Account");
                                      VALIDATE("Account No.",Acct);
                                      Description := Text31022895;
                                      VALIDATE("Currency Code",'');
                                      VALIDATE(Amount,-SumLCYAmt);
                                      "Source Code" := SourceCode;
                                      "System-Created Entry" := TRUE;
                                      INSERT;
                                    END;
                                  END;
                                END;
                                IF GenJnlLine.FINDSET THEN
                                  REPEAT
                                    GenJnlLine2 := GenJnlLine;
                                    GenJnlPostLine.SetFromSettlement(TRUE);
                                    GenJnlPostLine.RunWithCheck(GenJnlLine2);
                                  UNTIL GenJnlLine.NEXT = 0;


                                VendLedgEntry.GET(PostedDoc."Entry No.");
                                PostedDoc.GET(PostedDoc.Type,PostedDoc."Entry No.");
                                PostedDoc."Honored/Rejtd. at Date" := PostingDate;
                                PostedDoc."Remaining Amount" := RemainingAmt2;
                                PostedDoc."Remaining Amt. (LCY)" := ROUND(CurrExchRate.ExchangeAmtFCYToLCY(
                                                                          VendLedgEntry."Posting Date",
                                                                          VendLedgEntry."Currency Code",
                                                                          RemainingAmt2,
                                                                          CurrExchRate.ExchangeRate(VendLedgEntry."Posting Date",VendLedgEntry."Currency Code")),
                                                                          GLSetup."Amount Rounding Precision");
                                IF RemainingAmt2 = 0 THEN BEGIN
                                  PostedDoc."Remaining Amt. (LCY)" := 0;
                                  VendLedgEntry."Document Status" := VendLedgEntry."Document Status"::Honored;
                                  PostedDoc.Status := PostedDoc.Status::Honored;
                                  VendLedgEntry.Open := FALSE;
                                END ELSE BEGIN
                                  VendLedgEntry."Document Status" := VendLedgEntry."Document Status"::Open;
                                  PostedDoc.Status := PostedDoc.Status::Open;
                                  VendLedgEntry.Open := TRUE;
                                END;

                                VendLedgEntry.MODIFY;
                                PostedDoc.MODIFY;

                                DocPost.ClosePmtOrdIfEmpty(PostedPmtOrd,PostingDate);

                                Window.CLOSE;

                                IF ExistVATEntry THEN BEGIN
                                  GLReg.FINDLAST;
                                  GLReg."From VAT Entry No." := FirstVATEntryNo;
                                  GLReg."To VAT Entry No." := LastVATEntryNo;
                                  GLReg.MODIFY;
                                END;

                                COMMIT;

                                MESSAGE(
                                  Text31022896,
                                  DocCount,RemainingAmt,PostedPmtOrd."No.",AppliedAmt);
                              END;
                               }

  }
  REQUESTPAGE
  {
    PROPERTIES
    {
      SaveValues=No;
    }
    CONTROLS
    {
      { 1900000001;0;Container;
                  ContainerType=ContentArea }

      { 1900000002;1;Group  ;
                  CaptionML=[ENU=Options;
                             PTG=Op��es] }

      { 4   ;2   ;Field     ;
                  CaptionML=[ENU=Posting Date;
                             PTG=Data Registo];
                  NotBlank=Yes;
                  SourceExpr=PostingDate;
                  OnValidate=VAR
                               VendLedgEntry2@1110000 : Record 25;
                             BEGIN
                               VendLedgEntry2.GET(VendLedgEntryNo);
                               IF VendLedgEntry2."Document Type" = VendLedgEntry2."Document Type"::Invoice THEN BEGIN
                                 VendLedgEntry2.CALCFIELDS("Remaining Amount");
                                 IF PostingDate > VendLedgEntry2."Pmt. Discount Date" THEN
                                   RemainingAmt := -VendLedgEntry2."Remaining Amount"
                                 ELSE
                                   RemainingAmt := -VendLedgEntry2."Remaining Amount" + VendLedgEntry2."Remaining Pmt. Disc. Possible";
                                 AppliedAmt := RemainingAmt;
                               END;
                             END;
                              }

      { 2   ;2   ;Field     ;
                  CaptionML=[ENU=Remaining Amount;
                             PTG=Valor Pendente];
                  SourceExpr=RemainingAmt;
                  AutoFormatType=1;
                  AutoFormatExpr=CurrencyCode;
                  Editable=FALSE }

      { 3   ;2   ;Field     ;
                  CaptionML=[ENU=Currency Code;
                             PTG=C�d. Divisa];
                  SourceExpr=CurrencyCode;
                  TableRelation=Currency;
                  Editable=FALSE }

      { 1   ;2   ;Field     ;
                  CaptionML=[ENU=Settled Amount;
                             PTG=Valor Liquidado];
                  NotBlank=Yes;
                  SourceExpr=AppliedAmt;
                  AutoFormatType=1;
                  AutoFormatExpr=CurrencyCode;
                  MinValue=0;
                  Editable=TRUE;
                  OnValidate=BEGIN
                               IF AppliedAmt > RemainingAmt THEN
                                 ERROR(Text31022897,RemainingAmt);
                             END;
                              }

    }
  }
  LABELS
  {
  }
  CODE
  {
    VAR
      Text31022890@1000 : TextConst 'ENU=Settling payable documents       #1######;PTG=A liquidar parcialmente documentos a pagar      #1######';
      Text31022891@1001 : TextConst 'ENU=Partial Document settlement %1;PTG=Liquida��o parcial de documento a pagar %1';
      Text31022892@1002 : TextConst 'ENU=Partial Bill settlement %1/%2;PTG=Liquida��o parcial de letra %1/%2';
      Text31022893@1003 : TextConst 'ENU=No payable documents have been found that can be settled. \;PTG=N�o foram encontrados documentos a pagar que possam ser liquidados.';
      Text31022894@1004 : TextConst 'ENU=Please check that the selection is not empty and at least one payable document is open.;PTG=Por favor verifique que a sele��o n�o est� vazia e que pelo menos um documento a pagar est� aberto.';
      Text31022895@1005 : TextConst 'ENU=Residual adjust generated by rounding Amount;PTG=Ajuste residual gerado pelo arredondamento do Valor';
      Text31022896@1006 : TextConst 'ENU=%1 payable documents totaling %2 have been partially settled in Payment Order %3 by an amount of %4.;PTG=%1 documentos a pagar que totalizam %2 foram parcialmente liquidados na Ordem Pagamento %3 pelo valor de %4.';
      Text31022897@1007 : TextConst 'ENU=The maximum permitted value is %1;PTG=O valor m�ximo permitido � %1';
      Text31022898@1008 : TextConst 'ENU=Partial payable document settlement %1/%2;PTG=Liquida��o parcial de documento a pagar %1/%2';
      Text31022899@1009 : TextConst 'ENU=Partial payable document settlement %1;PTG=Liquida��o parcial de documento a pagar %1';
      SourceCodeSetup@1110000 : Record 242;
      PostedPmtOrd@1110001 : Record 31022953;
      GenJnlLine@1110002 : TEMPORARY Record 81;
      GenJnlLine2@1110003 : Record 81;
      VendLedgEntry@1110004 : Record 25;
      BankAccPostingGr@1110005 : Record 277;
      BankAcc@1110006 : Record 270;
      CurrExchRate@1110007 : Record 330;
      Currency@1110008 : Record 4;
      GLReg@1110009 : Record 45;
      GLSetup@1110010 : Record 98;
      VATPostingSetup@1110011 : Record 325;
      DocPost@1110012 : Codeunit 31022906;
      CarteraManagement@1110013 : Codeunit 31022901;
      GenJnlPostLine@1110014 : Codeunit 12;
      Window@1110015 : Dialog;
      PostingDate@1110016 : Date;
      Delay@1110017 : Decimal;
      SourceCode@1110018 : Code[10];
      Acct@1110019 : Code[20];
      DocCount@1110020 : Integer;
      GenJnlLineNextNo@1110021 : Integer;
      SumLCYAmt@1110022 : Decimal;
      CurrencyCode@1110023 : Code[10];
      RemainingAmt@1110024 : Decimal;
      RemainingAmt2@1110025 : Decimal;
      AppliedAmt@1110026 : Decimal;
      ExistVATEntry@1110027 : Boolean;
      FirstVATEntryNo@1110028 : Integer;
      LastVATEntryNo@1110029 : Integer;
      IsRedrawn@1110030 : Boolean;
      Dep@1110031 : Code[20];
      Proj@1110032 : Code[20];
      NoRealVATBuffer@1110035 : TEMPORARY Record 31022945;
      ExistsNoRealVAT@1110034 : Boolean;
      VendLedgEntryNo@1110036 : Integer;

    PROCEDURE SetInitValue@2(Amount@1110000 : Decimal;CurrCode@1110001 : Code[10];EntryNo@1110002 : Integer);
    BEGIN
      CurrencyCode := CurrCode;
      RemainingAmt := Amount;
      AppliedAmt := RemainingAmt;
      VendLedgEntryNo := EntryNo;
    END;

    LOCAL PROCEDURE InsertGenJournalLine@4(AccType@1110000 : Integer;AccNo@1110001 : Code[20];Amount2@1110002 : Decimal);
    BEGIN
      GenJnlLineNextNo :=  GenJnlLineNextNo + 10000;

      WITH GenJnlLine DO BEGIN
        CLEAR(GenJnlLine);
        INIT;
        "Line No." := GenJnlLineNextNo;
        "Posting Date" := PostingDate;
        "Document No." := PostedPmtOrd."No.";
        "Reason Code" := PostedPmtOrd."Reason Code";
        "Account Type" := AccType;
        "Account No." := AccNo;
        IF PostedDoc."Document Type" = PostedDoc."Document Type"::Bill THEN
          Description := COPYSTR(
            STRSUBSTNO(Text31022898,PostedDoc."Document No.",PostedDoc."No."),
            1,MAXSTRLEN(Description))
        ELSE
          Description := COPYSTR(
            STRSUBSTNO(Text31022899,PostedDoc."Document No."),
            1,MAXSTRLEN(Description));
        VALIDATE("Currency Code",PostedDoc."Currency Code");
        VALIDATE(Amount,-Amount2);
        "Applies-to Doc. Type" := VendLedgEntry."Document Type";
        "Applies-to Doc. No." := '';
        "Applies-to Bill No." := VendLedgEntry."Bill No.";
        "Source Code" := SourceCode;
        "System-Created Entry" := TRUE;
        "Shortcut Dimension 1 Code" := VendLedgEntry."Global Dimension 1 Code";
        "Shortcut Dimension 2 Code" := VendLedgEntry."Global Dimension 2 Code";
        "Dimension Set ID" := VendLedgEntry."Dimension Set ID";
        INSERT;
      END;
    END;

    PROCEDURE GetAmtLCY@7(Amount@1110000 : Decimal) : Decimal;
    VAR
      CurrencyExchRate@1110001 : Record 330;
      GLSetup@1110002 : Record 98;
    BEGIN
      IF PostedDoc."Currency Code" = '' THEN
        EXIT(Amount)
      ELSE BEGIN
        GLSetup.GET;
        EXIT(
          ROUND(
            CurrencyExchRate.ExchangeAmtFCYToLCY(
              PostingDate,PostedDoc."Currency Code",
              Amount,
              CurrExchRate.ExchangeRate(PostingDate,PostedDoc."Currency Code")),
            GLSetup."Amount Rounding Precision"));
      END;
    END;

    BEGIN
    END.
  }
  RDLDATA
  {
  }
}

