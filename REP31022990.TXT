OBJECT Report 31022990 Categorize Documents
{
  OBJECT-PROPERTIES
  {
    Date=01/02/15;
    Time=13:00:00;
    Version List=NAVPTSS81.00;
  }
  PROPERTIES
  {
    Permissions=TableData 31022935=m;
    CaptionML=[ENU=Categorize Documents;
               PTG=Classificar documentos];
    ProcessingOnly=Yes;
  }
  DATASET
  {
    { 2864;    ;DataItem;                    ;
               DataItemTable=Table31022935;
               DataItemTableView=SORTING(Type,Entry No.);
               OnPreDataItem=BEGIN
                               MODIFYALL("Category Code",CategoryCode);
                               CurrReport.BREAK;
                             END;
                              }

  }
  REQUESTPAGE
  {
    PROPERTIES
    {
    }
    CONTROLS
    {
      { 1900000001;0;Container;
                  ContainerType=ContentArea }

      { 1900000002;1;Group  ;
                  CaptionML=[ENU=Options;
                             PTG=Op��es] }

      { 1   ;2   ;Field     ;
                  CaptionML=[ENU=Category Code;
                             PTG=C�d. Classifica��o];
                  SourceExpr=CategoryCode;
                  TableRelation="Category Code" }

    }
  }
  LABELS
  {
  }
  CODE
  {
    VAR
      CategoryCode@1110000 : Code[10];

    BEGIN
    END.
  }
  RDLDATA
  {
  }
}

