OBJECT Page 397 Sales Invoice Statistics
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0,NAVPTSS81.00;
  }
  PROPERTIES
  {
    Editable=No;
    CaptionML=[ENU=Sales Invoice Statistics;
               PTG=Estat�stica Fatura Venda];
    LinksAllowed=No;
    SourceTable=Table112;
    PageType=ListPlus;
    OnAfterGetRecord=VAR
                       CustLedgEntry@1000 : Record 21;
                       CostCalcMgt@1001 : Codeunit 5836;
                     BEGIN
                       CLEARALL;

                       IF "Currency Code" = '' THEN
                         currency.InitRoundingPrecision
                       ELSE
                         currency.GET("Currency Code");

                       SalesInvLine.SETRANGE("Document No.","No.");
                       IF SalesInvLine.FIND('-') THEN
                         REPEAT
                           CustAmount := CustAmount + SalesInvLine.Amount;
                           AmountInclVAT := AmountInclVAT + SalesInvLine."Amount Including VAT";
                           IF "Prices Including VAT" THEN BEGIN
                             InvDiscAmount := InvDiscAmount + SalesInvLine."Inv. Discount Amount" / (1 + SalesInvLine."VAT %" / 100);
                             PmtDiscAmount := PmtDiscAmount + SalesInvLine."Pmt. Disc. Given Amount" / (1 + SalesInvLine."VAT %"  / 100); //soft, n
                           END ELSE BEGIN
                             InvDiscAmount := InvDiscAmount + SalesInvLine."Inv. Discount Amount";
                             PmtDiscAmount := PmtDiscAmount + SalesInvLine."Pmt. Disc. Given Amount"; //soft,n
                           END;
                           CostLCY := CostLCY + (SalesInvLine.Quantity * SalesInvLine."Unit Cost (LCY)");
                           LineQty := LineQty + SalesInvLine.Quantity;
                           TotalNetWeight := TotalNetWeight + (SalesInvLine.Quantity * SalesInvLine."Net Weight");
                           TotalGrossWeight := TotalGrossWeight + (SalesInvLine.Quantity * SalesInvLine."Gross Weight");
                           TotalVolume := TotalVolume + (SalesInvLine.Quantity * SalesInvLine."Unit Volume");
                           IF SalesInvLine."Units per Parcel" > 0 THEN
                             TotalParcels := TotalParcels + ROUND(SalesInvLine.Quantity / SalesInvLine."Units per Parcel",1,'>');
                           IF SalesInvLine."VAT %" <> VATPercentage THEN
                             IF VATPercentage = 0 THEN
                               //soft,o VATPercentage := SalesInvLine."VAT %"
                               VATPercentage := SalesInvLine."VAT %" + SalesInvLine."ND %" //soft,n
                             ELSE
                               VATPercentage := -1;
                           TotalAdjCostLCY := TotalAdjCostLCY + CostCalcMgt.CalcSalesInvLineCostLCY(SalesInvLine);
                         UNTIL SalesInvLine.NEXT = 0;
                       VATAmount := AmountInclVAT - CustAmount;
                       InvDiscAmount := ROUND(InvDiscAmount,currency."Amount Rounding Precision");

                       IF VATPercentage <= 0 THEN
                         VATAmountText := Text000
                       ELSE
                         VATAmountText := STRSUBSTNO(Text001,VATPercentage);

                       IF "Currency Code" = '' THEN
                         AmountLCY := CustAmount
                       ELSE
                         AmountLCY :=
                           CurrExchRate.ExchangeAmtFCYToLCY(
                             WORKDATE,"Currency Code",CustAmount,"Currency Factor");

                       CustLedgEntry.SETCURRENTKEY("Document No.");
                       CustLedgEntry.SETRANGE("Document No.","No.");
                       CustLedgEntry.SETRANGE("Document Type",CustLedgEntry."Document Type"::Invoice);
                       CustLedgEntry.SETRANGE("Customer No.","Bill-to Customer No.");
                       IF CustLedgEntry.FINDFIRST THEN
                         AmountLCY := CustLedgEntry."Sales (LCY)";

                       ProfitLCY := AmountLCY - CostLCY;
                       IF AmountLCY <> 0 THEN
                         ProfitPct := ROUND(100 * ProfitLCY / AmountLCY,0.1);

                       AdjProfitLCY := AmountLCY - TotalAdjCostLCY;
                       IF AmountLCY <> 0 THEN
                         AdjProfitPct := ROUND(100 * AdjProfitLCY / AmountLCY,0.1);

                       IF Cust.GET("Bill-to Customer No.") THEN
                         Cust.CALCFIELDS("Balance (LCY)")
                       ELSE
                         CLEAR(Cust);

                       CASE TRUE OF
                         Cust."Credit Limit (LCY)" = 0:
                           CreditLimitLCYExpendedPct := 0;
                         Cust."Balance (LCY)" / Cust."Credit Limit (LCY)" < 0:
                           CreditLimitLCYExpendedPct := 0;
                         Cust."Balance (LCY)" / Cust."Credit Limit (LCY)" > 1:
                           CreditLimitLCYExpendedPct := 10000;
                         ELSE
                           CreditLimitLCYExpendedPct := ROUND(Cust."Balance (LCY)" / Cust."Credit Limit (LCY)" * 10000,1);
                       END;

                       SalesInvLine.CalcVATAmountLines(Rec,TempVATAmountLine);
                       CurrPage.Subform.PAGE.SetTempVATAmountLine(TempVATAmountLine);
                       CurrPage.Subform.PAGE.InitGlobals("Currency Code",FALSE,FALSE,FALSE,FALSE,"VAT Base Discount %");
                     END;

  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 36  ;1   ;Group     ;
                CaptionML=[ENU=General;
                           PTG=Geral] }

    { 2   ;2   ;Field     ;
                CaptionML=[ENU=Amount;
                           PTG=Valor];
                ToolTipML=ENU=Specifies the net amount of all the lines in the sales document.;
                SourceExpr=CustAmount + InvDiscAmount+ PmtDiscAmount;
                AutoFormatType=1;
                AutoFormatExpr="Currency Code" }

    { 6   ;2   ;Field     ;
                CaptionML=[ENU=Inv. Discount Amount;
                           PTG=Valor Desconto Fatura];
                ToolTipML=ENU=Specifies the invoice discount amount for the sales document.;
                SourceExpr=InvDiscAmount;
                AutoFormatType=1;
                AutoFormatExpr="Currency Code" }

    { 1110000;2;Field     ;
                CaptionML=[ENU=Pmt. Disc. Given Amount;
                           PTG=Valor Desconto P.P. Concedido];
                SourceExpr=PmtDiscAmount;
                AutoFormatType=1;
                AutoFormatExpr="Currency Code" }

    { 12  ;2   ;Field     ;
                CaptionML=[ENU=Total;
                           PTG=Total];
                ToolTipML=ENU=Specifies the total amount, less any invoice discount amount, and excluding VAT for the sales document.;
                SourceExpr=CustAmount;
                AutoFormatType=1;
                AutoFormatExpr="Currency Code" }

    { 16  ;2   ;Field     ;
                CaptionML=[ENU=VAT Amount;
                           PTG=Valor IVA];
                ToolTipML=ENU=Specifies the total VAT amount that has been calculated for all the lines in the sales document.;
                SourceExpr=VATAmount;
                AutoFormatType=1;
                AutoFormatExpr="Currency Code";
                CaptionClass=FORMAT(VATAmountText) }

    { 20  ;2   ;Field     ;
                CaptionML=[ENU=Total Incl. VAT;
                           PTG=Total Incl. IVA];
                ToolTipML=ENU=Specifies the total amount, including VAT, that will be posted to the customer's account for all the lines in the sales document.;
                SourceExpr=AmountInclVAT;
                AutoFormatType=1;
                AutoFormatExpr="Currency Code" }

    { 26  ;2   ;Field     ;
                CaptionML=[ENU=Sales (LCY);
                           PTG=Vendas (DL)];
                ToolTipML=ENU=Specifies your total sales turnover in the fiscal year.;
                SourceExpr=AmountLCY;
                AutoFormatType=1 }

    { 30  ;2   ;Field     ;
                CaptionML=[ENU=Original Profit (LCY);
                           PTG=Margem Original (DL)];
                ToolTipML=ENU=Specifies the original profit that was associated with the sales when they were originally posted.;
                SourceExpr=ProfitLCY;
                AutoFormatType=1 }

    { 43  ;2   ;Field     ;
                CaptionML=[ENU=Adjusted Profit (LCY);
                           PTG=Margem Ajustado (DL)];
                ToolTipML=ENU=Specifies the profit, taking into consideration changes in the purchase prices of the goods.;
                SourceExpr=AdjProfitLCY;
                AutoFormatType=1 }

    { 32  ;2   ;Field     ;
                CaptionML=[ENU=Original Profit %;
                           PTG=% Margem Original];
                ToolTipML=ENU=Specifies the original percentage of profit that was associated with the sales when they were originally posted.;
                DecimalPlaces=1:1;
                SourceExpr=ProfitPct }

    { 41  ;2   ;Field     ;
                CaptionML=[ENU=Adjusted Profit %;
                           PTG=% Margem Ajustada];
                ToolTipML=ENU=Specifies the percentage of profit for all sales, including changes that occurred in the purchase prices of the goods.;
                DecimalPlaces=1:1;
                SourceExpr=AdjProfitPct }

    { 3   ;2   ;Field     ;
                CaptionML=[ENU=Quantity;
                           PTG=Quantidade];
                ToolTipML=ENU=Specifies the total quantity of G/L account entries, items and/or resources in the sales document.;
                DecimalPlaces=0:5;
                SourceExpr=LineQty }

    { 13  ;2   ;Field     ;
                CaptionML=[ENU=Parcels;
                           PTG=Lotes];
                ToolTipML=ENU=Specifies the total number of parcels in the sales document.;
                DecimalPlaces=0:5;
                SourceExpr=TotalParcels }

    { 7   ;2   ;Field     ;
                CaptionML=[ENU=Net Weight;
                           PTG=Peso L�quido];
                ToolTipML=ENU=Specifies the total net weight of the items in the sales document.;
                DecimalPlaces=0:5;
                SourceExpr=TotalNetWeight }

    { 10  ;2   ;Field     ;
                CaptionML=[ENU=Gross Weight;
                           PTG=Peso Bruto];
                ToolTipML=ENU=Specifies the total gross weight of the items in the sales document.;
                DecimalPlaces=0:5;
                SourceExpr=TotalGrossWeight }

    { 17  ;2   ;Field     ;
                CaptionML=[ENU=Volume;
                           PTG=Volume];
                ToolTipML=ENU=Specifies the total volume of the items in the sales document.;
                DecimalPlaces=0:5;
                SourceExpr=TotalVolume }

    { 28  ;2   ;Field     ;
                CaptionML=[ENU=Original Cost (LCY);
                           PTG=Custo Original (DL)];
                ToolTipML=ENU=Specifies the total cost, in LCY, of the G/L account entries, items and/or resources in the sales document.;
                SourceExpr=CostLCY;
                AutoFormatType=1 }

    { 35  ;2   ;Field     ;
                CaptionML=[ENU=Adjusted Cost (LCY);
                           PTG=Custo Ajustado (DL)];
                SourceExpr=TotalAdjCostLCY;
                AutoFormatType=1 }

    { 39  ;2   ;Field     ;
                CaptionML=[ENU=Cost Adjmt. Amount (LCY);
                           PTG=Valor Ajuste Custo (DL)];
                SourceExpr=TotalAdjCostLCY - CostLCY;
                AutoFormatType=1;
                OnLookup=BEGIN
                           LookupAdjmtValueEntries;
                         END;
                          }

    { 18  ;1   ;Part      ;
                Name=Subform;
                PagePartID=Page576;
                Editable=FALSE }

    { 1903289601;1;Group  ;
                CaptionML=[ENU=Customer;
                           PTG=Cliente] }

    { 24  ;2   ;Field     ;
                CaptionML=[ENU=Balance (LCY);
                           PTG=Saldo (DL)];
                ToolTipML=ENU=Specifies the balance in LCY on the customer's account.;
                SourceExpr=Cust."Balance (LCY)";
                AutoFormatType=1 }

    { 21  ;2   ;Field     ;
                CaptionML=[ENU=Credit Limit (LCY);
                           PTG=Limite Cr�dito (DL)];
                SourceExpr=Cust."Credit Limit (LCY)";
                AutoFormatType=1 }

    { 37  ;2   ;Field     ;
                ExtendedDatatype=Ratio;
                CaptionML=[ENU=Expended % of Credit Limit (LCY);
                           PTG=% Limite Cr�dito Usado (DL)];
                ToolTipML=[ENU=Specifies the expended percentage of the credit limit in (LCY).;
                           PTG=Especifica a percentagem gasta do limite de cr�dito em (DL)];
                SourceExpr=CreditLimitLCYExpendedPct }

  }
  CODE
  {
    VAR
      Text000@1000 : TextConst 'ENU=VAT Amount;PTG=Valor IVA';
      Text001@1001 : TextConst 'ENU=%1% VAT;PTG=%1% IVA';
      CurrExchRate@1002 : Record 330;
      SalesInvLine@1003 : Record 113;
      Cust@1004 : Record 18;
      TempVATAmountLine@1005 : TEMPORARY Record 290;
      currency@1022 : Record 4;
      TotalAdjCostLCY@1023 : Decimal;
      CustAmount@1006 : Decimal;
      AmountInclVAT@1007 : Decimal;
      InvDiscAmount@1008 : Decimal;
      VATAmount@1009 : Decimal;
      CostLCY@1010 : Decimal;
      ProfitLCY@1011 : Decimal;
      ProfitPct@1012 : Decimal;
      AdjProfitLCY@1027 : Decimal;
      AdjProfitPct@1026 : Decimal;
      LineQty@1013 : Decimal;
      TotalNetWeight@1014 : Decimal;
      TotalGrossWeight@1015 : Decimal;
      TotalVolume@1016 : Decimal;
      TotalParcels@1017 : Decimal;
      AmountLCY@1018 : Decimal;
      CreditLimitLCYExpendedPct@1019 : Decimal;
      VATPercentage@1020 : Decimal;
      VATAmountText@1021 : Text[30];
      "//--soft-global--//"@9000000 : Integer;
      PmtDiscAmount@1110001 : Decimal;

    BEGIN
    END.
  }
}

