OBJECT Table 31022940 Closed Bill Group
{
  OBJECT-PROPERTIES
  {
    Date=21/12/16;
    Time=13:00:00;
    Version List=NAVPTSS10.00;
  }
  PROPERTIES
  {
    OnDelete=BEGIN
               ClosedDoc.SETRANGE("Bill Gr./Pmt. Order No.","No.");
               ClosedDoc.DELETEALL;

               BGPOCommentLine.SETRANGE("BG/PO No.","No.");
               BGPOCommentLine.DELETEALL;
             END;

    CaptionML=[ENU=Closed Bill Group;
               PTG=Remessa Fechada];
    LookupPageID=Page331022961;
    DrillDownPageID=Page31022961;
  }
  FIELDS
  {
    { 2   ;   ;No.                 ;Code20        ;CaptionML=[ENU=No.;
                                                              PTG=N�] }
    { 3   ;   ;Bank Account No.    ;Code20        ;TableRelation="Bank Account";
                                                   CaptionML=[ENU=Bank Account No.;
                                                              PTG=N� Conta Banc�ria] }
    { 4   ;   ;Bank Account Name   ;Text50        ;FieldClass=FlowField;
                                                   CalcFormula=Lookup("Bank Account".Name WHERE (No.=FIELD(Bank Account No.)));
                                                   CaptionML=[ENU=Bank Account Name;
                                                              PTG=Nome Conta Banc�ria];
                                                   Editable=No }
    { 5   ;   ;Posting Description ;Text50        ;CaptionML=[ENU=Posting Description;
                                                              PTG=Texto Registo] }
    { 6   ;   ;Dealing Type        ;Option        ;CaptionML=[ENU=Dealing Type;
                                                              PTG=Tipo Gest�o];
                                                   OptionCaptionML=[ENU=Collection,Discount;
                                                                    PTG=Cobran�a,Desconto];
                                                   OptionString=Collection,Discount;
                                                   Editable=No }
    { 8   ;   ;Reason Code         ;Code10        ;TableRelation="Reason Code";
                                                   CaptionML=[ENU=Reason Code;
                                                              PTG=C�d. Auditoria] }
    { 9   ;   ;No. Printed         ;Integer       ;CaptionML=[ENU=No. Printed;
                                                              PTG=N� ImpressSes] }
    { 10  ;   ;Posting Date        ;Date          ;CaptionML=[ENU=Posting Date;
                                                              PTG=Data Registo] }
    { 11  ;   ;Comment             ;Boolean       ;FieldClass=FlowField;
                                                   CalcFormula=Exist("BG/PO Comment Line" WHERE (BG/PO No.=FIELD(No.),
                                                                                                 Type=FILTER(Receivable)));
                                                   CaptionML=[ENU=Comment;
                                                              PTG=Coment�rio];
                                                   Editable=No }
    { 14  ;   ;Global Dimension 1 Filter;Code20   ;FieldClass=FlowFilter;
                                                   TableRelation="Dimension Value".Code WHERE (Global Dimension No.=CONST(1));
                                                   CaptionML=[ENU=Global Dimension 1 Filter;
                                                              PTG=Filtro Dimens�o 1 Global];
                                                   CaptionClass='1,3,1' }
    { 15  ;   ;Global Dimension 2 Filter;Code20   ;FieldClass=FlowFilter;
                                                   TableRelation="Dimension Value".Code WHERE (Global Dimension No.=CONST(2));
                                                   CaptionML=[ENU=Global Dimension 2 Filter;
                                                              PTG=Filtro Dimens�o 2 Global];
                                                   CaptionClass='1,3,2' }
    { 16  ;   ;Amount Grouped      ;Decimal       ;FieldClass=FlowField;
                                                   CalcFormula=Sum("Closed Cartera Doc."."Amount for Collection" WHERE (Bill Gr./Pmt. Order No.=FIELD(No.),
                                                                                                                        Global Dimension 1 Code=FIELD(Global Dimension 1 Filter),
                                                                                                                        Global Dimension 2 Code=FIELD(Global Dimension 2 Filter),
                                                                                                                        Status=FIELD(Status Filter),
                                                                                                                        Type=CONST(Receivable)));
                                                   CaptionML=[ENU=Amount Grouped;
                                                              PTG=Valor em Remessa];
                                                   Editable=No;
                                                   AutoFormatType=1;
                                                   AutoFormatExpr="Currency Code" }
    { 18  ;   ;Status Filter       ;Option        ;FieldClass=FlowFilter;
                                                   CaptionML=[ENU=Status Filter;
                                                              PTG=Filtro Estado];
                                                   OptionCaptionML=[ENU=,Honored,Rejected;
                                                                    PTG=,Pago,Devolvido];
                                                   OptionString=,Honored,Rejected;
                                                   Editable=No }
    { 20  ;   ;Closing Date        ;Date          ;CaptionML=[ENU=Closing Date;
                                                              PTG=Data Fecho] }
    { 29  ;   ;Collection Expenses Amt.;Decimal   ;CaptionML=[ENU=Collection Expenses Amt.;
                                                              PTG=Valor Despesas Cobr.];
                                                   AutoFormatType=1;
                                                   AutoFormatExpr="Currency Code" }
    { 30  ;   ;Discount Expenses Amt.;Decimal     ;CaptionML=[ENU=Discount Expenses Amt.;
                                                              PTG=Valos Despesas Desconto];
                                                   AutoFormatType=1;
                                                   AutoFormatExpr="Currency Code" }
    { 31  ;   ;Discount Interests Amt.;Decimal    ;CaptionML=[ENU=Discount Interests Amt.;
                                                              PTG=Valor Juros Desconto];
                                                   AutoFormatType=1;
                                                   AutoFormatExpr="Currency Code" }
    { 32  ;   ;Rejection Expenses Amt.;Decimal    ;CaptionML=[ENU=Rejection Expenses Amt.;
                                                              PTG=Valor Despesas Devolu��o];
                                                   AutoFormatType=1;
                                                   AutoFormatExpr="Currency Code" }
    { 33  ;   ;Currency Code       ;Code10        ;TableRelation=Currency;
                                                   CaptionML=[ENU=Currency Code;
                                                              PTG=C�d. Divisa];
                                                   Editable=No }
    { 35  ;   ;Amount Grouped (LCY);Decimal       ;FieldClass=FlowField;
                                                   CalcFormula=Sum("Closed Cartera Doc."."Amt. for Collection (LCY)" WHERE (Bill Gr./Pmt. Order No.=FIELD(No.),
                                                                                                                            Global Dimension 1 Code=FIELD(Global Dimension 1 Filter),
                                                                                                                            Global Dimension 2 Code=FIELD(Global Dimension 2 Filter),
                                                                                                                            Status=FIELD(Status Filter),
                                                                                                                            Type=CONST(Receivable)));
                                                   CaptionML=[ENU=Amount Grouped (LCY);
                                                              PTG=Valor em Remessa (DL)];
                                                   Editable=No;
                                                   AutoFormatType=1 }
    { 37  ;   ;Risked Factoring Exp. Amt.;Decimal ;CaptionML=[ENU=Risked Factoring Exp. Amt.;
                                                              PTG=Valor Serv. Factoring Com Risco];
                                                   AutoFormatType=1;
                                                   AutoFormatExpr="Currency Code" }
    { 38  ;   ;Unrisked Factoring Exp. Amt.;Decimal;
                                                   CaptionML=[ENU=Unrisked Factoring Exp. Amt.;
                                                              PTG=Valor Serv. Factoring Sem Risco];
                                                   AutoFormatType=1;
                                                   AutoFormatExpr="Currency Code" }
    { 39  ;   ;Factoring           ;Option        ;CaptionML=[ENU=Factoring;
                                                              PTG=Factoring];
                                                   OptionCaptionML=[ENU=" ,Unrisked,Risked";
                                                                    PTG=" ,Sem Risco,Com Risco"];
                                                   OptionString=[ ,Unrisked,Risked];
                                                   Editable=No }
    { 40  ;   ;Partner Type        ;Option        ;CaptionML=[ENU=Partner Type;
                                                              PTG=Tipo  Parceiro];
                                                   OptionCaptionML=[ENU=" ,Company,Person";
                                                                    ESP=" ,Empresa,Persona";
                                                                    PTG=" ,Empresa,Pessoa"];
                                                   OptionString=[ ,Company,Person] }
  }
  KEYS
  {
    {    ;No.                                     ;Clustered=Yes }
    {    ;Bank Account No.,Posting Date           ;SumIndexFields=Collection Expenses Amt.,Discount Expenses Amt.,Discount Interests Amt.,Rejection Expenses Amt.,Risked Factoring Exp. Amt.,Unrisked Factoring Exp. Amt. }
    {    ;Bank Account No.,Posting Date,Factoring ;SumIndexFields=Collection Expenses Amt.,Discount Expenses Amt.,Discount Interests Amt.,Rejection Expenses Amt.,Risked Factoring Exp. Amt.,Unrisked Factoring Exp. Amt. }
  }
  FIELDGROUPS
  {
  }
  CODE
  {
    VAR
      Text31022890@1000 : TextConst 'ENU=untitled;PTG=SemT�tulo';
      ClosedBillGr@1110000 : Record 31022940;
      ClosedDoc@1110001 : Record 31022937;
      BGPOCommentLine@1110002 : Record 31022941;

    PROCEDURE PrintRecords@6(ShowRequestForm@1110000 : Boolean);
    VAR
      CarteraReportSelection@1110001 : Record 31022946;
    BEGIN
      WITH ClosedBillGr DO BEGIN
        COPY(Rec);
        CarteraReportSelection.SETRANGE(Usage,CarteraReportSelection.Usage::"Closed Bill Group");
        CarteraReportSelection.SETFILTER("Report ID",'<>0');
        CarteraReportSelection.FINDSET;
        REPEAT
          REPORT.RUNMODAL(CarteraReportSelection."Report ID",ShowRequestForm,FALSE,ClosedBillGr);
        UNTIL CarteraReportSelection.NEXT = 0;
      END;
    END;

    PROCEDURE Caption@9() : Text[100];
    VAR
      BankAcc@1110000 : Record 270;
    BEGIN
      IF "No." = '' THEN
        EXIT(Text31022890);
      CALCFIELDS("Bank Account Name");
      EXIT(STRSUBSTNO('%1 %2',"No.","Bank Account Name"));
    END;

    BEGIN
    END.
  }
}

