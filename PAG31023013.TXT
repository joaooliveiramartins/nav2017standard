OBJECT Page 31023013 Docs. in Posted PO Subform
{
  OBJECT-PROPERTIES
  {
    Date=11/04/17;
    Time=12:30:05;
    Modified=Yes;
    Version List=NAVPT10.00#00015;
  }
  PROPERTIES
  {
    Permissions=TableData 31022936=m;
    CaptionML=[ENU=Docs. in Posted Payment Orders;
               PTG=Docs. em Ordem Pagamento Registada];
    InsertAllowed=No;
    DeleteAllowed=No;
    SourceTable=Table31022936;
    SourceTableView=WHERE(Type=CONST(Payable));
    PageType=ListPart;
    OnModifyRecord=BEGIN
                     CODEUNIT.RUN(CODEUNIT::"Posted Cartera Doc.- Edit",Rec);
                     EXIT(FALSE);
                   END;

    ActionList=ACTIONS
    {
      { 1900000004;0 ;ActionContainer;
                      ActionContainerType=ActionItems }
      { 1907380404;1 ;ActionGroup;
                      CaptionML=[ENU=&Documents;
                                 PTG=&Documentos] }
      { 1902340004;2 ;Action    ;
                      Name=Dimensions;
                      ShortCutKey=Shift+Ctrl+D;
                      CaptionML=[ENU=Dime&nsions;
                                 PTG=Dime&ns�es];
                      ApplicationArea=#Basic,#Suite;
                      Image=Dimensions;
                      OnAction=BEGIN
                                 ShowDimension;
                               END;
                                }
      { 1902759804;2 ;Action    ;
                      Name=Categorize;
                      Ellipsis=Yes;
                      CaptionML=[ENU=Categorize;
                                 PTG=Classificar];
                      ApplicationArea=#Basic,#Suite;
                      Image=Category;
                      OnAction=BEGIN
                                 Categorize;
                               END;
                                }
      { 1900206204;2 ;Action    ;
                      Name=Decategorize;
                      CaptionML=[ENU=Decategorize;
                                 PTG=Desclassificar];
                      ApplicationArea=#Basic,#Suite;
                      Image=UndoCategory;
                      OnAction=BEGIN
                                 Decategorize;
                               END;
                                }
      { 1900295704;2 ;ActionGroup;
                      CaptionML=[ENU=Settle;
                                 PTG=Liquidar];
                      Enabled=TRUE;
                      Image=SettleOpenTransactions }
      { 1900725504;3 ;Action    ;
                      Name=TotalSettlement;
                      Ellipsis=Yes;
                      CaptionML=[ENU=Total Settlement;
                                 PTG=Liquida��o Total];
                      ApplicationArea=#Basic,#Suite;
                      OnAction=BEGIN
                                 Settle;
                               END;
                                }
      { 1900725604;3 ;Action    ;
                      Name=PartialSettlement;
                      Ellipsis=Yes;
                      CaptionML=[ENU=P&artial Settlement;
                                 PTG=Liquida��o &Parcial];
                      ApplicationArea=#Basic,#Suite;
                      OnAction=BEGIN
                                 PartialSettle;
                               END;
                                }
      { 31022891;2   ;Action    ;
                      Name=Reject;
                      Ellipsis=Yes;
                      CaptionML=[ENU=Reject;
                                 PTG=Devolver];
                      Image=Reject;
                      OnAction=BEGIN
                                 Reject;
                               END;
                                }
      { 1903866804;2 ;Action    ;
                      Name=Redraw;
                      Ellipsis=Yes;
                      CaptionML=[ENU=Redraw;
                                 PTG=Reformar];
                      ApplicationArea=#Basic,#Suite;
                      OnAction=BEGIN
                                 Redraw;
                               END;
                                }
      { 31022890;2   ;Action    ;
                      Name=Navigate;
                      CaptionML=[ENU=&Navigate;
                                 PTG=&Navegar];
                      Image=Navigate;
                      OnAction=BEGIN
                                 Navigate;
                               END;
                                }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 2   ;2   ;Field     ;
                ApplicationArea=#All;
                SourceExpr="Posting Date";
                Visible=FALSE;
                Editable=FALSE }

    { 40  ;2   ;Field     ;
                ApplicationArea=#All;
                SourceExpr="Document Type" }

    { 4   ;2   ;Field     ;
                ApplicationArea=#All;
                SourceExpr="Due Date";
                Editable=FALSE }

    { 24  ;2   ;Field     ;
                ApplicationArea=#All;
                SourceExpr=Status;
                Editable=FALSE }

    { 34  ;2   ;Field     ;
                ApplicationArea=#All;
                SourceExpr="Honored/Rejtd. at Date";
                Visible=FALSE;
                Editable=FALSE }

    { 20  ;2   ;Field     ;
                ApplicationArea=#All;
                SourceExpr="Payment Method Code";
                Editable=FALSE }

    { 6   ;2   ;Field     ;
                ApplicationArea=#All;
                SourceExpr="Document No.";
                Editable=FALSE }

    { 8   ;2   ;Field     ;
                ApplicationArea=#All;
                SourceExpr="No.";
                Editable=FALSE }

    { 10  ;2   ;Field     ;
                ApplicationArea=#All;
                SourceExpr=Description;
                Editable=FALSE }

    { 30  ;2   ;Field     ;
                ApplicationArea=#All;
                SourceExpr="Original Amount";
                Visible=FALSE;
                Editable=FALSE }

    { 32  ;2   ;Field     ;
                ApplicationArea=#All;
                SourceExpr="Original Amt. (LCY)";
                Visible=FALSE;
                Editable=FALSE }

    { 22  ;2   ;Field     ;
                ApplicationArea=#All;
                SourceExpr="Amount for Collection";
                Editable=FALSE }

    { 36  ;2   ;Field     ;
                ApplicationArea=#All;
                SourceExpr="Amt. for Collection (LCY)";
                Visible=FALSE;
                Editable=FALSE }

    { 12  ;2   ;Field     ;
                ApplicationArea=#All;
                SourceExpr="Remaining Amount";
                Editable=FALSE }

    { 38  ;2   ;Field     ;
                ApplicationArea=#All;
                SourceExpr="Remaining Amt. (LCY)";
                Visible=FALSE;
                Editable=FALSE }

    { 28  ;2   ;Field     ;
                ApplicationArea=#All;
                SourceExpr=Redrawn;
                Editable=FALSE }

    { 14  ;2   ;Field     ;
                ApplicationArea=#All;
                SourceExpr=Place;
                Visible=FALSE;
                Editable=FALSE }

    { 16  ;2   ;Field     ;
                ApplicationArea=#All;
                SourceExpr="Category Code" }

    { 18  ;2   ;Field     ;
                ApplicationArea=#All;
                SourceExpr="Account No.";
                Visible=FALSE;
                Editable=FALSE }

    { 26  ;2   ;Field     ;
                ApplicationArea=#All;
                SourceExpr="Entry No.";
                Editable=FALSE }

  }
  CODE
  {
    VAR
      Text31022890@1000 : TextConst 'ENU=No documents have been found that can be settled. \;PTG=N�o existem documentos a liquidar. \';
      Text31022891@1001 : TextConst 'ENU=Please check that at least one open document was selected.;PTG=Confira que selecionou no m�nimo um documento pendente.';
      Text31022892@1002 : TextConst 'ENU=No documents have been found that can be rejected. \;PTG=N�o existem documentos a devolver. \';
      Text31022893@1003 : TextConst 'ENU=Only invoices in Bill Groups marked as %1 Risked can be rejected.;PTG=S� podem ser devolvidas as faturas em remessas marcadas com risco %1.';
      Text31022894@1004 : TextConst 'ENU=No documents have been found that can be redrawn. \;PTG=N�o existem documentos que possam ser reformados. \';
      Text31022895@1005 : TextConst 'ENU=Please check that at least one rejected or honored document was selected.;PTG=Confira que selecionou no m�nimo um documento pago ou devolvido.';
      Text31022896@1006 : TextConst 'ENU=Only bills can be redrawn.;PTG=S� podem ser reformados t�tulos.';
      Text31022897@1007 : TextConst 'ENU=Please check that one open document was selected.;PTG=Confira que foi selecionado um documento pendente.';
      Text31022898@1008 : TextConst 'ENU=Only one open document can be selected;PTG=S� pode ser selecionado um documento pendente';
      PostedDoc@1110000 : Record 31022936;
      CustLedgEntry@1110001 : Record 21;
      VendLedgEntry@1110002 : Record 25;
      CarteraManagement@1110004 : Codeunit 31022901;
      DimMgmt@1009 : Codeunit 408;

    PROCEDURE Categorize@6();
    BEGIN
      CurrPage.SETSELECTIONFILTER(PostedDoc);
      CarteraManagement.CategorizePostedDocs(PostedDoc);
    END;

    PROCEDURE Decategorize@7();
    BEGIN
      CurrPage.SETSELECTIONFILTER(PostedDoc);
      CarteraManagement.DecategorizePostedDocs(PostedDoc);
    END;

    PROCEDURE Settle@2();
    BEGIN
      CurrPage.SETSELECTIONFILTER(PostedDoc);
      IF PostedDoc.ISEMPTY THEN
        EXIT;

      PostedDoc.SETRANGE(Status,PostedDoc.Status::Open);
      IF PostedDoc.ISEMPTY THEN
        ERROR(Text31022890 + Text31022891);

      REPORT.RUNMODAL(REPORT::"Settle Docs. in Posted PO",TRUE,FALSE,PostedDoc);
      CurrPage.UPDATE(FALSE);
    END;

    PROCEDURE Reject@3();
    VAR
      PostedBillGr@1110000 : Record 31022939;
    BEGIN
      CurrPage.SETSELECTIONFILTER(PostedDoc);
      IF PostedDoc.ISEMPTY THEN
        EXIT;

      PostedDoc.SETRANGE(Status,PostedDoc.Status::Open);
      IF NOT PostedDoc.FINDSET THEN
        ERROR(Text31022892 + Text31022891);

      IF PostedDoc.Factoring <> PostedDoc.Factoring::" " THEN BEGIN
        PostedBillGr.GET(PostedDoc."Bill Gr./Pmt. Order No.");
        IF PostedBillGr.Factoring = PostedBillGr.Factoring::Unrisked THEN
          ERROR(Text31022893,PostedBillGr.FIELDCAPTION(Factoring));
      END;

      CustLedgEntry.RESET;
      REPEAT
        CustLedgEntry.GET(PostedDoc."Entry No.");
        CustLedgEntry.MARK(TRUE);
      UNTIL PostedDoc.NEXT = 0;

      CustLedgEntry.MARKEDONLY(TRUE);
      REPORT.RUNMODAL(REPORT::"Reject Docs.",TRUE,FALSE,CustLedgEntry);
      CurrPage.UPDATE(FALSE);
    END;

    PROCEDURE Redraw@5();
    BEGIN
      CurrPage.SETSELECTIONFILTER(PostedDoc);
      IF PostedDoc.ISEMPTY THEN
        EXIT;

      PostedDoc.SETFILTER(Status,'<>%1',PostedDoc.Status::Open);
      IF PostedDoc.ISEMPTY THEN
        ERROR(Text31022894 + Text31022895);

      PostedDoc.SETFILTER("Document Type",'<>%1',PostedDoc."Document Type"::Bill);
      IF NOT PostedDoc.ISEMPTY THEN
        ERROR(Text31022896);

      PostedDoc.SETRANGE("Document Type");

      VendLedgEntry.RESET;
      REPEAT
        VendLedgEntry.GET(PostedDoc."Entry No.");
        VendLedgEntry.MARK(TRUE);
      UNTIL PostedDoc.NEXT = 0;

      VendLedgEntry.MARKEDONLY(TRUE);
      REPORT.RUNMODAL(REPORT::"Redraw Payable Bills",TRUE,FALSE,VendLedgEntry);
      CurrPage.UPDATE(FALSE);
    END;

    PROCEDURE Navigate@1();
    BEGIN
      CarteraManagement.NavigatePostedDoc(Rec);
    END;

    PROCEDURE PartialSettle@8();
    VAR
      PartialSettlePayable@1110000 : Report 31022981;
      PartialSettleReceivable@1110001 : Report 31022980;
      CustLedgEntry2@1110002 : Record 21;
      VendLedgEntry2@1110003 : Record 25;
    BEGIN
      CurrPage.SETSELECTIONFILTER(PostedDoc);
      IF PostedDoc.ISEMPTY THEN
        EXIT;

      PostedDoc.SETRANGE(Status,PostedDoc.Status::Open);
      IF PostedDoc.ISEMPTY THEN
        ERROR(Text31022890 + Text31022897);

      IF PostedDoc.COUNT > 1 THEN
        ERROR(Text31022898);

      PostedDoc.FINDFIRST;

      CLEAR(PartialSettlePayable);
      VendLedgEntry2.GET(PostedDoc."Entry No.");
      IF (WORKDATE <= VendLedgEntry2."Pmt. Discount Date") AND
         (PostedDoc."Document Type" = PostedDoc."Document Type"::Invoice) THEN
        PartialSettlePayable.SetInitValue(PostedDoc."Remaining Amount" + VendLedgEntry2."Remaining Pmt. Disc. Possible",
          PostedDoc."Currency Code",PostedDoc."Entry No.")
      ELSE
        PartialSettlePayable.SetInitValue(PostedDoc."Remaining Amount",
          PostedDoc."Currency Code",PostedDoc."Entry No.");
      PartialSettlePayable.SETTABLEVIEW(PostedDoc);
      PartialSettlePayable.RUNMODAL;
      CurrPage.UPDATE(FALSE);
    END;

    PROCEDURE ShowDimension@1110000();
    BEGIN
      DimMgmt.ShowDimensionSet("Dimension Set ID",STRSUBSTNO('%1 %2 %3',"Document Type","Document No.",Description));
    END;

    BEGIN
    {
      20170306 V10.00#00015 : Subform BG/PO
    }
    END.
  }
}

