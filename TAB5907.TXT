OBJECT Table 5907 Service Ledger Entry
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Service Ledger Entry;
               PTG=Mov. Servi�o];
    LookupPageID=Page5912;
    DrillDownPageID=Page5912;
  }
  FIELDS
  {
    { 1   ;   ;Entry No.           ;Integer       ;CaptionML=[ENU=Entry No.;
                                                              PTG=N� Mov.] }
    { 2   ;   ;Service Contract No.;Code20        ;TableRelation="Service Contract Header"."Contract No." WHERE (Contract Type=CONST(Contract));
                                                   CaptionML=[ENU=Service Contract No.;
                                                              PTG=N� Contrato Servi�o] }
    { 3   ;   ;Document Type       ;Option        ;CaptionML=[ENU=Document Type;
                                                              PTG=Tipo Documento];
                                                   OptionCaptionML=[ENU=" ,Payment,Invoice,Credit Memo,Finance Charge Memo,Reminder,Refund,Shipment";
                                                                    PTG=" ,Pagamento,Fatura,Nota Cr�dito,Nota Juros,Carta Aviso,Reembolso,Guia Remessa"];
                                                   OptionString=[ ,Payment,Invoice,Credit Memo,Finance Charge Memo,Reminder,Refund,Shipment] }
    { 4   ;   ;Document No.        ;Code20        ;CaptionML=[ENU=Document No.;
                                                              PTG=N� Documento] }
    { 5   ;   ;Serv. Contract Acc. Gr. Code;Code10;TableRelation="Service Contract Account Group".Code;
                                                   CaptionML=[ENU=Serv. Contract Acc. Gr. Code;
                                                              PTG=C�d. Grp. Conta Contrato Serv.] }
    { 6   ;   ;Document Line No.   ;Integer       ;CaptionML=[ENU=Document Line No.;
                                                              PTG=N� Linha Documento] }
    { 8   ;   ;Moved from Prepaid Acc.;Boolean    ;CaptionML=[ENU=Moved from Prepaid Acc.;
                                                              PTG=Movido de Conta Pr�-Paga] }
    { 9   ;   ;Posting Date        ;Date          ;CaptionML=[ENU=Posting Date;
                                                              PTG=Data Registo] }
    { 11  ;   ;Amount (LCY)        ;Decimal       ;CaptionML=[ENU=Amount (LCY);
                                                              PTG=Valor (DL)];
                                                   AutoFormatType=1 }
    { 12  ;   ;Customer No.        ;Code20        ;TableRelation=Customer;
                                                   CaptionML=[ENU=Customer No.;
                                                              PTG=N� Cliente] }
    { 13  ;   ;Ship-to Code        ;Code10        ;TableRelation="Ship-to Address".Code WHERE (Customer No.=FIELD(Customer No.));
                                                   CaptionML=[ENU=Ship-to Code;
                                                              PTG=Envio-a C�d. Endere�o] }
    { 14  ;   ;Item No. (Serviced) ;Code20        ;TableRelation=Item;
                                                   CaptionML=[ENU=Item No. (Serviced);
                                                              PTG=N� Produto (Reparado)] }
    { 15  ;   ;Serial No. (Serviced);Code20       ;CaptionML=[ENU=Serial No. (Serviced);
                                                              PTG=N� S�rie (Reparado)] }
    { 16  ;   ;User ID             ;Code50        ;TableRelation=User."User Name";
                                                   OnLookup=VAR
                                                              UserMgt@1000 : Codeunit 418;
                                                            BEGIN
                                                              UserMgt.LookupUserID("User ID");
                                                            END;

                                                   TestTableRelation=No;
                                                   CaptionML=[ENU=User ID;
                                                              PTG=ID Utilizador] }
    { 17  ;   ;Contract Invoice Period;Text30     ;CaptionML=[ENU=Contract Invoice Period;
                                                              PTG=Per�odo Fatura Contrato] }
    { 18  ;   ;Global Dimension 1 Code;Code20     ;TableRelation="Dimension Value".Code WHERE (Global Dimension No.=CONST(1));
                                                   CaptionML=[ENU=Global Dimension 1 Code;
                                                              PTG=C�d. Dimens�o 1 Global];
                                                   CaptionClass='1,2,1' }
    { 19  ;   ;Global Dimension 2 Code;Code20     ;TableRelation="Dimension Value".Code WHERE (Global Dimension No.=CONST(2));
                                                   CaptionML=[ENU=Global Dimension 2 Code;
                                                              PTG=C�d. Dimens�o 2 Global];
                                                   CaptionClass='1,2,2' }
    { 20  ;   ;Service Item No. (Serviced);Code20 ;TableRelation="Service Item";
                                                   CaptionML=[ENU=Service Item No. (Serviced);
                                                              PTG=N� Produto Servi�o (Reparado)] }
    { 21  ;   ;Variant Code (Serviced);Code10     ;TableRelation="Item Variant".Code WHERE (Item No.=FIELD("Item No. (Serviced)"));
                                                   CaptionML=[ENU=Variant Code (Serviced);
                                                              PTG=C�d Variante (Reparado)] }
    { 22  ;   ;Contract Group Code ;Code10        ;TableRelation="Contract Group".Code;
                                                   CaptionML=[ENU=Contract Group Code;
                                                              PTG=C�d. Grupo Contrato] }
    { 23  ;   ;Type                ;Option        ;CaptionML=[ENU=Type;
                                                              PTG=Tipo];
                                                   OptionCaptionML=[ENU=" ,Resource,Item,Service Cost,Service Contract,G/L Account";
                                                                    PTG=" ,Recurso,Produto,Custo Servi�o,Contrato Servi�o,Conta C/G"];
                                                   OptionString=[ ,Resource,Item,Service Cost,Service Contract,G/L Account] }
    { 24  ;   ;No.                 ;Code20        ;TableRelation=IF (Type=CONST(Service Contract)) "Service Contract Header"."Contract No." WHERE (Contract Type=CONST(Contract))
                                                                 ELSE IF (Type=CONST(" ")) "Standard Text"
                                                                 ELSE IF (Type=CONST(Item)) Item
                                                                 ELSE IF (Type=CONST(Resource)) Resource
                                                                 ELSE IF (Type=CONST(Service Cost)) "Service Cost"
                                                                 ELSE IF (Type=CONST(G/L Account)) "G/L Account";
                                                   CaptionML=[ENU=No.;
                                                              PTG=N�] }
    { 25  ;   ;Cost Amount         ;Decimal       ;CaptionML=[ENU=Cost Amount;
                                                              PTG=Valor Custo];
                                                   AutoFormatType=1 }
    { 26  ;   ;Discount Amount     ;Decimal       ;CaptionML=[ENU=Discount Amount;
                                                              PTG=Valor Desconto];
                                                   AutoFormatType=1 }
    { 27  ;   ;Unit Cost           ;Decimal       ;CaptionML=[ENU=Unit Cost;
                                                              PTG=Custo Unit�rio];
                                                   AutoFormatType=2 }
    { 28  ;   ;Quantity            ;Decimal       ;CaptionML=[ENU=Quantity;
                                                              PTG=Quantidade];
                                                   DecimalPlaces=0:5 }
    { 29  ;   ;Charged Qty.        ;Decimal       ;CaptionML=[ENU=Charged Qty.;
                                                              PTG=Qtd. Cobrada];
                                                   DecimalPlaces=0:5 }
    { 30  ;   ;Unit Price          ;Decimal       ;CaptionML=[ENU=Unit Price;
                                                              PTG=Pre�o Unit�rio];
                                                   AutoFormatType=2 }
    { 31  ;   ;Discount %          ;Decimal       ;CaptionML=[ENU=Discount %;
                                                              PTG=% Desconto];
                                                   DecimalPlaces=0:5 }
    { 32  ;   ;Contract Disc. Amount;Decimal      ;CaptionML=[ENU=Contract Disc. Amount;
                                                              PTG=Valor Desconto Contrato];
                                                   AutoFormatType=1 }
    { 33  ;   ;Bill-to Customer No.;Code20        ;TableRelation=Customer;
                                                   CaptionML=[ENU=Bill-to Customer No.;
                                                              PTG=Fatura-a N� Cliente] }
    { 34  ;   ;Fault Reason Code   ;Code10        ;TableRelation="Fault Reason Code";
                                                   CaptionML=[ENU=Fault Reason Code;
                                                              PTG=C�d. Raz�o Falha] }
    { 35  ;   ;Description         ;Text50        ;CaptionML=[ENU=Description;
                                                              PTG=Descri��o] }
    { 37  ;   ;Service Order Type  ;Code10        ;TableRelation="Service Order Type";
                                                   CaptionML=[ENU=Service Order Type;
                                                              PTG=Tipo Ordem Servi�o] }
    { 39  ;   ;Service Order No.   ;Code20        ;OnLookup=BEGIN
                                                              CLEAR(ServOrderMgt);
                                                              ServOrderMgt.ServHeaderLookup(1,"Service Order No.");
                                                            END;

                                                   CaptionML=[ENU=Service Order No.;
                                                              PTG=N� Ordem Servi�o] }
    { 40  ;   ;Job No.             ;Code20        ;TableRelation=Job.No. WHERE (Bill-to Customer No.=FIELD(Bill-to Customer No.));
                                                   CaptionML=[ENU=Job No.;
                                                              PTG=N� Projeto] }
    { 41  ;   ;Gen. Bus. Posting Group;Code10     ;TableRelation="Gen. Business Posting Group";
                                                   CaptionML=[ENU=Gen. Bus. Posting Group;
                                                              PTG=Gr. Contabil�stico Neg�cio] }
    { 42  ;   ;Gen. Prod. Posting Group;Code10    ;TableRelation="Gen. Product Posting Group";
                                                   CaptionML=[ENU=Gen. Prod. Posting Group;
                                                              PTG=Gr. Contabil�stico Produto] }
    { 43  ;   ;Location Code       ;Code10        ;TableRelation=Location;
                                                   CaptionML=[ENU=Location Code;
                                                              PTG=C�d. Localiza��o] }
    { 44  ;   ;Unit of Measure Code;Code10        ;TableRelation="Unit of Measure";
                                                   CaptionML=[ENU=Unit of Measure Code;
                                                              PTG=C�d. Unidade Medida] }
    { 45  ;   ;Work Type Code      ;Code10        ;TableRelation="Work Type";
                                                   CaptionML=[ENU=Work Type Code;
                                                              PTG=C�d. Tipo Trabalho] }
    { 46  ;   ;Bin Code            ;Code20        ;TableRelation=Bin.Code WHERE (Location Code=FIELD(Location Code));
                                                   CaptionML=[ENU=Bin Code;
                                                              PTG=C�d. Posi��o] }
    { 47  ;   ;Responsibility Center;Code10       ;TableRelation="Responsibility Center";
                                                   CaptionML=[ENU=Responsibility Center;
                                                              PTG=Centro Responsabilidade] }
    { 48  ;   ;Variant Code        ;Code10        ;TableRelation=IF (Type=CONST(Item)) "Item Variant".Code WHERE (Item No.=FIELD(No.));
                                                   CaptionML=[ENU=Variant Code;
                                                              PTG=C�d. Variante] }
    { 50  ;   ;Entry Type          ;Option        ;CaptionML=[ENU=Entry Type;
                                                              PTG=Tipo Mov.];
                                                   OptionCaptionML=[ENU=Usage,Sale,Consume,Contract;
                                                                    PTG=Utiliza��o,Venda,Consumo,Contrato];
                                                   OptionString=Usage,Sale,Consume,Contract }
    { 51  ;   ;Open                ;Boolean       ;CaptionML=[ENU=Open;
                                                              PTG=Pendente] }
    { 52  ;   ;Serv. Price Adjmt. Gr. Code;Code10 ;TableRelation="Service Price Adjustment Group";
                                                   CaptionML=[ENU=Serv. Price Adjmt. Gr. Code;
                                                              PTG=C�d. Grupo Ajuste Pre�o Servi�o] }
    { 53  ;   ;Service Price Group Code;Code10    ;TableRelation="Service Price Group";
                                                   CaptionML=[ENU=Service Price Group Code;
                                                              PTG=C�d. Grupo Pre�o Servi�o] }
    { 54  ;   ;Prepaid             ;Boolean       ;CaptionML=[ENU=Prepaid;
                                                              PTG=Pr�-Pago] }
    { 55  ;   ;Apply Until Entry No.;Integer      ;CaptionML=[ENU=Apply Until Entry No.;
                                                              PTG=Liq. at� N� Mov.] }
    { 56  ;   ;Applies-to Entry No.;Integer       ;AccessByPermission=TableData 27=R;
                                                   CaptionML=[ENU=Applies-to Entry No.;
                                                              PTG=Liq. por N� Mov.] }
    { 57  ;   ;Amount              ;Decimal       ;CaptionML=[ENU=Amount;
                                                              PTG=Valor];
                                                   AutoFormatType=1 }
    { 58  ;   ;Job Task No.        ;Code20        ;TableRelation="Job Task"."Job Task No." WHERE (Job No.=FIELD(Job No.));
                                                   CaptionML=[ENU=Job Task No.;
                                                              PTG=N� Tarefa Projeto] }
    { 59  ;   ;Job Line Type       ;Option        ;InitValue=Budget;
                                                   CaptionML=[ENU=Job Line Type;
                                                              PTG=Tipo Linha Projeto];
                                                   OptionCaptionML=[ENU=" ,Budget,Billable,Both Budget and Billable";
                                                                    PTG=" ,Or�amentado,Fatur�vel,Or�amentado e Fatur�vel"];
                                                   OptionString=[ ,Budget,Billable,Both Budget and Billable] }
    { 60  ;   ;Job Posted          ;Boolean       ;CaptionML=[ENU=Job Posted;
                                                              PTG=Projeto Registado] }
    { 480 ;   ;Dimension Set ID    ;Integer       ;TableRelation="Dimension Set Entry";
                                                   OnLookup=BEGIN
                                                              ShowDimensions;
                                                            END;

                                                   CaptionML=[ENU=Dimension Set ID;
                                                              PTG=ID Combina��o Dimens�o];
                                                   Editable=No }
  }
  KEYS
  {
    {    ;Entry No.                               ;Clustered=Yes }
    {    ;Document No.,Posting Date                }
    {    ;Entry Type,Document Type,Document No.,Document Line No. }
    {    ;Service Contract No.,Entry No.,Entry Type,Type,Moved from Prepaid Acc.,Posting Date,Open,Prepaid,Service Item No. (Serviced),Customer No.,Contract Group Code,Responsibility Center;
                                                   SumIndexFields=Amount (LCY),Cost Amount,Quantity,Charged Qty.,Contract Disc. Amount }
    {    ;Service Order No.,Service Item No. (Serviced),Entry Type,Moved from Prepaid Acc.,Posting Date,Open,Type,Service Contract No.;
                                                   SumIndexFields=Amount (LCY),Cost Amount,Quantity,Charged Qty.,Amount }
    {    ;Type,No.,Entry Type,Moved from Prepaid Acc.,Posting Date,Open,Prepaid;
                                                   SumIndexFields=Amount (LCY),Cost Amount,Quantity,Charged Qty. }
    {    ;Service Item No. (Serviced),Entry Type,Moved from Prepaid Acc.,Type,Posting Date,Open,Service Contract No.,Prepaid,Customer No.,Contract Group Code,Responsibility Center;
                                                   SumIndexFields=Amount (LCY),Cost Amount }
    {    ;Service Item No. (Serviced),Entry Type,Type,Service Contract No.,Posting Date,Service Order No.;
                                                   SumIndexFields=Amount (LCY),Cost Amount,Quantity,Charged Qty. }
  }
  FIELDGROUPS
  {
    { 1   ;DropDown            ;Entry No.,Entry Type,Service Contract No.,Posting Date }
  }
  CODE
  {
    VAR
      ServOrderMgt@1001 : Codeunit 5900;
      DimMgt@1000 : Codeunit 408;

    PROCEDURE ShowDimensions@1();
    BEGIN
      DimMgt.ShowDimensionSet("Dimension Set ID",STRSUBSTNO('%1 %2',TABLECAPTION,"Entry No."));
    END;

    PROCEDURE CopyFromServHeader@4(ServHeader@1000 : Record 5900);
    BEGIN
      "Service Order Type" := ServHeader."Service Order Type";
      "Customer No." := ServHeader."Customer No.";
      "Bill-to Customer No." := ServHeader."Bill-to Customer No.";
      "Service Order Type" := ServHeader."Service Order Type";
      "Responsibility Center" := ServHeader."Responsibility Center";
    END;

    PROCEDURE CopyFromServLine@2(ServLine@1000 : Record 5902;DocNo@1002 : Code[20]);
    BEGIN
      CASE ServLine.Type OF
        ServLine.Type::Item:
          BEGIN
            Type := Type::Item;
            "Bin Code" := ServLine."Bin Code";
          END;
        ServLine.Type::Resource:
          Type := Type::Resource;
        ServLine.Type::Cost:
          Type := Type::"Service Cost";
        ServLine.Type::"G/L Account":
          Type := Type::"G/L Account";
      END;

      IF ServLine."Document Type" = ServLine."Document Type"::Order THEN
        "Service Order No." := ServLine."Document No.";

      "Location Code" := ServLine."Location Code";
      "Job No." := ServLine."Job No.";
      "Job Task No." := ServLine."Job Task No.";
      "Job Line Type" := ServLine."Job Line Type";

      "Document Type" := "Document Type"::Shipment;
      "Document No." := DocNo;
      "Document Line No." := ServLine."Line No.";
      "Moved from Prepaid Acc." := TRUE;
      "Posting Date" := ServLine."Posting Date";
      "Entry Type" := "Entry Type"::Usage;
      "Ship-to Code" := ServLine."Ship-to Code";
      "Global Dimension 1 Code" := ServLine."Shortcut Dimension 1 Code";
      "Global Dimension 2 Code" := ServLine."Shortcut Dimension 2 Code";
      "Dimension Set ID" := ServLine."Dimension Set ID";
      "Gen. Bus. Posting Group" := ServLine."Gen. Bus. Posting Group";
      "Gen. Prod. Posting Group" := ServLine."Gen. Prod. Posting Group";
      Description := ServLine.Description;
      "Fault Reason Code" := ServLine."Fault Reason Code";
      "Unit of Measure Code" := ServLine."Unit of Measure Code";
      "Work Type Code" := ServLine."Work Type Code";
      "Serv. Price Adjmt. Gr. Code" := ServLine."Serv. Price Adjmt. Gr. Code";
      "Service Price Group Code" := ServLine."Service Price Group Code";
      "Discount %" := ServLine."Line Discount %";
      "Variant Code" := ServLine."Variant Code";
    END;

    PROCEDURE CopyServicedInfo@3(ServiceItemNo@1000 : Code[20];ItemNo@1001 : Code[20];SerialNo@1002 : Code[20];VariantCode@1003 : Code[10]);
    BEGIN
      "Service Item No. (Serviced)" := ServiceItemNo;
      "Item No. (Serviced)" := ItemNo;
      "Serial No. (Serviced)" := SerialNo;
      "Variant Code (Serviced)" := VariantCode;
    END;

    BEGIN
    END.
  }
}

