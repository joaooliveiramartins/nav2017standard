OBJECT Table 461 Prepayment Inv. Line Buffer
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0,NAVPTSS93.00;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Prepayment Inv. Line Buffer;
               PTG=Mem. Int. Linha Fatura Pr�-Pagamento];
  }
  FIELDS
  {
    { 1   ;   ;G/L Account No.     ;Code20        ;TableRelation="G/L Account";
                                                   CaptionML=[ENU=G/L Account No.;
                                                              PTG=N� Conta C/G] }
    { 2   ;   ;Line No.            ;Integer       ;CaptionML=[ENU=Line No.;
                                                              PTG=N� Linha] }
    { 3   ;   ;Amount              ;Decimal       ;CaptionML=[ENU=Amount;
                                                              PTG=Valor];
                                                   AutoFormatType=2 }
    { 4   ;   ;Description         ;Text50        ;CaptionML=[ENU=Description;
                                                              PTG=Descri��o] }
    { 5   ;   ;Gen. Bus. Posting Group;Code10     ;TableRelation="Gen. Business Posting Group";
                                                   CaptionML=[ENU=Gen. Bus. Posting Group;
                                                              PTG=Gr. Contabil�stico Neg�cio] }
    { 6   ;   ;Gen. Prod. Posting Group;Code10    ;TableRelation="Gen. Product Posting Group";
                                                   CaptionML=[ENU=Gen. Prod. Posting Group;
                                                              PTG=Gr. Contabil�stico Produto] }
    { 7   ;   ;VAT Bus. Posting Group;Code10      ;TableRelation="VAT Business Posting Group";
                                                   CaptionML=[ENU=VAT Bus. Posting Group;
                                                              PTG=Gr. Registo IVA Neg�cio] }
    { 8   ;   ;VAT Prod. Posting Group;Code10     ;TableRelation="VAT Product Posting Group";
                                                   CaptionML=[ENU=VAT Prod. Posting Group;
                                                              PTG=Gr. Registo IVA Produto] }
    { 9   ;   ;VAT Amount          ;Decimal       ;CaptionML=[ENU=VAT Amount;
                                                              PTG=Valor IVA];
                                                   AutoFormatType=1 }
    { 10  ;   ;VAT Calculation Type;Option        ;CaptionML=[ENU=VAT Calculation Type;
                                                              PTG=Tipo C�lculo IVA];
                                                   OptionCaptionML=[ENU=Normal VAT,Reverse Charge VAT,Full VAT,Sales Tax,,,,,,No Taxable VAT,Stamp Duty;
                                                                    PTG=IVA Normal,IVA Conta Autoliquida��o,IVA Total,Impostos Vendas,,,,,,N�o Sujeito,Imposto Selo];
                                                   OptionString=Normal VAT,Reverse Charge VAT,Full VAT,Sales Tax,,,,,,No Taxable VAT,Stamp Duty;
                                                   Description=soft }
    { 11  ;   ;VAT Base Amount     ;Decimal       ;CaptionML=[ENU=VAT Base Amount;
                                                              PTG=Valor Base IVA];
                                                   AutoFormatType=1 }
    { 12  ;   ;Amount (ACY)        ;Decimal       ;CaptionML=[ENU=Amount (ACY);
                                                              PTG=Valor (DA)];
                                                   AutoFormatType=1 }
    { 13  ;   ;VAT Amount (ACY)    ;Decimal       ;CaptionML=[ENU=VAT Amount (ACY);
                                                              PTG=Valor IVA (DA)];
                                                   AutoFormatType=1 }
    { 14  ;   ;VAT Base Amount (ACY);Decimal      ;CaptionML=[ENU=VAT Base Amount (ACY);
                                                              PTG=Valor Base IVA (DA)];
                                                   AutoFormatType=1 }
    { 15  ;   ;VAT Difference      ;Decimal       ;CaptionML=[ENU=VAT Difference;
                                                              PTG=Diferen�a IVA];
                                                   AutoFormatType=1 }
    { 16  ;   ;VAT %               ;Decimal       ;CaptionML=[ENU=VAT %;
                                                              PTG=% IVA];
                                                   DecimalPlaces=1:1 }
    { 17  ;   ;VAT Identifier      ;Code10        ;CaptionML=[ENU=VAT Identifier;
                                                              PTG=Identificador IVA];
                                                   Editable=No }
    { 19  ;   ;Global Dimension 1 Code;Code20     ;TableRelation="Dimension Value".Code WHERE (Global Dimension No.=CONST(1));
                                                   CaptionML=[ENU=Global Dimension 1 Code;
                                                              PTG=C�d. Dimens�o 1 Global];
                                                   CaptionClass='1,1,1' }
    { 20  ;   ;Global Dimension 2 Code;Code20     ;TableRelation="Dimension Value".Code WHERE (Global Dimension No.=CONST(2));
                                                   CaptionML=[ENU=Global Dimension 2 Code;
                                                              PTG=C�d. Dimens�o 2 Global];
                                                   CaptionClass='1,1,2' }
    { 21  ;   ;Job No.             ;Code20        ;TableRelation=Job;
                                                   CaptionML=[ENU=Job No.;
                                                              PTG=N� Projeto] }
    { 22  ;   ;Amount Incl. VAT    ;Decimal       ;CaptionML=[ENU=Amount Incl. VAT;
                                                              PTG=Valor Incl. IVA];
                                                   AutoFormatType=1 }
    { 24  ;   ;Tax Area Code       ;Code20        ;TableRelation="Tax Area";
                                                   CaptionML=[ENU=Tax Area Code;
                                                              PTG=C�d. �rea Imposto] }
    { 25  ;   ;Tax Liable          ;Boolean       ;CaptionML=[ENU=Tax Liable;
                                                              PTG=Sujeito a Imposto] }
    { 26  ;   ;Tax Group Code      ;Code10        ;TableRelation="Tax Group";
                                                   CaptionML=[ENU=Tax Group Code;
                                                              PTG=C�d. Grupo Imposto] }
    { 27  ;   ;Invoice Rounding    ;Boolean       ;CaptionML=[ENU=Invoice Rounding;
                                                              PTG=Arredondamento fatura] }
    { 28  ;   ;Adjustment          ;Boolean       ;CaptionML=[ENU=Adjustment;
                                                              PTG=Ajuste] }
    { 480 ;   ;Dimension Set ID    ;Integer       ;TableRelation="Dimension Set Entry";
                                                   CaptionML=[ENU=Dimension Set ID;
                                                              PTG=ID Combina��o Dimens�o];
                                                   Editable=No }
    { 1001;   ;Job Task No.        ;Code20        ;TableRelation="Job Task";
                                                   CaptionML=[ENU=Job Task No.;
                                                              PTG=N� Tarefa Projeto] }
    { 31022890;;VAT Clause Code    ;Code10        ;TableRelation="VAT Clause";
                                                   CaptionML=[ENU=VAT Clause Code;
                                                              PTG=C�d. Norma Isen��o Legal IVA];
                                                   Description=soft }
  }
  KEYS
  {
    {    ;G/L Account No.,Job No.,Tax Area Code,Tax Liable,Tax Group Code,Invoice Rounding,Adjustment,Line No.,Dimension Set ID;
                                                   SumIndexFields=Amount,Amount Incl. VAT;
                                                   Clustered=Yes }
    {    ;Adjustment                               }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    PROCEDURE IncrAmounts@1(PrepmtInvLineBuf@1001 : Record 461);
    BEGIN
      Amount := Amount + PrepmtInvLineBuf.Amount;
      "Amount Incl. VAT" := "Amount Incl. VAT" + PrepmtInvLineBuf."Amount Incl. VAT";
      "VAT Amount" := "VAT Amount" + PrepmtInvLineBuf."VAT Amount";
      "VAT Base Amount" := "VAT Base Amount" + PrepmtInvLineBuf."VAT Base Amount";
      "Amount (ACY)" := "Amount (ACY)" + PrepmtInvLineBuf."Amount (ACY)";
      "VAT Amount (ACY)" := "VAT Amount (ACY)" + PrepmtInvLineBuf."VAT Amount (ACY)";
      "VAT Base Amount (ACY)" := "VAT Base Amount (ACY)" + PrepmtInvLineBuf."VAT Base Amount (ACY)";
      "VAT Difference" := "VAT Difference" + PrepmtInvLineBuf."VAT Difference";
    END;

    PROCEDURE ReverseAmounts@2();
    BEGIN
      Amount := -Amount;
      "Amount Incl. VAT" := -"Amount Incl. VAT";
      "VAT Amount" := -"VAT Amount";
      "VAT Base Amount" := -"VAT Base Amount";
      "Amount (ACY)" := -"Amount (ACY)";
      "VAT Amount (ACY)" := -"VAT Amount (ACY)";
      "VAT Base Amount (ACY)" := -"VAT Base Amount (ACY)";
      "VAT Difference" := -"VAT Difference";
    END;

    PROCEDURE SetAmounts@12(AmountLCY@1000 : Decimal;AmountInclVAT@1001 : Decimal;VATBaseAmount@1002 : Decimal;AmountACY@1003 : Decimal;VATBaseAmountACY@1004 : Decimal;VATDifference@1005 : Decimal);
    BEGIN
      Amount := AmountLCY;
      "Amount Incl. VAT" := AmountInclVAT;
      "VAT Base Amount" := VATBaseAmount;
      "Amount (ACY)" := AmountACY;
      "VAT Base Amount (ACY)" := VATBaseAmountACY;
      "VAT Difference" := VATDifference;
    END;

    PROCEDURE InsertInvLineBuffer@3(PrepmtInvLineBuf2@1000 : Record 461);
    BEGIN
      Rec := PrepmtInvLineBuf2;
      IF FIND THEN BEGIN
        IncrAmounts(PrepmtInvLineBuf2);
        MODIFY;
      END ELSE
        INSERT;
    END;

    PROCEDURE CopyWithLineNo@4(PrepmtInvLineBuf@1001 : Record 461;LineNo@1002 : Integer);
    BEGIN
      Rec := PrepmtInvLineBuf;
      "Line No." := LineNo;
      INSERT;
    END;

    PROCEDURE CopyFromPurchLine@11(PurchLine@1000 : Record 39);
    BEGIN
      "Gen. Prod. Posting Group" := PurchLine."Gen. Prod. Posting Group";
      "VAT Prod. Posting Group" := PurchLine."VAT Prod. Posting Group";
      "Gen. Bus. Posting Group" := PurchLine."Gen. Bus. Posting Group";
      "VAT Bus. Posting Group" := PurchLine."VAT Bus. Posting Group";
      "VAT Calculation Type" := PurchLine."Prepmt. VAT Calc. Type";
      "VAT Identifier" := PurchLine."Prepayment VAT Identifier";
      "VAT %" := PurchLine."VAT %";
      "Global Dimension 1 Code" := PurchLine."Shortcut Dimension 1 Code";
      "Global Dimension 2 Code" := PurchLine."Shortcut Dimension 2 Code";
      "Dimension Set ID" := PurchLine."Dimension Set ID";
      "Job No." := PurchLine."Job No.";
      "Job Task No." := PurchLine."Job Task No.";
      "Tax Area Code" := PurchLine."Tax Area Code";
      "Tax Liable" := PurchLine."Tax Liable";
      "Tax Group Code" := PurchLine."Tax Group Code";
    END;

    PROCEDURE CopyFromSalesLine@13(SalesLine@1000 : Record 37);
    BEGIN
      "Gen. Prod. Posting Group" := SalesLine."Gen. Prod. Posting Group";
      "VAT Prod. Posting Group" := SalesLine."VAT Prod. Posting Group";
      "Gen. Bus. Posting Group" := SalesLine."Gen. Bus. Posting Group";
      "VAT Bus. Posting Group" := SalesLine."VAT Bus. Posting Group";
      "VAT Calculation Type" := SalesLine."Prepmt. VAT Calc. Type";
      "VAT Identifier" := SalesLine."Prepayment VAT Identifier";
      "VAT %" := SalesLine."VAT %";
      "Global Dimension 1 Code" := SalesLine."Shortcut Dimension 1 Code";
      "Global Dimension 2 Code" := SalesLine."Shortcut Dimension 2 Code";
      "Dimension Set ID" := SalesLine."Dimension Set ID";
      "Job No." := SalesLine."Job No.";
      "Job Task No." := SalesLine."Job Task No.";
      "Tax Area Code" := SalesLine."Tax Area Code";
      "Tax Liable" := SalesLine."Tax Liable";
      "Tax Group Code" := SalesLine."Tax Group Code";
      "VAT Clause Code" := SalesLine."VAT Clause Code";  //soft,n
    END;

    PROCEDURE SetFilterOnPKey@5(PrepmtInvLineBuf@1001 : Record 461);
    BEGIN
      RESET;
      SETRANGE("G/L Account No.",PrepmtInvLineBuf."G/L Account No.");
      SETRANGE("Dimension Set ID",PrepmtInvLineBuf."Dimension Set ID");
      SETRANGE("Job No.",PrepmtInvLineBuf."Job No.");
      SETRANGE("Tax Area Code",PrepmtInvLineBuf."Tax Area Code");
      SETRANGE("Tax Liable",PrepmtInvLineBuf."Tax Liable");
      SETRANGE("Tax Group Code",PrepmtInvLineBuf."Tax Group Code");
      SETRANGE("Invoice Rounding",PrepmtInvLineBuf."Invoice Rounding");
      SETRANGE(Adjustment,PrepmtInvLineBuf.Adjustment);
      IF PrepmtInvLineBuf."Line No." <> 0 THEN
        SETRANGE("Line No.",PrepmtInvLineBuf."Line No.");
    END;

    PROCEDURE FillAdjInvLineBuffer@6(PrepmtInvLineBuf@1000 : Record 461;GLAccountNo@1003 : Code[20];CorrAmount@1002 : Decimal;CorrAmountACY@1001 : Decimal);
    BEGIN
      INIT;
      Adjustment := TRUE;
      "G/L Account No." := GLAccountNo;
      Amount := CorrAmount;
      "Amount Incl. VAT" := CorrAmount;
      "Amount (ACY)" := CorrAmountACY;
      "Line No." := PrepmtInvLineBuf."Line No.";
      "Global Dimension 1 Code" := PrepmtInvLineBuf."Global Dimension 1 Code";
      "Global Dimension 2 Code" := PrepmtInvLineBuf."Global Dimension 2 Code";
      "Dimension Set ID" := PrepmtInvLineBuf."Dimension Set ID";
      Description := PrepmtInvLineBuf.Description;
    END;

    PROCEDURE FillFromGLAcc@7(CompressPrepayment@1002 : Boolean);
    VAR
      GLAcc@1001 : Record 15;
    BEGIN
      GLAcc.GET("G/L Account No.");
      "Gen. Prod. Posting Group" := GLAcc."Gen. Prod. Posting Group";
      "VAT Prod. Posting Group" := GLAcc."VAT Prod. Posting Group";
      IF CompressPrepayment THEN
        Description := GLAcc.Name;
    END;

    PROCEDURE AdjustVATBase@8(VATAdjustment@1002 : ARRAY [2] OF Decimal);
    BEGIN
      IF Amount <> "Amount Incl. VAT" THEN BEGIN
        Amount := Amount + VATAdjustment[1];
        "VAT Base Amount" := Amount;
        "VAT Amount" := "VAT Amount" + VATAdjustment[2];
        "Amount Incl. VAT" := Amount + "VAT Amount";
      END;
    END;

    PROCEDURE AmountsToArray@9(VAR VATAmount@1001 : ARRAY [2] OF Decimal);
    BEGIN
      VATAmount[1] := Amount;
      VATAmount[2] := "Amount Incl. VAT" - Amount;
    END;

    PROCEDURE CompressBuffer@10();
    VAR
      TempPrepmtInvLineBuffer2@1000 : TEMPORARY Record 461;
    BEGIN
      FIND('-');
      REPEAT
        TempPrepmtInvLineBuffer2 := Rec;
        TempPrepmtInvLineBuffer2."Line No." := 0;
        IF TempPrepmtInvLineBuffer2.FIND THEN BEGIN
          TempPrepmtInvLineBuffer2.IncrAmounts(Rec);
          TempPrepmtInvLineBuffer2.MODIFY;
        END ELSE
          TempPrepmtInvLineBuffer2.INSERT;
      UNTIL NEXT = 0;

      DELETEALL;

      TempPrepmtInvLineBuffer2.FIND('-');
      REPEAT
        Rec := TempPrepmtInvLineBuffer2;
        INSERT;
      UNTIL TempPrepmtInvLineBuffer2.NEXT = 0;
    END;

    PROCEDURE UpdateVATAmounts@14();
    VAR
      GLSetup@1000 : Record 98;
      Currency@1002 : Record 4;
      VATPostingSetup@1001 : Record 325;
    BEGIN
      GLSetup.GET;
      Currency.Initialize(GLSetup."Additional Reporting Currency");
      VATPostingSetup.GET("VAT Bus. Posting Group","VAT Prod. Posting Group");
      "VAT Amount" := ROUND(Amount * VATPostingSetup."VAT %" / 100);
      "VAT Amount (ACY)" := ROUND("Amount (ACY)" * VATPostingSetup."VAT %" / 100,Currency."Amount Rounding Precision");
    END;

    BEGIN
    {
      V92.00#00002 - Tipo C�lculo IVA corrigir op��o "N�o Sujeito" - EFD - 04.02.2016
      V93.00#00023 - Imposto Selo - SAFT - IRC - 2016.03.31
    }
    END.
  }
}

