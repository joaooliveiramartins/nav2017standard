OBJECT Page 5209 Employee Relatives
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Employee Relatives;
               PTG=Familiares Empregado];
    SourceTable=Table5205;
    DataCaptionFields=Employee No.;
    PageType=List;
    AutoSplitKey=Yes;
    ActionList=ACTIONS
    {
      { 1900000003;0 ;ActionContainer;
                      ActionContainerType=RelatedInformation }
      { 8       ;1   ;ActionGroup;
                      CaptionML=[ENU=&Relative;
                                 PTG=F&amiliar];
                      Image=Relatives }
      { 9       ;2   ;Action    ;
                      CaptionML=[ENU=Co&mments;
                                 PTG=Co&ment rios];
                      RunObject=Page 5222;
                      RunPageLink=Table Name=CONST(Employee Relative),
                                  No.=FIELD(Employee No.),
                                  Table Line No.=FIELD(Line No.);
                      Image=ViewComments }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 2   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies a relative code for the employee.;
                           PTG=""];
                SourceExpr="Relative Code" }

    { 4   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the first name of the employee's relative.;
                           PTG=""];
                SourceExpr="First Name" }

    { 6   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the middle name of the employee's relative.;
                           PTG=""];
                SourceExpr="Middle Name";
                Visible=FALSE }

    { 15  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the relative's date of birth.;
                           PTG=""];
                SourceExpr="Birth Date" }

    { 10  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the relative's telephone number.;
                           PTG=""];
                SourceExpr="Phone No." }

    { 19  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the relative's employee number, if the relative also works at the company.;
                           PTG=""];
                SourceExpr="Relative's Employee No.";
                Visible=FALSE }

    { 17  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies if a comment was entered for this entry.;
                           PTG=""];
                SourceExpr=Comment }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                Visible=TRUE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {

    BEGIN
    END.
  }
}

