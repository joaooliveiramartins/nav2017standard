OBJECT Table 7300 Zone
{
  OBJECT-PROPERTIES
  {
    Date=09/09/14;
    Time=12:00:00;
    Version List=NAVW18.00;
  }
  PROPERTIES
  {
    DataCaptionFields=Location Code,Code,Description;
    OnDelete=VAR
               Bin@1000 : Record 7354;
             BEGIN
               Bin.SETCURRENTKEY("Location Code","Zone Code");
               Bin.SETRANGE("Location Code","Location Code");
               Bin.SETRANGE("Zone Code",Code);
               Bin.DELETEALL(TRUE);
             END;

    CaptionML=[ENU=Zone;
               PTG=Zona];
    LookupPageID=Page7301;
  }
  FIELDS
  {
    { 1   ;   ;Location Code       ;Code10        ;TableRelation=Location;
                                                   CaptionML=[ENU=Location Code;
                                                              PTG=C�d. Localiza��o];
                                                   NotBlank=Yes;
                                                   Editable=No }
    { 2   ;   ;Code                ;Code10        ;CaptionML=[ENU=Code;
                                                              PTG=C�digo];
                                                   NotBlank=Yes }
    { 5   ;   ;Description         ;Text50        ;CaptionML=[ENU=Description;
                                                              PTG=Descri��o] }
    { 10  ;   ;Bin Type Code       ;Code10        ;TableRelation="Bin Type";
                                                   CaptionML=[ENU=Bin Type Code;
                                                              PTG=C�d. Tipo Posi��o] }
    { 11  ;   ;Warehouse Class Code;Code10        ;TableRelation="Warehouse Class";
                                                   CaptionML=[ENU=Warehouse Class Code;
                                                              PTG=C�d. Classe Armaz�m] }
    { 20  ;   ;Special Equipment Code;Code10      ;TableRelation="Special Equipment";
                                                   CaptionML=[ENU=Special Equipment Code;
                                                              PTG=C�d. Equipamento Especial] }
    { 21  ;   ;Zone Ranking        ;Integer       ;CaptionML=[ENU=Zone Ranking;
                                                              PTG=Classifica��o Zona] }
    { 40  ;   ;Cross-Dock Bin Zone ;Boolean       ;CaptionML=[ENU=Cross-Dock Bin Zone;
                                                              PTG=Zona Posi��o Tr�nsito Direto] }
  }
  KEYS
  {
    {    ;Location Code,Code                      ;Clustered=Yes }
    {    ;Code                                     }
    {    ;Description                              }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    END.
  }
}

