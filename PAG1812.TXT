OBJECT Page 1812 Item Approval WF Setup Wizard
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Item Approval Workflow Setup;
               PTG=Configura��o Workflow Aprova��o Produto];
    InsertAllowed=No;
    DeleteAllowed=No;
    LinksAllowed=No;
    SourceTable=Table1804;
    PageType=NavigatePage;
    SourceTableTemporary=Yes;
    ShowFilter=No;
    OnInit=BEGIN
             IF NOT GET THEN BEGIN
               INIT;
               SetDefaultValues;
               INSERT;
             END;
             LoadTopBanners;
           END;

    OnOpenPage=BEGIN
                 ShowIntroStep;
               END;

    OnQueryClosePage=VAR
                       AssistedSetup@1001 : Record 1803;
                     BEGIN
                       IF CloseAction = ACTION::OK THEN
                         IF AssistedSetup.GetStatus(PAGE::"Item Approval WF Setup Wizard") = AssistedSetup.Status::"Not Completed" THEN
                           IF NOT CONFIRM(NAVNotSetUpQst,FALSE) THEN
                             ERROR('');
                     END;

    ActionList=ACTIONS
    {
      { 8       ;    ;ActionContainer;
                      ActionContainerType=ActionItems }
      { 15      ;1   ;Action    ;
                      Name=PreviousPage;
                      CaptionML=[ENU=Back;
                                 PTG=Anterior];
                      ApplicationArea=#Suite;
                      Enabled=BackEnabled;
                      InFooterBar=Yes;
                      Image=PreviousRecord;
                      OnAction=BEGIN
                                 NextStep(TRUE);
                               END;
                                }
      { 14      ;1   ;Action    ;
                      Name=NextPage;
                      CaptionML=[ENU=Next;
                                 PTG=Seguinte];
                      ApplicationArea=#Suite;
                      Enabled=NextEnabled;
                      InFooterBar=Yes;
                      Image=NextRecord;
                      OnAction=BEGIN
                                 NextStep(FALSE);
                               END;
                                }
      { 9       ;1   ;Action    ;
                      Name=Finish;
                      CaptionML=[ENU=Finish;
                                 PTG=Fim];
                      ApplicationArea=#Suite;
                      Enabled=FinishEnabled;
                      InFooterBar=Yes;
                      Image=Approve;
                      OnAction=VAR
                                 AssistedSetup@1004 : Record 1803;
                                 ApprovalWorkflowSetupMgt@1001 : Codeunit 1804;
                               BEGIN
                                 ApprovalWorkflowSetupMgt.ApplyItemWizardUserInput(Rec);
                                 AssistedSetup.SetStatus(PAGE::"Item Approval WF Setup Wizard",AssistedSetup.Status::Completed);

                                 CurrPage.CLOSE;
                               END;
                                }
    }
  }
  CONTROLS
  {
    { 1   ;0   ;Container ;
                ContainerType=ContentArea }

    { 96  ;1   ;Group     ;
                Visible=TopBannerVisible AND NOT DoneVisible;
                Editable=FALSE;
                GroupType=Group }

    { 97  ;2   ;Field     ;
                ApplicationArea=#Suite;
                SourceExpr=MediaRepositoryStandard.Image;
                Editable=FALSE;
                ShowCaption=No }

    { 98  ;1   ;Group     ;
                Visible=TopBannerVisible AND DoneVisible;
                Editable=FALSE;
                GroupType=Group }

    { 99  ;2   ;Field     ;
                ApplicationArea=#Suite;
                SourceExpr=MediaRepositoryDone.Image;
                Editable=FALSE;
                ShowCaption=No }

    { 11  ;1   ;Group     ;
                Name=Step1;
                CaptionML=[ENU="";
                           PTG=""];
                Visible=IntroVisible;
                GroupType=Group }

    { 18  ;2   ;Group     ;
                Name=Para1.1;
                CaptionML=[ENU=Welcome to Item Approval Workflow Setup;
                           PTG=Bem-Vindo � Configura��o de Workflow Aprova��o Produto];
                GroupType=Group }

    { 19  ;3   ;Group     ;
                Name=Para1.1.1;
                CaptionML=[ENU="";
                           PTG=""];
                GroupType=Group;
                InstructionalTextML=[ENU=You can create approval workflows that automatically notify an approver when a user tries to create or change an item card.;
                                     PTG=Pode criar workflows de aprova��o que notificam o aprovador automaticamente, quando um utilizador tenta criar ou alterar uma ficha de produto.] }

    { 21  ;2   ;Group     ;
                Name=Para1.2;
                CaptionML=[ENU=Let's go!;
                           PTG=Come�ar!];
                GroupType=Group;
                InstructionalTextML=[ENU=Choose Next to specify basic approval workflow settings for changing an item card.;
                                     PTG=Escolha Seguinte para especificar defini��es b�sicas de workflow de aprova��o para a altera��o da ficha de produto.] }

    { 67  ;1   ;Group     ;
                Name=Step2;
                CaptionML=[ENU="";
                           PTG=""];
                Visible=ItemApproverSetupVisible;
                GroupType=Group }

    { 5   ;2   ;Group     ;
                Name=Para2.1;
                CaptionML=[ENU="";
                           PTG=""];
                GroupType=Group }

    { 4   ;3   ;Group     ;
                Name=Para2.1.1;
                CaptionML=[ENU="";
                           PTG=""];
                GroupType=Group;
                InstructionalTextML=[ENU=Choose who is authorized to approve or reject new or changed item cards.;
                                     PTG=Escolha quem est� autorizado a aprovar ou rejeitar fichas de produto novas ou alteradas.] }

    { 2   ;4   ;Field     ;
                CaptionML=[ENU=Approver;
                           PTG=Aprovador];
                ApplicationArea=#Suite;
                SourceExpr="Approver ID";
                LookupPageID=Approval User Setup;
                OnValidate=BEGIN
                             CanEnableNext;
                           END;
                            }

    { 66  ;2   ;Group     ;
                Name=Para2.2;
                CaptionML=[ENU="";
                           PTG=""];
                GroupType=Group }

    { 13  ;3   ;Group     ;
                Name=Para2.2.1;
                CaptionML=[ENU="";
                           PTG=""];
                GroupType=Group;
                InstructionalTextML=[ENU=Choose if the approval process starts automatically or if the user must start the process.;
                                     PTG=Escolha se o processo de aprova��o inicia automaticamente ou se o utilizador tem de iniciar o processo.] }

    { 65  ;3   ;Field     ;
                CaptionML=[ENU=The workflow starts when;
                           PTG=O workflow inicia quando];
                OptionCaptionML=[ENU=The user sends an approval requests manually,The user changes a specific field;
                                 PTG=O utilizador envia um pedido de aprova��o manualmente, O utilizador altera um campo espec�fico];
                ApplicationArea=#Suite;
                SourceExpr="App. Trigger";
                OnValidate=BEGIN
                             CanEnableNext;
                           END;
                            }

    { 12  ;1   ;Group     ;
                Name=Step3;
                CaptionML=[ENU="";
                           PTG=""];
                Visible=ItemAutoAppDetailsVisible;
                GroupType=Group }

    { 62  ;2   ;Group     ;
                Name=Para3.1;
                CaptionML=[ENU="";
                           PTG=""];
                GroupType=Group;
                InstructionalTextML=[ENU=Choose criteria for when the approval process starts automatically.;
                                     PTG=Escolha os crit�rios para que o processo de aprova��o inicie automaticamente.] }

    { 61  ;3   ;Group     ;
                Name=Para3.1.1;
                CaptionML=[ENU="";
                           PTG=""];
                GroupType=GridLayout;
                Layout=Rows }

    { 60  ;4   ;Group     ;
                Name=Para3.1.1.1;
                CaptionML=[ENU="";
                           PTG=""];
                GroupType=Group;
                InstructionalTextML=[ENU=The workflow starts when:;
                                     PTG=O workflow inicia quando:] }

    { 59  ;5   ;Field     ;
                Name=ItemFieldCap;
                CaptionML=[ENU=Field;
                           PTG=Campo];
                ApplicationArea=#Suite;
                SourceExpr=ItemFieldCaption;
                OnValidate=VAR
                             FieldRec@1000 : Record 2000000041;
                           BEGIN
                             IF ItemFieldCaption = '' THEN BEGIN
                               SetItemField(0);
                               EXIT;
                             END;

                             IF NOT FindAndFilterToField(FieldRec,ItemFieldCaption) THEN
                               ERROR(FieldNotExistErr,ItemFieldCaption);

                             IF FieldRec.COUNT = 1 THEN BEGIN
                               SetItemField(FieldRec."No.");
                               EXIT;
                             END;

                             IF PAGE.RUNMODAL(PAGE::"Field List",FieldRec) = ACTION::LookupOK THEN
                               SetItemField(FieldRec."No.")
                             ELSE
                               ERROR(FieldNotExistErr,ItemFieldCaption);
                           END;

                OnLookup=VAR
                           FieldRec@1000 : Record 2000000041;
                         BEGIN
                           FindAndFilterToField(FieldRec,Text);
                           FieldRec.SETRANGE("Field Caption");
                           FieldRec.SETRANGE("No.");

                           IF PAGE.RUNMODAL(PAGE::"Field List",FieldRec) = ACTION::LookupOK THEN
                             SetItemField(FieldRec."No.");
                         END;

                ShowCaption=No }

    { 58  ;5   ;Field     ;
                CaptionML=[ENU=is;
                           PTG=�];
                ApplicationArea=#Suite;
                ShowCaption=No }

    { 57  ;5   ;Field     ;
                CaptionML=[ENU=Operator;
                           PTG=Operador];
                ApplicationArea=#Suite;
                SourceExpr="Field Operator";
                ShowCaption=No }

    { 56  ;3   ;Group     ;
                Name=Para3.1.2;
                CaptionML=[ENU=Specify the message to display when the workflow starts.;
                           PTG=Especifica a mensagem a apresentar quando o workflow inicia.];
                GroupType=Group }

    { 54  ;4   ;Field     ;
                CaptionML=[ENU=Message;
                           PTG=Mensagem];
                ApplicationArea=#Suite;
                SourceExpr="Custom Message";
                ShowCaption=No }

    { 10  ;1   ;Group     ;
                Name=Step10;
                CaptionML=[ENU="";
                           PTG=""];
                Visible=DoneVisible;
                GroupType=Group }

    { 7   ;2   ;Group     ;
                Name=Para10.1;
                CaptionML=[ENU="";
                           PTG=""];
                GroupType=Group;
                InstructionalTextML=[ENU=Item Approval Workflow Overview;
                                     PTG=Vis�o Geral Workflow Aprova��o Produto] }

    { 6   ;3   ;Field     ;
                Name=Overview;
                ApplicationArea=#Suite;
                SourceExpr=SummaryText;
                Editable=FALSE;
                MultiLine=Yes;
                Style=StrongAccent;
                StyleExpr=TRUE;
                ShowCaption=No }

    { 25  ;2   ;Group     ;
                Name=Para10.2;
                CaptionML=[ENU=That's it!;
                           PTG=Est� Pronto!];
                GroupType=Group;
                InstructionalTextML=[ENU=Choose Finish to enable the workflow with the specified settings.;
                                     PTG=Escolha Fim para ativar o workflow com as defini��es especificadas.] }

  }
  CODE
  {
    VAR
      MediaRepositoryStandard@1040 : Record 9400;
      MediaRepositoryDone@1041 : Record 9400;
      Step@1015 : 'Intro,Item Approver Setup,Automatic Approval Setup,Done';
      BackEnabled@1014 : Boolean;
      NextEnabled@1013 : Boolean;
      FinishEnabled@1010 : Boolean;
      TopBannerVisible@1042 : Boolean;
      IntroVisible@1001 : Boolean;
      ItemApproverSetupVisible@1011 : Boolean;
      ItemAutoAppDetailsVisible@1027 : Boolean;
      DoneVisible@1004 : Boolean;
      NAVNotSetUpQst@1000 : TextConst 'ENU=Item Approval has not been set up.\\Are you sure that you want to exit?;PTG=Aprova��o de Produto n�o foi configurada.\\Tem a certeza de que quer sair?';
      MandatoryApproverErr@1002 : TextConst '@@@="%1 = User Name";ENU=You must select an approver before continuing.;PTG=Tem de selecionar um aprovador antes de continuar.';
      ItemFieldCaption@1021 : Text[250];
      FieldNotExistErr@1028 : TextConst '@@@="%1 = Field Caption";ENU=Field %1 does not exist.;PTG=Campo %1 n�o existe.';
      SummaryText@1006 : Text;
      ManualTriggerTxt@1008 : TextConst '@@@="%1 = User Name (eg. An approval request will be sent to the user Domain/Username when the user sends the request manually.)";ENU=An approval request will be sent to the user %1 when the user sends the request manually.;PTG=Um pedido de aprova��o vai ser enviado ao utilizador %1 quando o utilizador enviar o pedido manualmente.';
      AutoTriggerTxt@1007 : TextConst '@@@="%1 = User Name, %2 = Field caption, %3 = Of of this 3 values: Increased, Decreased, Changed (eg. An approval request will be sent to the user Domain/Username when the value in the Credit Limit (LCY) field is Increased.)";ENU=An approval request will be sent to the user %1 when the value in the %2 field is %3.;PTG=Um pedido de aprova��o vai ser enviado para o utilizador %1 quando o valor no campo %2 for %3.';

    LOCAL PROCEDURE NextStep@3(Backwards@1000 : Boolean);
    BEGIN
      IF Backwards THEN
        Step := Step - 1
      ELSE BEGIN
        IF ItemApproverSetupVisible THEN
          ValidateApprover;
        IF ItemAutoAppDetailsVisible THEN
          ValidateFieldSelection;
        Step := Step + 1;
      END;

      CASE Step OF
        Step::Intro:
          ShowIntroStep;
        Step::"Item Approver Setup":
          ShowApprovalUserSetupDetailsStep;
        Step::"Automatic Approval Setup":
          IF "App. Trigger" = "App. Trigger"::"The user changes a specific field"
          THEN
            ShowItemApprovalDetailsStep
          ELSE
            NextStep(Backwards);
        Step::Done:
          ShowDoneStep;
      END;
      CurrPage.UPDATE(TRUE);
    END;

    LOCAL PROCEDURE ShowIntroStep@1();
    BEGIN
      ResetWizardControls;
      IntroVisible := TRUE;
      BackEnabled := FALSE;
    END;

    LOCAL PROCEDURE ShowApprovalUserSetupDetailsStep@9();
    BEGIN
      ResetWizardControls;
      ItemApproverSetupVisible := TRUE;
    END;

    LOCAL PROCEDURE ShowItemApprovalDetailsStep@25();
    BEGIN
      ResetWizardControls;
      ItemAutoAppDetailsVisible := TRUE;
      SetItemField(Field);
    END;

    LOCAL PROCEDURE ShowDoneStep@6();
    BEGIN
      ResetWizardControls;
      DoneVisible := TRUE;
      NextEnabled := FALSE;
      FinishEnabled := TRUE;

      IF "App. Trigger" = "App. Trigger"::"The user sends an approval requests manually" THEN
        SummaryText := STRSUBSTNO(ManualTriggerTxt,"Approver ID");
      IF "App. Trigger" = "App. Trigger"::"The user changes a specific field"
      THEN BEGIN
        CALCFIELDS("Field Caption");
        SummaryText := STRSUBSTNO(AutoTriggerTxt,"Approver ID","Field Caption","Field Operator");
      END;

      SummaryText := CONVERTSTR(SummaryText,'\','/');
    END;

    LOCAL PROCEDURE ResetWizardControls@10();
    BEGIN
      // Buttons
      BackEnabled := TRUE;
      NextEnabled := TRUE;
      FinishEnabled := FALSE;

      // Tabs
      IntroVisible := FALSE;
      ItemApproverSetupVisible := FALSE;
      ItemAutoAppDetailsVisible := FALSE;
      DoneVisible := FALSE;
    END;

    LOCAL PROCEDURE SetDefaultValues@8();
    VAR
      Workflow@1000 : Record 1501;
      WorkflowRule@1007 : Record 1524;
      WorkflowStep@1003 : Record 1502;
      WorkflowStepArgument@1006 : Record 1523;
      WorkflowSetup@1002 : Codeunit 1502;
      WorkflowResponseHandling@1005 : Codeunit 1521;
      WorkflowCode@1001 : Code[20];
    BEGIN
      TableNo := DATABASE::Item;
      WorkflowCode := WorkflowSetup.GetWorkflowTemplateCode(WorkflowSetup.ItemUnitPriceChangeApprovalWorkflowCode);
      IF Workflow.GET(WorkflowCode) THEN BEGIN
        WorkflowRule.SETRANGE("Workflow Code",WorkflowCode);
        IF WorkflowRule.FINDFIRST THEN BEGIN
          Field := WorkflowRule."Field No.";
          "Field Operator" := WorkflowRule.Operator;
        END;
        WorkflowStep.SETRANGE("Workflow Code",WorkflowCode);
        WorkflowStep.SETRANGE("Function Name",WorkflowResponseHandling.ShowMessageCode);
        IF WorkflowStep.FINDFIRST THEN BEGIN
          WorkflowStepArgument.GET(WorkflowStep.Argument);
          "Custom Message" := WorkflowStepArgument.Message;
        END;
      END;
    END;

    LOCAL PROCEDURE ValidateApprover@11();
    BEGIN
      IF "Approver ID" = '' THEN
        ERROR(MandatoryApproverErr);
    END;

    LOCAL PROCEDURE ValidateFieldSelection@7();
    BEGIN
    END;

    LOCAL PROCEDURE CanEnableNext@32();
    BEGIN
      NextEnabled := TRUE;
    END;

    LOCAL PROCEDURE SetItemField@54(FieldNo@1000 : Integer);
    BEGIN
      Field := FieldNo;
      CALCFIELDS("Field Caption");
      ItemFieldCaption := "Field Caption";
    END;

    LOCAL PROCEDURE FindAndFilterToField@57(VAR FieldRec@1000 : Record 2000000041;CaptionToFind@1001 : Text) : Boolean;
    BEGIN
      FieldRec.FILTERGROUP(2);
      FieldRec.SETRANGE(TableNo,DATABASE::Item);
      FieldRec.SETFILTER(Type,STRSUBSTNO('%1|%2|%3|%4|%5|%6|%7|%8|%9|%10|%11|%12|%13',
          FieldRec.Type::Boolean,
          FieldRec.Type::Text,
          FieldRec.Type::Code,
          FieldRec.Type::Decimal,
          FieldRec.Type::Integer,
          FieldRec.Type::BigInteger,
          FieldRec.Type::Date,
          FieldRec.Type::Time,
          FieldRec.Type::DateTime,
          FieldRec.Type::DateFormula,
          FieldRec.Type::Option,
          FieldRec.Type::Duration,
          FieldRec.Type::RecordID));
      FieldRec.SETRANGE(Class,FieldRec.Class::Normal);

      IF CaptionToFind = "Field Caption" THEN
        FieldRec.SETRANGE("No.",Field)
      ELSE
        FieldRec.SETFILTER("Field Caption",'%1','@' + CaptionToFind + '*');

      EXIT(FieldRec.FINDFIRST);
    END;

    LOCAL PROCEDURE LoadTopBanners@40();
    BEGIN
      IF MediaRepositoryStandard.GET('AssistedSetup-NoText-400px.png',FORMAT(CURRENTCLIENTTYPE)) AND
         MediaRepositoryDone.GET('AssistedSetupDone-NoText-400px.png',FORMAT(CURRENTCLIENTTYPE))
      THEN
        TopBannerVisible := MediaRepositoryDone.Image.HASVALUE;
    END;

    BEGIN
    END.
  }
}

