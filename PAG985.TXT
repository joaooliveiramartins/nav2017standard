OBJECT Page 985 Document Search
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Document Search;
               PTG=Procura Documento];
    PageType=Card;
    ShowFilter=No;
    ActionList=ACTIONS
    {
      { 1       ;    ;ActionContainer;
                      ActionContainerType=ActionItems }
      { 12      ;1   ;ActionGroup;
                      CaptionML=[ENU=Search;
                                 PTG=Procurar] }
      { 4       ;2   ;Action    ;
                      Name=Search;
                      CaptionML=[ENU=Search;
                                 PTG=Procurar];
                      ToolTipML=[ENU=Search for unposted documents that have the specified numbers or amount.;
                                 PTG=Procurar documentos n�o registados com numero(s) ou montantes especificos.];
                      ApplicationArea=#Basic,#Suite;
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=Navigate;
                      PromotedCategory=Process;
                      OnAction=BEGIN
                                 PaymentRegistrationMgt.FindRecords(TempDocumentSearchResult,DocumentNo,Amount,AmountTolerance);
                                 PAGE.RUN(PAGE::"Document Search Result",TempDocumentSearchResult);
                               END;
                                }
    }
  }
  CONTROLS
  {
    { 3   ;    ;Container ;
                Name=Content1;
                ContainerType=ContentArea }

    { 7   ;1   ;Group     ;
                CaptionML=[ENU=Search Criteria;
                           PTG=Crit�rio Procura];
                GroupType=Group }

    { 5   ;2   ;Field     ;
                Name=DocumentNo;
                CaptionML=[ENU=Document No.;
                           PTG=N� Documento];
                ToolTipML=ENU=Specifies the number of the document that you are searching for.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr=DocumentNo }

    { 6   ;2   ;Field     ;
                Name=Amount;
                CaptionML=[ENU=Amount;
                           PTG=Valor];
                ToolTipML=ENU=Specifies the amounts that you want to search for when you search open documents.;
                ApplicationArea=#Basic,#Suite;
                BlankZero=Yes;
                SourceExpr=Amount;
                OnValidate=BEGIN
                             Warning := PaymentRegistrationMgt.SetToleranceLimits(Amount,AmountTolerance,ToleranceTxt);
                           END;
                            }

    { 2   ;2   ;Field     ;
                Name=AmountTolerance;
                CaptionML=[ENU=Amount Tolerance %;
                           PTG=Valor % Toler�ncia];
                ToolTipML=ENU=Specifies the range of amounts that you want to search within when you search open documents.;
                ApplicationArea=#Basic,#Suite;
                BlankZero=Yes;
                SourceExpr=AmountTolerance;
                MinValue=0;
                MaxValue=100;
                OnValidate=BEGIN
                             Warning := PaymentRegistrationMgt.SetToleranceLimits(Amount,AmountTolerance,ToleranceTxt)
                           END;
                            }

    { 10  ;1   ;Group     ;
                CaptionML=[ENU=Information;
                           PTG=Informa��o];
                GroupType=Group }

    { 9   ;2   ;Group     ;
                GroupType=FixedLayout }

    { 8   ;3   ;Group     ;
                GroupType=Group }

    { 11  ;4   ;Field     ;
                ToolTipML=ENU=Specifies warnings in connection with the search.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr=Warning;
                Editable=FALSE;
                Style=Strong;
                ShowCaption=No }

  }
  CODE
  {
    VAR
      TempDocumentSearchResult@1003 : TEMPORARY Record 983;
      PaymentRegistrationMgt@1002 : Codeunit 980;
      Warning@1001 : Text;
      DocumentNo@1004 : Code[20];
      Amount@1005 : Decimal;
      AmountTolerance@1000 : Decimal;
      ToleranceTxt@1006 : TextConst 'ENU=The program will search for documents with amounts between %1 and %2.;PTG=O programa ir� procurar documentos com valores entre %1 e %2.';

    BEGIN
    END.
  }
}

