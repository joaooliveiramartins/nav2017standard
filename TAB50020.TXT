OBJECT Table 50020 Posted Consolidation Entries
{
  OBJECT-PROPERTIES
  {
    Date=18/09/06;
    Time=12:43:33;
    Modified=Yes;
    Version List=SGG07.00;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Posted Consolidation Entries;
               PTG=Registo movs. concilia��o];
  }
  FIELDS
  {
    { 1   ;   ;Type                ;Option        ;CaptionML=[ENU=Type;
                                                              PTG=Tipo];
                                                   OptionString=Bancos,Contas,Ficheiros,Misto;
                                                   Editable=No }
    { 2   ;   ;Code                ;Code10        ;TableRelation=Conciliation.Code WHERE (Type=FIELD(Type));
                                                   CaptionML=[ENU=Code;
                                                              PTG=C�digo];
                                                   Editable=No }
    { 3   ;   ;Conciliation ID     ;Integer       ;CaptionML=[ENU=Conciliation ID;
                                                              PTG=Id. Concilia��o];
                                                   Editable=No }
    { 4   ;   ;Statement Amount    ;Decimal       ;CaptionML=[ENU=Statement Amount;
                                                              PTG=Valor extrato];
                                                   Editable=No }
    { 5   ;   ;Conciliated Amount  ;Decimal       ;CaptionML=[ENU=Conciliated Amount;
                                                              PTG=Valor conciliado];
                                                   Editable=No }
    { 6   ;   ;Difference          ;Decimal       ;CaptionML=[ENU=Difference;
                                                              PTG=Diferen�a];
                                                   Editable=No }
    { 7   ;   ;Conciliation Type   ;Option        ;CaptionML=[ENU=Conciliation Type;
                                                              PTG=Tipo concilia��o];
                                                   OptionString=,Autom�tico,Manual, ,Condicionada;
                                                   Editable=No }
    { 8   ;   ;Description         ;Text50        ;CaptionML=[ENU=Description;
                                                              PTG=Descri��o] }
    { 9   ;   ;Statement Entry     ;Decimal       ;CaptionML=[ENU=Statement Entry;
                                                              PTG=Mov. extrato];
                                                   Editable=No }
    { 10  ;   ;Conciliated Entry   ;Decimal       ;CaptionML=[ENU=Conciliated Entry;
                                                              PTG=Mov. conciliados];
                                                   Editable=No }
    { 11  ;   ;Posting Date        ;Date          ;CaptionML=[ENU=Posting Date;
                                                              PTG=Data registo];
                                                   Editable=No }
    { 12  ;   ;User ID             ;Code10        ;CaptionML=[ENU=User ID;
                                                              PTG=Id. utilizador];
                                                   Editable=No }
  }
  KEYS
  {
    {    ;Conciliation ID                         ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {
    VAR
      "Concilia��o"@1000000000 : Record 50016;
      Fun@1000000001 : Codeunit 50007;

    BEGIN
    {
      SGG07.00 PFILIPE 28-08-06 Criada nova Tabela
    }
    END.
  }
}

