OBJECT Table 442 IC Document Dimension
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0;
  }
  PROPERTIES
  {
    CaptionML=[ENU=IC Document Dimension;
               PTG=Dimens�o Documento IE];
  }
  FIELDS
  {
    { 1   ;   ;Table ID            ;Integer       ;TableRelation=AllObjWithCaption."Object ID" WHERE (Object Type=CONST(Table));
                                                   CaptionML=[ENU=Table ID;
                                                              PTG=ID Tabela];
                                                   NotBlank=Yes }
    { 2   ;   ;Transaction No.     ;Integer       ;CaptionML=[ENU=Transaction No.;
                                                              PTG=N� Transa��o] }
    { 3   ;   ;IC Partner Code     ;Code20        ;TableRelation="IC Partner";
                                                   CaptionML=[ENU=IC Partner Code;
                                                              PTG=C�d. Parceiro IE] }
    { 4   ;   ;Transaction Source  ;Option        ;CaptionML=[ENU=Transaction Source;
                                                              PTG=Origem Transa��o];
                                                   OptionCaptionML=[ENU=Rejected by Current Company,Created by Current Company;
                                                                    PTG=Rejeitado por Empresa Atual,Criado por Empresa Atual];
                                                   OptionString=Rejected by Current Company,Created by Current Company }
    { 5   ;   ;Line No.            ;Integer       ;CaptionML=[ENU=Line No.;
                                                              PTG=N� Linha] }
    { 6   ;   ;Dimension Code      ;Code20        ;TableRelation="IC Dimension";
                                                   OnValidate=BEGIN
                                                                IF NOT DimMgt.CheckICDim("Dimension Code") THEN
                                                                  ERROR(DimMgt.GetDimErr);
                                                                "Dimension Value Code" := '';
                                                              END;

                                                   CaptionML=[ENU=Dimension Code;
                                                              PTG=C�digo Dimens�o];
                                                   NotBlank=Yes }
    { 7   ;   ;Dimension Value Code;Code20        ;TableRelation="IC Dimension Value".Code WHERE (Dimension Code=FIELD(Dimension Code));
                                                   OnValidate=BEGIN
                                                                IF NOT DimMgt.CheckICDimValue("Dimension Code","Dimension Value Code") THEN
                                                                  ERROR(DimMgt.GetDimErr);
                                                              END;

                                                   CaptionML=[ENU=Dimension Value Code;
                                                              PTG=C�d. Valor Dimens�o];
                                                   NotBlank=Yes }
  }
  KEYS
  {
    {    ;Table ID,Transaction No.,IC Partner Code,Transaction Source,Line No.,Dimension Code;
                                                   Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {
    VAR
      DimMgt@1000 : Codeunit 408;

    BEGIN
    END.
  }
}

