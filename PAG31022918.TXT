OBJECT Page 31022918 IES Annexes
{
  OBJECT-PROPERTIES
  {
    Date=01/02/15;
    Time=13:00:00;
    Version List=NAVPTSS81.00;
  }
  PROPERTIES
  {
    CaptionML=[ENU=IES Annexes;
               PTG=Anexos IES];
    SourceTable=Table31022916;
    DataCaptionFields=IES Statement Name;
    PageType=List;
    ActionList=ACTIONS
    {
      { 1900000003;0 ;ActionContainer;
                      ActionContainerType=RelatedInformation }
      { 1110008 ;1   ;ActionGroup;
                      Name=&Annex;
                      CaptionML=[ENU=&Annex;
                                 PTG=&Anexo] }
      { 1110009 ;2   ;Action    ;
                      CaptionML=[ENU=Frames;
                                 PTG=Quadros];
                      RunObject=Page 31022919;
                      RunPageLink=IES Statement Name=FIELD(IES Statement Name),
                                  IES Annex Name=FIELD(Name) }
      { 1110010 ;2   ;Action    ;
                      CaptionML=[ENU=Structure Lines;
                                 PTG=Estrutura Linhas];
                      RunObject=Page 31022921;
                      RunPageLink=IES Statement Name=FIELD(IES Statement Name),
                                  IES Annex Name=FIELD(Name) }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1110000;1;Group     ;
                GroupType=Repeater }

    { 1110001;2;Field     ;
                SourceExpr=Name }

    { 1110003;2;Field     ;
                SourceExpr=Description }

    { 1110011;2;Field     ;
                SourceExpr="Do Not Export" }

  }
  CODE
  {

    BEGIN
    END.
  }
}

