OBJECT Page 9010 Production Planner Role Center
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Role Center;
               PTG=Centro Perfil];
    PageType=RoleCenter;
    ActionList=ACTIONS
    {
      { 1900000006;0 ;ActionContainer;
                      ActionContainerType=Reports }
      { 107     ;1   ;Action    ;
                      CaptionML=[ENU=Ro&uting Sheet;
                                 PTG=Fol&ha Gama Operat�ria];
                      RunObject=Report 99000787;
                      Image=Report }
      { 109     ;1   ;Separator  }
      { 108     ;1   ;Action    ;
                      CaptionML=[ENU=Inventory - &Availability Plan;
                                 PTG=Invent�rio - Pl&ano Disponibilidade];
                      RunObject=Report 707;
                      Image=ItemAvailability }
      { 110     ;1   ;Action    ;
                      CaptionML=[ENU=&Planning Availability;
                                 PTG=&Planeamento Disponibilidade];
                      RunObject=Report 99001048;
                      Image=Report }
      { 13      ;1   ;Action    ;
                      CaptionML=[ENU=&Capacity Task List;
                                 PTG=Lista Tarefas &Capacidade];
                      RunObject=Report 99000780;
                      Image=Report }
      { 18      ;1   ;Action    ;
                      CaptionML=[ENU=Subcontractor - &Dispatch List;
                                 PTG=Subcontratante - Lista Priori&dades];
                      RunObject=Report 99000789;
                      Image=Report }
      { 111     ;1   ;Separator  }
      { 112     ;1   ;Action    ;
                      CaptionML=[ENU=Production Order - &Shortage List;
                                 PTG=Ordem Produ��o - Li&sta Car�ncias];
                      RunObject=Report 99000788;
                      Image=Report }
      { 49      ;1   ;Action    ;
                      CaptionML=[ENU=D&etailed Calculation;
                                 PTG=C�lculo D&etalhado];
                      RunObject=Report 99000756;
                      Image=Report }
      { 113     ;1   ;Separator  }
      { 114     ;1   ;Action    ;
                      CaptionML=[ENU=P&roduction Order - Calculation;
                                 PTG=O&rdem Produ��o - C�lculo];
                      RunObject=Report 99000767;
                      Image=Report }
      { 115     ;1   ;Action    ;
                      CaptionML=[ENU=Sta&tus;
                                 PTG=Es&tado];
                      RunObject=Report 706;
                      Image=Report }
      { 116     ;1   ;Action    ;
                      CaptionML=[ENU=Inventory &Valuation WIP;
                                 PTG="&Valoriza��o Invent�rio Trab. Curso "];
                      RunObject=Report 5802;
                      Image=Report }
      { 1900000011;0 ;ActionContainer;
                      ActionContainerType=HomeItems }
      { 5       ;1   ;Action    ;
                      CaptionML=[ENU=Simulated Production Orders;
                                 PTG=Ordens Produ��o Simuladas];
                      RunObject=Page 9323 }
      { 7       ;1   ;Action    ;
                      CaptionML=[ENU=Planned Production Orders;
                                 PTG=Ordens Produ��o Planeadas];
                      RunObject=Page 9324 }
      { 9       ;1   ;Action    ;
                      CaptionML=[ENU=Firm Planned Production Orders;
                                 PTG=Ordens Produ��o Planeadas Firmes];
                      RunObject=Page 9325 }
      { 12      ;1   ;Action    ;
                      CaptionML=[ENU=Released Production Orders;
                                 PTG=Ordens Produ��o Lan�adas];
                      RunObject=Page 9326 }
      { 10      ;1   ;Action    ;
                      CaptionML=[ENU=Finished Production Orders;
                                 PTG=Ordens Produ��o Terminadas];
                      RunObject=Page 9327 }
      { 101     ;1   ;Action    ;
                      CaptionML=[ENU=Production Forecast;
                                 PTG=Previs�o Produ��o];
                      RunObject=Page 99000921 }
      { 102     ;1   ;Action    ;
                      CaptionML=[ENU=Blanket Sales Orders;
                                 PTG=Encomendas Venda Abertas];
                      RunObject=Page 9303 }
      { 14      ;1   ;Action    ;
                      CaptionML=[ENU=Sales Orders;
                                 PTG=Encomendas Venda];
                      RunObject=Page 9305;
                      Image=Order }
      { 103     ;1   ;Action    ;
                      CaptionML=[ENU=Blanket Purchase Orders;
                                 PTG=Encomendas Compra Abertas];
                      RunObject=Page 9310 }
      { 6       ;1   ;Action    ;
                      CaptionML=[ENU=Purchase Orders;
                                 PTG=Encomendas Compra];
                      RunObject=Page 9307 }
      { 106     ;1   ;Action    ;
                      CaptionML=[ENU=Transfer Orders;
                                 PTG=Ordens Transfer�ncia];
                      RunObject=Page 5742;
                      Image=Document }
      { 3       ;1   ;Action    ;
                      CaptionML=[ENU=Vendors;
                                 PTG=Fornecedores];
                      RunObject=Page 27;
                      Image=Vendor }
      { 17      ;1   ;Action    ;
                      Name=Items;
                      CaptionML=[ENU=Items;
                                 PTG=Produtos];
                      RunObject=Page 31;
                      Image=Item }
      { 11      ;1   ;Action    ;
                      Name=ItemsProduced;
                      CaptionML=[ENU=Produced;
                                 PTG=Produzido];
                      RunObject=Page 31;
                      RunPageView=WHERE(Replenishment System=CONST(Prod. Order)) }
      { 94      ;1   ;Action    ;
                      Name=ItemsRawMaterials;
                      CaptionML=[ENU=Raw Materials;
                                 PTG=Mat�rias-primas];
                      RunObject=Page 31;
                      RunPageView=WHERE(Low-Level Code=FILTER(>0),
                                        Replenishment System=CONST(Purchase),
                                        Production BOM No.=FILTER(='')) }
      { 95      ;1   ;Action    ;
                      CaptionML=[ENU=Stockkeeping Units;
                                 PTG=Unidades Armazenamento];
                      RunObject=Page 5701;
                      Image=SKU }
      { 16      ;1   ;Action    ;
                      Name=ProductionBOMs;
                      CaptionML=[ENU=Production BOMs;
                                 PTG=L.M. Produ��o];
                      RunObject=Page 99000787 }
      { 63      ;1   ;Action    ;
                      Name=ProductionBOMsCertified;
                      CaptionML=[ENU=Certified;
                                 PTG=Certificado];
                      RunObject=Page 99000787;
                      RunPageView=WHERE(Status=CONST(Certified)) }
      { 83      ;1   ;Action    ;
                      CaptionML=[ENU=Routings;
                                 PTG=Gamas Operat�rias];
                      RunObject=Page 99000764 }
      { 1900000012;0 ;ActionContainer;
                      ActionContainerType=ActivityButtons }
      { 19      ;1   ;ActionGroup;
                      CaptionML=[ENU=Journals;
                                 PTG=Di�rios];
                      Image=Journals }
      { 22      ;2   ;Action    ;
                      Name=ItemJournals;
                      CaptionML=[ENU=Item Journals;
                                 PTG=Di�rios Produto];
                      RunObject=Page 262;
                      RunPageView=WHERE(Template Type=CONST(Item),
                                        Recurring=CONST(No)) }
      { 23      ;2   ;Action    ;
                      Name=ItemReclassificationJournals;
                      CaptionML=[ENU=Item Reclassification Journals;
                                 PTG=Di�rios Reclassifica��o Produto];
                      RunObject=Page 262;
                      RunPageView=WHERE(Template Type=CONST(Transfer),
                                        Recurring=CONST(No)) }
      { 24      ;2   ;Action    ;
                      Name=RevaluationJournals;
                      CaptionML=[ENU=Revaluation Journals;
                                 PTG=Di�rios Revaloriza��o];
                      RunObject=Page 262;
                      RunPageView=WHERE(Template Type=CONST(Revaluation),
                                        Recurring=CONST(No)) }
      { 20      ;1   ;ActionGroup;
                      CaptionML=[ENU=Worksheets;
                                 PTG=Folhas];
                      Image=Worksheets }
      { 33      ;2   ;Action    ;
                      Name=PlanningWorksheets;
                      CaptionML=[ENU=Planning Worksheets;
                                 PTG=Folhas Planeamento];
                      RunObject=Page 295;
                      RunPageView=WHERE(Template Type=CONST(Planning),
                                        Recurring=CONST(No)) }
      { 32      ;2   ;Action    ;
                      Name=RequisitionWorksheets;
                      CaptionML=[ENU=Requisition Worksheets;
                                 PTG=Folhas Requisi��es];
                      RunObject=Page 295;
                      RunPageView=WHERE(Template Type=CONST(Req.),
                                        Recurring=CONST(No)) }
      { 34      ;2   ;Action    ;
                      Name=SubcontractingWorksheets;
                      CaptionML=[ENU=Subcontracting Worksheets;
                                 PTG=Folhas Subcontrata��o];
                      RunObject=Page 295;
                      RunPageView=WHERE(Template Type=CONST(For. Labor),
                                        Recurring=CONST(No)) }
      { 35      ;2   ;Action    ;
                      CaptionML=[ENU=Standard Cost Worksheet;
                                 PTG=Folha Custo Padr�o];
                      RunObject=Page 5840 }
      { 2       ;1   ;ActionGroup;
                      CaptionML=[ENU=Product Design;
                                 PTG=Desenho Produto];
                      Image=ProductDesign }
      { 15      ;2   ;Action    ;
                      Name=ProductionBOM;
                      CaptionML=[ENU=Production BOM;
                                 PTG=L.M. Produ��o];
                      RunObject=Page 99000787;
                      Image=BOM }
      { 25      ;2   ;Action    ;
                      Name=ProductionBOMCertified;
                      CaptionML=[ENU=Certified;
                                 PTG=Certificado];
                      RunObject=Page 99000787;
                      RunPageView=WHERE(Status=CONST(Certified)) }
      { 26      ;2   ;Action    ;
                      CaptionML=[ENU=Routings;
                                 PTG=Gamas Operat�rias];
                      RunObject=Page 99000764 }
      { 27      ;2   ;Action    ;
                      CaptionML=[ENU=Routing Links;
                                 PTG=Liga��es Gamas Operat�ria];
                      RunObject=Page 99000798 }
      { 28      ;2   ;Action    ;
                      CaptionML=[ENU=Standard Tasks;
                                 PTG=Tarefas Padr�o];
                      RunObject=Page 99000799 }
      { 29      ;2   ;Action    ;
                      CaptionML=[ENU=Families;
                                 PTG=Fam�lias];
                      RunObject=Page 99000791 }
      { 30      ;2   ;Action    ;
                      Name=ProdDesign_Items;
                      CaptionML=[ENU=Items;
                                 PTG=Produtos];
                      RunObject=Page 31;
                      Image=Item }
      { 31      ;2   ;Action    ;
                      Name=ProdDesign_ItemsProduced;
                      CaptionML=[ENU=Produced;
                                 PTG=Produzido];
                      RunObject=Page 31;
                      RunPageView=WHERE(Replenishment System=CONST(Prod. Order)) }
      { 36      ;2   ;Action    ;
                      Name=ProdDesign_ItemsRawMaterials;
                      CaptionML=[ENU=Raw Materials;
                                 PTG=Mat�rias-primas];
                      RunObject=Page 31;
                      RunPageView=WHERE(Low-Level Code=FILTER(>0),
                                        Replenishment System=CONST(Purchase)) }
      { 37      ;2   ;Action    ;
                      CaptionML=[ENU=Stockkeeping Units;
                                 PTG=Unidades Armazenamento];
                      RunObject=Page 5701;
                      Image=SKU }
      { 38      ;1   ;ActionGroup;
                      CaptionML=[ENU=Capacities;
                                 PTG=Capacidades];
                      Image=Capacities }
      { 39      ;2   ;Action    ;
                      Name=WorkCenters;
                      CaptionML=[ENU=Work Centers;
                                 PTG=Centros Trabalho];
                      RunObject=Page 99000755 }
      { 40      ;2   ;Action    ;
                      Name=WorkCentersInternal;
                      CaptionML=[ENU=Internal;
                                 PTG=Interno];
                      RunObject=Page 99000755;
                      RunPageView=WHERE(Subcontractor No.=FILTER(=''));
                      Image=Comment }
      { 41      ;2   ;Action    ;
                      Name=WorkCentersSubcontracted;
                      CaptionML=[ENU=Subcontracted;
                                 PTG=Subcontrata��o];
                      RunObject=Page 99000755;
                      RunPageView=WHERE(Subcontractor No.=FILTER(<>'')) }
      { 42      ;2   ;Action    ;
                      CaptionML=[ENU=Machine Centers;
                                 PTG=Centros M�quina];
                      RunObject=Page 99000761 }
      { 43      ;2   ;Action    ;
                      CaptionML=[ENU=Registered Absence;
                                 PTG=Aus�ncia Registada];
                      RunObject=Page 99000920 }
      { 44      ;2   ;Action    ;
                      CaptionML=[ENU=Vendors;
                                 PTG=Fornecedores];
                      RunObject=Page 27;
                      Image=Vendor }
      { 1       ;0   ;ActionContainer;
                      ActionContainerType=NewDocumentItems }
      { 53      ;1   ;Action    ;
                      CaptionML=[ENU=&Item;
                                 PTG=&Produto];
                      RunObject=Page 30;
                      Promoted=No;
                      Image=Item;
                      PromotedCategory=Process;
                      RunPageMode=Create }
      { 52      ;1   ;Action    ;
                      CaptionML=[ENU=Production &Order;
                                 PTG=&Ordem Produ��o];
                      RunObject=Page 99000813;
                      Promoted=No;
                      Image=Order;
                      PromotedCategory=Process;
                      RunPageMode=Create }
      { 51      ;1   ;Action    ;
                      CaptionML=[ENU=Production &BOM;
                                 PTG=&L.M. Produ��o];
                      RunObject=Page 99000786;
                      Promoted=No;
                      Image=BOM;
                      PromotedCategory=Process;
                      RunPageMode=Create }
      { 50      ;1   ;Action    ;
                      CaptionML=[ENU=&Routing;
                                 PTG=Gama Operat�r&ia];
                      RunObject=Page 99000766;
                      Promoted=No;
                      Image=Route;
                      PromotedCategory=Process;
                      RunPageMode=Create }
      { 21      ;1   ;Action    ;
                      CaptionML=[ENU=&Purchase Order;
                                 PTG=Encomenda Com&pra];
                      RunObject=Page 50;
                      Promoted=No;
                      Image=Document;
                      PromotedCategory=Process;
                      RunPageMode=Create }
      { 1900000004;0 ;ActionContainer;
                      ActionContainerType=ActionItems }
      { 67      ;1   ;Separator ;
                      CaptionML=[ENU=Tasks;
                                 PTG=Tarefas];
                      IsHeader=Yes }
      { 46      ;1   ;Action    ;
                      CaptionML=[ENU=Item &Journal;
                                 PTG=Di�rio Prod&uto];
                      RunObject=Page 40;
                      Image=Journals }
      { 47      ;1   ;Action    ;
                      CaptionML=[ENU=Re&quisition Worksheet;
                                 PTG=Folha Re&quisi��o];
                      RunObject=Page 291;
                      Image=Worksheet }
      { 48      ;1   ;Action    ;
                      CaptionML=[ENU=Planning Works&heet;
                                 PTG=Fol&ha Planeamento];
                      RunObject=Page 99000852;
                      Image=PlanningWorksheet }
      { 4       ;1   ;Action    ;
                      CaptionML=[ENU=Item Availability by Timeline;
                                 PTG=Disp. Produto por Calend�rio];
                      RunObject=Page 5540;
                      Image=Timeline }
      { 8       ;1   ;Action    ;
                      CaptionML=[ENU=Subcontracting &Worksheet;
                                 PTG=Folha &Subcontrata��o];
                      RunObject=Page 99000886;
                      Image=SubcontractingWorksheet }
      { 45      ;1   ;Separator  }
      { 122     ;1   ;Action    ;
                      CaptionML=[ENU=Change Pro&duction Order Status;
                                 PTG=&Alterar Estado Ordem Produ��o];
                      RunObject=Page 99000914;
                      Image=ChangeStatus }
      { 123     ;1   ;Action    ;
                      CaptionML=[ENU=Order Pla&nning;
                                 PTG=Planeamento En&comenda];
                      RunObject=Page 5522;
                      Image=Planning }
      { 84      ;1   ;Separator ;
                      CaptionML=[ENU=Administration;
                                 PTG=Administra��o];
                      IsHeader=Yes }
      { 124     ;1   ;Action    ;
                      CaptionML=[ENU=Order Promising S&etup;
                                 PTG=Configura��o Compromisso &Enc.];
                      RunObject=Page 99000958;
                      Image=OrderPromisingSetup }
      { 125     ;1   ;Action    ;
                      CaptionML=[ENU=&Manufacturing Setup;
                                 PTG=Con&figura��o Produ��o];
                      RunObject=Page 99000768;
                      Image=ProductionSetup }
      { 89      ;1   ;Separator ;
                      CaptionML=[ENU=History;
                                 PTG=Hist�rico];
                      IsHeader=Yes }
      { 126     ;1   ;Action    ;
                      CaptionML=[ENU=Item &Tracing;
                                 PTG=Con&trolo Produto];
                      RunObject=Page 6520;
                      Image=ItemTracing }
      { 90      ;1   ;Action    ;
                      CaptionML=[ENU=Navi&gate;
                                 PTG=Na&vegar];
                      RunObject=Page 344;
                      Image=Navigate }
    }
  }
  CONTROLS
  {
    { 1900000008;0;Container;
                ContainerType=RoleCenterArea }

    { 1900724808;1;Group   }

    { 1905113808;2;Part   ;
                PagePartID=Page9038;
                PartType=Page }

    { 1900724708;1;Group   }

    { 54  ;2   ;Part      ;
                PagePartID=Page675;
                Visible=false;
                PartType=Page }

    { 1905989608;2;Part   ;
                PagePartID=Page9152;
                PartType=Page }

    { 55  ;2   ;Part      ;
                PagePartID=Page681;
                PartType=Page }

    { 1903012608;2;Part   ;
                PagePartID=Page9175;
                PartType=Page }

    { 1901377608;2;Part   ;
                PartType=System;
                SystemPartID=MyNotes }

  }
  CODE
  {

    BEGIN
    END.
  }
}

