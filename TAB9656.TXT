OBJECT Table 9656 Report Layout Update Log
{
  OBJECT-PROPERTIES
  {
    Date=15/09/15;
    Time=12:00:00;
    Version List=NAVW19.00;
  }
  PROPERTIES
  {
    DataPerCompany=No;
    CaptionML=[ENU=Report Layout Update Log;
               PTG=Registo Atualiza��o Layout Mapa];
  }
  FIELDS
  {
    { 1   ;   ;No.                 ;Integer       ;AutoIncrement=Yes;
                                                   CaptionML=[ENU=No.;
                                                              PTG=N�] }
    { 2   ;   ;Status              ;Option        ;CaptionML=[ENU=Status;
                                                              PTG=Estado];
                                                   OptionCaptionML=[ENU=None,NoUpgradeApplied,UpgradeSuccess,UpgradeIgnoreSuccess,UpgradeWarnings,UpgradeErrors;
                                                                    PTG=Nenhum,SemUpgradeAplicado,SucessoUpgrade,UpgradeIgnoraSucesso,AvisosUpgrade,ErrosUpgrade];
                                                   OptionString=None,NoUpgradeApplied,UpgradeSuccess,UpgradeIgnoreSuccess,UpgradeWarnings,UpgradeErrors }
    { 3   ;   ;Field Name          ;Text80        ;CaptionML=[ENU=Field Name;
                                                              PTG=Nome Campo] }
    { 4   ;   ;Message             ;Text250       ;CaptionML=[ENU=Message;
                                                              PTG=Mensagem] }
    { 5   ;   ;Report ID           ;Integer       ;CaptionML=[ENU=Report ID;
                                                              PTG=ID Mapa] }
    { 6   ;   ;Layout Description  ;Text80        ;CaptionML=[ENU=Layout Description;
                                                              PTG=Descri��o Layout] }
    { 7   ;   ;Layout Type         ;Option        ;CaptionML=[ENU=Layout Type;
                                                              PTG=Tipo Layout];
                                                   OptionCaptionML=[ENU=RDLC,Word;
                                                                    PTG=RDLC,Word];
                                                   OptionString=RDLC,Word }
  }
  KEYS
  {
    {    ;No.                                     ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    END.
  }
}

