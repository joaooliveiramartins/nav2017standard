OBJECT Page 5976 Posted Service Shpt. Subform
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0;
  }
  PROPERTIES
  {
    Editable=No;
    CaptionML=[ENU=Lines;
               PTG=Linhas];
    InsertAllowed=No;
    DeleteAllowed=No;
    ModifyAllowed=No;
    LinksAllowed=No;
    SourceTable=Table5989;
    DelayedInsert=Yes;
    PageType=ListPart;
    AutoSplitKey=Yes;
    ActionList=ACTIONS
    {
      { 1900000004;0 ;ActionContainer;
                      ActionContainerType=ActionItems }
      { 1907935204;1 ;ActionGroup;
                      CaptionML=[ENU=&Line;
                                 PTG=&Linha];
                      Image=Line }
      { 1901741704;2 ;Action    ;
                      AccessByPermission=TableData 348=R;
                      ShortCutKey=Shift+Ctrl+D;
                      CaptionML=[ENU=Dimensions;
                                 PTG=Dimens�es];
                      ToolTipML=[ENU=View or edit dimensions, such as area, project, or department, that you can assign to sales and purchase documents to distribute costs and analyze transaction history.;
                                 PTG=""];
                      Image=Dimensions;
                      OnAction=BEGIN
                                 ShowDimensions;
                               END;
                                }
      { 1900206304;2 ;ActionGroup;
                      CaptionML=[ENU=Co&mments;
                                 PTG=Co&ment�rios];
                      Image=ViewComments }
      { 1902425004;3 ;Action    ;
                      CaptionML=[ENU=Faults;
                                 PTG=Falhas];
                      Image=Error;
                      OnAction=BEGIN
                                 ShowComments(1);
                               END;
                                }
      { 1906760504;3 ;Action    ;
                      CaptionML=[ENU=Resolutions;
                                 PTG=Resolu��es];
                      Image=Completed;
                      OnAction=BEGIN
                                 ShowComments(2);
                               END;
                                }
      { 1902366404;3 ;Action    ;
                      CaptionML=[ENU=Internal;
                                 PTG=Interno];
                      Image=Comment;
                      OnAction=BEGIN
                                 ShowComments(4);
                               END;
                                }
      { 1901972304;3 ;Action    ;
                      CaptionML=[ENU=Accessories;
                                 PTG=Acess�rios];
                      Image=ServiceAccessories;
                      OnAction=BEGIN
                                 ShowComments(3);
                               END;
                                }
      { 1906307804;3 ;Action    ;
                      CaptionML=[ENU=Lent Loaners;
                                 PTG=Empr�stimos Efetuados];
                      OnAction=BEGIN
                                 ShowComments(5);
                               END;
                                }
      { 1903841704;2 ;Action    ;
                      CaptionML=[ENU=Service Item &Log;
                                 PTG=A&ltera��es Produto Servi�o];
                      Image=Log;
                      OnAction=BEGIN
                                 ShowServItemEventLog;
                               END;
                                }
      { 1906587504;1 ;ActionGroup;
                      CaptionML=[ENU=F&unctions;
                                 PTG=F&un��es];
                      Image=Action }
      { 1903098504;2 ;Action    ;
                      CaptionML=[ENU=&Receive Loaner;
                                 PTG=&Receber Empr�stimo];
                      Image=ReceiveLoaner;
                      OnAction=BEGIN
                                 ReceiveLoaner;
                               END;
                                }
      { 1901820904;1 ;ActionGroup;
                      CaptionML=[ENU=&Shipment;
                                 PTG=&Envio];
                      Image=Shipment }
      { 1902395304;2 ;Action    ;
                      Name=ServiceShipmentLines;
                      ShortCutKey=Shift+Ctrl+I;
                      CaptionML=[ENU=Service Shipment Lines;
                                 PTG=Linhas Guia Remessa Servi�o];
                      OnAction=BEGIN
                                 ShowServShipmentLines;
                               END;
                                }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 4   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the number of this line.;
                           PTG=""];
                SourceExpr="Line No.";
                Visible=FALSE }

    { 6   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the number of the service item registered in the Service Item table and associated with the customer.;
                           PTG=""];
                SourceExpr="Service Item No." }

    { 36  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the code for the group associated with this service item.;
                           PTG=""];
                SourceExpr="Service Item Group Code" }

    { 74  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the ship-to code of the service item associated with the service contract or service order.;
                           PTG=""];
                SourceExpr="Ship-to Code";
                Visible=FALSE }

    { 8   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the number of the item to which this posted service item is related.;
                           PTG=""];
                SourceExpr="Item No." }

    { 44  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the code for the item variant on this line.;
                           PTG=""];
                SourceExpr="Variant Code";
                Visible=FALSE }

    { 10  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the serial number of this service item.;
                           PTG=""];
                SourceExpr="Serial No." }

    { 12  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies a description of the service item specified in the Service Item No. field on this line.;
                           PTG=""];
                SourceExpr=Description }

    { 48  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies an additional description of this service item.;
                           PTG=""];
                SourceExpr="Description 2";
                Visible=FALSE }

    { 54  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies that there is a fault comment for this service item.;
                           PTG=""];
                SourceExpr="Fault Comment";
                Visible=FALSE;
                OnDrillDown=BEGIN
                              ShowComments(1);
                            END;
                             }

    { 52  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies that there is a resolution comment for this service item.;
                           PTG=""];
                SourceExpr="Resolution Comment";
                Visible=FALSE;
                OnDrillDown=BEGIN
                              ShowComments(2);
                            END;
                             }

    { 14  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the number of the service shelf where the service item is stored while it is in the repair shop.;
                           PTG=""];
                SourceExpr="Service Shelf No.";
                Visible=FALSE }

    { 16  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies that there is a warranty on either parts or labor for this service item.;
                           PTG=""];
                SourceExpr=Warranty }

    { 18  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the date when the warranty starts on the service item spare parts.;
                           PTG=""];
                SourceExpr="Warranty Starting Date (Parts)";
                Visible=FALSE }

    { 20  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the date when the spare parts warranty expires for this service item.;
                           PTG=""];
                SourceExpr="Warranty Ending Date (Parts)";
                Visible=FALSE }

    { 22  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the percentage of spare parts costs covered by the warranty for this service item.;
                           PTG=""];
                SourceExpr="Warranty % (Parts)";
                Visible=FALSE }

    { 24  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the percentage of labor costs covered by the warranty on this service item.;
                           PTG=""];
                SourceExpr="Warranty % (Labor)";
                Visible=FALSE }

    { 66  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the date when the labor warranty for the posted service item starts.;
                           PTG=""];
                SourceExpr="Warranty Starting Date (Labor)";
                Visible=FALSE }

    { 68  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the date when the labor warranty expires on the posted service item.;
                           PTG=""];
                SourceExpr="Warranty Ending Date (Labor)";
                Visible=FALSE }

    { 26  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the number of the contract associated with the posted service item.;
                           PTG=""];
                SourceExpr="Contract No." }

    { 28  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the fault reason code assigned to the posted service item.;
                           PTG=""];
                SourceExpr="Fault Reason Code";
                Visible=FALSE }

    { 40  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the code of the service price group associated with this service item.;
                           PTG=""];
                SourceExpr="Service Price Group Code";
                Visible=FALSE }

    { 42  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the code that identifies the area of the fault encountered with this service item.;
                           PTG=""];
                SourceExpr="Fault Area Code";
                Visible=FALSE }

    { 34  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the code to identify the symptom of the service item fault.;
                           PTG=""];
                SourceExpr="Symptom Code";
                Visible=FALSE }

    { 46  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the code to identify the fault of the posted service item or the actions taken on the item.;
                           PTG=""];
                SourceExpr="Fault Code";
                Visible=FALSE }

    { 2   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the resolution code assigned to this item.;
                           PTG=""];
                SourceExpr="Resolution Code";
                Visible=FALSE }

    { 38  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the service priority for this posted service item.;
                           PTG=""];
                SourceExpr=Priority }

    { 64  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the estimated hours between the creation of the service order, to the time when the repair status changes from Initial, to In Process.;
                           PTG=""];
                SourceExpr="Response Time (Hours)" }

    { 30  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the estimated date when service starts on this service item.;
                           PTG=""];
                SourceExpr="Response Date" }

    { 32  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the time when service is expected to start on this service item.;
                           PTG=""];
                SourceExpr="Response Time" }

    { 62  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the number of the loaner that has been lent to the customer to replace this service item.;
                           PTG=""];
                SourceExpr="Loaner No." }

    { 70  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the number of the vendor who sold this service item.;
                           PTG=""];
                SourceExpr="Vendor No.";
                Visible=FALSE }

    { 72  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the number assigned to this service item by the vendor.;
                           PTG=""];
                SourceExpr="Vendor Item No.";
                Visible=FALSE }

    { 50  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the date when service on this service item started.;
                           PTG=""];
                SourceExpr="Starting Date";
                Visible=FALSE }

    { 56  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the time when service on this service item started.;
                           PTG=""];
                SourceExpr="Starting Time";
                Visible=FALSE }

    { 58  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the time when service on this service item is finished.;
                           PTG=""];
                SourceExpr="Finishing Date";
                Visible=FALSE }

    { 60  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the time when the service on the order is finished.;
                           PTG=""];
                SourceExpr="Finishing Time";
                Visible=FALSE }

  }
  CODE
  {
    VAR
      ServLoanerMgt@1000 : Codeunit 5901;
      Text000@1001 : TextConst 'ENU=You can view the Service Item Log only for service item lines with the specified Service Item No.;PTG=S� pode ver Altera��es Produto Servi�o de linhas de produto de servi�o com o N.� Produto Servi�o especificado.';

    LOCAL PROCEDURE ShowServShipmentLines@2();
    VAR
      ServShipmentLine@1000 : Record 5991;
      ServShipmentLines@1001 : Page 5970;
    BEGIN
      TESTFIELD("No.");
      CLEAR(ServShipmentLine);
      ServShipmentLine.SETRANGE("Document No.","No.");
      ServShipmentLine.FILTERGROUP(2);
      CLEAR(ServShipmentLines);
      ServShipmentLines.Initialize("Line No.");
      ServShipmentLines.SETTABLEVIEW(ServShipmentLine);
      ServShipmentLines.RUNMODAL;
      ServShipmentLine.FILTERGROUP(0);
    END;

    PROCEDURE ReceiveLoaner@1();
    BEGIN
      ServLoanerMgt.ReceiveLoanerShipment(Rec);
    END;

    LOCAL PROCEDURE ShowServItemEventLog@13();
    VAR
      ServItemLog@1000 : Record 5942;
    BEGIN
      IF "Service Item No." = '' THEN
        ERROR(Text000);
      CLEAR(ServItemLog);
      ServItemLog.SETRANGE("Service Item No.","Service Item No.");
      PAGE.RUNMODAL(PAGE::"Service Item Log",ServItemLog);
    END;

    BEGIN
    END.
  }
}

