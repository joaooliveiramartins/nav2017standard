OBJECT Page 1707 Deferral Sched. Arch. Subform
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Deferral Schedule Detail;
               PTG=Detalhe Plano Diferimentos];
    SourceTable=Table5128;
    PageType=ListPart;
    OnAfterGetRecord=BEGIN
                       Changed := FALSE;
                     END;

    OnNewRecord=BEGIN
                  UpdateTotal;
                END;

    OnAfterGetCurrRecord=BEGIN
                           UpdateTotal;
                         END;

  }
  CONTROLS
  {
    { 1   ;    ;Container ;
                ContainerType=ContentArea }

    { 2   ;1   ;Group     ;
                Name=Group;
                GroupType=Repeater }

    { 3   ;2   ;Field     ;
                ToolTipML=ENU=Specifies the posting date for the entry.;
                ApplicationArea=#Suite;
                SourceExpr="Posting Date" }

    { 4   ;2   ;Field     ;
                ToolTipML=ENU=Specifies a description of the record.;
                ApplicationArea=#Suite;
                SourceExpr=Description }

    { 5   ;2   ;Field     ;
                ToolTipML=ENU=Specifies the line's net amount.;
                ApplicationArea=#Suite;
                SourceExpr=Amount }

    { 8   ;1   ;Group     ;
                GroupType=Group }

    { 7   ;2   ;Group     ;
                GroupType=Group }

    { 6   ;3   ;Field     ;
                CaptionML=[ENU=Total Amount to Defer;
                           PTG=Valor Total a Deferir];
                ToolTipML=ENU=Specifies the total amount to defer.;
                ApplicationArea=#Suite;
                SourceExpr=TotalDeferral;
                Enabled=FALSE;
                Editable=FALSE }

  }
  CODE
  {
    VAR
      TotalDeferral@1002 : Decimal;
      Changed@1000 : Boolean;

    LOCAL PROCEDURE UpdateTotal@1();
    BEGIN
      CalcTotal(Rec,TotalDeferral);
    END;

    LOCAL PROCEDURE CalcTotal@2(VAR DeferralLineArchive@1005 : Record 5128;VAR TotalDeferral@1002 : Decimal);
    VAR
      DeferralLineArchiveTemp@1006 : Record 5128;
      ShowTotalDeferral@1000 : Boolean;
    BEGIN
      DeferralLineArchiveTemp.COPYFILTERS(DeferralLineArchive);
      ShowTotalDeferral := DeferralLineArchiveTemp.CALCSUMS(Amount);
      IF ShowTotalDeferral THEN
        TotalDeferral := DeferralLineArchiveTemp.Amount;
    END;

    PROCEDURE GetChanged@3() : Boolean;
    BEGIN
      EXIT(Changed);
    END;

    BEGIN
    END.
  }
}

