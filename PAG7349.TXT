OBJECT Page 7349 Registered Movement
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Registered Movement;
               PTG=Movimento Registado];
    InsertAllowed=No;
    SourceTable=Table5772;
    SourceTableView=WHERE(Type=CONST(Movement));
    PageType=Document;
    RefreshOnActivate=Yes;
    OnOpenPage=BEGIN
                 SetWhseLocationFilter;
               END;

    ActionList=ACTIONS
    {
      { 1900000003;0 ;ActionContainer;
                      ActionContainerType=RelatedInformation }
      { 100     ;1   ;ActionGroup;
                      CaptionML=[ENU=&Movement;
                                 PTG=&Movimento];
                      Image=CreateMovement }
      { 101     ;2   ;Action    ;
                      ShortCutKey=Shift+Ctrl+L;
                      CaptionML=[ENU=List;
                                 PTG=Lista];
                      Image=OpportunitiesList;
                      OnAction=BEGIN
                                 LookupRegisteredActivityHeader("Location Code",Rec);
                               END;
                                }
      { 18      ;2   ;Action    ;
                      CaptionML=[ENU=Co&mments;
                                 PTG=Co&ment�rios];
                      RunObject=Page 5776;
                      RunPageLink=Table Name=CONST(Rgstrd. Whse. Activity Header),
                                  Type=FIELD(Type),
                                  No.=FIELD(No.);
                      Image=ViewComments }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                CaptionML=[ENU=General;
                           PTG=Geral] }

    { 4   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the registered warehouse activity number.;
                           PTG=""];
                SourceExpr="No.";
                Editable=FALSE }

    { 7   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the warehouse activity number from which the activity was registered.;
                           PTG=""];
                SourceExpr="Whse. Activity No.";
                Editable=FALSE }

    { 28  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the code of the location in which the registered warehouse activity occurred.;
                           PTG=""];
                SourceExpr="Location Code";
                Editable=FALSE }

    { 10  ;2   ;Field     ;
                ToolTipML=[ENU=This object supports the program infrastructure and is intended for internal use.;
                           PTG=""];
                SourceExpr="Registering Date";
                Editable=FALSE }

    { 8   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the ID of the employee who is responsible for the document and assigned to perform the warehouse activity.;
                           PTG=""];
                SourceExpr="Assigned User ID";
                Editable=FALSE }

    { 15  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the date the user was assigned the activity.;
                           PTG=""];
                SourceExpr="Assignment Date";
                Editable=FALSE }

    { 23  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the time of day the user was assigned the activity.;
                           PTG=""];
                SourceExpr="Assignment Time";
                Editable=FALSE }

    { 12  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the method by which the lines were sorted on the warehouse header, such as by item, or bin code.;
                           PTG=""];
                OptionCaptionML=[ENU=" ,Item,,Bin Code,Due Date,,Bin Ranking,Action Type";
                                 PTG=" ,Produto,,C�d. Posi��o,Data Vencimento,,Classifica��o Posi��o,Tipo A��o"];
                SourceExpr="Sorting Method";
                Editable=FALSE }

    { 2   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the number of times the warehouse activity has been printed.;
                           PTG=""];
                SourceExpr="No. Printed";
                Editable=FALSE }

    { 97  ;1   ;Part      ;
                Name=WhseActivityLines;
                SubPageView=SORTING(Activity Type,No.,Sorting Sequence No.);
                SubPageLink=Activity Type=FIELD(Type),
                            No.=FIELD(No.);
                PagePartID=Page7350 }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                Visible=TRUE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {

    BEGIN
    END.
  }
}

