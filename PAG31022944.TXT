OBJECT Page 31022944 Receipt
{
  OBJECT-PROPERTIES
  {
    Date=01/02/15;
    Time=13:00:00;
    Version List=NAVPTSS81.00;
  }
  PROPERTIES
  {
    CaptionML=PTG=Recibo;
    InsertAllowed=No;
    DeleteAllowed=No;
    SourceTable=Table31022932;
    PageType=Document;
    RefreshOnActivate=Yes;
  }
  CONTROLS
  {
    { 1000000000;0;Container;
                ContainerType=ContentArea }

    { 1000000001;1;Group  ;
                Name=Geral;
                GroupType=Group }

    { 1000000002;2;Field  ;
                SourceExpr="No.";
                Importance=Promoted;
                Editable=FALSE }

    { 1000000003;2;Field  ;
                SourceExpr="Entry No.";
                Editable=FALSE }

    { 1000000016;2;Field  ;
                SourceExpr="Customer No.";
                Importance=Promoted;
                Editable=FALSE }

    { 1000000015;2;Field  ;
                SourceExpr="Customer Name";
                Editable=FALSE }

    { 1000000004;2;Field  ;
                SourceExpr="Posting Date";
                Editable=FALSE }

    { 1000000005;2;Field  ;
                SourceExpr="Payment Type";
                Editable=FALSE }

    { 1000000006;2;Field  ;
                SourceExpr=Description;
                Editable=FALSE }

    { 1000000011;2;Field  ;
                SourceExpr="Payment Method Code";
                Editable=FALSE }

    { 1000000012;2;Field  ;
                SourceExpr="Payment Amount";
                Editable=FALSE }

    { 1000000013;2;Field  ;
                SourceExpr="Payment Date";
                Editable=FALSE }

    { 1000000014;2;Field  ;
                SourceExpr="User ID";
                Editable=FALSE }

    { 1000000008;2;Field  ;
                SourceExpr=Cancelled;
                Editable=FALSE }

    { 1000000018;1;Part   ;
                SubPageLink=Receipt No.=FIELD(No.);
                PagePartID=Page31022945;
                PartType=Page }

  }
  CODE
  {

    BEGIN
    END.
  }
}

