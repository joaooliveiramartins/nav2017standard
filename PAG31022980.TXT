OBJECT Page 31022980 Customer Pmt. Address List
{
  OBJECT-PROPERTIES
  {
    Date=01/02/15;
    Time=13:00:00;
    Version List=NAVPTSS81.00;
  }
  PROPERTIES
  {
    Editable=No;
    CaptionML=[ENU=Customer Pmt. Address List;
               PTG=Lista Endere�o Pagto. Cliente];
    SourceTable=Table31022947;
    DataCaptionFields=Customer No.;
    PageType=List;
    CardPageID=Customer Pmt. Address Card;
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 2   ;2   ;Field     ;
                SourceExpr=Code }

    { 4   ;2   ;Field     ;
                SourceExpr=Name }

    { 11  ;2   ;Field     ;
                SourceExpr="Post Code";
                Visible=FALSE }

    { 13  ;2   ;Field     ;
                SourceExpr="Country/Region Code";
                Visible=FALSE }

    { 15  ;2   ;Field     ;
                SourceExpr="Phone No." }

    { 17  ;2   ;Field     ;
                SourceExpr="Fax No.";
                Visible=FALSE }

    { 19  ;2   ;Field     ;
                SourceExpr=Contact }

  }
  CODE
  {

    BEGIN
    END.
  }
}

