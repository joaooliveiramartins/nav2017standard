OBJECT Page 1312 Office 365 Credentials
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0;
  }
  PROPERTIES
  {
    Permissions=TableData 1612=rimd;
    CaptionML=[ENU=Office 365 Credentials;
               PTG=Credenciais Office 365];
    SourceTable=Table1612;
    PageType=StandardDialog;
    SourceTableTemporary=Yes;
    OnAfterGetCurrRecord=BEGIN
                           StatusText := GETLASTERRORTEXT;
                         END;

  }
  CONTROLS
  {
    { 1   ;0   ;Container ;
                ContainerType=ContentArea }

    { 7   ;1   ;Group     ;
                GroupType=Group;
                InstructionalTextML=[ENU=Provide your Office 365 email address and password:;
                                     PTG=Forne�a o seu endere�o de email do Office 365 e password:] }

    { 3   ;2   ;Field     ;
                ExtendedDatatype=E-Mail;
                ToolTipML=ENU=Specifies the email address that is associated with the Office 365 account.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr=Email }

    { 4   ;2   ;Field     ;
                ExtendedDatatype=Masked;
                ToolTipML=ENU=Specifies the password that is associated with the Office 365 account.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr=Password }

    { 5   ;2   ;Field     ;
                ApplicationArea=#Basic,#Suite;
                SourceExpr=StatusText;
                Editable=FALSE;
                Style=Attention;
                StyleExpr=TRUE;
                ShowCaption=No }

    { 2   ;2   ;Field     ;
                ApplicationArea=#Basic,#Suite;
                SourceExpr=WhySignInIsNeededLbl;
                Editable=FALSE;
                OnDrillDown=BEGIN
                              MESSAGE(WhySignInIsNeededDescriptionMsg);
                            END;

                ShowCaption=No }

  }
  CODE
  {
    VAR
      StatusText@1002 : Text;
      WhySignInIsNeededLbl@1006 : TextConst 'ENU=Why do I have to sign in to Office 365 now?;PTG=Porque tenho que iniciiar sess�o no Office 365 agora?';
      WhySignInIsNeededDescriptionMsg@1007 : TextConst 'ENU=To set up the Business Inbox in Outlook, we need your permission to install two add-ins in Office 365.;PTG=Para configurar a Caixa de Entrada de Neg�cio no Outlook, necessitamos da sua permiss�o para instalar dois add-ins no Office 365.';

    BEGIN
    END.
  }
}

