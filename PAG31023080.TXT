OBJECT Page 31023080 Series Groups Card
{
  OBJECT-PROPERTIES
  {
    Date=15/11/15;
    Time=13:00:00;
    Version List=NAVPTSS85.00;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Series Groups Card;
               PTG=Ficha Sequ�ncias S�ries];
    SourceTable=Table31022974;
    PageType=Card;
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1000000000;1;Group  ;
                CaptionML=[ENU=General;
                           PTG=Geral] }

    { 1000000001;2;Field  ;
                SourceExpr=Code }

    { 1000000003;2;Field  ;
                SourceExpr=Type }

    { 1900604001;1;Group  ;
                CaptionML=[ENU=Numeration;
                           PTG=Numera��o] }

    { 1000000005;2;Field  ;
                ApplicationArea=#All;
                SourceExpr=Quote }

    { 1000000007;2;Field  ;
                ApplicationArea=#All;
                SourceExpr=Order }

    { 1000000029;2;Field  ;
                ApplicationArea=#All;
                SourceExpr="Blanket Order" }

    { 1000000009;2;Field  ;
                ApplicationArea=#All;
                SourceExpr=Invoice }

    { 1000000013;2;Field  ;
                ApplicationArea=#All;
                SourceExpr="Credit Memo" }

    { 1000000017;2;Field  ;
                ApplicationArea=#All;
                SourceExpr=Return }

    { 1000000021;2;Field  ;
                ApplicationArea=#All;
                SourceExpr=Reminder }

    { 1000000025;2;Field  ;
                ApplicationArea=#All;
                SourceExpr="Finance Charge Memo" }

    { 1000000019;2;Field  ;
                ApplicationArea=#All;
                SourceExpr="Shipment/Receipts" }

    { 1000000052;2;Field  ;
                ApplicationArea=#All;
                SourceExpr=Transfer }

    { 1000000054;2;Field  ;
                ApplicationArea=#All;
                SourceExpr="Ship. Transfer" }

    { 1000000056;2;Field  ;
                ApplicationArea=#All;
                SourceExpr="Receipt Transfer" }

    { 1000000011;2;Field  ;
                ApplicationArea=#All;
                SourceExpr="Posted Invoice" }

    { 1000000015;2;Field  ;
                ApplicationArea=#All;
                SourceExpr="Posted Credit Memo" }

    { 1000000031;2;Field  ;
                ApplicationArea=#All;
                SourceExpr="Posted Ret. Receipts/Ship." }

    { 1000000023;2;Field  ;
                ApplicationArea=#All;
                SourceExpr="Issued Reminder" }

    { 1000000027;2;Field  ;
                ApplicationArea=#All;
                SourceExpr="Issued F. Charge Memo" }

    { 1000000036;2;Field  ;
                ApplicationArea=#All;
                SourceExpr=Shipment }

    { 1000000038;2;Field  ;
                ApplicationArea=#All;
                SourceExpr="Posted Shipment" }

    { 1000000040;2;Field  ;
                ApplicationArea=#All;
                SourceExpr=Receipt }

    { 1000000042;2;Field  ;
                ApplicationArea=#All;
                SourceExpr="Posted Receipt" }

    { 1000000044;2;Field  ;
                ApplicationArea=#All;
                SourceExpr="Put-away" }

    { 1000000046;2;Field  ;
                ApplicationArea=#All;
                SourceExpr="Register Put-away" }

    { 1000000048;2;Field  ;
                ApplicationArea=#All;
                SourceExpr=Pick }

    { 1000000050;2;Field  ;
                ApplicationArea=#All;
                SourceExpr="Register Pick" }

    { 1000000058;2;Field  ;
                ApplicationArea=#All;
                SourceExpr=Contract }

    { 1000000060;2;Field  ;
                ApplicationArea=#All;
                SourceExpr="Service Quote" }

    { 1000000062;2;Field  ;
                ApplicationArea=#All;
                SourceExpr="Service Order" }

    { 2   ;2   ;Field     ;
                Name=<Debit Memo>;
                CaptionML=[ENU=Debit Memo;
                           PTG=Nota D�bito];
                ApplicationArea=#All;
                SourceExpr="Debit Memo" }

    { 1   ;2   ;Field     ;
                Name=<Posted Debit Memo>;
                CaptionML=[ENU=Posted Debit Memo;
                           PTG=Nota D�bito Registada];
                ApplicationArea=#All;
                SourceExpr="Posted Debit Memo" }

  }
  CODE
  {

    BEGIN
    END.
  }
}

