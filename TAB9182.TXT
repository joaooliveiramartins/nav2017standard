OBJECT Table 9182 Generic Chart Y-Axis
{
  OBJECT-PROPERTIES
  {
    Date=07/09/12;
    Time=12:00:00;
    Version List=NAVW17.00;
  }
  PROPERTIES
  {
    OnInsert=BEGIN
               TESTFIELD("Y-Axis Measure Field ID");
             END;

    CaptionML=[ENU=Generic Chart Y-Axis;
               PTG=Eixo-Y Gr�fico Padr�o];
  }
  FIELDS
  {
    { 2   ;   ;ID                  ;Code20        ;CaptionML=[ENU=ID;
                                                              PTG=ID] }
    { 3   ;   ;Line No.            ;Integer       ;CaptionML=[ENU=Line No.;
                                                              PTG=N� Linha] }
    { 10  ;   ;Y-Axis Measure Field ID;Integer    ;CaptionML=[ENU=Y-Axis Measure Field ID;
                                                              PTG=Eixo-Y ID Campo Medida] }
    { 11  ;   ;Y-Axis Measure Field Name;Text50   ;CaptionML=[ENU=Y-Axis Measure Field Name;
                                                              PTG=Eixo-Y Nome Campo Medida] }
    { 12  ;   ;Measure Operator    ;Option        ;CaptionML=[ENU=Measure Operator;
                                                              PTG=Operador Medida];
                                                   OptionCaptionML=[ENU=Sum,Count;
                                                                    PTG=Soma,Contagem];
                                                   OptionString=Sum,Count }
    { 13  ;   ;Y-Axis Measure Field Caption;Text250;
                                                   CaptionML=[ENU=Y-Axis Measure Field Caption;
                                                              PTG=Eixo-Y Legenda Campo Medida] }
    { 20  ;   ;Show Title          ;Boolean       ;CaptionML=[ENU=Show Title;
                                                              PTG=Mostrar T�tulo] }
    { 21  ;   ;Aggregation         ;Option        ;CaptionML=[ENU=Aggregation;
                                                              PTG=Agrega��o];
                                                   OptionCaptionML=[ENU=None,Count,Sum,Min,Max,Avg;
                                                                    PTG=Nenhum,Contagem,Soma,M�n,M�x,M�d];
                                                   OptionString=None,Count,Sum,Min,Max,Avg }
    { 22  ;   ;Chart Type          ;Option        ;CaptionML=[ENU=Chart Type;
                                                              PTG=Tipo de Gr�fico];
                                                   OptionCaptionML=[ENU=Column,Point,Line,ColumnStacked,ColumnStacked100,Area,AreaStacked,AreaStacked100,StepLine,Pie,Doughnut,Range,Radar,Funnel;
                                                                    PTG=Coluna,Ponto,Linha,ColunasSobrep.,ColunasSobrep.100,�rea,�reaSobrep.,�reaSobrep.100,Etapas,Tarte,Donut,Intervalo,Radar,Funil];
                                                   OptionString=Column,Point,Line,ColumnStacked,ColumnStacked100,Area,AreaStacked,AreaStacked100,StepLine,Pie,Doughnut,Range,Radar,Funnel }
  }
  KEYS
  {
    {    ;ID,Line No.                             ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    END.
  }
}

