OBJECT Table 253 G/L Entry - VAT Entry Link
{
  OBJECT-PROPERTIES
  {
    Date=07/09/12;
    Time=12:00:00;
    Version List=NAVW17.10;
  }
  PROPERTIES
  {
    Permissions=TableData 253=rimd;
    CaptionML=[ENU=G/L Entry - VAT Entry Link;
               PTG=Liga��o Mov. C/G - Mov. IVA];
  }
  FIELDS
  {
    { 1   ;   ;G/L Entry No.       ;Integer       ;TableRelation="G/L Entry"."Entry No.";
                                                   CaptionML=[ENU=G/L Entry No.;
                                                              PTG=N� Mov.C/G] }
    { 2   ;   ;VAT Entry No.       ;Integer       ;TableRelation="VAT Entry"."Entry No.";
                                                   CaptionML=[ENU=VAT Entry No.;
                                                              PTG=N� Mov. IVA] }
  }
  KEYS
  {
    {    ;G/L Entry No.,VAT Entry No.             ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    PROCEDURE InsertLink@1(GLEntryNo@1000 : Integer;VATEntryNo@1001 : Integer);
    VAR
      GLEntryVatEntryLink@1002 : Record 253;
    BEGIN
      GLEntryVatEntryLink.INIT;
      GLEntryVatEntryLink."G/L Entry No." := GLEntryNo;
      GLEntryVatEntryLink."VAT Entry No." := VATEntryNo;
      GLEntryVatEntryLink.INSERT;
    END;

    BEGIN
    END.
  }
}

