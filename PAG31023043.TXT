OBJECT Page 31023043 Imparity Ledger Entries
{
  OBJECT-PROPERTIES
  {
    Date=01/02/15;
    Time=13:00:00;
    Version List=NAVPTSS81.00;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Imparity Ledger Entries;
               PTG=Movs. Imparidade];
    SourceTable=Table31022967;
    PageType=List;
  }
  CONTROLS
  {
    { 1102058000;0;Container;
                ContainerType=ContentArea }

    { 1102058001;1;Group  ;
                Name=Group;
                GroupType=Repeater }

    { 1102058002;2;Field  ;
                ApplicationArea=#All;
                SourceExpr="Entry No." }

    { 1102058003;2;Field  ;
                ApplicationArea=#All;
                SourceExpr="Imparity Code" }

    { 1102058004;2;Field  ;
                ApplicationArea=#All;
                SourceExpr=Description }

    { 1102058005;2;Field  ;
                ApplicationArea=#All;
                SourceExpr="Entry Type" }

    { 1102058006;2;Field  ;
                ApplicationArea=#All;
                SourceExpr=Amount }

    { 1102058007;2;Field  ;
                ApplicationArea=#All;
                SourceExpr="Imparity Type" }

    { 1102058008;2;Field  ;
                ApplicationArea=#All;
                SourceExpr="Imparity SubType" }

    { 1102058009;2;Field  ;
                ApplicationArea=#All;
                SourceExpr="Tax behavior" }

    { 1102058010;2;Field  ;
                ApplicationArea=#All;
                SourceExpr="Imparity Posting Gr." }

    { 1102058011;2;Field  ;
                ApplicationArea=#All;
                SourceExpr="Posting Date" }

    { 1102058012;2;Field  ;
                ApplicationArea=#All;
                SourceExpr="Source Type" }

    { 1102058013;2;Field  ;
                ApplicationArea=#All;
                SourceExpr="Source No" }

    { 1102058014;2;Field  ;
                ApplicationArea=#All;
                SourceExpr="User ID" }

    { 1102058015;2;Field  ;
                ApplicationArea=#All;
                SourceExpr="Document Type" }

    { 1102058016;2;Field  ;
                ApplicationArea=#All;
                SourceExpr="Document No." }

    { 1102058017;2;Field  ;
                ApplicationArea=#All;
                SourceExpr="FA Ledger Entry No." }

  }
  CODE
  {

    BEGIN
    END.
  }
}

