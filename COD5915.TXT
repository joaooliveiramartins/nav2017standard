OBJECT Codeunit 5915 Customer-Notify by Email
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0;
  }
  PROPERTIES
  {
    TableNo=5900;
    OnRun=BEGIN
            ServHeader := Rec;
            NotifyByEMailWhenServiceIsDone;
            Rec := ServHeader;
          END;

  }
  CODE
  {
    VAR
      Text000@1000 : TextConst 'ENU=We have finished carrying out service order %1.;PTG=Acabou de executar a ordem de servi�o %1.';
      Text001@1001 : TextConst 'ENU=You can collect your serviced items when it is convenient for you.;PTG=Pode recolher os seus produtos de servi�o quando lhe for conveniente.';
      Text002@1002 : TextConst 'ENU=The customer will be notified as requested because service order %1 is now %2.;PTG=O cliente ser� informado como pedido porque a ordem de servi�o %1 est� agora %2.';
      ServHeader@1003 : Record 5900;

    LOCAL PROCEDURE NotifyByEMailWhenServiceIsDone@1();
    VAR
      ServEmailQueue@1000 : Record 5935;
    BEGIN
      IF ServHeader."Notify Customer" <> ServHeader."Notify Customer"::"By Email" THEN
        EXIT;

      ServEmailQueue.INIT;
      IF ServHeader."Ship-to Code" <> '' THEN
        ServEmailQueue."To Address" := ServHeader."Ship-to E-Mail";
      IF ServEmailQueue."To Address" = '' THEN
        ServEmailQueue."To Address" := ServHeader."E-Mail";
      IF ServEmailQueue."To Address" = '' THEN
        EXIT;

      ServEmailQueue."Copy-to Address" := '';
      ServEmailQueue."Subject Line" := STRSUBSTNO(Text000,ServHeader."No.");
      ServEmailQueue."Body Line" := Text001;
      ServEmailQueue."Attachment Filename" := '';
      ServEmailQueue."Document Type" := ServEmailQueue."Document Type"::"Service Order";
      ServEmailQueue."Document No." := ServHeader."No.";
      ServEmailQueue.Status := ServEmailQueue.Status::" ";
      ServEmailQueue.INSERT(TRUE);
      ServEmailQueue.ScheduleInJobQueue;
      MESSAGE(
        Text002,
        ServHeader."No.",ServHeader.Status);
    END;

    BEGIN
    END.
  }
}

