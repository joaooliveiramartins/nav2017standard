OBJECT Page 31023086 Withholding Tax Ledger Entries
{
  OBJECT-PROPERTIES
  {
    Date=10/10/16;
    Time=13:00:00;
    Version List=NAVPTSS93.00;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Withholding Tax Ledger Entries;
               PTG=Movs. Reten��o na Fonte];
    SourceTable=Table31022977;
    PageType=List;
  }
  CONTROLS
  {
    { 31022890;0;Container;
                ContainerType=ContentArea }

    { 31022891;1;Group    ;
                Name=Group;
                GroupType=Repeater }

    { 31022892;2;Field    ;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Entry No." }

    { 31022893;2;Field    ;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Entity Type" }

    { 31022894;2;Field    ;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Entity No." }

    { 31022907;2;Field    ;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Income Amount" }

    { 31022895;2;Field    ;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Document Type" }

    { 31022896;2;Field    ;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Document No." }

    { 31022897;2;Field    ;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Document Date" }

    { 31022898;2;Field    ;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Posting Date" }

    { 31022899;2;Field    ;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Withholding Tax Amount" }

    { 31022900;2;Field    ;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Withholding Tax Code" }

    { 31022901;2;Field    ;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Withholding Tax %" }

    { 31022902;2;Field    ;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Has Withholding Tax" }

    { 31022904;2;Field    ;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Withholding Tax Account" }

    { 31022905;2;Field    ;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="VAT Registration No." }

  }
  CODE
  {

    BEGIN
    {
      V93.00#00018 - Reten��o a Clientes e Fornecedores -  - 2016.06.29
    }
    END.
  }
}

