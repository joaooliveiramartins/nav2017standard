OBJECT Page 7601 Base Calendar List
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0;
  }
  PROPERTIES
  {
    Editable=No;
    CaptionML=[ENU=Base Calendar List;
               PTG=Lista Calend�rio Base];
    SourceTable=Table7600;
    PageType=List;
    CardPageID=Base Calendar Card;
    OnInit=BEGIN
             CurrPage.LOOKUPMODE := TRUE;
           END;

    ActionList=ACTIONS
    {
      { 1900000003;0 ;ActionContainer;
                      ActionContainerType=RelatedInformation }
      { 16      ;1   ;ActionGroup;
                      CaptionML=[ENU=&Base Calendar;
                                 PTG=Calend�rio &Base];
                      Image=Calendar }
      { 10      ;2   ;Action    ;
                      CaptionML=[ENU=&Where-Used List;
                                 PTG=Lista &Onde-Usado];
                      Image=Track;
                      OnAction=VAR
                                 CalendarMgmt@1000 : Codeunit 7600;
                                 WhereUsedList@1002 : Page 7608;
                               BEGIN
                                 CalendarMgmt.CreateWhereUsedEntries(Code);
                                 WhereUsedList.RUNMODAL;
                                 CLEAR(WhereUsedList);
                               END;
                                }
      { 11      ;2   ;Separator ;
                      CaptionML=[ENU=-;
                                 PTG=-] }
      { 12      ;2   ;Action    ;
                      CaptionML=[ENU=&Base Calendar Changes;
                                 PTG=Altera��es Calend�rio &Base];
                      RunObject=Page 7607;
                      RunPageLink=Base Calendar Code=FIELD(Code);
                      Image=Change }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 2   ;2   ;Field     ;
                CaptionML=[ENU=Code;
                           PTG=C�digo];
                ToolTipML=[ENU=Specifies the code for the base calendar you have set up.;
                           PTG=""];
                SourceExpr=Code }

    { 4   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the name of the base calendar in the entry.;
                           PTG=""];
                SourceExpr=Name }

    { 13  ;2   ;Field     ;
                CaptionML=[ENU=Customized Changes Exist;
                           PTG=Existem Altera��es Person.];
                ToolTipML=[ENU=Specifies that the base calendar has been used to create customized calendars.;
                           PTG=""];
                SourceExpr="Customized Changes Exist" }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {

    BEGIN
    END.
  }
}

