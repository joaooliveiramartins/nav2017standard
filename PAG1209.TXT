OBJECT Page 1209 Credit Trans Re-export History
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0;
  }
  PROPERTIES
  {
    Editable=No;
    CaptionML=[ENU=Credit Trans Re-export History;
               PTG=Hist. Re-exporta��o Transf. Cr�dito];
    SourceTable=Table1209;
    PageType=List;
  }
  CONTROLS
  {
    { 1   ;0   ;Container ;
                ContainerType=ContentArea }

    { 2   ;1   ;Group     ;
                Name=Group;
                GroupType=Repeater }

    { 3   ;2   ;Field     ;
                ToolTipML=ENU=Specifies the date when the payment file was re-exported.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Re-export Date" }

    { 4   ;2   ;Field     ;
                ToolTipML=ENU=Specifies the user who re-exported the payment file.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Re-exported By" }

  }
  CODE
  {

    BEGIN
    END.
  }
}

