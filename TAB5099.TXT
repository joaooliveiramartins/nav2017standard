OBJECT Table 5099 Saved Segment Criteria Line
{
  OBJECT-PROPERTIES
  {
    Date=07/09/12;
    Time=12:00:00;
    Version List=NAVW17.10;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Saved Segment Criteria Line;
               PTG=Linha Crit�rio Seg. Guardado];
  }
  FIELDS
  {
    { 1   ;   ;Segment Criteria Code;Code10       ;TableRelation="Saved Segment Criteria";
                                                   CaptionML=[ENU=Segment Criteria Code;
                                                              PTG=C�d. Crit�rio Segmento] }
    { 2   ;   ;Line No.            ;Integer       ;CaptionML=[ENU=Line No.;
                                                              PTG=N� Linha] }
    { 3   ;   ;Type                ;Option        ;CaptionML=[ENU=Type;
                                                              PTG=Tipo];
                                                   OptionCaptionML=[ENU=Action,Filter;
                                                                    PTG=A��o,Filtro];
                                                   OptionString=Action,Filter }
    { 4   ;   ;Action              ;Option        ;CaptionML=[ENU=Action;
                                                              PTG=A��o];
                                                   OptionCaptionML=[ENU=" ,Add Contacts,Remove Contacts (Reduce),Remove Contacts (Refine)";
                                                                    PTG=" ,Adicionar Contactos,Remover Contactos (Reduzir),Remover Contactos (Redefinir)"];
                                                   OptionString=[ ,Add Contacts,Remove Contacts (Reduce),Remove Contacts (Refine)] }
    { 5   ;   ;Table No.           ;Integer       ;CaptionML=[ENU=Table No.;
                                                              PTG=N� Tabela] }
    { 7   ;   ;View                ;Text250       ;CaptionML=[ENU=View;
                                                              PTG=Express�o] }
    { 8   ;   ;Allow Existing Contacts;Boolean    ;CaptionML=[ENU=Allow Existing Contacts;
                                                              PTG=Permite Contactos Existentes] }
    { 9   ;   ;Expand Contact      ;Boolean       ;CaptionML=[ENU=Expand Contact;
                                                              PTG=Expandir Contactos] }
    { 10  ;   ;Allow Company with Persons;Boolean ;CaptionML=[ENU=Allow Company with Persons;
                                                              PTG=Permite Empresas com Pessoas] }
    { 11  ;   ;Ignore Exclusion    ;Boolean       ;CaptionML=[ENU=Ignore Exclusion;
                                                              PTG=Ignorar Exclus�o] }
    { 12  ;   ;Entire Companies    ;Boolean       ;CaptionML=[ENU=Entire Companies;
                                                              PTG=Empresas Completas] }
    { 13  ;   ;Table Caption       ;Text250       ;FieldClass=FlowField;
                                                   CalcFormula=Lookup(AllObjWithCaption."Object Caption" WHERE (Object Type=CONST(Table),
                                                                                                                Object ID=FIELD(Table No.)));
                                                   CaptionML=[ENU=Table Caption;
                                                              PTG=Nome Tabela];
                                                   Editable=No }
    { 14  ;   ;No. of Filters      ;Integer       ;CaptionML=[ENU=No. of Filters;
                                                              PTG=N� de Filtros] }
  }
  KEYS
  {
    {    ;Segment Criteria Code,Line No.,Action   ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    PROCEDURE ActionTable@1() : Text[250];
    BEGIN
      CASE Type OF
        Type::Action:
          EXIT(FORMAT(Action));
        Type::Filter:
          BEGIN
            CALCFIELDS("Table Caption");
            EXIT("Table Caption");
          END;
      END;
    END;

    PROCEDURE Filter@2() : Text[250];
    VAR
      SegCriteriaManagement@1000 : Codeunit 5062;
    BEGIN
      EXIT(SegCriteriaManagement.SegCriteriaFilter("Table No.",View));
    END;

    BEGIN
    END.
  }
}

