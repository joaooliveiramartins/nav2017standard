OBJECT Table 92 Customer Posting Group
{
  OBJECT-PROPERTIES
  {
    Date=15/09/15;
    Time=13:00:00;
    Version List=NAVW19.00,NAVPTSS82.00;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Customer Posting Group;
               PTG=Gr. Contabil�stico Cliente];
    LookupPageID=Page110;
  }
  FIELDS
  {
    { 1   ;   ;Code                ;Code10        ;CaptionML=[ENU=Code;
                                                              PTG=C�digo];
                                                   NotBlank=Yes }
    { 2   ;   ;Receivables Account ;Code20        ;TableRelation="G/L Account";
                                                   OnValidate=BEGIN
                                                                CheckGLAcc("Receivables Account",FALSE,FALSE);
                                                              END;

                                                   CaptionML=[ENU=Receivables Account;
                                                              PTG=Conta Clientes] }
    { 7   ;   ;Service Charge Acc. ;Code20        ;TableRelation="G/L Account";
                                                   OnValidate=BEGIN
                                                                CheckGLAcc("Service Charge Acc.",TRUE,TRUE);
                                                              END;

                                                   CaptionML=[ENU=Service Charge Acc.;
                                                              PTG=Conta Despesas] }
    { 8   ;   ;Payment Disc. Debit Acc.;Code20    ;TableRelation="G/L Account";
                                                   OnValidate=BEGIN
                                                                CheckGLAcc("Payment Disc. Debit Acc.",FALSE,FALSE);
                                                              END;

                                                   CaptionML=[ENU=Payment Disc. Debit Acc.;
                                                              PTG=Conta D�bito Dsct. Pagto.] }
    { 9   ;   ;Invoice Rounding Account;Code20    ;TableRelation="G/L Account";
                                                   OnValidate=BEGIN
                                                                CheckGLAcc("Invoice Rounding Account",TRUE,FALSE);
                                                              END;

                                                   CaptionML=[ENU=Invoice Rounding Account;
                                                              PTG=Conta Arred. Fatura] }
    { 10  ;   ;Additional Fee Account;Code20      ;TableRelation="G/L Account";
                                                   OnValidate=BEGIN
                                                                CheckGLAcc("Additional Fee Account",TRUE,TRUE);
                                                              END;

                                                   CaptionML=[ENU=Additional Fee Account;
                                                              PTG=Conta Encargo Fixo] }
    { 11  ;   ;Interest Account    ;Code20        ;TableRelation="G/L Account";
                                                   OnValidate=BEGIN
                                                                CheckGLAcc("Interest Account",TRUE,FALSE);
                                                              END;

                                                   CaptionML=[ENU=Interest Account;
                                                              PTG=Conta Juros] }
    { 12  ;   ;Debit Curr. Appln. Rndg. Acc.;Code20;
                                                   TableRelation="G/L Account";
                                                   CaptionML=[ENU=Debit Curr. Appln. Rndg. Acc.;
                                                              PTG=Conta D�bito Arred. Liq. Div.] }
    { 13  ;   ;Credit Curr. Appln. Rndg. Acc.;Code20;
                                                   TableRelation="G/L Account";
                                                   CaptionML=[ENU=Credit Curr. Appln. Rndg. Acc.;
                                                              PTG=Conta Cr�dito Arred. Liq. Div.] }
    { 14  ;   ;Debit Rounding Account;Code20      ;TableRelation="G/L Account";
                                                   CaptionML=[ENU=Debit Rounding Account;
                                                              PTG=Conta Arred. D�bito] }
    { 15  ;   ;Credit Rounding Account;Code20     ;TableRelation="G/L Account";
                                                   CaptionML=[ENU=Credit Rounding Account;
                                                              PTG=Conta Arred. Cr�dito] }
    { 16  ;   ;Payment Disc. Credit Acc.;Code20   ;TableRelation="G/L Account";
                                                   OnValidate=BEGIN
                                                                CheckGLAcc("Payment Disc. Credit Acc.",FALSE,FALSE);
                                                              END;

                                                   CaptionML=[ENU=Payment Disc. Credit Acc.;
                                                              PTG=Conta Cr�dito Dsct. Pag.] }
    { 17  ;   ;Payment Tolerance Debit Acc.;Code20;TableRelation="G/L Account";
                                                   CaptionML=[ENU=Payment Tolerance Debit Acc.;
                                                              PTG=Conta D�bito Toler�ncia Pag.] }
    { 18  ;   ;Payment Tolerance Credit Acc.;Code20;
                                                   TableRelation="G/L Account";
                                                   CaptionML=[ENU=Payment Tolerance Credit Acc.;
                                                              PTG=Conta Cr�dito Toler�ncia Pag.] }
    { 19  ;   ;Add. Fee per Line Account;Code20   ;TableRelation="G/L Account";
                                                   OnValidate=BEGIN
                                                                CheckGLAcc("Add. Fee per Line Account",TRUE,TRUE);
                                                              END;

                                                   CaptionML=[ENU=Add. Fee per Line Account;
                                                              PTG=Adicionar Taxa por Linha de Conta] }
    { 31022913;;Bills Account      ;Code20        ;TableRelation="G/L Account";
                                                   CaptionML=[ENU=Bills Account;
                                                              PTG=Conta T�tulos] }
    { 31022914;;Discted. Bills Acc.;Code20        ;TableRelation="G/L Account";
                                                   CaptionML=[ENU=Discted. Bills Acc.;
                                                              PTG=Conta T�tulos Descontados] }
    { 31022915;;Bills on Collection Acc.;Code20   ;TableRelation="G/L Account";
                                                   CaptionML=[ENU=Bills on Collection Acc.;
                                                              PTG=Conta T�tulos � Cobran�a] }
    { 31022916;;Rejected Bills Acc.;Code20        ;TableRelation="G/L Account";
                                                   CaptionML=[ENU=Rejected Bills Acc.;
                                                              PTG=Conta T�tulos Devolvidos] }
    { 31022917;;Finance Income Acc.;Code20        ;TableRelation="G/L Account";
                                                   CaptionML=[ENU=Finance Income Acc.;
                                                              PTG=Conta Proveitos Financeiros] }
    { 31022918;;Factoring for Collection Acc.;Code20;
                                                   TableRelation="G/L Account";
                                                   CaptionML=[ENU=Factoring for Collection Acc.;
                                                              PTG=Conta Faturas ao Cobro] }
    { 31022919;;Factoring for Discount Acc.;Code20;TableRelation="G/L Account";
                                                   CaptionML=[ENU=Factoring for Discount Acc.;
                                                              PTG=Conta Faturas ao Desconto] }
    { 31022920;;Rejected Factoring Acc.;Code20    ;TableRelation="G/L Account";
                                                   CaptionML=[ENU=Rejected Factoring Acc.;
                                                              PTG=Conta Faturas Devolvidas] }
  }
  KEYS
  {
    {    ;Code                                    ;Clustered=Yes }
  }
  FIELDGROUPS
  {
    { 1   ;Brick               ;Code                                     }
  }
  CODE
  {

    LOCAL PROCEDURE CheckGLAcc@2(AccNo@1000 : Code[20];CheckProdPostingGroup@1001 : Boolean;CheckDirectPosting@1002 : Boolean);
    VAR
      GLAcc@1003 : Record 15;
    BEGIN
      IF AccNo <> '' THEN BEGIN
        GLAcc.GET(AccNo);
        GLAcc.CheckGLAcc;
        IF CheckProdPostingGroup THEN
          GLAcc.TESTFIELD("Gen. Prod. Posting Group");
        IF CheckDirectPosting THEN
          GLAcc.TESTFIELD("Direct Posting",TRUE);
      END;
    END;

    PROCEDURE GetReceivablesAccount@6() : Code[20];
    BEGIN
      TESTFIELD("Receivables Account");
      EXIT("Receivables Account");
    END;

    PROCEDURE GetPmtDiscountAccount@1(Debit@1000 : Boolean) : Code[20];
    BEGIN
      IF Debit THEN BEGIN
        TESTFIELD("Payment Disc. Debit Acc.");
        EXIT("Payment Disc. Debit Acc.");
      END;
      TESTFIELD("Payment Disc. Credit Acc.");
      EXIT("Payment Disc. Credit Acc.");
    END;

    PROCEDURE GetPmtToleranceAccount@3(Debit@1000 : Boolean) : Code[20];
    BEGIN
      IF Debit THEN BEGIN
        TESTFIELD("Payment Tolerance Debit Acc.");
        EXIT("Payment Tolerance Debit Acc.");
      END;
      TESTFIELD("Payment Tolerance Credit Acc.");
      EXIT("Payment Tolerance Credit Acc.");
    END;

    PROCEDURE GetRoundingAccount@4(Debit@1000 : Boolean) : Code[20];
    BEGIN
      IF Debit THEN BEGIN
        TESTFIELD("Debit Rounding Account");
        EXIT("Debit Rounding Account");
      END;
      TESTFIELD("Credit Rounding Account");
      EXIT("Credit Rounding Account");
    END;

    PROCEDURE GetApplRoundingAccount@5(Debit@1000 : Boolean) : Code[20];
    BEGIN
      IF Debit THEN BEGIN
        TESTFIELD("Debit Curr. Appln. Rndg. Acc.");
        EXIT("Debit Curr. Appln. Rndg. Acc.");
      END;
      TESTFIELD("Credit Curr. Appln. Rndg. Acc.");
      EXIT("Credit Curr. Appln. Rndg. Acc.");
    END;

    PROCEDURE "//--soft-func--//"@9000005();
    BEGIN
    END;

    PROCEDURE GetBillsAccount@9000015() : Code[20];
    BEGIN
      //soft,sn
      TESTFIELD("Bills Account");
      EXIT("Bills Account");
      //soft,en
    END;

    PROCEDURE GetDisctedBillsAcc@7() : Code[20];
    BEGIN
      //soft,sn
      TESTFIELD("Discted. Bills Acc.");
      EXIT("Discted. Bills Acc.");
      //soft,en
    END;

    PROCEDURE GetBillsCollectionsAcc@9() : Code[20];
    BEGIN
      //soft,sn
      TESTFIELD("Bills on Collection Acc.");
      EXIT("Bills on Collection Acc.");
      //soft,en
    END;

    PROCEDURE GetRejectedBillsAcc@10() : Code[20];
    BEGIN
      //soft,sn
      TESTFIELD("Rejected Bills Acc.");
      EXIT("Rejected Bills Acc.");
      //soft,en
    END;

    PROCEDURE GetRejectedFactoringAcc@11() : Code[20];
    BEGIN
      //soft,sn
      TESTFIELD("Rejected Factoring Acc.");
      EXIT("Rejected Factoring Acc.");
      //soft,en
    END;

    PROCEDURE GetFactoringForDiscAcc@12() : Code[20];
    BEGIN
      //soft,sn
      TESTFIELD("Factoring for Discount Acc.");
      EXIT("Factoring for Discount Acc.");
      //soft,en
    END;

    PROCEDURE GetFactoringForCollAcc@13() : Code[20];
    BEGIN
      //soft,sn
      TESTFIELD("Factoring for Collection Acc.");
      EXIT("Factoring for Collection Acc.");
      //soft,en
    END;

    BEGIN
    END.
  }
}

