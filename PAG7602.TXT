OBJECT Page 7602 Base Calendar Changes
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Base Calendar Changes;
               PTG=Altera��es Calend�rio Base];
    SourceTable=Table7601;
    DataCaptionFields=Base Calendar Code;
    PageType=List;
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 2   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the code of the base calendar in the entry.;
                           PTG=""];
                SourceExpr="Base Calendar Code";
                Visible=FALSE }

    { 17  ;2   ;Field     ;
                CaptionML=[ENU=Recurring System;
                           PTG=Sistema Periodicidade];
                ToolTipML=[ENU=Specifies a date or day as a recurring nonworking day.;
                           PTG=""];
                SourceExpr="Recurring System" }

    { 4   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the date to change associated with the base calendar in this entry.;
                           PTG=""];
                SourceExpr=Date }

    { 19  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the day of the week associated with this change entry.;
                           PTG=""];
                SourceExpr=Day }

    { 23  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies a description of the change in this entry.;
                           PTG=""];
                SourceExpr=Description }

    { 21  ;2   ;Field     ;
                CaptionML=[ENU=Nonworking;
                           PTG=N�o �til];
                ToolTipML=[ENU=Selects the field when you make an entry in the Base Calendar Changes window.;
                           PTG=""];
                SourceExpr=Nonworking }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {

    BEGIN
    END.
  }
}

