OBJECT Page 526 Posted Sales Invoice Lines
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0,NAVPTSS83.00;
  }
  PROPERTIES
  {
    Editable=No;
    CaptionML=[ENU=Posted Sales Invoice Lines;
               PTG=Linhas Fatura Venda Registada];
    SourceTable=Table113;
    PageType=List;
    ActionList=ACTIONS
    {
      { 1900000003;0 ;ActionContainer;
                      ActionContainerType=RelatedInformation }
      { 59      ;1   ;ActionGroup;
                      CaptionML=[ENU=&Line;
                                 PTG=&Linha];
                      Image=Line }
      { 60      ;2   ;Action    ;
                      ShortCutKey=Shift+F7;
                      CaptionML=[ENU=Show Document;
                                 PTG=Mostrar Documento];
                      ToolTipML=ENU=Open the document that the selected line exists on.;
                      ApplicationArea=#Basic,#Suite;
                      Image=View;
                      OnAction=BEGIN
                                 SalesInvHeader.GET("Document No.");
                                 //soft,sn
                                 IF SalesInvHeader."Debit Memo" THEN
                                   PAGE.RUN(PAGE::"Posted Sales Debit Memo",SalesInvHeader)
                                 ELSE
                                 //soft,en
                                 PAGE.RUN(PAGE::"Posted Sales Invoice",SalesInvHeader);
                               END;
                                }
      { 61      ;2   ;Action    ;
                      AccessByPermission=TableData 348=R;
                      ShortCutKey=Shift+Ctrl+D;
                      CaptionML=[ENU=Dimensions;
                                 PTG=Dimens�es];
                      ToolTipML=ENU=View or edits dimensions, such as area, project, or department, that you can assign to sales and purchase documents to distribute costs and analyze transaction history.;
                      ApplicationArea=#Suite;
                      Image=Dimensions;
                      OnAction=BEGIN
                                 ShowDimensions;
                               END;
                                }
      { 6500    ;2   ;Action    ;
                      ShortCutKey=Shift+Ctrl+I;
                      CaptionML=[ENU=Item &Tracking Lines;
                                 PTG=Linhas Ras&treio Produto];
                      ToolTipML=ENU=View or edit serial numbers and lot numbers that are assigned to the item on the document or journal line.;
                      Image=ItemTrackingLines;
                      OnAction=BEGIN
                                 ShowItemTrackingLines;
                               END;
                                }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 2   ;2   ;Field     ;
                ToolTipML=ENU=Specifies the document number.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Document No." }

    { 4   ;2   ;Field     ;
                ToolTipML=ENU=Specifies the customer that the invoice was sent to.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Sell-to Customer No." }

    { 6   ;2   ;Field     ;
                ToolTipML=ENU=Specifies the line type.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr=Type }

    { 8   ;2   ;Field     ;
                ToolTipML=ENU=Specifies the number of a general ledger account, item, resource, additional cost, or fixed asset, depending on the contents of the Type field.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="No." }

    { 10  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the variant number of the items sold.;
                SourceExpr="Variant Code";
                Visible=FALSE }

    { 12  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the name of the item or general ledger account, or some descriptive text.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr=Description }

    { 14  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the dimension value code associated with the invoice.;
                SourceExpr="Shortcut Dimension 1 Code";
                Visible=FALSE }

    { 16  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the dimension value code associated with the invoice.;
                SourceExpr="Shortcut Dimension 2 Code";
                Visible=FALSE }

    { 22  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the number of units of the item specified on the line.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr=Quantity }

    { 24  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the unit of measure code for the items sold.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Unit of Measure Code" }

    { 26  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the unit of measure for the item (bottle or piece, for example).;
                SourceExpr="Unit of Measure";
                Visible=FALSE }

    { 28  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the price of one unit of the item on the invoice line.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Unit Price" }

    { 30  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the unit cost of the item on the invoice line.;
                SourceExpr="Unit Cost (LCY)";
                Visible=FALSE }

    { 32  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the line's net amount.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr=Amount }

    { 34  ;2   ;Field     ;
                ToolTipML=ENU=This field is used internally.;
                SourceExpr="Amount Including VAT";
                Visible=FALSE }

    { 36  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the line discount % that was given on the line.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Line Discount %" }

    { 38  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the amount of the discount given on the line.;
                SourceExpr="Line Discount Amount";
                Visible=FALSE }

    { 40  ;2   ;Field     ;
                ToolTipML=ENU=Specifies whether the invoice line could have been included in a possible invoice discount calculation.;
                SourceExpr="Allow Invoice Disc.";
                Visible=FALSE }

    { 42  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the invoice discount amount calculated on the line.;
                SourceExpr="Inv. Discount Amount";
                Visible=FALSE }

    { 48  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the number of the item ledger entry this invoice line was applied to.;
                SourceExpr="Appl.-to Item Entry";
                Visible=FALSE }

    { 50  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the job number that the sales invoice line is linked to.;
                SourceExpr="Job No.";
                Visible=FALSE }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {
    VAR
      SalesInvHeader@1000 : Record 112;

    BEGIN
    END.
  }
}

