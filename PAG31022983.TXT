OBJECT Page 31022983 Vendor Pmt. Address Card
{
  OBJECT-PROPERTIES
  {
    Date=01/02/15;
    Time=13:00:00;
    Version List=NAVPTSS81.00;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Vendor Pmt. Address Card;
               PTG=Ficha Endere�o Pagto. Fornecedor];
    SourceTable=Table31022948;
    DataCaptionExpr=Caption;
    PageType=Card;
    OnFindRecord=BEGIN
                   IF NOT FIND(Which) THEN
                     SETRANGE(Code);
                   EXIT(FIND(Which));
                 END;

  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                CaptionML=[ENU=General;
                           PTG=Geral] }

    { 2   ;2   ;Field     ;
                SourceExpr=Code }

    { 12  ;2   ;Field     ;
                SourceExpr=Name }

    { 13  ;2   ;Field     ;
                SourceExpr=Address }

    { 14  ;2   ;Field     ;
                SourceExpr="Address 2" }

    { 16  ;2   ;Field     ;
                CaptionML=[ENU=Post Code/City;
                           PTG=C�d. Postal/Cidade];
                SourceExpr="Post Code" }

    { 25  ;2   ;Field     ;
                SourceExpr=City }

    { 17  ;2   ;Field     ;
                SourceExpr=County }

    { 15  ;2   ;Field     ;
                SourceExpr="Country/Region Code" }

    { 19  ;2   ;Field     ;
                SourceExpr="Phone No." }

    { 26  ;2   ;Field     ;
                SourceExpr=Contact }

    { 21  ;2   ;Field     ;
                SourceExpr="Last Date Modified" }

    { 1902768601;1;Group  ;
                CaptionML=[ENU=Communication;
                           PTG=Comunica��o] }

    { 34  ;2   ;Field     ;
                Name=<Phone No.2>;
                SourceExpr="Phone No." }

    { 31  ;2   ;Field     ;
                SourceExpr="Fax No." }

    { 32  ;2   ;Field     ;
                SourceExpr="E-Mail" }

    { 33  ;2   ;Field     ;
                SourceExpr="Home Page" }

  }
  CODE
  {
    VAR
      Mail@1110000 : Codeunit 397;

    BEGIN
    END.
  }
}

