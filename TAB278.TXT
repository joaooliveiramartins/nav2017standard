OBJECT Table 278 Job Journal Quantity
{
  OBJECT-PROPERTIES
  {
    Date=05/08/11;
    Time=12:00:00;
    Version List=NAVW16.00;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Job Journal Quantity;
               PTG=Qtd. Di�rio Projeto];
  }
  FIELDS
  {
    { 1   ;   ;Is Total            ;Boolean       ;CaptionML=[ENU=Is Total;
                                                              PTG=? Total] }
    { 2   ;   ;Unit of Measure Code;Code10        ;TableRelation="Unit of Measure";
                                                   CaptionML=[ENU=Unit of Measure Code;
                                                              PTG=C�d. Unidade Medida] }
    { 3   ;   ;Line Type           ;Option        ;CaptionML=[ENU=Line Type;
                                                              PTG=Tipo Linha];
                                                   OptionCaptionML=[ENU=,Total;
                                                                    PTG=,Total];
                                                   OptionString=,Total }
    { 4   ;   ;Work Type Code      ;Code10        ;TableRelation="Work Type";
                                                   CaptionML=[ENU=Work Type Code;
                                                              PTG=C�d. Tipo Trabalho] }
    { 5   ;   ;Quantity            ;Decimal       ;CaptionML=[ENU=Quantity;
                                                              PTG=Quantidade];
                                                   DecimalPlaces=0:5 }
  }
  KEYS
  {
    {    ;Is Total,Unit of Measure Code,Line Type,Work Type Code;
                                                   Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    END.
  }
}

