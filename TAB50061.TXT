OBJECT Table 50061 Workplaces
{
  OBJECT-PROPERTIES
  {
    Date=03/12/14;
    Time=11:05:21;
    Modified=Yes;
    Version List=DTT;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Workplaces;
               PTG=Locais de Trabalho];
  }
  FIELDS
  {
    { 1   ;   ;Worplace Code       ;Code10        ;CaptionML=PTG=Cod. Local }
    { 2   ;   ;Description         ;Text60        ;CaptionML=PTG=Designa��o }
    { 3   ;   ;Establishment Plans Code;Code20    ;CaptionML=PTG=Cod. Quadros Pessoal }
    { 4   ;   ;Date Effective      ;Date          ;CaptionML=PTG=Dt. Efectiva }
    { 5   ;   ;Situation           ;Text90        ;CaptionML=PTG=Situa��o }
    { 6   ;   ;TaxPayer No         ;Code20        ;CaptionML=PTG=N� Contribuinte Seg. Social }
    { 7   ;   ;Actitity            ;Text90        ;CaptionML=PTG=Actividade }
    { 8   ;   ;Adress              ;Text90        ;CaptionML=PTG=Morada }
    { 9   ;   ;Locality            ;Text60        ;CaptionML=PTG=Localidade }
    { 10  ;   ;Post Code           ;Text30        ;CaptionML=PTG=C�digo Postal }
    { 11  ;   ;Post Code Suf       ;Text30        ;CaptionML=PTG=Cod. Postal - Sufixo }
    { 12  ;   ;Phone No            ;Text30        ;CaptionML=PTG=Telefone }
    { 13  ;   ;District            ;Text30        ;CaptionML=PTG=Distrito }
    { 14  ;   ;County              ;Text30        ;CaptionML=PTG=Concelho }
    { 15  ;   ;Parish              ;Text30        ;CaptionML=PTG=Freguesia }
    { 16  ;   ;Regional Social Sec Centre;Text30  ;CaptionML=PTG=C.Regional Seg. Social }
    { 17  ;   ;Fax                 ;Text30        ;CaptionML=PTG=Fax }
    { 18  ;   ;Flag File Cont      ;Boolean       ;CaptionML=PTG=Flag Ficheiro Cont. }
    { 19  ;   ;Distribution Finance Code;Code20   ;CaptionML=PTG=Cod. Reparti��o Finan�as }
    { 20  ;   ;TAX Table           ;Text30        ;CaptionML=PTG=Tabela de I.R.S }
  }
  KEYS
  {
    {    ;Worplace Code                           ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    END.
  }
}

