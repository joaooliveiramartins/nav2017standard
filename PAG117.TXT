OBJECT Page 117 Item Registers
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0;
  }
  PROPERTIES
  {
    Editable=No;
    CaptionML=[ENU=Item Registers;
               PTG=Regs. Movs. Produto];
    SourceTable=Table46;
    PageType=List;
    ActionList=ACTIONS
    {
      { 1900000003;0 ;ActionContainer;
                      ActionContainerType=RelatedInformation }
      { 18      ;1   ;ActionGroup;
                      CaptionML=[ENU=&Register;
                                 PTG=&Reg. Movs.];
                      Image=Register }
      { 19      ;2   ;Action    ;
                      CaptionML=[ENU=Item Ledger;
                                 PTG=Movs. Produto];
                      ToolTipML=ENU=View the item ledger entries that resulted in the current register entry.;
                      ApplicationArea=#Basic,#Suite;
                      RunObject=Codeunit 245;
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=ItemLedger;
                      PromotedCategory=Process }
      { 25      ;2   ;Action    ;
                      CaptionML=[ENU=Phys. Invent&ory Ledger;
                                 PTG=Movs. &Invent�rio F�sico];
                      ToolTipML=ENU=View the physical inventory ledger entries that resulted in the current register entry.;
                      ApplicationArea=#Basic,#Suite;
                      RunObject=Codeunit 390;
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=PhysicalInventoryLedger;
                      PromotedCategory=Process }
      { 30      ;2   ;Action    ;
                      CaptionML=[ENU=Value Entries;
                                 PTG=Movs. Valor];
                      ToolTipML=ENU=View the item value entries that resulted in the current register entry.;
                      ApplicationArea=#Basic,#Suite;
                      RunObject=Codeunit 5800;
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=ValueLedger;
                      PromotedCategory=Process }
      { 40      ;2   ;Action    ;
                      CaptionML=[ENU=&Capacity Ledger;
                                 PTG=Movs. &Capacidade];
                      ToolTipML=ENU=View the capacity ledger entries that resulted in the current register entry.;
                      ApplicationArea=#Basic,#Suite;
                      RunObject=Codeunit 5835;
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=CapacityLedger;
                      PromotedCategory=Process }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 2   ;2   ;Field     ;
                ToolTipML=ENU=Specifies the number of the item register.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="No." }

    { 4   ;2   ;Field     ;
                ToolTipML=ENU=Specifies the date when the entries in the register were posted.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Creation Date" }

    { 10  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the ID of the user who posted the entries and created the item register.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="User ID" }

    { 6   ;2   ;Field     ;
                ToolTipML=ENU=Specifies the source code for the entries in the register.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Source Code" }

    { 8   ;2   ;Field     ;
                ToolTipML=ENU=Specifies the name of the item journal that the entries were posted from.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Journal Batch Name" }

    { 12  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the first item entry number in the register.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="From Entry No." }

    { 14  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the last item entry number in the register.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="To Entry No." }

    { 21  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the first physical inventory ledger entry number in the register.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="From Phys. Inventory Entry No." }

    { 23  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the last physical inventory ledger entry number in the register.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="To Phys. Inventory Entry No." }

    { 26  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the first value entry number in the register.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="From Value Entry No." }

    { 28  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the last value entry number in this register.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="To Value Entry No." }

    { 36  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the first capacity entry number in the register.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="From Capacity Entry No." }

    { 38  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the last capacity ledger entry number in this register.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="To Capacity Entry No." }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {

    BEGIN
    END.
  }
}

