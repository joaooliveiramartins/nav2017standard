OBJECT Table 1229 Payment Export Remittance Text
{
  OBJECT-PROPERTIES
  {
    Date=07/09/12;
    Time=12:00:00;
    Version List=NAVW17.10;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Payment Export Remittance Text;
               PTG=Texto Remessa Exporta��o Pagamento];
  }
  FIELDS
  {
    { 1   ;   ;Pmt. Export Data Entry No.;Integer ;TableRelation="Payment Export Data";
                                                   CaptionML=[ENU=Pmt. Export Data Entry No.;
                                                              PTG=N� Mov. Dados Exp. Pagamento] }
    { 2   ;   ;Line No.            ;Integer       ;CaptionML=[ENU=Line No.;
                                                              PTG=N� Linha] }
    { 3   ;   ;Text                ;Text140       ;CaptionML=[ENU=Text;
                                                              PTG=Texto] }
  }
  KEYS
  {
    {    ;Pmt. Export Data Entry No.,Line No.     ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    END.
  }
}

