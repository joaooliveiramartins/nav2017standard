OBJECT Page 114 Item Vendor Catalog
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Item Vendor Catalog;
               PTG=Cat. Pre�os Fornecedor];
    SourceTable=Table99;
    DataCaptionFields=Item No.;
    PageType=List;
    ActionList=ACTIONS
    {
      { 1900000003;0 ;ActionContainer;
                      ActionContainerType=RelatedInformation }
      { 4       ;1   ;ActionGroup;
                      CaptionML=[ENU=&Item Vendor;
                                 PTG=Fornecedor &Produto];
                      Image=Item }
      { 5       ;2   ;Action    ;
                      CaptionML=[ENU=Purch. &Prices;
                                 PTG=&Pre�os Compra];
                      ToolTipML=ENU=Define purchase price agreements with vendors for specific items.;
                      ApplicationArea=#Suite;
                      RunObject=Page 7012;
                      RunPageView=SORTING(Item No.,Vendor No.);
                      RunPageLink=Item No.=FIELD(Item No.),
                                  Vendor No.=FIELD(Vendor No.);
                      Image=Price }
      { 6       ;2   ;Action    ;
                      CaptionML=[ENU=Purch. Line &Discounts;
                                 PTG=&Descontos Linha Compra];
                      ToolTipML=ENU=Define purchase line discounts with vendors. For example, you may get for a line discount if you buy items from a vendor in large quantities.;
                      ApplicationArea=#Suite;
                      RunObject=Page 7014;
                      RunPageView=SORTING(Item No.,Vendor No.);
                      RunPageLink=Item No.=FIELD(Item No.),
                                  Vendor No.=FIELD(Vendor No.);
                      Image=LineDiscount }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 20  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the number of the item that the alternate direct unit cost is valid for.;
                SourceExpr="Item No.";
                Visible=FALSE }

    { 22  ;2   ;Field     ;
                ToolTipML=ENU=Specifies a variant code for the item.;
                SourceExpr="Variant Code";
                Visible=FALSE }

    { 2   ;2   ;Field     ;
                ToolTipML=ENU=Specifies the number of the vendor who offers the alternate direct unit cost.;
                ApplicationArea=#Suite;
                SourceExpr="Vendor No." }

    { 10  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the number that the vendor uses for this item.;
                ApplicationArea=#Suite;
                SourceExpr="Vendor Item No." }

    { 8   ;2   ;Field     ;
                ToolTipML=ENU=Specifies a date formula for the amount of time that it takes to replenish the item.;
                ApplicationArea=#Suite;
                SourceExpr="Lead Time Calculation" }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {

    BEGIN
    END.
  }
}

