OBJECT Page 141 Posted Purch. Cr. Memo Subform
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0,NAVPTSS93.00;
  }
  PROPERTIES
  {
    Editable=No;
    CaptionML=[ENU=Lines;
               PTG=Linhas];
    LinksAllowed=No;
    SourceTable=Table125;
    PageType=ListPart;
    AutoSplitKey=Yes;
    OnAfterGetCurrRecord=BEGIN
                           DocumentTotals.CalculatePostedPurchCreditMemoTotals(TotalPurchCrMemoHdr,VATAmount,Rec);
                         END;

    ActionList=ACTIONS
    {
      { 1900000004;0 ;ActionContainer;
                      ActionContainerType=ActionItems }
      { 13      ;1   ;Action    ;
                      Name=DeferralSchedule;
                      CaptionML=[ENU=Deferral Schedule;
                                 PTG=Plano Diferimentos];
                      ToolTipML=ENU=View the deferral schedule that governs how expenses paid with this purchase document were deferred to different accounting periods when the document was posted.;
                      ApplicationArea=#Suite;
                      Image=PaymentPeriod;
                      OnAction=BEGIN
                                 ShowDeferrals;
                               END;
                                }
      { 1907935204;1 ;ActionGroup;
                      CaptionML=[ENU=&Line;
                                 PTG=&Linha];
                      Image=Line }
      { 1900295304;2 ;Action    ;
                      Name=Dimensions;
                      AccessByPermission=TableData 348=R;
                      ShortCutKey=Shift+Ctrl+D;
                      CaptionML=[ENU=Dimensions;
                                 PTG=Dimens�es];
                      ToolTipML=ENU=View or edit dimensions, such as area, project, or department, that you can assign to sales and purchase documents to distribute costs and analyze transaction history.;
                      Image=Dimensions;
                      OnAction=BEGIN
                                 ShowDimensions;
                               END;
                                }
      { 1903099904;2 ;Action    ;
                      Name=Comments;
                      CaptionML=[ENU=Co&mments;
                                 PTG=Co&ment�rios];
                      Image=ViewComments;
                      OnAction=BEGIN
                                 ShowLineComments;
                               END;
                                }
      { 1905987604;2 ;Action    ;
                      Name=ItemTrackingEntries;
                      CaptionML=[ENU=Item &Tracking Entries;
                                 PTG=Movs. Ras&treio Produto];
                      Image=ItemTrackingLedger;
                      OnAction=BEGIN
                                 ShowItemTrackingLines;
                               END;
                                }
      { 1901743204;2 ;Action    ;
                      Name=ItemReturnShipmentLines;
                      AccessByPermission=TableData 6650=R;
                      CaptionML=[ENU=Item Return Shipment &Lines;
                                 PTG=&Linhas Envio Devolu��o Produto];
                      OnAction=BEGIN
                                 IF NOT (Type IN [Type::Item,Type::"Charge (Item)"]) THEN
                                   TESTFIELD(Type);
                                 ShowItemReturnShptLines;
                               END;
                                }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 2   ;2   ;Field     ;
                ToolTipML=ENU=Specifies the line type.;
                ApplicationArea=#All;
                SourceExpr=Type }

    { 4   ;2   ;Field     ;
                ToolTipML=ENU=Specifies an item number that identifies the item or a general ledger account number that identifies the general ledger account used when posting the line.;
                ApplicationArea=#All;
                SourceExpr="No." }

    { 66  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the cross-referenced item number. If you enter a cross reference between yours and your vendor's or customer's item number, then this number will override the standard item number when you enter the cross-reference number on a sales or purchase document.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Cross-Reference No." }

    { 14  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the code of the IC partner that the line has been distributed to.;
                SourceExpr="IC Partner Code";
                Visible=FALSE }

    { 46  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the variant code for the item.;
                SourceExpr="Variant Code";
                Visible=FALSE }

    { 6   ;2   ;Field     ;
                ToolTipML=ENU=Specifies either the name of, or a description of, the item or general ledger account.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr=Description }

    { 38  ;2   ;Field     ;
                ToolTipML=ENU=Specifies a code that explains why the item is returned.;
                SourceExpr="Return Reason Code" }

    { 8   ;2   ;Field     ;
                ToolTipML=ENU=Specifies the number of units of the item specified on the credit memo line.;
                ApplicationArea=#Basic,#Suite;
                BlankZero=Yes;
                SourceExpr=Quantity }

    { 64  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the unit of measure code for the item.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Unit of Measure Code" }

    { 1000000000;2;Field  ;
                SourceExpr="Tax behavior" }

    { 10  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the unit of measure for the item (bottle or piece, for example).;
                SourceExpr="Unit of Measure";
                Visible=FALSE }

    { 12  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the direct cost of one item unit.;
                ApplicationArea=#Basic,#Suite;
                BlankZero=Yes;
                SourceExpr="Direct Unit Cost" }

    { 18  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the percentage indirect cost for the item.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Indirect Cost %";
                Visible=FALSE }

    { 50  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the unit cost of the item on the line.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Unit Cost (LCY)";
                Visible=FALSE }

    { 20  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the price for one unit of the item.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Unit Price (LCY)" }

    { 70  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the net amount (before subtracting the invoice discount amount) that must be paid for the items on the line.;
                ApplicationArea=#Basic,#Suite;
                BlankZero=Yes;
                SourceExpr="Line Amount" }

    { 16  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the line discount % granted on items on each individual line.;
                ApplicationArea=#Basic,#Suite;
                BlankZero=Yes;
                SourceExpr="Line Discount %" }

    { 44  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the discount amount that was granted on the line.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Line Discount Amount";
                Visible=FALSE }

    { 42  ;2   ;Field     ;
                ToolTipML=ENU=Specifies whether the credit memo line could have been included in an invoice discount calculation.;
                SourceExpr="Allow Invoice Disc.";
                Visible=FALSE }

    { 34  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the job number the purchase invoice line is linked to.;
                SourceExpr="Job No.";
                Visible=FALSE }

    { 62  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the production order number.;
                SourceExpr="Prod. Order No.";
                Visible=FALSE }

    { 24  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the insurance number on the purchase credit memo line.;
                SourceExpr="Insurance No.";
                Visible=FALSE }

    { 32  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the budgeted FA number on the purchase credit memo line.;
                SourceExpr="Budgeted FA No.";
                Visible=FALSE }

    { 22  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the FA posting type of the purchase credit memo line.;
                SourceExpr="FA Posting Type";
                Visible=FALSE }

    { 26  ;2   ;Field     ;
                ToolTipML=ENU=Specifies whether depreciation was calculated until the FA posting date of the line.;
                SourceExpr="Depr. until FA Posting Date";
                Visible=FALSE }

    { 30  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the depreciation book code on the purchase credit memo line.;
                SourceExpr="Depreciation Book Code";
                Visible=FALSE }

    { 28  ;2   ;Field     ;
                ToolTipML=ENU=Specifies whether, when this line was posted, the additional acquisition cost posted on the line was depreciated in proportion to the amount by which the fixed asset had already been depreciated.;
                SourceExpr="Depr. Acquisition Cost";
                Visible=FALSE }

    { 36  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the number of a particular item entry to which the credit memo was applied.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Appl.-to Item Entry";
                Visible=FALSE }

    { 11  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the deferral template that governs how expenses paid with this purchase document are deferred to the different accounting periods when the expenses were incurred.;
                ApplicationArea=#Suite;
                SourceExpr="Deferral Code" }

    { 56  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the code for Shortcut Dimension 1.;
                ApplicationArea=#Suite;
                SourceExpr="Shortcut Dimension 1 Code";
                Visible=FALSE }

    { 54  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the code for Shortcut Dimension 2.;
                ApplicationArea=#Suite;
                SourceExpr="Shortcut Dimension 2 Code";
                Visible=FALSE }

    { 1110000;2;Field     ;
                SourceExpr="DRF Code";
                Visible=FALSE }

    { 31022891;2;Field    ;
                SourceExpr="Withholding Tax Amount" }

    { 33  ;1   ;Group     ;
                GroupType=Group }

    { 23  ;2   ;Group     ;
                GroupType=Group }

    { 25  ;3   ;Field     ;
                Name=Invoice Discount Amount;
                CaptionML=[ENU=Invoice Discount Amount;
                           PTG=Valor Desconto Fatura];
                ToolTipML=ENU=Specifies a discount amount that is deducted from the value in the Total Incl. VAT field. You can enter or change the amount manually.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr=TotalPurchCrMemoHdr."Invoice Discount Amount";
                AutoFormatType=1;
                AutoFormatExpr=TotalPurchCrMemoHdr."Currency Code";
                CaptionClass=DocumentTotals.GetInvoiceDiscAmountWithVATCaption(TotalPurchCrMemoHdr."Prices Including VAT");
                Editable=FALSE }

    { 9   ;2   ;Group     ;
                GroupType=Group }

    { 7   ;3   ;Field     ;
                Name=Total Amount Excl. VAT;
                DrillDown=No;
                CaptionML=[ENU=Total Amount Excl. VAT;
                           PTG=Valor Total Excl. IVA];
                ToolTipML=ENU=Specifies the sum of the value in the Line Amount Excl. VAT field on all lines in the document minus any discount amount in the Invoice Discount Amount field.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr=TotalPurchCrMemoHdr.Amount;
                AutoFormatType=1;
                AutoFormatExpr=TotalPurchCrMemoHdr."Currency Code";
                CaptionClass=DocumentTotals.GetTotalExclVATCaption(TotalPurchCrMemoHdr."Currency Code");
                Editable=FALSE }

    { 5   ;3   ;Field     ;
                Name=Total VAT Amount;
                CaptionML=[ENU=Total VAT;
                           PTG=Total IVA];
                ToolTipML=ENU=Specifies the sum of VAT amounts on all lines in the document.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr=VATAmount;
                AutoFormatType=1;
                AutoFormatExpr=TotalPurchCrMemoHdr."Currency Code";
                CaptionClass=DocumentTotals.GetTotalVATCaption(TotalPurchCrMemoHdr."Currency Code");
                Editable=FALSE }

    { 3   ;3   ;Field     ;
                Name=Total Amount Incl. VAT;
                CaptionML=[ENU=Total Amount Incl. VAT;
                           PTG=Valor Total Incl. IVA];
                ToolTipML=ENU=Specifies the sum of the value in the Line Amount Incl. VAT field on all lines in the document minus any discount amount in the Invoice Discount Amount field.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr=TotalPurchCrMemoHdr."Amount Including VAT";
                AutoFormatType=1;
                AutoFormatExpr=TotalPurchCrMemoHdr."Currency Code";
                CaptionClass=DocumentTotals.GetTotalInclVATCaption(TotalPurchCrMemoHdr."Currency Code");
                Editable=FALSE;
                Style=Strong;
                StyleExpr=TRUE }

  }
  CODE
  {
    VAR
      TotalPurchCrMemoHdr@1004 : Record 124;
      DocumentTotals@1003 : Codeunit 57;
      VATAmount@1002 : Decimal;

    BEGIN
    {
      V93.00#00018 -  Reten��o a Clientes e Fornecedores -  - 2016.06.29
    }
    END.
  }
}

