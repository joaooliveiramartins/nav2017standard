OBJECT Report 31022906 Copy IES Statement Template
{
  OBJECT-PROPERTIES
  {
    Date=01/02/15;
    Time=13:00:00;
    Version List=NAVPTSS81.00;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Copy IES Statement Template;
               PTG=Copiar Modelo Declara��o IES];
    ProcessingOnly=Yes;
  }
  DATASET
  {
    { 6233;    ;DataItem;                    ;
               DataItemTable=Table31022915;
               DataItemTableView=SORTING(Name);
               OnAfterGetRecord=VAR
                                  IESStatementTemplate@1110000 : Record 31022915;
                                BEGIN
                                  IESStatementTemplate := "IES Statement Template";
                                  IESStatementTemplate.Name := IESStatementName;
                                  IESStatementTemplate.INSERT;
                                END;
                                 }

    { 6358;1   ;DataItem;                    ;
               DataItemTable=Table31022916;
               DataItemTableView=SORTING(IES Statement Name,Name);
               OnAfterGetRecord=VAR
                                  IESAnnex@1110000 : Record 31022916;
                                BEGIN
                                  IESAnnex := "IES Annex";
                                  IESAnnex."IES Statement Name" := IESStatementName;
                                  IESAnnex.INSERT;
                                END;

               DataItemLink=IES Statement Name=FIELD(Name) }

    { 9128;2   ;DataItem;                    ;
               DataItemTable=Table31022917;
               DataItemTableView=SORTING(IES Statement Name,IES Annex Name,Name);
               OnAfterGetRecord=VAR
                                  IESFrame@1110000 : Record 31022917;
                                BEGIN
                                  IESFrame := "IES Frame";
                                  IESFrame."IES Statement Name" := IESStatementName;
                                  IESFrame.INSERT;
                                END;

               DataItemLink=IES Statement Name=FIELD(IES Statement Name),
                            IES Annex Name=FIELD(Name) }

    { 1955;3   ;DataItem;                    ;
               DataItemTable=Table31022918;
               DataItemTableView=SORTING(IES Statement Name,IES Annex Name,IES Frame Name,Name);
               OnAfterGetRecord=VAR
                                  IESField@1110000 : Record 31022918;
                                BEGIN
                                  IESField := "IES Field";
                                  IESField."IES Statement Name" := IESStatementName;
                                  IESField.INSERT;
                                END;

               DataItemLink=IES Statement Name=FIELD(IES Statement Name),
                            IES Annex Name=FIELD(IES Annex Name),
                            IES Frame Name=FIELD(Name) }

    { 6585;2   ;DataItem;                    ;
               DataItemTable=Table31022919;
               DataItemTableView=SORTING(IES Statement Name,IES Annex Name,Line No.);
               OnAfterGetRecord=VAR
                                  IESAnnexStructureLine@1110000 : Record 31022919;
                                BEGIN
                                  IESAnnexStructureLine := "IES Annex Structure Line";
                                  IESAnnexStructureLine."IES Statement Name" := IESStatementName;
                                  IESAnnexStructureLine.INSERT;
                                END;

               DataItemLink=IES Statement Name=FIELD(IES Statement Name),
                            IES Annex Name=FIELD(Name) }

  }
  REQUESTPAGE
  {
    PROPERTIES
    {
    }
    CONTROLS
    {
      { 1900000001;0;Container;
                  ContainerType=ContentArea }

      { 1900000002;1;Group  ;
                  CaptionML=[ENU=Options;
                             PTG=Op��es] }

      { 1110000;2;Field     ;
                  CaptionML=[ENU=New IES Statement Name;
                             PTG=Nome Nova Declara��o IES];
                  SourceExpr=IESStatementName }

    }
  }
  LABELS
  {
  }
  CODE
  {
    VAR
      IESStatementName@1110000 : Code[20];

    BEGIN
    END.
  }
  RDLDATA
  {
  }
}

