OBJECT Page 9090 Item Replenishment FactBox
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Item Details - Replenishment;
               PTG=Detalhes Produto - Reaprovisionamento];
    SourceTable=Table27;
    PageType=CardPart;
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 3   ;1   ;Field     ;
                CaptionML=[ENU=Item No.;
                           PTG=N� Produto];
                ToolTipML=[ENU=Specifies the number of the item.;
                           PTG=""];
                SourceExpr="No.";
                OnDrillDown=BEGIN
                              ShowDetails;
                            END;
                             }

    { 1   ;1   ;Field     ;
                ToolTipML=[ENU=Specifies the type of supply order that is created by the planning system when the item needs to be replenished.;
                           PTG=""];
                SourceExpr="Replenishment System" }

    { 17  ;1   ;Group     ;
                CaptionML=[ENU=Purchase;
                           PTG=Compra] }

    { 5   ;2   ;Field     ;
                Lookup=No;
                ToolTipML=[ENU=Specifies the code of the vendor from whom this item is supplied by default.;
                           PTG=""];
                SourceExpr="Vendor No.";
                OnDrillDown=VAR
                              Vendor@1001 : Record 23;
                            BEGIN
                              Vendor.SETFILTER("No.","Vendor No.");

                              PAGE.RUN(PAGE::"Vendor Card",Vendor);
                            END;
                             }

    { 7   ;2   ;Field     ;
                Lookup=No;
                ToolTipML=[ENU=Specifies the number that the vendor uses for this item.;
                           PTG=""];
                SourceExpr="Vendor Item No." }

    { 18  ;1   ;Group     ;
                CaptionML=[ENU=Production;
                           PTG=Produ��o] }

    { 11  ;2   ;Field     ;
                ToolTipML=[ENU=Defines whether additional orders for any related components are calculated.;
                           PTG=""];
                SourceExpr="Manufacturing Policy" }

    { 13  ;2   ;Field     ;
                Lookup=No;
                ToolTipML=[ENU=Specifies the number of the routing.;
                           PTG=""];
                SourceExpr="Routing No.";
                OnDrillDown=VAR
                              RoutingHeader@1001 : Record 99000763;
                            BEGIN
                              RoutingHeader.SETFILTER("No.","Routing No.");

                              PAGE.RUN(PAGE::Routing,RoutingHeader);
                            END;
                             }

    { 15  ;2   ;Field     ;
                Lookup=No;
                ToolTipML=[ENU=Specifies the number of the production BOM.;
                           PTG=""];
                SourceExpr="Production BOM No.";
                OnDrillDown=VAR
                              ProdBomHeader@1001 : Record 99000771;
                            BEGIN
                              ProdBomHeader.SETFILTER("No.","Production BOM No.");

                              PAGE.RUN(PAGE::"Production BOM",ProdBomHeader);
                            END;
                             }

  }
  CODE
  {

    LOCAL PROCEDURE ShowDetails@1102601000();
    BEGIN
      PAGE.RUN(PAGE::"Item Card",Rec);
    END;

    BEGIN
    END.
  }
}

