OBJECT Table 2001 Time Series Forecast
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Time Series Forecast;
               PTG=Previs�es S�rie de Tempo];
  }
  FIELDS
  {
    { 1   ;   ;Group ID            ;Code50        ;CaptionML=[ENU=Group ID;
                                                              PTG=ID Grupo] }
    { 2   ;   ;Period No.          ;Integer       ;CaptionML=[ENU=Period No.;
                                                              PTG=N� Per�odo] }
    { 3   ;   ;Period Start Date   ;Date          ;CaptionML=[ENU=Period Start Date;
                                                              PTG=Data In�cio Per�odo] }
    { 4   ;   ;Value               ;Decimal       ;CaptionML=[ENU=Value;
                                                              PTG=Valor] }
    { 5   ;   ;Delta               ;Decimal       ;CaptionML=[ENU=Delta;
                                                              PTG=Delta] }
    { 6   ;   ;Delta %             ;Decimal       ;CaptionML=[ENU=Delta %;
                                                              PTG=% Delta] }
  }
  KEYS
  {
    {    ;Group ID,Period No.                     ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    END.
  }
}

