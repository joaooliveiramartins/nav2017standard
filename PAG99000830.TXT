OBJECT Page 99000830 Firm Planned Prod. Order Lines
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Lines;
               PTG=Linhas];
    MultipleNewLines=Yes;
    LinksAllowed=No;
    SourceTable=Table5406;
    DelayedInsert=Yes;
    SourceTableView=WHERE(Status=CONST(Firm Planned));
    PageType=ListPart;
    AutoSplitKey=Yes;
    OnAfterGetRecord=BEGIN
                       DescriptionIndent := 0;
                       ShowShortcutDimCode(ShortcutDimCode);
                       DescriptionOnFormat;
                     END;

    OnNewRecord=BEGIN
                  CLEAR(ShortcutDimCode);
                END;

    OnDeleteRecord=VAR
                     ReserveProdOrderLine@1000 : Codeunit 99000837;
                   BEGIN
                     COMMIT;
                     IF NOT ReserveProdOrderLine.DeleteLineConfirm(Rec) THEN
                       EXIT(FALSE);
                   END;

    ActionList=ACTIONS
    {
      { 1900000004;0 ;ActionContainer;
                      ActionContainerType=ActionItems }
      { 1906587504;1 ;ActionGroup;
                      CaptionML=[ENU=F&unctions;
                                 PTG=F&un��es];
                      Image=Action }
      { 1901741804;2 ;Action    ;
                      CaptionML=[ENU=&Reserve;
                                 PTG=&Reserva];
                      Image=Reserve;
                      OnAction=BEGIN
                                 PageShowReservation;
                               END;
                                }
      { 1900205904;2 ;Action    ;
                      CaptionML=[ENU=Order &Tracking;
                                 PTG=Ras&treio Encomenda];
                      Image=OrderTracking;
                      OnAction=BEGIN
                                 ShowTracking;
                               END;
                                }
      { 1907935204;1 ;ActionGroup;
                      CaptionML=[ENU=&Line;
                                 PTG=&Linha];
                      Image=Line }
      { 1903867004;2 ;ActionGroup;
                      CaptionML=[ENU=Item Availability by;
                                 PTG=Disponibilidade Produto por];
                      Image=ItemAvailability }
      { 5       ;3   ;Action    ;
                      CaptionML=[ENU=Event;
                                 PTG=Ocorr�ncia];
                      Image=Event;
                      OnAction=BEGIN
                                 ItemAvailFormsMgt.ShowItemAvailFromProdOrderLine(Rec,ItemAvailFormsMgt.ByEvent);
                               END;
                                }
      { 1901313404;3 ;Action    ;
                      CaptionML=[ENU=Period;
                                 PTG=Per�odo];
                      Image=Period;
                      OnAction=BEGIN
                                 ItemAvailFormsMgt.ShowItemAvailFromProdOrderLine(Rec,ItemAvailFormsMgt.ByPeriod);
                               END;
                                }
      { 1901652304;3 ;Action    ;
                      CaptionML=[ENU=Variant;
                                 PTG=Variante];
                      Image=ItemVariant;
                      OnAction=BEGIN
                                 ItemAvailFormsMgt.ShowItemAvailFromProdOrderLine(Rec,ItemAvailFormsMgt.ByVariant);
                               END;
                                }
      { 1901991504;3 ;Action    ;
                      AccessByPermission=TableData 14=R;
                      CaptionML=[ENU=Location;
                                 PTG=Localiza��o];
                      Image=Warehouse;
                      OnAction=BEGIN
                                 ItemAvailFormsMgt.ShowItemAvailFromProdOrderLine(Rec,ItemAvailFormsMgt.ByLocation);
                               END;
                                }
      { 3       ;3   ;Action    ;
                      CaptionML=[ENU=BOM Level;
                                 PTG=N�vel L.M.];
                      Image=BOMLevel;
                      OnAction=BEGIN
                                 ItemAvailFormsMgt.ShowItemAvailFromProdOrderLine(Rec,ItemAvailFormsMgt.ByBOM);
                               END;
                                }
      { 1901313004;2 ;Action    ;
                      AccessByPermission=TableData 27=R;
                      CaptionML=[ENU=Reservation Entries;
                                 PTG=Movs. Reserva];
                      Image=ReservationLedger;
                      OnAction=BEGIN
                                 ShowReservationEntries(TRUE);
                               END;
                                }
      { 1900545004;2 ;Action    ;
                      AccessByPermission=TableData 348=R;
                      ShortCutKey=Shift+Ctrl+D;
                      CaptionML=[ENU=Dimensions;
                                 PTG=Dimens�es];
                      ToolTipML=[ENU=View or edit dimensions, such as area, project, or department, that you can assign to sales and purchase documents to distribute costs and analyze transaction history.;
                                 PTG=""];
                      Image=Dimensions;
                      OnAction=BEGIN
                                 ShowDimensions;
                               END;
                                }
      { 1901652204;2 ;Action    ;
                      CaptionML=[ENU=Ro&uting;
                                 PTG=Ga&ma Operat�ria];
                      Image=Route;
                      OnAction=BEGIN
                                 ShowRouting;
                               END;
                                }
      { 1903098604;2 ;Action    ;
                      CaptionML=[ENU=Components;
                                 PTG=Componentes];
                      Image=Components;
                      OnAction=BEGIN
                                 ShowComponents;
                               END;
                                }
      { 1905987604;2 ;Action    ;
                      ShortCutKey=Shift+Ctrl+I;
                      CaptionML=[ENU=Item &Tracking Lines;
                                 PTG=Linhas Ras&treio Produto];
                      Image=ItemTrackingLines;
                      OnAction=BEGIN
                                 OpenItemTrackingLines;
                               END;
                                }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                IndentationColumnName=DescriptionIndent;
                IndentationControls=Description;
                GroupType=Repeater }

    { 2   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the number of the item that is to be produced.;
                           PTG=""];
                SourceExpr="Item No." }

    { 4   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies a code if you have set up variant codes in the Item Variants window.;
                           PTG=""];
                SourceExpr="Variant Code";
                Visible=FALSE }

    { 8   ;2   ;Field     ;
                ToolTipML=[ENU=Copies the date in this field from the corresponding field on the production order header.;
                           PTG=""];
                SourceExpr="Due Date" }

    { 46  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies whether the supply represented by this line is considered by the planning system when calculating action messages.;
                           PTG=""];
                SourceExpr="Planning Flexibility";
                Visible=FALSE }

    { 6   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the value of the Description field on the item card. If you enter a variant code, the variant description is copied to this field instead.;
                           PTG=""];
                SourceExpr=Description }

    { 10  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies an additional description.;
                           PTG=""];
                SourceExpr="Description 2";
                Visible=FALSE }

    { 40  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the number of the production BOM that is the basis for creating the Prod. Order Component list for this line.;
                           PTG=""];
                SourceExpr="Production BOM No.";
                Visible=FALSE }

    { 42  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the number of the routing used as the basis for creating the production order routing for this line.;
                           PTG=""];
                SourceExpr="Routing No.";
                Visible=FALSE }

    { 30  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the version code of the production BOM.;
                           PTG=""];
                SourceExpr="Production BOM Version Code";
                Visible=FALSE }

    { 32  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the version number of the routing.;
                           PTG=""];
                SourceExpr="Routing Version Code";
                Visible=FALSE }

    { 24  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the location code, if the produced items should be stored in a specific location.;
                           PTG=""];
                SourceExpr="Location Code";
                Visible=FALSE }

    { 26  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the bin that the produced item is posted to as output, and from where it can be taken to storage or cross-docked.;
                           PTG=""];
                SourceExpr="Bin Code";
                Visible=FALSE }

    { 50  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the starting date and the starting time, which are combined in a format called "starting date-time".;
                           PTG=Especifica a data e hora iniciais, que est�o combinadas no formato "data-hora inicial".];
                SourceExpr="Starting Date-Time" }

    { 16  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the entry's starting time, which is retrieved from the production order routing.;
                           PTG=""];
                SourceExpr="Starting Time";
                Visible=FALSE }

    { 14  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the entry's starting date, which is retrieved from the production order routing.;
                           PTG=""];
                SourceExpr="Starting Date";
                Visible=FALSE }

    { 52  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the ending date and the ending time, which are combined in a format called "ending date-time".;
                           PTG=Especifica a data e hora finais, que est�o combinadas no formato "data-hora final".];
                SourceExpr="Ending Date-Time" }

    { 20  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the entry's ending time, which is retrieved from the production order routing.;
                           PTG=""];
                SourceExpr="Ending Time";
                Visible=FALSE }

    { 18  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the entry's ending date, which is retrieved from the production order routing.;
                           PTG=""];
                SourceExpr="Ending Date";
                Visible=FALSE }

    { 38  ;2   ;Field     ;
                ToolTipML=[ENU=Copies the value in this field from the Scrap Percentage field on the item card when the Item No. field is filled in.;
                           PTG=""];
                SourceExpr="Scrap %";
                Visible=FALSE }

    { 28  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the quantity to be produced if you manually fill in this line.;
                           PTG=""];
                SourceExpr=Quantity }

    { 44  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the base unit of measure code for the item.;
                           PTG=""];
                SourceExpr="Unit of Measure Code" }

    { 48  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies how many units of this item have been reserved.;
                           PTG=""];
                SourceExpr="Reserved Quantity";
                Visible=FALSE }

    { 34  ;2   ;Field     ;
                ToolTipML=[ENU=Calculates the unit cost, based on the cost of the components in the production order component list, and the routing, if the costing method is not standard.;
                           PTG=""];
                SourceExpr="Unit Cost" }

    { 36  ;2   ;Field     ;
                ToolTipML=[ENU=Calculates the amount by multiplying the Unit Cost by the Quantity.;
                           PTG=""];
                SourceExpr="Cost Amount" }

    { 12  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies a dimension value code for a dimension.;
                           PTG=""];
                SourceExpr="Shortcut Dimension 1 Code";
                Visible=FALSE }

    { 22  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies a dimension value code for a dimension.;
                           PTG=""];
                SourceExpr="Shortcut Dimension 2 Code";
                Visible=FALSE }

    { 300 ;2   ;Field     ;
                SourceExpr=ShortcutDimCode[3];
                CaptionClass='1,2,3';
                TableRelation="Dimension Value".Code WHERE (Global Dimension No.=CONST(3),
                                                            Dimension Value Type=CONST(Standard),
                                                            Blocked=CONST(No));
                Visible=FALSE;
                OnValidate=BEGIN
                             ValidateShortcutDimCode(3,ShortcutDimCode[3]);
                           END;
                            }

    { 302 ;2   ;Field     ;
                SourceExpr=ShortcutDimCode[4];
                CaptionClass='1,2,4';
                TableRelation="Dimension Value".Code WHERE (Global Dimension No.=CONST(4),
                                                            Dimension Value Type=CONST(Standard),
                                                            Blocked=CONST(No));
                Visible=FALSE;
                OnValidate=BEGIN
                             ValidateShortcutDimCode(4,ShortcutDimCode[4]);
                           END;
                            }

    { 304 ;2   ;Field     ;
                SourceExpr=ShortcutDimCode[5];
                CaptionClass='1,2,5';
                TableRelation="Dimension Value".Code WHERE (Global Dimension No.=CONST(5),
                                                            Dimension Value Type=CONST(Standard),
                                                            Blocked=CONST(No));
                Visible=FALSE;
                OnValidate=BEGIN
                             ValidateShortcutDimCode(5,ShortcutDimCode[5]);
                           END;
                            }

    { 306 ;2   ;Field     ;
                SourceExpr=ShortcutDimCode[6];
                CaptionClass='1,2,6';
                TableRelation="Dimension Value".Code WHERE (Global Dimension No.=CONST(6),
                                                            Dimension Value Type=CONST(Standard),
                                                            Blocked=CONST(No));
                Visible=FALSE;
                OnValidate=BEGIN
                             ValidateShortcutDimCode(6,ShortcutDimCode[6]);
                           END;
                            }

    { 308 ;2   ;Field     ;
                SourceExpr=ShortcutDimCode[7];
                CaptionClass='1,2,7';
                TableRelation="Dimension Value".Code WHERE (Global Dimension No.=CONST(7),
                                                            Dimension Value Type=CONST(Standard),
                                                            Blocked=CONST(No));
                Visible=FALSE;
                OnValidate=BEGIN
                             ValidateShortcutDimCode(7,ShortcutDimCode[7]);
                           END;
                            }

    { 310 ;2   ;Field     ;
                SourceExpr=ShortcutDimCode[8];
                CaptionClass='1,2,8';
                TableRelation="Dimension Value".Code WHERE (Global Dimension No.=CONST(8),
                                                            Dimension Value Type=CONST(Standard),
                                                            Blocked=CONST(No));
                Visible=FALSE;
                OnValidate=BEGIN
                             ValidateShortcutDimCode(8,ShortcutDimCode[8]);
                           END;
                            }

  }
  CODE
  {
    VAR
      ItemAvailFormsMgt@1000 : Codeunit 353;
      ShortcutDimCode@1001 : ARRAY [8] OF Code[20];
      DescriptionIndent@19057867 : Integer INDATASET;

    LOCAL PROCEDURE ShowComponents@1();
    VAR
      ProdOrderComp@1000 : Record 5407;
    BEGIN
      ProdOrderComp.SETRANGE(Status,Status);
      ProdOrderComp.SETRANGE("Prod. Order No.","Prod. Order No.");
      ProdOrderComp.SETRANGE("Prod. Order Line No.","Line No.");

      PAGE.RUN(PAGE::"Prod. Order Components",ProdOrderComp);
    END;

    LOCAL PROCEDURE ShowTracking@3();
    VAR
      TrackingForm@1000 : Page 99000822;
    BEGIN
      TrackingForm.SetProdOrderLine(Rec);
      TrackingForm.RUNMODAL;
    END;

    LOCAL PROCEDURE PageShowReservation@4();
    BEGIN
      CurrPage.SAVERECORD;
      ShowReservation;
    END;

    PROCEDURE UpdateForm@6(SetSaveRecord@1000 : Boolean);
    BEGIN
      CurrPage.UPDATE(SetSaveRecord);
    END;

    LOCAL PROCEDURE DescriptionOnFormat@19023855();
    BEGIN
      DescriptionIndent := "Planning Level Code";
    END;

    BEGIN
    END.
  }
}

