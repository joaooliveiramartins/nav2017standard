OBJECT Page 805 Online Map Parameter FactBox
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0;
  }
  PROPERTIES
  {
    Editable=No;
    CaptionML=[ENU=Online Map Parameter FactBox;
               PTG=Caixa Factos Par�metros Mapa Online];
    PageType=CardPart;
  }
  CONTROLS
  {
    { 1   ;    ;Container ;
                Name=Container;
                ContainerType=ContentArea }

    { 2   ;1   ;Field     ;
                CaptionML=[ENU={1};
                           PTG={1}];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=Text001 }

    { 3   ;1   ;Field     ;
                CaptionML=[ENU={2};
                           PTG={2}];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=Text002 }

    { 4   ;1   ;Field     ;
                CaptionML=[ENU={3};
                           PTG={3}];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=Text003 }

    { 5   ;1   ;Field     ;
                CaptionML=[ENU={4};
                           PTG={4}];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=Text004 }

    { 6   ;1   ;Field     ;
                CaptionML=[ENU={5};
                           PTG={5}];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=Text005 }

    { 7   ;1   ;Field     ;
                CaptionML=[ENU={6};
                           PTG={6}];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=Text006 }

    { 8   ;1   ;Field     ;
                CaptionML=[ENU={7};
                           PTG={7}];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=Text007 }

    { 9   ;1   ;Field     ;
                CaptionML=[ENU={8};
                           PTG={8}];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=Text008 }

    { 10  ;1   ;Field     ;
                CaptionML=[ENU={9};
                           PTG={9}];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=Text009 }

    { 11  ;1   ;Field     ;
                CaptionML=[ENU={10};
                           PTG={10}];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=LatitudeLbl }

    { 12  ;1   ;Field     ;
                CaptionML=[ENU={11};
                           PTG={11}];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=LongitudeLbl }

  }
  CODE
  {
    VAR
      Text001@1000 : TextConst 'ENU=Street (Address1);PTG=Rua (Endere�o1)';
      Text002@1001 : TextConst 'ENU=City;PTG=Cidade';
      Text003@1002 : TextConst 'ENU=State (County);PTG=Estado (Regi�o)';
      Text004@1003 : TextConst 'ENU=Post Code/ZIP Code;PTG=C�d. Postal/CEP';
      Text005@1004 : TextConst 'ENU=Country/Region Code;PTG=C�digo Pa�s/Regi�o';
      Text006@1008 : TextConst 'ENU=Country/Region Name;PTG=C�d. Pa�s/Regi�o';
      Text007@1005 : TextConst 'ENU=Culture Information, e.g., en-us;PTG=Informa��o Cultural, ex., pt-pt';
      Text008@1006 : TextConst 'ENU=Distance in (Miles/Kilometers);PTG=Dist�ncia Em (Milhas/Quil�metros)';
      Text009@1007 : TextConst 'ENU=Route (Quickest/Shortest);PTG=Rota (Mais R�pida/Mais Curta)';
      LatitudeLbl@1009 : TextConst 'ENU=GPS Latitude;PTG=Latitude GPS';
      LongitudeLbl@1010 : TextConst 'ENU=GPS Longitude;PTG=Longitude GPS';

    BEGIN
    END.
  }
}

