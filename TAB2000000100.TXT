OBJECT Table 2000000100 Debugger Breakpoint
{
  OBJECT-PROPERTIES
  {
    Date=13/04/17;
    Time=14:36:26;
    Version List=NAVW110.0;
  }
  PROPERTIES
  {
    DataPerCompany=No;
    CaptionML=[ENU=Debugger Breakpoint;
               PTG=Debugger Breakpoint];
  }
  FIELDS
  {
    { 1   ;   ;Breakpoint ID       ;Integer       ;AutoIncrement=Yes;
                                                   CaptionML=[ENU=Breakpoint ID;
                                                              PTG=ID Breakpoint] }
    { 7   ;   ;Object Type         ;Option        ;InitValue=Page;
                                                   CaptionML=[ENU=Object Type;
                                                              PTG=Tipo Objeto];
                                                   OptionCaptionML=[ENU=,Table,,Report,,Codeunit,XMLport,,Page,Query;
                                                                    PTG=,Tabela,,Mapa,,Codeunit,XMLport,,Page,Query];
                                                   OptionString=,Table,,Report,,Codeunit,XMLport,,Page,Query }
    { 9   ;   ;Object ID           ;Integer       ;TableRelation=AllObjWithCaption."Object ID" WHERE (Object Type=FIELD(Object Type));
                                                   CaptionML=[ENU=Object ID;
                                                              PTG=ID Objeto] }
    { 10  ;   ;Object Name         ;Text30        ;FieldClass=FlowField;
                                                   CalcFormula=Lookup(AllObjWithCaption."Object Name" WHERE (Object Type=FIELD(Object Type),
                                                                                                             Object ID=FIELD(Object ID)));
                                                   CaptionML=[ENU=Object Name;
                                                              PTG=Nome Objeto] }
    { 11  ;   ;Line No.            ;Integer       ;CaptionML=[ENU=Line No.;
                                                              PTG=N� Linha] }
    { 13  ;   ;Column No.          ;Integer       ;CaptionML=[ENU=Column No.;
                                                              PTG=N� Coluna] }
    { 15  ;   ;End Line No.        ;Integer       ;CaptionML=[ENU=End Line No.;
                                                              PTG=N� Fim Linha] }
    { 17  ;   ;End Column No.      ;Integer       ;CaptionML=[ENU=End Column No.;
                                                              PTG=N� Fim Coluna] }
    { 19  ;   ;Function Name       ;Text128       ;CaptionML=[ENU=Function Name;
                                                              PTG=Nome Fun��o] }
    { 21  ;   ;Enabled             ;Boolean       ;InitValue=Yes;
                                                   CaptionML=[ENU=Enabled;
                                                              PTG=Ativo] }
    { 23  ;   ;Condition           ;Text250       ;CaptionML=[ENU=Condition;
                                                              PTG=Condi��o] }
    { 27  ;   ;Session Breakpoint ID;BigInteger   ;CaptionML=[ENU=Session Breakpoint ID;
                                                              PTG=ID Sess�o Breakpoint] }
    { 29  ;   ;Relative Line No.   ;Integer       ;CaptionML=[ENU=Relative Line No.;
                                                              PTG=N� Linha Relac.] }
    { 31  ;   ;Relative End Line No.;Integer      ;CaptionML=[ENU=Relative End Line No.;
                                                              PTG=N� Fim Linha Relac.] }
    { 33  ;   ;Function ID         ;Integer       ;CaptionML=[ENU=Function ID;
                                                              PTG=ID Fun��o] }
  }
  KEYS
  {
    {    ;Breakpoint ID                           ;Clustered=Yes }
    {    ;Object Type,Object ID,Line No.,Column No. }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    END.
  }
}

