OBJECT Table 454 Approval Entry
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0;
  }
  PROPERTIES
  {
    OnModify=BEGIN
               "Last Date-Time Modified" := CREATEDATETIME(TODAY,TIME);
               "Last Modified By User ID" := USERID;
             END;

    CaptionML=[ENU=Approval Entry;
               PTG=Mov. Aprova��o];
  }
  FIELDS
  {
    { 1   ;   ;Table ID            ;Integer       ;CaptionML=[ENU=Table ID;
                                                              PTG=ID Tabela] }
    { 2   ;   ;Document Type       ;Option        ;CaptionML=[ENU=Document Type;
                                                              PTG=Tipo Documento];
                                                   OptionCaptionML=[ENU="Quote,Order,Invoice,Credit Memo,Blanket Order,Return Order, ";
                                                                    PTG="Proposta,Encomenda,Fatura,Nota Cr�dito,Encomenda Aberta,Devolu��o, "];
                                                   OptionString=[Quote,Order,Invoice,Credit Memo,Blanket Order,Return Order, ] }
    { 3   ;   ;Document No.        ;Code20        ;CaptionML=[ENU=Document No.;
                                                              PTG=N� Documento] }
    { 4   ;   ;Sequence No.        ;Integer       ;CaptionML=[ENU=Sequence No.;
                                                              PTG=N� Sequ�ncia] }
    { 5   ;   ;Approval Code       ;Code20        ;CaptionML=[ENU=Approval Code;
                                                              PTG=C�digo Aprova��o] }
    { 6   ;   ;Sender ID           ;Code50        ;TableRelation=User."User Name";
                                                   OnLookup=BEGIN
                                                              UserMgt.LookupUserID("Sender ID");
                                                            END;

                                                   TestTableRelation=No;
                                                   CaptionML=[ENU=Sender ID;
                                                              PTG=ID Remetente] }
    { 7   ;   ;Salespers./Purch. Code;Code10      ;CaptionML=[ENU=Salespers./Purch. Code;
                                                              PTG=C�d. Vendedor/Comprador] }
    { 8   ;   ;Approver ID         ;Code50        ;TableRelation=User."User Name";
                                                   OnLookup=BEGIN
                                                              UserMgt.LookupUserID("Approver ID");
                                                            END;

                                                   TestTableRelation=No;
                                                   CaptionML=[ENU=Approver ID;
                                                              PTG=ID Aprovador] }
    { 9   ;   ;Status              ;Option        ;OnValidate=BEGIN
                                                                IF (xRec.Status = Status::Created) AND (Status = Status::Open) THEN
                                                                  "Date-Time Sent for Approval" := CREATEDATETIME(TODAY,TIME);
                                                              END;

                                                   CaptionML=[ENU=Status;
                                                              PTG=Estado];
                                                   OptionCaptionML=[ENU=Created,Open,Canceled,Rejected,Approved;
                                                                    PTG=Criado,Aberto,Cancelado,Rejeitado,Aprovado];
                                                   OptionString=Created,Open,Canceled,Rejected,Approved }
    { 10  ;   ;Date-Time Sent for Approval;DateTime;
                                                   CaptionML=[ENU=Date-Time Sent for Approval;
                                                              PTG=Data-Hora Envio para Aprova��o] }
    { 11  ;   ;Last Date-Time Modified;DateTime   ;CaptionML=[ENU=Last Date-Time Modified;
                                                              PTG=Data-Hora �ltima Modifica��o] }
    { 12  ;   ;Last Modified By User ID;Code50    ;TableRelation=User."User Name";
                                                   OnLookup=BEGIN
                                                              UserMgt.LookupUserID("Last Modified By User ID");
                                                            END;

                                                   TestTableRelation=No;
                                                   CaptionML=[ENU=Last Modified By User ID;
                                                              PTG=�ltima Modifica��o Por ID Utilizador] }
    { 13  ;   ;Comment             ;Boolean       ;FieldClass=FlowField;
                                                   CalcFormula=Exist("Approval Comment Line" WHERE (Table ID=FIELD(Table ID),
                                                                                                    Record ID to Approve=FIELD(Record ID to Approve),
                                                                                                    Workflow Step Instance ID=FIELD(Workflow Step Instance ID)));
                                                   CaptionML=[ENU=Comment;
                                                              PTG=Coment�rio];
                                                   Editable=No }
    { 14  ;   ;Due Date            ;Date          ;CaptionML=[ENU=Due Date;
                                                              PTG=Data Vencimento] }
    { 15  ;   ;Amount              ;Decimal       ;CaptionML=[ENU=Amount;
                                                              PTG=Valor];
                                                   AutoFormatType=1;
                                                   AutoFormatExpr="Currency Code" }
    { 16  ;   ;Amount (LCY)        ;Decimal       ;CaptionML=[ENU=Amount (LCY);
                                                              PTG=Valor (DL)];
                                                   AutoFormatType=1 }
    { 17  ;   ;Currency Code       ;Code10        ;TableRelation=Currency;
                                                   CaptionML=[ENU=Currency Code;
                                                              PTG=C�d. Divisa] }
    { 18  ;   ;Approval Type       ;Option        ;CaptionML=[ENU=Approval Type;
                                                              PTG=Tipo Aprova��o];
                                                   OptionCaptionML=[ENU=Workflow User Group,Sales Pers./Purchaser,Approver;
                                                                    PTG=Grupo Utilizador Workflow,Vendedor/Comprador,Aprovador];
                                                   OptionString=Workflow User Group,Sales Pers./Purchaser,Approver }
    { 19  ;   ;Limit Type          ;Option        ;CaptionML=[ENU=Limit Type;
                                                              PTG=Tipo Limite];
                                                   OptionCaptionML=[ENU=Approval Limits,Credit Limits,Request Limits,No Limits;
                                                                    PTG=Limites Aprova��o,Limites Cr�dito,Limites Pedido,Sem Limites];
                                                   OptionString=Approval Limits,Credit Limits,Request Limits,No Limits }
    { 20  ;   ;Available Credit Limit (LCY);Decimal;
                                                   CaptionML=[ENU=Available Credit Limit (LCY);
                                                              PTG=Limite Cr�dito Dispon�vel (DL)] }
    { 21  ;   ;Pending Approvals   ;Integer       ;FieldClass=FlowField;
                                                   CalcFormula=Count("Approval Entry" WHERE (Record ID to Approve=FIELD(Record ID to Approve),
                                                                                             Status=FILTER(Created|Open),
                                                                                             Workflow Step Instance ID=FIELD(Workflow Step Instance ID)));
                                                   CaptionML=[ENU=Pending Approvals;
                                                              PTG=Aprova��es Pendentes] }
    { 22  ;   ;Record ID to Approve;RecordID      ;CaptionML=[ENU=Record ID to Approve;
                                                              PTG=ID Registo a Aprovar] }
    { 23  ;   ;Delegation Date Formula;DateFormula;CaptionML=[ENU=Delegation Date Formula;
                                                              PTG=Delega��o F�rmula Data] }
    { 26  ;   ;Number of Approved Requests;Integer;FieldClass=FlowField;
                                                   CalcFormula=Count("Approval Entry" WHERE (Record ID to Approve=FIELD(Record ID to Approve),
                                                                                             Status=FILTER(Approved),
                                                                                             Workflow Step Instance ID=FIELD(Workflow Step Instance ID)));
                                                   CaptionML=[ENU=Number of Approved Requests;
                                                              PTG=N�mero de Pedidos Aprovados] }
    { 27  ;   ;Number of Rejected Requests;Integer;FieldClass=FlowField;
                                                   CalcFormula=Count("Approval Entry" WHERE (Record ID to Approve=FIELD(Record ID to Approve),
                                                                                             Status=FILTER(Rejected),
                                                                                             Workflow Step Instance ID=FIELD(Workflow Step Instance ID)));
                                                   CaptionML=[ENU=Number of Rejected Requests;
                                                              PTG=N�mero de Pedidos Rejeitados] }
    { 29  ;   ;Entry No.           ;Integer       ;AutoIncrement=Yes;
                                                   CaptionML=[ENU=Entry No.;
                                                              PTG=N� Mov.] }
    { 30  ;   ;Workflow Step Instance ID;GUID     ;CaptionML=[ENU=Workflow Step Instance ID;
                                                              PTG=ID Inst�ncia Etapa Workflow] }
    { 31  ;   ;Related to Change   ;Boolean       ;FieldClass=FlowField;
                                                   CalcFormula=Exist("Workflow - Record Change" WHERE (Workflow Step Instance ID=FIELD(Workflow Step Instance ID),
                                                                                                       Record ID=FIELD(Record ID to Approve)));
                                                   CaptionML=[ENU=Related to Change;
                                                              PTG=Relacionado com Altera��o] }
  }
  KEYS
  {
    {    ;Entry No.                               ;Clustered=Yes }
    {    ;Record ID to Approve,Workflow Step Instance ID,Sequence No. }
    {    ;Table ID,Document Type,Document No.,Sequence No.,Record ID to Approve }
    {    ;Approver ID,Status                       }
    {    ;Sender ID                                }
    {    ;Due Date                                 }
    {    ;Table ID,Record ID to Approve,Status,Workflow Step Instance ID,Sequence No. }
  }
  FIELDGROUPS
  {
  }
  CODE
  {
    VAR
      UserMgt@1000 : Codeunit 418;
      PageManagement@1001 : Codeunit 700;
      RecNotExistTxt@1002 : TextConst 'ENU=The record does not exist.;PTG=O registo n�o existe.';
      ChangeRecordDetailsTxt@1003 : TextConst '@@@="Prefix = Record information %1 = field caption %2 = old value %3 = new value. Example: Customer 123455; Credit Limit changed from 100.00 to 200.00";ENU="; %1 changed from %2 to %3";PTG="; %1 alterado de %2 para %3"';

    PROCEDURE ShowRecord@1();
    VAR
      RecRef@1000 : RecordRef;
    BEGIN
      IF NOT RecRef.GET("Record ID to Approve") THEN
        EXIT;
      RecRef.SETRECFILTER;
      PageManagement.PageRun(RecRef);
    END;

    PROCEDURE RecordCaption@4() : Text;
    VAR
      Object@1000 : Record 2000000001;
      RecRef@1002 : RecordRef;
      PageNo@1001 : Integer;
    BEGIN
      IF NOT RecRef.GET("Record ID to Approve") THEN
        EXIT;
      PageNo := PageManagement.GetPageID(RecRef);
      IF PageNo = 0 THEN
        EXIT;
      Object.GET(Object.Type::Page,'',PageNo);
      Object.CALCFIELDS(Caption);
      EXIT(STRSUBSTNO('%1 %2',Object.Caption,"Document No."));
    END;

    PROCEDURE RecordDetails@3() : Text;
    VAR
      SalesHeader@1001 : Record 36;
      PurchHeader@1000 : Record 38;
      RecRef@1002 : RecordRef;
      ChangeRecordDetails@1003 : Text;
    BEGIN
      IF NOT RecRef.GET("Record ID to Approve") THEN
        EXIT(RecNotExistTxt);

      ChangeRecordDetails := GetChangeRecordDetails;

      CASE RecRef.NUMBER OF
        DATABASE::"Sales Header":
          BEGIN
            RecRef.SETTABLE(SalesHeader);
            SalesHeader.CALCFIELDS(Amount);
            EXIT(STRSUBSTNO('%1 ; %2: %3',SalesHeader."Sell-to Customer Name",
                SalesHeader.FIELDCAPTION(Amount),SalesHeader.Amount));
          END;
        DATABASE::"Purchase Header":
          BEGIN
            RecRef.SETTABLE(PurchHeader);
            PurchHeader.CALCFIELDS(Amount);
            EXIT(STRSUBSTNO('%1 ; %2: %3',PurchHeader."Buy-from Vendor Name",
                PurchHeader.FIELDCAPTION(Amount),PurchHeader.Amount));
          END;
        ELSE
          EXIT(FORMAT("Record ID to Approve",0,1) + ChangeRecordDetails);
      END;
    END;

    PROCEDURE IsOverdue@5() : Boolean;
    BEGIN
      EXIT((Status IN [Status::Created,Status::Open]) AND ("Due Date" < TODAY));
    END;

    PROCEDURE GetCustVendorDetails@6(VAR CustVendorNo@1002 : Code[20];VAR CustVendorName@1003 : Text[50]);
    VAR
      PurchaseHeader@1000 : Record 38;
      SalesHeader@1001 : Record 36;
      Customer@1005 : Record 18;
      RecRef@1004 : RecordRef;
    BEGIN
      IF NOT RecRef.GET("Record ID to Approve") THEN
        EXIT;

      CASE "Table ID" OF
        DATABASE::"Purchase Header":
          BEGIN
            RecRef.SETTABLE(PurchaseHeader);
            CustVendorNo := PurchaseHeader."Pay-to Vendor No.";
            CustVendorName := PurchaseHeader."Pay-to Name";
          END;
        DATABASE::"Sales Header":
          BEGIN
            RecRef.SETTABLE(SalesHeader);
            CustVendorNo := SalesHeader."Bill-to Customer No.";
            CustVendorName := SalesHeader."Bill-to Name";
          END;
        DATABASE::Customer:
          BEGIN
            RecRef.SETTABLE(Customer);
            CustVendorNo := Customer."No.";
            CustVendorName := Customer.Name;
          END;
      END;
    END;

    PROCEDURE GetChangeRecordDetails@2() ChangeDetails : Text;
    VAR
      WorkflowRecordChange@1000 : Record 1525;
      OldValue@1001 : Text;
      NewValue@1002 : Text;
    BEGIN
      WorkflowRecordChange.SETRANGE("Record ID","Record ID to Approve");
      WorkflowRecordChange.SETRANGE("Workflow Step Instance ID","Workflow Step Instance ID");

      IF WorkflowRecordChange.FINDSET THEN
        REPEAT
          WorkflowRecordChange.CALCFIELDS("Field Caption");
          NewValue := WorkflowRecordChange.GetFormattedNewValue(TRUE);
          OldValue := WorkflowRecordChange.GetFormattedOldValue(TRUE);
          ChangeDetails += STRSUBSTNO(ChangeRecordDetailsTxt,WorkflowRecordChange."Field Caption",
              OldValue,NewValue);
        UNTIL WorkflowRecordChange.NEXT = 0;
    END;

    PROCEDURE CanCurrentUserEdit@7() : Boolean;
    VAR
      UserSetup@1000 : Record 91;
    BEGIN
      IF NOT UserSetup.GET(USERID) THEN
        EXIT(FALSE);
      EXIT((UserSetup."User ID" IN ["Sender ID","Approver ID"]) OR UserSetup."Approval Administrator");
    END;

    PROCEDURE MarkAllWhereUserisApproverOrSender@9();
    VAR
      UserSetup@1000 : Record 91;
    BEGIN
      IF UserSetup.GET(USERID) AND UserSetup."Approval Administrator" THEN
        EXIT;
      FILTERGROUP(-1); // Used to support the cross-column search
      SETRANGE("Approver ID",USERID);
      SETRANGE("Sender ID",USERID);
      IF FINDSET THEN
        REPEAT
          MARK(TRUE);
        UNTIL NEXT = 0;
      MARKEDONLY(TRUE);
    END;

    BEGIN
    END.
  }
}

