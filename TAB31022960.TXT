OBJECT Table 31022960 BP Ledger Entry
{
  OBJECT-PROPERTIES
  {
    Date=01/02/15;
    Time=13:00:00;
    Version List=NAVPTSS81.00;
  }
  PROPERTIES
  {
    OnDelete=BEGIN
               TESTFIELD(Status,Status::" ");
             END;

    CaptionML=[ENU=BP Ledger Entry;
               PTG=Movimentos Banco Portugal];
    LookupPageID=Page31023036;
    DrillDownPageID=Page31023036;
  }
  FIELDS
  {
    { 1   ;   ;Entry No.           ;Integer       ;CaptionML=[ENU=Entry No.;
                                                              PTG=N� Mov.] }
    { 2   ;   ;Ledger Entry No.    ;Integer       ;TableRelation=IF (Ledger Entry Type=FILTER(Customer)) "Cust. Ledger Entry"."Entry No."
                                                                 ELSE IF (Ledger Entry Type=FILTER(Vendor)) "Vendor Ledger Entry"."Entry No."
                                                                 ELSE IF (Ledger Entry Type=FILTER(Bank Account)) "Bank Account Ledger Entry"."Entry No.";
                                                   CaptionML=[ENU=Ledger Entry No.;
                                                              PTG=N� Mov.] }
    { 3   ;   ;Year                ;Integer       ;CaptionML=[ENU=Year;
                                                              PTG=Ano];
                                                   MinValue=2011;
                                                   MaxValue=9999 }
    { 4   ;   ;Month               ;Integer       ;CaptionML=[ENU=Month;
                                                              PTG=M�s];
                                                   MinValue=1;
                                                   MaxValue=12 }
    { 5   ;   ;Finance Institution ID;Text4       ;CaptionML=[ENU=Finance Institution ID;
                                                              PTG=Identifica��o Institui��o Financeira] }
    { 6   ;   ;Company ID          ;Text9         ;CaptionML=[ENU=Company ID;
                                                              PTG=Identifica��o Empresa] }
    { 7   ;   ;Identification Code ;Text20        ;CaptionML=[ENU=Identification Code;
                                                              PTG=C�digo Identifica��o] }
    { 8   ;   ;Reference Date      ;Integer       ;CaptionML=[ENU=Reference Date;
                                                              PTG=Data Refer�ncia];
                                                   MinValue=20110101;
                                                   MaxValue=21001231 }
    { 9   ;   ;Record Nature       ;Text1         ;CaptionML=[ENU=Record Nature;
                                                              PTG=Natureza Registo] }
    { 10  ;   ;NPC 2nd Intervener  ;Integer       ;CaptionML=[ENU=NPC 2nd Intervener;
                                                              PTG=NPC 2� Interveniente];
                                                   BlankZero=Yes }
    { 11  ;   ;Amount              ;Decimal       ;CaptionML=[ENU=Amount;
                                                              PTG=Valor] }
    { 12  ;   ;Currency Code       ;Code3         ;CaptionML=[ENU=Currency Code;
                                                              PTG=C�digo Divisa] }
    { 13  ;   ;Statistic Code      ;Code5         ;TableRelation="BP Statistic";
                                                   CaptionML=[ENU=Statistic Code;
                                                              PTG=C�digo Classifica��o Estat�stica] }
    { 14  ;   ;Amount Type         ;Code1         ;CaptionML=[ENU=Amount Type;
                                                              PTG=Tipo Valor] }
    { 15  ;   ;Account Type        ;Code1         ;TableRelation="BP Account Type";
                                                   CaptionML=[ENU=Account Type;
                                                              PTG=Tipo Conta] }
    { 16  ;   ;IF Code             ;Code10        ;CaptionML=[ENU=IF Code;
                                                              PTG=C�digo ID da Entidade] }
    { 17  ;   ;Foreign Country Code;Code3         ;TableRelation="BP Territory";
                                                   CaptionML=[ENU=Country Code;
                                                              PTG=Pa�s da Conta Externa] }
    { 18  ;   ;Counterpart Country Code;Code3     ;TableRelation="BP Territory";
                                                   CaptionML=[ENU=Counterpart Country Code;
                                                              PTG=Pa�s Entidade Contraparte] }
    { 19  ;   ;Active Country Code ;Code3         ;TableRelation="BP Territory";
                                                   CaptionML=[ENU=Active Country Code;
                                                              PTG=Pa�s Entidade Ativo Financeiro] }
    { 20  ;   ;Due Date            ;Integer       ;CaptionML=[ENU=Due Date;
                                                              PTG=Data Vencimento];
                                                   MinValue=20110101;
                                                   MaxValue=21001231 }
    { 21  ;   ;Counterpart ID      ;Text50        ;CaptionML=[ENU=Counterpart ID;
                                                              PTG=ID Entidade Ativo Contraparte] }
    { 22  ;   ;Observations        ;Text50        ;CaptionML=[ENU=Observations;
                                                              PTG=Observa��es] }
    { 23  ;   ;Ledger Entry Type   ;Option        ;CaptionML=[ENU=Ledger Entry Type;
                                                              PTG=Tipo Mov.];
                                                   OptionCaptionML=[ENU=Customer,Vendor,Bank Account;
                                                                    PTG=Cliente,Fornecedor,Conta Banc�ria];
                                                   OptionString=Customer,Vendor,Bank Account }
    { 24  ;   ;Status              ;Option        ;CaptionML=[ENU=Status;
                                                              PTG=Estado];
                                                   OptionCaptionML=[ENU=" ,Exported";
                                                                    PTG=" ,Exportado"];
                                                   OptionString=[ ,Exported] }
    { 25  ;   ;Document No.        ;Code20        ;CaptionML=[ENU=Document No.;
                                                              PTG=N� Documento];
                                                   Editable=No }
    { 26  ;   ;Description         ;Text50        ;CaptionML=[ENU=Description;
                                                              PTG=Descri��o];
                                                   Editable=No }
  }
  KEYS
  {
    {    ;Entry No.                               ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {
    VAR
      Text31022890@1000000000 : TextConst 'ENU=Export ended: %1.;PTG=Exporta��o terminou: %1.';
      FileName@1000000001 : Text[1024];
      CompanyInfo@1000000002 : Record 79;
      GeneralLedgerSetup@1000000003 : Record 98;

    PROCEDURE InitEntry@1000000000();
    VAR
      LastIntegrationBankPortugal@1000000000 : Record 31022960;
    BEGIN
      LastIntegrationBankPortugal.RESET;
      IF LastIntegrationBankPortugal.FINDLAST THEN
        "Entry No." := LastIntegrationBankPortugal."Entry No."+1
      ELSE
        "Entry No." := 1;
    END;

    PROCEDURE ExportXML@1000000001(Period@1000000004 : Code[6]);
    VAR
      IntegrationBankPortugal@1000000003 : Record 31022960;
      XMLIntegrationBankPortugal@1000000002 : XMLport 31022901;
      XMLFile@1000000001 : File;
      XMLStream@1000000000 : OutStream;
      FileManagement@1000000005 : Codeunit 419;
      ServerFileName@1000000006 : Text;
    BEGIN
      CLEAR(XMLIntegrationBankPortugal);
      CLEAR(XMLStream);
      CLEAR(XMLFile);
      CompanyInfo.GET;
      GeneralLedgerSetup.GET;

      ServerFileName := FileManagement.ServerTempFileName('xml');

      FileName := GeneralLedgerSetup."BP Folder"+'BOP_COPE.'+CompanyInfo."VAT Registration No."+'.'+Period+'.'+
        FORMAT(CURRENTDATETIME,0,'<Year4><Month,2><Day,2><Hour,2><Minute,2><Second,2>')+'.xml';

      XMLFile.CREATE(ServerFileName);
      XMLFile.CREATEOUTSTREAM(XMLStream);
      XMLIntegrationBankPortugal.SETDESTINATION(XMLStream);
      XMLIntegrationBankPortugal.EXPORT;
      XMLFile.CLOSE;
      XMLIntegrationBankPortugal.ClearNodes(ServerFileName);
      FileManagement.DownloadToFile(ServerFileName,FileName);
      MESSAGE(Text31022890,FileName);
    END;

    BEGIN
    END.
  }
}

