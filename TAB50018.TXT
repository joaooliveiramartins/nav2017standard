OBJECT Table 50018 Conciliation Lines A
{
  OBJECT-PROPERTIES
  {
    Date=28/08/06;
    Time=16:03:43;
    Modified=Yes;
    Version List=SGG07.00;
  }
  PROPERTIES
  {
    OnInsert=BEGIN
               ERROR(Text50000);
             END;

    OnDelete=BEGIN
               ERROR(Text50000);
             END;

    OnRename=BEGIN
               ERROR(Text50000);
             END;

    CaptionML=[ENU=Conciliation Lines A;
               PTG=Linhas concilia��o A];
  }
  FIELDS
  {
    { 1   ;   ;Type                ;Option        ;CaptionML=[ENU=Type;
                                                              PTG=Tipo];
                                                   OptionString=Bancos,Cuentas,Ficheros,Mixto;
                                                   Editable=No }
    { 2   ;   ;Code                ;Code10        ;TableRelation=Conciliation.Code WHERE (Type=FIELD(Type));
                                                   CaptionML=[ENU=Code;
                                                              PTG=C�digo];
                                                   Editable=No }
    { 3   ;   ;Line No.            ;Integer       ;CaptionML=[ENU=Line No.;
                                                              PTG=N� linha];
                                                   Editable=No }
    { 4   ;   ;Amount              ;Decimal       ;CaptionML=[ENU=Amount;
                                                              PTG=Valor];
                                                   Editable=No }
    { 5   ;   ;Date                ;Date          ;CaptionML=[ENU=Date;
                                                              PTG=Data];
                                                   Editable=No }
    { 6   ;   ;Value Date          ;Date          ;CaptionML=[ENU=Value Date;
                                                              PTG=Data valor];
                                                   Editable=Yes }
    { 7   ;   ;Description         ;Text100       ;CaptionML=[ENU=Description;
                                                              PTG=Descri��o];
                                                   Editable=No }
    { 8   ;   ;Dossier             ;Text30        ;CaptionML=[ENU=Dossier;
                                                              PTG=Expediente];
                                                   Editable=No }
    { 9   ;   ;Document No.        ;Code20        ;CaptionML=[ENU=Document No.;
                                                              PTG=N� documento];
                                                   Editable=No }
    { 18  ;   ;Entry no.           ;Integer       ;CaptionML=[ENU=Entry no.;
                                                              PTG=N� Mov.];
                                                   Editable=No }
    { 19  ;   ;Counter             ;Decimal       ;InitValue=1;
                                                   CaptionML=[ENU=Counter;
                                                              PTG=Contador];
                                                   Editable=No }
    { 20  ;   ;Closed by Line No.  ;Integer       ;CaptionML=[ENU=Closed by Line No.;
                                                              PTG=Fechado por linha];
                                                   Editable=No }
    { 21  ;   ;Positive            ;Boolean       ;CaptionML=[ENU=Positive;
                                                              PTG=Positivo];
                                                   Editable=No }
    { 22  ;   ;Open                ;Boolean       ;CaptionML=[ENU=Open;
                                                              PTG=Pendente];
                                                   Editable=No }
    { 23  ;   ;Conciliate          ;Boolean       ;OnValidate=BEGIN
                                                                IF Conciliate = TRUE THEN BEGIN
                                                                  "Linhas B".SETRANGE(Type,"Linhas B".Type::Bancos);
                                                                  "Linhas B".SETFILTER(Code,Code);
                                                                  "Linhas B".SETRANGE(Posted,FALSE);
                                                                  "Linhas B".SETRANGE(Open,TRUE);
                                                                  "Linhas B".SETRANGE(Amount,Amount);
                                                                  IF "Linhas B".FIND('-') THEN BEGIN
                                                                    "Linhas B".Conciliate := TRUE;
                                                                    "Linhas B"."Relation A" := "Line No.";
                                                                    "Relation B" := "Linhas B"."Line No.";
                                                                    "Linhas B".MODIFY;
                                                                    MODIFY;
                                                                  END;
                                                                END ELSE
                                                                  "Relation B" := 0;

                                                                {
                                                                ELSE BEGIN
                                                                  "Linhas B".SETRANGE(Tipo,"Linhas B".Tipo::Bancos);
                                                                  "Linhas B".SETFILTER(C�digo,C�digo);
                                                                  "Linhas B".SETRANGE(Registada,FALSE);
                                                                  "Linhas B".SETRANGE(Pendente,TRUE);
                                                                  "Linhas B".SETRANGE("Rela��o A","N� linha");
                                                                  IF "Linhas B".FIND('-') THEN BEGIN
                                                                    "Linhas B".Conciliar := FALSE;
                                                                    "Linhas B"."Rela��o A" := 0;
                                                                    "Rela��o B" := 0;
                                                                    "Linhas B".MODIFY;
                                                                    MODIFY;
                                                                  END;
                                                                END;
                                                                }
                                                              END;

                                                   CaptionML=[ENU=Conciliate;
                                                              PTG=Conciliar] }
    { 100 ;   ;Account No.         ;Code20        ;TableRelation="G/L Account";
                                                   CaptionML=[ENU=Account No.;
                                                              PTG=N� conta] }
    { 101 ;   ;Posted              ;Boolean       ;CaptionML=[ENU=Posted;
                                                              PTG=Registada] }
    { 102 ;   ;Relation B          ;Integer       ;CaptionML=[ENU=Relation B;
                                                              PTG=Rela��o B] }
  }
  KEYS
  {
    {    ;Type,Code,Line No.                      ;Clustered=Yes }
    {    ;Type,Code,Closed by Line No.,Date       ;SumIndexFields=Amount,Counter }
    {    ;Type,Code,Open,Conciliate               ;SumIndexFields=Amount,Counter }
    {    ;Type,Code,Open,Date,Amount               }
  }
  FIELDGROUPS
  {
  }
  CODE
  {
    VAR
      "Linhas B"@1000000000 : Record 50019;
      Text50000@1000000001 : TextConst 'ENU=It''s not possible to delete lines.;PTG=N�o � possivel anular as linhas.';

    BEGIN
    {
      SGG07.00 PFILIPE 28-08-06 Criada nova tabela
    }
    END.
  }
}

