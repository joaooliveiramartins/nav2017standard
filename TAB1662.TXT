OBJECT Table 1662 Payroll Import Buffer
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Payroll Import Buffer;
               PTG=Buffer Importa��o Folha de Pagamentos];
  }
  FIELDS
  {
    { 1   ;   ;Entry No.           ;Integer       ;AutoIncrement=Yes;
                                                   CaptionML=[ENU=Entry No.;
                                                              PTG=N� Movimento] }
    { 10  ;   ;Transaction date    ;Date          ;CaptionML=[ENU=Transaction date;
                                                              PTG=Data Transa��o] }
    { 11  ;   ;Account No.         ;Code20        ;CaptionML=[ENU=Account No.;
                                                              PTG=N� Conta] }
    { 12  ;   ;Amount              ;Decimal       ;CaptionML=[ENU=Amount;
                                                              PTG=Valor] }
    { 13  ;   ;Description         ;Text50        ;CaptionML=[ENU=Description;
                                                              PTG=Descri��o] }
  }
  KEYS
  {
    {    ;Entry No.                               ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    END.
  }
}

