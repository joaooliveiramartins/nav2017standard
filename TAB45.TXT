OBJECT Table 45 G/L Register
{
  OBJECT-PROPERTIES
  {
    Date=15/06/17;
    Time=14:03:14;
    Modified=Yes;
    Version List=NAVW17.00,NAVPTSS81.00,SGG;
  }
  PROPERTIES
  {
    Permissions=TableData 45=rim;
    CaptionML=[ENU=G/L Register;
               PTG=Reg. Movs. Contabilidade];
    LookupPageID=Page116;
  }
  FIELDS
  {
    { 1   ;   ;Transaction No.     ;Integer       ;CaptionML=[ENU=Transaction No.;
                                                              PTG=N� Transa��o];
                                                   Description=soft }
    { 2   ;   ;From Entry No.      ;Integer       ;TableRelation="G/L Entry";
                                                   TestTableRelation=No;
                                                   CaptionML=[ENU=From Entry No.;
                                                              PTG=De N� Mov.] }
    { 3   ;   ;To Entry No.        ;Integer       ;TableRelation="G/L Entry";
                                                   TestTableRelation=No;
                                                   CaptionML=[ENU=To Entry No.;
                                                              PTG=At� N� Mov.] }
    { 4   ;   ;Creation Date       ;Date          ;CaptionML=[ENU=Creation Date;
                                                              PTG=Data Cria��o] }
    { 5   ;   ;Source Code         ;Code10        ;TableRelation="Source Code";
                                                   CaptionML=[ENU=Source Code;
                                                              PTG=C�d. Origem] }
    { 6   ;   ;User ID             ;Code50        ;TableRelation=User."User Name";
                                                   OnLookup=VAR
                                                              UserMgt@1000 : Codeunit 418;
                                                            BEGIN
                                                              UserMgt.LookupUserID("User ID");
                                                            END;

                                                   TestTableRelation=No;
                                                   CaptionML=[ENU=User ID;
                                                              PTG=ID Utilizador] }
    { 7   ;   ;Journal Batch Name  ;Code10        ;CaptionML=[ENU=Journal Batch Name;
                                                              PTG=Nome Sec��o Di�rio] }
    { 8   ;   ;From VAT Entry No.  ;Integer       ;TableRelation="VAT Entry";
                                                   TestTableRelation=No;
                                                   CaptionML=[ENU=From VAT Entry No.;
                                                              PTG=De N� Mov. IVA] }
    { 9   ;   ;To VAT Entry No.    ;Integer       ;TableRelation="VAT Entry";
                                                   TestTableRelation=No;
                                                   CaptionML=[ENU=To VAT Entry No.;
                                                              PTG=At� N� Mov. IVA] }
    { 10  ;   ;Reversed            ;Boolean       ;CaptionML=[ENU=Reversed;
                                                              PTG=Estornado] }
    { 50002;  ;Reversed Entry No.  ;Integer       ;CaptionML=[ENU=Reversed Entry No.;
                                                              PTG=Movimento relacionado] }
    { 60000;  ;First Doc           ;Code20        ;FieldClass=FlowField;
                                                   CalcFormula=Lookup("G/L Entry"."Document No." WHERE (Entry No.=FIELD(From Entry No.)));
                                                   CaptionML=[ENU=1st Doc.;
                                                              PTG=1� Doc.] }
    { 31022890;;Period Trans. No.  ;Integer       ;CaptionML=[ENU=Period Trans. No.;
                                                              PTG=N� Transa��o Per�odo];
                                                   BlankZero=Yes }
    { 31022891;;Posting Date       ;Date          ;CaptionML=[ENU=Posting Date;
                                                              PTG=Data Registo];
                                                   ClosingDates=Yes }
    { 31022893;;Creation Time      ;Time          ;CaptionML=[ENU=Creation Time;
                                                              PTG=Hora Cria��o];
                                                   Editable=No }
  }
  KEYS
  {
    {    ;Transaction No.                         ;Clustered=Yes }
    {    ;Creation Date                            }
    {    ;Source Code,Journal Batch Name,Creation Date }
    {    ;Posting Date                             }
    {    ;Posting Date,Period Trans. No.           }
  }
  FIELDGROUPS
  {
    { 1   ;DropDown            ;Transaction No.,From Entry No.,To Entry No.,Creation Date,Source Code }
  }
  CODE
  {

    LOCAL PROCEDURE "//sgg"@31022890();
    BEGIN
    END;

    PROCEDURE CheckLastTransactionNo@1000000006();
    VAR
      rec45@1000000001 : Record 45;
      rec45_NewEntries@1000000013 : Record 45;
      lTransaction@1000000002 : Integer;
      FirstTransaction@1000000014 : Integer;
      i@1000000015 : Integer;
      FromGLEntry@1000000018 : Integer;
      ToGLEntry@1000000019 : Integer;
      FromVATEntry@1000000021 : Integer;
      ToVATEntry@1000000020 : Integer;
      MaxGLEntry@1000000016 : Integer;
      MaxVATEntry@1000000017 : Integer;
      rec17@1000000004 : Record 17;
      rec21@1000000003 : Record 21;
      rec25@1000000005 : Record 25;
      rec254@1000000000 : Record 254;
      rec271@1000000006 : Record 271;
      rec379@1000000007 : Record 379;
      rec380@1000000008 : Record 380;
      rec5601@1000000009 : Record 5601;
      rec50004@1000000010 : Record 50004;
      rec50010@1000000011 : Record 50010;
      rec50027@1000000012 : Record 50027;
      GenJnlLinePosted@1000000022 : Record 50027;
      LastDoc@1000000023 : Code[20];
      LastDate@1000000024 : Date;
    BEGIN
      rec45.LOCKTABLE;
      IF NOT rec45.FIND('+') THEN EXIT;
      FirstTransaction := rec45."Transaction No.";
      MaxGLEntry := rec45."From Entry No." - 1;
      MaxVATEntry := rec45."From VAT Entry No." - 1;


      WITH rec17 DO BEGIN
        SETCURRENTKEY("Transaction No.");
        SETFILTER("Transaction No.", '>%1', FirstTransaction);
        IF NOT FIND('-') THEN EXIT;

        FIND('+');
        lTransaction := rec17."Transaction No.";

        SETCURRENTKEY("Entry No.");
        FOR i:= FirstTransaction TO lTransaction DO BEGIN
          IF i <> FirstTransaction THEN BEGIN
            rec45_NewEntries.TRANSFERFIELDS(rec45);
            rec45_NewEntries."Transaction No." := i;
            rec45_NewEntries.INSERT;
          END ELSE BEGIN
            rec45_NewEntries.GET(i);
          END;


          rec17.SETRANGE("Transaction No.", i);
          IF FINDFIRST() THEN BEGIN
            FromGLEntry := "Entry No.";
            FINDLAST();
            ToGLEntry := "Entry No.";
            MaxGLEntry := ToGLEntry;
          END ELSE BEGIN
            FromGLEntry := MaxGLEntry + 1;
            ToGLEntry := MaxGLEntry;
          END;

          rec254.SETRANGE("Transaction No.", i);
          IF rec254.FINDFIRST() THEN BEGIN
            FromVATEntry := rec254."Entry No.";
            FINDLAST();
            ToVATEntry := rec254."Entry No.";
            MaxVATEntry := ToVATEntry;
          END ELSE BEGIN
            FromVATEntry := MaxVATEntry + 1;
            ToVATEntry := MaxVATEntry;
          END;

          rec45_NewEntries."From Entry No."  := FromGLEntry;
          rec45_NewEntries."To Entry No."    := ToGLEntry;
          rec45_NewEntries."From VAT Entry No." := FromVATEntry;
          rec45_NewEntries."To VAT Entry No."   := ToVATEntry;
          rec45_NewEntries.MODIFY;
        END;

        RESET;
        SETCURRENTKEY("Entry No.");

        rec45.GET(FirstTransaction);
        rec45_NewEntries.GET(lTransaction);

        LastDoc := '';
        LastDate := 0D;
        i := 0;
        GenJnlLinePosted.RESET;
        GenJnlLinePosted.SETCURRENTKEY("Journal Template Name","Journal Batch Name","Posting Date","Document No.");
        GenJnlLinePosted.SETRANGE("Register No.", FirstTransaction);

        IF GET(rec45."From Entry No.") THEN BEGIN
          REPEAT
            IF ("Document No." <> LastDoc) OR ("Posting Date" <> LastDate) THEN BEGIN
               LastDoc  := "Document No.";
               LastDate := "Posting Date";
               GenJnlLinePosted.SETRANGE("Posting Date", "Posting Date");
               GenJnlLinePosted.SETRANGE("Document No.", "Document No.");
               IF GenJnlLinePosted.FIND('-') THEN
                 GenJnlLinePosted.MODIFYALL("Register No.", "Transaction No.");
            END;

            IF NOT (NEXT(1) = 1) THEN i:= 1;
            IF "Entry No." > rec45_NewEntries."To Entry No." THEN i:= 1;
          UNTIL i = 1;
        END;
      END;
    END;

    BEGIN
    {
      SGG 12-08-08 Foi dada a Permiss�o de Read,Insert e Modify (Licen�a EPM)
    }
    END.
  }
}

