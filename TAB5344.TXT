OBJECT Table 5344 CRM Post
{
  OBJECT-PROPERTIES
  {
    Date=24/11/16;
    Time=12:00:00;
    Version List=NAVW110.0.00.14199;
  }
  PROPERTIES
  {
    TableType=CRM;
    ExternalName=post;
    CaptionML=[ENU=CRM Post;
               PTG=Registar];
    Description=An activity feed post.;
  }
  FIELDS
  {
    { 1   ;   ;CreatedBy           ;GUID          ;TableRelation="CRM Systemuser".SystemUserId;
                                                   ExternalName=createdby;
                                                   ExternalType=Lookup;
                                                   ExternalAccess=Read;
                                                   CaptionML=[ENU=Created By;
                                                              PTG=Criado Por];
                                                   Description=Shows who created the record. }
    { 2   ;   ;CreatedByName       ;Text200       ;FieldClass=FlowField;
                                                   CalcFormula=Lookup("CRM Systemuser".FullName WHERE (SystemUserId=FIELD(CreatedBy)));
                                                   ExternalName=createdbyname;
                                                   ExternalType=String;
                                                   ExternalAccess=Read;
                                                   CaptionML=[ENU=CreatedByName;
                                                              PTG=CriadoPorNome] }
    { 3   ;   ;CreatedOn           ;DateTime      ;ExternalName=createdon;
                                                   ExternalType=DateTime;
                                                   ExternalAccess=Read;
                                                   CaptionML=[ENU=Created On;
                                                              PTG=Criado Em];
                                                   Description=Shows the date and time when the record was created. The date and time are displayed in the time zone selected in Microsoft Dynamics CRM options. }
    { 4   ;   ;CreatedOnBehalfBy   ;GUID          ;TableRelation="CRM Systemuser".SystemUserId;
                                                   ExternalName=createdonbehalfby;
                                                   ExternalType=Lookup;
                                                   ExternalAccess=Read;
                                                   CaptionML=[ENU=Created By (Delegate);
                                                              PTG=Criado Por (Delegado)];
                                                   Description=Shows who created the record on behalf of another user. }
    { 5   ;   ;CreatedOnBehalfByName;Text200      ;FieldClass=FlowField;
                                                   CalcFormula=Lookup("CRM Systemuser".FullName WHERE (SystemUserId=FIELD(CreatedOnBehalfBy)));
                                                   ExternalName=createdonbehalfbyname;
                                                   ExternalType=String;
                                                   ExternalAccess=Read;
                                                   CaptionML=[ENU=CreatedOnBehalfByName;
                                                              PTG=CriadoEmNomePorNome] }
    { 6   ;   ;OrganizationId      ;GUID          ;TableRelation="CRM Organization".OrganizationId;
                                                   ExternalName=organizationid;
                                                   ExternalType=Lookup;
                                                   ExternalAccess=Read;
                                                   CaptionML=[ENU=Organization;
                                                              PTG=Organiza��o];
                                                   Description=Unique identifier of the organization associated with the solution. }
    { 7   ;   ;OrganizationIdName  ;Text160       ;FieldClass=FlowField;
                                                   CalcFormula=Lookup("CRM Organization".Name WHERE (OrganizationId=FIELD(OrganizationId)));
                                                   ExternalName=organizationidname;
                                                   ExternalType=String;
                                                   ExternalAccess=Read;
                                                   CaptionML=[ENU=OrganizationIdName;
                                                              PTG=NomeIDOrganiza��o] }
    { 8   ;   ;PostId              ;GUID          ;ExternalName=postid;
                                                   ExternalType=Uniqueidentifier;
                                                   ExternalAccess=Insert;
                                                   CaptionML=[ENU=Post;
                                                              PTG=Publicar];
                                                   Description=Unique identifier for entity instances }
    { 9   ;   ;RegardingObjectId   ;GUID          ;TableRelation=IF (RegardingObjectTypeCode=CONST(systemuser)) "CRM Systemuser".SystemUserId
                                                                 ELSE IF (RegardingObjectTypeCode=CONST(account)) "CRM Account".AccountId
                                                                 ELSE IF (RegardingObjectTypeCode=CONST(contact)) "CRM Contact".ContactId
                                                                 ELSE IF (RegardingObjectTypeCode=CONST(opportunity)) "CRM Opportunity".OpportunityId
                                                                 ELSE IF (RegardingObjectTypeCode=CONST(post)) "CRM Post".PostId
                                                                 ELSE IF (RegardingObjectTypeCode=CONST(transactioncurrency)) "CRM Transactioncurrency".TransactionCurrencyId
                                                                 ELSE IF (RegardingObjectTypeCode=CONST(pricelevel)) "CRM Pricelevel".PriceLevelId
                                                                 ELSE IF (RegardingObjectTypeCode=CONST(productpricelevel)) "CRM Productpricelevel".ProductPriceLevelId
                                                                 ELSE IF (RegardingObjectTypeCode=CONST(product)) "CRM Product".ProductId
                                                                 ELSE IF (RegardingObjectTypeCode=CONST(incident)) "CRM Incident".IncidentId
                                                                 ELSE IF (RegardingObjectTypeCode=CONST(incidentresolution)) "CRM Incidentresolution".ActivityId
                                                                 ELSE IF (RegardingObjectTypeCode=CONST(quote)) "CRM Quote".QuoteId
                                                                 ELSE IF (RegardingObjectTypeCode=CONST(quotedetail)) "CRM Quotedetail".QuoteDetailId
                                                                 ELSE IF (RegardingObjectTypeCode=CONST(salesorder)) "CRM Salesorder".SalesOrderId
                                                                 ELSE IF (RegardingObjectTypeCode=CONST(salesorderdetail)) "CRM Salesorderdetail".SalesOrderDetailId
                                                                 ELSE IF (RegardingObjectTypeCode=CONST(invoice)) "CRM Invoice".InvoiceId
                                                                 ELSE IF (RegardingObjectTypeCode=CONST(invoicedetail)) "CRM Invoicedetail".InvoiceDetailId
                                                                 ELSE IF (RegardingObjectTypeCode=CONST(contract)) "CRM Contract".ContractId
                                                                 ELSE IF (RegardingObjectTypeCode=CONST(team)) "CRM Team".TeamId
                                                                 ELSE IF (RegardingObjectTypeCode=CONST(customeraddress)) "CRM Customeraddress".CustomerAddressId
                                                                 ELSE IF (RegardingObjectTypeCode=CONST(uom)) "CRM Uom".UoMId
                                                                 ELSE IF (RegardingObjectTypeCode=CONST(uomschedule)) "CRM Uomschedule".UoMScheduleId
                                                                 ELSE IF (RegardingObjectTypeCode=CONST(organization)) "CRM Organization".OrganizationId
                                                                 ELSE IF (RegardingObjectTypeCode=CONST(businessunit)) "CRM Businessunit".BusinessUnitId
                                                                 ELSE IF (RegardingObjectTypeCode=CONST(discount)) "CRM Discount".DiscountId
                                                                 ELSE IF (RegardingObjectTypeCode=CONST(discounttype)) "CRM Discounttype".DiscountTypeId;
                                                   ExternalName=regardingobjectid;
                                                   ExternalType=Lookup;
                                                   ExternalAccess=Insert;
                                                   CaptionML=[ENU=Regarding;
                                                              PTG=A Respeito de];
                                                   Description=Choose the parent record for the post to identify the customer, opportunity, case, or other record that the post most closely relates to. }
    { 10  ;   ;RegardingObjectTypeCode;Option     ;ExternalName=regardingobjecttypecode;
                                                   ExternalType=EntityName;
                                                   ExternalAccess=Insert;
                                                   CaptionML=[ENU=RegardingObjectTypeCode;
                                                              PTG=C�digoTipoObjetoReferente];
                                                   OptionCaptionML=[ENU=" ,systemuser,account,contact,opportunity,post,transactioncurrency,pricelevel,productpricelevel,product,incident,incidentresolution,quote,quotedetail,salesorder,salesorderdetail,invoice,invoicedetail,contract,team,customeraddress,uom,uomschedule,organization,businessunit,discount,discounttype";
                                                                    PTG=" ,utilizadorsistema,conta,contacto,oportunidade,registo,divisatransa��o,n�velpre�o,n�velpre�oproduto,produto,incidente,resolu��oincidente,proposta,detalheproposta,encomendavenda,detalheencomendavenda,fatura,detalhefatura,contrato,equipa,endere�ocliente,uom,planouom,organiza��o,unidadeneg�cio,desconto,tipodesconto"];
                                                   OptionString=[ ,systemuser,account,contact,opportunity,post,transactioncurrency,pricelevel,productpricelevel,product,incident,incidentresolution,quote,quotedetail,salesorder,salesorderdetail,invoice,invoicedetail,contract,team,customeraddress,uom,uomschedule,organization,businessunit,discount,discounttype];
                                                   Description=Type of the RegardingObject }
    { 11  ;   ;Source              ;Option        ;InitValue=ManualPost;
                                                   ExternalName=source;
                                                   ExternalType=Picklist;
                                                   ExternalAccess=Insert;
                                                   OptionOrdinalValues=[1;2];
                                                   CaptionML=[ENU=Source;
                                                              PTG=Fonte];
                                                   OptionCaptionML=[ENU=Auto Post,Manual Post;
                                                                    PTG=Publicar Automaticamente,Publicar Manualmente];
                                                   OptionString=AutoPost,ManualPost;
                                                   Description=Select whether the post was created manually or automatically. }
    { 12  ;   ;Text                ;Text250       ;ExternalName=text;
                                                   ExternalType=String;
                                                   ExternalAccess=Insert;
                                                   CaptionML=[ENU=Text;
                                                              PTG=Texto];
                                                   Description=Shows the text of a post. If this is a manual post, it appears in plain text. If this is an auto post, it appears in XML. }
    { 13  ;   ;TimeZoneRuleVersionNumber;Integer  ;ExternalName=timezoneruleversionnumber;
                                                   ExternalType=Integer;
                                                   CaptionML=[ENU=Time Zone Rule Version Number;
                                                              PTG=N�mero Vers�o Regra Fuso-Hor�rio];
                                                   MinValue=-1;
                                                   Description=For internal use only. }
    { 14  ;   ;Type                ;Option        ;InitValue=Check-in;
                                                   ExternalName=type;
                                                   ExternalType=Picklist;
                                                   ExternalAccess=Insert;
                                                   OptionOrdinalValues=[1;2;3;4;5;6;7];
                                                   CaptionML=[ENU=Type;
                                                              PTG=Tipo];
                                                   OptionCaptionML=[ENU=Check-in,Idea,News,Private Message,Question,Re-post,Status;
                                                                    PTG=Check-in,Ideia,Not�cias,Mensagem Privada,Pergunta,Re-publica��o,Estado];
                                                   OptionString=Check-in,Idea,News,PrivateMessage,Question,Re-post,Status;
                                                   Description=Select the post type. }
    { 15  ;   ;UTCConversionTimeZoneCode;Integer  ;ExternalName=utcconversiontimezonecode;
                                                   ExternalType=Integer;
                                                   CaptionML=[ENU=UTC Conversion Time Zone Code;
                                                              PTG=C�digo Fuso-Hor�rio UTC];
                                                   MinValue=-1;
                                                   Description=Time zone code that was in use when the record was created. }
    { 16  ;   ;RegardingObjectOwnerId;GUID        ;TableRelation=IF (RegardingObjectOwnerIdType=CONST(systemuser)) "CRM Systemuser".SystemUserId
                                                                 ELSE IF (RegardingObjectOwnerIdType=CONST(team)) "CRM Team".TeamId;
                                                   ExternalName=regardingobjectownerid;
                                                   ExternalType=Owner;
                                                   ExternalAccess=Read;
                                                   CaptionML=[ENU=Owner;
                                                              PTG=Propriet�rio];
                                                   Description=Unique identifier of the user or team who owns the regarding object. }
    { 17  ;   ;RegardingObjectOwnerIdType;Option  ;ExternalName=regardingobjectowneridtype;
                                                   ExternalType=EntityName;
                                                   ExternalAccess=Read;
                                                   CaptionML=[ENU=RegardingObjectOwnerIdType;
                                                              PTG=TipoIDObjetoPropriet�rioReferente];
                                                   OptionCaptionML=[ENU=" ,systemuser,team";
                                                                    PTG=" ,utilizadorsistema,equipa"];
                                                   OptionString=[ ,systemuser,team];
                                                   Description=Type of the RegardingObjectOwnerId }
    { 18  ;   ;RegardingObjectOwningBusinessU;GUID;TableRelation="CRM Businessunit".BusinessUnitId;
                                                   ExternalName=regardingobjectowningbusinessunit;
                                                   ExternalType=Lookup;
                                                   ExternalAccess=Read;
                                                   CaptionML=[ENU=Regarding object owning Business Unit;
                                                              PTG=Objeto Propriet�rio Unidade de Neg�cio];
                                                   Description=Unique identifier of the business unit that owns the regarding object. }
    { 19  ;   ;ModifiedBy          ;GUID          ;TableRelation="CRM Systemuser".SystemUserId;
                                                   ExternalName=modifiedby;
                                                   ExternalType=Lookup;
                                                   ExternalAccess=Read;
                                                   CaptionML=[ENU=Modified By;
                                                              PTG=Modificado Por];
                                                   Description=Unique identifier of the user who modified the record. }
    { 20  ;   ;ModifiedByName      ;Text200       ;FieldClass=FlowField;
                                                   CalcFormula=Lookup("CRM Systemuser".FullName WHERE (SystemUserId=FIELD(ModifiedBy)));
                                                   ExternalName=modifiedbyname;
                                                   ExternalType=String;
                                                   ExternalAccess=Read;
                                                   CaptionML=[ENU=ModifiedByName;
                                                              PTG=ModificadoPorNome] }
    { 21  ;   ;ModifiedOnBehalfBy  ;GUID          ;TableRelation="CRM Systemuser".SystemUserId;
                                                   ExternalName=modifiedonbehalfby;
                                                   ExternalType=Lookup;
                                                   ExternalAccess=Read;
                                                   CaptionML=[ENU=Modified By (Delegate);
                                                              PTG=Modificado Por (Delegado)];
                                                   Description=Unique identifier of the delegate user who modified the record. }
    { 22  ;   ;ModifiedOnBehalfByName;Text200     ;FieldClass=FlowField;
                                                   CalcFormula=Lookup("CRM Systemuser".FullName WHERE (SystemUserId=FIELD(ModifiedOnBehalfBy)));
                                                   ExternalName=modifiedonbehalfbyname;
                                                   ExternalType=String;
                                                   ExternalAccess=Read;
                                                   CaptionML=[ENU=ModifiedOnBehalfByName;
                                                              PTG=ModificadoEmNomePorNome] }
    { 23  ;   ;ModifiedOn          ;DateTime      ;ExternalName=modifiedon;
                                                   ExternalType=DateTime;
                                                   ExternalAccess=Read;
                                                   CaptionML=[ENU=Modified On;
                                                              PTG=Modificado Em];
                                                   Description=Shows the date and time when the record was last updated. The date and time are displayed in the time zone selected in Microsoft Dynamics CRM options. }
  }
  KEYS
  {
    {    ;PostId                                  ;Clustered=Yes }
    {    ;Text                                     }
  }
  FIELDGROUPS
  {
    { 1   ;DropDown            ;Text                                     }
  }
  CODE
  {

    BEGIN
    {
      Dynamics CRM Version: 7.1.0.2040
    }
    END.
  }
}

