OBJECT Table 360 Dimension Buffer
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Dimension Buffer;
               PTG=Mem. Int. Dimens�o];
  }
  FIELDS
  {
    { 1   ;   ;Table ID            ;Integer       ;TableRelation=AllObjWithCaption."Object ID" WHERE (Object Type=CONST(Table));
                                                   CaptionML=[ENU=Table ID;
                                                              PTG=N� Tabela] }
    { 2   ;   ;Entry No.           ;Integer       ;CaptionML=[ENU=Entry No.;
                                                              PTG=N� Mov.] }
    { 3   ;   ;Dimension Code      ;Code20        ;TableRelation=Dimension;
                                                   OnValidate=BEGIN
                                                                IF NOT DimMgt.CheckDim("Dimension Code") THEN
                                                                  ERROR(DimMgt.GetDimErr);
                                                              END;

                                                   CaptionML=[ENU=Dimension Code;
                                                              PTG=C�digo Dimens�o];
                                                   NotBlank=Yes }
    { 4   ;   ;Dimension Value Code;Code20        ;TableRelation="Dimension Value".Code WHERE (Dimension Code=FIELD(Dimension Code));
                                                   OnValidate=BEGIN
                                                                IF NOT DimMgt.CheckDimValue("Dimension Code","Dimension Value Code") THEN
                                                                  ERROR(DimMgt.GetDimErr);
                                                              END;

                                                   CaptionML=[ENU=Dimension Value Code;
                                                              PTG=C�d. Valor Dimens�o];
                                                   NotBlank=Yes }
    { 5   ;   ;New Dimension Value Code;Code20    ;TableRelation="Dimension Value".Code WHERE (Dimension Code=FIELD(Dimension Code));
                                                   OnValidate=BEGIN
                                                                IF NOT DimMgt.CheckDimValue("Dimension Code","New Dimension Value Code") THEN
                                                                  ERROR(DimMgt.GetDimErr);
                                                              END;

                                                   CaptionML=[ENU=New Dimension Value Code;
                                                              PTG=Novo C�d. Valor Dimens�o] }
    { 6   ;   ;Line No.            ;Integer       ;CaptionML=[ENU=Line No.;
                                                              PTG=N� Linha] }
    { 7   ;   ;No. Of Dimensions   ;Integer       ;CaptionML=[ENU=No. Of Dimensions;
                                                              PTG=N� de Dimens�es] }
  }
  KEYS
  {
    {    ;Table ID,Entry No.,Dimension Code       ;Clustered=Yes }
    {    ;No. Of Dimensions                        }
  }
  FIELDGROUPS
  {
  }
  CODE
  {
    VAR
      DimMgt@1000 : Codeunit 408;

    BEGIN
    END.
  }
}

