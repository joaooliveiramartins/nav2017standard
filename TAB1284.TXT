OBJECT Table 1284 Outstanding Bank Transaction
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Outstanding Bank Transaction;
               PTG=Transa��o Banc�ria por Liquidar];
  }
  FIELDS
  {
    { 1   ;   ;Entry No.           ;Integer       ;CaptionML=[ENU=Entry No.;
                                                              PTG=N� Movimento] }
    { 2   ;   ;Posting Date        ;Date          ;CaptionML=[ENU=Posting Date;
                                                              PTG=Data Registo] }
    { 3   ;   ;Document Type       ;Option        ;CaptionML=[ENU=Document Type;
                                                              PTG=Tipo Documento];
                                                   OptionCaptionML=[ENU=" ,Payment,Invoice,Credit Memo,Finance Charge Memo,Reminder,Refund";
                                                                    PTG=" ,Pagamento,Fatura,Nota de Cr�dito,Nota de Juro,Carta de Aviso,Reembolso"];
                                                   OptionString=[ ,Payment,Invoice,Credit Memo,Finance Charge Memo,Reminder,Refund] }
    { 4   ;   ;Document No.        ;Code20        ;CaptionML=[ENU=Document No.;
                                                              PTG=N� Documento] }
    { 5   ;   ;Bank Account No.    ;Code20        ;CaptionML=[ENU=Bank Account No.;
                                                              PTG=N� Conta Banc�ria] }
    { 6   ;   ;Description         ;Text50        ;CaptionML=[ENU=Description;
                                                              PTG=Descri��o] }
    { 7   ;   ;Amount              ;Decimal       ;CaptionML=[ENU=Amount;
                                                              PTG=Valor];
                                                   AutoFormatExpr=GetCurrencyCode }
    { 8   ;   ;Type                ;Option        ;CaptionML=[ENU=Type;
                                                              PTG=Tipo];
                                                   OptionCaptionML=[ENU=Bank Account Ledger Entry,Check Ledger Entry;
                                                                    PTG=Movs. de Conta Banc�ria,Movs. de Cheque];
                                                   OptionString=Bank Account Ledger Entry,Check Ledger Entry }
    { 9   ;   ;Applied             ;Boolean       ;CaptionML=[ENU=Applied;
                                                              PTG=Liquidado] }
    { 10  ;   ;Statement Type      ;Option        ;CaptionML=[ENU=Statement Type;
                                                              PTG=Tipo Declara��o];
                                                   OptionCaptionML=[ENU=Bank Reconciliation,Payment Application;
                                                                    PTG=Reconcilia��o Banc�ria,Liquida��o de Pagamentos];
                                                   OptionString=Bank Reconciliation,Payment Application }
    { 11  ;   ;Statement No.       ;Code20        ;CaptionML=[ENU=Statement No.;
                                                              PTG=N� Declara��o] }
  }
  KEYS
  {
    {    ;Entry No.                               ;Clustered=Yes }
    {    ;Type                                     }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    PROCEDURE DrillDown@5(BankAccNo@1000 : Code[20];TransactionType@1001 : Option;StatementType@1003 : Integer;StatementNo@1004 : Code[20]);
    VAR
      TempOutstandingBankTransaction@1002 : TEMPORARY Record 1284;
    BEGIN
      CreateTempOutstandingBankTrxs(TempOutstandingBankTransaction,BankAccNo,StatementType,StatementNo);
      SetOutstandingBankTrxFilter(TempOutstandingBankTransaction,TransactionType);
      RunOustandingBankTrxsPage(TempOutstandingBankTransaction,TransactionType);
    END;

    PROCEDURE CreateTempOutstandingBankTrxs@19(VAR TempOutstandingBankTransaction@1001 : TEMPORARY Record 1284;BankAccNo@1002 : Code[20];StatementType@1005 : Integer;StatementNo@1004 : Code[20]);
    VAR
      BankAccountLedgerEntry@1000 : Record 271;
      RemainingAmt@1003 : Decimal;
    BEGIN
      BankAccountLedgerEntry.SETRANGE("Bank Account No.",BankAccNo);
      BankAccountLedgerEntry.SETRANGE(Open,TRUE);
      IF BankAccountLedgerEntry.FINDSET THEN BEGIN
        REPEAT
          RemainingAmt := BankAccountLedgerEntry.Amount - GetAppliedAmount(BankAccountLedgerEntry."Entry No.");
          IF RemainingAmt <> 0 THEN BEGIN
            TempOutstandingBankTransaction.INIT;
            TempOutstandingBankTransaction."Posting Date" := BankAccountLedgerEntry."Posting Date";
            TempOutstandingBankTransaction."Document Type" := BankAccountLedgerEntry."Document Type";
            TempOutstandingBankTransaction."Document No." := BankAccountLedgerEntry."Document No.";
            TempOutstandingBankTransaction."Bank Account No." := BankAccountLedgerEntry."Bank Account No.";
            TempOutstandingBankTransaction.Description := BankAccountLedgerEntry.Description;
            TempOutstandingBankTransaction.Amount := RemainingAmt;
            TempOutstandingBankTransaction."Entry No." := BankAccountLedgerEntry."Entry No.";
            TempOutstandingBankTransaction."Statement Type" := StatementType;
            TempOutstandingBankTransaction."Statement No." := StatementNo;
            BankAccountLedgerEntry.CALCFIELDS("Check Ledger Entries");
            IF BankAccountLedgerEntry."Check Ledger Entries" > 0 THEN
              TempOutstandingBankTransaction.Type := TempOutstandingBankTransaction.Type::"Check Ledger Entry"
            ELSE
              TempOutstandingBankTransaction.Type := TempOutstandingBankTransaction.Type::"Bank Account Ledger Entry";
            TempOutstandingBankTransaction.INSERT;
          END;
        UNTIL BankAccountLedgerEntry.NEXT = 0;
      END;
    END;

    PROCEDURE GetCurrencyCode@1() : Code[10];
    VAR
      BankAcc@1000 : Record 270;
    BEGIN
      IF ("Bank Account No." = BankAcc."No.") OR BankAcc.GET("Bank Account No.") THEN
        EXIT(BankAcc."Currency Code");

      EXIT('');
    END;

    LOCAL PROCEDURE SetOutstandingBankTrxFilter@2(VAR TempOutstandingBankTransaction@1000 : TEMPORARY Record 1284;TransactionType@1001 : Option);
    BEGIN
      TempOutstandingBankTransaction.RESET;
      TempOutstandingBankTransaction.FILTERGROUP := 2;
      TempOutstandingBankTransaction.SETRANGE(Type,TransactionType);
      TempOutstandingBankTransaction.SETRANGE(Applied,FALSE);
      TempOutstandingBankTransaction.FILTERGROUP := 0;
    END;

    LOCAL PROCEDURE RunOustandingBankTrxsPage@3(VAR TempOutstandingBankTransaction@1001 : TEMPORARY Record 1284;TransactionType@1000 : Option);
    VAR
      OutstandingBankTransactions@1002 : Page 1284;
    BEGIN
      OutstandingBankTransactions.SetRecords(TempOutstandingBankTransaction);
      OutstandingBankTransactions.SetPageCaption(TransactionType);
      OutstandingBankTransactions.SETTABLEVIEW(TempOutstandingBankTransaction);
      OutstandingBankTransactions.RUN;
    END;

    PROCEDURE CopyFromBankAccLedgerEntry@6(BankAccountLedgerEntry@1001 : Record 271;BankTransacType@1002 : Integer;StatementType@1005 : Integer;StatementNo@1004 : Code[20];RemainingAmt@1000 : Decimal);
    BEGIN
      INIT;
      "Entry No." := BankAccountLedgerEntry."Entry No.";
      "Posting Date" := BankAccountLedgerEntry."Posting Date";
      "Document Type" := BankAccountLedgerEntry."Document Type";
      "Document No." := BankAccountLedgerEntry."Document No.";
      "Bank Account No." := BankAccountLedgerEntry."Bank Account No.";
      Description := BankAccountLedgerEntry.Description;
      Amount := RemainingAmt;
      Type := BankTransacType;
      "Statement Type" := StatementType;
      "Statement No." := StatementNo;
      INSERT;
    END;

    PROCEDURE GetAppliedAmount@7(EntryNo@1001 : Integer) AppliedAmt : Decimal;
    VAR
      AppliedPaymentEntry@1000 : Record 1294;
    BEGIN
      AppliedPaymentEntry.SETRANGE("Applies-to Entry No.",EntryNo);
      IF AppliedPaymentEntry.FINDSET THEN BEGIN
        REPEAT
          AppliedAmt += AppliedPaymentEntry."Applied Amount";
        UNTIL AppliedPaymentEntry.NEXT = 0;
      END;

      EXIT(AppliedAmt);
    END;

    BEGIN
    END.
  }
}

