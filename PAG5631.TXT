OBJECT Page 5631 FA Journal Template List
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0;
  }
  PROPERTIES
  {
    Editable=No;
    CaptionML=[ENU=FA Journal Template List;
               PTG=Lista Livros Di rio Imob.];
    SourceTable=Table5619;
    PageType=List;
    RefreshOnActivate=Yes;
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 2   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the name of the journal template you are creating.;
                           PTG=""];
                ApplicationArea=#FixedAssets;
                SourceExpr=Name }

    { 4   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the journal template you are creating.;
                           PTG=""];
                ApplicationArea=#FixedAssets;
                SourceExpr=Description }

    { 22  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies whether the journal template will be a recurring journal.;
                           PTG=""];
                ApplicationArea=#FixedAssets;
                SourceExpr=Recurring;
                Visible=FALSE }

    { 18  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the source code linked to the journal template.;
                           PTG=""];
                ApplicationArea=#FixedAssets;
                SourceExpr="Source Code";
                Visible=FALSE }

    { 20  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the reason code linked to the journal template.;
                           PTG=""];
                ApplicationArea=#FixedAssets;
                SourceExpr="Reason Code";
                Visible=FALSE }

    { 9   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the ID of the window for batches under this journal template.;
                           PTG=""];
                ApplicationArea=#FixedAssets;
                SourceExpr="Page ID";
                Visible=FALSE;
                LookupPageID=Objects }

    { 7   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the report that will be printed if you choose to print a test report from a journal batch.;
                           PTG=""];
                ApplicationArea=#FixedAssets;
                SourceExpr="Test Report ID";
                Visible=FALSE;
                LookupPageID=Objects }

    { 11  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the report that is printed when you click Post and Print from a journal batch.;
                           PTG=""];
                ApplicationArea=#FixedAssets;
                SourceExpr="Posting Report ID";
                Visible=FALSE;
                LookupPageID=Objects }

    { 16  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies whether a report is printed automatically when you post.;
                           PTG=""];
                ApplicationArea=#FixedAssets;
                SourceExpr="Force Posting Report";
                Visible=FALSE }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {

    BEGIN
    END.
  }
}

