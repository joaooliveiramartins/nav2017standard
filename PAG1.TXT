OBJECT Page 1 Company Information
{
  OBJECT-PROPERTIES
  {
    Date=16/03/17;
    Time=14:07:29;
    Version List=NAVW110.0,NAVPT10.00#00025;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Company Information;
               PTG=Informa��o Empresa];
    InsertAllowed=No;
    DeleteAllowed=No;
    SourceTable=Table79;
    PageType=Card;
    PromotedActionCategoriesML=[ENU=New,Process,Report,Application Settings,System Settings,Currencies,Codes,Regional Settings;
                                PTG=Novo,Processar,Mapas,Configura��es Aplica��o,Configura��es Sistema,Divisas,C�digos,Configura��es Regionais];
    OnInit=BEGIN
             SetShowMandatoryConditions;
           END;

    OnOpenPage=BEGIN
                 RESET;
                 IF NOT GET THEN BEGIN
                   INIT;
                   INSERT;
                 END;
               END;

    OnClosePage=VAR
                  ApplicationAreaSetup@1000 : Record 9178;
                  BankAccount@1001 : Record 270;
                BEGIN
                  IF ApplicationAreaSetup.IsFoundationEnabled THEN
                    CompanyInformationMgt.UpdateCompanyBankAccount(Rec,BankAcctPostingGroup,BankAccount);
                END;

    OnAfterGetRecord=VAR
                       ApplicationAreaSetup@1000 : Record 9178;
                     BEGIN
                       ApplicationAreaSetup.GetExperienceTierCurrentCompany(Experience);
                     END;

    OnAfterGetCurrRecord=BEGIN
                           UpdateSystemIndicator;
                         END;

    ActionList=ACTIONS
    {
      { 1900000003;0 ;ActionContainer;
                      ActionContainerType=RelatedInformation }
      { 70      ;1   ;Action    ;
                      CaptionML=[ENU=Responsibility Centers;
                                 PTG=Centros Responsabilidade];
                      ToolTipML=ENU=Set up responsibility centers to administer business operations that cover multiple locations, such as a sales offices or a purchasing departments.;
                      RunObject=Page 5715;
                      Image=Position }
      { 25      ;1   ;Action    ;
                      CaptionML=[ENU=Report Layouts;
                                 PTG=Layouts Mapas];
                      ToolTipML=ENU=Specify the layout to use on reports when viewing, printing, and saving them. The layout defines things like text font, field placement, or background.;
                      ApplicationArea=#Basic,#Suite;
                      RunObject=Page 9652;
                      Promoted=Yes;
                      Image=Report;
                      PromotedCategory=Report;
                      PromotedOnly=Yes }
      { 53      ;1   ;ActionGroup;
                      CaptionML=[ENU=Application Settings;
                                 PTG=Configura��es Aplica��o] }
      { 55      ;2   ;ActionGroup;
                      CaptionML=[ENU=Setup;
                                 PTG=Configura��o];
                      Image=Setup }
      { 52      ;3   ;Action    ;
                      CaptionML=[ENU=General Ledger Setup;
                                 PTG=Configura��o Contabilidade];
                      ToolTipML=ENU=Define your general accounting policies, such as the allowed posting period and how payments are processed. Set up your default dimensions for financial analysis.;
                      ApplicationArea=#Basic,#Suite;
                      RunObject=Page 118;
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=JournalSetup;
                      PromotedCategory=Category4 }
      { 46      ;3   ;Action    ;
                      CaptionML=[ENU=Sales & Receivables Setup;
                                 PTG=Configura��o Vendas & Cobran�as];
                      ToolTipML=ENU=Define your general policies for sales invoicing and returns, such as when to show credit and stockout warnings and how to post sales discounts. Set up your number series for creating customers and different sales documents.;
                      ApplicationArea=#Basic,#Suite;
                      RunObject=Page 459;
                      Image=ReceivablesPayablesSetup }
      { 48      ;3   ;Action    ;
                      CaptionML=[ENU=Purchases & Payables Setup;
                                 PTG=Configura��o Compras & Pagamentos];
                      ToolTipML=ENU=Define your general policies for purchase invoicing and returns, such as whether to require vendor invoice numbers and how to post purchase discounts. Set up your number series for creating vendors and different purchase documents.;
                      ApplicationArea=#Basic,#Suite;
                      RunObject=Page 460;
                      Image=Purchase }
      { 45      ;3   ;Action    ;
                      CaptionML=[ENU=Inventory Setup;
                                 PTG=Configura��o Invent�rio];
                      ToolTipML=ENU=Define your general inventory policies, such as whether to allow negative inventory and how to post and adjust item costs. Set up your number series for creating new inventory items or services.;
                      ApplicationArea=#Basic,#Suite;
                      RunObject=Page 461;
                      Image=InventorySetup }
      { 44      ;3   ;Action    ;
                      CaptionML=[ENU=Fixed Assets Setup;
                                 PTG=Configura��o Imobilizado];
                      ToolTipML=ENU=Define your accounting policies for fixed assets, such as the allowed posting period and whether to allow posting to main assets. Set up your number series for creating new fixed assets.;
                      ApplicationArea=#FixedAssets;
                      RunObject=Page 5607;
                      Image=FixedAssets }
      { 41      ;3   ;Action    ;
                      CaptionML=[ENU=Human Resources Setup;
                                 PTG=Configura��o Recursos Humanos];
                      ToolTipML=ENU=Set up number series for creating new employee cards and define if employment time is measured by days or hours.;
                      RunObject=Page 5233;
                      Image=HRSetup }
      { 40      ;3   ;Action    ;
                      CaptionML=[ENU=Jobs Setup;
                                 PTG=Configura��o Projetos];
                      ToolTipML=ENU=Define your accounting policies for jobs, such as which WIP method to use and whether to update job item costs automatically.;
                      ApplicationArea=#Jobs;
                      RunObject=Page 463;
                      Image=Job }
      { 26      ;2   ;Action    ;
                      CaptionML=[ENU=No. Series;
                                 PTG=N� S�ries];
                      ToolTipML=ENU=Set up the number series from which a new number is automatically assigned to new cards and documents, such as item cards and sales invoices.;
                      ApplicationArea=#Basic,#Suite;
                      RunObject=Page 456;
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=NumberSetup;
                      PromotedCategory=Category4 }
      { 57      ;1   ;ActionGroup;
                      CaptionML=[ENU=System Settings;
                                 PTG=Configura��es Sistema] }
      { 29      ;2   ;Action    ;
                      CaptionML=[ENU=Users;
                                 PTG=Utilizadores];
                      ToolTipML=ENU=Set up the employees who will work in in this company.;
                      ApplicationArea=#Basic,#Suite;
                      RunObject=Page 9800;
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=Users;
                      PromotedCategory=Category5 }
      { 28      ;2   ;Action    ;
                      CaptionML=[ENU=Permission Sets;
                                 PTG=Conjuntos Permiss�es];
                      ToolTipML=ENU=View or edit which feature objects that users need to access and set up the related permissions in permission sets that you can assign to the users of the database.;
                      ApplicationArea=#Basic,#Suite;
                      RunObject=Page 9802;
                      Image=Permission }
      { 23      ;2   ;Action    ;
                      CaptionML=[ENU=SMTP Mail Setup;
                                 PTG=Configura��o Mail SMTP];
                      ToolTipML=ENU=Set up the integration and security of the mail server at your site that handles email.;
                      ApplicationArea=#Basic,#Suite;
                      RunObject=Page 409;
                      Image=MailSetup }
      { 59      ;1   ;ActionGroup;
                      CaptionML=[ENU=Currencies;
                                 PTG=Divisas] }
      { 27      ;2   ;Action    ;
                      CaptionML=[ENU=Currencies;
                                 PTG=Divisas];
                      ToolTipML=ENU=Set up the different currencies that you trade in by defining which general ledger accounts the involved transactions are posted to and how the foreign currency amounts are rounded.;
                      ApplicationArea=#Suite;
                      RunObject=Page 5;
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=Currencies;
                      PromotedCategory=Category6 }
      { 21      ;1   ;ActionGroup;
                      CaptionML=[ENU=Regional Settings;
                                 PTG=Configura��es Regionais] }
      { 19      ;2   ;Action    ;
                      CaptionML=[ENU=Countries/Regions;
                                 PTG=Pa�ses/Regi�es];
                      ToolTipML=ENU=Set up the country/regions where your different business partners are located, so that you can assign Country/Region codes to business partners where special local procedures are required.;
                      ApplicationArea=#Basic,#Suite;
                      RunObject=Page 10;
                      Image=CountryRegion }
      { 17      ;2   ;Action    ;
                      CaptionML=[ENU=Post Codes;
                                 PTG=C�ds. Postais];
                      ToolTipML=ENU=Set up the post codes of cities where your business partners are located.;
                      ApplicationArea=#Basic,#Suite;
                      RunObject=Page 367;
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=MailSetup;
                      PromotedCategory=Category8 }
      { 15      ;2   ;Action    ;
                      CaptionML=[ENU=Online Map Setup;
                                 PTG=Configura��o Mapas Online];
                      ToolTipML=ENU=Define which map provider to use and how routes and distances are displayed when you choose the Online Map field on business documents.;
                      ApplicationArea=#Basic,#Suite;
                      RunObject=Page 800;
                      Image=MapSetup }
      { 12      ;2   ;Action    ;
                      CaptionML=[ENU=Languages;
                                 PTG=Idiomas];
                      ToolTipML=ENU=Set up the languages that are spoken by your different business partners, so that you can print item names or descriptions in the relevant language.;
                      RunObject=Page 9;
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=Language;
                      PromotedCategory=Category7 }
      { 11      ;1   ;ActionGroup;
                      CaptionML=[ENU=Codes;
                                 PTG=C�digos];
                      ActionContainerType=NewDocumentItems }
      { 9       ;2   ;Action    ;
                      CaptionML=[ENU=Source Codes;
                                 PTG=C�ds. Origem];
                      ToolTipML=ENU=Set up codes for your different types of business transactions, so that you can track the source of the transactions in an audit.;
                      ApplicationArea=#Basic,#Suite;
                      RunObject=Page 257;
                      Image=CodesList }
      { 5       ;2   ;Action    ;
                      CaptionML=[ENU=Reason Codes;
                                 PTG=C�gigos Auditoria];
                      ToolTipML=ENU=Set up codes that specify reasons why entries were created, such as Return, to specify why a purchase credit memo was posted.;
                      ApplicationArea=#Basic,#Suite;
                      RunObject=Page 259;
                      Image=CodesList }
      { 1110018 ;1   ;ActionGroup;
                      CaptionML=[ENU=P&ayments;
                                 PTG=&Pagamentos] }
      { 1110019 ;2   ;Action    ;
                      CaptionML=[ENU=Payment Days;
                                 PTG=Dias de Pagamento];
                      RunObject=Page 31022899;
                      RunPageLink=Table Name=CONST(Company Information),
                                  Code=FIELD(Payment Days Code);
                      Image=PaymentDays }
      { 1110020 ;2   ;Action    ;
                      CaptionML=[ENU=Non-Payment Periods;
                                 PTG=Per�odos de N�o-Pagamento];
                      RunObject=Page 31022900;
                      RunPageLink=Table Name=CONST(Company Information),
                                  Code=FIELD(Non-Paymt. Periods Code);
                      Image=PaymentPeriod }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                CaptionML=[ENU=General;
                           PTG=Geral] }

    { 2   ;2   ;Field     ;
                ToolTipML=ENU=Specifies the company's name and corporate form. For example, Inc. or Ltd.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr=Name;
                ShowMandatory=TRUE }

    { 4   ;2   ;Field     ;
                ToolTipML=ENU=Specifies the company's address.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr=Address;
                ShowMandatory=TRUE }

    { 6   ;2   ;Field     ;
                ToolTipML=ENU=Specifies additional address information.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Address 2" }

    { 34  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the postal code.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Post Code";
                ShowMandatory=TRUE }

    { 8   ;2   ;Field     ;
                ToolTipML=ENU=Specifies the company's city.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr=City;
                ShowMandatory=TRUE }

    { 1110021;2;Field     ;
                ApplicationArea=#Basic,#Suite;
                SourceExpr=County }

    { 42  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the country/region of the address.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Country/Region Code";
                ShowMandatory=TRUE }

    { 10  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the company's telephone number.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Phone No." }

    { 14  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the company's VAT registration number.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="VAT Registration No." }

    { 1110010;2;Field     ;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="CAE Code" }

    { 1110012;2;Field     ;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="CAE Description" }

    { 31022896;2;Field    ;
                SourceExpr="Share Capital" }

    { 1110090;2;Field     ;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Registration Authority" }

    { 1110092;2;Field     ;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Registration No." }

    { 1110094;2;Field     ;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Business Name" }

    { 1110096;2;Field     ;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Software Vendor VAT" }

    { 1110098;2;Field     ;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Soft. Certificate Number" }

    { 33  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the picture that has been set up for the company, such as a company logo.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr=Picture;
                OnValidate=BEGIN
                             CurrPage.SAVERECORD;
                           END;
                            }

    { 7   ;2   ;Field     ;
                ToolTipML=ENU=Specifies your company in connection with electronic document exchange.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr=GLN }

    { 76  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the company's industrial classification code.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Industrial Classification";
                Importance=Additional }

    { 1110025;2;Field     ;
                SourceExpr="SAF-T PT Company Customer ID" }

    { 1902768601;1;Group  ;
                CaptionML=[ENU=Communication;
                           PTG=Comunica��o] }

    { 50  ;2   ;Field     ;
                Name=Phone No.2;
                ToolTipML=ENU=Specifies the company's telephone number.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Phone No.";
                Importance=Additional }

    { 54  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the company's fax number.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Fax No.";
                Importance=Additional }

    { 56  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the company's email address.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="E-Mail" }

    { 58  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the company's home page address.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Home Page" }

    { 78  ;2   ;Field     ;
                ToolTipML=ENU=Specifies your company's intercompany partner code.;
                SourceExpr="IC Partner Code";
                Importance=Additional }

    { 80  ;2   ;Field     ;
                ToolTipML=ENU=Specifies what type of intercompany inbox you have, either File Location or Database.;
                SourceExpr="IC Inbox Type";
                Importance=Additional }

    { 82  ;2   ;Field     ;
                ToolTipML=ENU=Specifies details about the location of your intercompany inbox, which can transfer intercompany transactions into your company.;
                SourceExpr="IC Inbox Details";
                Importance=Additional }

    { 1000000000;2;Field  ;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="AT Generic User ID" }

    { 1000000001;2;Field  ;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="AT Password" }

    { 1000000002;2;Field  ;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="AT File Path" }

    { 1901677601;1;Group  ;
                CaptionML=[ENU=Payments;
                           PTG=Pagamentos] }

    { 3   ;2   ;Field     ;
                ToolTipML=ENU=Specifies if you are allowed to create a sales invoice without filling the setup fields on this FastTab.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Allow Blank Payment Info." }

    { 16  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the name of the bank the company uses.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Bank Name";
                ShowMandatory=TRUE }

    { 18  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the bank's branch number.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Bank Branch No.";
                OnValidate=BEGIN
                             SetShowMandatoryConditions
                           END;

                ShowMandatory=IBANMissing }

    { 20  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the company's bank account number.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Bank Account No.";
                OnValidate=BEGIN
                             SetShowMandatoryConditions
                           END;

                ShowMandatory=IBANMissing }

    { 1110000;2;Field     ;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="CCC Bank No." }

    { 1110002;2;Field     ;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="CCC Bank Branch No." }

    { 1110004;2;Field     ;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="CCC Control Digits" }

    { 1110006;2;Field     ;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="CCC Bank Account No." }

    { 1110008;2;Field     ;
                SourceExpr="CCC No." }

    { 22  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the company's payment routing number.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Payment Routing No." }

    { 24  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the company's giro number.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Giro No." }

    { 72  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the SWIFT code (international bank identifier code) of your primary bank.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="SWIFT Code";
                OnValidate=BEGIN
                             SetShowMandatoryConditions
                           END;
                            }

    { 74  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the international bank account number of your primary bank account.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr=IBAN;
                OnValidate=BEGIN
                             SetShowMandatoryConditions
                           END;

                ShowMandatory=BankBranchNoOrAccountNoMissing }

    { 30  ;2   ;Field     ;
                Name=BankAccountPostingGroup;
                Lookup=Yes;
                CaptionML=[ENU=" Bank Account Posting Group";
                           PTG=" Grupo Contab. Conta Banc�ria"];
                ToolTipML=ENU=Specifies a code for the bank account posting group for the company's bank account.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr=BankAcctPostingGroup;
                TableRelation="Bank Account Posting Group".Code }

    { 1110014;2;Field     ;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Payment Days Code" }

    { 1110016;2;Field     ;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Non-Paymt. Periods Code" }

    { 1906801201;1;Group  ;
                CaptionML=[ENU=Shipping;
                           PTG=Envio] }

    { 35  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the name of the location to which items for the company should be shipped.;
                SourceExpr="Ship-to Name" }

    { 37  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the address of the location to which items for the company should be shipped.;
                SourceExpr="Ship-to Address" }

    { 39  ;2   ;Field     ;
                ToolTipML=ENU=Specifies additional address information.;
                SourceExpr="Ship-to Address 2" }

    { 51  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the postal code of the address.;
                SourceExpr="Ship-to Post Code" }

    { 49  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the city of the address.;
                SourceExpr="Ship-to City" }

    { 1110023;2;Field     ;
                SourceExpr="Ship-to County" }

    { 60  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the country/region code of the address.;
                SourceExpr="Ship-to Country/Region Code" }

    { 43  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the name of the contact person to whom items for the company should be shipped.;
                SourceExpr="Ship-to Contact" }

    { 47  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the location code that corresponds to the company's ship-to address.;
                SourceExpr="Location Code" }

    { 13  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the code for the default responsibility center.;
                SourceExpr="Responsibility Center" }

    { 62  ;2   ;Field     ;
                ToolTipML=ENU=Specifies a date formula that defines the length of the period after the planned shipment date on demand lines in which the system checks availability for the demand line in question.;
                SourceExpr="Check-Avail. Period Calc." }

    { 64  ;2   ;Field     ;
                ToolTipML=ENU=Specifies how frequently the system checks supply-demand events to discover if the item on the demand line is available on its shipment date.;
                SourceExpr="Check-Avail. Time Bucket" }

    { 67  ;2   ;Field     ;
                DrillDown=No;
                ToolTipML=ENU=Specifies the code for the base calendar that you want to assign to your company.;
                SourceExpr="Base Calendar Code" }

    { 69  ;2   ;Field     ;
                Name=Customized Calendar;
                DrillDown=Yes;
                CaptionML=[ENU=Customized Calendar;
                           PTG=Calend�rio Personalizado];
                ToolTipML=ENU=Specifies whether or not your company has set up a customized calendar.;
                SourceExpr=CalendarMgmt.CustomizedCalendarExistText(CustomizedCalendar."Source Type"::Company,'','',"Base Calendar Code");
                Editable=FALSE;
                OnDrillDown=BEGIN
                              CurrPage.SAVERECORD;
                              TESTFIELD("Base Calendar Code");
                              CalendarMgmt.ShowCustomizedCalendar(CustomizedCalEntry."Source Type"::Company,'','',"Base Calendar Code");
                            END;
                             }

    { 84  ;2   ;Field     ;
                ToolTipML=ENU=Specifies how dates based on calendar and calendar-related documents are calculated.;
                SourceExpr="Cal. Convergence Time Frame" }

    { 31022895;1;Group    ;
                CaptionML=[ENU=Annual Decl. / Model 22;
                           PTG=Decl. Anual / Modelo 22] }

    { 31022894;2;Field    ;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Tax Auth. Code" }

    { 31022893;2;Field    ;
                CaptionML=[ENU=Activ. Table Code (Port. n� 1011/2001, 21/8);
                           PTG=C�digo Tabela Ativ. (Port. n.� 1011/2001, 21/8)];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Activ. Table Code" }

    { 31022892;2;Field    ;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Legal Rep. Fiscal No." }

    { 31022891;2;Field    ;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Accountant Fiscal No." }

    { 31022890;2;Field    ;
                ApplicationArea=#Basic,#Suite;
                SourceExpr=Municipality }

    { 1904604101;1;Group  ;
                CaptionML=[ENU=System Indicator;
                           PTG=Indicador de Sistema] }

    { 100 ;2   ;Field     ;
                ToolTipML=ENU=Specifies how you want to use the system indicator when you are working with different versions of Microsoft Dynamics NAV.;
                SourceExpr="System Indicator";
                OnValidate=BEGIN
                             SystemIndicatorOnAfterValidate;
                           END;
                            }

    { 104 ;2   ;Field     ;
                ToolTipML=ENU=Specifies if you want to apply a certain style to the system indicator.;
                SourceExpr="System Indicator Style" }

    { 102 ;2   ;Field     ;
                Name=System Indicator Text;
                CaptionML=[ENU=System Indicator Text;
                           PTG=Texto do Indicador de Sistema];
                SourceExpr=SystemIndicatorText;
                Editable=SystemIndicatorTextEditable;
                OnValidate=BEGIN
                             "Custom System Indicator Text" := SystemIndicatorText;
                             SystemIndicatorTextOnAfterVali;
                           END;
                            }

    { 31  ;1   ;Group     ;
                CaptionML=[ENU=User Experience;
                           PTG=Experi�ncia Utilizador];
                Visible=Experience = Experience::Custom;
                GroupType=Group }

    { 32  ;2   ;Field     ;
                Name=ExperienceCustom;
                AccessByPermission=TableData 9178=IM;
                CaptionML=[ENU=Experience;
                           PTG=Experi�ncia];
                ToolTipML=ENU=Specifies for which application areas fields and actions are shown in the user interface. This is a way to simplify the product by hiding UI elements for features that you do not use.;
                OptionCaptionML=[ENU=" ,,,,,Basic,,,,,,,,,,Suite,,,,,Custom";
                                 PTG=" ,,,,,Basic,,,,,,,,,,Suite,,,,,Custom"];
                ApplicationArea=#All;
                BlankZero=Yes;
                SourceExpr=Experience;
                OnValidate=VAR
                             ApplicationAreaSetup@1000 : Record 9178;
                           BEGIN
                             ApplicationAreaSetup.SetExperienceTierCurrentCompany(Experience);
                             MESSAGE(ReSignInMsg);
                           END;
                            }

    { 38  ;1   ;Group     ;
                CaptionML=[ENU=User Experience;
                           PTG=Experi�ncia Utilizador];
                Visible=Experience <> Experience::Custom;
                GroupType=Group }

    { 36  ;2   ;Field     ;
                Name=Experience;
                AccessByPermission=TableData 9178=IM;
                CaptionML=[ENU=Experience;
                           PTG=Experi�ncia];
                ToolTipML=ENU=Specifies for which application areas fields and actions are shown in the user interface. This is a way to simplify the product by hiding UI elements for features that you do not use.;
                OptionCaptionML=[ENU=" ,,,,,Basic,,,,,,,,,,Suite";
                                 PTG=" ,,,,,Basic,,,,,,,,,,Suite"];
                ApplicationArea=#All;
                BlankZero=Yes;
                SourceExpr=Experience;
                OnValidate=VAR
                             ApplicationAreaSetup@1000 : Record 9178;
                           BEGIN
                             ApplicationAreaSetup.SetExperienceTierCurrentCompany(Experience);
                             MESSAGE(ReSignInMsg);
                           END;
                            }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {
    VAR
      CustomizedCalEntry@1007 : Record 7603;
      CustomizedCalendar@1005 : Record 7602;
      CalendarMgmt@1004 : Codeunit 7600;
      CompanyInformationMgt@1003 : Codeunit 1306;
      Experience@1008 : ' ,,,,,Basic,,,,,,,,,,Suite,,,,,Custom';
      SystemIndicatorText@1006 : Text[250];
      SystemIndicatorTextEditable@19043996 : Boolean INDATASET;
      IBANMissing@1000 : Boolean;
      BankBranchNoOrAccountNoMissing@1001 : Boolean;
      BankAcctPostingGroup@1002 : Code[10];
      ReSignInMsg@1009 : TextConst '@@@="""sign out"" and ""sign in"" are the same terms as shown in the Dynamics NAV client.";ENU=You must sign out and then sign in again to have the changes take effect.;PTG=Tem de terminar sess�o e iniciar novamente para que as altera��es tenham efeito.';

    LOCAL PROCEDURE UpdateSystemIndicator@1008();
    VAR
      IndicatorStyle@1000 : Option;
    BEGIN
      GetSystemIndicator(SystemIndicatorText,IndicatorStyle); // IndicatorStyle is not used
      SystemIndicatorTextEditable := CurrPage.EDITABLE AND ("System Indicator" = "System Indicator"::"Custom Text");
    END;

    LOCAL PROCEDURE SystemIndicatorTextOnAfterVali@19070270();
    BEGIN
      UpdateSystemIndicator
    END;

    LOCAL PROCEDURE SystemIndicatorOnAfterValidate@19079461();
    BEGIN
      UpdateSystemIndicator
    END;

    LOCAL PROCEDURE SetShowMandatoryConditions@2();
    BEGIN
      BankBranchNoOrAccountNoMissing := ("Bank Branch No." = '') OR ("Bank Account No." = '');
      IBANMissing := IBAN = ''
    END;

    BEGIN
    END.
  }
}

