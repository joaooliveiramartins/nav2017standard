OBJECT Page 8610 Config. Questionnaire
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Config. Questionnaire;
               PTG=Config. Question�rio];
    SourceTable=Table8610;
    PageType=List;
    PromotedActionCategoriesML=[ENU=New,Process,Report,Excel;
                                PTG=Novo,Processar,Mapas,Excel];
    ActionList=ACTIONS
    {
      { 1900000003;  ;ActionContainer;
                      ActionContainerType=ActionItems }
      { 14      ;1   ;ActionGroup;
                      CaptionML=[ENU=&Questionnaire;
                                 PTG=&Question�rio];
                      Image=Questionaire }
      { 11      ;2   ;Action    ;
                      Ellipsis=Yes;
                      CaptionML=[ENU=E&xport to Excel;
                                 PTG=E&xportar para Excel];
                      ToolTipML=[ENU=Export data in the questionnaire to Excel.;
                                 PTG=""];
                      ApplicationArea=#Basic,#Suite;
                      Promoted=Yes;
                      Image=ExportToExcel;
                      PromotedCategory=Category4;
                      OnAction=BEGIN
                                 TESTFIELD(Code);

                                 FileName := FileMgt.SaveFileDialog(Text002,FileName,'');
                                 IF FileName = '' THEN
                                   EXIT;

                                 IF QuestionnaireMgt.ExportQuestionnaireToExcel(FileName,Rec) THEN
                                   MESSAGE(Text000);
                               END;
                                }
      { 10      ;2   ;Action    ;
                      Ellipsis=Yes;
                      CaptionML=[ENU=&Import from Excel;
                                 PTG=&Importar do Excel];
                      ToolTipML=[ENU=Import information from Excel into the questionnaire.;
                                 PTG=""];
                      ApplicationArea=#Basic,#Suite;
                      Promoted=Yes;
                      Image=ImportExcel;
                      PromotedCategory=Category4;
                      OnAction=BEGIN
                                 FileName := FileMgt.OpenFileDialog(Text002,FileName,'');
                                 IF FileName = '' THEN
                                   EXIT;

                                 IF QuestionnaireMgt.ImportQuestionnaireFromExcel(FileName) THEN
                                   MESSAGE(Text001);
                               END;
                                }
      { 9       ;2   ;Separator  }
      { 8       ;2   ;Action    ;
                      Ellipsis=Yes;
                      CaptionML=[ENU=&Export to XML;
                                 PTG=&Exportar para XML];
                      ToolTipML=[ENU=Export information in the questionnaire to Excel.;
                                 PTG=""];
                      ApplicationArea=#Basic,#Suite;
                      Image=Export;
                      OnAction=BEGIN
                                 IF QuestionnaireMgt.ExportQuestionnaireAsXML(FileName,Rec) THEN
                                   MESSAGE(Text000)
                                 ELSE
                                   MESSAGE(Text003);
                               END;
                                }
      { 7       ;2   ;Action    ;
                      Ellipsis=Yes;
                      CaptionML=[ENU=&Import from XML;
                                 PTG=&Importar de XML];
                      ToolTipML=[ENU=Import information from XML into the questionnaire.;
                                 PTG=""];
                      ApplicationArea=#Basic,#Suite;
                      Image=Import;
                      OnAction=BEGIN
                                 IF QuestionnaireMgt.ImportQuestionnaireAsXMLFromClient THEN
                                   MESSAGE(Text001);
                               END;
                                }
      { 6       ;2   ;Separator  }
      { 5       ;2   ;Action    ;
                      CaptionML=[ENU=&Update Questionnaire;
                                 PTG=&Atualizar Question�rio];
                      ToolTipML=[ENU=Fill the question list based on the fields in the table on which the question area is based.;
                                 PTG=""];
                      ApplicationArea=#Basic,#Suite;
                      Image=Refresh;
                      PromotedCategory=Process;
                      OnAction=BEGIN
                                 IF QuestionnaireMgt.UpdateQuestionnaire(Rec) THEN
                                   MESSAGE(Text004);
                               END;
                                }
      { 3       ;2   ;Action    ;
                      CaptionML=[ENU=&Apply Answers;
                                 PTG=&Aplicar Respostas];
                      ToolTipML=[ENU=Implement answers in the questionnaire in the related setup fields.;
                                 PTG=""];
                      ApplicationArea=#Basic,#Suite;
                      Promoted=Yes;
                      Image=Apply;
                      PromotedCategory=Process;
                      OnAction=BEGIN
                                 IF QuestionnaireMgt.ApplyAnswers(Rec) THEN
                                   MESSAGE(Text005);
                               END;
                                }
      { 15      ;    ;ActionContainer;
                      ActionContainerType=RelatedInformation }
      { 16      ;1   ;ActionGroup;
                      CaptionML=[ENU=Areas;
                                 PTG=�reas] }
      { 13      ;2   ;Action    ;
                      CaptionML=[ENU=&Question Areas;
                                 PTG=�reas &Quest�o];
                      ToolTipML=[ENU=View the areas that questions are grouped by.;
                                 PTG=""];
                      ApplicationArea=#Basic,#Suite;
                      RunObject=Page 8613;
                      RunPageLink=Questionnaire Code=FIELD(Code);
                      Promoted=Yes;
                      Image=View;
                      PromotedCategory=Process }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 2   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies a code for the configuration questionnaire that you are creating.;
                           PTG=""];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=Code }

    { 4   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the description of the configuration questionnaire. You can provide a name or description of up to 50 characters, numbers, and spaces.;
                           PTG=""];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=Description }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {
    VAR
      Text000@1001 : TextConst 'ENU=The questionnaire has been successfully exported.;PTG=Question�rio exportado com sucesso.';
      Text001@1000 : TextConst 'ENU=The questionnaire has been successfully imported.;PTG=Question�rio importado com sucesso.';
      Text002@1002 : TextConst 'ENU=Save as Excel workbook;PTG=Guardar como livro Excel';
      Text003@1003 : TextConst 'ENU=The export of the questionnaire has been canceled.;PTG=Exporta��o do Question�rio cancelada.';
      QuestionnaireMgt@1004 : Codeunit 8610;
      FileMgt@1009 : Codeunit 419;
      FileName@1005 : Text;
      Text004@1006 : TextConst 'ENU=The questionnaire has been updated.;PTG=Question�rio atualizado.';
      Text005@1010 : TextConst 'ENU=Answers have been applied.;PTG=As respostas foram aplicadas.';

    BEGIN
    END.
  }
}

