OBJECT Table 5746 Transfer Receipt Header
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0,NAVPTSS92.00;
  }
  PROPERTIES
  {
    DataCaptionFields=No.;
    OnDelete=VAR
               InvtCommentLine@1000 : Record 5748;
               TransRcptLine@1001 : Record 5747;
               MoveEntries@1002 : Codeunit 361;
             BEGIN
               TransRcptLine.SETRANGE("Document No.","No.");
               IF TransRcptLine.FIND('-') THEN
                 REPEAT
                   TransRcptLine.DELETE;
                 UNTIL TransRcptLine.NEXT = 0;

               InvtCommentLine.SETRANGE("Document Type",InvtCommentLine."Document Type"::"Posted Transfer Receipt");
               InvtCommentLine.SETRANGE("No.","No.");
               InvtCommentLine.DELETEALL;

               ItemTrackingMgt.DeleteItemEntryRelation(
                 DATABASE::"Transfer Receipt Line",0,"No.",'',0,0,TRUE);

               MoveEntries.MoveDocRelatedEntries(DATABASE::"Transfer Receipt Header","No.");
             END;

    CaptionML=[ENU=Transfer Receipt Header;
               PTG=Cabe�alho Rece��o Transfer�ncia];
    LookupPageID=Page5753;
  }
  FIELDS
  {
    { 1   ;   ;No.                 ;Code20        ;CaptionML=[ENU=No.;
                                                              PTG=N�] }
    { 2   ;   ;Transfer-from Code  ;Code10        ;TableRelation=Location WHERE (Use As In-Transit=CONST(No));
                                                   CaptionML=[ENU=Transfer-from Code;
                                                              PTG=Transf.-de C�d.] }
    { 3   ;   ;Transfer-from Name  ;Text50        ;CaptionML=[ENU=Transfer-from Name;
                                                              PTG=Transf.-de Nome] }
    { 4   ;   ;Transfer-from Name 2;Text50        ;CaptionML=[ENU=Transfer-from Name 2;
                                                              PTG=Transf.-de Nome 2] }
    { 5   ;   ;Transfer-from Address;Text50       ;CaptionML=[ENU=Transfer-from Address;
                                                              PTG=Transf.-de Endere�o] }
    { 6   ;   ;Transfer-from Address 2;Text50     ;CaptionML=[ENU=Transfer-from Address 2;
                                                              PTG=Transf.-de Endere�o 2] }
    { 7   ;   ;Transfer-from Post Code;Code20     ;TableRelation="Post Code";
                                                   ValidateTableRelation=No;
                                                   TestTableRelation=No;
                                                   CaptionML=[ENU=Transfer-from Post Code;
                                                              PTG=Transf.-de C�d. Postal] }
    { 8   ;   ;Transfer-from City  ;Text30        ;TableRelation="Post Code".City;
                                                   ValidateTableRelation=No;
                                                   TestTableRelation=No;
                                                   CaptionML=[ENU=Transfer-from City;
                                                              PTG=Transf.-de Localidade] }
    { 9   ;   ;Transfer-from County;Text30        ;CaptionML=[ENU=Transfer-from County;
                                                              PTG=Transf.-de Distrito] }
    { 10  ;   ;Trsf.-from Country/Region Code;Code10;
                                                   TableRelation=Country/Region;
                                                   CaptionML=[ENU=Trsf.-from Country/Region Code;
                                                              PTG=Transf.-de C�d. Pa�s/Regi�o] }
    { 11  ;   ;Transfer-to Code    ;Code10        ;TableRelation=Location WHERE (Use As In-Transit=CONST(No));
                                                   CaptionML=[ENU=Transfer-to Code;
                                                              PTG=Transf.-para C�d.] }
    { 12  ;   ;Transfer-to Name    ;Text50        ;CaptionML=[ENU=Transfer-to Name;
                                                              PTG=Transf.-para Nome] }
    { 13  ;   ;Transfer-to Name 2  ;Text50        ;CaptionML=[ENU=Transfer-to Name 2;
                                                              PTG=Transf.-para Nome 2] }
    { 14  ;   ;Transfer-to Address ;Text50        ;CaptionML=[ENU=Transfer-to Address;
                                                              PTG=Transf.-para Endere�o] }
    { 15  ;   ;Transfer-to Address 2;Text50       ;CaptionML=[ENU=Transfer-to Address 2;
                                                              PTG=Transf.-para Endere�o 2] }
    { 16  ;   ;Transfer-to Post Code;Code20       ;TableRelation="Post Code";
                                                   ValidateTableRelation=No;
                                                   TestTableRelation=No;
                                                   CaptionML=[ENU=Transfer-to Post Code;
                                                              PTG=Transf.-para C�d. Postal] }
    { 17  ;   ;Transfer-to City    ;Text30        ;TableRelation="Post Code".City;
                                                   ValidateTableRelation=No;
                                                   TestTableRelation=No;
                                                   CaptionML=[ENU=Transfer-to City;
                                                              PTG=Transf.-para Localidade] }
    { 18  ;   ;Transfer-to County  ;Text30        ;CaptionML=[ENU=Transfer-to County;
                                                              PTG=Transf.-para Distrito] }
    { 19  ;   ;Trsf.-to Country/Region Code;Code10;TableRelation=Country/Region;
                                                   CaptionML=[ENU=Trsf.-to Country/Region Code;
                                                              PTG=Transf.-para C�d. Pa�s/Regi�o] }
    { 20  ;   ;Transfer Order Date ;Date          ;CaptionML=[ENU=Transfer Order Date;
                                                              PTG=Data Ordem Transfer�ncia] }
    { 21  ;   ;Posting Date        ;Date          ;CaptionML=[ENU=Posting Date;
                                                              PTG=Data Registo] }
    { 22  ;   ;Comment             ;Boolean       ;FieldClass=FlowField;
                                                   CalcFormula=Exist("Inventory Comment Line" WHERE (Document Type=CONST(Posted Transfer Receipt),
                                                                                                     No.=FIELD(No.)));
                                                   CaptionML=[ENU=Comment;
                                                              PTG=Coment�rio];
                                                   Editable=No }
    { 23  ;   ;Shortcut Dimension 1 Code;Code20   ;TableRelation="Dimension Value".Code WHERE (Global Dimension No.=CONST(1));
                                                   CaptionML=[ENU=Shortcut Dimension 1 Code;
                                                              PTG=C�d. Atalho Dimens�o 1];
                                                   CaptionClass='1,2,1' }
    { 24  ;   ;Shortcut Dimension 2 Code;Code20   ;TableRelation="Dimension Value".Code WHERE (Global Dimension No.=CONST(2));
                                                   CaptionML=[ENU=Shortcut Dimension 2 Code;
                                                              PTG=C�d. Atalho Dimens�o 2];
                                                   CaptionClass='1,2,2' }
    { 25  ;   ;Transfer Order No.  ;Code20        ;TableRelation="Transfer Header";
                                                   ValidateTableRelation=No;
                                                   TestTableRelation=No;
                                                   CaptionML=[ENU=Transfer Order No.;
                                                              PTG=N� Ordem Transf.] }
    { 26  ;   ;No. Series          ;Code10        ;TableRelation="No. Series";
                                                   CaptionML=[ENU=No. Series;
                                                              PTG=N� S�ries] }
    { 27  ;   ;Shipment Date       ;Date          ;CaptionML=[ENU=Shipment Date;
                                                              PTG=Data Envio] }
    { 28  ;   ;Receipt Date        ;Date          ;CaptionML=[ENU=Receipt Date;
                                                              PTG=Data Rece��o] }
    { 29  ;   ;In-Transit Code     ;Code10        ;TableRelation=Location.Code WHERE (Use As In-Transit=CONST(Yes));
                                                   CaptionML=[ENU=In-Transit Code;
                                                              PTG=C�d. Em-Tr�nsito] }
    { 30  ;   ;Transfer-from Contact;Text50       ;CaptionML=[ENU=Transfer-from Contact;
                                                              PTG=Transf.-de Contacto] }
    { 31  ;   ;Transfer-to Contact ;Text50        ;CaptionML=[ENU=Transfer-to Contact;
                                                              PTG=Transf.-para Contacto] }
    { 32  ;   ;External Document No.;Code35       ;CaptionML=[ENU=External Document No.;
                                                              PTG=N� Documento Externo] }
    { 33  ;   ;Shipping Agent Code ;Code10        ;TableRelation="Shipping Agent";
                                                   AccessByPermission=TableData 5790=R;
                                                   CaptionML=[ENU=Shipping Agent Code;
                                                              PTG=C�d. Transportador] }
    { 34  ;   ;Shipping Agent Service Code;Code10 ;TableRelation="Shipping Agent Services".Code WHERE (Shipping Agent Code=FIELD(Shipping Agent Code));
                                                   CaptionML=[ENU=Shipping Agent Service Code;
                                                              PTG=C�d. Servi�o Transportador] }
    { 35  ;   ;Shipment Method Code;Code10        ;TableRelation="Shipment Method";
                                                   CaptionML=[ENU=Shipment Method Code;
                                                              PTG=C�d. Condi��es Envio] }
    { 47  ;   ;Transaction Type    ;Code10        ;TableRelation="Transaction Type";
                                                   CaptionML=[ENU=Transaction Type;
                                                              PTG=Tipo Transa��o] }
    { 48  ;   ;Transport Method    ;Code10        ;TableRelation="Transport Method";
                                                   CaptionML=[ENU=Transport Method;
                                                              PTG=M�todo Transporte] }
    { 59  ;   ;Entry/Exit Point    ;Code10        ;TableRelation="Entry/Exit Point";
                                                   CaptionML=[ENU=Entry/Exit Point;
                                                              PTG=Porto/Aeroporto] }
    { 63  ;   ;Area                ;Code10        ;TableRelation=Area;
                                                   CaptionML=[ENU=Area;
                                                              PTG=�rea] }
    { 64  ;   ;Transaction Specification;Code10   ;TableRelation="Transaction Specification";
                                                   CaptionML=[ENU=Transaction Specification;
                                                              PTG=Especifica��o Transa��o] }
    { 480 ;   ;Dimension Set ID    ;Integer       ;TableRelation="Dimension Set Entry";
                                                   OnLookup=BEGIN
                                                              ShowDimensions;
                                                            END;

                                                   CaptionML=[ENU=Dimension Set ID;
                                                              PTG=ID Combina��o Dimens�o];
                                                   Editable=No }
    { 31022890;;Series Group       ;Code10        ;TableRelation="Series Groups".Code;
                                                   CaptionML=[ENU=Series Group;
                                                              PTG=Sequ�ncia S�ries];
                                                   Description=soft }
    { 31022891;;Transfer-to VAT Reg. No.;Text20   ;CaptionML=[ENU=Transfer-to VAT Reg. No.;
                                                              PTG=Transf.-para N� Contribuinte];
                                                   Description=V92.00#00033;
                                                   Editable=No }
    { 31022892;;Location Type      ;Option        ;CaptionML=[ENU=Location Type;
                                                              PTG=Tipo Localiza��o];
                                                   OptionCaptionML=[ENU=Internal,External - Customer,External - Vendor;
                                                                    PTG=Interna,Externa - Cliente,Externa - Fornecedor];
                                                   OptionString=Internal,External - Customer,External - Vendor;
                                                   Description=V92.00#00033 }
    { 31022893;;External Entity No.;Code20        ;TableRelation=IF (Location Type=CONST(External - Customer)) Customer.No.
                                                                 ELSE IF (Location Type=CONST(External - Vendor)) Vendor.No.;
                                                   CaptionML=[ENU=External Entity No.;
                                                              PTG=N� Entidade Externa];
                                                   Description=V92.00#00033 }
    { 31022894;;Ship-to Code       ;Code20        ;TableRelation="Ship-to Address".Code WHERE (Customer No.=FIELD(External Entity No.));
                                                   CaptionML=[ENU=Ship-to Code;
                                                              PTG=C�d. Endere�o Envio];
                                                   Description=V92.00#00033 }
  }
  KEYS
  {
    {    ;No.                                     ;Clustered=Yes }
  }
  FIELDGROUPS
  {
    { 1   ;DropDown            ;No.,Transfer-from Code,Transfer-to Code,Posting Date,Transfer Order Date }
  }
  CODE
  {
    VAR
      DimMgt@1000 : Codeunit 408;
      ItemTrackingMgt@1001 : Codeunit 6500;

    PROCEDURE Navigate@2();
    VAR
      NavigateForm@1000 : Page 344;
    BEGIN
      NavigateForm.SetDoc("Posting Date","No.");
      NavigateForm.RUN;
    END;

    PROCEDURE PrintRecords@3(ShowRequestForm@1000 : Boolean);
    VAR
      ReportSelection@1001 : Record 77;
      TransRcptHeader@1002 : Record 5746;
    BEGIN
      WITH TransRcptHeader DO BEGIN
        COPY(Rec);
        ReportSelection.PrintWithGUIYesNo(ReportSelection.Usage::Inv3,TransRcptHeader,ShowRequestForm,'');
      END;
    END;

    PROCEDURE ShowDimensions@1();
    BEGIN
      DimMgt.ShowDimensionSet("Dimension Set ID",STRSUBSTNO('%1 %2',TABLECAPTION,"No."));
    END;

    PROCEDURE CopyFromTransferHeader@4(TransHeader@1000 : Record 5740);
    BEGIN
      "Transfer-from Code" := TransHeader."Transfer-from Code";
      "Transfer-from Name" := TransHeader."Transfer-from Name";
      "Transfer-from Name 2" := TransHeader."Transfer-from Name 2";
      "Transfer-from Address" := TransHeader."Transfer-from Address";
      "Transfer-from Address 2" := TransHeader."Transfer-from Address 2";
      "Transfer-from Post Code" := TransHeader."Transfer-from Post Code";
      "Transfer-from City" := TransHeader."Transfer-from City";
      "Transfer-from County" := TransHeader."Transfer-from County";
      "Trsf.-from Country/Region Code" := TransHeader."Trsf.-from Country/Region Code";
      "Transfer-from Contact" := TransHeader."Transfer-from Contact";
      "Transfer-to Code" := TransHeader."Transfer-to Code";
      "Transfer-to Name" := TransHeader."Transfer-to Name";
      "Transfer-to Name 2" := TransHeader."Transfer-to Name 2";
      "Transfer-to Address" := TransHeader."Transfer-to Address";
      "Transfer-to Address 2" := TransHeader."Transfer-to Address 2";
      "Transfer-to Post Code" := TransHeader."Transfer-to Post Code";
      "Transfer-to City" := TransHeader."Transfer-to City";
      "Transfer-to County" := TransHeader."Transfer-to County";
      "Trsf.-to Country/Region Code" := TransHeader."Trsf.-to Country/Region Code";
      "Transfer-to Contact" := TransHeader."Transfer-to Contact";
      "Transfer Order Date" := TransHeader."Posting Date";
      "Posting Date" := TransHeader."Posting Date";
      "Shipment Date" := TransHeader."Shipment Date";
      "Receipt Date" := TransHeader."Receipt Date";
      "Shortcut Dimension 1 Code" := TransHeader."Shortcut Dimension 1 Code";
      "Shortcut Dimension 2 Code" := TransHeader."Shortcut Dimension 2 Code";
      "Dimension Set ID" := TransHeader."Dimension Set ID";
      "Transfer Order No." := TransHeader."No.";
      "External Document No." := TransHeader."External Document No.";
      "In-Transit Code" := TransHeader."In-Transit Code";
      "Shipping Agent Code" := TransHeader."Shipping Agent Code";
      "Shipping Agent Service Code" := TransHeader."Shipping Agent Service Code";
      "Shipment Method Code" := TransHeader."Shipment Method Code";
      "Transaction Type" := TransHeader."Transaction Type";
      "Transport Method" := TransHeader."Transport Method";
      "Entry/Exit Point" := TransHeader."Entry/Exit Point";
      Area := TransHeader.Area;
      "Transaction Specification" := TransHeader."Transaction Specification";
    END;

    BEGIN
    {
      V92.00#00033 - NF - Ordens Transfer�ncia a Terceiros - EFD - 2016.03.14
    }
    END.
  }
}

