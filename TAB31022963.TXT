OBJECT Table 31022963 BP Territory
{
  OBJECT-PROPERTIES
  {
    Date=01/02/15;
    Time=13:00:00;
    Version List=NAVPTSS81.00;
  }
  PROPERTIES
  {
    CaptionML=[ENU=BP Territory;
               PTG=Territ�rios Banco Portugal];
    LookupPageID=Page31023039;
    DrillDownPageID=Page31023039;
  }
  FIELDS
  {
    { 1   ;   ;Code                ;Code3         ;CaptionML=[ENU=Code;
                                                              PTG=C�digo] }
    { 2   ;   ;Description         ;Text50        ;CaptionML=[ENU=Description;
                                                              PTG=Descri��o] }
  }
  KEYS
  {
    {    ;Code                                    ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    END.
  }
}

