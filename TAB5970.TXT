OBJECT Table 5970 Filed Service Contract Header
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0;
  }
  PROPERTIES
  {
    Permissions=TableData 5970=rimd,
                TableData 5971=rimd;
    OnDelete=BEGIN
               FiledContractLine.RESET;
               FiledContractLine.SETRANGE("Entry No.","Entry No.");
               FiledContractLine.DELETEALL;
             END;

    CaptionML=[ENU=Filed Service Contract Header;
               PTG=Cab. Contrato Servi�o Arquivado];
    LookupPageID=Page6073;
    DrillDownPageID=Page6073;
  }
  FIELDS
  {
    { 1   ;   ;Contract No.        ;Code20        ;CaptionML=[ENU=Contract No.;
                                                              PTG=N� Contrato] }
    { 2   ;   ;Contract Type       ;Option        ;CaptionML=[ENU=Contract Type;
                                                              PTG=Tipo Contrato];
                                                   OptionCaptionML=[ENU=Quote,Contract;
                                                                    PTG=Proposta,Contrato];
                                                   OptionString=Quote,Contract }
    { 3   ;   ;Description         ;Text50        ;CaptionML=[ENU=Description;
                                                              PTG=Descri��o] }
    { 4   ;   ;Description 2       ;Text50        ;CaptionML=[ENU=Description 2;
                                                              PTG=Descri��o 2] }
    { 5   ;   ;Status              ;Option        ;CaptionML=[ENU=Status;
                                                              PTG=Estado];
                                                   OptionCaptionML=[ENU=" ,Signed,Canceled";
                                                                    PTG=" ,Assinado,Cancelado"];
                                                   OptionString=[ ,Signed,Canceled];
                                                   Editable=Yes }
    { 6   ;   ;Change Status       ;Option        ;CaptionML=[ENU=Change Status;
                                                              PTG=Estado Altera��o];
                                                   OptionCaptionML=[ENU=Open,Locked;
                                                                    PTG=Aberto,Bloqueado];
                                                   OptionString=Open,Locked }
    { 7   ;   ;Customer No.        ;Code20        ;TableRelation=Customer;
                                                   CaptionML=[ENU=Customer No.;
                                                              PTG=N� Cliente];
                                                   NotBlank=Yes }
    { 8   ;   ;Name                ;Text50        ;CaptionML=[ENU=Name;
                                                              PTG=Nome];
                                                   Editable=No }
    { 9   ;   ;Address             ;Text50        ;CaptionML=[ENU=Address;
                                                              PTG=Endere�o];
                                                   Editable=No }
    { 10  ;   ;Address 2           ;Text50        ;CaptionML=[ENU=Address 2;
                                                              PTG=Endere�o 2];
                                                   Editable=No }
    { 11  ;   ;Post Code           ;Code20        ;TableRelation="Post Code";
                                                   ValidateTableRelation=No;
                                                   TestTableRelation=No;
                                                   CaptionML=[ENU=Post Code;
                                                              PTG=C�d. Postal];
                                                   Editable=No }
    { 12  ;   ;City                ;Text30        ;TableRelation="Post Code".City;
                                                   ValidateTableRelation=No;
                                                   TestTableRelation=No;
                                                   CaptionML=[ENU=City;
                                                              PTG=Cidade];
                                                   Editable=No }
    { 13  ;   ;Contact Name        ;Text50        ;CaptionML=[ENU=Contact Name;
                                                              PTG=Nome Contacto] }
    { 14  ;   ;Your Reference      ;Text35        ;CaptionML=[ENU=Your Reference;
                                                              PTG=Sua Refer�ncia] }
    { 15  ;   ;Salesperson Code    ;Code10        ;TableRelation=Salesperson/Purchaser;
                                                   CaptionML=[ENU=Salesperson Code;
                                                              PTG=C�d. Vendedor] }
    { 16  ;   ;Bill-to Customer No.;Code20        ;TableRelation=Customer;
                                                   CaptionML=[ENU=Bill-to Customer No.;
                                                              PTG=Fatura-a N� Cliente] }
    { 17  ;   ;Bill-to Name        ;Text50        ;CaptionML=[ENU=Bill-to Name;
                                                              PTG=Fatura-a Nome];
                                                   Editable=No }
    { 18  ;   ;Bill-to Address     ;Text50        ;CaptionML=[ENU=Bill-to Address;
                                                              PTG=Fatura-a Endere�o];
                                                   Editable=No }
    { 19  ;   ;Bill-to Address 2   ;Text50        ;CaptionML=[ENU=Bill-to Address 2;
                                                              PTG=Fatura-a Endere�o 2];
                                                   Editable=No }
    { 20  ;   ;Bill-to Post Code   ;Code20        ;TableRelation="Post Code";
                                                   ValidateTableRelation=No;
                                                   TestTableRelation=No;
                                                   CaptionML=[ENU=Bill-to Post Code;
                                                              PTG=Fatura-a C�d. Postal];
                                                   Editable=No }
    { 21  ;   ;Bill-to City        ;Text30        ;TableRelation="Post Code".City;
                                                   ValidateTableRelation=No;
                                                   TestTableRelation=No;
                                                   CaptionML=[ENU=Bill-to City;
                                                              PTG=Fatura-a Cidade];
                                                   Editable=No }
    { 22  ;   ;Ship-to Code        ;Code10        ;TableRelation="Ship-to Address".Code WHERE (Customer No.=FIELD(Customer No.));
                                                   CaptionML=[ENU=Ship-to Code;
                                                              PTG=Fatura-a C�d. Endere�o] }
    { 23  ;   ;Ship-to Name        ;Text50        ;CaptionML=[ENU=Ship-to Name;
                                                              PTG=Envio-a Nome];
                                                   Editable=No }
    { 24  ;   ;Ship-to Address     ;Text50        ;CaptionML=[ENU=Ship-to Address;
                                                              PTG=Envio-a Endere�o];
                                                   Editable=No }
    { 25  ;   ;Ship-to Address 2   ;Text50        ;CaptionML=[ENU=Ship-to Address 2;
                                                              PTG=Envio-a Endere�o 2];
                                                   Editable=No }
    { 26  ;   ;Ship-to Post Code   ;Code20        ;TableRelation="Post Code";
                                                   ValidateTableRelation=No;
                                                   TestTableRelation=No;
                                                   CaptionML=[ENU=Ship-to Post Code;
                                                              PTG=Envio-a C�d. Postal];
                                                   Editable=No }
    { 27  ;   ;Ship-to City        ;Text30        ;TableRelation="Post Code".City;
                                                   ValidateTableRelation=No;
                                                   TestTableRelation=No;
                                                   CaptionML=[ENU=Ship-to City;
                                                              PTG=Envio-a Localidade];
                                                   Editable=No }
    { 28  ;   ;Serv. Contract Acc. Gr. Code;Code10;TableRelation="Service Contract Account Group".Code;
                                                   CaptionML=[ENU=Serv. Contract Acc. Gr. Code;
                                                              PTG=C�d. Gr. Conta Contrato Servi�o] }
    { 32  ;   ;Invoice Period      ;Option        ;CaptionML=[ENU=Invoice Period;
                                                              PTG=Per�odo Fatura];
                                                   OptionCaptionML=[ENU=Month,Two Months,Quarter,Half Year,Year,None;
                                                                    PTG=M�s,Dois Meses,Trimestre,Semestre,Ano,Nenhum];
                                                   OptionString=Month,Two Months,Quarter,Half Year,Year,None }
    { 33  ;   ;Last Invoice Date   ;Date          ;CaptionML=[ENU=Last Invoice Date;
                                                              PTG=�lt. Data Fatura];
                                                   Editable=No }
    { 34  ;   ;Next Invoice Date   ;Date          ;CaptionML=[ENU=Next Invoice Date;
                                                              PTG=Pr�xima Data Fatura] }
    { 35  ;   ;Starting Date       ;Date          ;CaptionML=[ENU=Starting Date;
                                                              PTG=Data Inicial] }
    { 36  ;   ;Expiration Date     ;Date          ;CaptionML=[ENU=Expiration Date;
                                                              PTG=Data Expira��o] }
    { 38  ;   ;First Service Date  ;Date          ;CaptionML=[ENU=First Service Date;
                                                              PTG=Primeira Data Servi�o] }
    { 39  ;   ;Max. Labor Unit Price;Decimal      ;CaptionML=[ENU=Max. Labor Unit Price;
                                                              PTG=Pre�o Unit. M�x. Trabalho];
                                                   AutoFormatType=2;
                                                   AutoFormatExpr="Currency Code" }
    { 40  ;   ;Calcd. Annual Amount;Decimal       ;CaptionML=[ENU=Calcd. Annual Amount;
                                                              PTG=Valor Anual Calculado] }
    { 42  ;   ;Annual Amount       ;Decimal       ;CaptionML=[ENU=Annual Amount;
                                                              PTG=Valor Anual];
                                                   AutoFormatType=1 }
    { 43  ;   ;Amount per Period   ;Decimal       ;CaptionML=[ENU=Amount per Period;
                                                              PTG=Valor por Per�odo];
                                                   Editable=No;
                                                   AutoFormatType=1 }
    { 44  ;   ;Combine Invoices    ;Boolean       ;CaptionML=[ENU=Combine Invoices;
                                                              PTG=Juntar Faturas] }
    { 45  ;   ;Prepaid             ;Boolean       ;CaptionML=[ENU=Prepaid;
                                                              PTG=Pr�-Pago] }
    { 46  ;   ;Next Invoice Period ;Text30        ;CaptionML=[ENU=Next Invoice Period;
                                                              PTG=Pr�ximo Per�odo Fatura];
                                                   Editable=No }
    { 47  ;   ;Service Zone Code   ;Code10        ;TableRelation="Service Zone";
                                                   CaptionML=[ENU=Service Zone Code;
                                                              PTG=C�d. Zona Servi�o] }
    { 48  ;   ;Language Code       ;Code10        ;TableRelation=Language;
                                                   CaptionML=[ENU=Language Code;
                                                              PTG=C�d. Idioma] }
    { 50  ;   ;Cancel Reason Code  ;Code10        ;TableRelation="Reason Code";
                                                   CaptionML=[ENU=Cancel Reason Code;
                                                              PTG=C�d. Raz�o Cancelamento] }
    { 51  ;   ;Last Price Update Date;Date        ;CaptionML=[ENU=Last Price Update Date;
                                                              PTG=Data �lt. Atualiza��o Pre�o];
                                                   Editable=No }
    { 52  ;   ;Next Price Update Date;Date        ;CaptionML=[ENU=Next Price Update Date;
                                                              PTG=Data Pr�xima Atualiza��o Pre�o] }
    { 53  ;   ;Last Price Update % ;Decimal       ;CaptionML=[ENU=Last Price Update %;
                                                              PTG=�lt. atualiza��o pre�o %];
                                                   DecimalPlaces=0:5;
                                                   Editable=No }
    { 55  ;   ;Response Time (Hours);Decimal      ;CaptionML=[ENU=Response Time (Hours);
                                                              PTG=Tempo Resposta (Horas)];
                                                   DecimalPlaces=0:5 }
    { 56  ;   ;Contract Lines on Invoice;Boolean  ;CaptionML=[ENU=Contract Lines on Invoice;
                                                              PTG=Linhas Contrato em Fatura] }
    { 59  ;   ;Service Period      ;DateFormula   ;CaptionML=[ENU=Service Period;
                                                              PTG=Per�odo Servi�o] }
    { 60  ;   ;Payment Terms Code  ;Code10        ;TableRelation="Payment Terms";
                                                   CaptionML=[ENU=Payment Terms Code;
                                                              PTG=C�d. Termos Pagamento] }
    { 62  ;   ;Invoice after Service;Boolean      ;CaptionML=[ENU=Invoice after Service;
                                                              PTG=Fatura Ap�s Servi�o] }
    { 63  ;   ;Quote Type          ;Option        ;CaptionML=[ENU=Quote Type;
                                                              PTG=Tipo Proposta];
                                                   OptionCaptionML=[ENU=Quote 1.,Quote 2.,Quote 3.,Quote 4.,Quote 5.,Quote 6.,Quote 7.,Quote 8.;
                                                                    PTG=Proposta 1.,Proposta 2.,Proposta 3.,Proposta 4.,Proposta 5.,Proposta 6.,Proposta 7.,Proposta 8.];
                                                   OptionString=Quote 1.,Quote 2.,Quote 3.,Quote 4.,Quote 5.,Quote 6.,Quote 7.,Quote 8. }
    { 64  ;   ;Allow Unbalanced Amounts;Boolean   ;CaptionML=[ENU=Allow Unbalanced Amounts;
                                                              PTG=Permite Valores N�o Balanceados] }
    { 65  ;   ;Contract Group Code ;Code10        ;TableRelation="Contract Group";
                                                   CaptionML=[ENU=Contract Group Code;
                                                              PTG=C�d. Grupo Contrato] }
    { 66  ;   ;Service Order Type  ;Code10        ;TableRelation="Service Order Type";
                                                   CaptionML=[ENU=Service Order Type;
                                                              PTG=Tipo Ordem Servi�o] }
    { 67  ;   ;Shortcut Dimension 1 Code;Code20   ;TableRelation="Dimension Value".Code WHERE (Global Dimension No.=CONST(1));
                                                   CaptionML=[ENU=Shortcut Dimension 1 Code;
                                                              PTG=C�d. Atalho Dimens�o 1];
                                                   CaptionClass='1,2,1' }
    { 68  ;   ;Shortcut Dimension 2 Code;Code20   ;TableRelation="Dimension Value".Code WHERE (Global Dimension No.=CONST(2));
                                                   CaptionML=[ENU=Shortcut Dimension 2 Code;
                                                              PTG=C�d. Atalho Dimens�o 2];
                                                   CaptionClass='1,2,2' }
    { 69  ;   ;Accept Before       ;Date          ;CaptionML=[ENU=Accept Before;
                                                              PTG=Aceitar Antes] }
    { 71  ;   ;Automatic Credit Memos;Boolean     ;CaptionML=[ENU=Automatic Credit Memos;
                                                              PTG=Notas Cr�dito Autom�ticas] }
    { 74  ;   ;Template No.        ;Code20        ;CaptionML=[ENU=Template No.;
                                                              PTG=N� Modelo] }
    { 75  ;   ;Price Update Period ;DateFormula   ;InitValue=1Y;
                                                   CaptionML=[ENU=Price Update Period;
                                                              PTG=Per�odo Atualiza��o Pre�o] }
    { 79  ;   ;Price Inv. Increase Code;Code20    ;TableRelation="Standard Text";
                                                   CaptionML=[ENU=Price Inv. Increase Code;
                                                              PTG=C�d. Aumento Pre�o Fatura] }
    { 80  ;   ;Print Increase Text ;Boolean       ;CaptionML=[ENU=Print Increase Text;
                                                              PTG=Imprimir Texto Autom�tico] }
    { 81  ;   ;Currency Code       ;Code10        ;TableRelation=Currency;
                                                   CaptionML=[ENU=Currency Code;
                                                              PTG=C�d. Divisa] }
    { 82  ;   ;No. Series          ;Code10        ;TableRelation="No. Series";
                                                   CaptionML=[ENU=No. Series;
                                                              PTG=N�s S�ries] }
    { 83  ;   ;Probability         ;Decimal       ;InitValue=100;
                                                   CaptionML=[ENU=Probability;
                                                              PTG=Probabilidade];
                                                   DecimalPlaces=0:5 }
    { 85  ;   ;Responsibility Center;Code10       ;TableRelation="Responsibility Center";
                                                   CaptionML=[ENU=Responsibility Center;
                                                              PTG=Centro Responsabilidade] }
    { 86  ;   ;Phone No.           ;Text30        ;ExtendedDatatype=Phone No.;
                                                   CaptionML=[ENU=Phone No.;
                                                              PTG=Telefone] }
    { 87  ;   ;Fax No.             ;Text30        ;CaptionML=[ENU=Fax No.;
                                                              PTG=N� Fax] }
    { 88  ;   ;E-Mail              ;Text80        ;ExtendedDatatype=E-Mail;
                                                   CaptionML=[ENU=Email;
                                                              PTG=Email] }
    { 89  ;   ;Bill-to County      ;Text30        ;CaptionML=[ENU=Bill-to County;
                                                              PTG=Fatura-a Distrito] }
    { 90  ;   ;County              ;Text30        ;CaptionML=[ENU=County;
                                                              PTG=Distrito] }
    { 91  ;   ;Ship-to County      ;Text30        ;CaptionML=[ENU=Ship-to County;
                                                              PTG=Envio-a Distrito] }
    { 92  ;   ;Country/Region Code ;Code10        ;TableRelation=Country/Region;
                                                   CaptionML=[ENU=Country/Region Code;
                                                              PTG=C�digo Pa�s/Regi�o] }
    { 93  ;   ;Bill-to Country/Region Code;Code10 ;TableRelation=Country/Region;
                                                   CaptionML=[ENU=Bill-to Country/Region Code;
                                                              PTG=Fatura-a C�d. Pa�s/Regi�o] }
    { 94  ;   ;Ship-to Country/Region Code;Code10 ;TableRelation=Country/Region;
                                                   CaptionML=[ENU=Ship-to Country/Region Code;
                                                              PTG=Envio-a C�d. Pa�s/Regi�o] }
    { 95  ;   ;Name 2              ;Text50        ;CaptionML=[ENU=Name 2;
                                                              PTG=Nome 2];
                                                   Editable=No }
    { 96  ;   ;Bill-to Name 2      ;Text50        ;CaptionML=[ENU=Bill-to Name 2;
                                                              PTG=Fatura-a Nome 2];
                                                   Editable=No }
    { 97  ;   ;Ship-to Name 2      ;Text50        ;CaptionML=[ENU=Ship-to Name 2;
                                                              PTG=Envio-a Nome 2];
                                                   Editable=No }
    { 98  ;   ;Next Invoice Period Start;Date     ;CaptionML=[ENU=Next Invoice Period Start;
                                                              PTG=In�cio Per�odo Pr�xima Fatura];
                                                   Editable=No }
    { 99  ;   ;Next Invoice Period End;Date       ;CaptionML=[ENU=Next Invoice Period End;
                                                              PTG=Fim Per�odo Pr�xima Fatura];
                                                   Editable=No }
    { 100 ;   ;Entry No.           ;Integer       ;CaptionML=[ENU=Entry No.;
                                                              PTG=N� Mov.] }
    { 101 ;   ;File Date           ;Date          ;CaptionML=[ENU=File Date;
                                                              PTG=Data Arquivo] }
    { 102 ;   ;File Time           ;Time          ;CaptionML=[ENU=File Time;
                                                              PTG=Tempo Arquivo] }
    { 103 ;   ;Filed By            ;Code50        ;TableRelation=User."User Name";
                                                   OnLookup=VAR
                                                              UserMgt@1000 : Codeunit 418;
                                                            BEGIN
                                                              UserMgt.LookupUserID("Filed By");
                                                            END;

                                                   TestTableRelation=No;
                                                   CaptionML=[ENU=Filed By;
                                                              PTG=Arquivado por] }
    { 104 ;   ;Reason for Filing   ;Option        ;CaptionML=[ENU=Reason for Filing;
                                                              PTG=Raz�o para Arquivar];
                                                   OptionCaptionML=[ENU=" ,Contract Signed,Contract Canceled";
                                                                    PTG=" ,Contrato Assinado,Contrato Cancelado"];
                                                   OptionString=[ ,Contract Signed,Contract Canceled] }
    { 105 ;   ;Contract Type Relation;Option      ;CaptionML=[ENU=Contract Type Relation;
                                                              PTG=Rela��o Tipo Contrato];
                                                   OptionCaptionML=[ENU=Quote,Contract;
                                                                    PTG=Proposta,Contrato];
                                                   OptionString=Quote,Contract }
    { 106 ;   ;Contract No. Relation;Code20       ;TableRelation="Service Contract Header"."Contract No." WHERE (Contract Type=FIELD(Contract Type Relation));
                                                   CaptionML=[ENU=Contract No. Relation;
                                                              PTG=Rela��o N� Contrato] }
    { 480 ;   ;Dimension Set ID    ;Integer       ;TableRelation="Dimension Set Entry";
                                                   OnLookup=BEGIN
                                                              ShowDimensions;
                                                            END;

                                                   CaptionML=[ENU=Dimension Set ID;
                                                              PTG=ID Combina��o Dimens�o];
                                                   Editable=No }
    { 5050;   ;Contact No.         ;Code20        ;CaptionML=[ENU=Contact No.;
                                                              PTG=N� Contacto] }
    { 5051;   ;Bill-to Contact No. ;Code20        ;AccessByPermission=TableData 5771=R;
                                                   CaptionML=[ENU=Bill-to Contact No.;
                                                              PTG=N� Contacto p/ Fatura] }
    { 5052;   ;Bill-to Contact     ;Text50        ;CaptionML=[ENU=Bill-to Contact;
                                                              PTG=Contacto p/ Fatura] }
  }
  KEYS
  {
    {    ;Entry No.                               ;Clustered=Yes }
    {    ;Contract Type Relation,Contract No. Relation,File Date,File Time }
    {    ;Contract Type,Contract No.               }
  }
  FIELDGROUPS
  {
  }
  CODE
  {
    VAR
      FiledServContractHeader@1000 : Record 5970;
      FiledContractLine@1001 : Record 5971;
      DimMgt@1006 : Codeunit 408;
      SigningQuotation@1003 : Boolean;
      CancelContract@1004 : Boolean;
      Text027@1005 : TextConst 'ENU=%1 to %2;PTG=%1 a %2';

    PROCEDURE FileContract@1(ServContractHeader@1000 : Record 5965);
    VAR
      ServContractLine@1002 : Record 5964;
      NextEntryNo@1001 : Integer;
    BEGIN
      WITH ServContractHeader DO BEGIN
        TESTFIELD("Contract No.");

        FiledContractLine.LOCKTABLE;
        FiledServContractHeader.LOCKTABLE;

        FiledServContractHeader.RESET;
        IF FiledServContractHeader.FINDLAST THEN
          NextEntryNo := FiledServContractHeader."Entry No." + 1
        ELSE
          NextEntryNo := 1;

        FiledServContractHeader.INIT;
        CALCFIELDS(
          Name,Address,"Address 2","Post Code",City,County,"Country/Region Code","Name 2",
          "Bill-to Name","Bill-to Address","Bill-to Address 2","Bill-to Post Code",
          "Bill-to City","Bill-to County","Bill-to Country/Region Code","Bill-to Name 2",
          "Calcd. Annual Amount");
        IF "Ship-to Code" = '' THEN BEGIN
          "Ship-to Name" := Name;
          "Ship-to Address" := Address;
          "Ship-to Address 2" := "Address 2";
          "Ship-to Post Code" := "Post Code";
          "Ship-to City" := City;
          "Ship-to County" := County;
          "Ship-to Country/Region Code" := "Country/Region Code";
          "Ship-to Name 2" := "Name 2";
        END ELSE
          CALCFIELDS(
            "Ship-to Name","Ship-to Address","Ship-to Address 2","Ship-to Post Code","Ship-to City",
            "Ship-to County","Ship-to Country/Region Code","Ship-to Name 2");

        FiledServContractHeader.TRANSFERFIELDS(ServContractHeader);

        IF SigningQuotation THEN
          FiledServContractHeader."Reason for Filing" :=
            FiledServContractHeader."Reason for Filing"::"Contract Signed";

        IF CancelContract THEN
          FiledServContractHeader."Reason for Filing" :=
            FiledServContractHeader."Reason for Filing"::"Contract Canceled";

        FiledServContractHeader."Contract Type Relation" := "Contract Type";
        FiledServContractHeader."Contract No. Relation" := "Contract No.";
        FiledServContractHeader."Entry No." := NextEntryNo;
        FiledServContractHeader."File Date" := TODAY;
        FiledServContractHeader."File Time" := TIME;
        FiledServContractHeader."Filed By" := USERID;
        FiledServContractHeader.Name := Name;
        FiledServContractHeader.Address := Address;
        FiledServContractHeader."Address 2" := "Address 2";
        FiledServContractHeader."Post Code" := "Post Code";
        FiledServContractHeader.City := City;
        FiledServContractHeader."Bill-to Name" := "Bill-to Name";
        FiledServContractHeader."Bill-to Address" := "Bill-to Address";
        FiledServContractHeader."Bill-to Address 2" := "Bill-to Address 2";
        FiledServContractHeader."Bill-to Post Code" := "Bill-to Post Code";
        FiledServContractHeader."Bill-to City" := "Bill-to City";
        FiledServContractHeader."Ship-to Name" := "Ship-to Name";
        FiledServContractHeader."Ship-to Address" := "Ship-to Address";
        FiledServContractHeader."Ship-to Address 2" := "Ship-to Address 2";
        FiledServContractHeader."Ship-to Post Code" := "Ship-to Post Code";
        FiledServContractHeader."Ship-to City" := "Ship-to City";
        FiledServContractHeader."Calcd. Annual Amount" := "Calcd. Annual Amount";
        FiledServContractHeader."Bill-to County" := "Bill-to County";
        FiledServContractHeader.County := County;
        FiledServContractHeader."Ship-to County" := "Ship-to County";
        FiledServContractHeader."Country/Region Code" := "Country/Region Code";
        FiledServContractHeader."Bill-to Country/Region Code" := "Bill-to Country/Region Code";
        FiledServContractHeader."Ship-to Country/Region Code" := "Ship-to Country/Region Code";
        FiledServContractHeader."Name 2" := "Name 2";
        FiledServContractHeader."Bill-to Name 2" := "Bill-to Name 2";
        FiledServContractHeader."Ship-to Name 2" := "Ship-to Name 2";
        FiledServContractHeader.INSERT;

        ServContractLine.RESET;
        ServContractLine.SETRANGE("Contract Type","Contract Type");
        ServContractLine.SETRANGE("Contract No.","Contract No.");
        IF ServContractLine.FIND('-') THEN
          REPEAT
            FiledContractLine.INIT;
            FiledContractLine."Entry No." := FiledServContractHeader."Entry No.";
            FiledContractLine.TRANSFERFIELDS(ServContractLine);
            FiledContractLine.INSERT;
          UNTIL ServContractLine.NEXT = 0;
      END;
    END;

    PROCEDURE FileQuotationBeforeSigning@4(ServContract@1000 : Record 5965);
    BEGIN
      SigningQuotation := TRUE;
      FileContract(ServContract);
      SigningQuotation := FALSE;
    END;

    PROCEDURE FileContractBeforeCancellation@7(ServContract@1000 : Record 5965);
    BEGIN
      CancelContract := TRUE;
      FileContract(ServContract);
      CancelContract := FALSE;
    END;

    PROCEDURE NextInvoicePeriod@8() : Text[250];
    BEGIN
      IF ("Next Invoice Period Start" <> 0D) AND ("Next Invoice Period End" <> 0D) THEN
        EXIT(STRSUBSTNO(Text027,"Next Invoice Period Start","Next Invoice Period End"));
    END;

    LOCAL PROCEDURE ShowDimensions@2();
    BEGIN
      DimMgt.ShowDimensionSet("Dimension Set ID",STRSUBSTNO('%1 %2',TABLECAPTION,"Contract No."));
    END;

    BEGIN
    END.
  }
}

