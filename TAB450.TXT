OBJECT Table 450 Bar Chart Buffer
{
  OBJECT-PROPERTIES
  {
    Date=05/08/11;
    Time=12:00:00;
    Version List=NAVW16.00;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Bar Chart Buffer;
               PTG=Mem. Int. Gr�fico Barras];
  }
  FIELDS
  {
    { 1   ;   ;Series No.          ;Integer       ;CaptionML=[ENU=Series No.;
                                                              PTG=N� S�ries] }
    { 2   ;   ;Column No.          ;Integer       ;CaptionML=[ENU=Column No.;
                                                              PTG=N� Coluna] }
    { 3   ;   ;Y Value             ;Decimal       ;CaptionML=[ENU=Y Value;
                                                              PTG=Valor Y] }
    { 4   ;   ;X Value             ;Text100       ;CaptionML=[ENU=X Value;
                                                              PTG=Valor X] }
    { 5   ;   ;Tag                 ;Text250       ;CaptionML=[ENU=Tag;
                                                              PTG=Etiqueta] }
  }
  KEYS
  {
    {    ;Series No.                              ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    END.
  }
}

