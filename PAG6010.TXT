OBJECT Page 6010 Res.Gr. Availability (Service)
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Res.Gr. Availability (Service);
               PTG=Disponibilidade Fam��la Recurso (Servi�o)];
    SaveValues=Yes;
    InsertAllowed=No;
    DeleteAllowed=No;
    LinksAllowed=No;
    SourceTable=Table152;
    DataCaptionExpr='';
    PageType=Card;
    OnOpenPage=BEGIN
                 ServMgtSetup.GET;
                 SetColumns(SetWanted::Initial);
               END;

    ActionList=ACTIONS
    {
      { 1900000004;0 ;ActionContainer;
                      ActionContainerType=ActionItems }
      { 77      ;1   ;Action    ;
                      Name=ShowMatrix;
                      CaptionML=[ENU=&Show Matrix;
                                 PTG=&Mostrar Matriz];
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=ShowMatrix;
                      PromotedCategory=Process;
                      OnAction=VAR
                                 MatrixForm@1117 : Page 9221;
                               BEGIN
                                 MatrixForm.SetData(
                                   CurrentDocumentType,CurrentDocumentNo,CurrentEntryNo,
                                   MatrixColumnCaptions,MatrixRecords,PeriodType);
                                 MatrixForm.SETTABLEVIEW(Rec);
                                 MatrixForm.RUNMODAL;
                               END;
                                }
      { 6       ;1   ;Action    ;
                      CaptionML=[ENU=Next Set;
                                 PTG=Conjunto Seguinte];
                      ToolTipML=[ENU=Go to the next set of data;
                                 PTG=Ir para o conjunto de dados seguinte];
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=NextSet;
                      PromotedCategory=Process;
                      OnAction=BEGIN
                                 SetColumns(SetWanted::Next);
                               END;
                                }
      { 3       ;1   ;Action    ;
                      CaptionML=[ENU=Previous Set;
                                 PTG=Conjunto Anterior];
                      ToolTipML=[ENU=Go to the next set of data;
                                 PTG=Ir para o conjunto de dados anterior];
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=PreviousSet;
                      PromotedCategory=Process;
                      OnAction=BEGIN
                                 SetColumns(SetWanted::Previous);
                               END;
                                }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 22  ;1   ;Group     ;
                CaptionML=[ENU=Matrix Options;
                           PTG=Op��es de Matriz] }

    { 4   ;2   ;Field     ;
                CaptionML=[ENU=View by;
                           PTG=Ver por];
                ToolTipML=[ENU=Specifies by which period amounts are displayed.;
                           PTG=""];
                OptionCaptionML=[ENU=Day,Week,Month,Quarter,Year,Accounting Period;
                                 PTG=Dia,Semana,M�s,Trimestre,Ano,Per�odo Contabil�stico];
                SourceExpr=PeriodType;
                OnValidate=BEGIN
                             DateControl;
                             SetColumns(SetWanted::Initial);
                           END;
                            }

    { 5   ;2   ;Field     ;
                CaptionML=[ENU=Date Filter;
                           PTG=Filtro Data];
                SourceExpr=DateFilter;
                OnValidate=BEGIN
                             DateControl;
                             SetColumns(SetWanted::Initial);
                             DateFilterOnAfterValidate;
                           END;
                            }

    { 2   ;2   ;Field     ;
                CaptionML=[ENU=Column set;
                           PTG=Conjunto de colunas];
                SourceExpr=ColumnsSet;
                Editable=FALSE }

  }
  CODE
  {
    VAR
      MatrixRecords@1042 : ARRAY [32] OF Record 2000000007;
      ResRec2@1041 : Record 156;
      ServMgtSetup@1001 : Record 5911;
      ApplicationManagement@1039 : Codeunit 1;
      CurrentDocumentType@1019 : Integer;
      CurrentDocumentNo@1007 : Code[20];
      CurrentEntryNo@1009 : Integer;
      PeriodType@1012 : 'Day,Week,Month,Quarter,Year,Accounting Period';
      DateFilter@1035 : Text;
      SetWanted@1049 : 'Initial,Previous,Same,Next';
      PKFirstRecInCurrSet@1048 : Text[1024];
      MatrixColumnCaptions@1047 : ARRAY [32] OF Text[100];
      ColumnsSet@1046 : Text[1024];
      CurrSetLength@1045 : Integer;

    PROCEDURE SetData@2(DocumentType@1003 : Integer;DocumentNo@1000 : Code[20];EntryNo@1002 : Integer);
    BEGIN
      CurrentDocumentType := DocumentType;
      CurrentDocumentNo := DocumentNo;
      CurrentEntryNo := EntryNo;
    END;

    LOCAL PROCEDURE DateControl@6();
    BEGIN
      IF ApplicationManagement.MakeDateFilter(DateFilter) = 0 THEN;
      ResRec2.SETFILTER("Date Filter",DateFilter);
      DateFilter := ResRec2.GETFILTER("Date Filter");
    END;

    PROCEDURE SetColumns@11(SetWanted@1001 : 'Initial,Previous,Same,Next');
    VAR
      MatrixMgt@1000 : Codeunit 9200;
    BEGIN
      MatrixMgt.GeneratePeriodMatrixData(SetWanted,32,FALSE,PeriodType,DateFilter,
        PKFirstRecInCurrSet,MatrixColumnCaptions,ColumnsSet,CurrSetLength,MatrixRecords);
    END;

    LOCAL PROCEDURE DateFilterOnAfterValidate@19006009();
    BEGIN
      CurrPage.UPDATE;
    END;

    BEGIN
    END.
  }
}

