OBJECT Page 7710 ADCS Users
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0;
  }
  PROPERTIES
  {
    CaptionML=[ENU=ADCS Users;
               PTG=Utilizadores ADCS];
    SourceTable=Table7710;
    DelayedInsert=Yes;
    PageType=List;
  }
  CONTROLS
  {
    { 1   ;0   ;Container ;
                ContainerType=ContentArea }

    { 2   ;1   ;Group     ;
                Name=Group;
                CaptionML=[ENU=Group;
                           PTG=Agrupar];
                GroupType=Repeater }

    { 4   ;2   ;Field     ;
                CaptionML=[ENU=Name;
                           PTG=Nome];
                ToolTipML=[ENU=Specifies the name of an ADCS user.;
                           PTG=""];
                SourceExpr=Name }

    { 5   ;2   ;Field     ;
                ExtendedDatatype=Masked;
                CaptionML=[ENU=Password;
                           PTG=Palavra-passe];
                ToolTipML=[ENU=Specifies the password of an ADCS user.;
                           PTG=""];
                SourceExpr=Password }

  }
  CODE
  {

    BEGIN
    END.
  }
}

