OBJECT Table 86 Exch. Rate Adjmt. Reg.
{
  OBJECT-PROPERTIES
  {
    Date=15/09/15;
    Time=12:00:00;
    Version List=NAVW19.00;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Exch. Rate Adjmt. Reg.;
               PTG=Hist�rico Ajuste Divisas];
    LookupPageID=Page106;
  }
  FIELDS
  {
    { 1   ;   ;No.                 ;Integer       ;CaptionML=[ENU=No.;
                                                              PTG=N�] }
    { 2   ;   ;Creation Date       ;Date          ;CaptionML=[ENU=Creation Date;
                                                              PTG=Data Cria��o] }
    { 3   ;   ;Account Type        ;Option        ;CaptionML=[ENU=Account Type;
                                                              PTG=Tipo Conta];
                                                   OptionCaptionML=[ENU=G/L Account,Customer,Vendor,Bank Account;
                                                                    PTG=Conta C/G,Cliente,Fornecedor,Conta Banc�ria];
                                                   OptionString=G/L Account,Customer,Vendor,Bank Account }
    { 4   ;   ;Posting Group       ;Code10        ;TableRelation=IF (Account Type=CONST(Customer)) "Customer Posting Group"
                                                                 ELSE IF (Account Type=CONST(Vendor)) "Vendor Posting Group"
                                                                 ELSE IF (Account Type=CONST(Bank Account)) "Bank Account Posting Group";
                                                   CaptionML=[ENU=Posting Group;
                                                              PTG=Gr. Contabil�stico] }
    { 5   ;   ;Currency Code       ;Code10        ;TableRelation=Currency;
                                                   CaptionML=[ENU=Currency Code;
                                                              PTG=C�d. Divisa] }
    { 6   ;   ;Currency Factor     ;Decimal       ;CaptionML=[ENU=Currency Factor;
                                                              PTG=Factor Divisa];
                                                   DecimalPlaces=0:15;
                                                   MinValue=0 }
    { 7   ;   ;Adjusted Base       ;Decimal       ;CaptionML=[ENU=Adjusted Base;
                                                              PTG=Valor Base Ajustado];
                                                   AutoFormatType=1;
                                                   AutoFormatExpr="Currency Code" }
    { 8   ;   ;Adjusted Base (LCY) ;Decimal       ;CaptionML=[ENU=Adjusted Base (LCY);
                                                              PTG=Valor Base Ajustado (DL)];
                                                   AutoFormatType=1 }
    { 9   ;   ;Adjusted Amt. (LCY) ;Decimal       ;CaptionML=[ENU=Adjusted Amt. (LCY);
                                                              PTG=Valor Ajustado (DL)];
                                                   AutoFormatType=1 }
    { 10  ;   ;Adjusted Base (Add.-Curr.);Decimal ;AccessByPermission=TableData 4=R;
                                                   CaptionML=[ENU=Adjusted Base (Add.-Curr.);
                                                              PTG=Valor Base Ajustado (Div.Adic)];
                                                   AutoFormatType=1;
                                                   AutoFormatExpr=GetCurrencyCodeFromGLSetup }
    { 11  ;   ;Adjusted Amt. (Add.-Curr.);Decimal ;AccessByPermission=TableData 4=R;
                                                   CaptionML=[ENU=Adjusted Amt. (Add.-Curr.);
                                                              PTG=Valor Ajustado (Div.-Adic.)];
                                                   AutoFormatType=1;
                                                   AutoFormatExpr=GetCurrencyCodeFromGLSetup }
  }
  KEYS
  {
    {    ;No.                                     ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    LOCAL PROCEDURE GetCurrencyCodeFromGLSetup@1() : Code[10];
    VAR
      GLSetup@1000 : Record 98;
    BEGIN
      GLSetup.GET;
      EXIT(GLSetup."Additional Reporting Currency");
    END;

    BEGIN
    END.
  }
}

