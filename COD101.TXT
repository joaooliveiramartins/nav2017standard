OBJECT Codeunit 101 Cust. Entry-SetAppl.ID
{
  OBJECT-PROPERTIES
  {
    Date=01/02/15;
    Time=13:00:00;
    Version List=NAVW17.00,NAVPTSS81.00;
  }
  PROPERTIES
  {
    Permissions=TableData 21=imd;
    OnRun=BEGIN
          END;

  }
  CODE
  {
    VAR
      CustEntryApplID@1000 : Code[50];
      "//--soft-text--//"@90000001 : TextConst;
      Text31022890@1110100 : TextConst 'ENU=%1 cannot be applied, since it is included in a bill group.;PTG=%1 n�o se pode liquidar, j� que est� inclu�do numa remessa.';
      Text31022891@1110101 : TextConst 'ENU=" Remove it from its bill group and try again.";PTG=" Retire-o da remessa e tente novamente."';
      "//--soft-global--//"@90000000 : Integer;
      Doc@1110102 : Record 31022935;
      ApplicationManagement@1110000 : Codeunit 1;

    PROCEDURE SetApplId@1(VAR CustLedgEntry@1000 : Record 21;ApplyingCustLedgEntry@1003 : Record 21;AppliesToID@1001 : Code[50]);
    BEGIN
      CustLedgEntry.LOCKTABLE;
      IF CustLedgEntry.FIND('-') THEN BEGIN
        // Make Applies-to ID
        IF CustLedgEntry."Applies-to ID" <> '' THEN
          CustEntryApplID := ''
        ELSE BEGIN
          CustEntryApplID := AppliesToID;
          IF CustEntryApplID = '' THEN BEGIN
            CustEntryApplID := USERID;
            IF CustEntryApplID = '' THEN
              CustEntryApplID := '***';
          END;
        END;

        // Set Applies-to ID
        REPEAT
          CustLedgEntry.TESTFIELD(Open,TRUE);
          //soft,sn
          IF CustLedgEntry."Document Situation" = CustLedgEntry."Document Situation"::"Posted BG/PO" THEN
            ERROR(Text31022890,CustLedgEntry.Description);

          IF (((CustLedgEntry."Document Type" = CustLedgEntry."Document Type"::"7") OR
               (CustLedgEntry."Document Type" = CustLedgEntry."Document Type"::Invoice))) AND
             ApplicationManagement.AccessToCartera THEN
            IF Doc.GET(Doc.Type::Receivable,CustLedgEntry."Entry No.") THEN
              IF Doc."Bill Gr./Pmt. Order No." <> '' THEN
                ERROR(Text31022890 + Text31022891, CustLedgEntry.Description);
           //soft,en
          CustLedgEntry."Applies-to ID" := CustEntryApplID;
          IF CustLedgEntry."Applies-to ID" = '' THEN BEGIN
            CustLedgEntry."Accepted Pmt. Disc. Tolerance" := FALSE;
            CustLedgEntry."Accepted Payment Tolerance" := 0;
          END;
          // Set Amount to Apply
          IF ((CustLedgEntry."Amount to Apply" <> 0) AND (CustEntryApplID = '')) OR
             (CustEntryApplID = '')
          THEN
            CustLedgEntry."Amount to Apply" := 0
          ELSE
            IF CustLedgEntry."Amount to Apply" = 0 THEN BEGIN
              CustLedgEntry.CALCFIELDS("Remaining Amount");
              CustLedgEntry."Amount to Apply" := CustLedgEntry."Remaining Amount"
            END;

          IF CustLedgEntry."Entry No." = ApplyingCustLedgEntry."Entry No." THEN
            CustLedgEntry."Applying Entry" := ApplyingCustLedgEntry."Applying Entry";
          CustLedgEntry.MODIFY;
        UNTIL CustLedgEntry.NEXT = 0;
      END;
    END;

    BEGIN
    END.
  }
}

