OBJECT Page 1402 Purchase No. Series Setup
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Purchase No. Series Setup;
               PTG=Config. N�s S�ries Compra];
    InsertAllowed=No;
    DeleteAllowed=No;
    SourceTable=Table312;
    PageType=ListPlus;
    ActionList=ACTIONS
    {
      { 3       ;    ;ActionContainer;
                      ActionContainerType=ActionItems }
      { 4       ;1   ;Action    ;
                      Name=Setup;
                      CaptionML=[ENU=Purchases & Payables Setup;
                                 PTG=Config. Compras & Pagamentos];
                      ToolTipML=ENU=View company policies for purchase invoicing and returns and offers actions to set up codes and values that you use in purchases and payables.;
                      ApplicationArea=#Basic,#Suite;
                      RunObject=Page 460;
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=Setup;
                      PromotedCategory=Process }
    }
  }
  CONTROLS
  {
    { 35  ;0   ;Container ;
                ContainerType=ContentArea }

    { 14  ;1   ;Group     ;
                CaptionML=[ENU=Numbering;
                           PTG=Numera��o];
                InstructionalTextML=[ENU=To fill the Document No. field automatically, you must set up a number series.;
                                     PTG=Para preencher o N� Documento automaticamente, deve configurar um n�mero s�ries.] }

    { 12  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the code for the number series that will be used to assign numbers to purchase quotes. To see the number series that have been set up in the No. Series table, click the field.;
                ApplicationArea=#All;
                SourceExpr="Quote Nos.";
                Visible=QuoteNosVisible }

    { 11  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the code for the number series that will be used to assign numbers to blanket purchase orders. To see the number series that have been set up in the No. Series table, click the field.;
                ApplicationArea=#All;
                SourceExpr="Blanket Order Nos.";
                Visible=BlanketOrderNosVisible }

    { 10  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the code for the number series that will be used to assign numbers to purchase orders. To see the number series that have been set up in the No. Series table, click the field.;
                ApplicationArea=#All;
                SourceExpr="Order Nos.";
                Visible=OrderNosVisible }

    { 9   ;2   ;Field     ;
                ToolTipML=ENU=Specifies the number series that is used to assign numbers to new purchase return orders.;
                ApplicationArea=#All;
                SourceExpr="Return Order Nos.";
                Visible=ReturnOrderNosVisible }

    { 8   ;2   ;Field     ;
                ToolTipML=ENU=Specifies the code for the number series that will be used to assign numbers to purchase invoices. To see the number series that have been set up in the No. Series table, click the field.;
                ApplicationArea=#All;
                SourceExpr="Invoice Nos.";
                Visible=InvoiceNosVisible }

    { 6   ;2   ;Field     ;
                ToolTipML=ENU=Specifies the code for the number series that will be used to assign numbers to purchase credit memos. To see the number series that have been set up in the No. Series table, click the field.;
                ApplicationArea=#All;
                SourceExpr="Credit Memo Nos.";
                Visible=CreditMemoNosVisible }

  }
  CODE
  {
    VAR
      QuoteNosVisible@1000 : Boolean;
      BlanketOrderNosVisible@1001 : Boolean;
      OrderNosVisible@1002 : Boolean;
      ReturnOrderNosVisible@1003 : Boolean;
      InvoiceNosVisible@1004 : Boolean;
      CreditMemoNosVisible@1005 : Boolean;

    PROCEDURE SetFieldsVisibility@1(DocType@1000 : 'Quote,Order,Invoice,Credit Memo,Blanket Order,Return Order');
    BEGIN
      QuoteNosVisible := (DocType = DocType::Quote);
      BlanketOrderNosVisible := (DocType = DocType::"Blanket Order");
      OrderNosVisible := (DocType = DocType::Order);
      ReturnOrderNosVisible := (DocType = DocType::"Return Order");
      InvoiceNosVisible := (DocType = DocType::Invoice);
      CreditMemoNosVisible := (DocType = DocType::"Credit Memo");
    END;

    BEGIN
    END.
  }
}

