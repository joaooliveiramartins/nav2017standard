OBJECT Table 5641 FA Buffer Projection
{
  OBJECT-PROPERTIES
  {
    Date=05/08/11;
    Time=12:00:00;
    Version List=NAVW16.00;
  }
  PROPERTIES
  {
    CaptionML=[ENU=FA Buffer Projection;
               PTG=Mem. Int. Imob. Proje��o];
  }
  FIELDS
  {
    { 1   ;   ;Entry No.           ;Integer       ;CaptionML=[ENU=Entry No.;
                                                              PTG=N� Mov.] }
    { 2   ;   ;FA Posting Date     ;Date          ;CaptionML=[ENU=FA Posting Date;
                                                              PTG=Data Registo Imob.] }
    { 3   ;   ;Depreciation        ;Decimal       ;CaptionML=[ENU=Depreciation;
                                                              PTG=Amortiza��o];
                                                   AutoFormatType=1 }
    { 4   ;   ;Custom 1            ;Decimal       ;CaptionML=[ENU=Custom 1;
                                                              PTG=Especial 1];
                                                   AutoFormatType=1 }
    { 5   ;   ;Code Name           ;Code20        ;CaptionML=[ENU=Code Name;
                                                              PTG=Nome C�digo] }
  }
  KEYS
  {
    {    ;Code Name,FA Posting Date,Entry No.     ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    END.
  }
}

