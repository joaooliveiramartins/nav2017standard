OBJECT Page 5745 Posted Transfer Receipt
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0,NAVPTSS92.00;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Posted Transfer Receipt;
               PTG=Rece��o Transf. Registado];
    InsertAllowed=No;
    SourceTable=Table5746;
    PageType=Document;
    RefreshOnActivate=Yes;
    ActionList=ACTIONS
    {
      { 1900000003;0 ;ActionContainer;
                      ActionContainerType=RelatedInformation }
      { 50      ;1   ;ActionGroup;
                      CaptionML=[ENU=&Receipt;
                                 PTG=&Rece��o];
                      Image=Receipt }
      { 56      ;2   ;Action    ;
                      ShortCutKey=F7;
                      CaptionML=[ENU=Statistics;
                                 PTG=Estat�sticas];
                      RunObject=Page 5757;
                      RunPageLink=No.=FIELD(No.);
                      Promoted=Yes;
                      Image=Statistics;
                      PromotedCategory=Process }
      { 57      ;2   ;Action    ;
                      CaptionML=[ENU=Co&mments;
                                 PTG=Co&ment�rios];
                      RunObject=Page 5750;
                      RunPageLink=Document Type=CONST(Posted Transfer Receipt),
                                  No.=FIELD(No.);
                      Image=ViewComments }
      { 58      ;2   ;Action    ;
                      AccessByPermission=TableData 348=R;
                      ShortCutKey=Shift+Ctrl+D;
                      CaptionML=[ENU=Dimensions;
                                 PTG=Dimens�es];
                      ToolTipML=[ENU=View or edit dimensions, such as area, project, or department, that you can assign to sales and purchase documents to distribute costs and analyze transaction history.;
                                 PTG=""];
                      Image=Dimensions;
                      OnAction=BEGIN
                                 ShowDimensions;
                               END;
                                }
      { 1900000004;0 ;ActionContainer;
                      ActionContainerType=ActionItems }
      { 51      ;1   ;Action    ;
                      Ellipsis=Yes;
                      CaptionML=[ENU=&Print;
                                 PTG=&Imprimir];
                      Promoted=Yes;
                      Image=Print;
                      PromotedCategory=Process;
                      OnAction=VAR
                                 TransRcptHeader@1001 : Record 5746;
                               BEGIN
                                 CurrPage.SETSELECTIONFILTER(TransRcptHeader);
                                 TransRcptHeader.PrintRecords(TRUE);
                               END;
                                }
      { 52      ;1   ;Action    ;
                      CaptionML=[ENU=&Navigate;
                                 PTG=&Navegar];
                      Promoted=Yes;
                      Image=Navigate;
                      PromotedCategory=Process;
                      OnAction=BEGIN
                                 Navigate;
                               END;
                                }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                CaptionML=[ENU=General;
                           PTG=Geral] }

    { 2   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the number of the transfer receipt.;
                           PTG=""];
                SourceExpr="No.";
                Importance=Promoted;
                Editable=FALSE }

    { 4   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the code of the location that you are transferring items from.;
                           PTG=""];
                SourceExpr="Transfer-from Code";
                Importance=Promoted;
                Editable=FALSE }

    { 6   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the code of the location that you are transferring items to.;
                           PTG=""];
                SourceExpr="Transfer-to Code";
                Importance=Promoted;
                Editable=FALSE }

    { 27  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the in-transit code that identifies this transfer.;
                           PTG=""];
                SourceExpr="In-Transit Code";
                Editable=FALSE }

    { 53  ;2   ;Field     ;
                Lookup=No;
                ToolTipML=[ENU=Specifies the number of the transfer order on which the transfer receipt was based.;
                           PTG=""];
                SourceExpr="Transfer Order No.";
                Importance=Promoted;
                Editable=FALSE }

    { 8   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the date you created the transfer order.;
                           PTG=""];
                SourceExpr="Transfer Order Date";
                Editable=FALSE }

    { 10  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the posting date for this document.;
                           PTG=""];
                SourceExpr="Posting Date";
                Editable=FALSE }

    { 12  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the dimension value code for the dimension that has been chosen as Global Dimension 1.;
                           PTG=""];
                SourceExpr="Shortcut Dimension 1 Code";
                Editable=FALSE }

    { 14  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the dimension value code for the dimension that has been chosen as Global Dimension 2.;
                           PTG=""];
                SourceExpr="Shortcut Dimension 2 Code";
                Editable=FALSE }

    { 7   ;2   ;Field     ;
                SourceExpr="Location Type";
                Editable=FALSE }

    { 9   ;2   ;Field     ;
                SourceExpr="External Entity No.";
                Editable=FALSE }

    { 3   ;2   ;Field     ;
                SourceExpr="Ship-to Code";
                Editable=FALSE }

    { 49  ;1   ;Part      ;
                Name=TransferReceiptLines;
                SubPageLink=Document No.=FIELD(No.);
                PagePartID=Page5746 }

    { 1904655901;1;Group  ;
                CaptionML=[ENU=Transfer-from;
                           PTG=Transferir-de] }

    { 16  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the name of the location that you are transferring items from.;
                           PTG=""];
                SourceExpr="Transfer-from Name";
                Editable=FALSE }

    { 18  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies an additional part of the name of the location that you are transferring items from.;
                           PTG=""];
                SourceExpr="Transfer-from Name 2";
                Editable=FALSE }

    { 20  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the address of the location that you are transferring items from.;
                           PTG=""];
                SourceExpr="Transfer-from Address";
                Editable=FALSE }

    { 22  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies an additional part of the address of the location.;
                           PTG=""];
                SourceExpr="Transfer-from Address 2";
                Editable=FALSE }

    { 24  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the postal code of the location that you are transferring items from.;
                           PTG=""];
                SourceExpr="Transfer-from Post Code";
                Editable=FALSE }

    { 26  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the city of the location that you are transferring items from.;
                           PTG=""];
                SourceExpr="Transfer-from City";
                Editable=FALSE }

    { 1110000;2;Field     ;
                SourceExpr="Transfer-from County";
                Editable=FALSE }

    { 44  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the name of the contact person at the transfer-from location.;
                           PTG=""];
                SourceExpr="Transfer-from Contact";
                Editable=FALSE }

    { 29  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the shipment date of the transfer order.;
                           PTG=""];
                SourceExpr="Shipment Date";
                Importance=Promoted;
                Editable=FALSE }

    { 67  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies a code that represents the shipment method for the receipt.;
                           PTG=""];
                SourceExpr="Shipment Method Code";
                Editable=FALSE }

    { 62  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the code for the shipping agent that delivered this transfer shipment to the transfer-to location.;
                           PTG=""];
                SourceExpr="Shipping Agent Code";
                Editable=FALSE }

    { 64  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the code for the shipping agent service that applies to the transfer shipment received at the transfer-to location.;
                           PTG=""];
                SourceExpr="Shipping Agent Service Code";
                Importance=Promoted;
                Editable=FALSE }

    { 1901454601;1;Group  ;
                CaptionML=[ENU=Transfer-to;
                           PTG=Transferir-para] }

    { 32  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the name of the location that you are transferring items to.;
                           PTG=""];
                SourceExpr="Transfer-to Name";
                Editable=FALSE }

    { 34  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies an additional part of the name of the location that you are transferring items to.;
                           PTG=""];
                SourceExpr="Transfer-to Name 2";
                Editable=FALSE }

    { 36  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the address of the location that you are transferring items to.;
                           PTG=""];
                SourceExpr="Transfer-to Address";
                Editable=FALSE }

    { 38  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies an additional part of the address of the location that you are transferring items to.;
                           PTG=""];
                SourceExpr="Transfer-to Address 2";
                Editable=FALSE }

    { 40  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the postal code of the location that you are transferring items to.;
                           PTG=""];
                SourceExpr="Transfer-to Post Code";
                Editable=FALSE }

    { 42  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the city of the location that you are transferring items to.;
                           PTG=""];
                SourceExpr="Transfer-to City";
                Editable=FALSE }

    { 1110002;2;Field     ;
                SourceExpr="Transfer-to County";
                Editable=FALSE }

    { 46  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the name of the contact person at the transfer-to location.;
                           PTG=""];
                SourceExpr="Transfer-to Contact";
                Editable=FALSE }

    { 31  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the receipt date of the transfer order.;
                           PTG=""];
                SourceExpr="Receipt Date";
                Importance=Promoted;
                Editable=FALSE }

    { 5   ;2   ;Field     ;
                SourceExpr="Transfer-to VAT Reg. No.";
                Importance=Promoted;
                Editable=FALSE }

    { 1907468901;1;Group  ;
                CaptionML=[ENU=Foreign Trade;
                           PTG=Com�rcio Externo] }

    { 78  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the transaction type of the transfer.;
                           PTG=""];
                SourceExpr="Transaction Type";
                Importance=Promoted;
                Editable=FALSE }

    { 76  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the transaction specification code that was used in the transfer.;
                           PTG=""];
                SourceExpr="Transaction Specification";
                Editable=FALSE }

    { 74  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the code for the transport method used for the item on this line.;
                           PTG=""];
                SourceExpr="Transport Method";
                Importance=Promoted;
                Editable=FALSE }

    { 72  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the code for an area at the customer or vendor with which you are trading the items on the line.;
                           PTG=""];
                SourceExpr=Area;
                Editable=FALSE }

    { 70  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the code of either the port of entry at which the items passed into your country/region, or the port of exit.;
                           PTG=""];
                SourceExpr="Entry/Exit Point";
                Editable=FALSE }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                Visible=TRUE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {

    BEGIN
    {
      V92.00#00033 - NF - Ordens Transfer�ncia a Terceiros - EFD - 2016.03.14
    }
    END.
  }
}

