OBJECT Page 371 Bank Account List
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0,NAVPTSS10.00;
  }
  PROPERTIES
  {
    Editable=No;
    CaptionML=[ENU=Bank Account List;
               PTG=Lista Contas Banc�rias];
    SourceTable=Table270;
    PageType=List;
    CardPageID=Bank Account Card;
    PromotedActionCategoriesML=[ENU=New,Process,Report,Bank Statement Service;
                                PTG=Novo,Processar,Mapas,Servi�o Extrato Banc�rio];
    OnOpenPage=BEGIN
                 ShowBankLinkingActions := StatementProvidersExist;
               END;

    OnAfterGetRecord=BEGIN
                       CALCFIELDS("Check Report Name");
                       GetOnlineFeedStatementStatus(OnlineFeedStatementStatus,Linked);
                     END;

    OnAfterGetCurrRecord=BEGIN
                           GetOnlineFeedStatementStatus(OnlineFeedStatementStatus,Linked);
                           ShowBankLinkingActions := StatementProvidersExist;
                         END;

    ActionList=ACTIONS
    {
      { 1900000003;0 ;ActionContainer;
                      ActionContainerType=RelatedInformation }
      { 15      ;1   ;ActionGroup;
                      CaptionML=[ENU=&Bank Acc.;
                                 PTG=Conta &Banc�ria];
                      Image=Bank }
      { 17      ;2   ;Action    ;
                      ShortCutKey=F7;
                      CaptionML=[ENU=Statistics;
                                 PTG=Estat�sticas];
                      ToolTipML=ENU=View statistical information, such as the value of posted entries, for the record.;
                      ApplicationArea=#Basic,#Suite;
                      RunObject=Page 375;
                      RunPageLink=No.=FIELD(No.),
                                  Date Filter=FIELD(Date Filter),
                                  Global Dimension 1 Filter=FIELD(Global Dimension 1 Filter),
                                  Global Dimension 2 Filter=FIELD(Global Dimension 2 Filter);
                      Promoted=Yes;
                      Image=Statistics;
                      PromotedCategory=Process;
                      PromotedOnly=Yes }
      { 18      ;2   ;Action    ;
                      CaptionML=[ENU=Co&mments;
                                 PTG=Co&ment�rios];
                      ToolTipML=ENU=Create a comment attached to the selected bank account.;
                      RunObject=Page 124;
                      RunPageLink=Table Name=CONST(Bank Account),
                                  No.=FIELD(No.);
                      Image=ViewComments }
      { 9       ;2   ;Action    ;
                      Name=PositivePayExport;
                      CaptionML=[ENU=Positive Pay Export;
                                 PTG=Exporta��o Positive Pay];
                      ToolTipML=ENU=Export a Positive Pay file with relevant payment information that you then send to the bank for reference when you process payments to make sure that your bank only clears validated checks and amounts.;
                      ApplicationArea=#Suite;
                      RunObject=Page 1233;
                      RunPageLink=No.=FIELD(No.);
                      Promoted=Yes;
                      Visible=FALSE;
                      Image=Export;
                      PromotedCategory=Process }
      { 22      ;2   ;ActionGroup;
                      CaptionML=[ENU=Dimensions;
                                 PTG=Dimens�es];
                      Image=Dimensions }
      { 84      ;3   ;Action    ;
                      ShortCutKey=Shift+Ctrl+D;
                      CaptionML=[ENU=Dimensions-Single;
                                 PTG=Dimens�es-Singular];
                      ToolTipML=ENU=View or edit the single set of dimensions that are set up for the selected record.;
                      ApplicationArea=#Suite;
                      RunObject=Page 540;
                      RunPageLink=Table ID=CONST(270),
                                  No.=FIELD(No.);
                      Image=Dimensions }
      { 21      ;3   ;Action    ;
                      AccessByPermission=TableData 348=R;
                      CaptionML=[ENU=Dimensions-&Multiple;
                                 PTG=Dimens�es-&M�ltipla];
                      ToolTipML=ENU=View or edit dimensions for a group of records. You can assign dimension codes to transactions to distribute costs and analyze historical information.;
                      ApplicationArea=#Suite;
                      Image=DimensionSets;
                      OnAction=VAR
                                 BankAcc@1001 : Record 270;
                                 DefaultDimMultiple@1002 : Page 542;
                               BEGIN
                                 CurrPage.SETSELECTIONFILTER(BankAcc);
                                 DefaultDimMultiple.SetMultiBankAcc(BankAcc);
                                 DefaultDimMultiple.RUNMODAL;
                               END;
                                }
      { 6       ;2   ;Action    ;
                      CaptionML=[ENU=Balance;
                                 PTG=Saldo];
                      ToolTipML=ENU=View a summary of the bank account balance at different periods.;
                      ApplicationArea=#Basic,#Suite;
                      RunObject=Page 377;
                      RunPageLink=No.=FIELD(No.),
                                  Date Filter=FIELD(Date Filter),
                                  Global Dimension 1 Filter=FIELD(Global Dimension 1 Filter),
                                  Global Dimension 2 Filter=FIELD(Global Dimension 2 Filter);
                      Promoted=Yes;
                      Image=Balance;
                      PromotedCategory=Process;
                      PromotedOnly=Yes }
      { 7       ;2   ;Action    ;
                      Name=Statements;
                      CaptionML=[ENU=St&atements;
                                 PTG=E&xtratos];
                      ToolTipML=ENU=View posted bank statements and reconciliations.;
                      RunObject=Page 389;
                      RunPageLink=Bank Account No.=FIELD(No.);
                      Image=List }
      { 19      ;2   ;Action    ;
                      ShortCutKey=Ctrl+F7;
                      CaptionML=[ENU=Ledger E&ntries;
                                 PTG=Movime&ntos];
                      ToolTipML=ENU=View the history of transactions that have been posted for the selected record.;
                      RunObject=Page 372;
                      RunPageView=SORTING(Bank Account No.)
                                  ORDER(Descending);
                      RunPageLink=Bank Account No.=FIELD(No.);
                      Promoted=No;
                      Image=BankAccountLedger;
                      PromotedCategory=Process }
      { 20      ;2   ;Action    ;
                      CaptionML=[ENU=Chec&k Ledger Entries;
                                 PTG=Movs. C&heques];
                      ToolTipML=ENU=View check ledger entries that result from posting transactions in a payment journal for the relevant bank account.;
                      ApplicationArea=#Basic,#Suite;
                      RunObject=Page 374;
                      RunPageView=SORTING(Bank Account No.)
                                  ORDER(Descending);
                      RunPageLink=Bank Account No.=FIELD(No.);
                      Image=CheckLedger }
      { 25      ;2   ;Action    ;
                      CaptionML=[ENU=C&ontact;
                                 PTG=C&ontacto];
                      ToolTipML=ENU=View or edit detailed information about the contact person at the customer.;
                      ApplicationArea=#Basic,#Suite;
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=ContactPerson;
                      PromotedCategory=Process;
                      PromotedOnly=Yes;
                      OnAction=BEGIN
                                 ShowContact;
                               END;
                                }
      { 16      ;2   ;Action    ;
                      Name=CreateNewLinkedBankAccount;
                      CaptionML=[ENU=Create New Linked Bank Account;
                                 PTG=Criar Nova Conta Banc�ria Associada];
                      ToolTipML=ENU=Create a new online bank account to link to the selected bank account.;
                      ApplicationArea=#Basic,#Suite;
                      Promoted=Yes;
                      Visible=ShowBankLinkingActions;
                      PromotedIsBig=Yes;
                      Image=NewBank;
                      PromotedCategory=Category4;
                      OnAction=VAR
                                 BankAccount@1001 : Record 270;
                               BEGIN
                                 BankAccount.INIT;
                                 BankAccount.LinkStatementProvider(BankAccount);
                               END;
                                }
      { 11      ;2   ;Action    ;
                      Name=LinkToOnlineBankAccount;
                      CaptionML=[ENU=Link to Online Bank Account;
                                 PTG=Associar a Conta Banc�ria Online];
                      ToolTipML=ENU=Create a link to an online bank account from the selected bank account.;
                      ApplicationArea=#Basic,#Suite;
                      Promoted=Yes;
                      Visible=ShowBankLinkingActions;
                      Enabled=NOT Linked;
                      PromotedIsBig=Yes;
                      Image=LinkAccount;
                      PromotedCategory=Category4;
                      OnAction=BEGIN
                                 VerifySingleSelection;
                                 LinkStatementProvider(Rec);
                               END;
                                }
      { 13      ;2   ;Action    ;
                      Name=UnlinkOnlineBankAccount;
                      CaptionML=[ENU=Unlink Online Bank Account;
                                 PTG=Desassociar Conta Banc�ria Online];
                      ToolTipML=ENU=Remove a link to an online bank account from the selected bank account.;
                      ApplicationArea=#Basic,#Suite;
                      Promoted=Yes;
                      Visible=ShowBankLinkingActions;
                      Enabled=Linked;
                      PromotedIsBig=Yes;
                      Image=UnLinkAccount;
                      PromotedCategory=Category4;
                      OnAction=BEGIN
                                 VerifySingleSelection;
                                 UnlinkStatementProvider;
                                 CurrPage.UPDATE(TRUE);
                               END;
                                }
      { 14      ;2   ;Action    ;
                      Name=UpdateBankAccountLinking;
                      CaptionML=[ENU=Update Bank Account Linking;
                                 PTG=Atualizar Associa��o Conta Banc�ria];
                      ToolTipML=ENU=Link any non-linked bank accounts to their related bank accounts.;
                      ApplicationArea=#Basic,#Suite;
                      Promoted=Yes;
                      Visible=ShowBankLinkingActions;
                      PromotedIsBig=Yes;
                      Image=MapAccounts;
                      PromotedCategory=Category4;
                      OnAction=BEGIN
                                 UpdateBankAccountLinking;
                               END;
                                }
      { 24      ;2   ;Action    ;
                      Name=AutomaticBankStatementImportSetup;
                      CaptionML=[ENU=Automatic Bank Statement Import Setup;
                                 PTG=Config. Importa��o Autom�tica Extrato Banc�rio];
                      ToolTipML=ENU=Set up the information for importing bank statement files.;
                      ApplicationArea=#Basic,#Suite;
                      RunObject=Page 1269;
                      RunPageOnRec=Yes;
                      Promoted=Yes;
                      Visible=ShowBankLinkingActions;
                      Enabled=Linked;
                      PromotedIsBig=Yes;
                      Image=ElectronicBanking;
                      PromotedCategory=Category4 }
      { 5       ;2   ;Action    ;
                      Name=PagePosPayEntries;
                      CaptionML=[ENU=Positive Pay Entries;
                                 PTG=Movs. Positive Pay];
                      ToolTipML=ENU=View the bank ledger entries that are related to Positive Pay transactions.;
                      ApplicationArea=#Suite;
                      RunObject=Page 1231;
                      RunPageView=SORTING(Bank Account No.,Upload Date-Time)
                                  ORDER(Descending);
                      RunPageLink=Bank Account No.=FIELD(No.);
                      Visible=FALSE;
                      Image=CheckLedger }
      { 1900000006;0 ;ActionContainer;
                      ActionContainerType=Reports }
      { 1900670506;1 ;Action    ;
                      CaptionML=[ENU=Detail Trial Balance;
                                 PTG=Balancete Detalhado];
                      ToolTipML=ENU=View a detailed trial balance for selected checks.;
                      ApplicationArea=#Basic,#Suite;
                      RunObject=Report 1404;
                      Promoted=Yes;
                      Image=Report;
                      PromotedCategory=Report;
                      PromotedOnly=Yes }
      { 1904208406;1 ;Action    ;
                      CaptionML=[ENU=Check Details;
                                 PTG=Detalhes Cheque];
                      ToolTipML=ENU=View a detailed trial balance for selected checks.;
                      ApplicationArea=#Basic,#Suite;
                      RunObject=Report 1406;
                      Image=Report;
                      PromotedCategory=Report }
      { 1902174606;1 ;Action    ;
                      CaptionML=[ENU=Trial Balance by Period;
                                 PTG=Balancete por Per�odo];
                      ToolTipML=ENU=View a detailed trial balance for selected checks within a selected period.;
                      ApplicationArea=#Basic,#Suite;
                      RunObject=Report 38;
                      Promoted=Yes;
                      Image=Report;
                      PromotedCategory=Report;
                      PromotedOnly=Yes }
      { 1904082706;1 ;Action    ;
                      CaptionML=[ENU=Trial Balance;
                                 PTG=Balancete];
                      ToolTipML=ENU=View a detailed trial balance for the selected bank account.;
                      ApplicationArea=#Suite;
                      RunObject=Report 6;
                      Image=Report;
                      PromotedCategory=Report }
      { 3       ;1   ;Action    ;
                      CaptionML=[ENU=Bank Account Statements;
                                 PTG=Extratos Conta Banc�ria];
                      ToolTipML=[ENU=View statements for selected bank accounts. For each bank transaction, the report shows a description, an applied amount, a statement amount, and other information.;
                                 PTG=Ver, imprimir, ou gravar extratos por contas banc�rias selecionadas. Para cada extratoo banc�ria, o map mostra uma descri��o, um valor liquidado, um valor declarado, e outra informa��o.];
                      ApplicationArea=#Suite;
                      RunObject=Report 1407;
                      Image=Report }
      { 31022891;1   ;Action    ;
                      CaptionML=[ENU=Bank - Summ. Bill Group;
                                 PTG=Banco - Resumo Remessas];
                      ApplicationArea=#Basic,#Suite;
                      RunObject=Report 31022965;
                      Promoted=Yes;
                      Image=Report;
                      PromotedCategory=Report }
      { 31022890;1   ;Action    ;
                      CaptionML=[ENU=Bank - Risk;
                                 PTG=Banco - Risco];
                      ApplicationArea=#Basic,#Suite;
                      RunObject=Report 31022966;
                      Promoted=Yes;
                      Image=Report;
                      PromotedCategory=Report }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 2   ;2   ;Field     ;
                ToolTipML=ENU=Specifies the number of the bank account.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="No." }

    { 4   ;2   ;Field     ;
                ToolTipML=ENU=Specifies the name of the bank where you have the bank account.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr=Name }

    { 12  ;2   ;Field     ;
                CaptionML=[ENU=Bank Account Linking Status;
                           PTG=Estado Associa��o Conta Banc�ria];
                ToolTipML=ENU=Specifies if the bank account is linked to an online bank account through the bank statement service.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr=OnlineFeedStatementStatus;
                Visible=ShowBankLinkingActions;
                Editable=FALSE }

    { 87  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the postal code of the address.;
                SourceExpr="Post Code";
                Visible=FALSE }

    { 89  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the country/region code of the address.;
                SourceExpr="Country/Region Code";
                Visible=FALSE }

    { 91  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the telephone number of the bank where you have the bank account.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Phone No." }

    { 93  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the fax number associated with the address.;
                SourceExpr="Fax No.";
                Visible=FALSE }

    { 23  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the name of the bank employee regularly contacted in connection with this bank account.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr=Contact }

    { 105 ;2   ;Field     ;
                ToolTipML=ENU=Specifies the number used by the bank for the bank account.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Bank Account No.";
                Visible=FALSE }

    { 26  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the international bank identifier code (SWIFT) of the bank where you have the account.;
                SourceExpr="SWIFT Code";
                Visible=FALSE }

    { 28  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the bank account's international bank account number.;
                SourceExpr=IBAN;
                Visible=FALSE }

    { 95  ;2   ;Field     ;
                ToolTipML=ENU=Specifies a code to specify the employee who is responsible for this bank account.;
                SourceExpr="Our Contact Code";
                Visible=FALSE }

    { 97  ;2   ;Field     ;
                ToolTipML=ENU=Specifies a code for the bank account posting group for the bank account.;
                SourceExpr="Bank Acc. Posting Group";
                Visible=FALSE }

    { 8   ;2   ;Field     ;
                ToolTipML=ENU=Specifies the relevant currency code for the bank account.;
                ApplicationArea=#Suite;
                SourceExpr="Currency Code";
                Visible=FALSE }

    { 103 ;2   ;Field     ;
                ToolTipML=ENU=Specifies a code that determines the language associated with this bank account.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Language Code";
                Visible=FALSE }

    { 10  ;2   ;Field     ;
                ToolTipML=ENU=Specifies a search name for the bank account.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Search Name" }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1905532107;1;Part   ;
                SubPageLink=Table ID=CONST(270),
                            No.=FIELD(No.);
                PagePartID=Page9083;
                Visible=FALSE;
                PartType=Page }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {
    VAR
      MultiselectNotSupportedErr@1001 : TextConst 'ENU=You can only link to one online bank account at a time.;PTG=S� pode associar a uma conta banc�ria online de cada vez.';
      Linked@1000 : Boolean;
      ShowBankLinkingActions@1002 : Boolean;
      OnlineFeedStatementStatus@1003 : 'Not Linked,Linked,Linked and Auto. Bank Statement Enabled';

    LOCAL PROCEDURE VerifySingleSelection@1();
    VAR
      BankAccount@1000 : Record 270;
    BEGIN
      CurrPage.SETSELECTIONFILTER(BankAccount);

      IF BankAccount.COUNT > 1 THEN
        ERROR(MultiselectNotSupportedErr);
    END;

    BEGIN
    END.
  }
}

