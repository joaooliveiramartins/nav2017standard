OBJECT Page 9301 Sales Invoice List
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0.00.15052,NAVPTSS84.00;
  }
  PROPERTIES
  {
    Editable=No;
    CaptionML=[ENU=Sales Invoices;
               PTG=Faturas Venda];
    SourceTable=Table36;
    SourceTableView=WHERE(Document Type=CONST(Invoice),
                          Debit Memo=FILTER(No));
    DataCaptionFields=Sell-to Customer No.;
    PageType=List;
    CardPageID=Sales Invoice;
    RefreshOnActivate=Yes;
    PromotedActionCategoriesML=[ENU=New,Process,Report,Release,Posting,Invoice,Request Approval;
                                PTG=Novo,Processar,Mapas,Libertar,Registo,Fatura,Pedir Aprova��o];
    OnOpenPage=VAR
                 SalesSetup@1000 : Record 311;
               BEGIN
                 SetSecurityFilterOnRespCenter;
                 JobQueueActive := SalesSetup.JobQueueActive;

                 CopySellToCustomerFilter;
               END;

    OnAfterGetCurrRecord=BEGIN
                           SetControlAppearance;
                           CurrPage.IncomingDocAttachFactBox.PAGE.LoadDataFromRecord(Rec);
                         END;

    ActionList=ACTIONS
    {
      { 1900000003;0 ;ActionContainer;
                      ActionContainerType=RelatedInformation }
      { 1102601019;1 ;ActionGroup;
                      CaptionML=[ENU=&Invoice;
                                 PTG=F&actura];
                      Image=Invoice }
      { 1102601021;2 ;Action    ;
                      Name=Statistics;
                      ShortCutKey=F7;
                      CaptionML=[ENU=Statistics;
                                 PTG=Estat�sticas];
                      Promoted=Yes;
                      Image=Statistics;
                      PromotedCategory=Category6;
                      OnAction=BEGIN
                                 CalcInvDiscForHeader;
                                 COMMIT;
                                 PAGE.RUNMODAL(PAGE::"Sales Statistics",Rec);
                               END;
                                }
      { 1102601023;2 ;Action    ;
                      CaptionML=[ENU=Co&mments;
                                 PTG=Co&ment�rios];
                      RunObject=Page 67;
                      RunPageLink=Document Type=FIELD(Document Type),
                                  No.=FIELD(No.),
                                  Document Line No.=CONST(0);
                      Promoted=Yes;
                      Image=ViewComments;
                      PromotedCategory=Category6 }
      { 1102601024;2 ;Action    ;
                      AccessByPermission=TableData 348=R;
                      ShortCutKey=Shift+Ctrl+D;
                      CaptionML=[ENU=Dimensions;
                                 PTG=Dimens�es];
                      ToolTipML=[ENU=View or edit dimensions, such as area, project, or department, that you can assign to sales and purchase documents to distribute costs and analyze transaction history.;
                                 PTG=""];
                      Promoted=Yes;
                      Image=Dimensions;
                      PromotedCategory=Category6;
                      OnAction=BEGIN
                                 ShowDocDim;
                               END;
                                }
      { 1102601025;2 ;Action    ;
                      Name=Approvals;
                      AccessByPermission=TableData 454=R;
                      CaptionML=[ENU=Approvals;
                                 PTG=Aprova��es];
                      ToolTipML=[ENU=View a list of the records that are waiting to be approved. For example, you can see who requested the record to be approved, when it was sent, and when it is due to be approved.;
                                 PTG=Ver uma lista dos registos que est�o � espera de aprova��o. Por exemplo, pode ver quem pediu a aprova��o do registo, quando foi enviado e quando a aprova��o deve ser conclu�da.];
                      ApplicationArea=#Suite;
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=Approvals;
                      PromotedCategory=Category6;
                      PromotedOnly=Yes;
                      OnAction=VAR
                                 ApprovalEntries@1001 : Page 658;
                               BEGIN
                                 ApprovalEntries.Setfilters(DATABASE::"Sales Header","Document Type","No.");
                                 ApprovalEntries.RUN;
                               END;
                                }
      { 14      ;2   ;Action    ;
                      ShortCutKey=Shift+F7;
                      CaptionML=[ENU=Customer;
                                 PTG=Cliente];
                      ToolTipML=[ENU=View or edit detailed information about the customer on the selected sales document.;
                                 PTG=""];
                      ApplicationArea=#Basic,#Suite;
                      RunObject=Page 21;
                      RunPageLink=No.=FIELD(Sell-to Customer No.);
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=Customer;
                      PromotedCategory=Category6;
                      PromotedOnly=Yes;
                      Scope=Repeater }
      { 1900000004;0 ;ActionContainer;
                      ActionContainerType=ActionItems }
      { 8       ;1   ;ActionGroup;
                      Name=Invoice;
                      CaptionML=[ENU=Post and Email;
                                 PTG=Registar e Enviar E-Mail];
                      Image=Invoice }
      { 7       ;1   ;ActionGroup;
                      CaptionML=[ENU=Release;
                                 PTG=Libertar];
                      Image=ReleaseDoc }
      { 1102601017;2 ;Action    ;
                      ShortCutKey=Ctrl+F9;
                      CaptionML=[ENU=Re&lease;
                                 PTG=&Libertar];
                      Promoted=Yes;
                      Image=ReleaseDoc;
                      PromotedCategory=Category4;
                      OnAction=VAR
                                 ReleaseSalesDoc@1000 : Codeunit 414;
                               BEGIN
                                 ReleaseSalesDoc.PerformManualRelease(Rec);
                               END;
                                }
      { 1102601018;2 ;Action    ;
                      CaptionML=[ENU=Re&open;
                                 PTG=Re&abrir];
                      Promoted=Yes;
                      Image=ReOpen;
                      PromotedCategory=Category4;
                      OnAction=VAR
                                 ReleaseSalesDoc@1001 : Codeunit 414;
                               BEGIN
                                 ReleaseSalesDoc.PerformManualReopen(Rec);
                               END;
                                }
      { 1102601000;1 ;ActionGroup;
                      CaptionML=[ENU=Request Approval;
                                 PTG=Pedir Aprova��o];
                      Image=Action }
      { 1102601014;2 ;Action    ;
                      Name=SendApprovalRequest;
                      CaptionML=[ENU=Send A&pproval Request;
                                 PTG=Enviar Pedido A&prova��o];
                      ToolTipML=[ENU=Send an approval request.;
                                 PTG=""];
                      Promoted=Yes;
                      Enabled=NOT OpenApprovalEntriesExist;
                      PromotedIsBig=Yes;
                      Image=SendApprovalRequest;
                      PromotedCategory=Category7;
                      PromotedOnly=Yes;
                      OnAction=VAR
                                 ApprovalsMgmt@1001 : Codeunit 1535;
                               BEGIN
                                 IF ApprovalsMgmt.CheckSalesApprovalPossible(Rec) THEN
                                   ApprovalsMgmt.OnSendSalesDocForApproval(Rec);
                               END;
                                }
      { 1102601015;2 ;Action    ;
                      Name=CancelApprovalRequest;
                      CaptionML=[ENU=Cancel Approval Re&quest;
                                 PTG=Cancelar Pedido Aprova&��o];
                      ToolTipML=[ENU=Cancel the approval request.;
                                 PTG=""];
                      Promoted=Yes;
                      Enabled=CanCancelApprovalForRecord;
                      PromotedIsBig=Yes;
                      Image=CancelApprovalRequest;
                      PromotedCategory=Category7;
                      PromotedOnly=Yes;
                      OnAction=VAR
                                 ApprovalsMgmt@1001 : Codeunit 1535;
                               BEGIN
                                 ApprovalsMgmt.OnCancelSalesApprovalRequest(Rec);
                               END;
                                }
      { 49      ;1   ;ActionGroup;
                      CaptionML=[ENU=P&osting;
                                 PTG=&Registo];
                      Image=Post }
      { 53      ;2   ;Action    ;
                      Ellipsis=Yes;
                      CaptionML=[ENU=Test Report;
                                 PTG=Verificar];
                      ToolTipML=[ENU=View a test report so that you can find and correct any errors before you perform the actual posting of the journal or document.;
                                 PTG=""];
                      Image=TestReport;
                      OnAction=BEGIN
                                 ReportPrint.PrintSalesHeader(Rec);
                               END;
                                }
      { 51      ;2   ;Action    ;
                      Name=Post;
                      ShortCutKey=F9;
                      CaptionML=[ENU=P&ost;
                                 PTG=R&egistar];
                      ToolTipML=[ENU=Finalize the document or journal by posting the amounts and quantities to the related accounts in your company books.;
                                 PTG=""];
                      ApplicationArea=#Basic,#Suite;
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=PostOrder;
                      PromotedCategory=Category5;
                      PromotedOnly=Yes;
                      OnAction=BEGIN
                                 Post(CODEUNIT::"Sales-Post (Yes/No)");
                               END;
                                }
      { 50      ;2   ;Action    ;
                      Ellipsis=Yes;
                      CaptionML=[ENU=Post &Batch;
                                 PTG=Registar por &Lotes];
                      Promoted=Yes;
                      Image=PostBatch;
                      PromotedCategory=Category5;
                      OnAction=BEGIN
                                 REPORT.RUNMODAL(REPORT::"Batch Post Sales Invoices",TRUE,TRUE,Rec);
                                 CurrPage.UPDATE(FALSE);
                               END;
                                }
      { 59      ;2   ;Action    ;
                      Name=PostAndSend;
                      Ellipsis=Yes;
                      CaptionML=[ENU=&Invoice;
                                 PTG=Faturar];
                      ToolTipML=[ENU=Finalize and prepare to send the document according to the customer's sending profile, such as attached to an email. The Send document to window opens first so you can confirm or select a sending profile.;
                                 PTG=""];
                      ApplicationArea=#Basic,#Suite;
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=PostSendTo;
                      PromotedCategory=Category5;
                      PromotedOnly=Yes;
                      OnAction=BEGIN
                                 LinesInstructionMgt.SalesCheckAllLinesHaveQuantityAssigned(Rec);
                                 SendToPosting(CODEUNIT::"Sales-Post and Send");
                               END;
                                }
      { 5       ;2   ;Action    ;
                      CaptionML=[ENU=Remove From Job Queue;
                                 PTG=Remover da Fila de Tarefas];
                      ToolTipML=[ENU=Remove the scheduled processing of this record from the job queue.;
                                 PTG=Remover o processamento agendado deste registo da fila de tarefas.];
                      ApplicationArea=#All;
                      Visible=JobQueueActive;
                      Image=RemoveLine;
                      PromotedCategory=Category5;
                      OnAction=BEGIN
                                 CancelBackgroundPosting;
                               END;
                                }
      { 10      ;2   ;Action    ;
                      Name=Preview;
                      CaptionML=[ENU=Preview Posting;
                                 PTG=Pr�-Visualizar Registo];
                      ToolTipML=[ENU=Review the different types of entries that will be created when you post the document or journal.;
                                 PTG=""];
                      Image=ViewPostedOrder;
                      OnAction=BEGIN
                                 ShowPreview;
                               END;
                                }
      { 43      ;0   ;ActionContainer;
                      ActionContainerType=Reports }
      { 38      ;1   ;ActionGroup;
                      Name=Reports;
                      CaptionML=[ENU=Reports;
                                 PTG=Mapas];
                      ActionContainerType=Reports;
                      Image=Report }
      { 31      ;2   ;ActionGroup;
                      Name=FinanceReports;
                      CaptionML=[ENU=Finance Reports;
                                 PTG=Mapas de Financeira];
                      Image=Report }
      { 30      ;3   ;Action    ;
                      Name=Report Statement;
                      CaptionML=[ENU=Statement;
                                 PTG=Balancete];
                      ToolTipML=[ENU=View a list of a customer's transactions for a selected period, for example, to send to the customer at the close of an accounting period. You can choose to have all overdue balances displayed regardless of the period specified, or you can choose to include an aging band.;
                                 PTG=""];
                      ApplicationArea=#Basic,#Suite;
                      Image=Report;
                      OnAction=VAR
                                 Customer@1000 : Record 18;
                               BEGIN
                                 CODEUNIT.RUN(CODEUNIT::"Customer Layout - Statement",Customer);
                               END;
                                }
      { 28      ;3   ;Action    ;
                      CaptionML=[ENU=Customer - Balance to Date;
                                 PTG=Cliente - Saldo � Data];
                      ToolTipML=[ENU=View, print, or save customers' balances on a certain date. You can use the report to extract your total sales income at the close of an accounting period or fiscal year.;
                                 PTG=""];
                      RunObject=Report 121;
                      Image=Report }
      { 26      ;3   ;Action    ;
                      CaptionML=[ENU=Customer - Trial Balance;
                                 PTG=Cliente - Balancete];
                      ToolTipML=[ENU=View the beginning and ending balance for customers with entries within a specified period. The report can be used to verify that the balance for a customer posting group is equal to the balance on the corresponding general ledger account on a certain date.;
                                 PTG=""];
                      ApplicationArea=#Suite;
                      RunObject=Report 129;
                      Image=Report }
      { 25      ;3   ;Action    ;
                      CaptionML=[ENU=Customer - Detail Trial Bal.;
                                 PTG=Cliente - Balancete Detalhado];
                      ToolTipML=[ENU=View the balance for customers with balances on a specified date. For example, the report can be used at the close of an accounting period or for an audit.;
                                 PTG=""];
                      ApplicationArea=#Basic,#Suite;
                      RunObject=Report 104;
                      Image=Report }
      { 24      ;3   ;Action    ;
                      CaptionML=[ENU=Customer - Summary Aging;
                                 PTG=Cliente - Vencimentos Per�odo];
                      ToolTipML=[ENU=View, print, or save a summary of each customer's total payments due, divided into three time periods. The report can be used to decide when to issue reminders, to evaluate a customer's creditworthiness, or to prepare liquidity analyses.;
                                 PTG=""];
                      RunObject=Report 105;
                      Image=Report }
      { 22      ;3   ;Action    ;
                      CaptionML=[ENU=Customer - Detailed Aging;
                                 PTG=Cliente - D�vida Pendente];
                      ToolTipML=[ENU=View, print, or save a detailed list of each customer's total payments due, divided into three time periods. The report can be used to decide when to issue reminders, to evaluate a customer's creditworthiness, or to prepare liquidity analyses.;
                                 PTG=""];
                      RunObject=Report 106;
                      Image=Report }
      { 20      ;3   ;Action    ;
                      CaptionML=[ENU=Aged Accounts Receivable;
                                 PTG=Antiguidade Contas a Receber];
                      ToolTipML=[ENU=View an overview of when customer payments are due or overdue, divided into four periods. You must specify the date you want aging calculated from and the length of the period that each column will contain data for.;
                                 PTG=""];
                      ApplicationArea=#Basic,#Suite;
                      RunObject=Report 120;
                      Image=Report }
      { 19      ;3   ;Action    ;
                      CaptionML=[ENU=Customer - Payment Receipt;
                                 PTG=Cliente - Rece��o Pagamento];
                      ToolTipML=[ENU=View a document showing which customer ledger entries that a payment has been applied to. This report can be used as a payment receipt that you send to the customer.;
                                 PTG=""];
                      ApplicationArea=#Suite;
                      RunObject=Report 211;
                      Image=Report }
      { 37      ;2   ;ActionGroup;
                      Name=SalesReports;
                      CaptionML=[ENU=Sales Reports;
                                 PTG=Mapas de Vendas];
                      Image=Report }
      { 36      ;3   ;Action    ;
                      CaptionML=[ENU=Customer - Top 10 List;
                                 PTG=Cliente - Lista Top 10];
                      ToolTipML=[ENU=View which customers purchase the most or owe the most in a selected period. Only customers that have either purchases during the period or a balance at the end of the period will be included.;
                                 PTG=""];
                      ApplicationArea=#Basic,#Suite;
                      RunObject=Report 111;
                      Image=Report }
      { 34      ;3   ;Action    ;
                      CaptionML=[ENU=Customer - Sales List;
                                 PTG=Cliente - Lista de Vendas];
                      ToolTipML=[ENU=View customer sales in a period, for example, to report sales activity to customs and tax authorities.;
                                 PTG=""];
                      ApplicationArea=#Basic,#Suite;
                      RunObject=Report 119;
                      Image=Report }
      { 32      ;3   ;Action    ;
                      CaptionML=[ENU=Sales Statistics;
                                 PTG=Estat�stica de Vendas];
                      ToolTipML=[ENU=View the customer's total cost, sale, and profit over time, for example, to analyze earnings trends. The report shows amounts for original and adjusted cost, sales, profit, invoice discount, payment discount, and profit percentage in three adjustable periods.;
                                 PTG=""];
                      ApplicationArea=#Basic,#Suite;
                      RunObject=Report 112;
                      Image=Report }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 2   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the number of the sales document.;
                           PTG=""];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="No." }

    { 4   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the number of the customer who will receive the products and be billed by default.;
                           PTG=""];
                SourceExpr="Sell-to Customer No." }

    { 6   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the name of the customer who will receive the products and be billed by default.;
                           PTG=""];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Sell-to Customer Name" }

    { 17  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the number that the customer uses in their own system to refer to this sales document.;
                           PTG=""];
                SourceExpr="External Document No." }

    { 33  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the postal code of the address.;
                           PTG=""];
                SourceExpr="Sell-to Post Code";
                Visible=FALSE }

    { 29  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the country/region code of the address.;
                           PTG=""];
                SourceExpr="Sell-to Country/Region Code";
                Visible=FALSE }

    { 41  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the name of the person to contact at the customer that the items were sold to.;
                           PTG=""];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Sell-to Contact" }

    { 13  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the customer to whom you will send the sales invoice when this customer is different from the sell-to customer.;
                           PTG=""];
                SourceExpr="Bill-to Customer No.";
                Visible=FALSE }

    { 15  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the customer to whom you will send the sales invoice, when different from the customer that you are selling to.;
                           PTG=""];
                SourceExpr="Bill-to Name";
                Visible=FALSE }

    { 39  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the postal code of the address.;
                           PTG=""];
                SourceExpr="Bill-to Post Code";
                Visible=FALSE }

    { 35  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the country/region code of the address.;
                           PTG=""];
                SourceExpr="Bill-to Country/Region Code";
                Visible=FALSE }

    { 159 ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the name of the person you should contact at the customer who you are sending the invoice to.;
                           PTG=""];
                SourceExpr="Bill-to Contact";
                Visible=FALSE }

    { 155 ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the code for another shipment address than the customer's own address, which is entered by default.;
                           PTG=""];
                SourceExpr="Ship-to Code";
                Visible=FALSE }

    { 153 ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the name that products on the sales document will be shipped to.;
                           PTG=""];
                SourceExpr="Ship-to Name";
                Visible=FALSE }

    { 27  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the postal code of the address.;
                           PTG=""];
                SourceExpr="Ship-to Post Code";
                Visible=FALSE }

    { 23  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the country/region code of the address.;
                           PTG=""];
                SourceExpr="Ship-to Country/Region Code";
                Visible=FALSE }

    { 143 ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the name of the contact person at the address that products will be shipped to.;
                           PTG=""];
                SourceExpr="Ship-to Contact";
                Visible=FALSE }

    { 139 ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the date when the posting of the sales document will be recorded.;
                           PTG=""];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Posting Date" }

    { 121 ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the dimension value code associated with the sales header.;
                           PTG=""];
                SourceExpr="Shortcut Dimension 1 Code";
                Visible=FALSE }

    { 119 ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the dimension value code associated with the sales header.;
                           PTG=""];
                SourceExpr="Shortcut Dimension 2 Code";
                Visible=FALSE }

    { 123 ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the location from where inventory items to the customer on the sales document are to be shipped by default.;
                           PTG=""];
                SourceExpr="Location Code" }

    { 99  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the name of the sales person who is assigned to the customer.;
                           PTG=""];
                SourceExpr="Salesperson Code";
                Visible=FALSE }

    { 21  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the ID of the user who is responsible for the document.;
                           PTG=""];
                SourceExpr="Assigned User ID" }

    { 11  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the currency of amounts on the sales document.;
                           PTG=""];
                SourceExpr="Currency Code";
                Visible=FALSE }

    { 1102601001;2;Field  ;
                ToolTipML=[ENU=Specifies the date when you created the sales document.;
                           PTG=""];
                SourceExpr="Document Date";
                Visible=FALSE }

    { 1102601003;2;Field  ;
                ToolTipML=[ENU=Specifies the number of the campaign that the document is linked to.;
                           PTG=""];
                SourceExpr="Campaign No.";
                Visible=FALSE }

    { 1102601005;2;Field  ;
                ToolTipML=[ENU=Specifies whether the document is open, waiting to be approved, has been invoiced for prepayment, or has been released to the next stage of processing.;
                           PTG=""];
                SourceExpr=Status;
                Visible=FALSE }

    { 1102601007;2;Field  ;
                ToolTipML=[ENU=Specifies a formula that calculates the payment due date, payment discount date, and payment discount amount on the purchase document.;
                           PTG=""];
                SourceExpr="Payment Terms Code";
                Visible=FALSE }

    { 1102601009;2;Field  ;
                ToolTipML=[ENU=Specifies when the sales invoice must be paid.;
                           PTG=""];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Due Date" }

    { 1102601011;2;Field  ;
                ToolTipML=[ENU=Specifies the payment discount percentage granted if the customer pays on or before the date entered in the Pmt. Discount Date field.;
                           PTG=""];
                SourceExpr="Payment Discount %";
                Visible=FALSE }

    { 1102601022;2;Field  ;
                ToolTipML=[ENU=Specifies how items on the sales document are shipped to the customer.;
                           PTG=""];
                SourceExpr="Shipment Method Code";
                Visible=FALSE }

    { 16  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies which shipping agent is used to transport the items on the sales document to the customer.;
                           PTG=""];
                ApplicationArea=#Suite;
                SourceExpr="Shipping Agent Code";
                Visible=FALSE }

    { 18  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies which shipping agent service is used to transport the items on the sales document to the customer.;
                           PTG=""];
                ApplicationArea=#Suite;
                SourceExpr="Shipping Agent Service Code";
                Visible=FALSE }

    { 40  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the shipping agent's package number.;
                           PTG=""];
                ApplicationArea=#Suite;
                SourceExpr="Package Tracking No.";
                Visible=FALSE }

    { 1102601013;2;Field  ;
                ToolTipML=[ENU=Specifies the date you expect to ship items on the sales document.;
                           PTG=""];
                SourceExpr="Shipment Date";
                Visible=FALSE }

    { 3   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the status of a job queue entry or task that handles the posting of sales orders.;
                           PTG=""];
                ApplicationArea=#All;
                SourceExpr="Job Queue Status";
                Visible=JobQueueActive }

    { 12  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the sum of amounts in the Line Amount field on the sales order lines. It is used to calculate the invoice discount of the sales order.;
                           PTG=""];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=Amount }

    { 901 ;2   ;Field     ;
                SourceExpr="Customer Posting Group";
                Importance=Standard;
                Visible=FALSE;
                Enabled=TRUE;
                Editable=TRUE }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1902018507;1;Part   ;
                SubPageLink=No.=FIELD(Bill-to Customer No.),
                            Date Filter=FIELD(Date Filter);
                PagePartID=Page9082;
                PartType=Page }

    { 1900316107;1;Part   ;
                SubPageLink=No.=FIELD(Bill-to Customer No.),
                            Date Filter=FIELD(Date Filter);
                PagePartID=Page9084;
                PartType=Page }

    { 9   ;1   ;Part      ;
                Name=IncomingDocAttachFactBox;
                PagePartID=Page193;
                Visible=FALSE;
                PartType=Page;
                ShowFilter=No }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {
    VAR
      DummyApplicationAreaSetup@1001 : Record 9178;
      ReportPrint@1102601000 : Codeunit 228;
      LinesInstructionMgt@1003 : Codeunit 1320;
      JobQueueActive@1000 : Boolean INDATASET;
      OpenApprovalEntriesExist@1004 : Boolean;
      OpenPostedSalesInvQst@1006 : TextConst 'ENU=The invoice has been posted and moved to the Posted Sales Invoice list.\\Do you want to open the posted invoice?;PTG=A fatura foi registada e movida para a lista de Faturas de Venda Registadas.\\Deseja abrir a fatura registada?';
      CanCancelApprovalForRecord@1002 : Boolean;

    PROCEDURE ShowPreview@1();
    VAR
      SalesPostYesNo@1001 : Codeunit 81;
    BEGIN
      SalesPostYesNo.Preview(Rec);
    END;

    LOCAL PROCEDURE SetControlAppearance@5();
    VAR
      ApprovalsMgmt@1002 : Codeunit 1535;
    BEGIN
      OpenApprovalEntriesExist := ApprovalsMgmt.HasOpenApprovalEntries(RECORDID);

      CanCancelApprovalForRecord := ApprovalsMgmt.CanCancelApprovalForRecord(RECORDID);
    END;

    LOCAL PROCEDURE Post@4(PostingCodeunitID@1000 : Integer);
    VAR
      PreAssignedNo@1001 : Code[20];
    BEGIN
      IF DummyApplicationAreaSetup.IsFoundationEnabled THEN BEGIN
        LinesInstructionMgt.SalesCheckAllLinesHaveQuantityAssigned(Rec);
        PreAssignedNo := "No.";
      END;

      SendToPosting(PostingCodeunitID);

      IF DummyApplicationAreaSetup.IsFoundationEnabled THEN
        ShowPostedConfirmationMessage(PreAssignedNo);
    END;

    LOCAL PROCEDURE ShowPostedConfirmationMessage@7(PreAssignedNo@1001 : Code[20]);
    VAR
      SalesInvoiceHeader@1000 : Record 112;
    BEGIN
      SalesInvoiceHeader.SETRANGE("Pre-Assigned No.",PreAssignedNo);
      IF SalesInvoiceHeader.FINDFIRST THEN
        IF DIALOG.CONFIRM(OpenPostedSalesInvQst,FALSE) THEN
          PAGE.RUN(PAGE::"Posted Sales Invoice",SalesInvoiceHeader);
    END;

    BEGIN
    END.
  }
}

