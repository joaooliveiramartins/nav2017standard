OBJECT Page 1012 Job Item Prices
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Job Item Prices;
               PTG=Pre�os Produtos Projeto];
    SourceTable=Table1013;
    PageType=List;
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 2   ;2   ;Field     ;
                ToolTipML=ENU=Specifies the number of the job that this item price applies to.;
                ApplicationArea=#Jobs;
                SourceExpr="Job No." }

    { 4   ;2   ;Field     ;
                ToolTipML=ENU=Specifies the number of the job task if the item price should only apply to a specific job task.;
                ApplicationArea=#Jobs;
                SourceExpr="Job Task No." }

    { 6   ;2   ;Field     ;
                ToolTipML=ENU=Specifies the item that this price applies to. Choose the field to see the available items.;
                ApplicationArea=#Jobs;
                SourceExpr="Item No." }

    { 23  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the variant code if the price that you are setting up should apply to a specific variant of the item.;
                ApplicationArea=#Jobs;
                SourceExpr="Variant Code";
                Visible=FALSE }

    { 8   ;2   ;Field     ;
                ToolTipML=ENU=Specifies the unit of measure code if the price that you are setting up should apply to a specific unit of measure.;
                ApplicationArea=#Jobs;
                SourceExpr="Unit of Measure Code" }

    { 12  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the default currency code that is defined for a job. Job item prices will only be used if the currency code for the job item is the same as the currency code set for the job.;
                ApplicationArea=#Jobs;
                SourceExpr="Currency Code" }

    { 10  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the job-specific sales price that applies to this line. This price is in the currency that is represented by the code in the Currency Code field for this line. You can set the unit price to zero if you have agreed with your customer that usage of a certain item is non-chargeable. However, it is recommended that you set the discount to 100% instead.;
                ApplicationArea=#Jobs;
                SourceExpr="Unit Price" }

    { 14  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the unit cost factor, if you have agreed with your customer that he should pay certain item usage by cost value plus a certain percent value to cover your overhead expenses.;
                ApplicationArea=#Jobs;
                SourceExpr="Unit Cost Factor" }

    { 16  ;2   ;Field     ;
                ToolTipML=ENU=Specifies a job-specific line discount percent that applies to this line. This is useful, for example, if you want invoice lines for the job to show a discount percent.;
                ApplicationArea=#Jobs;
                SourceExpr="Line Discount %" }

    { 18  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the description of the item you have entered in the Item No. field.;
                ApplicationArea=#Jobs;
                SourceExpr=Description }

    { 27  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the check box for this field if the job-specific discount percent for this item should apply to the job. The default line discount for the line that is defined is included when job entries are created, but you can modify this value.;
                ApplicationArea=#Jobs;
                SourceExpr="Apply Job Discount";
                Visible=FALSE }

    { 25  ;2   ;Field     ;
                ToolTipML=ENU=Specifies whether the job-specific price or unit cost factor for this item should apply to the job. The default job price that is defined is included when job-related entries are created, but you can modify this value.;
                ApplicationArea=#Jobs;
                SourceExpr="Apply Job Price";
                Visible=FALSE }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {

    BEGIN
    END.
  }
}

