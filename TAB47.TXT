OBJECT Table 47 Aging Band Buffer
{
  OBJECT-PROPERTIES
  {
    Date=05/08/11;
    Time=12:00:00;
    Version List=NAVW16.00;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Aging Band Buffer;
               PTG=Mem. Int. Intervalo Antiguidade];
  }
  FIELDS
  {
    { 1   ;   ;Currency Code       ;Code20        ;CaptionML=[ENU=Currency Code;
                                                              PTG=C�d. Divisa] }
    { 2   ;   ;Column 1 Amt.       ;Decimal       ;CaptionML=[ENU=Column 1 Amt.;
                                                              PTG=Valor Coluna 1] }
    { 3   ;   ;Column 2 Amt.       ;Decimal       ;CaptionML=[ENU=Column 2 Amt.;
                                                              PTG=Valor Coluna 2] }
    { 4   ;   ;Column 3 Amt.       ;Decimal       ;CaptionML=[ENU=Column 3 Amt.;
                                                              PTG=Valor Coluna 3] }
    { 5   ;   ;Column 4 Amt.       ;Decimal       ;CaptionML=[ENU=Column 4 Amt.;
                                                              PTG=Valor Coluna 4] }
    { 6   ;   ;Column 5 Amt.       ;Decimal       ;CaptionML=[ENU=Column 5 Amt.;
                                                              PTG=Valor Coluna 5] }
  }
  KEYS
  {
    {    ;Currency Code                           ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    END.
  }
}

