OBJECT Page 6701 Contact Sync. Setup
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0;
  }
  PROPERTIES
  {
    Permissions=TableData 1261=rimd;
    CaptionML=[ENU=Contact Sync. Setup;
               PTG=Configura��o Sincroniza��o Contacto];
    InsertAllowed=No;
    DeleteAllowed=No;
    LinksAllowed=No;
    SourceTable=Table6700;
    PageType=Card;
    PromotedActionCategoriesML=[ENU=New,Process,Report,Filter,Logging;
                                PTG=Novo,Processar,Mapas,Filtro,Registo];
    OnOpenPage=VAR
                 User@1000 : Record 2000000120;
               BEGIN
                 GetUser(User);
                 IF NOT O365SyncManagement.IsO365Setup(FALSE) THEN
                   ERROR(EmailMissingErr);
               END;

    ActionList=ACTIONS
    {
      { 6       ;    ;ActionContainer;
                      ActionContainerType=ActionItems }
      { 18      ;1   ;ActionGroup;
                      CaptionML=[ENU=Process;
                                 PTG=Processar];
                      Image=Action }
      { 20      ;2   ;Action    ;
                      Name=Validate Exchange Connection;
                      CaptionML=[ENU=Validate Exchange Connection;
                                 PTG=Validar Liga��o Exchange];
                      ToolTipML=ENU=Test that the provided exchange server connection works.;
                      ApplicationArea=#Basic,#Suite;
                      Promoted=Yes;
                      Image=ValidateEmailLoggingSetup;
                      PromotedCategory=Process;
                      OnAction=BEGIN
                                 ProgressWindow.OPEN(ProgressWindowMsg);

                                 IF O365SyncManagement.CreateExchangeConnection(Rec) THEN
                                   MESSAGE(ConnectionSuccessMsg)
                                 ELSE BEGIN
                                   ProgressWindow.CLOSE;
                                   ERROR(ConnectionFailureErr);
                                 END;

                                 ProgressWindow.CLOSE;
                               END;
                                }
      { 4       ;2   ;Action    ;
                      Name=SyncO365;
                      CaptionML=[ENU=Sync with Office 365;
                                 PTG=Sincronizar com Office 365];
                      ToolTipML=ENU=Synchronize with Office 365 based on last sync date and last modified date. All changes in Office 365 since the last sync date will be synchronized back.;
                      ApplicationArea=#Basic,#Suite;
                      Promoted=Yes;
                      Image=Refresh;
                      PromotedCategory=Process;
                      OnAction=BEGIN
                                 CLEAR(O365SyncManagement);
                                 IF O365SyncManagement.IsO365Setup(FALSE) THEN
                                   O365SyncManagement.SyncExchangeContacts(Rec,FALSE);
                               END;
                                }
      { 11      ;2   ;Action    ;
                      Name=SetSyncFilter;
                      CaptionML=[ENU=Set Sync Filter;
                                 PTG=Definir Filtro Sincroniza��o];
                      ToolTipML=ENU=Synchronize, but ignore the last synchronized and last modified dates. All changes will be pushed to Office 365 and take all contacts from your Exchange folder and sync back.;
                      ApplicationArea=#Basic,#Suite;
                      Promoted=Yes;
                      Image=Filter;
                      PromotedCategory=Category4;
                      OnAction=VAR
                                 ExchangeContactSync@1001 : Codeunit 6703;
                               BEGIN
                                 ExchangeContactSync.GetRequestParameters(Rec);
                               END;
                                }
      { 9       ;1   ;ActionGroup;
                      CaptionML=[ENU=Logging;
                                 PTG=Registo] }
      { 10      ;2   ;Action    ;
                      Name=ActivityLog;
                      CaptionML=[ENU=Activity Log;
                                 PTG=Registo Atividade];
                      ToolTipML=ENU=View the status and any errors related to the connection to Exchange.;
                      ApplicationArea=#Basic,#Suite;
                      Image=Log;
                      PromotedCategory=Category5;
                      OnAction=VAR
                                 ActivityLog@1000 : Record 710;
                               BEGIN
                                 ActivityLog.ShowEntries(Rec);
                               END;
                                }
      { 12      ;2   ;Action    ;
                      Name=DeleteActivityLog;
                      CaptionML=[ENU=Delete Activity Log;
                                 PTG=Eliminar Registo Atividade];
                      ToolTipML=ENU=Delete the exchange synchronization log file.;
                      ApplicationArea=#Basic,#Suite;
                      Image=Delete;
                      PromotedCategory=Category5;
                      OnAction=BEGIN
                                 DeleteActivityLog;
                               END;
                                }
    }
  }
  CONTROLS
  {
    { 1   ;0   ;Container ;
                ContainerType=ContentArea }

    { 2   ;1   ;Group     ;
                Name=General;
                CaptionML=[ENU=General;
                           PTG=Geral];
                GroupType=Group }

    { 3   ;2   ;Field     ;
                Lookup=No;
                ToolTipML=ENU=Specifies the ID of the user.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="User ID";
                Editable=false }

    { 5   ;2   ;Field     ;
                ToolTipML=ENU=Specifies the public folder on the Exchange server that you want to use for your queue and storage folders.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Folder ID" }

    { 7   ;2   ;Field     ;
                ToolTipML=ENU=Specifies the last date/time that the Exchange server was synchronized.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Last Sync Date Time" }

    { 8   ;2   ;Field     ;
                CaptionML=[ENU=Enable Background Synchronization;
                           PTG=Ativar Sincroniza��o Background];
                ToolTipML=ENU=Specifies that data synchronization can occur while users perform related tasks.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr=Enabled }

  }
  CODE
  {
    VAR
      O365SyncManagement@1002 : Codeunit 6700;
      ProgressWindow@1008 : Dialog;
      ProgressWindowMsg@1004 : TextConst 'ENU=Validating the connection to Exchange.;PTG=A validar liga��o ao Exchange.';
      ConnectionSuccessMsg@1006 : TextConst 'ENU=Connected successfully to Exchange.;PTG=Liga��o ao Exchange feita com sucesso.';
      ConnectionFailureErr@1005 : TextConst 'ENU=Cannot connect to Exchange. Check your user name, password and Folder ID, and then try again.;PTG=N�o foi poss�vel ligar ao Exchange. Verifique o seu nome de utilizado, password e ID Pasta e tente novamente.';
      EmailMissingErr@1007 : TextConst 'ENU=An authentication email and Exchange password must be set in order to set up contact synchronization.;PTG=Tem de definir um email de autentica��o e password do Exchange para configurar a sincroniza��o de contacto.';

    LOCAL PROCEDURE GetUser@3(VAR User@1000 : Record 2000000120) : Boolean;
    BEGIN
      User.SETRANGE("User Name",USERID);
      EXIT(User.FINDFIRST);
    END;

    BEGIN
    END.
  }
}

