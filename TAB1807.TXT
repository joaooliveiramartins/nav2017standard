OBJECT Table 1807 Assisted Setup Log
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Assisted Setup Log;
               PTG=Registo de Configura��o Assistida];
  }
  FIELDS
  {
    { 1   ;   ;No.                 ;Integer       ;AutoIncrement=Yes;
                                                   CaptionML=[ENU=No.;
                                                              PTG=N�] }
    { 3   ;   ;Entery No.          ;Integer       ;TableRelation="Assisted Setup"."Page ID";
                                                   CaptionML=[ENU=Entery No.;
                                                              PTG=N� Movimento] }
    { 10  ;   ;Date Time           ;DateTime      ;CaptionML=[ENU=Date Time;
                                                              PTG=Data Hora] }
    { 11  ;   ;Invoked Action      ;Option        ;CaptionML=[ENU=Invoked Action;
                                                              PTG=A��o Invocada];
                                                   OptionCaptionML=[ENU=" ,Video,Help,Tour,Assisted Setup";
                                                                    PTG=" V�deo,Ajuda,Apresenta��o,Configura��o Assistida"];
                                                   OptionString=[ ,Video,Help,Tour,Assisted Setup] }
  }
  KEYS
  {
    {    ;No.                                     ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    PROCEDURE Log@1(EnteryNo@1000 : Integer;InvokedActionType@1001 : Option);
    BEGIN
      "Date Time" := CURRENTDATETIME;
      "Entery No." := EnteryNo;
      "Invoked Action" := InvokedActionType;
      INSERT(TRUE);
      COMMIT;
    END;

    BEGIN
    END.
  }
}

