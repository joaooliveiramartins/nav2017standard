OBJECT Table 9052 Service Cue
{
  OBJECT-PROPERTIES
  {
    Date=07/09/12;
    Time=12:00:00;
    Version List=NAVW17.10;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Service Cue;
               PTG=Pilha de Servi�os];
  }
  FIELDS
  {
    { 1   ;   ;Primary Key         ;Code10        ;CaptionML=[ENU=Primary Key;
                                                              PTG=Chave Prim�ria] }
    { 2   ;   ;Service Orders - in Process;Integer;FieldClass=FlowField;
                                                   CalcFormula=Count("Service Header" WHERE (Document Type=FILTER(Order),
                                                                                             Status=FILTER(In Process),
                                                                                             Responsibility Center=FIELD(Responsibility Center Filter)));
                                                   CaptionML=[ENU=Service Orders - in Process;
                                                              PTG=Ordens Servi�o - em Processamento];
                                                   Editable=No }
    { 3   ;   ;Service Orders - Finished;Integer  ;FieldClass=FlowField;
                                                   CalcFormula=Count("Service Header" WHERE (Document Type=FILTER(Order),
                                                                                             Status=FILTER(Finished),
                                                                                             Responsibility Center=FIELD(Responsibility Center Filter)));
                                                   CaptionML=[ENU=Service Orders - Finished;
                                                              PTG=Ordens Servi�o - Conclu�das];
                                                   Editable=No }
    { 4   ;   ;Service Orders - Inactive;Integer  ;FieldClass=FlowField;
                                                   CalcFormula=Count("Service Header" WHERE (Document Type=FILTER(Order),
                                                                                             Status=FILTER(Pending|On Hold),
                                                                                             Responsibility Center=FIELD(Responsibility Center Filter)));
                                                   CaptionML=[ENU=Service Orders - Inactive;
                                                              PTG=Ordens Servi�o - Inativas];
                                                   Editable=No }
    { 5   ;   ;Open Service Quotes ;Integer       ;FieldClass=FlowField;
                                                   CalcFormula=Count("Service Header" WHERE (Document Type=FILTER(Quote),
                                                                                             Status=FILTER(Pending|On Hold),
                                                                                             Responsibility Center=FIELD(Responsibility Center Filter)));
                                                   CaptionML=[ENU=Open Service Quotes;
                                                              PTG=Abrir Propostas Servi�o];
                                                   Editable=No }
    { 6   ;   ;Open Service Contract Quotes;Integer;
                                                   FieldClass=FlowField;
                                                   CalcFormula=Count("Service Contract Header" WHERE (Contract Type=FILTER(Quote),
                                                                                                      Status=FILTER(' '),
                                                                                                      Responsibility Center=FIELD(Responsibility Center Filter)));
                                                   CaptionML=[ENU=Open Service Contract Quotes;
                                                              PTG=Abrir Propostas Contrato Servi�o];
                                                   Editable=No }
    { 7   ;   ;Service Contracts to Expire;Integer;FieldClass=FlowField;
                                                   CalcFormula=Count("Service Contract Header" WHERE (Contract Type=FILTER(Contract),
                                                                                                      Expiration Date=FIELD(Date Filter),
                                                                                                      Responsibility Center=FIELD(Responsibility Center Filter)));
                                                   CaptionML=[ENU=Service Contracts to Expire;
                                                              PTG=Contratos Servi�o a Expirar];
                                                   Editable=No }
    { 8   ;   ;Service Orders - Today;Integer     ;FieldClass=FlowField;
                                                   CalcFormula=Count("Service Header" WHERE (Document Type=FILTER(Order),
                                                                                             Response Date=FIELD(Date Filter),
                                                                                             Responsibility Center=FIELD(Responsibility Center Filter)));
                                                   CaptionML=[ENU=Service Orders - Today;
                                                              PTG=Ordens Servi�o - Hoje];
                                                   Editable=No }
    { 9   ;   ;Service Orders - to Follow-up;Integer;
                                                   FieldClass=FlowField;
                                                   CalcFormula=Count("Service Header" WHERE (Document Type=FILTER(Order),
                                                                                             Status=FILTER(In Process),
                                                                                             Responsibility Center=FIELD(Responsibility Center Filter)));
                                                   CaptionML=[ENU=Service Orders - to Follow-up;
                                                              PTG=Ordens Servi�o - para Seguimento];
                                                   Editable=No }
    { 20  ;   ;Date Filter         ;Date          ;FieldClass=FlowFilter;
                                                   CaptionML=[ENU=Date Filter;
                                                              PTG=Filtro Data];
                                                   Editable=No }
    { 21  ;   ;Responsibility Center Filter;Code10;FieldClass=FlowFilter;
                                                   CaptionML=[ENU=Responsibility Center Filter;
                                                              PTG=Filtro Centro Responsabilidade];
                                                   Editable=No }
  }
  KEYS
  {
    {    ;Primary Key                             ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    PROCEDURE SetRespCenterFilter@1();
    VAR
      UserSetupMgt@1000 : Codeunit 5700;
      RespCenterCode@1001 : Code[10];
    BEGIN
      RespCenterCode := UserSetupMgt.GetServiceFilter;
      IF RespCenterCode <> '' THEN BEGIN
        FILTERGROUP(2);
        SETRANGE("Responsibility Center Filter",RespCenterCode);
        FILTERGROUP(0);
      END;
    END;

    BEGIN
    END.
  }
}

