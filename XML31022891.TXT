OBJECT XMLport 31022891 Export AGECOP Statement
{
  OBJECT-PROPERTIES
  {
    Date=10/10/16;
    Time=13:00:00;
    Version List=NAVW17.10,NAVPTSS93.00;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Export AGECOP Statement;
               PTG=Exportar Declara��o AGECOP];
    Direction=Export;
    TextEncoding=UTF-8;
    OnPreXMLport=BEGIN
                   PeriodEndDate := CALCDATE(PeriodLength,PeriodStartDate);
                 END;

    Format=Variable Text;
    FieldSeparator=[;];
    TableSeparator=<NewLine>;
    FileName=Declara��o.csv;
  }
  ELEMENTS
  {
    { [{77C044CF-2320-4F2C-9E57-DD635AA51C1D}];  ;Root                ;Element ;Text     }

    { [{7F1B15DD-6B26-47A0-B273-F943A8ACDF37}];1 ;DeclarationHeader   ;Element ;Table   ;
                                                  SourceTable=Table2000000026;
                                                  SourceTableView=SORTING(Field1)
                                                                  WHERE(Field1=CONST(1));
                                                  Export::OnAfterGetRecord=BEGIN
                                                                             "Temporary Table".DELETEALL;
                                                                             ItemLedgEntry.SETCURRENTKEY("Product ID",Size);
                                                                             ItemLedgEntry.SETRANGE("Compensation Tax",TRUE);
                                                                             ItemLedgEntry.SETFILTER("Product ID",'<>%1','');
                                                                             ItemLedgEntry.SETRANGE("Posting Date",PeriodStartDate,PeriodEndDate);
                                                                             IF ItemLedgEntry.FINDSET THEN REPEAT
                                                                               IF ((ItemLedgEntry."Product ID" <> ItemLedgEntryProdID) AND (ItemLedgEntryProdID <> '')) OR ((ItemLedgEntry.Size <> ItemLedgEntrySize) AND (ItemLedgEntrySize <> 0)) THEN BEGIN
                                                                                 InsertTempTable;
                                                                                 UndVendPT := 0;
                                                                                 UndVendUE_OM := 0;
                                                                                 QuantNc_Dev := 0;
                                                                                 ItemLedgEntryUndSalesAmt := 0;
                                                                               END;

                                                                               CompanyInfo.GET;

                                                                               ItemLedgEntryProdID := ItemLedgEntry."Product ID";
                                                                               ItemLedgEntrySize := ItemLedgEntry.Size;

                                                                               IF (ItemLedgEntry."Document Type" = ItemLedgEntry."Document Type" :: "Sales Shipment") OR (ItemLedgEntry."Document Type" = ItemLedgEntry."Document Type"::"Sales Invoice") THEN BEGIN
                                                                                 IF ItemLedgEntry."Country/Region Code" IN ['',CompanyInfo."Country/Region Code"] THEN
                                                                                   UndVendPT += -ItemLedgEntry.Quantity
                                                                                 ELSE
                                                                                   UndVendUE_OM += -ItemLedgEntry.Quantity;

                                                                                 ItemLedgEntryUndSalesAmt += ItemLedgEntry."Sales Amount Excl. Comp.";
                                                                               END ELSE
                                                                                 QuantNc_Dev += ItemLedgEntry.Quantity;

                                                                               UndNVendPT := 0;
                                                                               UndNVendNrIsencao := 0;
                                                                               UndNVendAlinea := '';
                                                                               UndIsencoes := 0;
                                                                               UndNrIsencoes := 0;
                                                                               UndAlinea := '';
                                                                               ExpExpIntacom := 0;
                                                                               IF IncludeUnitTaxAmount THEN
                                                                                 ValorTaxa := ItemLedgEntry."Unit Tax Amount";
                                                                             UNTIL ItemLedgEntry.NEXT = 0;

                                                                             InsertTempTable;
                                                                           END;
                                                                            }

    { [{AAF9CB34-29F3-411D-95F3-2DD09E846CDC}];2 ;ProdutoID           ;Element ;Text    ;
                                                  Export::OnBeforePassVariable=BEGIN
                                                                                 ProdutoID := Text31022890;
                                                                               END;
                                                                                }

    { [{CD73ECE0-8341-408E-989F-D449637ED043}];2 ;Capacidade          ;Element ;Text    ;
                                                  Export::OnBeforePassVariable=BEGIN
                                                                                 Capacidade := Text31022891;
                                                                               END;
                                                                                }

    { [{D7415DF3-92A8-4BAE-97EC-D8487CAE82A5}];2 ;PrecoVenda          ;Element ;Text    ;
                                                  Export::OnBeforePassVariable=BEGIN
                                                                                 PrecoVenda := Text31022892;
                                                                               END;
                                                                                }

    { [{D74A6E0F-7F2B-42E1-8F71-C1B671D7D82E}];2 ;ComCompensacaoEquitativaLiquidada;Element;Text;
                                                  Export::OnBeforePassVariable=BEGIN
                                                                                 ComCompensacaoEquitativaLiquidada := Text31022893;
                                                                               END;
                                                                                }

    { [{159E6DB4-09AB-4253-8488-1B33574A0116}];2 ;ComCompensacaoEquitativaNaoLiquidada;Element;Text;
                                                  Export::OnBeforePassVariable=BEGIN
                                                                                 ComCompensacaoEquitativaNaoLiquidada := Text31022894;
                                                                               END;
                                                                                }

    { [{1010BFD9-7E55-49DA-AC2A-DBE030DC55D3}];2 ;ComCompensacaoEquitativaNaoLiquidadaNrIsencao;Element;Text;
                                                  Export::OnBeforePassVariable=BEGIN
                                                                                 ComCompensacaoEquitativaNaoLiquidadaNrIsencao :=Text31022895;
                                                                               END;
                                                                                }

    { [{3635076D-E0A3-46E0-9DB1-B2B128FE572C}];2 ;ComCompensacaoEquitativaNaoLiquidadaAlinea;Element;Text;
                                                  Export::OnBeforePassVariable=BEGIN
                                                                                 ComCompensacaoEquitativaNaoLiquidadaAlinea := Text31022896;
                                                                               END;
                                                                                }

    { [{48CD860C-410E-4BEE-8B61-7FAF37F97D67}];2 ;UnidadesVendidasUniaoEuropeiaPaisesTerceiros;Element;Text;
                                                  Export::OnBeforePassVariable=BEGIN
                                                                                 UnidadesVendidasUniaoEuropeiaPaisesTerceiros := Text31022897;
                                                                               END;
                                                                                }

    { [{7C97FB97-3291-4FB8-9FC5-DB264CF0B78F}];2 ;IsencoesConcedidasAGECOP;Element;Text ;
                                                  Export::OnBeforePassVariable=BEGIN
                                                                                 IsencoesConcedidasAGECOP := Text31022898;
                                                                               END;
                                                                                }

    { [{3C3A2DB6-6836-4133-9CF7-A3222B27E19D}];2 ;IsencoesConcedidasAGECOPNrIsencao;Element;Text;
                                                  Export::OnBeforePassVariable=BEGIN
                                                                                 IsencoesConcedidasAGECOPNrIsencao := Text31022899;
                                                                               END;
                                                                                }

    { [{F4632292-1854-4B26-BFE3-6204C7900B3F}];2 ;IsencoesConcedidasAGECOPAlinea;Element;Text;
                                                  Export::OnBeforePassVariable=BEGIN
                                                                                 IsencoesConcedidasAGECOPAlinea := Text31022900;
                                                                               END;
                                                                                }

    { [{9D682F66-936E-4623-A296-2D01AE8BA590}];2 ;ExportacoesExpedicoesIntracomunitarias;Element;Text;
                                                  Export::OnBeforePassVariable=BEGIN
                                                                                 ExportacoesExpedicoesIntracomunitarias := Text31022901;
                                                                               END;
                                                                                }

    { [{668C8F94-573F-4189-BF6B-C239C3DF5363}];2 ;DevolucoesSituacoesCredito;Element;Text;
                                                  Export::OnBeforePassVariable=BEGIN
                                                                                 DevolucoesSituacoesCredito := Text31022902;
                                                                               END;
                                                                                }

    { [{6523689A-13C3-40E2-8018-8E6F5383E401}];2 ;ValorTaxaUnit       ;Element ;Text    ;
                                                  Export::OnBeforePassVariable=BEGIN
                                                                                 IF NOT IncludeUnitTaxAmount THEN currXMLport.BREAKUNBOUND;
                                                                                 ValorTaxaUnit := Text31022903;
                                                                               END;
                                                                                }

    { [{249C3E65-2B7D-477D-8E46-B493068CD824}];1 ;DeclarationBody     ;Element ;Table   ;
                                                  SourceTable=Table31022999;
                                                  SourceTableView=SORTING(Field1);
                                                  Temporary=Yes }

    { [{F3D2F0D4-A66E-4EB6-AEA2-0D839A60D1B6}];2 ;ItemLedgEntryProdIDTxt;Element;Field  ;
                                                  DataType=Code;
                                                  SourceField=Temporary Table::Code1 }

    { [{FC68131A-79EF-45AA-BF80-D3E8FF2C19FA}];2 ;ItemLedgEntrySizeTxt;Element ;Field   ;
                                                  DataType=Decimal;
                                                  SourceField=Temporary Table::Decimal1 }

    { [{5A62A232-1A39-452A-B48F-810DDDA4E4A1}];2 ;ItemLedgEntryUndSalesAmtTxt;Element;Field;
                                                  DataType=Decimal;
                                                  SourceField=Temporary Table::Decimal2 }

    { [{B60F93A8-C5E7-49C3-98AD-C63A99EDA8AB}];2 ;UndVendPTTxt        ;Element ;Field   ;
                                                  DataType=Decimal;
                                                  SourceField=Temporary Table::Decimal3 }

    { [{FBFD7AA1-59EF-4EC4-A73B-44D0D3547137}];2 ;UndNVendPTTxt       ;Element ;Field   ;
                                                  DataType=Integer;
                                                  SourceField=Temporary Table::Integer1 }

    { [{E104DC1D-3768-4A44-8264-0BBF521A1E65}];2 ;UndNVendNrIsencaoTxt;Element ;Field   ;
                                                  DataType=Integer;
                                                  SourceField=Temporary Table::Integer2 }

    { [{818511B4-D5AC-4545-BA63-02426D1B703D}];2 ;UndNVendAlineaTxt   ;Element ;Field   ;
                                                  DataType=Text;
                                                  SourceField=Temporary Table::Text2 }

    { [{CC430FBA-00CE-43DB-BC0B-CC2494940B6A}];2 ;UndVendUE_OMTxt     ;Element ;Field   ;
                                                  DataType=Decimal;
                                                  SourceField=Temporary Table::Decimal4 }

    { [{79004F75-E177-4FB5-8A6D-DA7229BE5D1E}];2 ;UndIsencoesTxt      ;Element ;Field   ;
                                                  DataType=Integer;
                                                  SourceField=Temporary Table::Integer3 }

    { [{70A5A444-1EED-47F2-B39E-7635A096A63E}];2 ;UndNrIsencoesTxt    ;Element ;Field   ;
                                                  DataType=Integer;
                                                  SourceField=Temporary Table::Integer4 }

    { [{4F8D4836-C977-4BC7-8AE7-263556FDA6B5}];2 ;UndAlineaTxt        ;Element ;Field   ;
                                                  DataType=Text;
                                                  SourceField=Temporary Table::Text3 }

    { [{779CCB65-038E-46F1-A6D7-6F6526F1F193}];2 ;ExpExpIntracom      ;Element ;Field   ;
                                                  DataType=Text;
                                                  SourceField=Temporary Table::Text4 }

    { [{B6BC161E-F63D-4224-B7FF-00ED526B88EC}];2 ;QuantNc_DevTxt      ;Element ;Field   ;
                                                  DataType=Integer;
                                                  SourceField=Temporary Table::Integer5 }

    { [{21673B3F-84F8-4DA8-AA96-1D9C9941A314}];2 ;ValorTaxaTxt        ;Element ;Field   ;
                                                  DataType=Decimal;
                                                  SourceField=Temporary Table::Decimal5;
                                                  Export::OnBeforePassField=BEGIN
                                                                              IF NOT IncludeUnitTaxAmount THEN currXMLport.BREAKUNBOUND;
                                                                            END;
                                                                             }

  }
  EVENTS
  {
  }
  REQUESTPAGE
  {
    PROPERTIES
    {
      OnOpenPage=BEGIN
                   IF PeriodStartDate = 0D THEN
                     PeriodStartDate := WORKDATE;
                   IF FORMAT(PeriodLength) = '' THEN
                     EVALUATE(PeriodLength,'<1M>');
                 END;

    }
    CONTROLS
    {
      { 31022890;;Container ;
                  ContainerType=ContentArea }

      { 31022893;1;Field    ;
                  Name=StartingDate;
                  CaptionML=[ENU=Starting Date;
                             PTG=Data Inicial];
                  NotBlank=Yes;
                  SourceExpr=PeriodStartDate }

      { 31022892;1;Field    ;
                  Name=PeriodLength;
                  CaptionML=[ENU=Period Length;
                             PTG=Dura��o Per�odo];
                  SourceExpr=PeriodLength }

      { 31022891;1;Field    ;
                  CaptionML=PTG=Incluir Valor Taxa Unit�ria;
                  SourceExpr=IncludeUnitTaxAmount }

    }
  }
  CODE
  {
    VAR
      CompanyInfo@31022891 : Record 79;
      ItemLedgEntryProdID@31022902 : Code[10];
      ItemLedgEntrySize@31022892 : Decimal;
      ItemLedgEntryUndSalesAmt@31022890 : Decimal;
      UndVendPT@31022893 : Decimal;
      UndNVendPT@31022894 : Decimal;
      UndNVendNrIsencao@31022895 : Decimal;
      UndNVendAlinea@31022903 : Text;
      UndVendUE_OM@31022896 : Decimal;
      UndIsencoes@31022897 : Decimal;
      UndNrIsencoes@31022898 : Decimal;
      UndAlinea@31022904 : Text;
      ExpExpIntacom@31022921 : Decimal;
      QuantNc_Dev@31022899 : Decimal;
      ValorTaxa@31022900 : Decimal;
      ItemLedgEntry@31022901 : Record 32;
      Text31022890@31022918 : TextConst 'PTG=ProdutoID';
      Text31022891@31022917 : TextConst 'PTG=Capacidade';
      Text31022892@31022916 : TextConst 'PTG=PrecoVenda';
      Text31022893@31022915 : TextConst 'PTG=ComCompensacaoEquitativaLiquidada';
      Text31022894@31022914 : TextConst 'PTG=ComCompensacaoEquitativaNaoLiquidada';
      Text31022895@31022913 : TextConst 'PTG=ComCompensacaoEquitativaNaoLiquidadaNrIsencao';
      Text31022896@31022912 : TextConst 'PTG=ComCompensacaoEquitativaNaoLiquidadaAlinea';
      Text31022897@31022911 : TextConst 'PTG=UnidadesVendidasUniaoEuropeiaPaisesTerceiros';
      Text31022898@31022910 : TextConst 'PTG=IsencoesConcedidasAGECOP';
      Text31022899@31022909 : TextConst 'PTG=IsencoesConcedidasAGECOPNrIsencao';
      Text31022900@31022908 : TextConst 'PTG=IsencoesConcedidasAGECOPAlinea';
      Text31022901@31022907 : TextConst 'PTG=ExportacoesExpedicoesIntracomunitarias';
      Text31022902@31022906 : TextConst 'PTG=DevolucoesSituacoesCredito';
      Text31022903@31022919 : TextConst 'PTG=ValorTaxa';
      Text31022904@31022905 : TextConst 'PTG=Declara��o.csv';
      IncludeUnitTaxAmount@31022920 : Boolean;
      Period@31022922 : 'Year,Quarter,Month';
      PeriodLength@31022924 : DateFormula;
      PeriodStartDate@31022923 : Date;
      PeriodEndDate@31022926 : Date;

    LOCAL PROCEDURE InsertTempTable@31022890();
    BEGIN
      "Temporary Table"."Entry No." += 1;
      "Temporary Table".Code1 := ItemLedgEntryProdID;
      "Temporary Table".Decimal1 := ItemLedgEntrySize;
      "Temporary Table".Decimal2 := ItemLedgEntryUndSalesAmt / (UndVendPT + UndVendUE_OM);
      "Temporary Table".Decimal3 := UndVendPT;
      "Temporary Table".Integer1 := UndNVendPT;
      "Temporary Table".Integer2 := UndNVendNrIsencao;
      "Temporary Table".Text2 := UndNVendAlinea;
      "Temporary Table".Decimal4 := UndVendUE_OM;
      "Temporary Table".Integer3 := UndIsencoes;
      "Temporary Table".Integer4 := UndNrIsencoes;
      "Temporary Table".Text3 := UndAlinea;
      "Temporary Table".Text4 := FORMAT(ExpExpIntacom);
      "Temporary Table".Integer5 := QuantNc_Dev;
      IF IncludeUnitTaxAmount THEN
        "Temporary Table".Decimal5 := ValorTaxa;
      "Temporary Table".INSERT;
    END;

    BEGIN
    END.
  }
}

