OBJECT Page 2108 O365 Outstanding Customer List
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0;
  }
  PROPERTIES
  {
    Editable=No;
    CaptionML=[ENU=Customers;
               PTG=Cliente];
    InsertAllowed=No;
    DeleteAllowed=No;
    ModifyAllowed=No;
    SourceTable=Table18;
    SourceTableView=SORTING(Name);
    PageType=List;
    ActionList=ACTIONS
    {
      { 3       ;    ;ActionContainer;
                      ActionContainerType=ActionItems }
      { 5       ;1   ;Action    ;
                      ShortCutKey=Return;
                      CaptionML=[ENU=View;
                                 PTG=Visualizar];
                      ToolTipML=[ENU=Open the card for the selected record.;
                                 PTG=Abrir a ficha para o registo selecionado.];
                      ApplicationArea=#Basic,#Suite;
                      Image=ViewDetails;
                      RunPageMode=View;
                      OnAction=VAR
                                 O365SalesDocument@1000 : Record 2103;
                               BEGIN
                                 O365SalesDocument.SETRANGE(Posted,TRUE);
                                 O365SalesDocument.SETFILTER("Outstanding Amount",'>0');
                                 O365SalesDocument.SETFILTER("Sell-to Customer No.","No.");
                                 O365SalesDocument.SetSortByDocDate;

                                 PAGE.RUN(PAGE::"O365 Customer Sales Documents",O365SalesDocument);
                               END;

                      Gesture=None }
      { 7       ;1   ;Action    ;
                      Name=NewSalesInvoice;
                      CaptionML=[ENU=New Invoice;
                                 PTG=Nova Fatura];
                      ToolTipML=[ENU=Create a new invoice for the customer.;
                                 PTG=Criar uma nova fatura para o cliente.];
                      ApplicationArea=#Basic,#Suite;
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=NewSalesInvoice;
                      RunPageMode=Create;
                      Scope=Repeater;
                      OnAction=VAR
                                 SalesHeader@1000 : Record 36;
                               BEGIN
                                 SalesHeader.INIT;
                                 SalesHeader.VALIDATE("Document Type",SalesHeader."Document Type"::Invoice);
                                 SalesHeader.VALIDATE("Sell-to Customer No.","No.");
                                 SalesHeader.INSERT(TRUE);
                                 COMMIT;

                                 PAGE.RUN(PAGE::"O365 Sales Invoice",SalesHeader);
                               END;

                      Gesture=RightSwipe }
    }
  }
  CONTROLS
  {
    { 1900000001;;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                CaptionML=[ENU="";
                           PTG=""];
                GroupType=Repeater }

    { 2   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the number of the customer.;
                           PTG=Especifica o n�mero do cliente];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="No.";
                Visible=FALSE }

    { 4   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the customer's name. This name will appear on all sales documents for the customer. You can enter a maximum of 50 characters, both numbers and letters.;
                           PTG=Especifica o nome do cliente. Este nome vai aparecer em todos os documentos de venda para este cliente. Pode inserir um m�ximo de 50 caract�res, n�meros e letras.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=Name }

    { 6   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the customer's telephone number.;
                           PTG=Especifica o n�mero de telefone de cliente.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Phone No." }

    { 34  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the name of the person you regularly contact when you do business with this customer.;
                           PTG=Especifica o nome da pessoa que contacta regularmente quando faz neg�cios com este cliente.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=Contact }

    { 62  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the payment amount that the customer owes for completed sales. This value is also known as the customer's balance.;
                           PTG=Especifica o valor de pagamento que o cliente deve para as vendas efetuadas. Este valor � tamb�m conhecido como saldo cliente];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Balance (LCY)";
                OnDrillDown=BEGIN
                              OpenCustomerLedgerEntries(FALSE);
                            END;
                             }

    { 59  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies payments from the customer that are overdue per today's date.;
                           PTG=Especifica pagamentos de cliente que est�o vencidos � data de hoje.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Balance Due (LCY)";
                OnDrillDown=BEGIN
                              OpenCustomerLedgerEntries(TRUE);
                            END;
                             }

    { 32  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the total net amount of sales to the customer in LCY.;
                           PTG=Especifica o valor l�quido total de vendas para o cliente em DL.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Sales (LCY)" }

  }
  CODE
  {

    BEGIN
    END.
  }
}

