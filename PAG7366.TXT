OBJECT Page 7366 Whse. Worksheet Template List
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0;
  }
  PROPERTIES
  {
    Editable=No;
    CaptionML=[ENU=Whse. Worksheet Template List;
               PTG=Lista Livro Folha Armaz�m];
    SourceTable=Table7328;
    PageType=List;
    ActionList=ACTIONS
    {
      { 1900000003;0 ;ActionContainer;
                      ActionContainerType=RelatedInformation }
      { 15      ;1   ;ActionGroup;
                      CaptionML=[ENU=Te&mplate;
                                 PTG=Mo&delo];
                      Image=Template }
      { 16      ;2   ;Action    ;
                      CaptionML=[ENU=Names;
                                 PTG=Nomes];
                      RunObject=Page 7344;
                      RunPageLink=Worksheet Template Name=FIELD(Name);
                      Image=Description }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 2   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the name you enter for the warehouse worksheet template you are creating.;
                           PTG=""];
                SourceExpr=Name }

    { 4   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the description of the warehouse worksheet template you are creating.;
                           PTG=""];
                SourceExpr=Description }

    { 6   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies information about the activity you can plan in the warehouse worksheets that will be defined by this template.;
                           PTG=""];
                SourceExpr=Type }

    { 8   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the window number used by the program for the warehouse worksheet.;
                           PTG=""];
                SourceExpr="Page ID" }

    { 10  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the name of the page with the Page ID entered on the line.;
                           PTG=""];
                SourceExpr="Page Caption" }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {

    BEGIN
    END.
  }
}

