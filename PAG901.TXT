OBJECT Page 901 Assembly Order Subform
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0.00.16177;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Lines;
               PTG=Linhas];
    SourceTable=Table901;
    DelayedInsert=Yes;
    PopulateAllFields=Yes;
    PageType=ListPart;
    AutoSplitKey=Yes;
    OnAfterGetRecord=BEGIN
                       ReservationStatusField := ReservationStatus;
                     END;

    OnDeleteRecord=VAR
                     AssemblyLineReserve@1000 : Codeunit 926;
                   BEGIN
                     IF (Quantity <> 0) AND ItemExists("No.") THEN BEGIN
                       COMMIT;
                       IF NOT AssemblyLineReserve.DeleteLineConfirm(Rec) THEN
                         EXIT(FALSE);
                       AssemblyLineReserve.DeleteLine(Rec);
                     END;
                   END;

    ActionList=ACTIONS
    {
      { 40      ;    ;ActionContainer;
                      ActionContainerType=ActionItems }
      { 27      ;1   ;ActionGroup;
                      CaptionML=[ENU=&Line;
                                 PTG=&Linha];
                      ActionContainerType=ActionItems;
                      Image=Line }
      { 25      ;2   ;ActionGroup;
                      CaptionML=[ENU=Item Availability by;
                                 PTG=Disponibilidade Produto por];
                      ActionContainerType=NewDocumentItems;
                      Image=ItemAvailability }
      { 39      ;3   ;Action    ;
                      CaptionML=[ENU=Event;
                                 PTG=Ocorr�ncia];
                      Image=Event;
                      OnAction=BEGIN
                                 ItemAvailFormsMgt.ShowItemAvailFromAsmLine(Rec,ItemAvailFormsMgt.ByEvent);
                               END;
                                }
      { 28      ;3   ;Action    ;
                      CaptionML=[ENU=Period;
                                 PTG=Per�odo];
                      Image=Period;
                      OnAction=BEGIN
                                 ItemAvailFormsMgt.ShowItemAvailFromAsmLine(Rec,ItemAvailFormsMgt.ByPeriod);
                               END;
                                }
      { 32      ;3   ;Action    ;
                      CaptionML=[ENU=Variant;
                                 PTG=Variante];
                      Image=ItemVariant;
                      OnAction=BEGIN
                                 ItemAvailFormsMgt.ShowItemAvailFromAsmLine(Rec,ItemAvailFormsMgt.ByVariant);
                               END;
                                }
      { 29      ;3   ;Action    ;
                      AccessByPermission=TableData 14=R;
                      CaptionML=[ENU=Location;
                                 PTG=Localiza��o];
                      Image=Warehouse;
                      OnAction=BEGIN
                                 ItemAvailFormsMgt.ShowItemAvailFromAsmLine(Rec,ItemAvailFormsMgt.ByLocation);
                               END;
                                }
      { 44      ;3   ;Action    ;
                      CaptionML=[ENU=BOM Level;
                                 PTG=N�vel L.M.];
                      Image=BOMLevel;
                      OnAction=BEGIN
                                 ItemAvailFormsMgt.ShowItemAvailFromAsmLine(Rec,ItemAvailFormsMgt.ByBOM);
                               END;
                                }
      { 94      ;2   ;Action    ;
                      AccessByPermission=TableData 27=R;
                      CaptionML=[ENU=Reservation Entries;
                                 PTG=Movs. Reserva];
                      Image=ReservationLedger;
                      OnAction=BEGIN
                                 ShowReservationEntries(TRUE);
                               END;
                                }
      { 45      ;2   ;Action    ;
                      Name=Item Tracking Lines;
                      ShortCutKey=Shift+Ctrl+I;
                      CaptionML=[ENU=Item &Tracking Lines;
                                 PTG=Linhas Ras&treio Produto];
                      Image=ItemTrackingLines;
                      OnAction=BEGIN
                                 OpenItemTrackingLines;
                               END;
                                }
      { 34      ;2   ;Action    ;
                      CaptionML=[ENU=Show Warning;
                                 PTG=Mostrar Aviso];
                      Image=ShowWarning;
                      OnAction=BEGIN
                                 ShowAvailabilityWarning;
                               END;
                                }
      { 23      ;2   ;Action    ;
                      AccessByPermission=TableData 348=R;
                      ShortCutKey=Shift+Ctrl+D;
                      CaptionML=[ENU=Dimensions;
                                 PTG=Dimens�es];
                      ToolTipML=ENU=View or edit dimensions, such as area, project, or department, that you can assign to sales and purchase documents to distribute costs and analyze transaction history.;
                      Image=Dimensions;
                      OnAction=BEGIN
                                 ShowDimensions;
                               END;
                                }
      { 21      ;2   ;Action    ;
                      CaptionML=[ENU=Co&mments;
                                 PTG=Co&ment�rios];
                      RunObject=Page 907;
                      RunPageLink=Document Type=FIELD(Document Type),
                                  Document No.=FIELD(Document No.),
                                  Document Line No.=FIELD(Line No.);
                      Image=ViewComments }
      { 16      ;2   ;Action    ;
                      Name=AssemblyBOM;
                      CaptionML=[ENU=Assembly BOM;
                                 PTG=L.M. Montagem];
                      Image=AssemblyBOM;
                      OnAction=BEGIN
                                 ShowAssemblyList;
                               END;
                                }
      { 41      ;1   ;ActionGroup;
                      CaptionML=[ENU=F&unctions;
                                 PTG=F&un��es];
                      ActionContainerType=NewDocumentItems;
                      Image=Action }
      { 26      ;2   ;Action    ;
                      Name=SelectItemSubstitution;
                      CaptionML=[ENU=Select Item Substitution;
                                 PTG=Selecionar Subs. Produto];
                      Image=SelectItemSubstitution;
                      OnAction=BEGIN
                                 CurrPage.SAVERECORD;
                                 ShowItemSub;
                                 CurrPage.UPDATE(TRUE);
                                 AutoReserve;
                               END;
                                }
      { 14      ;2   ;Action    ;
                      Name=ExplodeBOM;
                      CaptionML=[ENU=E&xplode BOM;
                                 PTG=E&xpandir L.M.];
                      Image=ExplodeBOM;
                      OnAction=BEGIN
                                 ExplodeAssemblyList;
                                 CurrPage.UPDATE;
                               END;
                                }
      { 93      ;2   ;Action    ;
                      Name=Reserve;
                      Ellipsis=Yes;
                      CaptionML=[ENU=&Reserve;
                                 PTG=&Reserva];
                      Image=Reserve;
                      OnAction=BEGIN
                                 ShowReservation;
                               END;
                                }
      { 42      ;2   ;Action    ;
                      CaptionML=[ENU=Order &Tracking;
                                 PTG=Ras&treio Encomenda];
                      Image=OrderTracking;
                      OnAction=BEGIN
                                 ShowTracking;
                               END;
                                }
    }
  }
  CONTROLS
  {
    { 1   ;0   ;Container ;
                ContainerType=ContentArea }

    { 2   ;1   ;Group     ;
                Name=Group;
                GroupType=Repeater }

    { 33  ;2   ;Field     ;
                DrillDown=Yes;
                ToolTipML=ENU=Specifies Yes if the assembly component is not available in the quantity and on the due date of the assembly order line.;
                BlankZero=Yes;
                SourceExpr="Avail. Warning";
                OnDrillDown=BEGIN
                              ShowAvailabilityWarning;
                            END;
                             }

    { 3   ;2   ;Field     ;
                ToolTipML=ENU=Specifies if the assembly order line is of type Item or Resource.;
                SourceExpr=Type }

    { 35  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the item or resource that is represented by the assembly order line.;
                SourceExpr="No.";
                OnValidate=BEGIN
                             ReserveItem;
                           END;
                            }

    { 6   ;2   ;Field     ;
                ToolTipML=ENU=Specifies the description of the assembly component.;
                SourceExpr=Description }

    { 5   ;2   ;Field     ;
                ToolTipML=ENU=Specifies the second description of the assembly component.;
                SourceExpr="Description 2";
                Visible=False }

    { 4   ;2   ;Field     ;
                ToolTipML=ENU=Specifies the code of the item variant of the assembly component.;
                SourceExpr="Variant Code";
                OnValidate=BEGIN
                             ReserveItem;
                           END;
                            }

    { 12  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the location from which you want to post consumption of the assembly component.;
                SourceExpr="Location Code";
                OnValidate=BEGIN
                             ReserveItem;
                           END;
                            }

    { 8   ;2   ;Field     ;
                ToolTipML=ENU=Specifies the unit of measure in which the assembly component is consumed on the assembly order.;
                SourceExpr="Unit of Measure Code";
                OnValidate=BEGIN
                             ReserveItem;
                           END;
                            }

    { 13  ;2   ;Field     ;
                ToolTipML=ENU=Specifies how many units of the assembly component are required to assemble one assembly item.;
                SourceExpr="Quantity per";
                OnValidate=BEGIN
                             ReserveItem;
                           END;
                            }

    { 15  ;2   ;Field     ;
                ToolTipML=ENU=Specifies how many units of the assembly component are expected to be consumed.;
                SourceExpr=Quantity;
                OnValidate=BEGIN
                             ReserveItem;
                           END;
                            }

    { 36  ;2   ;Field     ;
                ToolTipML=ENU=Specifies how many units of the assembly component you want to post as consumed when you post the assembly order.;
                SourceExpr="Quantity to Consume" }

    { 37  ;2   ;Field     ;
                ToolTipML=ENU=Specifies how many units of the assembly component have been posted as consumed during the assembly.;
                SourceExpr="Consumed Quantity" }

    { 38  ;2   ;Field     ;
                ToolTipML=ENU=Specifies how many units of the assembly component remain to be consumed during assembly.;
                SourceExpr="Remaining Quantity" }

    { 47  ;2   ;Field     ;
                ToolTipML=ENU=Specifies how many units of the assembly component have been moved or picked for the assembly order line.;
                SourceExpr="Qty. Picked";
                Visible=FALSE }

    { 48  ;2   ;Field     ;
                ToolTipML=ENU=Specifies how many units of the assembly component are currently on warehouse pick lines.;
                SourceExpr="Pick Qty.";
                Visible=FALSE }

    { 24  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the date when the assembly component must be available for consumption by the assembly order.;
                SourceExpr="Due Date";
                Visible=False;
                OnValidate=BEGIN
                             ReserveItem;
                           END;
                            }

    { 30  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the lead-time offset that is defined for the assembly component on the assembly BOM.;
                SourceExpr="Lead-Time Offset";
                Visible=FALSE }

    { 9   ;2   ;Field     ;
                ToolTipML=ENU=Specifies the code of the shortcut dimension 1 value that the assembly order line is linked to.;
                SourceExpr="Shortcut Dimension 1 Code";
                Visible=false }

    { 10  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the code of the shortcut dimension 2 value that the assembly order line is linked to.;
                SourceExpr="Shortcut Dimension 2 Code";
                Visible=False }

    { 11  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the code of the bin where assembly components must be placed prior to assembly and from where they are posted as consumed.;
                SourceExpr="Bin Code";
                Visible=False }

    { 17  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the inventory posting group to which the item on this assembly order line is posted.;
                SourceExpr="Inventory Posting Group";
                Visible=False }

    { 18  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the unit cost of the assembly component.;
                SourceExpr="Unit Cost" }

    { 19  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the cost of the assembly order line.;
                SourceExpr="Cost Amount" }

    { 20  ;2   ;Field     ;
                ToolTipML=ENU=Specifies how many units of the assembly component have been reserved for this assembly order line.;
                SourceExpr="Reserved Quantity" }

    { 57  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the reserve option for the assembly order line.;
                SourceExpr=Reserve;
                Visible=FALSE;
                OnValidate=BEGIN
                             ReserveItem;
                           END;
                            }

    { 43  ;2   ;Field     ;
                Name=ReservationStatusField;
                CaptionML=[ENU=Reservation Status;
                           PTG=Estado Reserva];
                ToolTipML=ENU=Specifies if the value in the Quantity field on the assembly order line is fully or partially reserved.;
                OptionCaptionML=[ENU=" ,Partial,Full";
                                 PTG=" ,Parcial,Total"];
                SourceExpr=ReservationStatusField;
                Visible=FALSE;
                Editable=FALSE }

    { 46  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the quantity per unit of measure of the component item on the assembly order line.;
                SourceExpr="Qty. per Unit of Measure" }

    { 22  ;2   ;Field     ;
                ToolTipML=ENU=Specifies how the cost of the resource on the assembly order line is allocated to the assembly item.;
                SourceExpr="Resource Usage Type" }

    { 7   ;2   ;Field     ;
                ToolTipML=ENU=Specifies the number of a particular item ledger entry that the assembly order line is applied to.;
                SourceExpr="Appl.-to Item Entry" }

    { 31  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the number of the item ledger entry that the assembly order line is applied from.;
                SourceExpr="Appl.-from Item Entry" }

  }
  CODE
  {
    VAR
      ItemAvailFormsMgt@1001 : Codeunit 353;
      ReservationStatusField@1000 : ' ,Partial,Full';

    LOCAL PROCEDURE ReserveItem@1();
    BEGIN
      IF Type <> Type::Item THEN
        EXIT;

      IF ("Remaining Quantity (Base)" <> xRec."Remaining Quantity (Base)") OR
         ("No." <> xRec."No.") OR
         ("Location Code" <> xRec."Location Code") OR
         ("Variant Code" <> xRec."Variant Code") OR
         ("Due Date" <> xRec."Due Date") OR
         ((Reserve <> xRec.Reserve) AND ("Remaining Quantity (Base)" <> 0))
      THEN
        IF Reserve = Reserve::Always THEN BEGIN
          CurrPage.SAVERECORD;
          AutoReserve;
          CurrPage.UPDATE(FALSE);
        END;

      ReservationStatusField := ReservationStatus;
    END;

    BEGIN
    END.
  }
}

