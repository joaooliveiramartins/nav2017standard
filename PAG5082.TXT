OBJECT Page 5082 Postponed Interactions
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0;
  }
  PROPERTIES
  {
    Editable=No;
    CaptionML=[ENU=Postponed Interactions;
               PTG=Intera��es Adiadas];
    SourceTable=Table5065;
    SourceTableView=WHERE(Postponed=CONST(Yes));
    PageType=List;
    OnOpenPage=BEGIN
                 SetCaption;
               END;

    OnAfterGetCurrRecord=BEGIN
                           CALCFIELDS("Contact Name","Contact Company Name");
                         END;

    ActionList=ACTIONS
    {
      { 1900000004;0 ;ActionContainer;
                      ActionContainerType=ActionItems }
      { 5       ;1   ;ActionGroup;
                      Name=Functions;
                      CaptionML=[ENU=F&unctions;
                                 PTG=F&un��es];
                      Image=Action }
      { 7       ;2   ;Action    ;
                      Name=Filter;
                      CaptionML=[ENU=Filter;
                                 PTG=Filtrar];
                      ToolTipML=ENU=Apply a filter to view specific interaction log entries.;
                      ApplicationArea=#RelationshipMgmt;
                      Promoted=Yes;
                      Image=Filter;
                      OnAction=VAR
                                 FilterPageBuilder@1000 : FilterPageBuilder;
                               BEGIN
                                 FilterPageBuilder.ADDTABLE(TABLENAME,DATABASE::"Interaction Log Entry");
                                 FilterPageBuilder.SETVIEW(TABLENAME,GETVIEW);

                                 IF GETFILTER("Contact No.") = '' THEN
                                   FilterPageBuilder.ADDFIELDNO(TABLENAME,FIELDNO("Contact No."));
                                 IF GETFILTER("Contact Company No.") = '' THEN
                                   FilterPageBuilder.ADDFIELDNO(TABLENAME,FIELDNO("Contact Company No."));

                                 IF FilterPageBuilder.RUNMODAL THEN
                                   SETVIEW(FilterPageBuilder.GETVIEW(TABLENAME));
                               END;
                                }
      { 9       ;2   ;Action    ;
                      Name=ClearFilter;
                      CaptionML=[ENU=Clear Filter;
                                 PTG=Limpar Filtro];
                      ToolTipML=ENU=Clear the applied filter on specific interaction log entries.;
                      ApplicationArea=#RelationshipMgmt;
                      Promoted=Yes;
                      Image=ClearFilter;
                      OnAction=BEGIN
                                 RESET;
                                 FILTERGROUP(2);
                                 SETRANGE(Postponed,TRUE);
                                 FILTERGROUP(0);
                               END;
                                }
      { 54      ;2   ;Action    ;
                      CaptionML=[ENU=&Delete;
                                 PTG=&Eliminar];
                      ToolTipML=ENU=Delete the selected postponed interactions.;
                      ApplicationArea=#RelationshipMgmt;
                      Image=Delete;
                      OnAction=BEGIN
                                 IF CONFIRM(Text001) THEN BEGIN
                                   CurrPage.SETSELECTIONFILTER(InteractionLogEntry);
                                   IF NOT InteractionLogEntry.ISEMPTY THEN
                                     InteractionLogEntry.DELETEALL(TRUE)
                                   ELSE
                                     DELETE(TRUE);
                                 END;
                               END;
                                }
      { 2       ;1   ;Action    ;
                      Name=Resume;
                      CaptionML=[ENU=&Resume;
                                 PTG=&Retomar];
                      ToolTipML=ENU=Resume a postponed interaction.;
                      ApplicationArea=#RelationshipMgmt;
                      Promoted=Yes;
                      Image=Start;
                      PromotedCategory=Process;
                      Scope=Repeater;
                      OnAction=BEGIN
                                 IF ISEMPTY THEN
                                   EXIT;

                                 ResumeInteraction
                               END;
                                }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                Editable=FALSE;
                GroupType=Repeater }

    { 6   ;2   ;Field     ;
                ToolTipML=ENU=Specifies whether the interaction records an failed attempt to reach the contact. This field is not editable.;
                ApplicationArea=#RelationshipMgmt;
                SourceExpr="Attempt Failed" }

    { 34  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the type of document if there is one that the interaction log entry records. You cannot change the contents of this field.;
                SourceExpr="Document Type";
                Visible=FALSE }

    { 64  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the number of the document (if any) that the interaction log entry records.;
                SourceExpr="Document No.";
                Visible=FALSE }

    { 62  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the status of the delivery of the attachment. There are three options:;
                SourceExpr="Delivery Status";
                Visible=FALSE }

    { 8   ;2   ;Field     ;
                ToolTipML=ENU=Specifies the date that you have entered in the Date field in the Create Interaction wizard or the Segment window when you created the interaction. The field is not editable.;
                ApplicationArea=#All;
                SourceExpr=Date }

    { 10  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the time when the interaction was created. This field is not editable.;
                SourceExpr="Time of Interaction";
                Visible=FALSE }

    { 12  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the type of correspondence of the attachment in the interaction template. This field is not editable.;
                SourceExpr="Correspondence Type";
                Visible=FALSE }

    { 14  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the code of the interaction group used to create this interaction. This field is not editable.;
                SourceExpr="Interaction Group Code";
                Visible=FALSE }

    { 16  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the code for the interaction template used to create the interaction. This field is not editable.;
                ApplicationArea=#RelationshipMgmt;
                SourceExpr="Interaction Template Code" }

    { 26  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the description of the interaction.;
                ApplicationArea=#All;
                SourceExpr=Description }

    { 3   ;2   ;Field     ;
                CaptionML=[ENU=Attachment;
                           PTG=Anexo];
                ToolTipML=ENU=Specifies if the attachment that is linked to the segment line is inherited or unique.;
                ApplicationArea=#RelationshipMgmt;
                BlankZero=Yes;
                SourceExpr="Attachment No." <> 0;
                OnAssistEdit=BEGIN
                               IF "Attachment No." <> 0 THEN
                                 OpenAttachment;
                             END;
                              }

    { 18  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the direction of information flow recorded by the interaction. There are two options: Outbound (the information was received by your contact) and Inbound (the information was received by your company).;
                SourceExpr="Information Flow";
                Visible=FALSE }

    { 20  ;2   ;Field     ;
                ToolTipML=ENU=Specifies who initiated the interaction. There are two options: Us (the interaction was initiated by your company) and Them (the interaction was initiated by your contact).;
                SourceExpr="Initiated By";
                Visible=FALSE }

    { 22  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the number of the contact involved in this interaction. This field is not editable.;
                ApplicationArea=#RelationshipMgmt;
                SourceExpr="Contact No." }

    { 24  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the number of the contact company.;
                SourceExpr="Contact Company No.";
                Visible=FALSE }

    { 28  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the evaluation of the interaction. There are five options: Very Positive, Positive, Neutral, Negative, and Very Negative.;
                ApplicationArea=#RelationshipMgmt;
                SourceExpr=Evaluation }

    { 30  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the cost of the interaction.;
                ApplicationArea=#RelationshipMgmt;
                SourceExpr="Cost (LCY)" }

    { 32  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the duration of the interaction.;
                ApplicationArea=#RelationshipMgmt;
                SourceExpr="Duration (Min.)" }

    { 36  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the code for the salesperson who carried out the interaction. This field is not editable.;
                ApplicationArea=#Suite,#RelationshipMgmt;
                SourceExpr="Salesperson Code" }

    { 38  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the ID of the user who logged this entry. This field is not editable.;
                SourceExpr="User ID";
                Visible=FALSE }

    { 40  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the number of the segment. This field is valid only for interactions created for segments, and is not editable.;
                SourceExpr="Segment No.";
                Visible=FALSE }

    { 42  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the number of the campaign (if any) to which the interaction is linked. This field is not editable.;
                SourceExpr="Campaign No." }

    { 44  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the number of the campaign entry to which the interaction log entry is linked.;
                SourceExpr="Campaign Entry No.";
                Visible=FALSE }

    { 46  ;2   ;Field     ;
                ToolTipML=ENU=Specifies whether the interaction records a response to a campaign.;
                SourceExpr="Campaign Response";
                Visible=FALSE }

    { 48  ;2   ;Field     ;
                ToolTipML=ENU=Specifies whether the interaction is applied to contacts that are part of the campaign target. This field is not editable.;
                SourceExpr="Campaign Target";
                Visible=FALSE }

    { 81  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the number of the opportunity to which the interaction is linked.;
                ApplicationArea=#RelationshipMgmt;
                SourceExpr="Opportunity No." }

    { 50  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the number of the to-do if the interaction has been created to complete a to-do. This field is not editable.;
                SourceExpr="To-do No.";
                Visible=FALSE }

    { 68  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the language code for the interaction for the interaction log. The code is copied from the language code of the interaction template, if one is specified.;
                SourceExpr="Interaction Language Code";
                Visible=FALSE }

    { 70  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the subject text that will be used for this interaction.;
                SourceExpr=Subject;
                Visible=FALSE }

    { 66  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the telephone number that you used when calling the contact.;
                SourceExpr="Contact Via";
                Visible=FALSE }

    { 52  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the unique number identifying the interaction log entry. The field is not editable.;
                ApplicationArea=#All;
                SourceExpr="Entry No." }

    { 77  ;2   ;Field     ;
                ToolTipML=ENU=Specifies that a comment exists for this interaction log entry.;
                SourceExpr=Comment }

    { 78  ;1   ;Group      }

    { 72  ;2   ;Field     ;
                DrillDown=No;
                CaptionML=[ENU=Contact Name;
                           PTG=Nome Contacto];
                ToolTipML=[ENU=Specifies the name of the contact for which an interaction has been logged.;
                           PTG=""];
                ApplicationArea=#RelationshipMgmt;
                SourceExpr="Contact Name" }

    { 79  ;2   ;Field     ;
                DrillDown=No;
                ToolTipML=[ENU=Specifies the name of the contact company for which an interaction has been logged.;
                           PTG=""];
                ApplicationArea=#RelationshipMgmt;
                SourceExpr="Contact Company Name" }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                Visible=TRUE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {
    VAR
      InteractionLogEntry@1000 : Record 5065;
      Text001@1001 : TextConst 'ENU=Delete selected lines?;PTG=Eliminar as linhas selecionadas?';

    LOCAL PROCEDURE SetCaption@2();
    VAR
      Contact@1000 : Record 5050;
      SalespersonPurchaser@1003 : Record 13;
      ToDo@1002 : Record 5080;
      Opportunity@1001 : Record 5092;
    BEGIN
      IF Contact.GET("Contact Company No.") THEN
        CurrPage.CAPTION(CurrPage.CAPTION + ' - ' + Contact."Company No." + ' . ' + Contact."Company Name");
      IF Contact.GET("Contact No.") THEN BEGIN
        CurrPage.CAPTION(CurrPage.CAPTION + ' - ' + Contact."No." + ' . ' + Contact.Name);
        EXIT;
      END;
      IF "Contact Company No." <> '' THEN
        EXIT;
      IF SalespersonPurchaser.GET("Salesperson Code") THEN BEGIN
        CurrPage.CAPTION(CurrPage.CAPTION + ' - ' + "Salesperson Code" + ' . ' + SalespersonPurchaser.Name);
        EXIT;
      END;
      IF "Interaction Template Code" <> '' THEN BEGIN
        CurrPage.CAPTION(CurrPage.CAPTION + ' - ' + "Interaction Template Code");
        EXIT;
      END;
      IF ToDo.GET("To-do No.") THEN BEGIN
        CurrPage.CAPTION(CurrPage.CAPTION + ' - ' + ToDo."No." + ' . ' + ToDo.Description);
        EXIT;
      END;
      IF Opportunity.GET("Opportunity No.") THEN
        CurrPage.CAPTION(CurrPage.CAPTION + ' - ' + Opportunity."No." + ' . ' + Opportunity.Description);
    END;

    BEGIN
    END.
  }
}

