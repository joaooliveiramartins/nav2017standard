OBJECT Table 462 Payment Term Translation
{
  OBJECT-PROPERTIES
  {
    Date=05/08/11;
    Time=12:00:00;
    Version List=NAVW16.00;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Payment Term Translation;
               PTG=Tradu��o Prazo Pagamento];
  }
  FIELDS
  {
    { 1   ;   ;Payment Term        ;Code10        ;TableRelation="Payment Terms";
                                                   CaptionML=[ENU=Payment Term;
                                                              PTG=Prazo Pagamento] }
    { 2   ;   ;Language Code       ;Code10        ;TableRelation=Language;
                                                   CaptionML=[ENU=Language Code;
                                                              PTG=C�d. Idioma] }
    { 3   ;   ;Description         ;Text50        ;CaptionML=[ENU=Description;
                                                              PTG=Descri��o] }
  }
  KEYS
  {
    {    ;Payment Term,Language Code              ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    END.
  }
}

