OBJECT Codeunit 31022896 AT Single Instance Aux
{
  OBJECT-PROPERTIES
  {
    Date=01/02/15;
    Time=13:00:00;
    Version List=NAVPTSS81.00;
  }
  PROPERTIES
  {
    SingleInstance=Yes;
    OnRun=BEGIN
          END;

  }
  CODE
  {
    VAR
      booSkipMsg@1100409000 : Boolean;

    PROCEDURE SkipMessages@1100409001(pSkip@1100409000 : Boolean) : Boolean;
    BEGIN
      booSkipMsg := pSkip
    END;

    PROCEDURE GetSkipMsg@1100409000() : Boolean;
    BEGIN
      EXIT(booSkipMsg);
    END;

    BEGIN
    END.
  }
}

