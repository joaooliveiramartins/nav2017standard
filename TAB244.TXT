OBJECT Table 244 Req. Wksh. Template
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0;
  }
  PROPERTIES
  {
    OnInsert=BEGIN
               VALIDATE("Page ID");
             END;

    OnDelete=BEGIN
               ReqLine.SETRANGE("Worksheet Template Name",Name);
               ReqLine.DELETEALL(TRUE);
               ReqWkshName.SETRANGE("Worksheet Template Name",Name);
               ReqWkshName.DELETEALL;
             END;

    CaptionML=[ENU=Req. Wksh. Template;
               PTG=Livro Folha Requisi��o];
    LookupPageID=Page292;
  }
  FIELDS
  {
    { 1   ;   ;Name                ;Code10        ;CaptionML=[ENU=Name;
                                                              PTG=Nome];
                                                   NotBlank=Yes }
    { 2   ;   ;Description         ;Text80        ;CaptionML=[ENU=Description;
                                                              PTG=Descri��o] }
    { 6   ;   ;Page ID             ;Integer       ;TableRelation=AllObjWithCaption."Object ID" WHERE (Object Type=CONST(Page));
                                                   OnValidate=BEGIN
                                                                IF "Page ID" = 0 THEN
                                                                  VALIDATE(Recurring);
                                                              END;

                                                   CaptionML=[ENU=Page ID;
                                                              PTG=ID P�gina] }
    { 12  ;   ;Recurring           ;Boolean       ;OnValidate=BEGIN
                                                                IF Recurring THEN
                                                                  "Page ID" := PAGE::"Recurring Req. Worksheet"
                                                                ELSE
                                                                  CASE Type OF
                                                                    Type::"Req.":
                                                                      "Page ID" := PAGE::"Req. Worksheet";
                                                                    Type::"For. Labor":
                                                                      "Page ID" := PAGE::"Subcontracting Worksheet";
                                                                    Type::Planning:
                                                                      "Page ID" := PAGE::"Planning Worksheet";
                                                                  END;
                                                              END;

                                                   CaptionML=[ENU=Recurring;
                                                              PTG=Peri�dico] }
    { 16  ;   ;Page Caption        ;Text250       ;FieldClass=FlowField;
                                                   CalcFormula=Lookup(AllObjWithCaption."Object Caption" WHERE (Object Type=CONST(Page),
                                                                                                                Object ID=FIELD(Page ID)));
                                                   CaptionML=[ENU=Page Caption;
                                                              PTG=Legenda P�gina];
                                                   Editable=No }
    { 99000750;;Type               ;Option        ;CaptionML=[ENU=Type;
                                                              PTG=Tipo];
                                                   OptionCaptionML=[ENU=Req.,For. Labor,Planning;
                                                                    PTG=Nec.,M�o-de-Obra Ext.,Planeamento];
                                                   OptionString=Req.,For. Labor,Planning }
  }
  KEYS
  {
    {    ;Name                                    ;Clustered=Yes }
  }
  FIELDGROUPS
  {
    { 1   ;DropDown            ;Name,Description,Recurring,Type          }
  }
  CODE
  {
    VAR
      ReqWkshName@1000 : Record 245;
      ReqLine@1001 : Record 246;

    BEGIN
    END.
  }
}

