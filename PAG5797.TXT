OBJECT Page 5797 Registered Whse. Activity List
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0;
  }
  PROPERTIES
  {
    Editable=No;
    CaptionML=[ENU=Registered Whse. Activity List;
               PTG=Lista Atividades Armz. Reg.];
    SourceTable=Table5772;
    PageType=List;
    OnFindRecord=BEGIN
                   IF FIND(Which) THEN BEGIN
                     RegisteredWhseActivHeader := Rec;
                     WHILE TRUE DO BEGIN
                       IF WMSManagement.LocationIsAllowed("Location Code") THEN
                         EXIT(TRUE);
                       IF NEXT(1) = 0 THEN BEGIN
                         Rec := RegisteredWhseActivHeader;
                         IF FIND(Which) THEN
                           WHILE TRUE DO BEGIN
                             IF WMSManagement.LocationIsAllowed("Location Code") THEN
                               EXIT(TRUE);
                             IF NEXT(-1) = 0 THEN
                               EXIT(FALSE);
                           END;
                       END;
                     END;
                   END;
                   EXIT(FALSE);
                 END;

    OnNextRecord=VAR
                   RealSteps@1001 : Integer;
                   NextSteps@1000 : Integer;
                 BEGIN
                   IF Steps = 0 THEN
                     EXIT;

                   RegisteredWhseActivHeader := Rec;
                   REPEAT
                     NextSteps := NEXT(Steps / ABS(Steps));
                     IF WMSManagement.LocationIsAllowed("Location Code") THEN BEGIN
                       RealSteps := RealSteps + NextSteps;
                       RegisteredWhseActivHeader := Rec;
                     END;
                   UNTIL (NextSteps = 0) OR (RealSteps = Steps);
                   Rec := RegisteredWhseActivHeader;
                   FIND;
                   EXIT(RealSteps);
                 END;

    OnAfterGetCurrRecord=BEGIN
                           CurrPage.CAPTION := FormCaption;
                         END;

    ActionList=ACTIONS
    {
      { 1900000003;0 ;ActionContainer;
                      ActionContainerType=RelatedInformation }
      { 17      ;1   ;ActionGroup;
                      CaptionML=[ENU=&Line;
                                 PTG=&Linha];
                      Image=Line }
      { 19      ;2   ;Action    ;
                      ShortCutKey=Shift+F7;
                      CaptionML=[ENU=Card;
                                 PTG=Ficha];
                      Image=EditLines;
                      OnAction=BEGIN
                                 CASE Type OF
                                   Type::"Put-away":
                                     PAGE.RUN(PAGE::"Registered Put-away",Rec);
                                   Type::Pick:
                                     PAGE.RUN(PAGE::"Registered Pick",Rec);
                                   Type::Movement:
                                     PAGE.RUN(PAGE::"Registered Movement",Rec);
                                 END;
                               END;
                                }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 18  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the type of activity that the warehouse performed on the lines attached to the header, such as put-away, pick or movement.;
                           PTG=""];
                SourceExpr=Type;
                Visible=FALSE }

    { 8   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the registered warehouse activity number.;
                           PTG=""];
                SourceExpr="No." }

    { 2   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the warehouse activity number from which the activity was registered.;
                           PTG=""];
                SourceExpr="Whse. Activity No." }

    { 4   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the code of the location in which the registered warehouse activity occurred.;
                           PTG=""];
                SourceExpr="Location Code" }

    { 15  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the ID of the employee who is responsible for the document and assigned to perform the warehouse activity.;
                           PTG=""];
                SourceExpr="Assigned User ID" }

    { 6   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the method by which the lines were sorted on the warehouse header, such as by item, or bin code.;
                           PTG=""];
                SourceExpr="Sorting Method" }

    { 10  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the number series code used if a number was assigned to the registered warehouse activity header.;
                           PTG=""];
                SourceExpr="No. Series" }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {
    VAR
      RegisteredWhseActivHeader@1001 : Record 5772;
      WMSManagement@1000 : Codeunit 7302;
      Text000@1003 : TextConst 'ENU=Registered Whse. Put-away List;PTG=Lista Arrumar Armz. Reg.';
      Text001@1002 : TextConst 'ENU=Registered Whse. Pick List;PTG=Lista Recolha Armz. Reg.';
      Text002@1004 : TextConst 'ENU=Registered Whse. Movement List;PTG=Lista Movimentos Armz. Reg.';
      Text003@1005 : TextConst 'ENU=Registered Whse. Activity List;PTG=Lista Atividades Armz. Reg.';

    LOCAL PROCEDURE FormCaption@1() : Text[250];
    BEGIN
      CASE Type OF
        Type::"Put-away":
          EXIT(Text000);
        Type::Pick:
          EXIT(Text001);
        Type::Movement:
          EXIT(Text002);
        ELSE
          EXIT(Text003);
      END;
    END;

    BEGIN
    END.
  }
}

