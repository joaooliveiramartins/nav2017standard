OBJECT Page 451 Issued Fin. Charge Memo Lines
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0,NAVPTSS81.00;
  }
  PROPERTIES
  {
    Editable=No;
    CaptionML=[ENU=Lines;
               PTG=Linhas];
    LinksAllowed=No;
    SourceTable=Table305;
    PageType=ListPart;
    AutoSplitKey=Yes;
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 28  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the line type.;
                SourceExpr=Type }

    { 30  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the number of the general ledger account this finance charge memo line is for.;
                SourceExpr="No." }

    { 2   ;2   ;Field     ;
                ToolTipML=ENU=Specifies the posting date of the customer ledger entry that this finance charge memo line is for.;
                SourceExpr="Posting Date";
                Visible=FALSE }

    { 20  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the document date of the customer ledger entry this finance charge memo line is for.;
                SourceExpr="Document Date";
                Visible=FALSE }

    { 6   ;2   ;Field     ;
                ToolTipML=ENU=Specifies the document type of the customer ledger entry this finance charge memo line is for.;
                SourceExpr="Document Type" }

    { 8   ;2   ;Field     ;
                ToolTipML=ENU=Specifies the document number of the customer ledger entry this finance charge memo line is for.;
                SourceExpr="Document No." }

    { 22  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the due date of the customer ledger entry this finance charge memo line is for.;
                SourceExpr="Due Date" }

    { 10  ;2   ;Field     ;
                ToolTipML=ENU=Specifies an entry description, based on the contents of the Type field.;
                SourceExpr=Description }

    { 14  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the original amount of the customer ledger entry that this finance charge memo line is for.;
                SourceExpr="Original Amount";
                Visible=FALSE }

    { 16  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the remaining amount of the customer ledger entry this finance charge memo line is for.;
                SourceExpr="Remaining Amount" }

    { 18  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the amount in the currency of the finance charge memo.;
                SourceExpr=Amount }

    { 1110000;2;Field     ;
                SourceExpr="DRF Code";
                Visible=FALSE }

  }
  CODE
  {

    BEGIN
    END.
  }
}

