OBJECT Table 2000000078 Chart
{
  OBJECT-PROPERTIES
  {
    Date=13/04/17;
    Time=14:36:26;
    Version List=NAVW17.10;
  }
  PROPERTIES
  {
    DataPerCompany=No;
    CaptionML=[ENU=Chart;
               PTG=Gr fico];
  }
  FIELDS
  {
    { 3   ;   ;ID                  ;Code20        ;CaptionML=[ENU=ID;
                                                              PTG=ID] }
    { 6   ;   ;Name                ;Text30        ;CaptionML=[ENU=Name;
                                                              PTG=Nome] }
    { 9   ;   ;BLOB                ;BLOB          ;CaptionML=[ENU=BLOB;
                                                              PTG=BLOB] }
  }
  KEYS
  {
    {    ;ID                                      ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    END.
  }
}

