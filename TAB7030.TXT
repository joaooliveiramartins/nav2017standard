OBJECT Table 7030 Campaign Target Group
{
  OBJECT-PROPERTIES
  {
    Date=05/08/11;
    Time=12:00:00;
    Version List=NAVW16.00;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Campaign Target Group;
               PTG=Grupo Alvo Campanha];
  }
  FIELDS
  {
    { 1   ;   ;Type                ;Option        ;CaptionML=[ENU=Type;
                                                              PTG=Tipo];
                                                   OptionCaptionML=[ENU=Customer,Contact;
                                                                    PTG=Cliente,Contacto];
                                                   OptionString=Customer,Contact }
    { 2   ;   ;No.                 ;Code20        ;TableRelation=IF (Type=CONST(Customer)) Customer
                                                                 ELSE IF (Type=CONST(Contact)) Contact WHERE (Type=FILTER(Company));
                                                   CaptionML=[ENU=No.;
                                                              PTG=N�] }
    { 3   ;   ;Campaign No.        ;Code20        ;TableRelation=Campaign;
                                                   CaptionML=[ENU=Campaign No.;
                                                              PTG=N� Campanha] }
  }
  KEYS
  {
    {    ;Type,No.,Campaign No.                   ;Clustered=Yes }
    {    ;Campaign No.                             }
    {    ;No.                                      }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    END.
  }
}

