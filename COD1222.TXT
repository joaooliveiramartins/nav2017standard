OBJECT Codeunit 1222 SEPA CT-Prepare Source
{
  OBJECT-PROPERTIES
  {
    Date=13/02/15;
    Time=13:00:00;
    Version List=NAVW18.00,NAVPTSS82.00;
  }
  PROPERTIES
  {
    TableNo=81;
    OnRun=VAR
            GenJnlLine@1000 : Record 81;
          BEGIN
            GenJnlLine.COPYFILTERS(Rec);
            CopyJnlLines(GenJnlLine,Rec);
          END;

  }
  CODE
  {

    LOCAL PROCEDURE CopyJnlLines@4(VAR FromGenJnlLine@1000 : Record 81;VAR TempGenJnlLine@1001 : TEMPORARY Record 81);
    VAR
      GenJnlBatch@1003 : Record 232;
    BEGIN
      IF FromGenJnlLine.FINDSET THEN BEGIN
        GenJnlBatch.GET(FromGenJnlLine."Journal Template Name",FromGenJnlLine."Journal Batch Name");

        REPEAT
          TempGenJnlLine := FromGenJnlLine;
          TempGenJnlLine.INSERT;
        UNTIL FromGenJnlLine.NEXT = 0
      END ELSE
        CreateTempJnlLines(FromGenJnlLine,TempGenJnlLine);
    END;

    LOCAL PROCEDURE CreateTempJnlLines@5(VAR FromGenJnlLine@1000 : Record 81;VAR TempGenJnlLine@1001 : TEMPORARY Record 81);
    VAR
      PaymentOrder@1000000002 : Record 31022952;
      CarteraDoc@1000000001 : Record 31022935;
      PaymentOrderNo@1000000000 : Code[20];
    BEGIN
      // To fill TempGenJnlLine from the source identified by filters set on FromGenJnlLine
      //soft,o TempGenJnlLine := FromGenJnlLine;
      //soft,sn
      TempGenJnlLine.RESET;
      PaymentOrderNo := FromGenJnlLine.GETFILTER("Document No.");
      PaymentOrder.GET(PaymentOrderNo);
      CarteraDoc.RESET;
      CarteraDoc.SETCURRENTKEY(Type,"Collection Agent","Bill Gr./Pmt. Order No.");
      CarteraDoc.SETRANGE(Type,CarteraDoc.Type::Payable);
      CarteraDoc.SETRANGE("Collection Agent",CarteraDoc."Collection Agent"::Bank);
      CarteraDoc.SETRANGE("Bill Gr./Pmt. Order No.",PaymentOrder."No.");
      IF CarteraDoc.FINDSET THEN REPEAT
        WITH TempGenJnlLine DO BEGIN
          INIT;
          "Journal Template Name" := '';
          "Journal Batch Name" := '';
          "Line No." := CarteraDoc."Entry No.";
          "Posting Date" := CarteraDoc."Due Date";
          "Due Date" := CarteraDoc."Due Date";
          "Document Type" := "Document Type"::Payment;
          "Account Type" := "Account Type"::Vendor;
          "Account No." := CarteraDoc."Account No.";
          "Recipient Bank Account" := CarteraDoc."Cust./Vendor Bank Acc. Code";
          "Bal. Account Type" := TempGenJnlLine."Bal. Account Type"::"Bank Account";
          "Bal. Account No." := PaymentOrder."Bank Account No.";
          "Document No." := CarteraDoc."Document No.";
          "Currency Code" := CarteraDoc."Currency Code";
          Amount := CarteraDoc."Remaining Amount";
          INSERT;
        END;
      UNTIL CarteraDoc.NEXT = 0;
      //soft,en
    END;

    BEGIN
    END.
  }
}

