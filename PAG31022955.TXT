OBJECT Page 31022955 Receivable Closed Cartera Docs
{
  OBJECT-PROPERTIES
  {
    Date=21/12/16;
    Time=13:00:00;
    Version List=NAVPTSS10.00;
  }
  PROPERTIES
  {
    Editable=No;
    CaptionML=[ENU=Receivable Closed Cartera Documents;
               PTG=Documentos Carteira Fechados a Cobrar];
    SourceTable=Table31022937;
    SourceTableView=SORTING(Type,Collection Agent,Bill Gr./Pmt. Order No.,Currency Code,Status,Redrawn)
                    WHERE(Type=CONST(Receivable));
    PageType=List;
    ActionList=ACTIONS
    {
      { 1900000003;0 ;ActionContainer;
                      ActionContainerType=RelatedInformation }
      { 25      ;1   ;ActionGroup;
                      CaptionML=[ENU=&Documents;
                                 PTG=&Documentos] }
      { 39      ;2   ;Action    ;
                      CaptionML=[ENU=Analysis;
                                 PTG=An�lise];
                      ApplicationArea=#Basic,#Suite;
                      Image=Report;
                      OnAction=BEGIN
                                 ClosedDoc.COPY(Rec);
                                 PAGE.RUN(PAGE::"Closed Documents Analysis",ClosedDoc);
                               END;
                                }
      { 24      ;2   ;Action    ;
                      Name=Redraw;
                      Ellipsis=Yes;
                      CaptionML=[ENU=Redraw;
                                 PTG=Reforma];
                      ApplicationArea=#Basic,#Suite;
                      Image=RefreshVoucher;
                      OnAction=BEGIN
                                 CurrPage.SETSELECTIONFILTER(ClosedDoc);
                                 DocMisc.RedrawDoc(ClosedDoc);
                               END;
                                }
      { 1110000 ;2   ;Separator  }
      { 1110001 ;2   ;Action    ;
                      ShortCutKey=Shift+Ctrl+D;
                      CaptionML=[ENU=Dime&nsions;
                                 PTG=Dime&ns�es];
                      ApplicationArea=#Basic,#Suite;
                      Image=Dimensions;
                      OnAction=VAR
                                 Status@1110001 : 'Open,Posted,Closed';
                               BEGIN
                                 ShowDimensions;
                               END;
                                }
      { 1900000004;0 ;ActionContainer;
                      ActionContainerType=ActionItems }
      { 40      ;1   ;Action    ;
                      CaptionML=[ENU=&Navigate;
                                 PTG=&Navegar];
                      ApplicationArea=#Basic,#Suite;
                      Promoted=Yes;
                      Image=Navigate;
                      PromotedCategory=Process;
                      OnAction=BEGIN
                                 CarteraManagement.NavigateClosedDoc(Rec);
                               END;
                                }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 30  ;2   ;Field     ;
                SourceExpr="Document Type" }

    { 2   ;2   ;Field     ;
                SourceExpr="Posting Date";
                Visible=FALSE }

    { 4   ;2   ;Field     ;
                SourceExpr="Due Date" }

    { 26  ;2   ;Field     ;
                SourceExpr=Status }

    { 34  ;2   ;Field     ;
                SourceExpr="Honored/Rejtd. at Date";
                Visible=FALSE }

    { 6   ;2   ;Field     ;
                SourceExpr="Payment Method Code" }

    { 8   ;2   ;Field     ;
                SourceExpr="Document No." }

    { 10  ;2   ;Field     ;
                SourceExpr="No." }

    { 12  ;2   ;Field     ;
                SourceExpr=Description }

    { 18  ;2   ;Field     ;
                SourceExpr="Currency Code";
                Visible=FALSE }

    { 33  ;2   ;Field     ;
                SourceExpr="Original Amount" }

    { 37  ;2   ;Field     ;
                SourceExpr="Original Amount (LCY)";
                Visible=FALSE }

    { 16  ;2   ;Field     ;
                SourceExpr="Remaining Amount" }

    { 14  ;2   ;Field     ;
                SourceExpr="Remaining Amt. (LCY)";
                Visible=FALSE }

    { 31  ;2   ;Field     ;
                SourceExpr=Redrawn }

    { 20  ;2   ;Field     ;
                SourceExpr=Place;
                Visible=FALSE }

    { 22  ;2   ;Field     ;
                SourceExpr="Account No.";
                Visible=FALSE }

    { 28  ;2   ;Field     ;
                SourceExpr="Entry No." }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1907056007;1;Part   ;
                SubPageLink=Type=CONST(Receivable),
                            Entry No.=FIELD(Entry No.);
                PagePartID=Page31023023;
                Visible=TRUE;
                PartType=Page }

    { 1907056107;1;Part   ;
                SubPageLink=Type=CONST(Receivable),
                            Entry No.=FIELD(Entry No.);
                PagePartID=Page31023024;
                Visible=TRUE;
                PartType=Page }

  }
  CODE
  {
    VAR
      ClosedDoc@1110000 : Record 31022937;
      CarteraManagement@1110002 : Codeunit 31022901;
      DocMisc@1110003 : Codeunit 31022907;
      DimMgmt@31022891 : Codeunit 408;
      CurrTotalAmount@31022890 : Decimal;

    BEGIN
    END.
  }
}

