OBJECT Table 5772 Registered Whse. Activity Hdr.
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0,NAVPTSS92.00;
  }
  PROPERTIES
  {
    Permissions=TableData 5773=rimd;
    OnDelete=VAR
               RgstrdWhseActivLine@1001 : Record 5773;
               WhseCommentLine@1000 : Record 5770;
             BEGIN
               RgstrdWhseActivLine.SETRANGE("Activity Type",Type);
               RgstrdWhseActivLine.SETRANGE("No.","No.");
               RgstrdWhseActivLine.DELETEALL;

               WhseCommentLine.SETRANGE("Table Name",WhseCommentLine."Table Name"::"Rgstrd. Whse. Activity Header");
               WhseCommentLine.SETRANGE(Type,Type);
               WhseCommentLine.SETRANGE("No.","No.");
               WhseCommentLine.DELETEALL;
             END;

    CaptionML=[ENU=Registered Whse. Activity Hdr.;
               PTG=Cabe�alho Atividade Armaz�m Reg.];
    LookupPageID=Page5797;
  }
  FIELDS
  {
    { 1   ;   ;Type                ;Option        ;CaptionML=[ENU=Type;
                                                              PTG=Tipo];
                                                   OptionCaptionML=[ENU=" ,Put-away,Pick,Movement";
                                                                    PTG=" ,Arrumar,Recolha,Movimento"];
                                                   OptionString=[ ,Put-away,Pick,Movement] }
    { 2   ;   ;No.                 ;Code20        ;CaptionML=[ENU=No.;
                                                              PTG=N�] }
    { 3   ;   ;Location Code       ;Code10        ;TableRelation=Location;
                                                   CaptionML=[ENU=Location Code;
                                                              PTG=C�d. Localiza��o];
                                                   NotBlank=Yes }
    { 4   ;   ;Assigned User ID    ;Code50        ;TableRelation="Warehouse Employee" WHERE (Location Code=FIELD(Location Code));
                                                   CaptionML=[ENU=Assigned User ID;
                                                              PTG=ID Utilizador Atribu�do] }
    { 5   ;   ;Assignment Date     ;Date          ;CaptionML=[ENU=Assignment Date;
                                                              PTG=Data Atribui��o] }
    { 6   ;   ;Assignment Time     ;Time          ;CaptionML=[ENU=Assignment Time;
                                                              PTG=Hora Atribui��o] }
    { 7   ;   ;Sorting Method      ;Option        ;CaptionML=[ENU=Sorting Method;
                                                              PTG=M�todo Ordena��o];
                                                   OptionCaptionML=[ENU=" ,Item,Document,Shelf or Bin,Due Date,Ship-To,Bin Ranking,Action Type";
                                                                    PTG=" ,Produto,Documento,Prateleira ou Posi��o,Data Vencimento,Envio-para,Classifica��o Posi��o,Tipo A��o"];
                                                   OptionString=[ ,Item,Document,Shelf or Bin,Due Date,Ship-To,Bin Ranking,Action Type] }
    { 8   ;   ;Registering Date    ;Date          ;CaptionML=[ENU=Registering Date;
                                                              PTG=Data Registo] }
    { 9   ;   ;No. Series          ;Code10        ;TableRelation="No. Series";
                                                   CaptionML=[ENU=No. Series;
                                                              PTG=N� S�ries];
                                                   Editable=No }
    { 10  ;   ;Comment             ;Boolean       ;FieldClass=FlowField;
                                                   CalcFormula=Exist("Warehouse Comment Line" WHERE (Table Name=CONST(Rgstrd. Whse. Activity Header),
                                                                                                     Type=FIELD(Type),
                                                                                                     No.=FIELD(No.)));
                                                   CaptionML=[ENU=Comment;
                                                              PTG=Coment�rio];
                                                   Editable=No }
    { 11  ;   ;Whse. Activity No.  ;Code20        ;CaptionML=[ENU=Whse. Activity No.;
                                                              PTG=N� Atividade Armaz�m] }
    { 12  ;   ;No. Printed         ;Integer       ;CaptionML=[ENU=No. Printed;
                                                              PTG=N� Impress�es];
                                                   Editable=No }
    { 31022919;;Series Group       ;Code10        ;TableRelation="Series Groups".Code;
                                                   CaptionML=[ENU=Series Group;
                                                              PTG=Sequ�ncia S�ries];
                                                   Description=soft,V92.00#00037 }
  }
  KEYS
  {
    {    ;Type,No.                                ;Clustered=Yes }
    {    ;No.,Type                                 }
    {    ;Whse. Activity No.                       }
    {    ;Location Code                            }
  }
  FIELDGROUPS
  {
  }
  CODE
  {
    VAR
      Text000@1000 : TextConst 'ENU=You must first set up user %1 as a warehouse employee.;PTG=Deve primeiro configurar o utilizador %1 como um empregado de armaz�m.';

    PROCEDURE SetWhseLocationFilter@12();
    VAR
      WmsManagement@1000 : Codeunit 7302;
    BEGIN
      IF USERID <> '' THEN BEGIN
        FILTERGROUP := 2;
        SETRANGE("Location Code",WmsManagement.GetAllowedLocation("Location Code"));
        FILTERGROUP := 0;
      END;
    END;

    PROCEDURE LookupRegisteredActivityHeader@1(VAR CurrentLocationCode@1000 : Code[10];VAR RegisteredWhseActivHeader@1001 : Record 5772);
    BEGIN
      COMMIT;
      IF USERID <> '' THEN BEGIN
        RegisteredWhseActivHeader.FILTERGROUP := 2;
        RegisteredWhseActivHeader.SETRANGE("Location Code");
      END;
      IF PAGE.RUNMODAL(0,RegisteredWhseActivHeader) = ACTION::LookupOK THEN;
      IF USERID <> '' THEN BEGIN
        RegisteredWhseActivHeader.FILTERGROUP := 2;
        RegisteredWhseActivHeader.SETRANGE("Location Code",RegisteredWhseActivHeader."Location Code");
        RegisteredWhseActivHeader.FILTERGROUP := 0;
      END;
      CurrentLocationCode := RegisteredWhseActivHeader."Location Code";
    END;

    BEGIN
    {
      V92.00#00037 - ID Series Group Errado - EFD - 2016.03.31
    }
    END.
  }
}

