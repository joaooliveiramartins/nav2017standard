OBJECT Page 9007 Purchasing Agent Role Center
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Role Center;
               PTG=Centro Perfil];
    PageType=RoleCenter;
    ActionList=ACTIONS
    {
      { 1900000006;0 ;ActionContainer;
                      ActionContainerType=Reports }
      { 5       ;1   ;Action    ;
                      CaptionML=[ENU=Vendor - T&op 10 List;
                                 PTG=F&ornecedor - Lista 10 Melhores];
                      RunObject=Report 311;
                      Image=Report }
      { 6       ;1   ;Action    ;
                      CaptionML=[ENU=Vendor/&Item Purchases;
                                 PTG=Compras &Fornecedor/Produto];
                      RunObject=Report 313;
                      Image=Report }
      { 28      ;1   ;Separator  }
      { 8       ;1   ;Action    ;
                      CaptionML=[ENU=Inventory - &Availability Plan;
                                 PTG=Invent�rio - Pl&ano Disponibilidade];
                      RunObject=Report 707;
                      Image=ItemAvailability }
      { 9       ;1   ;Action    ;
                      CaptionML=[ENU=Inventory &Purchase Orders;
                                 PTG=Encomendas Com&pra Invent�rio];
                      RunObject=Report 709;
                      Image=Report }
      { 13      ;1   ;Action    ;
                      CaptionML=[ENU=Inventory - &Vendor Purchases;
                                 PTG=In&vent�rio - Compras Fornecedor];
                      RunObject=Report 714;
                      Image=Report }
      { 14      ;1   ;Action    ;
                      CaptionML=[ENU=Inventory &Cost and Price List;
                                 PTG=Lista &Custos e Pre�os Invent�rio];
                      RunObject=Report 716;
                      Image=Report }
      { 1900000011;0 ;ActionContainer;
                      ActionContainerType=HomeItems }
      { 7       ;1   ;Action    ;
                      Name=PurchaseOrders;
                      CaptionML=[ENU=Purchase Orders;
                                 PTG=Encomendas Compra];
                      RunObject=Page 9307 }
      { 3       ;1   ;Action    ;
                      Name=PurchaseOrdersPendConf;
                      CaptionML=[ENU=Pending Confirmation;
                                 PTG=Confirma��o Pendente];
                      RunObject=Page 9307;
                      RunPageView=WHERE(Status=FILTER(Open)) }
      { 23      ;1   ;Action    ;
                      Name=PurchaseOrdersPartDeliv;
                      CaptionML=[ENU=Partially Delivered;
                                 PTG=Entregue Parcialmente];
                      RunObject=Page 9307;
                      RunPageView=WHERE(Status=FILTER(Released),
                                        Receive=FILTER(Yes),
                                        Completely Received=FILTER(No)) }
      { 76      ;1   ;Action    ;
                      CaptionML=[ENU=Purchase Quotes;
                                 PTG=Propostas Compra];
                      RunObject=Page 9306 }
      { 78      ;1   ;Action    ;
                      CaptionML=[ENU=Blanket Purchase Orders;
                                 PTG=Encomendas Compra Abertas];
                      RunObject=Page 9310 }
      { 82      ;1   ;Action    ;
                      CaptionML=[ENU=Purchase Invoices;
                                 PTG=Faturas Compra];
                      RunObject=Page 9308 }
      { 83      ;1   ;Action    ;
                      CaptionML=[ENU=Purchase Return Orders;
                                 PTG=Devolu��es Compra];
                      RunObject=Page 9311 }
      { 31      ;1   ;Action    ;
                      CaptionML=[ENU=Purchase Credit Memos;
                                 PTG=Notas Cr�dito Compra];
                      RunObject=Page 9309 }
      { 47      ;1   ;Action    ;
                      CaptionML=[ENU=Purchase Debit Memos;
                                 PTG=Notas D�bito Compra];
                      RunObject=Page 31023069 }
      { 26      ;1   ;Action    ;
                      CaptionML=[ENU=Assembly Orders;
                                 PTG=Ordens Montagem];
                      RunObject=Page 902 }
      { 32      ;1   ;Action    ;
                      CaptionML=[ENU=Sales Orders;
                                 PTG=Encomendas Venda];
                      RunObject=Page 9305;
                      Image=Order }
      { 85      ;1   ;Action    ;
                      CaptionML=[ENU=Vendors;
                                 PTG=Fornecedores];
                      RunObject=Page 27;
                      Image=Vendor }
      { 88      ;1   ;Action    ;
                      CaptionML=[ENU=Items;
                                 PTG=Produtos];
                      RunObject=Page 31;
                      Image=Item }
      { 91      ;1   ;Action    ;
                      CaptionML=[ENU=Nonstock Items;
                                 PTG=Produtos N�o Armazenados];
                      RunObject=Page 5726;
                      Image=NonStockItem }
      { 94      ;1   ;Action    ;
                      CaptionML=[ENU=Stockkeeping Units;
                                 PTG=Unidades Armazenamento];
                      RunObject=Page 5701;
                      Image=SKU }
      { 95      ;1   ;Action    ;
                      CaptionML=[ENU=Purchase Analysis Reports;
                                 PTG=Mapas An�lise Compras];
                      RunObject=Page 9375;
                      RunPageView=WHERE(Analysis Area=FILTER(Purchase)) }
      { 96      ;1   ;Action    ;
                      CaptionML=[ENU=Inventory Analysis Reports;
                                 PTG=Mapas An�lise Invent�rio];
                      RunObject=Page 9377;
                      RunPageView=WHERE(Analysis Area=FILTER(Inventory)) }
      { 10      ;1   ;Action    ;
                      CaptionML=[ENU=Item Journals;
                                 PTG=Di�rios Produto];
                      RunObject=Page 262;
                      RunPageView=WHERE(Template Type=CONST(Item),
                                        Recurring=CONST(No)) }
      { 17      ;1   ;Action    ;
                      CaptionML=[ENU=Purchase Journals;
                                 PTG=Di�rios Compras];
                      RunObject=Page 251;
                      RunPageView=WHERE(Template Type=CONST(Purchases),
                                        Recurring=CONST(No)) }
      { 19      ;1   ;Action    ;
                      Name=RequisitionWorksheets;
                      CaptionML=[ENU=Requisition Worksheets;
                                 PTG=Folhas Requisi��es];
                      RunObject=Page 295;
                      RunPageView=WHERE(Template Type=CONST(Req.),
                                        Recurring=CONST(No)) }
      { 20      ;1   ;Action    ;
                      Name=SubcontractingWorksheets;
                      CaptionML=[ENU=Subcontracting Worksheets;
                                 PTG=Folhas de Subcontrata��o];
                      RunObject=Page 295;
                      RunPageView=WHERE(Template Type=CONST(For. Labor),
                                        Recurring=CONST(No)) }
      { 22      ;1   ;Action    ;
                      CaptionML=[ENU=Standard Cost Worksheets;
                                 PTG=Folhas Custos Padr�o];
                      RunObject=Page 5840 }
      { 1900000012;0 ;ActionContainer;
                      ActionContainerType=ActivityButtons }
      { 39      ;1   ;ActionGroup;
                      CaptionML=[ENU=Posted Documents;
                                 PTG=Documentos Registados];
                      Image=FiledPosted }
      { 40      ;2   ;Action    ;
                      CaptionML=[ENU=Posted Purchase Receipts;
                                 PTG=Guias Remessa Compra Registadas];
                      RunObject=Page 145 }
      { 42      ;2   ;Action    ;
                      CaptionML=[ENU=Posted Purchase Invoices;
                                 PTG=Faturas Compra Registadas];
                      RunObject=Page 146 }
      { 12      ;2   ;Action    ;
                      CaptionML=[ENU=Posted Return Shipments;
                                 PTG=Envios Devolu��o Registados];
                      RunObject=Page 6652 }
      { 15      ;2   ;Action    ;
                      CaptionML=[ENU=Posted Purchase Credit Memos;
                                 PTG=Notas Cr�dito Compra Registadas];
                      RunObject=Page 147 }
      { 48      ;2   ;Action    ;
                      CaptionML=[ENU=Posted Purchase Debit Memos;
                                 PTG=Notas D�bito Compra Registadas];
                      RunObject=Page 31023072 }
      { 27      ;2   ;Action    ;
                      CaptionML=[ENU=Posted Assembly Orders;
                                 PTG=Encs. Montagem Registadas];
                      RunObject=Page 922 }
      { 1       ;0   ;ActionContainer;
                      ActionContainerType=NewDocumentItems }
      { 18      ;1   ;Action    ;
                      CaptionML=[ENU=Purchase &Quote;
                                 PTG=Propost&a Compra];
                      RunObject=Page 49;
                      Promoted=No;
                      Image=Quote;
                      PromotedCategory=Process;
                      RunPageMode=Create }
      { 16      ;1   ;Action    ;
                      CaptionML=[ENU=Purchase &Invoice;
                                 PTG=Fatura &Compra];
                      RunObject=Page 51;
                      Promoted=No;
                      Image=Invoice;
                      PromotedCategory=Process;
                      RunPageMode=Create }
      { 49      ;1   ;Action    ;
                      CaptionML=[ENU=Purchase Debit Memo;
                                 PTG=Nota D�bito Compra];
                      RunObject=Page 31023069;
                      Promoted=No;
                      Image=Invoice;
                      PromotedCategory=Process;
                      RunPageMode=Create }
      { 4       ;1   ;Action    ;
                      CaptionML=[ENU=Purchase &Order;
                                 PTG=Encomenda C&ompra];
                      RunObject=Page 50;
                      Promoted=No;
                      Image=Document;
                      PromotedCategory=Process;
                      RunPageMode=Create }
      { 2       ;1   ;Action    ;
                      CaptionML=[ENU=Purchase &Return Order;
                                 PTG=De&volu��o Compra];
                      RunObject=Page 6640;
                      Promoted=No;
                      Image=ReturnOrder;
                      PromotedCategory=Process;
                      RunPageMode=Create }
      { 1900000004;0 ;ActionContainer;
                      ActionContainerType=ActionItems }
      { 24      ;1   ;Separator ;
                      CaptionML=[ENU=Tasks;
                                 PTG=Tarefas];
                      IsHeader=Yes }
      { 29      ;1   ;Action    ;
                      CaptionML=[ENU=&Purchase Journal;
                                 PTG=Di�rio Com&pras];
                      RunObject=Page 254;
                      Image=Journals }
      { 30      ;1   ;Action    ;
                      CaptionML=[ENU=Item &Journal;
                                 PTG=&Di�rio Produto];
                      RunObject=Page 40;
                      Image=Journals }
      { 11      ;1   ;Action    ;
                      CaptionML=[ENU=Order Plan&ning;
                                 PTG=Planea&mento da Encomenda];
                      RunObject=Page 5522;
                      Image=Planning }
      { 38      ;1   ;Separator  }
      { 33      ;1   ;Action    ;
                      CaptionML=[ENU=Requisition &Worksheet;
                                 PTG=Fol&ha Requisi��o];
                      RunObject=Page 295;
                      RunPageView=WHERE(Template Type=CONST(Req.),
                                        Recurring=CONST(No));
                      Image=Worksheet }
      { 34      ;1   ;Action    ;
                      CaptionML=[ENU=Pur&chase Prices;
                                 PTG=Pr&e�os Compra];
                      RunObject=Page 7012;
                      Image=Price }
      { 41      ;1   ;Action    ;
                      CaptionML=[ENU=Purchase &Line Discounts;
                                 PTG=Descontos &Linha Compra];
                      RunObject=Page 7014;
                      Image=LineDiscount }
      { 36      ;1   ;Separator ;
                      CaptionML=[ENU=History;
                                 PTG=Hist�rico];
                      IsHeader=Yes }
      { 46      ;1   ;Action    ;
                      CaptionML=[ENU=Navi&gate;
                                 PTG=Nave&gar];
                      RunObject=Page 344;
                      Image=Navigate }
    }
  }
  CONTROLS
  {
    { 1900000008;0;Container;
                ContainerType=RoleCenterArea }

    { 1900724808;1;Group   }

    { 1907662708;2;Part   ;
                PagePartID=Page9063;
                PartType=Page }

    { 1902476008;2;Part   ;
                PagePartID=Page9151;
                PartType=Page }

    { 1900724708;1;Group   }

    { 25  ;2   ;Part      ;
                PagePartID=Page771;
                PartType=Page }

    { 37  ;2   ;Part      ;
                PagePartID=Page771;
                Visible=false;
                PartType=Page }

    { 21  ;2   ;Part      ;
                PagePartID=Page772;
                PartType=Page }

    { 44  ;2   ;Part      ;
                PagePartID=Page772;
                Visible=false;
                PartType=Page }

    { 45  ;2   ;Part      ;
                PagePartID=Page681;
                PartType=Page }

    { 35  ;2   ;Part      ;
                PagePartID=Page675;
                Visible=false;
                PartType=Page }

    { 1905989608;2;Part   ;
                PagePartID=Page9152;
                PartType=Page }

    { 1903012608;2;Part   ;
                PagePartID=Page9175;
                Visible=FALSE;
                PartType=Page }

    { 43  ;2   ;Part      ;
                PartType=System;
                SystemPartID=MyNotes }

  }
  CODE
  {

    BEGIN
    END.
  }
}

