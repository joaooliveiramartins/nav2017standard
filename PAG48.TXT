OBJECT Page 48 Sales Orders
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0;
  }
  PROPERTIES
  {
    Editable=No;
    CaptionML=[ENU=Sales Orders;
               PTG=Encomendas Venda];
    SourceTable=Table37;
    SourceTableView=WHERE(Document Type=FILTER(Order));
    DataCaptionFields=No.;
    PageType=List;
    ActionList=ACTIONS
    {
      { 1900000003;0 ;ActionContainer;
                      ActionContainerType=RelatedInformation }
      { 32      ;1   ;ActionGroup;
                      CaptionML=[ENU=&Line;
                                 PTG=&Linha];
                      Image=Line }
      { 33      ;2   ;Action    ;
                      ShortCutKey=Shift+F7;
                      CaptionML=[ENU=Show Order;
                                 PTG=Mostrar Encomenda];
                      ToolTipML=ENU=View the selected sales order.;
                      ApplicationArea=#Basic,#Suite;
                      RunObject=Page 42;
                      RunPageLink=Document Type=FIELD(Document Type),
                                  No.=FIELD(Document No.);
                      Image=ViewOrder }
      { 31      ;2   ;Action    ;
                      AccessByPermission=TableData 27=R;
                      CaptionML=[ENU=Reservation Entries;
                                 PTG=Movs. Reserva];
                      Image=ReservationLedger;
                      OnAction=BEGIN
                                 ShowReservationEntries(TRUE);
                               END;
                                }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 21  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the type of entity that will be posted for this sales line, such as Item, Resource, or G/L Account.;
                SourceExpr=Type }

    { 23  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the number of a general ledger account, an item, a resource, an additional cost or a fixed asset, depending on what you selected in the Type field.;
                ApplicationArea=#All;
                SourceExpr="No." }

    { 25  ;2   ;Field     ;
                ToolTipML=ENU=Specifies a description of the entry. The description depends on what you chose in the Type field. If you did not choose Blank, the program will fill in the field when you enter something in the No. field.;
                ApplicationArea=#All;
                SourceExpr=Description }

    { 2   ;2   ;Field     ;
                ToolTipML=ENU=Specifies the date that the items on the line are in inventory and available to be picked.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Shipment Date" }

    { 4   ;2   ;Field     ;
                ToolTipML=ENU=Specifies the number of the customer to whom the items in the sales order will be shipped.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Sell-to Customer No." }

    { 6   ;2   ;Field     ;
                ToolTipML=ENU=Specifies the document number.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Document No." }

    { 27  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the currency code for the amount on this line.;
                ApplicationArea=#Suite;
                SourceExpr="Currency Code" }

    { 8   ;2   ;Field     ;
                ToolTipML=ENU=Specifies how many units are being sold.;
                SourceExpr=Quantity }

    { 10  ;2   ;Field     ;
                ToolTipML=ENU=Specifies how many units on the order line have not yet been shipped.;
                SourceExpr="Outstanding Quantity" }

    { 29  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the unit of measure that is used to determine the value in the Unit Price field on the sales line.;
                SourceExpr="Unit of Measure Code" }

    { 34  ;2   ;Field     ;
                ToolTipML=ENU=Belongs to the Job application area.;
                SourceExpr="Work Type Code";
                Visible=FALSE }

    { 12  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the sum of amounts in the Line Amount field on the sales order lines.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr=Amount }

    { 14  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the price for one unit on the sales line.;
                SourceExpr="Unit Price" }

    { 16  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the line discount percentage that is valid for the item quantity on the line.;
                SourceExpr="Line Discount %" }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {

    BEGIN
    END.
  }
}

