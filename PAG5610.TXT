OBJECT Page 5610 Depreciation Book Card
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0,NAVPTSS81.00;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Depreciation Book Card;
               PTG=Ficha Livro Amortiza��o];
    SourceTable=Table5611;
    PageType=Card;
    RefreshOnActivate=Yes;
    ActionList=ACTIONS
    {
      { 1900000003;0 ;ActionContainer;
                      ActionContainerType=RelatedInformation }
      { 59      ;1   ;ActionGroup;
                      CaptionML=[ENU=&Depr. Book;
                                 PTG=Livro &Amortiza��o];
                      Image=DepreciationsBooks }
      { 61      ;2   ;Action    ;
                      Ellipsis=No;
                      CaptionML=[ENU=FA Posting Type Setup;
                                 PTG=Conf. Tipo Registo Imobilizado];
                      ToolTipML=ENU=Set up how to handle the write-down, appreciation, custom 1, and custom 2 posting types that you use when posting to fixed assets.;
                      ApplicationArea=#FixedAssets;
                      RunObject=Page 5608;
                      RunPageLink=Depreciation Book Code=FIELD(Code);
                      Image=Setup }
      { 16      ;2   ;Action    ;
                      CaptionML=[ENU=FA &Journal Setup;
                                 PTG=Con&fig. Di�rio Imobilizado];
                      ToolTipML=ENU=Set up the FA general ledger journal, the FA journal, and the insurance journal templates and batches to use when duplicating depreciation entries and acquisition-cost entries and when calculating depreciation or indexing fixed assets.;
                      ApplicationArea=#FixedAssets;
                      RunObject=Page 5609;
                      RunPageLink=Depreciation Book Code=FIELD(Code);
                      Image=JournalSetup }
      { 1900000004;0 ;ActionContainer;
                      ActionContainerType=ActionItems }
      { 40      ;1   ;ActionGroup;
                      CaptionML=[ENU=F&unctions;
                                 PTG=F&un��es];
                      Image=Action }
      { 41      ;2   ;Action    ;
                      Ellipsis=Yes;
                      CaptionML=[ENU=Create FA Depreciation Books;
                                 PTG=Criar Livros Amortiza��o Imob.];
                      ToolTipML=ENU=Create depreciation books for the fixed asset. You can create empty fixed asset depreciation books, for example for all fixed assets, when you have set up a new depreciation book. You can also use an existing fixed asset depreciation book as the basis for new book.;
                      ApplicationArea=#FixedAssets;
                      RunObject=Report 5689;
                      Image=NewDepreciationBook }
      { 19      ;2   ;Action    ;
                      Ellipsis=Yes;
                      CaptionML=[ENU=C&opy Depreciation Book;
                                 PTG=C&opiar Livro Amortiza��o];
                      ToolTipML=ENU=Copy specified entries from one depreciation book to another. The entries are not posted to the new depreciation book - they are either inserted as lines in a general ledger fixed asset journal or in a fixed asset journal, depending on whether the new depreciation book has activated general ledger integration.;
                      ApplicationArea=#FixedAssets;
                      RunObject=Report 5687;
                      Image=CopyDepreciationBook }
      { 18      ;2   ;Action    ;
                      Ellipsis=Yes;
                      CaptionML=[ENU=C&ancel FA Ledger Entries;
                                 PTG=C&ancelar Movs. Imobilizado];
                      ToolTipML=ENU=Remove one or more fixed asset ledger entries from the FA Ledger Entries window. If you posted erroneous transactions to one or more fixed assets, you can use this function to cancel the fixed asset ledger entries. In the FA Ledger Entries window, select the entry or entries that you want to cancel.;
                      ApplicationArea=#FixedAssets;
                      RunObject=Report 5688;
                      Image=CancelFALedgerEntries }
      { 55      ;2   ;Action    ;
                      Ellipsis=Yes;
                      CaptionML=[ENU=Co&py FA Entries to G/L Budget;
                                 PTG=Co&piar Movs. Imob. para Or�amento C/G];
                      ApplicationArea=#Suite;
                      RunObject=Report 5684;
                      Image=CopyLedgerToBudget }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                CaptionML=[ENU=General;
                           PTG=Geral] }

    { 2   ;2   ;Field     ;
                ToolTipML=ENU=Specifies a code that identifies the depreciation book.;
                ApplicationArea=#FixedAssets;
                SourceExpr=Code }

    { 4   ;2   ;Field     ;
                ToolTipML=ENU=Specifies the purpose of the depreciation book.;
                ApplicationArea=#FixedAssets;
                SourceExpr=Description }

    { 6   ;2   ;Field     ;
                ToolTipML=ENU=Specifies the final rounding amount to use if the Final Rounding Amount field is zero.;
                ApplicationArea=#FixedAssets;
                SourceExpr="Default Final Rounding Amount" }

    { 8   ;2   ;Field     ;
                ToolTipML=ENU=Specifies the ending book value to use if the Ending Book Value field is zero.;
                ApplicationArea=#FixedAssets;
                SourceExpr="Default Ending Book Value" }

    { 10  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the disposal method for the current depreciation book.;
                ApplicationArea=#FixedAssets;
                SourceExpr="Disposal Calculation Method" }

    { 53  ;2   ;Field     ;
                ToolTipML=ENU=Specifies that the line and invoice discount are subtracted from the acquisition cost posted for the fixed asset.;
                ApplicationArea=#FixedAssets;
                SourceExpr="Subtract Disc. in Purch. Inv." }

    { 56  ;2   ;Field     ;
                ToolTipML=ENU=Specifies whether to correct fixed ledger entries of the Disposal type.;
                ApplicationArea=#FixedAssets;
                SourceExpr="Allow Correction of Disposal" }

    { 39  ;2   ;Field     ;
                ToolTipML=ENU=Specifies whether to allow the depreciation fields to be modified.;
                ApplicationArea=#FixedAssets;
                SourceExpr="Allow Changes in Depr. Fields" }

    { 68  ;2   ;Field     ;
                ToolTipML=ENU=Specifies whether you sell a fixed asset with the net disposal method.;
                ApplicationArea=#FixedAssets;
                SourceExpr="VAT on Net Disposal Entries" }

    { 64  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the check box for this field to allow identical document numbers in the depreciation book.;
                ApplicationArea=#FixedAssets;
                SourceExpr="Allow Identical Document No." }

    { 45  ;2   ;Field     ;
                ToolTipML=ENU=Specifies whether to allow indexation of FA ledger entries and maintenance ledger entries posted to this book.;
                ApplicationArea=#FixedAssets;
                SourceExpr="Allow Indexation" }

    { 43  ;2   ;Field     ;
                ToolTipML=ENU=Specifies whether to allow the Calculate Depreciation batch job to continue calculating depreciation even if the book value is zero or below.;
                ApplicationArea=#FixedAssets;
                SourceExpr="Allow Depr. below Zero" }

    { 62  ;2   ;Field     ;
                ToolTipML=ENU=Specifies if the fiscal year has more than 360 depreciation days.;
                ApplicationArea=#FixedAssets;
                SourceExpr="Allow more than 360/365 Days" }

    { 35  ;2   ;Field     ;
                ToolTipML=ENU=Specifies which checks to perform before posting a journal line.;
                ApplicationArea=#FixedAssets;
                SourceExpr="Use FA Ledger Check" }

    { 49  ;2   ;Field     ;
                ToolTipML=ENU=Specifies whether the calculated periodic depreciation amounts should be rounded to whole numbers.;
                ApplicationArea=#FixedAssets;
                SourceExpr="Use Rounding in Periodic Depr." }

    { 22  ;2   ;Field     ;
                ToolTipML=ENU=Specifies whether to indicate that the Posting Date and the FA Posting Date must be the same on a journal line before posting.;
                ApplicationArea=#FixedAssets;
                SourceExpr="Use Same FA+G/L Posting Dates" }

    { 66  ;2   ;Field     ;
                ToolTipML=ENU=Specifies that when the Calculate Depreciation batch job calculates depreciations, a standardized year of 360 days, where each month has 30 days, is used.;
                ApplicationArea=#FixedAssets;
                SourceExpr="Fiscal Year 365 Days" }

    { 1110001;2;Field     ;
                SourceExpr="POC Calculation and Disposal" }

    { 1905052801;1;Group  ;
                CaptionML=[ENU=Integration;
                           PTG=Integra��o] }

    { 26  ;2   ;Group     ;
                CaptionML=[ENU=G/L Integration;
                           PTG=Integra��o C/G] }

    { 17  ;3   ;Field     ;
                CaptionML=[ENU=Acquisition Cost;
                           PTG=Custo Aquisi��o];
                ToolTipML=ENU=Specifies whether acquisition cost entries posted to this depreciation book are posted both to the general ledger and the FA ledger.;
                ApplicationArea=#FixedAssets;
                SourceExpr="G/L Integration - Acq. Cost" }

    { 29  ;3   ;Field     ;
                CaptionML=[ENU=Depreciation;
                           PTG=Amortiza��o];
                ToolTipML=ENU=Specifies whether depreciation entries posted to this depreciation book are posted both to the general ledger and the FA ledger.;
                ApplicationArea=#FixedAssets;
                SourceExpr="G/L Integration - Depreciation" }

    { 27  ;3   ;Field     ;
                CaptionML=[ENU=Write-Down;
                           PTG=Deprecia��o];
                ToolTipML=ENU=Specifies whether write-down entries posted to this depreciation book should be posted to the general ledger and the FA ledger.;
                ApplicationArea=#FixedAssets;
                SourceExpr="G/L Integration - Write-Down" }

    { 30  ;3   ;Field     ;
                CaptionML=[ENU=Appreciation;
                           PTG=Reavalia��o];
                ToolTipML=ENU=Specifies whether appreciation entries posted to this depreciation book are posted to the general ledger and the FA ledger.;
                ApplicationArea=#FixedAssets;
                SourceExpr="G/L Integration - Appreciation" }

    { 32  ;3   ;Field     ;
                CaptionML=[ENU=Custom 1;
                           PTG=Especial 1];
                ToolTipML=ENU=Specifies whether custom 1 entries posted to this depreciation book are posted to the general ledger and the FA ledger.;
                ApplicationArea=#FixedAssets;
                SourceExpr="G/L Integration - Custom 1" }

    { 34  ;3   ;Field     ;
                CaptionML=[ENU=Custom 2;
                           PTG=Especial 2];
                ToolTipML=ENU=Specifies whether custom 2 entries posted to this depreciation book are posted to the general ledger and the FA ledger.;
                ApplicationArea=#FixedAssets;
                SourceExpr="G/L Integration - Custom 2" }

    { 36  ;3   ;Field     ;
                CaptionML=[ENU=Disposal;
                           PTG=Venda/Abate];
                ToolTipML=ENU=Specifies whether disposal entries posted to this depreciation book are posted to the general ledger and the FA ledger.;
                ApplicationArea=#FixedAssets;
                SourceExpr="G/L Integration - Disposal" }

    { 38  ;3   ;Field     ;
                CaptionML=[ENU=Maintenance;
                           PTG=Manuten��o];
                ToolTipML=ENU=Specifies whether maintenance entries that are posted to this depreciation book are posted both to the general ledger and the FA ledger.;
                ApplicationArea=#FixedAssets;
                SourceExpr="G/L Integration - Maintenance" }

    { 1000000000;3;Field  ;
                CaptionML=[ENU=Imparities;
                           PTG=Imparidades];
                SourceExpr="G/L Integration - Imparities" }

    { 1907428201;1;Group  ;
                CaptionML=[ENU=Duplication;
                           PTG=Duplica��o] }

    { 28  ;2   ;Field     ;
                ToolTipML=ENU=Specifies whether to indicate that entries made in another depreciation book should be duplicated to this depreciation book.;
                ApplicationArea=#FixedAssets;
                SourceExpr="Part of Duplication List" }

    { 20  ;2   ;Field     ;
                ToolTipML=ENU=Specifies whether to use the FA Exchange Rate field when you duplicate entries from one depreciation book to another.;
                ApplicationArea=#FixedAssets;
                SourceExpr="Use FA Exch. Rate in Duplic." }

    { 24  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the exchange rate to use if the rate in the FA Exchange Rate field is zero.;
                ApplicationArea=#FixedAssets;
                SourceExpr="Default Exchange Rate" }

    { 1904488501;1;Group  ;
                CaptionML=[ENU=Reporting;
                           PTG=Mapas] }

    { 13  ;2   ;Group     ;
                CaptionML=[ENU=Use Add.-Curr Exch. Rate;
                           PTG=Utilizar Taxa C�mbio Div.-Adic.] }

    { 12  ;3   ;Field     ;
                CaptionML=[ENU=Acquisition Cost;
                           PTG=Custo Aquisi��o];
                ToolTipML=ENU=Specifies whether acquisition cost entries posted to this depreciation book are posted both to the general ledger and the FA ledger.;
                ApplicationArea=#Suite;
                SourceExpr="Add-Curr Exch Rate - Acq. Cost" }

    { 14  ;3   ;Field     ;
                CaptionML=[ENU=Depreciation;
                           PTG=Amortiza��o];
                ToolTipML=ENU=Records depreciation transactions in the general ledger in both LCY and any additional reporting currency.;
                ApplicationArea=#Suite;
                SourceExpr="Add.-Curr. Exch. Rate - Depr." }

    { 15  ;3   ;Field     ;
                CaptionML=[ENU=Write-Down;
                           PTG=Deprecia��o];
                ToolTipML=ENU=Records write-down transactions in the general ledger in both LCY and any additional reporting currency.;
                ApplicationArea=#Suite;
                SourceExpr="Add-Curr Exch Rate -Write-Down" }

    { 33  ;3   ;Field     ;
                CaptionML=[ENU=Appreciation;
                           PTG=Reavalia��o];
                ToolTipML=ENU=Records appreciation transactions in the general ledger in both LCY and any additional reporting currency.;
                ApplicationArea=#Suite;
                SourceExpr="Add-Curr. Exch. Rate - Apprec." }

    { 47  ;3   ;Field     ;
                CaptionML=[ENU=Custom 1;
                           PTG=Especial 1];
                ToolTipML=ENU=Specifies whether custom 1 entries posted to this depreciation book are posted to the general ledger and the FA ledger.;
                ApplicationArea=#Suite;
                SourceExpr="Add-Curr. Exch Rate - Custom 1" }

    { 48  ;3   ;Field     ;
                CaptionML=[ENU=Custom 2;
                           PTG=Especial 2];
                ToolTipML=ENU=Records custom 2 transactions in the general ledger in both LCY and any additional reporting currency.;
                ApplicationArea=#Suite;
                SourceExpr="Add-Curr. Exch Rate - Custom 2" }

    { 51  ;3   ;Field     ;
                CaptionML=[ENU=Disposal;
                           PTG=Venda/Abate];
                ToolTipML=ENU=Records disposal transactions in the general ledger in both LCY and any additional reporting currency.;
                ApplicationArea=#Suite;
                SourceExpr="Add.-Curr. Exch. Rate - Disp." }

    { 52  ;3   ;Field     ;
                CaptionML=[ENU=Maintenance;
                           PTG=Manuten��o];
                ToolTipML=ENU=Records maintenance transactions in the general ledger in both LCY and any additional reporting currency.;
                ApplicationArea=#Suite;
                SourceExpr="Add.-Curr. Exch. Rate - Maint." }

    { 1000000001;3;Field  ;
                CaptionML=[ENU=Imparities;
                           PTG=Imparidades];
                SourceExpr="Add.-Curr. Exch. Rate - Impa." }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {

    BEGIN
    END.
  }
}

