OBJECT Table 31022969 Imparities Journal Batch
{
  OBJECT-PROPERTIES
  {
    Date=21/12/16;
    Time=13:00:00;
    Version List=NAVPTSS10.00;
  }
  PROPERTIES
  {
    DataCaptionFields=Name,Description;
    CaptionML=[ENU=Imparity Journal Batch;
               PTG=Sec��o Di�rio Imparidade];
    LookupPageID=Page31023045;
    DrillDownPageID=Page31023045;
  }
  FIELDS
  {
    { 1   ;   ;Journal Template Name;Code10       ;TableRelation="Imparities Journal Template";
                                                   CaptionML=[ENU=Journal Template Name;
                                                              PTG=Nome Livro Di�rio];
                                                   NotBlank=Yes }
    { 2   ;   ;Name                ;Code10        ;CaptionML=[ENU=Name;
                                                              PTG=Nome];
                                                   NotBlank=Yes }
    { 3   ;   ;Description         ;Text50        ;CaptionML=[ENU=Description;
                                                              PTG=Descri��o] }
    { 5   ;   ;No. Series          ;Code10        ;TableRelation="No. Series";
                                                   OnValidate=BEGIN
                                                                IF "No. Series" <> '' THEN BEGIN
                                                                  ImpJnlTemplate.GET("Journal Template Name");
                                                                  IF "No. Series" = "Posting No. Series" THEN
                                                                    VALIDATE("Posting No. Series",'');
                                                                END;
                                                              END;

                                                   CaptionML=[ENU=No. Series;
                                                              PTG=N� S�ries] }
    { 6   ;   ;Posting No. Series  ;Code10        ;TableRelation="No. Series";
                                                   OnValidate=BEGIN
                                                                IF ("Posting No. Series" = "No. Series") AND ("Posting No. Series" <> '') THEN
                                                                  FIELDERROR("Posting No. Series",STRSUBSTNO(Text31022890,"Posting No. Series"));
                                                                FAJnlLine.SETRANGE("Journal Template Name","Journal Template Name");
                                                                FAJnlLine.SETRANGE("Journal Batch Name",Name);
                                                                FAJnlLine.MODIFYALL("Posting No. Series","Posting No. Series");
                                                                MODIFY;
                                                              END;

                                                   CaptionML=[ENU=Posting No. Series;
                                                              PTG=N� S�ries Registo] }
    { 20  ;   ;Post to G/L         ;Boolean       ;CaptionML=[ENU=Post to G/L;
                                                              PTG=Regista para C/G] }
    { 22  ;   ;Correction          ;Boolean       ;CaptionML=[ENU=Correction;
                                                              PTG=Estorno] }
  }
  KEYS
  {
    {    ;Journal Template Name,Name              ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {
    VAR
      ImpJnlTemplate@1102058000 : Record 31022968;
      Text31022890@1000 : TextConst 'ENU=must not be %1;PTG=N�o pode ser %1';
      FAJnlLine@1001 : Record 5621;

    PROCEDURE SetupNewBatch@3();
    BEGIN
      ImpJnlTemplate.GET("Journal Template Name");
      "No. Series" := ImpJnlTemplate."No. Series";
      "Posting No. Series" := ImpJnlTemplate."Posting No. Series";
    END;

    BEGIN
    END.
  }
}

