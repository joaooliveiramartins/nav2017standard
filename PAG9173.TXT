OBJECT Page 9173 User Personalization List
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0;
  }
  PROPERTIES
  {
    Editable=No;
    CaptionML=[ENU=User Personalization List;
               PTG=Lista Personaliza��o Utilizador];
    SourceTable=Table2000000073;
    PageType=List;
    CardPageID=User Personalization Card;
    OnOpenPage=BEGIN
                 HideExternalUsers;
               END;

  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 4   ;2   ;Field     ;
                DrillDown=No;
                CaptionML=[ENU=User ID;
                           PTG=ID Utilizador];
                ToolTipML=[ENU=Specifies the user ID of a user who is using Database Server Authentication to log on to Dynamics NAV.;
                           PTG=""];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="User ID" }

    { 6   ;2   ;Field     ;
                Lookup=No;
                CaptionML=[ENU=Profile ID;
                           PTG=ID Perfil];
                ToolTipML=[ENU=Specifies the ID of the profile that is associated with the current user.;
                           PTG=""];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Profile ID" }

    { 8   ;2   ;Field     ;
                CaptionML=[ENU=Language ID;
                           PTG=ID Idioma];
                ToolTipML=[ENU=Specifies the ID of the language that Microsoft Windows is set up to run for the selected user.;
                           PTG=""];
                BlankZero=Yes;
                SourceExpr="Language ID" }

    { 2   ;2   ;Field     ;
                CaptionML=[ENU=Locale ID;
                           PTG=ID Local];
                ToolTipML=[ENU=Specifies the ID of the locale that Microsoft Windows is set up to run for the selected user.;
                           PTG=""];
                BlankZero=Yes;
                SourceExpr="Locale ID";
                Visible=FALSE }

    { 3   ;2   ;Field     ;
                CaptionML=[ENU=Time Zone;
                           PTG=Fuso Hor�rio];
                ToolTipML=[ENU=Specifies the time zone that Microsoft Windows is set up to run for the selected user.;
                           PTG=""];
                SourceExpr="Time Zone";
                Visible=FALSE }

    { 10  ;2   ;Field     ;
                Lookup=No;
                CaptionML=[ENU=Company;
                           PTG=Empresa];
                ToolTipML=[ENU=Specifies the company that is associated with the user.;
                           PTG=""];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=Company }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {

    LOCAL PROCEDURE HideExternalUsers@5();
    VAR
      PermissionManager@1001 : Codeunit 9002;
      OriginalFilterGroup@1000 : Integer;
    BEGIN
      IF NOT PermissionManager.SoftwareAsAService THEN
        EXIT;

      OriginalFilterGroup := FILTERGROUP;
      FILTERGROUP := 2;
      CALCFIELDS("License Type");
      SETFILTER("License Type",'<>%1',"License Type"::"External User");
      FILTERGROUP := OriginalFilterGroup;
    END;

    BEGIN
    END.
  }
}

