OBJECT Page 5731 Product Groups
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Product Groups;
               PTG=Grupos Produto];
    SourceTable=Table5723;
    DataCaptionFields=Item Category Code;
    PageType=List;
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 11  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the item category code to which this product group code belongs.;
                           PTG=""];
                SourceExpr="Item Category Code";
                Visible=FALSE }

    { 2   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the code for the product group that applies to the item.;
                           PTG=""];
                SourceExpr=Code }

    { 9   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the warehouse class code for the product group.;
                           PTG=""];
                SourceExpr="Warehouse Class Code";
                Visible=FALSE }

    { 4   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the description of the product group.;
                           PTG=""];
                SourceExpr=Description }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {

    BEGIN
    END.
  }
}

