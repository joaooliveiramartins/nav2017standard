OBJECT Report 70000 Recover GLEntry_Vat Entry Link
{
  OBJECT-PROPERTIES
  {
    Date=13/02/16;
    Time=13:00:00;
    Version List=NAVPTSS82.00;
  }
  PROPERTIES
  {
    CaptionML=PTG=Restaurar Movimento Liga��o;
    ProcessingOnly=Yes;
  }
  DATASET
  {
    { 1000000000;;DataItem;                  ;
               DataItemTable=Table254;
               DataItemTableView=SORTING(Entry No.)
                                 WHERE(Amount=FILTER(<>0));
               OnAfterGetRecord=BEGIN
                                  Inserted := FALSE;
                                  GLEntry.SETRANGE("Document Type","Document Type");
                                  GLEntry.SETRANGE("Document No.","Document No.");
                                  GLEntry.SETRANGE("Gen. Posting Type",Type);
                                  GLEntry.SETRANGE("VAT Bus. Posting Group","VAT Bus. Posting Group");
                                  GLEntry.SETRANGE("VAT Prod. Posting Group","VAT Prod. Posting Group");
                                  GLEntry.FINDSET;
                                  IF GLEntry.COUNT = 1 THEN BEGIN
                                    GLEntryVATEntryLink."G/L Entry No." := GLEntry."Entry No.";
                                    GLEntryVATEntryLink."VAT Entry No." := "Entry No.";
                                    IF GLEntryVATEntryLink.INSERT THEN;
                                  END ELSE BEGIN
                                    REPEAT
                                      IF GLEntry.Amount = Base THEN BEGIN
                                        GLEntryVATEntryLink."G/L Entry No." := GLEntry."Entry No.";
                                        GLEntryVATEntryLink."VAT Entry No." := "Entry No.";
                                        IF NOT GLEntryVATEntryLink.INSERT THEN;
                                        Inserted := TRUE;
                                      END;
                                    UNTIL (GLEntry.NEXT = 0) OR Inserted;
                                    IF NOT Inserted THEN
                                      ERROR(Text0001,"Document No.",FORMAT("Entry No."));
                                  END;
                                END;
                                 }

  }
  REQUESTPAGE
  {
    PROPERTIES
    {
    }
    CONTROLS
    {
    }
  }
  LABELS
  {
  }
  CODE
  {
    VAR
      GLEntry@1000000000 : Record 17;
      GLEntryVATEntryLink@1000000001 : Record 253;
      Inserted@1000000002 : Boolean;
      Text0001@1000000003 : TextConst 'ENU=Existe mais do que 1 mov para o documento %1, n� mov. IVA %2.;PTG=Existe mais do que um movimento para o documento %1, n� mov. IVA %2.';

    BEGIN
    END.
  }
  RDLDATA
  {
  }
}

