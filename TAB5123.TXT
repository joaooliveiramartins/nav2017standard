OBJECT Table 5123 Inter. Log Entry Comment Line
{
  OBJECT-PROPERTIES
  {
    Date=07/09/12;
    Time=12:00:00;
    Version List=NAVW17.00;
  }
  PROPERTIES
  {
    OnModify=BEGIN
               "Last Date Modified" := TODAY;
             END;

    CaptionML=[ENU=Inter. Log Entry Comment Line;
               PTG=Linha Coment. Mov. Intera��o];
    LookupPageID=Page5188;
    DrillDownPageID=Page5188;
  }
  FIELDS
  {
    { 1   ;   ;Entry No.           ;Integer       ;TableRelation="Interaction Log Entry"."Entry No.";
                                                   CaptionML=[ENU=Entry No.;
                                                              PTG=N� Mov.] }
    { 4   ;   ;Line No.            ;Integer       ;CaptionML=[ENU=Line No.;
                                                              PTG=N� Linha] }
    { 5   ;   ;Date                ;Date          ;CaptionML=[ENU=Date;
                                                              PTG=Data] }
    { 6   ;   ;Code                ;Code10        ;CaptionML=[ENU=Code;
                                                              PTG=C�digo] }
    { 7   ;   ;Comment             ;Text80        ;CaptionML=[ENU=Comment;
                                                              PTG=Coment�rio] }
    { 8   ;   ;Last Date Modified  ;Date          ;CaptionML=[ENU=Last Date Modified;
                                                              PTG=Data �ltima Modif.];
                                                   Editable=No }
  }
  KEYS
  {
    {    ;Entry No.,Line No.                      ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    PROCEDURE SetUpNewLine@1();
    VAR
      InteractionCommentLine@1000 : Record 5123;
    BEGIN
      InteractionCommentLine.SETRANGE("Entry No.","Entry No.");
      InteractionCommentLine.SETRANGE(Date,WORKDATE);
      IF NOT InteractionCommentLine.FINDFIRST THEN
        Date := WORKDATE;
    END;

    BEGIN
    END.
  }
}

