OBJECT Table 6651 Return Shipment Line
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0,NAVPTSS93.00;
  }
  PROPERTIES
  {
    Permissions=TableData 32=r,
                TableData 5802=r;
    OnDelete=VAR
               PurchDocLineComments@1000 : Record 43;
             BEGIN
               PurchDocLineComments.SETRANGE("Document Type",PurchDocLineComments."Document Type"::"Posted Return Shipment");
               PurchDocLineComments.SETRANGE("No.","Document No.");
               PurchDocLineComments.SETRANGE("Document Line No.","Line No.");
               IF NOT PurchDocLineComments.ISEMPTY THEN
                 PurchDocLineComments.DELETEALL;
             END;

    CaptionML=[ENU=Return Shipment Line;
               PTG=Linha Envio Devolu��o];
    LookupPageID=Page6653;
  }
  FIELDS
  {
    { 2   ;   ;Buy-from Vendor No. ;Code20        ;TableRelation=Vendor;
                                                   CaptionML=[ENU=Buy-from Vendor No.;
                                                              PTG=Compra-a N� Fornecedor];
                                                   Editable=No }
    { 3   ;   ;Document No.        ;Code20        ;TableRelation="Return Shipment Header";
                                                   CaptionML=[ENU=Document No.;
                                                              PTG=N� Documento] }
    { 4   ;   ;Line No.            ;Integer       ;CaptionML=[ENU=Line No.;
                                                              PTG=N� Linha] }
    { 5   ;   ;Type                ;Option        ;CaptionML=[ENU=Type;
                                                              PTG=Tipo];
                                                   OptionCaptionML=[ENU=" ,G/L Account,Item,,Fixed Asset,Charge (Item)";
                                                                    PTG=" ,Conta C/G,Produto,,Imobilizado,Encargo (Prod.)"];
                                                   OptionString=[ ,G/L Account,Item,,Fixed Asset,Charge (Item)] }
    { 6   ;   ;No.                 ;Code20        ;TableRelation=IF (Type=CONST(G/L Account)) "G/L Account"
                                                                 ELSE IF (Type=CONST(Item)) Item
                                                                 ELSE IF (Type=CONST(Fixed Asset)) "Fixed Asset"
                                                                 ELSE IF (Type=CONST("Charge (Item)")) "Item Charge";
                                                   CaptionML=[ENU=No.;
                                                              PTG=N�] }
    { 7   ;   ;Location Code       ;Code10        ;TableRelation=Location WHERE (Use As In-Transit=CONST(No));
                                                   CaptionML=[ENU=Location Code;
                                                              PTG=C�d. Localiza��o] }
    { 8   ;   ;Posting Group       ;Code10        ;TableRelation=IF (Type=CONST(Item)) "Inventory Posting Group"
                                                                 ELSE IF (Type=CONST(Fixed Asset)) "FA Posting Group";
                                                   CaptionML=[ENU=Posting Group;
                                                              PTG=Gr. Contabil�stico];
                                                   Editable=No }
    { 11  ;   ;Description         ;Text50        ;CaptionML=[ENU=Description;
                                                              PTG=Descri��o] }
    { 12  ;   ;Description 2       ;Text50        ;CaptionML=[ENU=Description 2;
                                                              PTG=Descri��o 2] }
    { 13  ;   ;Unit of Measure     ;Text10        ;CaptionML=[ENU=Unit of Measure;
                                                              PTG=Unidade Medida] }
    { 15  ;   ;Quantity            ;Decimal       ;CaptionML=[ENU=Quantity;
                                                              PTG=Quantidade];
                                                   DecimalPlaces=0:5 }
    { 22  ;   ;Direct Unit Cost    ;Decimal       ;CaptionML=[ENU=Direct Unit Cost;
                                                              PTG=Custo Unit�rio Direto];
                                                   AutoFormatType=2;
                                                   AutoFormatExpr=GetCurrencyCode }
    { 23  ;   ;Unit Cost (LCY)     ;Decimal       ;CaptionML=[ENU=Unit Cost (LCY);
                                                              PTG=Custo Unit�rio (DL)];
                                                   AutoFormatType=2 }
    { 25  ;   ;VAT %               ;Decimal       ;CaptionML=[ENU=VAT %;
                                                              PTG=% IVA];
                                                   DecimalPlaces=0:5;
                                                   Editable=No }
    { 27  ;   ;Line Discount %     ;Decimal       ;CaptionML=[ENU=Line Discount %;
                                                              PTG=% Desconto Linha];
                                                   DecimalPlaces=0:5;
                                                   MinValue=0;
                                                   MaxValue=100 }
    { 31  ;   ;Unit Price (LCY)    ;Decimal       ;CaptionML=[ENU=Unit Price (LCY);
                                                              PTG=Pre�o Unit�rio (DL)];
                                                   AutoFormatType=2 }
    { 32  ;   ;Allow Invoice Disc. ;Boolean       ;InitValue=Yes;
                                                   CaptionML=[ENU=Allow Invoice Disc.;
                                                              PTG=Permite Desconto Fatura] }
    { 34  ;   ;Gross Weight        ;Decimal       ;CaptionML=[ENU=Gross Weight;
                                                              PTG=Peso Bruto];
                                                   DecimalPlaces=0:5 }
    { 35  ;   ;Net Weight          ;Decimal       ;CaptionML=[ENU=Net Weight;
                                                              PTG=Peso L�quido];
                                                   DecimalPlaces=0:5 }
    { 36  ;   ;Units per Parcel    ;Decimal       ;CaptionML=[ENU=Units per Parcel;
                                                              PTG=Unidades por Lote];
                                                   DecimalPlaces=0:5 }
    { 37  ;   ;Unit Volume         ;Decimal       ;CaptionML=[ENU=Unit Volume;
                                                              PTG=Unidade Volume];
                                                   DecimalPlaces=0:5 }
    { 38  ;   ;Appl.-to Item Entry ;Integer       ;AccessByPermission=TableData 27=R;
                                                   CaptionML=[ENU=Appl.-to Item Entry;
                                                              PTG=Liq. por N� Mov. Produto] }
    { 39  ;   ;Item Shpt. Entry No.;Integer       ;CaptionML=[ENU=Item Shpt. Entry No.;
                                                              PTG=N� Mov. Produto Assoc.] }
    { 40  ;   ;Shortcut Dimension 1 Code;Code20   ;TableRelation="Dimension Value".Code WHERE (Global Dimension No.=CONST(1));
                                                   CaptionML=[ENU=Shortcut Dimension 1 Code;
                                                              PTG=C�d. Atalho Dimens�o 1];
                                                   CaptionClass='1,2,1' }
    { 41  ;   ;Shortcut Dimension 2 Code;Code20   ;TableRelation="Dimension Value".Code WHERE (Global Dimension No.=CONST(2));
                                                   CaptionML=[ENU=Shortcut Dimension 2 Code;
                                                              PTG=C�d. Atalho Dimens�o 2];
                                                   CaptionClass='1,2,2' }
    { 45  ;   ;Job No.             ;Code20        ;TableRelation=Job;
                                                   CaptionML=[ENU=Job No.;
                                                              PTG=N� Projeto] }
    { 54  ;   ;Indirect Cost %     ;Decimal       ;CaptionML=[ENU=Indirect Cost %;
                                                              PTG=% Custo Indireto];
                                                   DecimalPlaces=0:5;
                                                   MinValue=0 }
    { 61  ;   ;Quantity Invoiced   ;Decimal       ;CaptionML=[ENU=Quantity Invoiced;
                                                              PTG=Qtd. Faturada];
                                                   DecimalPlaces=0:5;
                                                   Editable=No }
    { 68  ;   ;Pay-to Vendor No.   ;Code20        ;TableRelation=Vendor;
                                                   CaptionML=[ENU=Pay-to Vendor No.;
                                                              PTG=Pagto.-a N� Fornecedor];
                                                   Editable=No }
    { 70  ;   ;Vendor Item No.     ;Text20        ;CaptionML=[ENU=Vendor Item No.;
                                                              PTG=C�d. Produto Fornecedor] }
    { 74  ;   ;Gen. Bus. Posting Group;Code10     ;TableRelation="Gen. Business Posting Group";
                                                   CaptionML=[ENU=Gen. Bus. Posting Group;
                                                              PTG=Gr. Contabil�stico Neg�cio] }
    { 75  ;   ;Gen. Prod. Posting Group;Code10    ;TableRelation="Gen. Product Posting Group";
                                                   CaptionML=[ENU=Gen. Prod. Posting Group;
                                                              PTG=Gr. Contabil�stico Produto] }
    { 77  ;   ;VAT Calculation Type;Option        ;CaptionML=[ENU=VAT Calculation Type;
                                                              PTG=Tipo C�lculo IVA];
                                                   OptionCaptionML=[ENU=Normal VAT,Reverse Charge VAT,Full VAT,Sales Tax,,,,,,No Taxable VAT,Stamp Duty;
                                                                    PTG=IVA Normal,IVA Conta Autoliquida��o,IVA Total,Impostos Vendas,,,,,,N�o Sujeito,Imposto Selo];
                                                   OptionString=Normal VAT,Reverse Charge VAT,Full VAT,Sales Tax,,,,,,No Taxable VAT,Stamp Duty;
                                                   Description=V93.00#00023 }
    { 78  ;   ;Transaction Type    ;Code10        ;TableRelation="Transaction Type";
                                                   CaptionML=[ENU=Transaction Type;
                                                              PTG=Natureza Transa��o] }
    { 79  ;   ;Transport Method    ;Code10        ;TableRelation="Transport Method";
                                                   CaptionML=[ENU=Transport Method;
                                                              PTG=M�todo Transporte] }
    { 80  ;   ;Attached to Line No.;Integer       ;TableRelation="Return Shipment Line"."Line No." WHERE (Document No.=FIELD(Document No.));
                                                   CaptionML=[ENU=Attached to Line No.;
                                                              PTG=Ligado � Linha N�] }
    { 81  ;   ;Entry Point         ;Code10        ;TableRelation="Entry/Exit Point";
                                                   CaptionML=[ENU=Entry Point;
                                                              PTG=Porto/Aeroporto Entrada] }
    { 82  ;   ;Area                ;Code10        ;TableRelation=Area;
                                                   CaptionML=[ENU=Area;
                                                              PTG=�rea] }
    { 83  ;   ;Transaction Specification;Code10   ;TableRelation="Transaction Specification";
                                                   CaptionML=[ENU=Transaction Specification;
                                                              PTG=Especifica��o Transa��o] }
    { 85  ;   ;Tax Area Code       ;Code20        ;TableRelation="Tax Area";
                                                   CaptionML=[ENU=Tax Area Code;
                                                              PTG=C�d. �rea Imposto] }
    { 86  ;   ;Tax Liable          ;Boolean       ;CaptionML=[ENU=Tax Liable;
                                                              PTG=Sujeito a Imposto] }
    { 87  ;   ;Tax Group Code      ;Code10        ;TableRelation="Tax Group";
                                                   CaptionML=[ENU=Tax Group Code;
                                                              PTG=C�d. Grupo Imposto] }
    { 88  ;   ;Use Tax             ;Boolean       ;CaptionML=[ENU=Use Tax;
                                                              PTG=Utiliza Imposto] }
    { 89  ;   ;VAT Bus. Posting Group;Code10      ;TableRelation="VAT Business Posting Group";
                                                   CaptionML=[ENU=VAT Bus. Posting Group;
                                                              PTG=Gr. Registo IVA Neg�cio] }
    { 90  ;   ;VAT Prod. Posting Group;Code10     ;TableRelation="VAT Product Posting Group";
                                                   CaptionML=[ENU=VAT Prod. Posting Group;
                                                              PTG=Gr. Registo IVA Produto] }
    { 91  ;   ;Currency Code       ;Code10        ;FieldClass=FlowField;
                                                   CalcFormula=Lookup("Return Shipment Header"."Currency Code" WHERE (No.=FIELD(Document No.)));
                                                   CaptionML=[ENU=Currency Code;
                                                              PTG=C�d. Divisa];
                                                   Editable=No }
    { 97  ;   ;Blanket Order No.   ;Code20        ;TableRelation="Sales Header".No. WHERE (Document Type=CONST(Blanket Order));
                                                   TestTableRelation=No;
                                                   AccessByPermission=TableData 6650=R;
                                                   CaptionML=[ENU=Blanket Order No.;
                                                              PTG=N� Encomenda Aberta] }
    { 98  ;   ;Blanket Order Line No.;Integer     ;TableRelation="Sales Line"."Line No." WHERE (Document Type=CONST(Blanket Order),
                                                                                                Document No.=FIELD(Blanket Order No.));
                                                   TestTableRelation=No;
                                                   AccessByPermission=TableData 6650=R;
                                                   CaptionML=[ENU=Blanket Order Line No.;
                                                              PTG=N� Linha Enc. Aberta] }
    { 99  ;   ;VAT Base Amount     ;Decimal       ;CaptionML=[ENU=VAT Base Amount;
                                                              PTG=Valor Base IVA];
                                                   Editable=No;
                                                   AutoFormatType=1;
                                                   AutoFormatExpr=GetCurrencyCode }
    { 100 ;   ;Unit Cost           ;Decimal       ;CaptionML=[ENU=Unit Cost;
                                                              PTG=Custo Unit�rio];
                                                   Editable=No;
                                                   AutoFormatType=2;
                                                   AutoFormatExpr=GetCurrencyCode }
    { 131 ;   ;Posting Date        ;Date          ;CaptionML=[ENU=Posting Date;
                                                              PTG=Data Registo] }
    { 480 ;   ;Dimension Set ID    ;Integer       ;TableRelation="Dimension Set Entry";
                                                   OnLookup=BEGIN
                                                              ShowDimensions;
                                                            END;

                                                   CaptionML=[ENU=Dimension Set ID;
                                                              PTG=ID Combina��o Dimens�o];
                                                   Editable=No }
    { 1001;   ;Job Task No.        ;Code20        ;TableRelation="Job Task"."Job Task No." WHERE (Job No.=FIELD(Job No.));
                                                   CaptionML=[ENU=Job Task No.;
                                                              PTG=N� Tarefa Projeto] }
    { 5401;   ;Prod. Order No.     ;Code20        ;CaptionML=[ENU=Prod. Order No.;
                                                              PTG=N� Ordem Produ��o] }
    { 5402;   ;Variant Code        ;Code10        ;TableRelation=IF (Type=CONST(Item)) "Item Variant".Code WHERE (Item No.=FIELD(No.));
                                                   CaptionML=[ENU=Variant Code;
                                                              PTG=C�d. Variante] }
    { 5403;   ;Bin Code            ;Code20        ;TableRelation=Bin.Code WHERE (Location Code=FIELD(Location Code),
                                                                                 Item Filter=FIELD(No.),
                                                                                 Variant Filter=FIELD(Variant Code));
                                                   CaptionML=[ENU=Bin Code;
                                                              PTG=C�d. Posi��o] }
    { 5404;   ;Qty. per Unit of Measure;Decimal   ;CaptionML=[ENU=Qty. per Unit of Measure;
                                                              PTG=Qtd. por Unidade Medida];
                                                   DecimalPlaces=0:5;
                                                   Editable=No }
    { 5407;   ;Unit of Measure Code;Code10        ;TableRelation=IF (Type=CONST(Item)) "Item Unit of Measure".Code WHERE (Item No.=FIELD(No.))
                                                                 ELSE "Unit of Measure";
                                                   CaptionML=[ENU=Unit of Measure Code;
                                                              PTG=C�d. Unidade Medida] }
    { 5415;   ;Quantity (Base)     ;Decimal       ;CaptionML=[ENU=Quantity (Base);
                                                              PTG=Quantidade (Base)];
                                                   DecimalPlaces=0:5 }
    { 5461;   ;Qty. Invoiced (Base);Decimal       ;CaptionML=[ENU=Qty. Invoiced (Base);
                                                              PTG=Qtd. Faturada (Base)];
                                                   DecimalPlaces=0:5;
                                                   Editable=No }
    { 5600;   ;FA Posting Date     ;Date          ;CaptionML=[ENU=FA Posting Date;
                                                              PTG=Data Registo Imob.] }
    { 5601;   ;FA Posting Type     ;Option        ;CaptionML=[ENU=FA Posting Type;
                                                              PTG=Tipo Registo Imob.];
                                                   OptionCaptionML=[ENU=" ,Acquisition Cost,Maintenance";
                                                                    PTG=" ,Custo,Manuten��o"];
                                                   OptionString=[ ,Acquisition Cost,Maintenance] }
    { 5602;   ;Depreciation Book Code;Code10      ;TableRelation="Depreciation Book";
                                                   CaptionML=[ENU=Depreciation Book Code;
                                                              PTG=C�d. Livro Amortiza��o] }
    { 5603;   ;Salvage Value       ;Decimal       ;CaptionML=[ENU=Salvage Value;
                                                              PTG=Valor Residual];
                                                   AutoFormatType=1 }
    { 5605;   ;Depr. until FA Posting Date;Boolean;CaptionML=[ENU=Depr. until FA Posting Date;
                                                              PTG=Amort. At� Data Registo Imob.] }
    { 5606;   ;Depr. Acquisition Cost;Boolean     ;CaptionML=[ENU=Depr. Acquisition Cost;
                                                              PTG=Amortiza��o At� Custo] }
    { 5609;   ;Maintenance Code    ;Code10        ;TableRelation=Maintenance;
                                                   CaptionML=[ENU=Maintenance Code;
                                                              PTG=C�d. Manuten��o] }
    { 5610;   ;Insurance No.       ;Code20        ;TableRelation=Insurance;
                                                   CaptionML=[ENU=Insurance No.;
                                                              PTG=N� Seguro] }
    { 5611;   ;Budgeted FA No.     ;Code20        ;TableRelation="Fixed Asset";
                                                   CaptionML=[ENU=Budgeted FA No.;
                                                              PTG=N� Imobilizado Or�amentado] }
    { 5612;   ;Duplicate in Depreciation Book;Code10;
                                                   TableRelation="Depreciation Book";
                                                   CaptionML=[ENU=Duplicate in Depreciation Book;
                                                              PTG=Duplicado em Livro Amortiza��o] }
    { 5613;   ;Use Duplication List;Boolean       ;CaptionML=[ENU=Use Duplication List;
                                                              PTG=Usa Lista Duplicados] }
    { 5700;   ;Responsibility Center;Code10       ;TableRelation="Responsibility Center";
                                                   ValidateTableRelation=Yes;
                                                   CaptionML=[ENU=Responsibility Center;
                                                              PTG=Centro Responsabilidade] }
    { 5705;   ;Cross-Reference No. ;Code20        ;AccessByPermission=TableData 5717=R;
                                                   CaptionML=[ENU=Cross-Reference No.;
                                                              PTG=N� Ref. Cruzada] }
    { 5706;   ;Unit of Measure (Cross Ref.);Code10;TableRelation=IF (Type=CONST(Item)) "Item Unit of Measure".Code WHERE (Item No.=FIELD(No.));
                                                   CaptionML=[ENU=Unit of Measure (Cross Ref.);
                                                              PTG=Unid. Medida (Ref. Cruzada)] }
    { 5707;   ;Cross-Reference Type;Option        ;CaptionML=[ENU=Cross-Reference Type;
                                                              PTG=Tipo Ref. Cruzada];
                                                   OptionCaptionML=[ENU=" ,Customer,Vendor,Bar Code";
                                                                    PTG=" ,Cliente,Fornecedor,C�d. Barras"];
                                                   OptionString=[ ,Customer,Vendor,Bar Code] }
    { 5708;   ;Cross-Reference Type No.;Code30    ;CaptionML=[ENU=Cross-Reference Type No.;
                                                              PTG=N� Tipo Ref. Cruzada] }
    { 5709;   ;Item Category Code  ;Code20        ;TableRelation=IF (Type=CONST(Item)) "Item Category";
                                                   CaptionML=[ENU=Item Category Code;
                                                              PTG=C�d. Categoria Produto] }
    { 5710;   ;Nonstock            ;Boolean       ;CaptionML=[ENU=Nonstock;
                                                              PTG=N�o Armazenado] }
    { 5711;   ;Purchasing Code     ;Code10        ;TableRelation=Purchasing;
                                                   CaptionML=[ENU=Purchasing Code;
                                                              PTG=C�d. Compra] }
    { 5712;   ;Product Group Code  ;Code10        ;TableRelation="Product Group".Code WHERE (Item Category Code=FIELD(Item Category Code));
                                                   CaptionML=[ENU=Product Group Code;
                                                              PTG=C�d. Grupo Produto] }
    { 5805;   ;Return Qty. Shipped Not Invd.;Decimal;
                                                   CaptionML=[ENU=Return Qty. Shipped Not Invd.;
                                                              PTG=Qtd. Enviada Dev. N�o Fatura];
                                                   DecimalPlaces=0:5;
                                                   Editable=No }
    { 5811;   ;Item Charge Base Amount;Decimal    ;CaptionML=[ENU=Item Charge Base Amount;
                                                              PTG=Valor Base Encargo Produto];
                                                   AutoFormatType=1;
                                                   AutoFormatExpr=GetCurrencyCode }
    { 5817;   ;Correction          ;Boolean       ;CaptionML=[ENU=Correction;
                                                              PTG=Corre��o];
                                                   Editable=No }
    { 6602;   ;Return Order No.    ;Code20        ;CaptionML=[ENU=Return Order No.;
                                                              PTG=N� Devolu��o];
                                                   Editable=No }
    { 6603;   ;Return Order Line No.;Integer      ;CaptionML=[ENU=Return Order Line No.;
                                                              PTG=N� Linha Devolu��o];
                                                   Editable=No }
    { 6608;   ;Return Reason Code  ;Code10        ;TableRelation="Return Reason";
                                                   CaptionML=[ENU=Return Reason Code;
                                                              PTG=C�d. Raz�o Devolu��o] }
  }
  KEYS
  {
    {    ;Document No.,Line No.                   ;Clustered=Yes }
    {    ;Return Order No.,Return Order Line No.   }
    {    ;Blanket Order No.,Blanket Order Line No. }
    {    ;Pay-to Vendor No.                        }
    {    ;Buy-from Vendor No.                      }
    {    ;Posting Date,Type,Quantity              ;SumIndexFields=Quantity }
  }
  FIELDGROUPS
  {
  }
  CODE
  {
    VAR
      Currency@1004 : Record 4;
      ReturnShptHeader@1000 : Record 6650;
      Text000@1003 : TextConst 'ENU=Return Shipment No. %1:;PTG=Envio Devolu��o N� %1:';
      Text001@1002 : TextConst 'ENU=The program cannot find this purchase line.;PTG=O programa n�o encontra esta linha compra.';
      CurrencyRead@1005 : Boolean;

    PROCEDURE GetCurrencyCode@1() : Code[10];
    BEGIN
      IF "Document No." = ReturnShptHeader."No." THEN
        EXIT(ReturnShptHeader."Currency Code");
      IF ReturnShptHeader.GET("Document No.") THEN
        EXIT(ReturnShptHeader."Currency Code");
      EXIT('');
    END;

    PROCEDURE ShowDimensions@25();
    VAR
      DimMgt@1002 : Codeunit 408;
    BEGIN
      DimMgt.ShowDimensionSet("Dimension Set ID",
        STRSUBSTNO('%1 %2 %3',TABLECAPTION,"Document No.","Line No."));
    END;

    PROCEDURE ShowItemTrackingLines@3();
    VAR
      ItemTrackingDocMgt@1000 : Codeunit 6503;
    BEGIN
      ItemTrackingDocMgt.ShowItemTrackingForShptRcptLine(DATABASE::"Return Shipment Line",0,"Document No.",'',0,"Line No.");
    END;

    PROCEDURE InsertInvLineFromRetShptLine@2(VAR PurchLine@1000 : Record 39);
    VAR
      PurchHeader@1007 : Record 38;
      PurchHeader2@1008 : Record 38;
      PurchOrderLine@1005 : Record 39;
      TempPurchLine@1003 : TEMPORARY Record 39;
      PurchSetup@1020 : Record 312;
      TransferOldExtLines@1002 : Codeunit 379;
      ItemTrackingMgt@1004 : Codeunit 6500;
      NextLineNo@1001 : Integer;
      ExtTextLine@1006 : Boolean;
    BEGIN
      SETRANGE("Document No.","Document No.");

      TempPurchLine := PurchLine;
      IF PurchLine.FIND('+') THEN
        NextLineNo := PurchLine."Line No." + 10000
      ELSE
        NextLineNo := 10000;

      IF PurchHeader."No." <> TempPurchLine."Document No." THEN
        PurchHeader.GET(TempPurchLine."Document Type",TempPurchLine."Document No.");

      IF PurchLine."Return Shipment No." <> "Document No." THEN BEGIN
        PurchLine.INIT;
        PurchLine."Line No." := NextLineNo;
        PurchLine."Document Type" := TempPurchLine."Document Type";
        PurchLine."Document No." := TempPurchLine."Document No.";
        PurchLine.Description := STRSUBSTNO(Text000,"Document No.");
        PurchLine.INSERT;
        NextLineNo := NextLineNo + 10000;
      END;

      TransferOldExtLines.ClearLineNumbers;
      PurchSetup.GET;
      REPEAT
        ExtTextLine := (TransferOldExtLines.GetNewLineNumber("Attached to Line No.") <> 0);

        IF NOT PurchOrderLine.GET(
             PurchOrderLine."Document Type"::"Return Order","Return Order No.","Return Order Line No.")
        THEN BEGIN
          IF ExtTextLine THEN BEGIN
            PurchOrderLine.INIT;
            PurchOrderLine."Line No." := "Return Order Line No.";
            PurchOrderLine.Description := Description;
            PurchOrderLine."Description 2" := "Description 2";
          END ELSE
            ERROR(Text001);
        END ELSE BEGIN
          IF (PurchHeader2."Document Type" <> PurchOrderLine."Document Type"::"Return Order") OR
             (PurchHeader2."No." <> PurchOrderLine."Document No.")
          THEN
            PurchHeader2.GET(PurchOrderLine."Document Type"::"Return Order","Return Order No.");

          InitCurrency("Currency Code");

          IF PurchHeader."Prices Including VAT" THEN BEGIN
            IF NOT PurchHeader2."Prices Including VAT" THEN
              PurchOrderLine."Direct Unit Cost" :=
                ROUND(
                  PurchOrderLine."Direct Unit Cost" * (1 + PurchOrderLine."VAT %" / 100),
                  Currency."Unit-Amount Rounding Precision");
          END ELSE BEGIN
            IF PurchHeader2."Prices Including VAT" THEN
              PurchOrderLine."Direct Unit Cost" :=
                ROUND(
                  PurchOrderLine."Direct Unit Cost" / (1 + PurchOrderLine."VAT %" / 100),
                  Currency."Unit-Amount Rounding Precision");
          END;
        END;
        PurchLine := PurchOrderLine;
        PurchLine."Line No." := NextLineNo;
        PurchLine."Document Type" := TempPurchLine."Document Type";
        PurchLine."Document No." := TempPurchLine."Document No.";
        PurchLine."Variant Code" := "Variant Code";
        PurchLine."Location Code" := "Location Code";
        PurchLine."Return Reason Code" := "Return Reason Code";
        PurchLine."Quantity (Base)" := 0;
        PurchLine.Quantity := 0;
        PurchLine."Outstanding Qty. (Base)" := 0;
        PurchLine."Outstanding Quantity" := 0;
        PurchLine."Return Qty. Shipped" := 0;
        PurchLine."Return Qty. Shipped (Base)" := 0;
        PurchLine."Quantity Invoiced" := 0;
        PurchLine."Qty. Invoiced (Base)" := 0;
        PurchLine."Sales Order No." := '';
        PurchLine."Sales Order Line No." := 0;
        PurchLine."Drop Shipment" := FALSE;
        PurchLine."Return Shipment No." := "Document No.";
        PurchLine."Return Shipment Line No." := "Line No.";
        PurchLine."Appl.-to Item Entry" := 0;

        IF NOT ExtTextLine THEN BEGIN
          PurchLine.VALIDATE(Quantity,Quantity - "Quantity Invoiced");
          PurchLine.VALIDATE("Direct Unit Cost",PurchOrderLine."Direct Unit Cost");
          PurchLine.VALIDATE("Line Discount %",PurchOrderLine."Line Discount %");
          IF PurchOrderLine.Quantity = 0 THEN
            PurchLine.VALIDATE("Inv. Discount Amount",0)
          ELSE
            PurchLine.VALIDATE(
              "Inv. Discount Amount",
              ROUND(
                PurchOrderLine."Inv. Discount Amount" * PurchLine.Quantity / PurchOrderLine.Quantity,
                Currency."Amount Rounding Precision"));
        END;
        PurchLine."Attached to Line No." :=
          TransferOldExtLines.TransferExtendedText(
            "Line No.",
            NextLineNo,
            "Attached to Line No.");
        PurchLine."Shortcut Dimension 1 Code" := PurchOrderLine."Shortcut Dimension 1 Code";
        PurchLine."Shortcut Dimension 2 Code" := PurchOrderLine."Shortcut Dimension 2 Code";
        PurchLine."Dimension Set ID" := PurchOrderLine."Dimension Set ID";
        PurchLine.INSERT;

        ItemTrackingMgt.CopyHandledItemTrkgToInvLine2(PurchOrderLine,PurchLine);

        NextLineNo := NextLineNo + 10000;
        IF "Attached to Line No." = 0 THEN
          SETRANGE("Attached to Line No.","Line No.");
      UNTIL (NEXT = 0) OR ("Attached to Line No." = 0);
    END;

    LOCAL PROCEDURE GetPurchCrMemoLines@4(VAR TempPurchCrMemoLine@1000 : TEMPORARY Record 125);
    VAR
      PurchCrMemoLine@1003 : Record 125;
      ItemLedgEntry@1002 : Record 32;
      ValueEntry@1001 : Record 5802;
    BEGIN
      TempPurchCrMemoLine.RESET;
      TempPurchCrMemoLine.DELETEALL;

      IF Type <> Type::Item THEN
        EXIT;

      FilterPstdDocLnItemLedgEntries(ItemLedgEntry);
      ItemLedgEntry.SETFILTER("Invoiced Quantity",'<>0');
      IF ItemLedgEntry.FINDSET THEN BEGIN
        ValueEntry.SETCURRENTKEY("Item Ledger Entry No.","Entry Type");
        ValueEntry.SETRANGE("Entry Type",ValueEntry."Entry Type"::"Direct Cost");
        ValueEntry.SETFILTER("Invoiced Quantity",'<>0');
        REPEAT
          ValueEntry.SETRANGE("Item Ledger Entry No.",ItemLedgEntry."Entry No.");
          IF ValueEntry.FINDSET THEN
            REPEAT
              IF ValueEntry."Document Type" = ValueEntry."Document Type"::"Purchase Credit Memo" THEN
                IF PurchCrMemoLine.GET(ValueEntry."Document No.",ValueEntry."Document Line No.") THEN BEGIN
                  TempPurchCrMemoLine.INIT;
                  TempPurchCrMemoLine := PurchCrMemoLine;
                  IF TempPurchCrMemoLine.INSERT THEN;
                END;
            UNTIL ValueEntry.NEXT = 0;
        UNTIL ItemLedgEntry.NEXT = 0;
      END;
    END;

    PROCEDURE FilterPstdDocLnItemLedgEntries@6(VAR ItemLedgEntry@1000 : Record 32);
    BEGIN
      ItemLedgEntry.RESET;
      ItemLedgEntry.SETCURRENTKEY("Document No.");
      ItemLedgEntry.SETRANGE("Document No.","Document No.");
      ItemLedgEntry.SETRANGE("Document Type",ItemLedgEntry."Document Type"::"Purchase Return Shipment");
      ItemLedgEntry.SETRANGE("Document Line No.","Line No.");
    END;

    PROCEDURE ShowItemPurchCrMemoLines@9();
    VAR
      TempPurchCrMemoLine@1000 : TEMPORARY Record 125;
    BEGIN
      IF Type = Type::Item THEN BEGIN
        GetPurchCrMemoLines(TempPurchCrMemoLine);
        PAGE.RUNMODAL(0,TempPurchCrMemoLine);
      END;
    END;

    LOCAL PROCEDURE InitCurrency@8(CurrencyCode@1001 : Code[10]);
    BEGIN
      IF (Currency.Code = CurrencyCode) AND CurrencyRead THEN
        EXIT;

      IF CurrencyCode <> '' THEN
        Currency.GET(CurrencyCode)
      ELSE
        Currency.InitRoundingPrecision;
      CurrencyRead := TRUE;
    END;

    PROCEDURE ShowLineComments@5();
    VAR
      PurchDocLineComments@1000 : Record 43;
      PurchDocCommentSheet@1001 : Page 66;
    BEGIN
      PurchDocLineComments.SETRANGE("Document Type",PurchDocLineComments."Document Type"::"Posted Return Shipment");
      PurchDocLineComments.SETRANGE("No.","Document No.");
      PurchDocLineComments.SETRANGE("Document Line No.","Line No.");
      PurchDocCommentSheet.SETTABLEVIEW(PurchDocLineComments);
      PurchDocCommentSheet.RUNMODAL;
    END;

    PROCEDURE InitFromPurchLine@10(ReturnShptHeader@1001 : Record 6650;PurchLine@1002 : Record 39);
    BEGIN
      INIT;
      TRANSFERFIELDS(PurchLine);
      IF ("No." = '') AND (Type IN [Type::"G/L Account"..Type::"Charge (Item)"]) THEN
        Type := Type::" ";
      "Posting Date" := ReturnShptHeader."Posting Date";
      "Document No." := ReturnShptHeader."No.";
      Quantity := PurchLine."Return Qty. to Ship";
      "Quantity (Base)" := PurchLine."Return Qty. to Ship (Base)";
      IF ABS(PurchLine."Qty. to Invoice") > ABS(PurchLine."Return Qty. to Ship") THEN BEGIN
        "Quantity Invoiced" := PurchLine."Return Qty. to Ship";
        "Qty. Invoiced (Base)" := PurchLine."Return Qty. to Ship (Base)";
      END ELSE BEGIN
        "Quantity Invoiced" := PurchLine."Qty. to Invoice";
        "Qty. Invoiced (Base)" := PurchLine."Qty. to Invoice (Base)";
      END;
      "Return Qty. Shipped Not Invd." := Quantity - "Quantity Invoiced";
      IF PurchLine."Document Type" = PurchLine."Document Type"::"Return Order" THEN BEGIN
        "Return Order No." := PurchLine."Document No.";
        "Return Order Line No." := PurchLine."Line No.";
      END;
    END;

    BEGIN
    {
      V92.00#00002 - Tipo C�lculo IVA corrigir op��o "N�o Sujeito" - EFD - 04.02.2016
      V93.00#00023 - Imposto Selo - SAFT - IRC - 2016.03.31
    }
    END.
  }
}

