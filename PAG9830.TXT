OBJECT Page 9830 User Groups
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0;
  }
  PROPERTIES
  {
    CaptionML=[ENU=User Groups;
               PTG=Grupos Utilizador];
    SourceTable=Table9000;
    DataCaptionFields=Code,Name;
    PageType=List;
    OnOpenPage=VAR
                 PermissionManager@1000 : Codeunit 9002;
               BEGIN
                 SoftwareAsAService := PermissionManager.SoftwareAsAService;
               END;

    ActionList=ACTIONS
    {
      { 9       ;    ;ActionContainer;
                      ActionContainerType=RelatedInformation }
      { 10      ;1   ;Action    ;
                      Name=UserGroupMembers;
                      CaptionML=[ENU=User Group Members;
                                 PTG=Membros Grupo Utilizador];
                      ToolTipML=[ENU=View or edit the members of the user group.;
                                 PTG=""];
                      ApplicationArea=#Basic,#Suite;
                      RunObject=Page 9831;
                      RunPageLink=User Group Code=FIELD(Code);
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=Users;
                      PromotedCategory=Process;
                      Scope=Repeater }
      { 13      ;1   ;Action    ;
                      Name=UserGroupPermissionSets;
                      CaptionML=[ENU=User Group Permission Sets;
                                 PTG=Conjuntos Permiss�o Grupo Utilizador];
                      ToolTipML=[ENU=View or edit the permission sets that are assigned to the user group.;
                                 PTG=""];
                      ApplicationArea=#Basic,#Suite;
                      RunObject=Page 9834;
                      RunPageLink=User Group Code=FIELD(Code);
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=Permission;
                      PromotedCategory=Process;
                      Scope=Repeater }
      { 15      ;1   ;Action    ;
                      Name=PageUserbyUserGroup;
                      CaptionML=[ENU=User by User Group;
                                 PTG=Utilizador por Grupo Utilizador];
                      ToolTipML=[ENU=View and assign user groups to users.;
                                 PTG=""];
                      ApplicationArea=#Basic,#Suite;
                      RunObject=Page 9838;
                      Promoted=Yes;
                      Image=User;
                      PromotedCategory=Process }
      { 14      ;1   ;Action    ;
                      Name=PagePermissionSetbyUserGroup;
                      CaptionML=[ENU=Permission Set by User Group;
                                 PTG=Conjunto Permiss�o por Grupo Utilizador];
                      ToolTipML=[ENU=View or edit the available permission sets and apply permission sets to existing user groups.;
                                 PTG=""];
                      ApplicationArea=#Basic,#Suite;
                      RunObject=Page 9837;
                      Promoted=Yes;
                      Image=Permission;
                      PromotedCategory=Process }
      { 16      ;0   ;ActionContainer;
                      ActionContainerType=ActionItems }
      { 19      ;1   ;Action    ;
                      Name=CopyUserGroup;
                      Ellipsis=Yes;
                      CaptionML=[ENU=Copy User Group;
                                 PTG=Copiar Grupos Utilizador];
                      ToolTipML=[ENU=Create a copy of the current user group with a name that you specify.;
                                 PTG=""];
                      ApplicationArea=#Basic,#Suite;
                      Promoted=Yes;
                      Image=Copy;
                      OnAction=VAR
                                 UserGroup@1035 : Record 9000;
                               BEGIN
                                 UserGroup.SETRANGE(Code,Code);
                                 REPORT.RUNMODAL(REPORT::"Copy User Group",TRUE,FALSE,UserGroup);
                               END;
                                }
      { 17      ;1   ;Action    ;
                      Name=ExportUserGroups;
                      CaptionML=[ENU=Export User Groups;
                                 PTG=Exportar Grupos Utilizador];
                      ToolTipML=[ENU=Export the existing user groups to an XML file.;
                                 PTG=""];
                      ApplicationArea=#Basic,#Suite;
                      Image=ExportFile;
                      OnAction=BEGIN
                                 ExportUserGroups('');
                               END;
                                }
      { 18      ;1   ;Action    ;
                      Name=ImportUserGroups;
                      CaptionML=[ENU=Import User Groups;
                                 PTG=Importar Grupos Utilizador];
                      ToolTipML=[ENU=Import user groups from an XML file.;
                                 PTG=""];
                      ApplicationArea=#Basic,#Suite;
                      Image=Import;
                      OnAction=BEGIN
                                 ImportUserGroups('');
                               END;
                                }
    }
  }
  CONTROLS
  {
    { 1   ;0   ;Container ;
                ContainerType=ContentArea }

    { 2   ;1   ;Group     ;
                Name=Group;
                GroupType=Repeater }

    { 3   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the code of the record.;
                           PTG=""];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=Code }

    { 4   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the name of the record.;
                           PTG=""];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=Name }

    { 20  ;2   ;Field     ;
                CaptionML=[ENU=Default Profile;
                           PTG=Perfil Pr�-definido];
                ToolTipML=[ENU=Specifies the default profile for members in this user group. The profile determines the layout of the home page.;
                           PTG=""];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Default Profile ID" }

    { 21  ;2   ;Field     ;
                CaptionML=[ENU=Assign to All New Users;
                           PTG=Atribuir a Todos os Utilizadores];
                ToolTipML=[ENU=Specifies that new users are automatically added to this user group when you import them from Office 365.;
                           PTG=""];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Assign to All New Users";
                Visible=SoftwareAsAService }

    { 5   ;0   ;Container ;
                ContainerType=FactBoxArea }

    { 12  ;1   ;Part      ;
                ApplicationArea=#Basic,#Suite;
                SubPageLink=User Group Code=FIELD(Code);
                PagePartID=Page9835;
                PartType=Page }

    { 11  ;1   ;Part      ;
                ApplicationArea=#Basic,#Suite;
                SubPageLink=User Group Code=FIELD(Code);
                PagePartID=Page9832;
                PartType=Page }

    { 6   ;1   ;Part      ;
                PartType=System;
                SystemPartID=Notes }

    { 7   ;1   ;Part      ;
                PartType=System;
                SystemPartID=MyNotes }

    { 8   ;1   ;Part      ;
                PartType=System;
                SystemPartID=RecordLinks }

  }
  CODE
  {
    VAR
      SoftwareAsAService@1000 : Boolean;

    BEGIN
    END.
  }
}

