OBJECT Page 9006 Order Processor Role Center
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Role Center;
               PTG=Centro Perfil];
    PageType=RoleCenter;
    ActionList=ACTIONS
    {
      { 1900000011;0 ;ActionContainer;
                      ToolTipML=[ENU=Manage sales processes. See KPIs and your favorite items and customers.;
                                 PTG=Gerir processos venda. Ver KPI's e os seus produtos e clientes favoritos.];
                      ActionContainerType=HomeItems }
      { 2       ;1   ;Action    ;
                      Name=SalesOrders;
                      CaptionML=[ENU=Sales Orders;
                                 PTG=Encomendas Venda];
                      ToolTipML=[ENU=Open the list of sales orders where you can sell items and services.;
                                 PTG=Abre a lista de encomendas de venda, onde pode vender  produtos e servi�os.];
                      ApplicationArea=#Basic,#Suite;
                      RunObject=Page 9305;
                      Image=Order }
      { 6       ;1   ;Action    ;
                      Name=SalesOrdersShptNotInv;
                      CaptionML=[ENU=Shipped Not Invoiced;
                                 PTG=Enviado N�o Faturado];
                      ToolTipML=[ENU=View sales that are shipped but not yet invoiced.;
                                 PTG=Ver as vendas enviadas que ainda n�o foram faturadas];
                      ApplicationArea=#Basic,#Suite;
                      RunObject=Page 9305;
                      RunPageView=WHERE(Shipped Not Invoiced=CONST(Yes)) }
      { 7       ;1   ;Action    ;
                      Name=SalesOrdersComplShtNotInv;
                      CaptionML=[ENU=Completely Shipped Not Invoiced;
                                 PTG=Totalmente Enviados e N�o Faturados];
                      ToolTipML=[ENU=View sales documents that are fully shipped but not fully invoiced.;
                                 PTG=Ver documentos de venda totalmente enviados que n�o foram faturados totalmente.];
                      ApplicationArea=#Basic,#Suite;
                      RunObject=Page 9305;
                      RunPageView=WHERE(Completely Shipped=CONST(Yes),
                                        Invoice=CONST(No)) }
      { 25      ;1   ;Action    ;
                      CaptionML=[ENU=Dynamics CRM Sales Orders;
                                 PTG=Encomendas Venda Dynamics CRM];
                      ToolTipML=[ENU=View sales orders in Dynamics CRM that are coupled with sales orders in Dynamics NAV.;
                                 PTG=Ver encomendas de venda do Dynamics CRM acoplados a pedidos de venda do Dynamics NAV.];
                      ApplicationArea=#Suite;
                      RunObject=Page 5353;
                      RunPageView=WHERE(StateCode=FILTER(Submitted),
                                        LastBackofficeSubmit=FILTER('')) }
      { 11      ;1   ;Action    ;
                      CaptionML=[ENU=Sales Quotes;
                                 PTG=Propostas Venda];
                      ToolTipML=[ENU=Open the list of sales quotes where you offer items or services to customers.;
                                 PTG=Abre a lista de propostas de vendas, onde pode propor produtos ou servi�os a clientes.];
                      ApplicationArea=#Basic,#Suite;
                      RunObject=Page 9300;
                      Image=Quote }
      { 49      ;1   ;Action    ;
                      CaptionML=[ENU=Blanket Sales Orders;
                                 PTG=Encomendas Venda Abertas];
                      RunObject=Page 9303 }
      { 50      ;1   ;Action    ;
                      CaptionML=[ENU=Sales Invoices;
                                 PTG=Faturas Venda];
                      ToolTipML=[ENU=Open the list of sales invoices where you can invoice items or services.;
                                 PTG=Abre a lista de faturas de venda, onde pode faturar produtos ou servi�os.];
                      ApplicationArea=#Basic,#Suite;
                      RunObject=Page 9301;
                      Image=Invoice }
      { 51      ;1   ;Action    ;
                      CaptionML=[ENU=Sales Return Orders;
                                 PTG=Devolu��es Venda];
                      RunObject=Page 9304;
                      Image=ReturnOrder }
      { 52      ;1   ;Action    ;
                      CaptionML=[ENU=Sales Credit Memos;
                                 PTG=Notas Cr�dito Venda];
                      ToolTipML=[ENU=Open the list of sales credit memos where you can revert posted sales invoices.;
                                 PTG=Abre a lista de notas de cr�dito de venda, onde pode anular faturas de venda registadas.];
                      ApplicationArea=#Basic,#Suite;
                      RunObject=Page 9302 }
      { 23      ;1   ;Action    ;
                      CaptionML=[ENU=Items;
                                 PTG=Produtos];
                      ToolTipML=[ENU=Open the list of items that you trade in.;
                                 PTG=Abre a lista de produtos que pode comercializar.];
                      ApplicationArea=#Basic,#Suite;
                      RunObject=Page 31;
                      Image=Item }
      { 26      ;1   ;Action    ;
                      CaptionML=[ENU=Customers;
                                 PTG=Clientes];
                      ToolTipML=[ENU=Open the list of customers.;
                                 PTG=Abre a lista de clientes.];
                      ApplicationArea=#Basic,#Suite;
                      RunObject=Page 22;
                      Image=Customer }
      { 3       ;1   ;Action    ;
                      CaptionML=[ENU=Item Journals;
                                 PTG=Di�rios Produto];
                      ToolTipML=[ENU=Open a list of journals where you can adjust the physical quantity of items on inventory.;
                                 PTG=Abrir uma lista de di�rios, onde pode ajustar a quantidade f�sica de produtos em invent�rio.];
                      ApplicationArea=#Basic,#Suite;
                      RunObject=Page 262;
                      RunPageView=WHERE(Template Type=CONST(Item),
                                        Recurring=CONST(No)) }
      { 5       ;1   ;Action    ;
                      Name=SalesJournals;
                      CaptionML=[ENU=Sales Journals;
                                 PTG=Di�rios Vendas];
                      ToolTipML=[ENU=Open the list of sales journals where you can batch post sales transactions to G/L, bank, customer, vendor and fixed assets accounts.;
                                 PTG=Abre a lista de di�rios de venda, onde pode registar por lotes as transa��es de venda nas contas da contabilidada, bancos, clientes, fornecedores e imobilizado.];
                      ApplicationArea=#Basic,#Suite;
                      RunObject=Page 251;
                      RunPageView=WHERE(Template Type=CONST(Sales),
                                        Recurring=CONST(No)) }
      { 10      ;1   ;Action    ;
                      Name=CashReceiptJournals;
                      CaptionML=[ENU=Cash Receipt Journals;
                                 PTG=Di�rios Cobran�as];
                      ToolTipML=[ENU=Register received payments by applying them to the related customer, vendor, or bank ledger entries.;
                                 PTG=Registar pagamentos recebidos liquidando os movimentos relacionados de cliente, fornecedor ou banco.];
                      ApplicationArea=#Basic,#Suite;
                      RunObject=Page 251;
                      RunPageView=WHERE(Template Type=CONST(Cash Receipts),
                                        Recurring=CONST(No));
                      Image=Journals }
      { 1900000012;0 ;ActionContainer;
                      ActionContainerType=ActivityButtons }
      { 39      ;1   ;ActionGroup;
                      CaptionML=[ENU=Posted Documents;
                                 PTG=Documentos Registados];
                      ToolTipML=[ENU=View history for sales, shipments, and inventory.;
                                 PTG=Ver hist�rico vendas, envios e invent�rio.];
                      Image=FiledPosted }
      { 40      ;2   ;Action    ;
                      CaptionML=[ENU=Posted Sales Shipments;
                                 PTG=Guias Remessa Vendas Registadas];
                      ToolTipML=[ENU=View the posted sales shipments.;
                                 PTG=Ver as guias de remessa de venda registadas.];
                      ApplicationArea=#Basic,#Suite;
                      RunObject=Page 142;
                      Image=PostedShipment }
      { 32      ;2   ;Action    ;
                      CaptionML=[ENU=Posted Sales Invoices;
                                 PTG=Faturas Venda Registadas];
                      ToolTipML=[ENU=View the posted sales invoices.;
                                 PTG=Ver as faturas de venda registadas.];
                      ApplicationArea=#Basic,#Suite;
                      RunObject=Page 143;
                      Image=PostedOrder }
      { 31022890;2   ;Action    ;
                      Name=<Page Posted Sales Debit Memos>;
                      CaptionML=[ENU=Posted Sales Debit Memos;
                                 PTG=Notas D�bito Venda Registadas];
                      ToolTipML=[ENU=View the posted sales debit memo.;
                                 PTG=Ver as notas de d�bito de venda registadas.];
                      ApplicationArea=#Basic,#Suite;
                      RunObject=Page 31023060;
                      Image=PostedOrder }
      { 33      ;2   ;Action    ;
                      CaptionML=[ENU=Posted Return Receipts;
                                 PTG=Rece��es Devolu��o Registadas];
                      RunObject=Page 6662;
                      Image=PostedReturnReceipt }
      { 34      ;2   ;Action    ;
                      CaptionML=[ENU=Posted Sales Credit Memos;
                                 PTG=Notas Cr�dito Venda Registadas];
                      ToolTipML=[ENU=View the posted sales credit memos.;
                                 PTG=Ver as notas de cr�dito de venda registadas.];
                      ApplicationArea=#Basic,#Suite;
                      RunObject=Page 144;
                      Image=PostedOrder }
      { 53      ;2   ;Action    ;
                      CaptionML=[ENU=Posted Purchase Receipts;
                                 PTG=Guias Remessa Compra Registadas];
                      RunObject=Page 145 }
      { 54      ;2   ;Action    ;
                      CaptionML=[ENU=Posted Purchase Invoices;
                                 PTG=Faturas Compra Registadas];
                      ToolTipML=[ENU=View the posted purchase invoices.;
                                 PTG=Ver as facturas de compra registadas.];
                      ApplicationArea=#Basic,#Suite;
                      RunObject=Page 146 }
      { 31022891;2   ;Action    ;
                      Name=<Page Posted Purchase Debit Memos>;
                      CaptionML=PTG=Notas D�bito Compra Registadas;
                      ToolTipML=[ENU=View the posted purchase debit memos.;
                                 PTG=Ver as notas de d�bito de compra registadas.];
                      ApplicationArea=#Basic,#Suite;
                      RunObject=Page 31023072 }
      { 57      ;1   ;ActionGroup;
                      CaptionML=[ENU=Self-Service;
                                 PTG=Self-Service];
                      ToolTipML=[ENU=Manage your time sheets and assignments.;
                                 PTG=Gerir as suas folhas de horas assigna��es.];
                      Image=HumanResources }
      { 24      ;2   ;Action    ;
                      CaptionML=[ENU=Inventory - Sales &Back Orders;
                                 PTG=Invent�rio - Encomendas Pendentes Vendas];
                      ToolTipML=[ENU=View all time sheets.;
                                 PTG=Ver todas as folhas de horas.];
                      ApplicationArea=#Suite;
                      RunObject=Page 951;
                      Gesture=None }
      { 16      ;0   ;ActionContainer;
                      ActionContainerType=NewDocumentItems }
      { 18      ;1   ;Action    ;
                      CaptionML=[ENU=Sales &Quote;
                                 PTG=P&roposta Venda];
                      ToolTipML=[ENU=Offer items or services to a customer.;
                                 PTG=Propor produtos ou servi�os a um cliente.];
                      ApplicationArea=#Basic,#Suite;
                      RunObject=Page 41;
                      Promoted=No;
                      Image=NewSalesQuote;
                      PromotedCategory=Process;
                      RunPageMode=Create }
      { 15      ;1   ;Action    ;
                      CaptionML=[ENU=Sales &Invoice;
                                 PTG=Fatura &Venda];
                      ToolTipML=[ENU=Create a new invoice for items or services. Invoice quantities cannot be posted partially.;
                                 PTG=Criar uma nova fatura de produtos ou servi�os. As quantidades da fatura n�o podem ser registadas parcialmente.];
                      ApplicationArea=#Basic,#Suite;
                      RunObject=Page 43;
                      Promoted=No;
                      Image=NewSalesInvoice;
                      PromotedCategory=Process;
                      RunPageMode=Create }
      { 31022892;1   ;Action    ;
                      Name=<Page Sales Debit Memo>;
                      CaptionML=[ENU=Sales &Debit Memo;
                                 PTG=Nota &D�bito Venda];
                      ToolTipML=[ENU=Create a new debit memo.;
                                 PTG=Criar uma nova nota de d�bito.];
                      ApplicationArea=#Basic,#Suite;
                      RunObject=Page 43;
                      Promoted=No;
                      Image=NewSalesInvoice;
                      PromotedCategory=Process;
                      RunPageMode=Create }
      { 12      ;1   ;Action    ;
                      CaptionML=[ENU=Sales &Order;
                                 PTG=Enc&omenda Venda];
                      ToolTipML=[ENU=Create a new sales order for items or services that require partial posting.;
                                 PTG=Criar uma nova encomenda de venda para produtos ou servi�os que requerem registo parcial.];
                      ApplicationArea=#Basic,#Suite;
                      RunObject=Page 42;
                      Promoted=No;
                      Image=Document;
                      PromotedCategory=Process;
                      RunPageMode=Create }
      { 9       ;1   ;Action    ;
                      CaptionML=[ENU=Sales &Return Order;
                                 PTG=&Devolu��o Venda];
                      RunObject=Page 6630;
                      Promoted=No;
                      Image=ReturnOrder;
                      PromotedCategory=Process;
                      RunPageMode=Create }
      { 8       ;1   ;Action    ;
                      CaptionML=[ENU=Sales &Credit Memo;
                                 PTG=Nota &Cr�dito Venda];
                      ToolTipML=[ENU=Create a new sales credit memo to revert a posted sales invoice.;
                                 PTG=Criar uma nova nota de cr�dito de venda para anular uma fatura de venda registada.];
                      ApplicationArea=#Basic,#Suite;
                      RunObject=Page 44;
                      Promoted=No;
                      Image=CreditMemo;
                      PromotedCategory=Process;
                      RunPageMode=Create }
      { 1900000004;0 ;ActionContainer;
                      ActionContainerType=ActionItems }
      { 35      ;1   ;ActionGroup;
                      CaptionML=[ENU=Tasks;
                                 PTG=Tarefas] }
      { 36      ;2   ;Action    ;
                      CaptionML=[ENU=Sales &Journal;
                                 PTG=Di�r&io Vendas];
                      ToolTipML=[ENU=Open a sales journal where you can batch post sales transactions to G/L, bank, customer, vendor and fixed assets accounts.;
                                 PTG=Abre um di�rio de vendas, onde pode registar por lotes as transa��es de venda nas contas da contabilidade, bancos, clientes, fornecedores e imobilizado.];
                      ApplicationArea=#Basic,#Suite;
                      RunObject=Page 253;
                      Image=Journals }
      { 38      ;2   ;Action    ;
                      CaptionML=[ENU=Sales Price &Worksheet;
                                 PTG=Fol&ha Pre�os Venda];
                      RunObject=Page 7023;
                      Image=PriceWorksheet }
      { 42      ;1   ;ActionGroup;
                      CaptionML=[ENU=Sales;
                                 PTG=Vendas] }
      { 43      ;2   ;Action    ;
                      CaptionML=[ENU=&Prices;
                                 PTG=&Pre�os];
                      ToolTipML=[ENU=Set up different prices for items that you sell to the customer. An item price is automatically granted on invoice lines when the specified criteria are met, such as customer, quantity, or ending date.;
                                 PTG=Configurar pre�os distintos para os produtos que se vendem ao cliente. Um pre�o de produto � concedido automaticamente nas linhas de fatura quando os crit�rios especificados, como o cliente, a quantidade ou a data final, s�o cumpridos.];
                      ApplicationArea=#Basic,#Suite;
                      RunObject=Page 7002;
                      Image=SalesPrices }
      { 44      ;2   ;Action    ;
                      CaptionML=[ENU=&Line Discounts;
                                 PTG=Descontos &Linha];
                      ToolTipML=[ENU=Set up different discounts for items that you sell to the customer. An item discount is automatically granted on invoice lines when the specified criteria are met, such as customer, quantity, or ending date.;
                                 PTG=Configurar descontos distintos para os produtos que se vendem ao cliente. Um desconto de produto � concedido automaticamente nas linhas de fatura quando os crit�rios especificados, como o cliente, a quantidade ou a data final, s�o cumpridos.];
                      ApplicationArea=#Basic,#Suite;
                      RunObject=Page 7004;
                      Image=SalesLineDisc }
      { 27      ;1   ;ActionGroup;
                      CaptionML=[ENU=Reports;
                                 PTG=Mapas] }
      { 55      ;2   ;ActionGroup;
                      CaptionML=[ENU=Customer;
                                 PTG=Cliente];
                      Image=Customer }
      { 48      ;3   ;Action    ;
                      CaptionML=[ENU=Customer - &Order Summary;
                                 PTG=Cliente - Detalhe Enc&omendas];
                      ToolTipML=[ENU=View the quantity not yet shipped for each customer in three periods of 30 days each, starting from a selected date. There are also columns with orders to be shipped before and after the three periods and a column with the total order detail for each customer. The report can be used to analyze a company's expected sales volume.;
                                 PTG=Ver a quantidade pendente de envio por cliente em tr�s per�odos consecutivos de 30 d�as cada um, a partir de uma data selecionada. Existem tamb�m colunas com pedidos por entregar antes e depois dos tr�s per�odos e uma coluna com o detalhe total da encomenda por cliente. Utilize o mapa para analizar o volume de vendas previsto de uma empresa.];
                      ApplicationArea=#Basic,#Suite;
                      RunObject=Report 107;
                      Image=Report }
      { 41      ;3   ;Action    ;
                      CaptionML=[ENU=Customer - &Top 10 List;
                                 PTG=Cliente - Lista 10 &Melhores];
                      ToolTipML=[ENU=View which customers purchase the most or owe the most in a selected period. Only customers that have either purchases during the period or a balance at the end of the period will be included.;
                                 PTG=Ver os clientes que mais compram ou que mais devem num per�odo selecionado. S� s�o inclu�dos os clientes que tenham comprado durante o per�odo selecionado o que tenham saldo no final do per�odo.];
                      ApplicationArea=#Basic,#Suite;
                      RunObject=Report 111;
                      Image=Report }
      { 37      ;3   ;Action    ;
                      CaptionML=[ENU=Customer/&Item Sales;
                                 PTG=Cl&iente/Vendas Produto];
                      ToolTipML=[ENU=View a list of item sales for each customer during a selected time period. The report contains information on quantity, sales amount, profit, and possible discounts. It can be used, for example, to analyze a company's customer groups.;
                                 PTG=Ver uma lista das vendas de produto por cliente durante um per�odo determinado. O mapa contem informa��o sobre a quantidade, valor de vendas, ganhos e poss�veis descontos. Pode ser usado, por exemplo, para analizar os grupos de clientes de uma empresa.];
                      ApplicationArea=#Basic,#Suite;
                      RunObject=Report 113;
                      Image=Report }
      { 31      ;2   ;ActionGroup;
                      CaptionML=[ENU=Sales;
                                 PTG=Vendas];
                      Image=Sales }
      { 30      ;3   ;Action    ;
                      CaptionML=[ENU=Salesperson - Sales &Statistics;
                                 PTG=Vendedor - E&stat�sticas Vendas];
                      ToolTipML=[ENU=View amounts for sales, profit, invoice discount, and payment discount, as well as profit percentage, for each salesperson for a selected period. The report also shows the adjusted profit and adjusted profit percentage, which reflect any changes to the original costs of the items in the sales.;
                                 PTG=Ver valores de vendas, ganhos, desconto fatura, descontos pronto pagamento e percentagem de ganho, por vendedor para um per�odo selecionado. O mapa tamb�m mostra o ganho e a percentagem de ganho atualizados, que refletem qualquer altera��o aos custos originais dos produtos nas vendas.];
                      ApplicationArea=#Suite;
                      RunObject=Report 114;
                      Image=Report }
      { 29      ;3   ;Action    ;
                      CaptionML=[ENU=Price &List;
                                 PTG=&Lista Pre�os];
                      ToolTipML=[ENU=View a list of your items and their prices, for example, to send to customers. You can create the list for specific customers, campaigns, currencies, or other criteria.;
                                 PTG=Ver uma lista dos produtos e pre�os, por exemplo, para enviar aos clientes. Pode criar a lista para clientes, campanhas ou outros crit�rios especificos .];
                      ApplicationArea=#Basic,#Suite;
                      RunObject=Report 715;
                      Image=Report }
      { 28      ;3   ;Action    ;
                      CaptionML=[ENU=Inventory - Sales &Back Orders;
                                 PTG=Inventario - &Encomendas Venda Pendentes];
                      ToolTipML=[ENU=View a list with the order lines whose shipment date has been exceeded. The following information is shown for the individual orders for each item: number, customer name, customer's telephone number, shipment date, order quantity and quantity on back order. The report also shows whether there are other items for the customer on back order.;
                                 PTG=Ver uma lista com as linhas de encomenda, cuja data de envio foi excedida. A seguinte informa��o � apresentada para encomendas individuais de cada produto: n�mero, nome do cliente, n�mero de telefone do cliente, data de env�o, quantidade do pedido e quantidade em encomendas pendentes. O mapa tamb�m mostra se existem outros produtos para o cliente com encomendas pendentes.];
                      ApplicationArea=#Basic,#Suite;
                      RunObject=Report 718;
                      Image=Report }
      { 45      ;1   ;ActionGroup;
                      CaptionML=[ENU=History;
                                 PTG=Hist�rico] }
      { 46      ;2   ;Action    ;
                      CaptionML=[ENU=Navi&gate;
                                 PTG=Nave&gar];
                      ToolTipML=[ENU=Find all entries and documents that exist for the document number and posting date on the selected entry or document.;
                                 PTG=Encontrar todos os movimentos e documentos que existem para o n�mero de documento e data e registo que constam no movimento ou documento selecionado.];
                      ApplicationArea=#Basic,#Suite;
                      RunObject=Page 344;
                      Image=Navigate }
    }
  }
  CONTROLS
  {
    { 1900000008;0;Container;
                ContainerType=RoleCenterArea }

    { 1900724808;1;Group   }

    { 1901851508;2;Part   ;
                AccessByPermission=TableData 110=R;
                ApplicationArea=#Basic,#Suite;
                PagePartID=Page9060;
                PartType=Page }

    { 14  ;2   ;Part      ;
                ApplicationArea=#Suite;
                PagePartID=Page9042;
                PartType=Page }

    { 1907692008;2;Part   ;
                ApplicationArea=#Basic,#Suite;
                PagePartID=Page9150;
                PartType=Page }

    { 13  ;2   ;Part      ;
                ApplicationArea=#Basic,#Suite;
                PagePartID=Page6303;
                PartType=Page }

    { 1900724708;1;Group   }

    { 1   ;2   ;Part      ;
                AccessByPermission=TableData 110=R;
                ApplicationArea=#Basic,#Suite;
                PagePartID=Page760;
                PartType=Page }

    { 4   ;2   ;Part      ;
                PagePartID=Page675;
                Visible=false;
                PartType=Page }

    { 1905989608;2;Part   ;
                AccessByPermission=TableData 9152=R;
                ApplicationArea=#Basic,#Suite;
                PagePartID=Page9152;
                PartType=Page }

    { 21  ;2   ;Part      ;
                AccessByPermission=TableData 477=R;
                ApplicationArea=#Suite;
                PagePartID=Page681;
                PartType=Page }

    { 1903012608;2;Part   ;
                PagePartID=Page9175;
                Visible=FALSE;
                PartType=Page }

    { 1901377608;2;Part   ;
                PartType=System;
                SystemPartID=MyNotes }

  }
  CODE
  {

    BEGIN
    END.
  }
}

