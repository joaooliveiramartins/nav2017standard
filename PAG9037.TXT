OBJECT Page 9037 Accountant Activities
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Activities;
               PTG=Atividades];
    SourceTable=Table9054;
    PageType=CardPart;
    RefreshOnActivate=Yes;
    OnOpenPage=VAR
                 RoleCenterNotificationMgt@1000 : Codeunit 1430;
               BEGIN
                 RESET;
                 IF NOT GET THEN BEGIN
                   INIT;
                   INSERT;
                 END;

                 SETFILTER("Due Date Filter",'<=%1',WORKDATE);
                 SETFILTER("Overdue Date Filter",'<%1',WORKDATE);
                 SETFILTER("Due Next Week Filter",'%1..%2',CALCDATE('<1D>',WORKDATE),CALCDATE('<1W>',WORKDATE));

                 RoleCenterNotificationMgt.ShowNotifications;
               END;

    ActionList=ACTIONS
    {
      { 22      ;    ;ActionContainer;
                      ActionContainerType=ActionItems }
      { 24      ;1   ;Action    ;
                      Name=Set Up Cues;
                      CaptionML=[ENU=Set Up Cues;
                                 PTG=Configurar Filas];
                      ToolTipML=ENU=Set up the cues (status tiles) related to the role.;
                      ApplicationArea=#Basic,#Suite;
                      Image=Setup;
                      OnAction=VAR
                                 CueSetup@1001 : Codeunit 9701;
                                 CueRecordRef@1000 : RecordRef;
                               BEGIN
                                 CueRecordRef.GETTABLE(Rec);
                                 CueSetup.OpenCustomizePageForCurrentUser(CueRecordRef.NUMBER);
                               END;
                                }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 6   ;1   ;Group     ;
                CaptionML=[ENU=Payments;
                           PTG=Pagamentos];
                GroupType=CueGroup;
                ActionList=ACTIONS
                {
                  { 1       ;0   ;Action    ;
                                  CaptionML=[ENU=Edit Cash Receipt Journal;
                                             PTG=Editar Di�rio Recebimentos];
                                  ToolTipML=ENU=Register received payments in a cash receipt journal that may already contain journal lines.;
                                  ApplicationArea=#Basic,#Suite;
                                  RunObject=Page 255 }
                  { 3       ;0   ;Action    ;
                                  CaptionML=[ENU=New Sales Credit Memo;
                                             PTG=Nova Nota de Cr�dito Venda];
                                  ToolTipML=ENU=Process a return or refund by creating a new sales credit memo.;
                                  ApplicationArea=#Basic,#Suite;
                                  RunObject=Page 44;
                                  RunPageMode=Create }
                  { 4       ;0   ;Action    ;
                                  CaptionML=[ENU=Edit Payment Journal;
                                             PTG=Editar Di�rio Pagamentos];
                                  ToolTipML=ENU=Pay your vendors by filling the payment journal automatically according to payments due, and potentially export all payment to your bank for automatic processing.;
                                  ApplicationArea=#Basic,#Suite;
                                  RunObject=Page 256 }
                  { 5       ;0   ;Action    ;
                                  CaptionML=[ENU=New Purchase Credit Memo;
                                             PTG=Nova Nota Cr�dito Compra];
                                  ToolTipML=ENU=Create a new purchase credit memo so you can manage returned items to a vendor.;
                                  ApplicationArea=#Basic,#Suite;
                                  RunObject=Page 52;
                                  RunPageMode=Create }
                }
                 }

    { 7   ;2   ;Field     ;
                ToolTipML=ENU=Specifies the number of purchase invoices where your payment is late.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Overdue Purchase Documents";
                DrillDownPageID=Vendor Ledger Entries }

    { 9   ;2   ;Field     ;
                ToolTipML=ENU=Specifies the number of purchase invoices that are due for payment today.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Purchase Documents Due Today";
                DrillDownPageID=Vendor Ledger Entries }

    { 16  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the number of payments to vendors that are due next week.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Purch. Invoices Due Next Week" }

    { 17  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the number of purchase discounts that are available next week, for example, because the discount expires after next week.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Purchase Discounts Next Week" }

    { 19  ;1   ;Group     ;
                CaptionML=[ENU=Document Approvals;
                           PTG=Aprova��es de Documentos];
                GroupType=CueGroup;
                ActionList=ACTIONS
                {
                  { 25      ;    ;Action    ;
                                  CaptionML=[ENU=Create Reminders...;
                                             PTG=Criar Cartas de Aviso...];
                                  ToolTipML=ENU=Remind your customers of late payments.;
                                  RunObject=Report 188;
                                  Image=CreateReminders }
                  { 26      ;    ;Action    ;
                                  CaptionML=[ENU=Create Finance Charge Memos...;
                                             PTG=Criar Notas de Juros];
                                  ToolTipML=ENU=Issue finance charge memos to your customers as a consequence of late payment.;
                                  RunObject=Report 191;
                                  Image=CreateFinanceChargememo }
                }
                 }

    { 23  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the number of purchase orders that are pending approval.;
                ApplicationArea=#Suite;
                SourceExpr="POs Pending Approval";
                DrillDownPageID=Purchase Order List }

    { 20  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the number of sales orders that are pending approval.;
                ApplicationArea=#Suite;
                SourceExpr="SOs Pending Approval";
                DrillDownPageID=Sales Order List }

    { 14  ;1   ;Group     ;
                CaptionML=[ENU=Financials;
                           PTG=Financeira];
                GroupType=CueGroup;
                ActionList=ACTIONS
                {
                  { 15      ;    ;Action    ;
                                  CaptionML=[ENU=New Payment Reconciliation Journal;
                                             PTG=Novo Di�rio de Reconcilia��o Pagamentos];
                                  ToolTipML=ENU=Reconcile unpaid documents automatically with their related bank transactions by importing bank a bank statement feed or file.;
                                  ApplicationArea=#Basic,#Suite;
                                  OnAction=VAR
                                             BankAccReconciliation@1000 : Record 273;
                                           BEGIN
                                             BankAccReconciliation.OpenNewWorksheet
                                           END;
                                            }
                }
                 }

    { 11  ;2   ;Field     ;
                CaptionML=[ENU=Payment Reconciliation Journals;
                           PTG=Di�rios Pagamento Reconcilia��o"];
                ToolTipML=[ENU=Specifies journals where you can reconcile unpaid documents automatically with their related bank transactions by importing bank a bank statement feed or file.;
                           PTG=""];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Non-Applied Payments";
                Image=Cash;
                DrillDownPageID=Pmt. Reconciliation Journals }

    { 2   ;1   ;Group     ;
                CaptionML=[ENU=Incoming Documents;
                           PTG=Documentos a Receber];
                GroupType=CueGroup;
                ActionList=ACTIONS
                {
                  { 12      ;    ;Action    ;
                                  Name=CheckForOCR;
                                  CaptionML=[ENU=Receive from OCR Service;
                                             PTG=Receber de Servi�o OCR];
                                  ToolTipML=ENU=Process new incoming electronic documents that have been created by the OCR service and that you can convert to, for example, purchase invoices in Dynamics NAV.;
                                  ApplicationArea=#Basic,#Suite;
                                  RunObject=Codeunit 881;
                                  RunPageMode=View }
                }
                 }

    { 8   ;2   ;Field     ;
                ToolTipML=ENU=Specifies the number of new incoming documents in the company. The documents are filtered by today's date.;
                SourceExpr="New Incoming Documents";
                DrillDownPageID=Incoming Documents }

    { 10  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the number of approved incoming documents in the company. The documents are filtered by today's date.;
                SourceExpr="Approved Incoming Documents";
                DrillDownPageID=Incoming Documents }

    { 13  ;2   ;Field     ;
                ToolTipML=ENU=Specifies that incoming document records that have been created by the OCR service.;
                SourceExpr="OCR Completed";
                DrillDownPageID=Incoming Documents }

    { 18  ;1   ;Group     ;
                CaptionML=[ENU=New Journal Entries;
                           PTG=Movimentos Novo Di�rio];
                GroupType=CueGroup;
                ActionList=ACTIONS
                {
                  { 21      ;    ;Action    ;
                                  CaptionML=[ENU=New G/L Journal Entry;
                                             PTG=Novo Movimento C/G];
                                  ToolTipML=ENU=Prepare to post any transaction to the company books.;
                                  ApplicationArea=#Basic,#Suite;
                                  RunObject=Page 39;
                                  Image=TileNew }
                  { 28      ;    ;Action    ;
                                  CaptionML=[ENU=New Payment Journal Entry;
                                             PTG=Novo Movimento Di�rio Pagamentos];
                                  ToolTipML=ENU=Pay your vendors by filling the payment journal automatically according to payments due, and potentially export all payment to your bank for automatic processing.;
                                  ApplicationArea=#Basic,#Suite;
                                  RunObject=Page 256;
                                  Image=TileNew }
                }
                 }

  }
  CODE
  {

    BEGIN
    END.
  }
}

