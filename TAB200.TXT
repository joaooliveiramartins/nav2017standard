OBJECT Table 200 Work Type
{
  OBJECT-PROPERTIES
  {
    Date=07/09/12;
    Time=12:00:00;
    Version List=NAVW17.00;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Work Type;
               PTG=Tipo Trabalho];
    LookupPageID=Page208;
    DrillDownPageID=Page208;
  }
  FIELDS
  {
    { 1   ;   ;Code                ;Code10        ;CaptionML=[ENU=Code;
                                                              PTG=C�digo];
                                                   NotBlank=Yes }
    { 2   ;   ;Description         ;Text50        ;CaptionML=[ENU=Description;
                                                              PTG=Descri��o] }
    { 3   ;   ;Unit of Measure Code;Code10        ;TableRelation="Unit of Measure";
                                                   CaptionML=[ENU=Unit of Measure Code;
                                                              PTG=C�d. Unidade Medida] }
  }
  KEYS
  {
    {    ;Code                                    ;Clustered=Yes }
  }
  FIELDGROUPS
  {
    { 1   ;DropDown            ;Code,Description,Unit of Measure Code    }
  }
  CODE
  {

    BEGIN
    END.
  }
}

