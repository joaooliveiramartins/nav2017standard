OBJECT Page 31023044 Imparities Journal Templates
{
  OBJECT-PROPERTIES
  {
    Date=21/12/16;
    Time=13:00:00;
    Version List=NAVPTSS10.00;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Imparities Journal Templates;
               PTG=Livros Di�rio Imparidades];
    SourceTable=Table31022968;
    PageType=List;
    ActionList=ACTIONS
    {
      { 1102058013;  ;ActionContainer;
                      ActionContainerType=RelatedInformation }
      { 1102058014;1 ;ActionGroup;
                      CaptionML=[ENU=Te&mplate;
                                 PTG=&Livro] }
      { 1102058015;2 ;Action    ;
                      CaptionML=[ENU=Batches;
                                 PTG=Sec��es];
                      RunObject=Page 31023045;
                      RunPageLink=Journal Template Name=FIELD(Name);
                      Image=Description }
    }
  }
  CONTROLS
  {
    { 1102058000;0;Container;
                ContainerType=ContentArea }

    { 1102058001;1;Group  ;
                Name=Group;
                GroupType=Repeater }

    { 1102058002;2;Field  ;
                ApplicationArea=#All;
                SourceExpr=Name }

    { 1102058003;2;Field  ;
                ApplicationArea=#All;
                SourceExpr=Description }

    { 1102058004;2;Field  ;
                ApplicationArea=#All;
                SourceExpr="Test Report ID";
                Visible=FALSE;
                LookupPageID=Objects }

    { 1102058005;2;Field  ;
                ApplicationArea=#All;
                SourceExpr="Page ID";
                Visible=FALSE;
                LookupPageID=Objects }

    { 1102058006;2;Field  ;
                DrillDown=No;
                ApplicationArea=#All;
                SourceExpr="Test Report Name";
                Visible=FALSE }

    { 1102058007;2;Field  ;
                DrillDown=No;
                ApplicationArea=#All;
                SourceExpr="Page Name";
                Visible=FALSE }

    { 1102058016;2;Field  ;
                ApplicationArea=#All;
                SourceExpr="Posting Report ID";
                Visible=FALSE }

    { 1102058017;2;Field  ;
                DrillDown=No;
                ApplicationArea=#All;
                SourceExpr="Posting Report Name";
                Visible=FALSE }

    { 1102058008;2;Field  ;
                ApplicationArea=#All;
                SourceExpr="No. Series" }

    { 1102058009;2;Field  ;
                ApplicationArea=#All;
                SourceExpr="Posting No. Series" }

    { 1102058010;0;Container;
                ContainerType=FactBoxArea }

    { 1102058011;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1102058012;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {

    BEGIN
    END.
  }
}

