OBJECT Page 31023040 BP XML Date
{
  OBJECT-PROPERTIES
  {
    Date=29/03/17;
    Time=14:32:45;
    Modified=Yes;
    Version List=NAVPT10.00#00032;
  }
  PROPERTIES
  {
    CaptionML=[ENU=BP XML Date;
               PTG=Data XML Banco Portugal];
    SourceTable=Table31022960;
    PageType=Worksheet;
    ActionList=ACTIONS
    {
      { 1000000003;  ;ActionContainer;
                      Name=Function;
                      CaptionML=[ENU=Function;
                                 PTG=Fun��o];
                      ActionContainerType=ActionItems }
      { 1000000004;1 ;Action    ;
                      Name=Export XML;
                      CaptionML=[ENU=Export XML;
                                 PTG=Exportar XML];
                      ApplicationArea=#All;
                      Promoted=Yes;
                      Image=Export;
                      OnAction=VAR
                                 BPLedgerEntry@1000000000 : Record 31022960;
                               BEGIN
                                 BPLedgerEntry.RESET;
                                 BPLedgerEntry.SETFILTER(Status,'<>%1',BPLedgerEntry.Status::Exported);
                                 //10.00#00032,so
                                 //BPLedgerEntry.SETRANGE(Month,Month);
                                 //BPLedgerEntry.SETRANGE(Year,Year);
                                 //10.00#00032,eo
                                 //10.00#00032,so
                                 BPLedgerEntry.SETRANGE(Month,ExportMonth);
                                 BPLedgerEntry.SETRANGE(Year,ExportYear);
                                 //10.00#00032,eo
                                 IF BPLedgerEntry.FINDFIRST THEN
                                   ExportXML(COPYSTR(FORMAT(BPLedgerEntry."Reference Date"),1,6))
                                 ELSE
                                   ERROR(Text31022980);
                               END;
                                }
    }
  }
  CONTROLS
  {
    { 1000000000;;Container;
                Name=Select Date;
                ContainerType=ContentArea }

    { 1000000001;1;Field  ;
                ApplicationArea=#All;
                SourceExpr=Month }

    { 1000000002;1;Field  ;
                ApplicationArea=#All;
                SourceExpr=Year }

  }
  CODE
  {
    VAR
      Text31022980@1000000000 : TextConst 'ENU=Unable to generate the XML. The XML has been generated already or there are no records for the month/year selected.;PTG=N�o foi possivel gerar o XML. O XML j� foi gerado ou n�o existem registos para o m�s/ano selecionado.';
      ExportMonth@31022891 : Integer;
      ExportYear@31022890 : Integer;

    BEGIN
    END.
  }
}

