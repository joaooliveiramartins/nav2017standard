OBJECT Table 31022975 Withholding Tax Codes
{
  OBJECT-PROPERTIES
  {
    Date=10/10/16;
    Time=13:00:00;
    Version List=NAVPTSS93.00;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Withholding Tax Codes;
               PTG=C�digos Reten��o na Fonte];
    LookupPageID=Page31023084;
    DrillDownPageID=Page31023084;
  }
  FIELDS
  {
    { 1   ;   ;Code                ;Code20        ;CaptionML=[ENU=Code;
                                                              PTG=C�digo] }
    { 2   ;   ;Description         ;Text60        ;CaptionML=[ENU=Description;
                                                              PTG=Descri��o] }
    { 3   ;   ;Tax %               ;Decimal       ;CaptionML=[ENU=Retention %;
                                                              PTG=% Reten��o] }
    { 4   ;   ;Account No.         ;Code20        ;TableRelation="G/L Account";
                                                   CaptionML=[ENU=Account No.;
                                                              PTG=N� Conta] }
    { 5   ;   ;IRC Code            ;Boolean       ;CaptionML=[ENU=IRC Code;
                                                              PTG=C�digo IRC] }
    { 6   ;   ;Max. Correction Amount;Decimal     ;CaptionML=[ENU=Max. Correction Amount;
                                                              PTG=Valor M�ximo de Corre��o];
                                                   MinValue=0 }
  }
  KEYS
  {
    {    ;Code                                    ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    {
      V93.00#00018 - Reten��o a Clientes e Fornecedores -  - 2016.06.29
    }
    END.
  }
}

