OBJECT Page 2501 Extension Details
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Extension Details;
               PTG=Detalhes da Extens�o];
    InsertAllowed=No;
    DeleteAllowed=No;
    SourceTable=Table2000000160;
    PageType=NavigatePage;
    SourceTableTemporary=Yes;
    OnOpenPage=VAR
                 WinLanguagesTable@1000 : Record 2000000045;
               BEGIN
                 NavAppTable.SETFILTER("Package ID",'%1',"Package ID");
                 IF NOT NavAppTable.FINDFIRST THEN
                   CurrPage.CLOSE;

                 SetNavAppRecord;

                 IsInstalled := NavExtensionInstallationMgmt.IsInstalled("Package ID");
                 IF IsInstalled THEN
                   CurrPage.CAPTION(UninstallationPageCaptionMsg)
                 ELSE
                   CurrPage.CAPTION(InstallationPageCaptionMsg);

                 // Any legal info to display
                 Legal := ((STRLEN("Privacy Statement") <> 0) OR (STRLEN(EULA) <> 0));

                 // Next only enabled if legal info is found
                 NextEnabled := NOT IsInstalled;

                 // Step1 enabled if installing
                 Step1Enabled := NOT IsInstalled;

                 // Auto accept if no legal info
                 Accepted := NOT Legal;

                 LanguageID := GLOBALLANGUAGE;
                 WinLanguagesTable.SETRANGE("Language ID",LanguageID);
                 IF WinLanguagesTable.FINDFIRST THEN
                   LanguageName := WinLanguagesTable.Name;
               END;

    ActionList=ACTIONS
    {
      { 25      ;    ;ActionContainer;
                      ActionContainerType=ActionItems }
      { 24      ;1   ;Action    ;
                      Name=Back;
                      CaptionML=[ENU=Back;
                                 PTG=Para tr�s];
                      ApplicationArea=#Basic,#Suite;
                      Visible=BackEnabled;
                      InFooterBar=Yes;
                      Image=PreviousRecord;
                      OnAction=BEGIN
                                 BackEnabled := FALSE;
                                 NextEnabled := TRUE;
                                 Step1Enabled := TRUE;
                                 InstallEnabled := FALSE;
                               END;
                                }
      { 22      ;1   ;Action    ;
                      Name=Next;
                      CaptionML=[ENU=Next;
                                 PTG=Pr�ximo];
                      ApplicationArea=#Basic,#Suite;
                      Visible=NextEnabled;
                      InFooterBar=Yes;
                      Image=NextRecord;
                      OnAction=BEGIN
                                 BackEnabled := TRUE;
                                 NextEnabled := FALSE;
                                 Step1Enabled := FALSE;
                                 InstallEnabled := TRUE;
                               END;
                                }
      { 23      ;1   ;Action    ;
                      Name=Install;
                      CaptionML=[ENU=Install;
                                 PTG=Instalar];
                      ToolTipML=[ENU=Install the extension for the current tenant.;
                                 PTG=Instalar a extens�o para o tenant atual];
                      ApplicationArea=#Basic,#Suite;
                      Visible=InstallEnabled;
                      Enabled=Accepted;
                      InFooterBar=Yes;
                      Image=Approve;
                      OnAction=VAR
                                 Dependencies@1000 : Text;
                                 CanChange@1001 : Boolean;
                               BEGIN
                                 CanChange := NavExtensionInstallationMgmt.IsInstalled("Package ID");

                                 IF CanChange THEN BEGIN
                                   MESSAGE(STRSUBSTNO(AlreadyInstalledMsg,Name));
                                   EXIT;
                                 END;

                                 Dependencies := NavExtensionInstallationMgmt.GetDependenciesForExtensionToInstall("Package ID");
                                 CanChange := (STRLEN(Dependencies) = 0);

                                 IF NOT CanChange THEN
                                   CanChange := CONFIRM(STRSUBSTNO(DependenciesFoundQst,Name,Dependencies),FALSE);

                                 IF CanChange THEN
                                   NavExtensionInstallationMgmt.InstallNavExtension("Package ID",LanguageID);

                                 // If successfully installed, message users to restart activity for menusuites
                                 IF NavExtensionInstallationMgmt.IsInstalled("Package ID") THEN
                                   MESSAGE(STRSUBSTNO(RestartActivityInstallMsg,Name));

                                 CurrPage.CLOSE;
                               END;
                                }
      { 21      ;1   ;Action    ;
                      Name=Uninstall;
                      CaptionML=[ENU=Uninstall;
                                 PTG=Desisntalar];
                      ToolTipML=[ENU=Remove the extension from the current tenant.;
                                 PTG=Remover a exten��o do tenant atual.];
                      ApplicationArea=#Basic,#Suite;
                      Visible=IsInstalled;
                      InFooterBar=Yes;
                      Image=Approve;
                      OnAction=VAR
                                 Dependents@1000 : Text;
                                 CanChange@1001 : Boolean;
                               BEGIN
                                 CanChange := NavExtensionInstallationMgmt.IsInstalled("Package ID");
                                 IF NOT CanChange THEN
                                   MESSAGE(STRSUBSTNO(AlreadyUninstalledMsg,Name));

                                 Dependents := NavExtensionInstallationMgmt.GetDependentForExtensionToUninstall("Package ID");
                                 CanChange := (STRLEN(Dependents) = 0);
                                 IF NOT CanChange THEN
                                   CanChange := CONFIRM(STRSUBSTNO(DependentsFoundQst,Name,Dependents),FALSE);

                                 IF CanChange THEN
                                   NavExtensionInstallationMgmt.UninstallNavExtension("Package ID");

                                 // If successfully uninstalled, message users to restart activity for menusuites
                                 IF NOT NavExtensionInstallationMgmt.IsInstalled("Package ID") THEN
                                   MESSAGE(STRSUBSTNO(RestartActivityUninstallMsg,Name));

                                 CurrPage.CLOSE;
                               END;
                                }
    }
  }
  CONTROLS
  {
    { 1   ;0   ;Container ;
                ContainerType=ContentArea }

    { 2   ;1   ;Group     ;
                Name=Install NAV Extension;
                CaptionML=[ENU=Install Extension;
                           PTG=Install Extension];
                Visible=Step1Enabled;
                Editable=FALSE;
                GroupType=Group }

    { 27  ;2   ;Group     ;
                Name=InstallGroup;
                CaptionML=[ENU=Install Extension;
                           PTG=Instalar Extens�o];
                Editable=FALSE;
                GroupType=Group;
                InstructionalTextML=[ENU=Extensions add new capabilities that extend and enhance functionality.;
                                     PTG=Extens�es adicionam novas capacidades que melhoram o funcionamento.] }

    { 5   ;3   ;Field     ;
                Name=In_Name;
                ToolTipML=[ENU=Specifies the name of the extension.;
                           PTG=Especifica o nome da extens�o];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=Name }

    { 16  ;3   ;Field     ;
                Name=In_Des;
                CaptionML=[ENU=Description;
                           PTG=Descri��o];
                ToolTipML=[ENU=Specifies the full description of the extension.;
                           PTG=Especifica a descri��o completa da extens�o];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=AppDescription;
                Editable=False;
                MultiLine=Yes }

    { 7   ;3   ;Field     ;
                Name=In_Ver;
                CaptionML=[ENU=Version;
                           PTG=Vers�o];
                ToolTipML=[ENU=Specifies the version of the extension.;
                           PTG=Especifica a vers�o da extens�o.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=VersionDisplay }

    { 6   ;3   ;Field     ;
                Name=In_Pub;
                ToolTipML=[ENU=Specifies the person or company that created the extension;
                           PTG=Especifica a pessoa ou empresa que criou a extens�o];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=Publisher }

    { 14  ;3   ;Field     ;
                Name=In_Url;
                ToolTipML=[ENU=Specifies the support site for the extension.;
                           PTG=Especificao site de apoio � extens�o.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=UrlLbl;
                Editable=FALSE;
                OnDrillDown=BEGIN
                              HYPERLINK(Url);
                            END;

                ShowCaption=No }

    { 20  ;3   ;Field     ;
                Name=In_Help;
                ToolTipML=[ENU=Specifies the Help site for the extension.;
                           PTG=Especifica o site de ajuda da extens�o.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=HelpLbl;
                Editable=FALSE;
                OnDrillDown=BEGIN
                              HYPERLINK(Help);
                            END;

                ShowCaption=No }

    { 3   ;1   ;Group     ;
                Name=Uninstall NAV Extension;
                CaptionML=[ENU=Uninstall Extension;
                           PTG=Desinstalar Extens�o];
                Visible=IsInstalled;
                GroupType=Group }

    { 4   ;2   ;Group     ;
                Name=UninstallGroup;
                CaptionML=[ENU=Uninstall Extension;
                           PTG=Desinstalar Extens�o];
                Editable=FALSE;
                GroupType=Group;
                InstructionalTextML=[ENU=Uninstall extension to remove added features.;
                                     PTG=Desinstalar extens�o para remover caracter�sticas adicionadas.] }

    { 29  ;3   ;Field     ;
                Name=Un_Name;
                ToolTipML=[ENU=Specifies the name of the extension.;
                           PTG=Especifica o nome da extens�o.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=Name }

    { 15  ;3   ;Field     ;
                Name=Un_Des;
                CaptionML=[ENU=Description;
                           PTG=Descri��o];
                ToolTipML=[ENU=Specifies the full description of the extension.;
                           PTG=Especifica a descri��o completa da extens�o.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=AppDescription;
                Editable=False;
                MultiLine=Yes }

    { 11  ;3   ;Field     ;
                Name=Un_Ver;
                CaptionML=[ENU=Version;
                           PTG=Vers�o];
                ToolTipML=[ENU=Specifies the version of the extension.;
                           PTG=Especifica a vers�o da extens�o.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=VersionDisplay }

    { 10  ;3   ;Field     ;
                Name=Un_Pub;
                ToolTipML=[ENU=Specifies the person or company that created the extension;
                           PTG=Especifica a pessoa ou empresa que criou a extens�o];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=Publisher }

    { 32  ;3   ;Field     ;
                Name=Un_Terms;
                ToolTipML=[ENU=Specifies the terms of use for the extension.;
                           PTG=Especifica os termos de uso para uma extens�o.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=TermsLbl;
                Visible=Legal;
                Editable=False;
                OnDrillDown=BEGIN
                              HYPERLINK(EULA);
                            END;

                ShowCaption=No }

    { 33  ;3   ;Field     ;
                Name=Un_Privacy;
                ToolTipML=[ENU=Specifies the privacy information for the extension.;
                           PTG=Especifica a informa��o de privacidade para a extens�o.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=PrivacyLbl;
                Visible=Legal;
                Editable=False;
                OnDrillDown=BEGIN
                              HYPERLINK("Privacy Statement");
                            END;

                ShowCaption=No }

    { 9   ;3   ;Field     ;
                Name=Un_Url;
                ToolTipML=[ENU=Specifies the support site for the extension.;
                           PTG=Especifica o site de suporte para a extens�o.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=UrlLbl;
                Editable=FALSE;
                OnDrillDown=BEGIN
                              HYPERLINK(Url);
                            END;

                ShowCaption=No }

    { 8   ;3   ;Field     ;
                Name=Un_Help;
                ToolTipML=[ENU=Specifies the Help site for the extension.;
                           PTG=Especifica o site de Ajuda para a extens�o.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=HelpLbl;
                Editable=FALSE;
                OnDrillDown=BEGIN
                              HYPERLINK(Help);
                            END;

                ShowCaption=No }

    { 12  ;1   ;Group     ;
                Name=Installation;
                CaptionML=[ENU=Installation;
                           PTG=Instala��o];
                Visible=BackEnabled;
                GroupType=Group }

    { 13  ;2   ;Group     ;
                CaptionML=[ENU=Review Extension Information before installation;
                           PTG=Rever Informa��o Extens�o];
                GroupType=Group }

    { 17  ;3   ;Field     ;
                ToolTipML=[ENU=Specifies the name of the extension.;
                           PTG=Especifica o nome da extens�o.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=Name;
                Editable=FALSE }

    { 28  ;3   ;Field     ;
                ToolTipML=[ENU=Specifies the person or company that created the extension;
                           PTG=Especifica a pessoa ou empresa que criou a extens�o];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=Publisher;
                Editable=FALSE }

    { 31  ;3   ;Field     ;
                Name=Language;
                CaptionML=[ENU=Language;
                           PTG=Idioma];
                ToolTipML=[ENU=Specifies the language to install the extension against.;
                           PTG=Especifica o idioma a instalar na qual vai instalar a extens�o.];
                SourceExpr=LanguageName;
                OnValidate=VAR
                             WinLanguagesTable@1000 : Record 2000000045;
                           BEGIN
                             WinLanguagesTable.SETRANGE(Name,LanguageName);
                             WinLanguagesTable.SETFILTER("Globally Enabled",'Yes');
                             WinLanguagesTable.SETFILTER("Localization Exist",'Yes');
                             IF WinLanguagesTable.FINDFIRST THEN
                               LanguageID := WinLanguagesTable."Language ID"
                             ELSE
                               ERROR(LanguageNotFoundErr,LanguageName);
                           END;

                OnLookup=VAR
                           WinLanguagesTable@1001 : Record 2000000045;
                         BEGIN
                           WinLanguagesTable.SETFILTER("Globally Enabled",'Yes');
                           WinLanguagesTable.SETFILTER("Localization Exist",'Yes');
                           IF PAGE.RUNMODAL(PAGE::"Windows Languages",WinLanguagesTable) = ACTION::LookupOK THEN BEGIN
                             LanguageID := WinLanguagesTable."Language ID";
                             LanguageName := WinLanguagesTable.Name;
                           END;
                         END;
                          }

    { 30  ;3   ;Group     ;
                Visible=Legal;
                GroupType=Group }

    { 18  ;4   ;Field     ;
                Name=Terms;
                ToolTipML=[ENU=Specifies the terms of use for the extension.;
                           PTG=Especifica os termos de uso da extens�o.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=TermsLbl;
                Editable=FALSE;
                OnDrillDown=BEGIN
                              HYPERLINK(EULA);
                            END;

                ShowCaption=No }

    { 19  ;4   ;Field     ;
                Name=Privacy;
                ToolTipML=[ENU=Specifies the privacy information for the extension.;
                           PTG=Especifica a informa��o de privacidade da extens�o.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=PrivacyLbl;
                Editable=FALSE;
                OnDrillDown=BEGIN
                              HYPERLINK("Privacy Statement");
                            END;

                ShowCaption=No }

    { 26  ;4   ;Field     ;
                Name=Accepted;
                CaptionML=[ENU=I accept the terms and conditions;
                           PTG=Aceito os termos e condi��es];
                ToolTipML=[ENU=Specifies if you accept the terms and conditions.;
                           PTG=Especifica se aceita os termos e condicoes.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=Accepted }

  }
  CODE
  {
    VAR
      NavAppTable@1018 : Record 2000000160;
      NavExtensionInstallationMgmt@1000 : Codeunit 2500;
      AppDescription@1001 : BigText;
      VersionDisplay@1015 : Text;
      LanguageName@1022 : Text;
      BackEnabled@1002 : Boolean;
      NextEnabled@1003 : Boolean;
      InstallEnabled@1004 : Boolean;
      Accepted@1005 : Boolean;
      IsInstalled@1006 : Boolean;
      Legal@1007 : Boolean;
      Step1Enabled@1008 : Boolean;
      DependenciesFoundQst@1014 : TextConst '@@@="%1=name of app, %2=semicolon separated list of uninstalled dependencies";ENU=The extension %1 has a dependency on one or more extensions: %2. \ \Do you wish to install %1 and all of its dependencies?;PTG=A extens�o %1 tem uma depend�ncia em uma ou mais extens�es: %2. \ \ Deseja instalar %1 e todas as suas depend�ncias?';
      DependentsFoundQst@1013 : TextConst '@@@="%1=name of app, %2=semicolon separated list of installed dependents";ENU=The extension %1 is a dependency for on or more extensions: %2. \ \Do you wish to uninstall %1 and all of its dependents?;PTG=A extens�o %1 � uma depend�ncia para uma ou mais extens�es: %2. \ \ Deseja desinstalar %1 e todas as suas depend�ncias?';
      AlreadyInstalledMsg@1012 : TextConst '@@@="%1=name of app";ENU=The extension %1 is already installed.;PTG=A extens�o %1 j� est� instalada.';
      AlreadyUninstalledMsg@1011 : TextConst '@@@="%1=name of app";ENU=The extension %1 is not installed.;PTG=A extens�o %1 n�o est� instalada.';
      InstallationPageCaptionMsg@1010 : TextConst '@@@=Caption for when extension needs to be installed;ENU=Extension Installation;PTG=Instala��o de Extens�es';
      RestartActivityInstallMsg@1020 : TextConst '@@@="Indicates that users need to restart their activity to pick up new menusuite items. %1=Name of Extension";ENU=The %1 extension was successfully installed. All active users must log out and log in again to see the navigation changes.;PTG=A extens�o %1 foi instalada com sucesso. Todos os users ativos devem terminar sess�o e iniciar sess�o de novo para visualizarem as antera��es de navega��o.';
      RestartActivityUninstallMsg@1019 : TextConst '@@@="Indicates that users need to restart their activity to pick up new menusuite items. %1=Name of Extension";ENU=The %1 extension was successfully uninstalled. All active users must log out and log in again to see the navigation changes.;PTG=A extens�o %1 foi desinstalada com sucesso. Todos os users ativos devem terminar sess�o e iniciar sess�o de novo para visualizarem as antera��es de navega��o.';
      UninstallationPageCaptionMsg@1009 : TextConst '@@@=Caption for when extension needs to be uninstalled;ENU=Extension Uninstallation;PTG=Desisntala��o de Extens�es';
      LanguageNotFoundErr@1025 : TextConst '@@@="Error message to notify user that the entered language was not found. This could mean that the language doesn''t exist or that the language is not valid within the filter set for the lookup. %1=Entered value.";ENU=Language %1 does not exist, or is not enabled globally and contains a localization. Use the lookup to select a language.;PTG=Idioma %1 n�o existe, ou n�o foi ativada globalmente e cont�m uma localiza��o. Use o lookup para selecionar um idioma.';
      TermsLbl@1016 : TextConst 'ENU=Terms and Conditions;PTG=Termos e Condi��es';
      PrivacyLbl@1017 : TextConst '@@@=Label for privacy statement link;ENU=Privacy Statement;PTG=Declara��o de Privacidade';
      UrlLbl@1026 : TextConst 'ENU=Website;PTG=Website';
      HelpLbl@1021 : TextConst 'ENU=Help;PTG=Ajuda';
      LanguageID@1023 : Integer;

    LOCAL PROCEDURE SetNavAppRecord@1();
    VAR
      DescriptionStream@1000 : InStream;
    BEGIN
      "Package ID" := NavAppTable."Package ID";
      ID := NavAppTable.ID;
      Name := NavAppTable.Name;
      Publisher := NavAppTable.Publisher;
      VersionDisplay :=
        NavExtensionInstallationMgmt.GetVersionDisplayString(
          NavAppTable."Version Major",NavAppTable."Version Minor",
          NavAppTable."Version Build",NavAppTable."Version Revision");
      NavAppTable.CALCFIELDS(Description);
      NavAppTable.Description.CREATEINSTREAM(DescriptionStream,TEXTENCODING::UTF8);
      AppDescription.READ(DescriptionStream);
      Url := NavAppTable.Url;
      Help := NavAppTable.Help;
      "Privacy Statement" := NavAppTable."Privacy Statement";
      EULA := NavAppTable.EULA;
      INSERT;
    END;

    BEGIN
    END.
  }
}

