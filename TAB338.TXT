OBJECT Table 338 Entry Summary
{
  OBJECT-PROPERTIES
  {
    Date=09/09/14;
    Time=12:00:00;
    Version List=NAVW18.00;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Entry Summary;
               PTG=Resumo Mov.];
  }
  FIELDS
  {
    { 1   ;   ;Entry No.           ;Integer       ;CaptionML=[ENU=Entry No.;
                                                              PTG=N� Mov.] }
    { 2   ;   ;Table ID            ;Integer       ;CaptionML=[ENU=Table ID;
                                                              PTG=N� Tabela] }
    { 3   ;   ;Summary Type        ;Text80        ;CaptionML=[ENU=Summary Type;
                                                              PTG=Tipo Resumo] }
    { 4   ;   ;Total Quantity      ;Decimal       ;CaptionML=[ENU=Total Quantity;
                                                              PTG=Quantidade Total];
                                                   DecimalPlaces=0:5 }
    { 5   ;   ;Total Reserved Quantity;Decimal    ;CaptionML=[ENU=Total Reserved Quantity;
                                                              PTG=Qtd. Total Reservada];
                                                   DecimalPlaces=0:5 }
    { 6   ;   ;Total Available Quantity;Decimal   ;CaptionML=[ENU=Total Available Quantity;
                                                              PTG=Qtd. Total Dispon�vel];
                                                   DecimalPlaces=0:5 }
    { 7   ;   ;Current Reserved Quantity;Decimal  ;CaptionML=[ENU=Current Reserved Quantity;
                                                              PTG=Qtd. Reservada Atualmente];
                                                   DecimalPlaces=0:5 }
    { 8   ;   ;Source Subtype      ;Integer       ;CaptionML=[ENU=Source Subtype;
                                                              PTG=Subtipo Origem] }
    { 15  ;   ;Qty. Alloc. in Warehouse;Decimal   ;CaptionML=[ENU=Qty. Alloc. in Warehouse;
                                                              PTG=Qtd. Atribu�da no Armaz�m];
                                                   DecimalPlaces=0:5 }
    { 16  ;   ;Res. Qty. on Picks & Shipmts.;Decimal;
                                                   CaptionML=[ENU=Res. Qty. on Picks & Shipmts.;
                                                              PTG=Qtd. Res. em Recolhas & Envios];
                                                   DecimalPlaces=0:5 }
    { 6500;   ;Serial No.          ;Code20        ;CaptionML=[ENU=Serial No.;
                                                              PTG=N� S�rie];
                                                   Editable=No }
    { 6501;   ;Lot No.             ;Code20        ;CaptionML=[ENU=Lot No.;
                                                              PTG=N� Lote];
                                                   Editable=No }
    { 6502;   ;Warranty Date       ;Date          ;CaptionML=[ENU=Warranty Date;
                                                              PTG=Data Garantia];
                                                   Editable=No }
    { 6503;   ;Expiration Date     ;Date          ;CaptionML=[ENU=Expiration Date;
                                                              PTG=Data Expira��o];
                                                   Editable=No }
    { 6504;   ;Total Requested Quantity;Decimal   ;CaptionML=[ENU=Total Requested Quantity;
                                                              PTG=Quantidade Pedida Total];
                                                   DecimalPlaces=0:5 }
    { 6505;   ;Selected Quantity   ;Decimal       ;OnValidate=BEGIN
                                                                IF "Bin Active" AND ("Total Available Quantity" > "Bin Content") THEN BEGIN
                                                                  IF "Selected Quantity" > "Bin Content" THEN
                                                                    ERROR(Text001,"Bin Content");
                                                                END ELSE
                                                                  IF "Selected Quantity" > "Total Available Quantity" THEN
                                                                    ERROR(Text001,"Total Available Quantity");
                                                              END;

                                                   CaptionML=[ENU=Selected Quantity;
                                                              PTG=Quantidade Selecionada];
                                                   DecimalPlaces=0:5;
                                                   MinValue=0;
                                                   BlankZero=Yes }
    { 6506;   ;Current Pending Quantity;Decimal   ;CaptionML=[ENU=Current Pending Quantity;
                                                              PTG=Quantidade Pendente Atual];
                                                   DecimalPlaces=0:5 }
    { 6507;   ;Current Requested Quantity;Decimal ;CaptionML=[ENU=Current Requested Quantity;
                                                              PTG=Quantidade Pedida Atual] }
    { 6508;   ;Bin Content         ;Decimal       ;AccessByPermission=TableData 5771=R;
                                                   CaptionML=[ENU=Bin Content;
                                                              PTG=Conte�do Posi��o];
                                                   DecimalPlaces=0:5;
                                                   Editable=No }
    { 6509;   ;Bin Active          ;Boolean       ;CaptionML=[ENU=Bin Active;
                                                              PTG=Posi��o Ativa];
                                                   Editable=No }
    { 6510;   ;Non-specific Reserved Qty.;Decimal ;CaptionML=[ENU=Non-specific Reserved Qty.;
                                                              PTG=Qtd. Reservada N�o Espec�fica];
                                                   Editable=No }
    { 6511;   ;Double-entry Adjustment;Decimal    ;CaptionML=[ENU=Double-entry Adjustment;
                                                              PTG=Ajuste de Partida Dobrada];
                                                   Editable=No }
  }
  KEYS
  {
    {    ;Entry No.                               ;Clustered=Yes }
    {    ;Lot No.,Serial No.                       }
    {    ;Expiration Date                          }
  }
  FIELDGROUPS
  {
  }
  CODE
  {
    VAR
      Text001@1000 : TextConst 'ENU=You cannot select more than %1 units.;PTG=N�o � poss�vel selecionar mais do que %1 unidade.';

    PROCEDURE UpdateAvailable@1();
    BEGIN
      "Total Available Quantity" :=
        "Total Quantity" -
        "Total Requested Quantity" -
        "Current Pending Quantity" +
        "Double-entry Adjustment";
    END;

    PROCEDURE HasQuantity@2() : Boolean;
    BEGIN
      EXIT(("Total Quantity" <> 0) OR
        ("Qty. Alloc. in Warehouse" <> 0) OR
        ("Total Requested Quantity" <> 0) OR
        ("Current Pending Quantity" <> 0) OR
        ("Double-entry Adjustment" <> 0));
    END;

    BEGIN
    END.
  }
}

