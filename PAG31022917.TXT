OBJECT Page 31022917 IES Statement Templates
{
  OBJECT-PROPERTIES
  {
    Date=01/02/15;
    Time=13:00:00;
    Version List=NAVPTSS81.00;
  }
  PROPERTIES
  {
    CaptionML=[ENU=IES Statement Templates;
               PTG=Modelos Declara��o IES];
    SourceTable=Table31022915;
    PageType=List;
    ActionList=ACTIONS
    {
      { 1900000003;0 ;ActionContainer;
                      ActionContainerType=RelatedInformation }
      { 1110008 ;1   ;ActionGroup;
                      Name=Te&mplate;
                      CaptionML=[ENU=Te&mplate;
                                 PTG=&Livro] }
      { 1110009 ;2   ;Action    ;
                      CaptionML=[ENU=Annexes;
                                 PTG=Anexos];
                      RunObject=Page 31022918;
                      RunPageLink=IES Statement Name=FIELD(Name) }
      { 1110016 ;2   ;Action    ;
                      Ellipsis=Yes;
                      CaptionML=[ENU=Copy Template;
                                 PTG=Copiar Modelo];
                      OnAction=BEGIN
                                 CopyTemplate;
                               END;
                                }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1110000;1;Group     ;
                GroupType=Repeater }

    { 1110001;2;Field     ;
                SourceExpr=Name }

    { 1110003;2;Field     ;
                SourceExpr=Description }

    { 1110010;2;Field     ;
                SourceExpr=xmlns }

    { 1110012;2;Field     ;
                SourceExpr="xmlns:xsi" }

    { 1110014;2;Field     ;
                SourceExpr="xsi:schemaLocation" }

    { 1110017;2;Field     ;
                SourceExpr="IES Schema Version" }

  }
  CODE
  {

    BEGIN
    END.
  }
}

