OBJECT Report 593 Intrastat - Make Disk Tax Auth
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0,NAVPTSS72.10;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Intrastat - Make Disk Tax Auth;
               PTG=Intrastat - Ficheiro Declara��o];
    ProcessingOnly=Yes;
    OnPreReport=VAR
                  "//--soft-local--//"@90000002 : Integer;
                  FileMgt@1000 : Codeunit 419;
                BEGIN
                  FileName := FileMgt.ServerTempFileName('');

                  IntrastatJnlLine4.COPYFILTERS("Intrastat Jnl. Line");
                  IF FileName = '' THEN
                    ERROR(Text000);
                  IntraFile.TEXTMODE := TRUE;
                  IntraFile.WRITEMODE := TRUE;
                  IntraFile.CREATE(FileName);
                  //soft,sn
                  IntraFile.WRITE(
                                      TextFLUXO + ';' +
                                      TextPERIODO + ';' +
                                      TextNIF + ';' +
                                      TextREF + ';' +
                                      TextNC + ';' +
                                      TextPAIS + ';' +
                                      TextPORIGEM + ';' +
                                      TextREGIAO + ';' +
                                      TextCODENT + ';' +
                                      TextNATTRA + ';' +
                                      TextMODTRA + ';' +
                                      TextAERPOR + ';' +
                                      TextMASSA + ';' +
                                      TextUNSUP + ';' +
                                      TextVALFAC + ';' +
                                      TextVALEST + ';' +
                                      TextADQNIF);
                  //soft,en
                END;

    OnPostReport=VAR
                   ToFile@1000 : Text[1024];
                 BEGIN
                   //soft,en
                   IntraFile.CLOSE;
                   FileMgt.DownloadToFile(ServerFileName,ClientFilename);
                   //soft,en
                 END;

  }
  DATASET
  {
    { 2880;    ;DataItem;                    ;
               DataItemTable=Table262;
               DataItemTableView=SORTING(Journal Template Name,Name);
               OnPreDataItem=BEGIN
                               IntrastatJnlLine4.COPYFILTER("Journal Template Name","Journal Template Name");
                               IntrastatJnlLine4.COPYFILTER("Journal Batch Name",Name);
                             END;

               OnAfterGetRecord=BEGIN
                                  TESTFIELD(Reported,FALSE);
                                  //soft,o IntraReferenceNo := "Statistics Period" + '000000';
                                  IntraReferenceNo := "Statistics Period" + '0000'; //soft,n
                                END;

               ReqFilterFields=Journal Template Name,Name }

    { 9905;1   ;DataItem;                    ;
               DataItemTable=Table263;
               OnPreDataItem=BEGIN
                               NrAd := 0;  //soft,n
                             END;

               OnAfterGetRecord=BEGIN
                                  //soft,sn
                                  JournalBatch.GET("Journal Template Name","Journal Batch Name");
                                  JournalBatch.TESTFIELD(JournalBatch.Reported,FALSE);
                                  //soft,en
                                  IF ("Tariff No." = '') AND
                                     ("Country/Region Code" = '') AND
                                     ("Transaction Type" = '') AND
                                     ("Transport Method" = '') AND
                                     ("Total Weight" = 0)
                                  THEN
                                    CurrReport.SKIP;

                                  "Tariff No." := DELCHR("Tariff No."); //soft,n
                                  TESTFIELD("Tariff No.");
                                  TESTFIELD("Country/Region Code");
                                  TESTFIELD("Transaction Type");
                                  TESTFIELD("Total Weight");
                                  //soft,so
                                  //IF "Supplementary Units" THEN
                                  //  TESTFIELD(Quantity);
                                  //CompoundField :=
                                  //  FORMAT("Country/Region Code",10) + FORMAT(DELCHR("Tariff No."),10) +
                                  //  FORMAT("Transaction Type",10) + FORMAT("Transport Method",10);

                                  //IF (TempType <> Type) OR (STRLEN(TempCompoundField) = 0) THEN BEGIN
                                  //  TempType := Type;
                                  //  TempCompoundField := CompoundField;
                                  //  IntraReferenceNo := COPYSTR(IntraReferenceNo,1,4) + FORMAT(Type,1,2) + '01001';
                                  //END ELSE
                                  //  IF TempCompoundField <> CompoundField THEN BEGIN
                                  //    TempCompoundField := CompoundField;
                                  //    IF COPYSTR(IntraReferenceNo,8,3) = '999' THEN
                                  //      IntraReferenceNo := INCSTR(COPYSTR(IntraReferenceNo,1,7)) + '001'
                                  //    ELSE
                                  //      IntraReferenceNo := INCSTR(IntraReferenceNo);
                                  //  END;
                                  //"Internal Ref. No." := IntraReferenceNo;
                                  //MODIFY;
                                  //soft,eo
                                  //soft,sn
                                  IF Country.GET("Country/Region Code") THEN
                                    CountryCode := Country."EU Country/Region Code"
                                  ELSE
                                    CountryCode := '';
                                  IF Country.GET("Country/Region of Origin Code") THEN
                                    CountryOfOriginCode := Country."EU Country/Region Code"
                                  ELSE
                                    CountryOfOriginCode := '';
                                  CASE "Statistical System" OF 0:
                                      StatSystem := '';
                                    "Statistical System"::"1-Final Destination":
                                      StatSystem := '1';
                                    "Statistical System"::"2-Temporary Destination":
                                      StatSystem := '2';
                                    "Statistical System"::"3-Temporary Destination+Transformation":
                                      StatSystem := '3';
                                    "Statistical System"::"4-Return":
                                      StatSystem := '4';
                                    "Statistical System"::"5-Return+Transformation":
                                      StatSystem := '5';
                                  END;

                                  NrAd := NrAd + 1;

                                  CLEAR(fluxo);
                                  IF "Intrastat Jnl. Line".Type = "Intrastat Jnl. Line".Type::Receipt THEN
                                    fluxo := 'INTRA-CH'
                                  ELSE
                                    fluxo := 'INTRA-EX';

                                  CompanyInfo.GET;
                                  CLEAR(porigem);
                                  CLEAR(Item);
                                  CLEAR(SourceNo);
                                  CLEAR(CountryCodeADQNIF);

                                  IF "Source Type" = "Source Type"::"FA Entry" THEN
                                    porigem :=  "Country/Region of Origin Code"
                                  ELSE BEGIN
                                    Item.GET("Intrastat Jnl. Line"."Item No.");
                                    porigem := Item."Country/Region of Origin Code";
                                  END;

                                  IF fluxo = 'INTRA-EX' THEN BEGIN
                                    ItemLedgEntry.GET("Intrastat Jnl. Line"."Source Entry No.");
                                    SourceNo := ItemLedgEntry."Source No.";
                                    CountryCodeADQNIF := CountryCode;
                                  END;

                                  IF NOT "Supplementary Units" THEN
                                    Quantity := 0;

                                  IF Quantity = 0 THEN
                                    TxtQuantity := ''
                                  ELSE
                                    TxtQuantity := DecimalNumeralZeroFormat(Quantity,10);

                                  IntraFile.WRITE(
                                                fluxo + ';' +
                                                "Intrastat Jnl. Batch"."Statistics Period" + ';' +
                                                CompanyInfo."VAT Registration No." + ';' +
                                                DecimalNumeralZeroFormat(NrAd,5) + ';' +
                                                PADSTR("Tariff No.",9) + ';' +
                                                PADSTR(CountryCode,2) + ';' +
                                                porigem + ';' +
                                                PADSTR(Area,2) + ';' +
                                                COPYSTR("Shipment Method Code #1",1,3) + ';' +
                                                COPYSTR("Transaction Type",1,2) + ';' +
                                                COPYSTR("Transport Method",1,1) + ';' +
                                                PADSTR("Entry/Exit Point",3) + ';' +
                                                DecimalNumeralZeroFormat("Total Weight",12) + ';' +
                                                TxtQuantity + ';' +
                                                DecimalNumeralZeroFormat(Amount,9) + ';' +
                                                DecimalNumeralZeroFormat("Statistical Value",9) + ';' +
                                                CountryCodeADQNIF+SourceNo);
                                  //soft,en
                                END;

               OnPostDataItem=VAR
                                ToFile@1000 : Text[1024];
                              BEGIN
                                "Intrastat Jnl. Batch".Reported := TRUE;
                                "Intrastat Jnl. Batch".MODIFY;
                              END;

               ReqFilterFields=Type;
               DataItemLink=Journal Template Name=FIELD(Journal Template Name),
                            Journal Batch Name=FIELD(Name) }

    { 2177;1   ;DataItem;IntrastatJnlLine2   ;
               DataItemTable=Table263;
               DataItemTableView=SORTING(Internal Ref. No.);
               OnPreDataItem=BEGIN
                               CompanyInfo.GET;
                               VATRegNo := CONVERTSTR(CompanyInfo."VAT Registration No.",Text001,'    ');
                               IntraFile.WRITE(FORMAT('00' + FORMAT(VATRegNo,8) + Text002,80));
                               IntraFile.WRITE(FORMAT('0100004',80));

                               SETRANGE("Internal Ref. No.",COPYSTR(IntraReferenceNo,1,4),COPYSTR(IntraReferenceNo,1,4) + '9');
                               CurrReport.CREATETOTALS(Quantity,"Statistical Value","Total Weight");

                               IntrastatJnlLine3.SETCURRENTKEY("Internal Ref. No.");
                             END;

               OnAfterGetRecord=BEGIN
                                  IF ("Tariff No." = '') AND
                                     ("Country/Region Code" = '') AND
                                     ("Transaction Type" = '') AND
                                     ("Transport Method" = '') AND
                                     ("Total Weight" = 0)
                                  THEN
                                    CurrReport.SKIP;
                                  "Tariff No." := DELCHR("Tariff No.");

                                  TotalWeightAmt += "Total Weight";
                                  QuantityAmt += Quantity;
                                  StatisticalValueAmt += "Statistical Value";

                                  IntrastatJnlLine5.COPY(IntrastatJnlLine2);
                                  IF IntrastatJnlLine5.NEXT = 1 THEN BEGIN
                                    IF (DELCHR(IntrastatJnlLine5."Tariff No.") = "Tariff No.") AND
                                       (IntrastatJnlLine5."Country/Region Code" = "Country/Region Code") AND
                                       (IntrastatJnlLine5."Transaction Type" = "Transaction Type") AND
                                       (IntrastatJnlLine5."Transport Method" = "Transport Method")
                                    THEN
                                      GroupTotal := FALSE
                                    ELSE
                                      GroupTotal := TRUE;
                                  END ELSE
                                    GroupTotal := TRUE;

                                  IF GroupTotal THEN BEGIN
                                    WriteGrTotalsToFile(TotalWeightAmt,QuantityAmt,StatisticalValueAmt);
                                    StatisticalValueTotalAmt += StatisticalValueAmt;
                                    TotalWeightAmt := 0;
                                    QuantityAmt := 0;
                                    StatisticalValueAmt := 0;
                                  END;
                                END;

               OnPostDataItem=BEGIN
                                IF NOT Receipt THEN
                                  IntraFile.WRITE(
                                    FORMAT(
                                      '02000' + FORMAT(IntraReferenceNo,4) + '100000' +
                                      FORMAT(VATRegNo,8) + '1' + FORMAT(IntraReferenceNo,4),
                                      80));
                                IF NOT Shipment THEN
                                  IntraFile.WRITE(
                                    FORMAT(
                                      '02000' + FORMAT(IntraReferenceNo,4) + '200000' +
                                      FORMAT(VATRegNo,8) + '2' + FORMAT(IntraReferenceNo,4),
                                      80));
                                IntraFile.WRITE(FORMAT('10' + DecimalNumeralZeroFormat(StatisticalValueTotalAmt,16),80));
                                IntraFile.CLOSE;

                                "Intrastat Jnl. Batch".Reported := TRUE;
                                "Intrastat Jnl. Batch".MODIFY;

                                IF ServerFileName = '' THEN
                                  FileMgt.DownloadHandler(FileName,'','',FileMgt.GetToFilterText('',DefaultFilenameTxt),DefaultFilenameTxt)
                                ELSE
                                  FileMgt.CopyServerFile(FileName,ServerFileName,TRUE);
                              END;
                               }

  }
  REQUESTPAGE
  {
    PROPERTIES
    {
      SaveValues=Yes;
    }
    CONTROLS
    {
    }
  }
  LABELS
  {
  }
  CODE
  {
    VAR
      Text000@1000 : TextConst 'ENU=Enter the file name.;PTG=Introduza o nome do ficheiro.';
      Text001@1001 : TextConst 'ENU=WwWw;PTG=WwWw';
      Text002@1002 : TextConst 'ENU=INTRASTAT;PTG=INTRASTAT';
      Text003@1003 : TextConst 'ENU=It is not possible to display %1 in a field with a length of %2.;PTG=N�o se podem mostrar %1 caracteres num campo cujo tamanho � %2.';
      IntrastatJnlLine3@1004 : Record 263;
      IntrastatJnlLine4@1005 : Record 263;
      IntrastatJnlLine5@1026 : Record 263;
      CompanyInfo@1006 : Record 79;
      Country@1007 : Record 9;
      FileMgt@1030 : Codeunit 419;
      IntraFile@1008 : File;
      QuantityAmt@1023 : Decimal;
      StatisticalValueAmt@1024 : Decimal;
      StatisticalValueTotalAmt@1028 : Decimal;
      TotalWeightAmt@1025 : Decimal;
      FileName@1009 : Text;
      IntraReferenceNo@1010 : Text[10];
      CompoundField@1011 : Text[40];
      TempCompoundField@1012 : Text[40];
      ServerFileName@1020 : Text;
      TempType@1013 : Integer;
      NoOfEntries@1014 : Text[3];
      Receipt@1015 : Boolean;
      Shipment@1016 : Boolean;
      VATRegNo@1017 : Code[20];
      ImportExport@1018 : Code[1];
      OK@1019 : Boolean;
      DefaultFilenameTxt@1022 : TextConst '@@@={Locked};ENU=Default.txt;PTG=Default.txt';
      GroupTotal@1027 : Boolean;
      ClientFilename@1021 : Text;
      "//--soft-global--//"@90000000 : Integer;
      Item@1000000022 : Record 27;
      JournalBatch@1110006 : Record 262;
      ItemLedgEntry@1000000024 : Record 32;
      StatSystem@1110003 : Code[1];
      CountryCode@1110004 : Code[3];
      CountryOfOriginCode@1110005 : Code[3];
      CountryCodeADQNIF@1000000026 : Code[10];
      NrAd@1110012 : Integer;
      integerpart@1000000023 : Integer;
      fluxo@1000000017 : Text[8];
      porigem@1000000021 : Text[2];
      SourceNo@1000000025 : Text;
      TxtQuantity@1000000027 : Text;
      "//--soft-text--//"@90000001 : TextConst;
      TextFLUXO@1000000000 : TextConst 'ENU=FLUXO;PTG=FLUXO';
      TextPERIODO@1000000001 : TextConst 'ENU=PERIODO;PTG=PERIODO';
      TextNIF@1000000002 : TextConst 'ENU=NIF;PTG=NIF';
      TextREF@1000000003 : TextConst 'ENU=REF;PTG=REF';
      TextNC@1000000004 : TextConst 'ENU=NC;PTG=NC';
      TextPAIS@1000000005 : TextConst 'ENU=PAIS;PTG=PAIS';
      TextPORIGEM@1000000006 : TextConst 'ENU=PORIGEM;PTG=PORIGEM';
      TextREGIAO@1000000007 : TextConst 'ENU=REGIAO;PTG=REGIAO';
      TextCODENT@1000000008 : TextConst 'ENU=CODENT;PTG=CODENT';
      TextNATTRA@1000000009 : TextConst 'ENU=NATTRA;PTG=NATTRA';
      TextMODTRA@1000000010 : TextConst 'ENU=MODTRA;PTG=MODTRA';
      TextAERPOR@1000000011 : TextConst 'ENU=AERPOR;PTG=AERPOR';
      TextMASSA@1000000012 : TextConst 'ENU=MASSA;PTG=MASSA';
      TextUNSUP@1000000013 : TextConst 'ENU=UNSUP;PTG=UNSUP';
      TextVALFAC@1000000014 : TextConst 'ENU=VALFAC;PTG=VALFAC';
      TextVALEST@1000000015 : TextConst 'ENU=VALEST;PTG=VALEST';
      TextADQNIF@1000000016 : TextConst 'ENU=ADQNIF;PTG=ADQNIF';
      FilterStringTxt2@1000000028 : TextConst 'ENU=Text Files (*.txt)|*.txt;PTG=Ficheiros Texto (*.txt)|*.txt';

    LOCAL PROCEDURE DecimalNumeralZeroFormat@1(DecimalNumeral@1000 : Decimal;Length@1001 : Integer) : Text[250];
    BEGIN
      EXIT(TextZeroFormat(DELCHR(FORMAT(ROUND(ABS(DecimalNumeral),1,'<'),0,1)),Length));
    END;

    LOCAL PROCEDURE TextZeroFormat@2(Text@1000 : Text[250];Length@1001 : Integer) : Text[250];
    BEGIN
      IF STRLEN(Text) > Length THEN
        ERROR(Text003,Text,Length);
      //soft,o  EXIT(PADSTR('',Length - STRLEN(Text),'0') + Text);
      EXIT(Text);
    END;

    PROCEDURE InitializeRequest@4(newServerFileName@1000 : Text);
    BEGIN
      ServerFileName := newServerFileName;
    END;

    PROCEDURE WriteGrTotalsToFile@3(TotalWeightAmt@1000 : Decimal;QuantityAmt@1001 : Decimal;StatisticalValueAmt@1002 : Decimal);
    BEGIN
      //soft,so
      //WITH IntrastatJnlLine2 DO BEGIN
      //  OK := COPYSTR("Internal Ref. No.",8,3) = '001';
      //  IF OK THEN BEGIN
      //    IntrastatJnlLine3.SETRANGE(
      //      "Internal Ref. No.",
      //      COPYSTR("Internal Ref. No.",1,7) + '000',
      //      COPYSTR("Internal Ref. No.",1,7) + '999');
      //    IntrastatJnlLine3.FINDLAST;
      //    NoOfEntries := COPYSTR(IntrastatJnlLine3."Internal Ref. No.",8,3);
      //  END;
      //  ImportExport := INCSTR(FORMAT(Type,1,2));
      //
      //  IF Type = Type::Receipt THEN
      //    Receipt := TRUE
      //  ELSE
      //    Shipment := TRUE;
      //  Country.GET("Country/Region Code");
      //  Country.TESTFIELD("Intrastat Code");
      //
      //  IF OK THEN
      //    IntraFile.WRITE(
      //      FORMAT(
      //        '02' +
      //        TextZeroFormat(DELCHR(NoOfEntries),3) +
      //        FORMAT(COPYSTR(IntrastatJnlLine3."Internal Ref. No.",1,7) + '000',10) +
      //        FORMAT(VATRegNo,8) + FORMAT(ImportExport,1) + FORMAT(IntraReferenceNo,4),
      //        80));
      //
      //  IntraFile.WRITE(
      //    FORMAT(
      //      '03' +
      //      TextZeroFormat(COPYSTR("Internal Ref. No.",8,3),3) +
      //      FORMAT("Internal Ref. No.",10) + FORMAT(Country."Intrastat Code",3) + FORMAT("Transaction Type",2) +
      //      '0' + FORMAT("Transport Method",1) + PADSTR("Tariff No.",9,'0') +
      //      DecimalNumeralZeroFormat(ROUND(TotalWeightAmt,1,'>'),15) +
      //      DecimalNumeralZeroFormat(QuantityAmt,10) +
      //      DecimalNumeralZeroFormat(StatisticalValueAmt,15),
      //      80));
      //END;
    END;

    BEGIN
    END.
  }
  RDLDATA
  {
  }
}

