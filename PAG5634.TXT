OBJECT Page 5634 Recurring Fixed Asset Journal
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Recurring Fixed Asset Journal;
               PTG=Di�rio Peri�dico Imobilizado];
    SaveValues=Yes;
    SourceTable=Table5621;
    DelayedInsert=Yes;
    DataCaptionFields=Journal Batch Name;
    PageType=Worksheet;
    AutoSplitKey=Yes;
    OnOpenPage=VAR
                 JnlSelected@1000 : Boolean;
               BEGIN
                 IF IsOpenedFromBatch THEN BEGIN
                   CurrentJnlBatchName := "Journal Batch Name";
                   FAJnlManagement.OpenJournal(CurrentJnlBatchName,Rec);
                   EXIT;
                 END;
                 FAJnlManagement.TemplateSelection(PAGE::"Recurring Fixed Asset Journal",TRUE,Rec,JnlSelected);
                 IF NOT JnlSelected THEN
                   ERROR('');
                 FAJnlManagement.OpenJournal(CurrentJnlBatchName,Rec);
               END;

    OnAfterGetRecord=BEGIN
                       ShowShortcutDimCode(ShortcutDimCode);
                     END;

    OnNewRecord=BEGIN
                  SetUpNewLine(xRec);
                  CLEAR(ShortcutDimCode);
                END;

    OnAfterGetCurrRecord=BEGIN
                           FAJnlManagement.GetFA("FA No.",FADescription);
                         END;

    ActionList=ACTIONS
    {
      { 1900000003;0 ;ActionContainer;
                      ActionContainerType=RelatedInformation }
      { 62      ;1   ;ActionGroup;
                      CaptionML=[ENU=&Line;
                                 PTG=&Linha];
                      Image=Line }
      { 63      ;2   ;Action    ;
                      AccessByPermission=TableData 348=R;
                      ShortCutKey=Shift+Ctrl+D;
                      CaptionML=[ENU=Dimensions;
                                 PTG=Dimens�es];
                      ToolTipML=[ENU=View or edits dimensions, such as area, project, or department, that you can assign to sales and purchase documents to distribute costs and analyze transaction history.;
                                 PTG=""];
                      ApplicationArea=#Suite;
                      Image=Dimensions;
                      OnAction=BEGIN
                                 ShowDimensions;
                                 CurrPage.SAVERECORD;
                               END;
                                }
      { 44      ;1   ;ActionGroup;
                      CaptionML=[ENU=Fixed &Asset;
                                 PTG=Imobiliz&ado];
                      Image=FixedAssets }
      { 46      ;2   ;Action    ;
                      ShortCutKey=Shift+F7;
                      CaptionML=[ENU=Card;
                                 PTG=Ficha];
                      ToolTipML=[ENU=View or edit detailed information about the fixed asset.;
                                 PTG=""];
                      ApplicationArea=#FixedAssets;
                      RunObject=Page 5600;
                      RunPageLink=No.=FIELD(FA No.);
                      Image=EditLines }
      { 47      ;2   ;Action    ;
                      ShortCutKey=Ctrl+F7;
                      CaptionML=[ENU=Ledger E&ntries;
                                 PTG=Movime&ntos];
                      ToolTipML=ENU=View the ledger entries for the selected fixed asset.;
                      ApplicationArea=#FixedAssets;
                      RunObject=Codeunit 5634;
                      Promoted=No;
                      Image=FixedAssetLedger;
                      PromotedCategory=Process }
      { 1900000004;0 ;ActionContainer;
                      ActionContainerType=ActionItems }
      { 45      ;1   ;ActionGroup;
                      CaptionML=[ENU=P&osting;
                                 PTG=&Registo];
                      Image=Post }
      { 49      ;2   ;Action    ;
                      Ellipsis=Yes;
                      CaptionML=[ENU=Test Report;
                                 PTG=Verificar...];
                      ToolTipML=[ENU=View a test report so that you can find and correct any errors before you perform the actual posting of the journal or document.;
                                 PTG=""];
                      ApplicationArea=#FixedAssets;
                      Image=TestReport;
                      OnAction=BEGIN
                                 ReportPrint.PrintFAJnlLine(Rec);
                               END;
                                }
      { 50      ;2   ;Action    ;
                      ShortCutKey=F9;
                      CaptionML=[ENU=P&ost;
                                 PTG=R&egistar];
                      ToolTipML=[ENU=Finalize the document or journal by posting the amounts and quantities to the related accounts in your company books.;
                                 PTG=""];
                      ApplicationArea=#FixedAssets;
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=PostOrder;
                      PromotedCategory=Process;
                      OnAction=BEGIN
                                 CODEUNIT.RUN(CODEUNIT::"FA. Jnl.-Post",Rec);
                                 CurrentJnlBatchName := GETRANGEMAX("Journal Batch Name");
                                 CurrPage.UPDATE(FALSE);
                               END;
                                }
      { 4       ;2   ;Action    ;
                      Name=Preview;
                      CaptionML=[ENU=Preview Posting;
                                 PTG=Pr�-Visualizar Registo];
                      ToolTipML=[ENU=Review the different types of entries that will be created when you post the document or journal.;
                                 PTG=""];
                      ApplicationArea=#FixedAssets;
                      Image=ViewPostedOrder;
                      OnAction=VAR
                                 FAJnlPost@1000 : Codeunit 5636;
                               BEGIN
                                 FAJnlPost.Preview(Rec);
                               END;
                                }
      { 51      ;2   ;Action    ;
                      ShortCutKey=Shift+F9;
                      CaptionML=[ENU=Post and &Print;
                                 PTG=Registar e &Imprimir];
                      ToolTipML=[ENU=Finalize and prepare to print the document or journal. The values and quantities are posted to the related accounts. A report request window where you can specify what to include on the print-out.;
                                 PTG=""];
                      ApplicationArea=#FixedAssets;
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=PostPrint;
                      PromotedCategory=Process;
                      OnAction=BEGIN
                                 CODEUNIT.RUN(CODEUNIT::"FA. Jnl.-Post+Print",Rec);
                                 CurrentJnlBatchName := GETRANGEMAX("Journal Batch Name");
                                 CurrPage.UPDATE(FALSE);
                               END;
                                }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 42  ;1   ;Field     ;
                Lookup=Yes;
                CaptionML=[ENU=Batch Name;
                           PTG=Nome Sec��o];
                ToolTipML=[ENU=Specifies the name of the journal batch of the fixed asset journal.;
                           PTG=""];
                ApplicationArea=#FixedAssets;
                SourceExpr=CurrentJnlBatchName;
                OnValidate=BEGIN
                             FAJnlManagement.CheckName(CurrentJnlBatchName,Rec);
                             CurrentJnlBatchNameOnAfterVali;
                           END;

                OnLookup=BEGIN
                           CurrPage.SAVERECORD;
                           FAJnlManagement.LookupName(CurrentJnlBatchName,Rec);
                           CurrPage.UPDATE(FALSE);
                         END;
                          }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 54  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies a recurring method, if you have indicated that the journal is recurring.;
                           PTG=""];
                ApplicationArea=#FixedAssets;
                SourceExpr="Recurring Method" }

    { 56  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies a recurring frequency if you indicated that the journal is a recurring.;
                           PTG=""];
                ApplicationArea=#FixedAssets;
                SourceExpr="Recurring Frequency" }

    { 3   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the date that will be used as the posting date on FA ledger entries.;
                           PTG=""];
                ApplicationArea=#FixedAssets;
                SourceExpr="FA Posting Date" }

    { 17  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the same date as the FA Posting Date field when the line is posted.;
                           PTG=""];
                ApplicationArea=#FixedAssets;
                SourceExpr="Posting Date";
                Visible=FALSE }

    { 5   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the appropriate document type for the amount you want to post.;
                           PTG=""];
                ApplicationArea=#FixedAssets;
                SourceExpr="Document Type" }

    { 7   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies a document number for the journal line.;
                           PTG=""];
                ApplicationArea=#FixedAssets;
                SourceExpr="Document No." }

    { 9   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the number of the resource you want to post an entry for.;
                           PTG=""];
                ApplicationArea=#FixedAssets;
                SourceExpr="FA No.";
                OnValidate=BEGIN
                             FAJnlManagement.GetFA("FA No.",FADescription);
                           END;
                            }

    { 18  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the code for the depreciation book to which the line will be posted.;
                           PTG=""];
                ApplicationArea=#FixedAssets;
                SourceExpr="Depreciation Book Code" }

    { 11  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the appropriate posting type for the amount you want to post.;
                           PTG=""];
                ApplicationArea=#FixedAssets;
                SourceExpr="FA Posting Type" }

    { 13  ;2   ;Field     ;
                ToolTipML=[ENU=Automatically retrieves the description from the FA card when the FA No. field is filled in.;
                           PTG=""];
                ApplicationArea=#FixedAssets;
                SourceExpr=Description }

    { 15  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the total amount the journal line consists of.;
                           PTG=""];
                ApplicationArea=#FixedAssets;
                SourceExpr=Amount }

    { 38  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the code for Shortcut Dimension 1.;
                           PTG=""];
                ApplicationArea=#Suite;
                SourceExpr="Shortcut Dimension 1 Code";
                Visible=FALSE }

    { 31  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the code for Shortcut Dimension 2.;
                           PTG=""];
                ApplicationArea=#Suite;
                SourceExpr="Shortcut Dimension 2 Code";
                Visible=FALSE }

    { 300 ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the dimension value code linked to the journal line.;
                           PTG=""];
                ApplicationArea=#Suite;
                SourceExpr=ShortcutDimCode[3];
                CaptionClass='1,2,3';
                TableRelation="Dimension Value".Code WHERE (Global Dimension No.=CONST(3),
                                                            Dimension Value Type=CONST(Standard),
                                                            Blocked=CONST(No));
                Visible=FALSE;
                OnValidate=BEGIN
                             ValidateShortcutDimCode(3,ShortcutDimCode[3]);
                           END;
                            }

    { 302 ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the dimension value code linked to the journal line.;
                           PTG=""];
                ApplicationArea=#Suite;
                SourceExpr=ShortcutDimCode[4];
                CaptionClass='1,2,4';
                TableRelation="Dimension Value".Code WHERE (Global Dimension No.=CONST(4),
                                                            Dimension Value Type=CONST(Standard),
                                                            Blocked=CONST(No));
                Visible=FALSE;
                OnValidate=BEGIN
                             ValidateShortcutDimCode(4,ShortcutDimCode[4]);
                           END;
                            }

    { 304 ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the dimension value code linked to the journal line.;
                           PTG=""];
                ApplicationArea=#Suite;
                SourceExpr=ShortcutDimCode[5];
                CaptionClass='1,2,5';
                TableRelation="Dimension Value".Code WHERE (Global Dimension No.=CONST(5),
                                                            Dimension Value Type=CONST(Standard),
                                                            Blocked=CONST(No));
                Visible=FALSE;
                OnValidate=BEGIN
                             ValidateShortcutDimCode(5,ShortcutDimCode[5]);
                           END;
                            }

    { 306 ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the dimension value code linked to the journal line.;
                           PTG=""];
                ApplicationArea=#Suite;
                SourceExpr=ShortcutDimCode[6];
                CaptionClass='1,2,6';
                TableRelation="Dimension Value".Code WHERE (Global Dimension No.=CONST(6),
                                                            Dimension Value Type=CONST(Standard),
                                                            Blocked=CONST(No));
                Visible=FALSE;
                OnValidate=BEGIN
                             ValidateShortcutDimCode(6,ShortcutDimCode[6]);
                           END;
                            }

    { 308 ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the dimension value code linked to the journal line.;
                           PTG=""];
                ApplicationArea=#Suite;
                SourceExpr=ShortcutDimCode[7];
                CaptionClass='1,2,7';
                TableRelation="Dimension Value".Code WHERE (Global Dimension No.=CONST(7),
                                                            Dimension Value Type=CONST(Standard),
                                                            Blocked=CONST(No));
                Visible=FALSE;
                OnValidate=BEGIN
                             ValidateShortcutDimCode(7,ShortcutDimCode[7]);
                           END;
                            }

    { 310 ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the dimension value code linked to the journal line.;
                           PTG=""];
                ApplicationArea=#Suite;
                SourceExpr=ShortcutDimCode[8];
                CaptionClass='1,2,8';
                TableRelation="Dimension Value".Code WHERE (Global Dimension No.=CONST(8),
                                                            Dimension Value Type=CONST(Standard),
                                                            Blocked=CONST(No));
                Visible=FALSE;
                OnValidate=BEGIN
                             ValidateShortcutDimCode(8,ShortcutDimCode[8]);
                           END;
                            }

    { 29  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies whether to automatically post depreciation of the existing (old) fixed asset.;
                           PTG=""];
                ApplicationArea=#FixedAssets;
                SourceExpr="Depr. until FA Posting Date";
                Visible=FALSE }

    { 25  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies a maintenance code.;
                           PTG=""];
                ApplicationArea=#FixedAssets;
                SourceExpr="Maintenance Code";
                Visible=FALSE }

    { 27  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies an insurance code if you have selected the Acquisition Cost option in the FA Posting Type field.;
                           PTG=""];
                ApplicationArea=#FixedAssets;
                SourceExpr="Insurance No.";
                Visible=FALSE }

    { 36  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies a fixed asset number.;
                           PTG=""];
                ApplicationArea=#Suite;
                SourceExpr="Budgeted FA No.";
                Visible=FALSE }

    { 60  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies a depreciation book code if you want the journal line to be posted to that depreciation book, as well as to the depreciation book in the Depreciation Book Code field.;
                           PTG=""];
                ApplicationArea=#FixedAssets;
                SourceExpr="Duplicate in Depreciation Book";
                Visible=FALSE }

    { 20  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies whether the line is to be posted to all depreciation books, using different journal batches and with a check mark in the Part of Duplication List field.;
                           PTG=""];
                ApplicationArea=#FixedAssets;
                SourceExpr="Use Duplication List";
                Visible=FALSE }

    { 21  ;2   ;Field     ;
                ToolTipML=[ENU=Automatically selects the field if the entry was generated from an FA reclassification journal.;
                           PTG=""];
                ApplicationArea=#FixedAssets;
                SourceExpr="FA Reclassification Entry";
                Visible=FALSE }

    { 52  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies whether to post an indexation.;
                           PTG=""];
                ApplicationArea=#FixedAssets;
                SourceExpr="Index Entry";
                Visible=FALSE }

    { 33  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the number of a posted FA ledger entry to mark as an error entry.;
                           PTG=""];
                ApplicationArea=#FixedAssets;
                SourceExpr="FA Error Entry No.";
                Visible=FALSE }

    { 58  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the last date on which the recurring journal will be posted.;
                           PTG=""];
                ApplicationArea=#FixedAssets;
                SourceExpr="Expiration Date" }

    { 2   ;1   ;Group      }

    { 1900116601;2;Group  ;
                GroupType=FixedLayout }

    { 1901313201;3;Group  ;
                CaptionML=[ENU=FA Description;
                           PTG=Descri��o Imobilizado] }

    { 40  ;4   ;Field     ;
                ToolTipML=[ENU=Specifies a description of the fixed asset.;
                           PTG=""];
                ApplicationArea=#FixedAssets;
                SourceExpr=FADescription;
                Editable=FALSE;
                ShowCaption=No }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {
    VAR
      FAJnlManagement@1000 : Codeunit 5638;
      ReportPrint@1001 : Codeunit 228;
      CurrentJnlBatchName@1002 : Code[10];
      FADescription@1003 : Text[30];
      ShortcutDimCode@1004 : ARRAY [8] OF Code[20];

    LOCAL PROCEDURE CurrentJnlBatchNameOnAfterVali@19002411();
    BEGIN
      CurrPage.SAVERECORD;
      FAJnlManagement.SetName(CurrentJnlBatchName,Rec);
      CurrPage.UPDATE(FALSE);
    END;

    BEGIN
    END.
  }
}

