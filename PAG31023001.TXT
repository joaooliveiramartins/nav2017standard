OBJECT Page 31023001 Customer Ratings
{
  OBJECT-PROPERTIES
  {
    Date=01/02/15;
    Time=13:00:00;
    Version List=NAVPTSS81.00;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Customer Ratings;
               PTG=Risco Clientes];
    SourceTable=Table31022955;
    DataCaptionExpr=Caption;
    PageType=List;
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 2   ;2   ;Field     ;
                SourceExpr="Customer No." }

    { 4   ;2   ;Field     ;
                SourceExpr="Risk Percentage" }

  }
  CODE
  {

    BEGIN
    END.
  }
}

