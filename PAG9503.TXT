OBJECT Page 9503 Debugger Watch Value FactBox
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0;
  }
  PROPERTIES
  {
    Editable=No;
    CaptionML=[ENU=Debugger Watch Value FactBox;
               PTG=Caixa Factos Controlo Valor Debugger];
    LinksAllowed=No;
    SourceTable=Table2000000103;
    PageType=ListPart;
    ActionList=ACTIONS
    {
      { 6       ;    ;ActionContainer;
                      ActionContainerType=ActionItems }
      { 9       ;1   ;Separator  }
      { 7       ;1   ;Action    ;
                      Name=Delete Watch;
                      ShortCutKey=Ctrl+Delete;
                      CaptionML=[ENU=Delete Watch;
                                 PTG=Eliminar Controlo];
                      ToolTipML=[ENU=Delete the selected variables from the watch list.;
                                 PTG=Eliminar as vari veis assinaladas da lista de controlo.];
                      ApplicationArea=#All;
                      Image=Delete;
                      OnAction=VAR
                                 DebuggerWatch@1000 : Record 2000000104;
                                 DebuggerWatchValue@1002 : Record 2000000103;
                                 DebuggerWatchValueTemp@1003 : TEMPORARY Record 2000000103;
                               BEGIN
                                 CurrPage.SETSELECTIONFILTER(DebuggerWatchValue);

                                 // Copy the selected records to take a snapshot of the watches,
                                 // otherwise the DELETEALL would dynamically change the Debugger Watch Value primary keys
                                 // causing incorrect records to be deleted.

                                 IF DebuggerWatchValue.FIND('-') THEN
                                   REPEAT
                                     DebuggerWatchValueTemp := DebuggerWatchValue;
                                     DebuggerWatchValueTemp.INSERT;
                                   UNTIL DebuggerWatchValue.NEXT = 0;

                                 IF DebuggerWatchValueTemp.FIND('-') THEN BEGIN
                                   REPEAT
                                     DebuggerWatch.SETRANGE(Path,DebuggerWatchValueTemp.Name);
                                     DebuggerWatch.DELETEALL(TRUE);
                                   UNTIL DebuggerWatchValueTemp.NEXT = 0;
                                 END;
                               END;
                                }
    }
  }
  CONTROLS
  {
    { 1   ;0   ;Container ;
                ContainerType=ContentArea }

    { 2   ;1   ;Group     ;
                Name=Group;
                GroupType=Repeater }

    { 3   ;2   ;Field     ;
                CaptionML=[ENU=Name;
                           PTG=Nome];
                ApplicationArea=#All;
                SourceExpr=Name }

    { 4   ;2   ;Field     ;
                CaptionML=[ENU=Value;
                           PTG=Valor];
                ApplicationArea=#All;
                SourceExpr=Value }

    { 5   ;2   ;Field     ;
                CaptionML=[ENU=Type;
                           PTG=Tipo];
                ApplicationArea=#All;
                SourceExpr=Type }

  }
  CODE
  {

    BEGIN
    END.
  }
}

