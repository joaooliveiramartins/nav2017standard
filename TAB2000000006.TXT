OBJECT Table 2000000006 Company
{
  OBJECT-PROPERTIES
  {
    Date=13/04/17;
    Time=14:36:25;
    Version List=NAVW110.0;
  }
  PROPERTIES
  {
    DataPerCompany=No;
    CaptionML=[ENU=Company;
               PTG=Empresa];
  }
  FIELDS
  {
    { 1   ;   ;Name                ;Text30        ;CaptionML=[ENU=Name;
                                                              PTG=Nome] }
    { 2   ;   ;Evaluation Company  ;Boolean       ;CaptionML=[ENU=Evaluation Company;
                                                              PTG=Empresa Demonstra��o] }
  }
  KEYS
  {
    {    ;Name                                    ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    END.
  }
}

