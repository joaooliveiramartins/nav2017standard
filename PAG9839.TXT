OBJECT Page 9839 Printer Selections FactBox
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0;
  }
  PROPERTIES
  {
    Editable=No;
    CaptionML=[ENU=Printer Selections FactBox;
               PTG=Caixa Factos Sele��es Impressora];
    SourceTable=Table78;
    PageType=ListPart;
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 2   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the ID of the user for whom you want to define permissions.;
                           PTG=""];
                SourceExpr="User ID";
                Visible=FALSE }

    { 11  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the report that will be linked to a particular user and/or printer.;
                           PTG=""];
                SourceExpr="Report ID";
                LookupPageID=Objects }

    { 4   ;2   ;Field     ;
                DrillDown=No;
                ToolTipML=[ENU=Specifies the name of the report.;
                           PTG=""];
                SourceExpr="Report Caption" }

    { 6   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the printer that the user will be allowed to use or on which the report will be printed.;
                           PTG=""];
                SourceExpr="Printer Name";
                LookupPageID=Printers }

  }
  CODE
  {

    BEGIN
    END.
  }
}

