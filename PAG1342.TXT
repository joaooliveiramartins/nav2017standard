OBJECT Page 1342 Item Template Card
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Item Template;
               PTG=Modelo  Produto];
    SourceTable=Table1301;
    DataCaptionExpr="Template Name";
    PageType=Card;
    SourceTableTemporary=Yes;
    CardPageID=Item Template Card;
    PromotedActionCategoriesML=[ENU=New,Process,Reports,Master Data;
                                PTG=Novo,Processar,Mapas,Dados Mestre];
    OnOpenPage=BEGIN
                 IF Item."No." <> '' THEN
                   CreateConfigTemplateFromExistingItem(Item,Rec);
               END;

    OnDeleteRecord=BEGIN
                     CheckTemplateNameProvided
                   END;

    OnQueryClosePage=BEGIN
                       CASE CloseAction OF
                         ACTION::LookupOK:
                           IF Code <> '' THEN
                             CheckTemplateNameProvided;
                         ACTION::LookupCancel:
                           IF DELETE(TRUE) THEN;
                       END;
                     END;

    OnAfterGetCurrRecord=BEGIN
                           SetInventoryPostingGroupEditable;
                           SetDimensionsEnabled;
                           SetTemplateEnabled;
                         END;

    ActionList=ACTIONS
    {
      { 22      ;    ;ActionContainer;
                      ActionContainerType=ActionItems }
      { 3       ;1   ;ActionGroup;
                      CaptionML=[ENU=Master Data;
                                 PTG=Dados Mestre] }
      { 24      ;2   ;Action    ;
                      Name=Default Dimensions;
                      ShortCutKey=Shift+Ctrl+D;
                      CaptionML=[ENU=Dimensions;
                                 PTG=Dimens�es];
                      ToolTipML=ENU=View or edits dimensions, such as area, project, or department, that you can assign to sales and purchase documents to distribute costs and analyze transaction history.;
                      ApplicationArea=#Suite;
                      RunObject=Page 1343;
                      RunPageLink=Table Id=CONST(27),
                                  Master Record Template Code=FIELD(Code);
                      Promoted=Yes;
                      Enabled=DimensionsEnabled;
                      PromotedIsBig=Yes;
                      Image=Dimensions;
                      PromotedCategory=Category4 }
    }
  }
  CONTROLS
  {
    { 1   ;0   ;Container ;
                ContainerType=ContentArea }

    { 2   ;1   ;Group     ;
                Name=General;
                CaptionML=[ENU=General;
                           PTG=Geral];
                GroupType=Group }

    { 4   ;2   ;Field     ;
                ToolTipML=ENU=Specifies the name of the template.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Template Name";
                OnValidate=BEGIN
                             SetDimensionsEnabled;
                           END;
                            }

    { 23  ;2   ;Field     ;
                Name=TemplateEnabled;
                CaptionML=[ENU=Enabled;
                           PTG=Ativo];
                ToolTipML=ENU=Specifies if the template is ready to be used;
                ApplicationArea=#Basic,#Suite;
                SourceExpr=TemplateEnabled;
                OnValidate=VAR
                             ConfigTemplateHeader@1000 : Record 8618;
                           BEGIN
                             IF ConfigTemplateHeader.GET(Code) THEN
                               ConfigTemplateHeader.SetTemplateEnabled(TemplateEnabled);
                           END;
                            }

    { 18  ;1   ;Group     ;
                Name=Item Setup;
                CaptionML=[ENU=Item Setup;
                           PTG=Configura��o Produto];
                GroupType=Group }

    { 5   ;2   ;Field     ;
                ToolTipML=ENU=Specifies the unit in which the item is held in inventory. The base unit of measure also serves as the conversion basis for alternate units of measure.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Base Unit of Measure" }

    { 6   ;2   ;Field     ;
                ToolTipML=ENU=Specifies if the item card represents a physical item (Inventory) or a service (Service).;
                ApplicationArea=#Basic,#Suite;
                SourceExpr=Type;
                OnValidate=BEGIN
                             SetInventoryPostingGroupEditable;
                           END;
                            }

    { 16  ;2   ;Field     ;
                ToolTipML=ENU=Specifies that an extended text will be added on sales or purchase documents for this item.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Automatic Ext. Texts" }

    { 21  ;1   ;Group     ;
                Name=Price;
                CaptionML=[ENU=Price;
                           PTG=Pre�o];
                GroupType=Group }

    { 14  ;2   ;Field     ;
                ToolTipML=ENU=Specifies if the Unit Price and Line Amount fields on sales document lines for this item should be shown with or without VAT.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Price Includes VAT" }

    { 10  ;2   ;Field     ;
                ToolTipML=ENU=Specifies if the Profit Percentage field, the Unit Price field, or neither field is calculated and filled.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Price/Profit Calculation" }

    { 11  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the profit you have made from the customer in the current fiscal year, as a percentage of the customer's total sales.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Profit %" }

    { 9   ;2   ;Field     ;
                ToolTipML=ENU=Specifies if the item should be included in the calculation of an invoice discount on documents where the item is traded.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Allow Invoice Disc." }

    { 8   ;2   ;Field     ;
                ToolTipML=ENU=Specifies an item group code that can be used as a criterion to grant a discount when the item is sold to a certain customer.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Item Disc. Group" }

    { 20  ;1   ;Group     ;
                Name=Cost;
                CaptionML=[ENU=Cost;
                           PTG=Custo];
                GroupType=Group }

    { 12  ;2   ;Field     ;
                ToolTipML=ENU=Specifies links between business transactions made for this item and the general ledger, to account for VAT amounts that result from trade with the item.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Costing Method" }

    { 13  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the percentage of the item's last purchase cost that includes indirect costs, such as freight that is associated with the purchase of the item.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Indirect Cost %" }

    { 19  ;1   ;Group     ;
                Name=Financial Details;
                CaptionML=[ENU=Financial Details;
                           PTG=Detalhes Financeiros];
                GroupType=Group }

    { 15  ;2   ;Field     ;
                ToolTipML=ENU=Specifies links between business transactions made for this item and the general ledger, to account for the value of trade with the item.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Gen. Prod. Posting Group" }

    { 17  ;2   ;Field     ;
                ToolTipML=ENU=Specifies links between business transactions made for this item and the general ledger, to account for VAT amounts that result from trade.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="VAT Prod. Posting Group" }

    { 7   ;2   ;Field     ;
                ToolTipML=ENU=Specifies links between business transactions made for the item and an inventory account in the general ledger, to group amounts for that item type.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Inventory Posting Group";
                Editable=InventoryPostingGroupEditable }

    { 27  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the tax group code for the tax-detail entry.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Tax Group Code" }

    { 25  ;1   ;Group     ;
                Name=Categorization;
                CaptionML=[ENU=Categorization;
                           PTG=Categoriza��o];
                GroupType=Group }

    { 26  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the category that the item belongs to.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Item Category Code" }

    { 28  ;2   ;Field     ;
                SourceExpr="Service Item Group" }

    { 29  ;2   ;Field     ;
                SourceExpr="Warehouse Class Code" }

  }
  CODE
  {
    VAR
      Item@1003 : Record 27;
      InventoryPostingGroupEditable@1000 : Boolean INDATASET;
      DimensionsEnabled@1001 : Boolean INDATASET;
      ProvideTemplateNameErr@1002 : TextConst '@@@=%1 Template Name;ENU=You must enter a %1.;PTG=Tem de introduzir um %1.';
      TemplateEnabled@1004 : Boolean;

    PROCEDURE SetInventoryPostingGroupEditable@1();
    BEGIN
      InventoryPostingGroupEditable := Type = Type::Inventory;
    END;

    LOCAL PROCEDURE SetDimensionsEnabled@4();
    BEGIN
      DimensionsEnabled := "Template Name" <> '';
    END;

    LOCAL PROCEDURE SetTemplateEnabled@5();
    VAR
      ConfigTemplateHeader@1000 : Record 8618;
    BEGIN
      TemplateEnabled := ConfigTemplateHeader.GET(Code) AND ConfigTemplateHeader.Enabled;
    END;

    LOCAL PROCEDURE CheckTemplateNameProvided@2();
    BEGIN
      IF "Template Name" = '' THEN
        ERROR(STRSUBSTNO(ProvideTemplateNameErr,FIELDCAPTION("Template Name")));
    END;

    PROCEDURE CreateFromItem@3(FromItem@1000 : Record 27);
    BEGIN
      Item := FromItem;
    END;

    BEGIN
    END.
  }
}

