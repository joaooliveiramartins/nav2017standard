OBJECT Page 5197 Attendee Subform
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Lines;
               PTG=Linhas];
    LinksAllowed=No;
    SourceTable=Table5199;
    DelayedInsert=Yes;
    PageType=ListPart;
    AutoSplitKey=Yes;
    OnAfterGetRecord=BEGIN
                       StyleIsStrong := FALSE;
                       AttendanceTypeIndent := 0;
                       SendInvitationEditable := TRUE;

                       IF "Attendance Type" = "Attendance Type"::"To-do Organizer" THEN BEGIN
                         StyleIsStrong := TRUE;
                         SendInvitationEditable := FALSE;
                       END ELSE
                         AttendanceTypeIndent := 1;
                     END;

    ActionList=ACTIONS
    {
      { 1900000004;0 ;ActionContainer;
                      ActionContainerType=ActionItems }
      { 1906587504;1 ;ActionGroup;
                      CaptionML=[ENU=F&unctions;
                                 PTG=F&un��es];
                      Image=Action }
      { 1901742104;2 ;Action    ;
                      CaptionML=[ENU=Make &Phone Call;
                                 PTG=Efect&uar Telefonema];
                      ToolTipML=[ENU=Call the selected contact.;
                                 PTG=""];
                      ApplicationArea=#RelationshipMgmt;
                      Image=Calls;
                      OnAction=BEGIN
                                 MakePhoneCall;
                               END;
                                }
      { 1907935204;1 ;ActionGroup;
                      CaptionML=[ENU=&Line;
                                 PTG=&Linha];
                      Image=Line }
      { 1901313204;2 ;Action    ;
                      ShortCutKey=Shift+F7;
                      CaptionML=[ENU=Card;
                                 PTG=Ficha];
                      ToolTipML=[ENU=View or change detailed information about the attendee.;
                                 PTG=""];
                      ApplicationArea=#RelationshipMgmt;
                      Image=EditLines;
                      OnAction=BEGIN
                                 ShowCard;
                               END;
                                }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                IndentationColumnName=AttendanceTypeIndent;
                IndentationControls=Attendance Type;
                GroupType=Repeater }

    { 2   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the type of attendance for the meeting. You can select from: Required, Optional and To-do Organizer.;
                           PTG=""];
                ApplicationArea=#RelationshipMgmt;
                SourceExpr="Attendance Type";
                Style=Strong;
                StyleExpr=StyleIsStrong }

    { 4   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the type of the attendee. You can choose from Contact or Salesperson.;
                           PTG=""];
                ApplicationArea=#RelationshipMgmt;
                SourceExpr="Attendee Type";
                Style=Strong;
                StyleExpr=StyleIsStrong }

    { 6   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the number of the attendee participating in the to-do.;
                           PTG=""];
                ApplicationArea=#RelationshipMgmt;
                SourceExpr="Attendee No.";
                Style=Strong;
                StyleExpr=StyleIsStrong }

    { 8   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the name of the attendee participating in the to-do.;
                           PTG=""];
                ApplicationArea=#RelationshipMgmt;
                SourceExpr="Attendee Name";
                Style=Strong;
                StyleExpr=StyleIsStrong }

    { 10  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies that you want to send an invitation to the attendee by e-mail. The Send Invitation option is only available for contacts and salespeople with an e-mail address. The Send Invitation option is not available for the meeting organizer.;
                           PTG=""];
                ApplicationArea=#RelationshipMgmt;
                SourceExpr="Send Invitation";
                Editable=SendInvitationEditable }

    { 12  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the type of the attendee's response to a meeting invitation.;
                           PTG=""];
                ApplicationArea=#RelationshipMgmt;
                SourceExpr="Invitation Response Type";
                Style=Strong;
                StyleExpr=StyleIsStrong }

    { 14  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies that the meeting invitation has been sent to the attendee. The Send Invitation option is not available for the meeting organizer.;
                           PTG=""];
                ApplicationArea=#RelationshipMgmt;
                SourceExpr="Invitation Sent" }

  }
  CODE
  {
    VAR
      Text004@1000 : TextConst 'ENU=The Make Phone Call function is not available for a salesperson.;PTG=A fun��o Efetuar Telefonema n�o est� dispon�vel para um vendedor.';
      StyleIsStrong@1001 : Boolean INDATASET;
      SendInvitationEditable@1002 : Boolean INDATASET;
      AttendanceTypeIndent@1003 : Integer;

    LOCAL PROCEDURE ShowCard@1();
    VAR
      Cont@1000 : Record 5050;
      Salesperson@1001 : Record 13;
    BEGIN
      IF "Attendee Type" = "Attendee Type"::Contact THEN BEGIN
        IF Cont.GET("Attendee No.") THEN
          PAGE.RUN(PAGE::"Contact Card",Cont);
      END ELSE
        IF Salesperson.GET("Attendee No.") THEN
          PAGE.RUN(PAGE::"Salesperson/Purchaser Card",Salesperson);
    END;

    LOCAL PROCEDURE MakePhoneCall@2();
    VAR
      Attendee@1003 : Record 5199;
      SegLine@1002 : Record 5077;
      Cont@1001 : Record 5050;
      Todo@1000 : Record 5080;
    BEGIN
      IF "Attendee Type" = "Attendee Type"::Salesperson THEN
        ERROR(Text004);
      IF Cont.GET("Attendee No.") THEN BEGIN
        IF Todo.FindAttendeeTodo(Todo,Attendee) THEN
          SegLine."To-do No." := Todo."No.";
        SegLine."Contact No." := Cont."No.";
        SegLine."Contact Company No." := Cont."Company No.";
        SegLine."Campaign No." := Todo."Campaign No.";
        SegLine.CreateCall;
      END;
    END;

    BEGIN
    END.
  }
}

