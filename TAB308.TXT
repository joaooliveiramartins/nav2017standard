OBJECT Table 308 No. Series
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0,NAVPTSS85.00;
  }
  PROPERTIES
  {
    DataCaptionFields=Code,Description;
    OnDelete=BEGIN
               SAFTTestChangeNoSerie(Code,FALSE); //soft,n

               NoSeriesLine.SETRANGE("Series Code",Code);
               NoSeriesLine.DELETEALL;

               NoSeriesRelationship.SETRANGE(Code,Code);
               NoSeriesRelationship.DELETEALL;
               NoSeriesRelationship.SETRANGE(Code);

               NoSeriesRelationship.SETRANGE("Series Code",Code);
               NoSeriesRelationship.DELETEALL;
               NoSeriesRelationship.SETRANGE("Series Code");
             END;

    OnRename=BEGIN
               //soft,sn
               IF "SAF-T Invoice Type" <> "SAF-T Invoice Type"::" " THEN
                 FIELDERROR("SAF-T Invoice Type",STRSUBSTNO(Text31022893,FIELDCAPTION("SAF-T Invoice Type"),"SAF-T Invoice Type"));

               SAFTTestChangeNoSerie(xRec.Code,TRUE);
               //soft,en
             END;

    CaptionML=[ENU=No. Series;
               PTG=N�s S�ries];
    LookupPageID=Page571;
    DrillDownPageID=Page571;
  }
  FIELDS
  {
    { 1   ;   ;Code                ;Code10        ;CaptionML=[ENU=Code;
                                                              PTG=C�digo];
                                                   NotBlank=Yes }
    { 2   ;   ;Description         ;Text50        ;CaptionML=[ENU=Description;
                                                              PTG=Descri��o] }
    { 3   ;   ;Default Nos.        ;Boolean       ;OnValidate=BEGIN
                                                                IF ("Default Nos." = FALSE) AND (xRec."Default Nos." <> "Default Nos.") AND ("Manual Nos." = FALSE) THEN
                                                                  VALIDATE("Manual Nos.",TRUE);
                                                              END;

                                                   CaptionML=[ENU=Default Nos.;
                                                              PTG=Numera��o Gen�rica] }
    { 4   ;   ;Manual Nos.         ;Boolean       ;OnValidate=BEGIN
                                                                IF ("Manual Nos." = FALSE) AND (xRec."Manual Nos." <> "Manual Nos.") AND ("Default Nos." = FALSE) THEN
                                                                  VALIDATE("Default Nos.",TRUE);
                                                              END;

                                                   CaptionML=[ENU=Manual Nos.;
                                                              PTG=Numera��o Manual] }
    { 5   ;   ;Date Order          ;Boolean       ;CaptionML=[ENU=Date Order;
                                                              PTG=Sequ�ncia datas] }
    { 31022890;;SAF-T Invoice Type ;Option        ;OnValidate=BEGIN
                                                                IF (xRec."SAF-T Invoice Type" <> xRec."SAF-T Invoice Type"::" ") AND
                                                                  (Rec."SAF-T Invoice Type" = Rec."SAF-T Invoice Type"::" ") THEN
                                                                    MESSAGE(Text31022890);

                                                                TESTFIELD("GTAT Document Type", "GTAT Document Type"::" ");
                                                              END;

                                                   CaptionML=[ENU=SAF-T Doc. Type;
                                                              PTG=Tipo Fatura SAF-T];
                                                   OptionCaptionML=[ENU=" ,FT,FS,ND,NC,VD,TV,TD,AA,DA,RP,RE,CS,LD,RA,FR";
                                                                    PTG=" ,FT-Fatura,FS-Fatura Simplificada,ND-Nota D�bito,NC-Nota Cr�dito,VD-Venda Dinheiro(F/R),TV-Tal�o Venda,TD-Tal�o Devolu��o,AA-Aliena��o Ativos,DA-Devolu��o Ativos,RP-Pr�mio (Recibo Pr�mio),RE-Estorno (Recibo Estorno),CS-Imputa��o a Co-Seguradoras,LD-Imputa��o a Co-Segurada L�der,RA- Resseguro Aceite,FR-Fatura-Recibo"];
                                                   OptionString=[ ,FT,FS,ND,NC,VD,TV,TD,AA,DA,RP,RE,CS,LD,RA,FR];
                                                   Description=soft }
    { 31022891;;GTAT Document Type ;Option        ;OnValidate=BEGIN
                                                                IF NOT ("GTAT Document Type" IN ["GTAT Document Type"::" ",
                                                                  "GTAT Document Type"::GR,"GTAT Document Type"::GT,
                                                                  "GTAT Document Type"::GA,"GTAT Document Type"::GC,
                                                                  "GTAT Document Type"::GD]) THEN

                                                                  FIELDERROR("GTAT Document Type");

                                                                TESTFIELD("SAF-T Invoice Type", "SAF-T Invoice Type"::" ");
                                                              END;

                                                   CaptionML=[ENU=GTAT Document Type;
                                                              PTG=Tipo Documento GTAT];
                                                   OptionCaptionML=[ENU=" ,GR,GT,GA,GC,GD";
                                                                    PTG=" ,GR-Guia Remessa,GT-Guia Transporte,GA-Guia Movimenta��o Ativos Pr�prios,GC-Guia Consigna��o,GD-Guia ou Nota Devolu��o Efetuada pelo Cliente"];
                                                   OptionString=[ ,GR,GT,GA,GC,GD];
                                                   Description=soft }
    { 31022892;;Credit Invoice     ;Boolean       ;OnValidate=BEGIN
                                                                NoSeriesMgmt.SetNoSeriesLineFilter(NoSeriesLine,Code,TODAY);
                                                                IF NoSeriesLine."Last No. Used" <> '' THEN
                                                                  ERROR(Text31022894,FIELDCAPTION("Credit Invoice"),Code)
                                                              END;

                                                   CaptionML=[ENU=Credit Invoices;
                                                              PTG=Creditar Faturas];
                                                   Description=soft }
    { 31022893;;CAE Code           ;Code10        ;CaptionML=[ENU=CAE Code;
                                                              PTG=C�digo CAE];
                                                   Description=soft }
    { 31022894;;Series Group       ;Code10        ;TableRelation="Series Groups";
                                                   CaptionML=[ENU=Series Group;
                                                              PTG=Sequ�ncia S�ries];
                                                   Description=soft }
  }
  KEYS
  {
    {    ;Code                                    ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {
    VAR
      NoSeriesLine@1000 : Record 309;
      NoSeriesRelationship@1001 : Record 310;
      "//--soft-text--//"@1000000000 : TextConst;
      Text31022890@1000000001 : TextConst 'ENU=This configuration as impact on SAFT extraction. Confirm this setup.;PTG=Esta configura��o tem impacto na gera��o do SAFT. Valide a sua configura��o.';
      Text31022891@1000000002 : TextConst 'ENU=Can''t rename %1 on %2 because there are records in %3.;PTG=N�o pode renomear %1 em %2 porque existem registos em %3.';
      Text31022892@1000000003 : TextConst 'ENU=Can''t delete %1 on %2 because there are records in %3.;PTG=N�o pode eliminar %1 em %2 porque existem registos em %3.';
      Text31022893@1000000004 : TextConst 'ENU=Can''t rename because %1 is %2.;PTG=N�o pode renomear porque %1 � %2.';
      Text31022894@1000000008 : TextConst 'ENU=Can''t change %1 on %2 because there are records in this series.;PTG=N�o pode alterar %1 em %2 porque j� existem registos nesta s�rie.';
      "//--soft-global--//"@9000000 : Integer;
      NoSeriesMgmt@1002 : Codeunit 396;

    PROCEDURE DrillDown@6();
    VAR
      NoSeriesLine@1000 : Record 309;
    BEGIN
      FindNoSeriesLineToShow(NoSeriesLine);
      IF NoSeriesLine.FIND('-') THEN;
      NoSeriesLine.SETRANGE("Starting Date");
      NoSeriesLine.SETRANGE(Open);
      PAGE.RUNMODAL(0,NoSeriesLine);
    END;

    PROCEDURE UpdateLine@3(VAR StartDate@1007 : Date;VAR StartNo@1006 : Code[20];VAR EndNo@1005 : Code[20];VAR LastNoUsed@1004 : Code[20];VAR WarningNo@1003 : Code[20];VAR IncrementByNo@1002 : Integer;VAR LastDateUsed@1001 : Date);
    VAR
      NoSeriesLine@1000 : Record 309;
    BEGIN
      FindNoSeriesLineToShow(NoSeriesLine);
      IF NOT NoSeriesLine.FIND('-') THEN
        NoSeriesLine.INIT;
      StartDate := NoSeriesLine."Starting Date";
      StartNo := NoSeriesLine."Starting No.";
      EndNo := NoSeriesLine."Ending No.";
      LastNoUsed := NoSeriesLine."Last No. Used";
      WarningNo := NoSeriesLine."Warning No.";
      IncrementByNo := NoSeriesLine."Increment-by No.";
      LastDateUsed := NoSeriesLine."Last Date Used"
    END;

    LOCAL PROCEDURE FindNoSeriesLineToShow@1(VAR NoSeriesLine@1001 : Record 309);
    VAR
      NoSeriesMgt@1000 : Codeunit 396;
    BEGIN
      NoSeriesMgt.SetNoSeriesLineFilter(NoSeriesLine,Code,0D);

      IF NoSeriesLine.FINDLAST THEN
        EXIT;

      NoSeriesLine.RESET;
      NoSeriesLine.SETRANGE("Series Code",Code);
    END;

    PROCEDURE "//--soft-func--//"@9000005();
    BEGIN
    END;

    LOCAL PROCEDURE SAFTTestChangeNoSerie@1000000000(pCodSerieNo@1000000000 : Code[10];pBlnRenameRec@1000000010 : Boolean);
    VAR
      SalesShipmentHeader@1000000001 : Record 110;
      SalesInvoiceHeader@1000000002 : Record 112;
      ServiceInvoiceHeader@1000000003 : Record 5992;
      SalesCrMemoHeader@1000000004 : Record 114;
      ServiceCrMemoHeader@1000000005 : Record 5994;
      TransferShipmentHeader@1000000006 : Record 5744;
      ReturnShipmentHeader@1000000007 : Record 6650;
      PostedWhseShipmentHeader@1000000008 : Record 7322;
      ServiceShipmentHeader@1000000009 : Record 5990;
      ReceiptHeader@1000000012 : Record 31022932;
      TxtError@1000000011 : Text[1024];
    BEGIN
      //soft,sn
      IF pCodSerieNo = '' THEN
        EXIT;

      IF pBlnRenameRec THEN
        TxtError := Text31022891
      ELSE
        TxtError := Text31022892;

      // Sales Shipment Header
      CLEAR(SalesShipmentHeader);
      SalesShipmentHeader.SETRANGE("No. Series",pCodSerieNo);
      IF NOT SalesShipmentHeader.ISEMPTY THEN
        ERROR(TxtError,pCodSerieNo,TABLECAPTION,SalesShipmentHeader.TABLECAPTION);
      CLEAR(SalesShipmentHeader);

      // Sales Invoice Header
      CLEAR(SalesInvoiceHeader);
      SalesInvoiceHeader.SETRANGE("No. Series",pCodSerieNo);
      IF NOT SalesInvoiceHeader.ISEMPTY THEN
        ERROR(TxtError,pCodSerieNo,TABLECAPTION,SalesInvoiceHeader.TABLECAPTION);
      CLEAR(SalesInvoiceHeader);

      SalesInvoiceHeader.SETRANGE("Prepayment No. Series",pCodSerieNo);
      IF NOT SalesInvoiceHeader.ISEMPTY THEN
        ERROR(TxtError,pCodSerieNo,TABLECAPTION,SalesInvoiceHeader.TABLECAPTION);
      CLEAR(SalesInvoiceHeader);

      // Service Invoice Header
      CLEAR(ServiceInvoiceHeader);
      ServiceInvoiceHeader.SETRANGE("No. Series",pCodSerieNo);
      IF NOT ServiceInvoiceHeader.ISEMPTY THEN
        ERROR(TxtError,pCodSerieNo,TABLECAPTION,ServiceInvoiceHeader.TABLECAPTION);
      CLEAR(ServiceInvoiceHeader);

      //Sales Cr. Memo Header
      CLEAR(SalesCrMemoHeader);
      SalesCrMemoHeader.SETRANGE("No. Series",pCodSerieNo);
      IF NOT SalesCrMemoHeader.ISEMPTY THEN
        ERROR(TxtError,pCodSerieNo,TABLECAPTION,SalesCrMemoHeader.TABLECAPTION);
      CLEAR(SalesCrMemoHeader);

      SalesCrMemoHeader.SETRANGE("Prepmt. Cr. Memo No. Series",pCodSerieNo);
      IF NOT SalesCrMemoHeader.ISEMPTY THEN
        ERROR(TxtError,pCodSerieNo,TABLECAPTION,SalesCrMemoHeader.TABLECAPTION);
      CLEAR(SalesCrMemoHeader);

      // Service Cr. Memo Header
      CLEAR(ServiceCrMemoHeader);
      ServiceCrMemoHeader.SETRANGE("No. Series",pCodSerieNo);
      IF NOT ServiceCrMemoHeader.ISEMPTY THEN
        ERROR(TxtError,pCodSerieNo,TABLECAPTION,ServiceCrMemoHeader.TABLECAPTION);
      CLEAR(ServiceCrMemoHeader);

      // Transfer Shipment Header
      CLEAR(TransferShipmentHeader);
      TransferShipmentHeader.SETRANGE("No. Series", pCodSerieNo);
      IF NOT TransferShipmentHeader.ISEMPTY THEN
        ERROR(TxtError,pCodSerieNo,TABLECAPTION,TransferShipmentHeader.TABLECAPTION);
      CLEAR(TransferShipmentHeader);

      // Return Shipment Header
      CLEAR(ReturnShipmentHeader);
      ReturnShipmentHeader.SETRANGE("No. Series",pCodSerieNo);
      IF NOT ReturnShipmentHeader.ISEMPTY THEN
        ERROR(TxtError,pCodSerieNo,TABLECAPTION,ReturnShipmentHeader.TABLECAPTION);
      CLEAR(ReturnShipmentHeader);

      // Posted Whse. Shipment Header
      CLEAR(PostedWhseShipmentHeader);
      PostedWhseShipmentHeader.SETRANGE("No. Series",pCodSerieNo);
      IF NOT PostedWhseShipmentHeader.ISEMPTY THEN
        ERROR(TxtError,pCodSerieNo,TABLECAPTION,PostedWhseShipmentHeader.TABLECAPTION);
      CLEAR(PostedWhseShipmentHeader);

      // Service Shipment Header
      CLEAR(ServiceShipmentHeader);
      ServiceShipmentHeader.SETRANGE("No. Series",pCodSerieNo);
      IF NOT ServiceShipmentHeader.ISEMPTY THEN
        ERROR(TxtError,pCodSerieNo,TABLECAPTION,ServiceShipmentHeader.TABLECAPTION);
      CLEAR(ServiceShipmentHeader);

      // Receipt Header
      CLEAR(ReceiptHeader);
      ReceiptHeader.SETRANGE("No. Series",pCodSerieNo);
      IF NOT ReceiptHeader.ISEMPTY THEN
        ERROR(TxtError,pCodSerieNo,TABLECAPTION,ReceiptHeader.TABLECAPTION);
      CLEAR(ReceiptHeader);

      //soft,en
    END;

    PROCEDURE TestNoSeriesCreditInvoice@1000000001(NoSeries@1000000000 : Code[20]) : Boolean;
    BEGIN
      //soft,sn
      GET(NoSeries);
      EXIT("Credit Invoice");
      //soft,en
    END;

    BEGIN
    END.
  }
}

