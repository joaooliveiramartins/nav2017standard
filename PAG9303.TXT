OBJECT Page 9303 Blanket Sales Orders
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0.00.15052,NAVPTSS84.00;
  }
  PROPERTIES
  {
    Editable=No;
    CaptionML=[ENU=Blanket Sales Orders;
               PTG=Encomendas Venda Abertas];
    SourceTable=Table36;
    SourceTableView=WHERE(Document Type=CONST(Blanket Order));
    DataCaptionFields=Sell-to Customer No.;
    PageType=List;
    CardPageID=Blanket Sales Order;
    RefreshOnActivate=Yes;
    PromotedActionCategoriesML=[ENU=New,Process,Report,Request Approval;
                                PTG=Novo,Processar,Mapa,Pedido Aprova��o];
    OnOpenPage=BEGIN
                 SetSecurityFilterOnRespCenter;

                 CopySellToCustomerFilter;
               END;

    OnAfterGetCurrRecord=BEGIN
                           SetControlAppearance;
                         END;

    ActionList=ACTIONS
    {
      { 1900000003;0 ;ActionContainer;
                      ActionContainerType=RelatedInformation }
      { 1102601015;1 ;ActionGroup;
                      CaptionML=[ENU=O&rder;
                                 PTG=&Encomenda];
                      Image=Order }
      { 1102601017;2 ;Action    ;
                      Name=Statistics;
                      ShortCutKey=F7;
                      CaptionML=[ENU=Statistics;
                                 PTG=Estat�sticas];
                      Promoted=Yes;
                      Image=Statistics;
                      PromotedCategory=Process;
                      OnAction=BEGIN
                                 OpenSalesOrderStatistics;
                               END;
                                }
      { 1102601019;2 ;Action    ;
                      CaptionML=[ENU=Co&mments;
                                 PTG=Co&ment�rios];
                      RunObject=Page 67;
                      RunPageLink=Document Type=CONST(Blanket Order),
                                  No.=FIELD(No.),
                                  Document Line No.=CONST(0);
                      Image=ViewComments }
      { 1102601020;2 ;Action    ;
                      AccessByPermission=TableData 348=R;
                      ShortCutKey=Shift+Ctrl+D;
                      CaptionML=[ENU=Dimensions;
                                 PTG=Dimens�es];
                      ToolTipML=[ENU=View or edit dimensions, such as area, project, or department, that you can assign to sales and purchase documents to distribute costs and analyze transaction history.;
                                 PTG=""];
                      Image=Dimensions;
                      OnAction=BEGIN
                                 ShowDocDim;
                               END;
                                }
      { 1102601021;2 ;Action    ;
                      Name=Approvals;
                      AccessByPermission=TableData 454=R;
                      CaptionML=[ENU=Approvals;
                                 PTG=Aprova��es];
                      ToolTipML=[ENU=View a list of the records that are waiting to be approved. For example, you can see who requested the record to be approved, when it was sent, and when it is due to be approved.;
                                 PTG=Ver uma lista dos registos que est�o � espera de aprova��o. Por exemplo, pode ver quem pediu a aprova��o do registo, quando foi enviado e quando a aprova��o deve ser conclu�da.];
                      Image=Approvals;
                      OnAction=VAR
                                 ApprovalEntries@1000 : Page 658;
                               BEGIN
                                 ApprovalEntries.Setfilters(DATABASE::"Sales Header","Document Type","No.");
                                 ApprovalEntries.RUN;
                               END;
                                }
      { 1900000004;0 ;ActionContainer;
                      ActionContainerType=ActionItems }
      { 1102601000;1 ;ActionGroup;
                      CaptionML=[ENU=F&unctions;
                                 PTG=F&un��es];
                      Image=Action }
      { 1102601012;2 ;Separator  }
      { 1102601013;2 ;Action    ;
                      ShortCutKey=Ctrl+F9;
                      CaptionML=[ENU=Re&lease;
                                 PTG=&Libertar];
                      Image=ReleaseDoc;
                      OnAction=VAR
                                 ReleaseSalesDoc@1000 : Codeunit 414;
                               BEGIN
                                 ReleaseSalesDoc.PerformManualRelease(Rec);
                               END;
                                }
      { 1102601014;2 ;Action    ;
                      CaptionML=[ENU=Re&open;
                                 PTG=Re&abrir];
                      Image=ReOpen;
                      OnAction=VAR
                                 ReleaseSalesDoc@1001 : Codeunit 414;
                               BEGIN
                                 ReleaseSalesDoc.PerformManualReopen(Rec);
                               END;
                                }
      { 73      ;1   ;Action    ;
                      CaptionML=[ENU=Make &Order;
                                 PTG=&Criar Enc.];
                      Promoted=Yes;
                      Image=MakeOrder;
                      PromotedCategory=Process;
                      OnAction=VAR
                                 ApprovalsMgmt@1001 : Codeunit 1535;
                               BEGIN
                                 IF ApprovalsMgmt.PrePostApprovalCheckSales(Rec) THEN
                                   CODEUNIT.RUN(CODEUNIT::"Blnkt Sales Ord. to Ord. (Y/N)",Rec);
                               END;
                                }
      { 78      ;1   ;Action    ;
                      Name=Print;
                      Ellipsis=Yes;
                      CaptionML=[ENU=&Print;
                                 PTG=&Imprimir];
                      Promoted=Yes;
                      Image=Print;
                      PromotedCategory=Process;
                      OnAction=BEGIN
                                 DocPrint.PrintSalesHeader(Rec);
                               END;
                                }
      { 3       ;1   ;ActionGroup;
                      CaptionML=[ENU=Request Approval;
                                 PTG=Pedir Aprova��o] }
      { 1102601010;2 ;Action    ;
                      Name=SendApprovalRequest;
                      CaptionML=[ENU=Send A&pproval Request;
                                 PTG=Enviar Pedido A&prova��o];
                      ToolTipML=[ENU=Send an approval request.;
                                 PTG=""];
                      Promoted=Yes;
                      Enabled=NOT OpenApprovalEntriesExist;
                      PromotedIsBig=Yes;
                      Image=SendApprovalRequest;
                      PromotedCategory=Category4;
                      PromotedOnly=Yes;
                      OnAction=VAR
                                 ApprovalsMgmt@1000 : Codeunit 1535;
                               BEGIN
                                 IF ApprovalsMgmt.CheckSalesApprovalPossible(Rec) THEN
                                   ApprovalsMgmt.OnSendSalesDocForApproval(Rec);
                               END;
                                }
      { 1102601011;2 ;Action    ;
                      Name=CancelApprovalRequest;
                      CaptionML=[ENU=Cancel Approval Re&quest;
                                 PTG=Cancelar Pedido Aprova&��o];
                      Promoted=Yes;
                      Enabled=CanCancelApprovalForRecord;
                      PromotedIsBig=Yes;
                      Image=CancelApprovalRequest;
                      PromotedCategory=Category4;
                      PromotedOnly=Yes;
                      OnAction=VAR
                                 ApprovalsMgmt@1000 : Codeunit 1535;
                               BEGIN
                                 ApprovalsMgmt.OnCancelSalesApprovalRequest(Rec);
                               END;
                                }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 2   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the number of the sales document.;
                           PTG=""];
                SourceExpr="No." }

    { 4   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the number of the customer who will receive the products and be billed by default.;
                           PTG=""];
                SourceExpr="Sell-to Customer No." }

    { 6   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the name of the customer who will receive the products and be billed by default.;
                           PTG=""];
                SourceExpr="Sell-to Customer Name" }

    { 17  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the number that the customer uses in their own system to refer to this sales document.;
                           PTG=""];
                SourceExpr="External Document No." }

    { 33  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the postal code of the address.;
                           PTG=""];
                SourceExpr="Sell-to Post Code";
                Visible=FALSE }

    { 29  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the country/region code of the address.;
                           PTG=""];
                SourceExpr="Sell-to Country/Region Code";
                Visible=FALSE }

    { 41  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the name of the person to contact at the customer.;
                           PTG=""];
                SourceExpr="Sell-to Contact";
                Visible=FALSE }

    { 13  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the customer to whom you will send the sales invoice when this customer is different from the sell-to customer.;
                           PTG=""];
                SourceExpr="Bill-to Customer No.";
                Visible=FALSE }

    { 15  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the customer to whom you will send the sales invoice, when different from the customer that you are selling to.;
                           PTG=""];
                SourceExpr="Bill-to Name";
                Visible=FALSE }

    { 39  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the postal code of the address.;
                           PTG=""];
                SourceExpr="Bill-to Post Code";
                Visible=FALSE }

    { 35  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the country/region code of the address.;
                           PTG=""];
                SourceExpr="Bill-to Country/Region Code";
                Visible=FALSE }

    { 159 ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the name of the person you should contact at the customer who you are sending the invoice to.;
                           PTG=""];
                SourceExpr="Bill-to Contact";
                Visible=FALSE }

    { 155 ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the code for another shipment address than the customer's own address, which is entered by default.;
                           PTG=""];
                SourceExpr="Ship-to Code";
                Visible=FALSE }

    { 153 ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the name that products on the sales document will be shipped to.;
                           PTG=""];
                SourceExpr="Ship-to Name";
                Visible=FALSE }

    { 27  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the postal code of the address.;
                           PTG=""];
                SourceExpr="Ship-to Post Code";
                Visible=FALSE }

    { 23  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the country/region code of the address.;
                           PTG=""];
                SourceExpr="Ship-to Country/Region Code";
                Visible=FALSE }

    { 143 ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the name of the contact person at the address that products will be shipped to.;
                           PTG=""];
                SourceExpr="Ship-to Contact";
                Visible=FALSE }

    { 139 ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the date when the posting of the sales document will be recorded.;
                           PTG=""];
                SourceExpr="Posting Date";
                Visible=FALSE }

    { 121 ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the dimension value code associated with the sales header.;
                           PTG=""];
                SourceExpr="Shortcut Dimension 1 Code";
                Visible=FALSE }

    { 119 ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the dimension value code associated with the sales header.;
                           PTG=""];
                SourceExpr="Shortcut Dimension 2 Code";
                Visible=FALSE }

    { 123 ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the location from where inventory items to the customer on the sales document are to be shipped by default.;
                           PTG=""];
                SourceExpr="Location Code" }

    { 99  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the name of the sales person who is assigned to the customer.;
                           PTG=""];
                SourceExpr="Salesperson Code";
                Visible=FALSE }

    { 21  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the ID of the user who is responsible for the document.;
                           PTG=""];
                SourceExpr="Assigned User ID" }

    { 11  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the currency of amounts on the sales document.;
                           PTG=""];
                SourceExpr="Currency Code";
                Visible=FALSE }

    { 301 ;2   ;Field     ;
                SourceExpr="Customer Posting Group";
                Importance=Standard;
                Visible=FALSE;
                Enabled=TRUE;
                Editable=TRUE }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1902018507;1;Part   ;
                SubPageLink=No.=FIELD(Bill-to Customer No.),
                            Date Filter=FIELD(Date Filter);
                PagePartID=Page9082;
                PartType=Page }

    { 1900316107;1;Part   ;
                SubPageLink=No.=FIELD(Bill-to Customer No.),
                            Date Filter=FIELD(Date Filter);
                PagePartID=Page9084;
                PartType=Page }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {
    VAR
      DocPrint@1102601000 : Codeunit 229;
      OpenApprovalEntriesExist@1002 : Boolean;
      CanCancelApprovalForRecord@1000 : Boolean;

    LOCAL PROCEDURE SetControlAppearance@5();
    VAR
      ApprovalsMgmt@1000 : Codeunit 1535;
    BEGIN
      OpenApprovalEntriesExist := ApprovalsMgmt.HasOpenApprovalEntries(RECORDID);

      CanCancelApprovalForRecord := ApprovalsMgmt.CanCancelApprovalForRecord(RECORDID);
    END;

    BEGIN
    END.
  }
}

