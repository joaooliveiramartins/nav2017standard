OBJECT Table 31022905 Sales/Purch. Book VAT Buffer
{
  OBJECT-PROPERTIES
  {
    Date=01/02/15;
    Time=13:00:00;
    Version List=NAVPTSS81.00;
  }
  PROPERTIES
  {
    OnInsert=BEGIN
               VATPostingSetup.GET("ND %",Base);
               "VAT %" := VATPostingSetup."VAT %";
             END;

    CaptionML=[ENU=Sales/Purch. Book VAT Buffer;
               PTG=Livro IVA Vendas/Compras Temp.];
    LookupPageID=Page315;
  }
  FIELDS
  {
    { 1   ;   ;VAT %               ;Decimal       ;CaptionML=[ENU=VAT %;
                                                              PTG=% IVA] }
    { 2   ;   ;ND %                ;Decimal       ;CaptionML=[ENU=ND %;
                                                              PTG=% ND] }
    { 3   ;   ;Base                ;Decimal       ;CaptionML=[ENU=Base;
                                                              PTG=Base] }
    { 4   ;   ;Amount              ;Decimal       ;CaptionML=[ENU=Amount;
                                                              PTG=Valor] }
  }
  KEYS
  {
    {    ;VAT %,ND %                              ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {
    VAR
      Cust@1110000 : Record 18;
      Vend@1110001 : Record 23;
      VATPostingSetup@1110002 : Record 325;

    BEGIN
    END.
  }
}

