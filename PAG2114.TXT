OBJECT Page 2114 O365 Posted Sales Inv. Lines
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0;
  }
  PROPERTIES
  {
    Editable=No;
    CaptionML=[ENU=Sent Invoice Lines;
               PTG=Linhas Fatura Enviadas];
    InsertAllowed=No;
    DeleteAllowed=No;
    SourceTable=Table113;
    PageType=ListPart;
  }
  CONTROLS
  {
    { 1   ;0   ;Container ;
                ContainerType=ContentArea }

    { 2   ;1   ;Group     ;
                Name=Group;
                GroupType=Repeater }

    { 3   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the number of the record.;
                           PTG=Especifica o n�mero de registo.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="No." }

    { 4   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies a description of the item or service on the line.;
                           PTG=Especifica uma descri��o do produto ou servi�o na linha.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=Description }

    { 12  ;2   ;Field     ;
                ApplicationArea=#Basic,#Suite;
                SourceExpr=Quantity }

    { 9   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the sum of amounts in the Line Amount field on the sales lines. It is used to calculate the invoice discount of the sales document.;
                           PTG=Especifica a soma de valores no campo Valor Linha nas linhas de venda. � usado para calcular o desconto fatura do documento de venda.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=Amount }

    { 5   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the unit of measure code for the item.;
                           PTG=Especifica o c�digo unidade de medida para o produto.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Unit of Measure" }

    { 8   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the VAT % that was used on the sales or purchase lines with this VAT Identifier.;
                           PTG=Especifica a % IVA que foi usada nas linhas de compra ou venda com este Identificador IVA.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="VAT %" }

    { 6   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the tax group code for the tax-detail entry.;
                           PTG=Especifica o c�digo de grupo de imposto para o mov. detalhe imposto.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Tax Group Code" }

    { 10  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the total of the amounts in all the amount fields on the invoice, in the currency of the invoice. The amount includes VAT.;
                           PTG=Especifica o total de valores em todos os campos de valor na fatura, na divisa da fatura. O valor incl�i IVA.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Amount Including VAT" }

    { 14  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the price for one unit on the sales line.;
                           PTG=Especifica o pre�o para uma unidade na linha de venda.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Unit Price" }

  }
  CODE
  {

    BEGIN
    END.
  }
}

