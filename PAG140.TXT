OBJECT Page 140 Posted Purchase Credit Memo
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0.00.15052,NAVPTSS93.00;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Posted Purchase Credit Memo;
               PTG=Nota Cr�dito Compra Registada];
    InsertAllowed=No;
    ModifyAllowed=No;
    SourceTable=Table124;
    PageType=Document;
    RefreshOnActivate=Yes;
    PromotedActionCategoriesML=[ENU=New,Process,Report,Cancel;
                                PTG=Novo,Processar,Mapas,Cancelar];
    OnOpenPage=VAR
                 OfficeMgt@1000 : Codeunit 1630;
               BEGIN
                 SetSecurityFilterOnRespCenter;
                 IsOfficeAddin := OfficeMgt.IsAvailable;
               END;

    OnAfterGetCurrRecord=VAR
                           IncomingDocument@1000 : Record 130;
                         BEGIN
                           HasIncomingDocument := IncomingDocument.PostedDocExists("No.","Posting Date");
                           CurrPage.IncomingDocAttachFactBox.PAGE.LoadDataFromRecord(Rec);
                         END;

    ActionList=ACTIONS
    {
      { 1900000003;0 ;ActionContainer;
                      ActionContainerType=RelatedInformation }
      { 45      ;1   ;ActionGroup;
                      CaptionML=[ENU=&Cr. Memo;
                                 PTG=&Nota Cr�dito];
                      Image=CreditMemo }
      { 8       ;2   ;Action    ;
                      Name=Statistics;
                      ShortCutKey=F7;
                      CaptionML=[ENU=Statistics;
                                 PTG=Estat�sticas];
                      RunObject=Page 401;
                      RunPageLink=No.=FIELD(No.);
                      Promoted=Yes;
                      Image=Statistics;
                      PromotedCategory=Process }
      { 47      ;2   ;Action    ;
                      CaptionML=[ENU=Co&mments;
                                 PTG=Co&ment�rios];
                      ToolTipML=ENU=View or add notes about the posted purchase credit memo.;
                      RunObject=Page 66;
                      RunPageLink=Document Type=CONST(Posted Credit Memo),
                                  No.=FIELD(No.),
                                  Document Line No.=CONST(0);
                      Image=ViewComments }
      { 79      ;2   ;Action    ;
                      AccessByPermission=TableData 348=R;
                      ShortCutKey=Shift+Ctrl+D;
                      CaptionML=[ENU=Dimensions;
                                 PTG=Dimens�es];
                      ToolTipML=ENU=View or edits dimensions, such as area, project, or department, that you can assign to sales and purchase documents to distribute costs and analyze transaction history.;
                      ApplicationArea=#Suite;
                      Image=Dimensions;
                      OnAction=BEGIN
                                 ShowDimensions;
                               END;
                                }
      { 88      ;2   ;Action    ;
                      AccessByPermission=TableData 456=R;
                      CaptionML=[ENU=Approvals;
                                 PTG=Aprova��es];
                      ToolTipML=ENU=View a list of the records that are waiting to be approved. For example, you can see who requested the record to be approved, when it was sent, and when it is due to be approved.;
                      ApplicationArea=#Suite;
                      Image=Approvals;
                      OnAction=VAR
                                 ApprovalsMgmt@1000 : Codeunit 1535;
                               BEGIN
                                 ApprovalsMgmt.ShowPostedApprovalEntries(RECORDID);
                               END;
                                }
      { 1900000004;  ;ActionContainer;
                      Name=<Action1900000004>>;
                      ActionContainerType=ActionItems }
      { 15      ;1   ;Action    ;
                      ShortCutKey=Shift+F7;
                      CaptionML=[ENU=Vendor;
                                 PTG=Fornecedor];
                      ToolTipML=ENU=View or edit detailed information about the vendor on the selected posted purchase document.;
                      ApplicationArea=#Basic,#Suite;
                      RunObject=Page 26;
                      RunPageLink=No.=FIELD(Buy-from Vendor No.);
                      Promoted=Yes;
                      Image=Vendor;
                      PromotedCategory=Process }
      { 48      ;1   ;Action    ;
                      Ellipsis=Yes;
                      CaptionML=[ENU=&Print;
                                 PTG=&Imprimir];
                      ToolTipML=ENU=Prepare to print the document. A report request window for the document opens where you can specify what to include on the print-out.;
                      ApplicationArea=#Basic,#Suite;
                      Promoted=Yes;
                      Visible=NOT IsOfficeAddin;
                      Image=Print;
                      PromotedCategory=Process;
                      OnAction=BEGIN
                                 CurrPage.SETSELECTIONFILTER(PurchCrMemoHeader);
                                 PurchCrMemoHeader.PrintRecords(TRUE);
                               END;
                                }
      { 49      ;1   ;Action    ;
                      CaptionML=[ENU=&Navigate;
                                 PTG=&Navegar];
                      ToolTipML=ENU=Find all entries and documents that exist for the document number and posting date on the selected entry or document.;
                      ApplicationArea=#Basic,#Suite;
                      Promoted=Yes;
                      Visible=NOT IsOfficeAddin;
                      Image=Navigate;
                      PromotedCategory=Process;
                      OnAction=BEGIN
                                 Navigate;
                               END;
                                }
      { 29      ;1   ;ActionGroup;
                      CaptionML=[ENU=Cancel;
                                 PTG=Cancelar] }
      { 24      ;2   ;Action    ;
                      Name=CancelCrMemo;
                      CaptionML=[ENU=Cancel;
                                 PTG=Cancelar];
                      ToolTipML=ENU=Create and post a purchase invoice that reverses this posted purchase credit memo. This posted purchase credit memo will be canceled.;
                      ApplicationArea=#Basic,#Suite;
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=Cancel;
                      PromotedCategory=Category4;
                      OnAction=BEGIN
                                 CODEUNIT.RUN(CODEUNIT::"Cancel PstdPurchCrM (Yes/No)",Rec);
                               END;
                                }
      { 27      ;2   ;Action    ;
                      Name=ShowInvoice;
                      CaptionML=[ENU=Show Canceled/Corrective Invoice;
                                 PTG=Mostrar Cancelado/Fatura Corre��o];
                      ToolTipML=ENU=Open the posted sales invoice that was created when you canceled the posted sales credit memo. If the posted sales credit memo is the result of a canceled sales invoice, then canceled invoice will open.;
                      ApplicationArea=#Basic,#Suite;
                      Promoted=Yes;
                      Enabled=Cancelled OR Corrective;
                      PromotedIsBig=Yes;
                      Image=Invoice;
                      PromotedCategory=Category4;
                      Scope=Repeater;
                      OnAction=BEGIN
                                 ShowCanceledOrCorrInvoice;
                               END;
                                }
      { 13      ;1   ;ActionGroup;
                      Name=IncomingDocument;
                      CaptionML=[ENU=Incoming Document;
                                 PTG=Documento Recebido];
                      ActionContainerType=NewDocumentItems;
                      Image=Documents }
      { 11      ;2   ;Action    ;
                      Name=IncomingDocCard;
                      CaptionML=[ENU=View Incoming Document;
                                 PTG=Ver Documento Recebido];
                      ToolTipML=ENU=View any incoming document records and file attachments that exist for the entry or document.;
                      ApplicationArea=#Basic,#Suite;
                      Visible=NOT IsOfficeAddin;
                      Enabled=HasIncomingDocument;
                      Image=ViewOrder;
                      OnAction=VAR
                                 IncomingDocument@1000 : Record 130;
                               BEGIN
                                 IncomingDocument.ShowCard("No.","Posting Date");
                               END;
                                }
      { 9       ;2   ;Action    ;
                      Name=SelectIncomingDoc;
                      AccessByPermission=TableData 130=R;
                      CaptionML=[ENU=Select Incoming Document;
                                 PTG=Selecionar Documento Recebido];
                      ToolTipML=ENU=Select an incoming document record and file attachment that you want to link to the entry or document.;
                      ApplicationArea=#Basic,#Suite;
                      Enabled=NOT HasIncomingDocument;
                      Image=SelectLineToApply;
                      OnAction=VAR
                                 IncomingDocument@1000 : Record 130;
                               BEGIN
                                 IncomingDocument.SelectIncomingDocumentForPostedDocument("No.","Posting Date",RECORDID);
                               END;
                                }
      { 5       ;2   ;Action    ;
                      Name=IncomingDocAttachFile;
                      Ellipsis=Yes;
                      CaptionML=[ENU=Create Incoming Document from File;
                                 PTG=Criar Documento Recebido a partir de Ficheiro];
                      ToolTipML=ENU=Create an incoming document record by selecting a file to attach, and then link the incoming document record to the entry or document.;
                      ApplicationArea=#Basic,#Suite;
                      Enabled=NOT HasIncomingDocument;
                      Image=Attach;
                      OnAction=VAR
                                 IncomingDocumentAttachment@1000 : Record 133;
                               BEGIN
                                 IncomingDocumentAttachment.NewAttachmentFromPostedDocument("No.","Posting Date");
                               END;
                                }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                CaptionML=[ENU=General;
                           PTG=Geral] }

    { 2   ;2   ;Field     ;
                ToolTipML=ENU=Specifies the posted credit memo number.;
                ApplicationArea=#All;
                SourceExpr="No.";
                Importance=Promoted;
                Editable=FALSE }

    { 51  ;2   ;Field     ;
                CaptionML=[ENU=Vendor;
                           PTG=Fornecedor];
                ToolTipML=ENU=Specifies the name of the vendor who shipped the items.;
                ApplicationArea=#All;
                SourceExpr="Buy-from Vendor Name";
                TableRelation=Vendor.Name;
                Importance=Promoted;
                Editable=FALSE }

    { 4   ;2   ;Group     ;
                CaptionML=[ENU=Buy-from;
                           PTG=Compra-a];
                GroupType=Group }

    { 53  ;3   ;Field     ;
                CaptionML=[ENU=Address;
                           PTG=Endere�o];
                ToolTipML=ENU=Specifies the address of the vendor who shipped the items.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Buy-from Address";
                Importance=Additional;
                Editable=FALSE }

    { 55  ;3   ;Field     ;
                CaptionML=[ENU=Address 2;
                           PTG=Endere�o 2];
                ToolTipML=ENU=Specifies additional address information.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Buy-from Address 2";
                Importance=Additional;
                Editable=FALSE }

    { 6   ;3   ;Field     ;
                CaptionML=[ENU=Post Code;
                           PTG=C�d. Postal];
                ToolTipML=ENU=Specifies the postal code.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Buy-from Post Code";
                Importance=Additional;
                Editable=FALSE }

    { 57  ;3   ;Field     ;
                CaptionML=[ENU=City;
                           PTG=Cidade];
                ToolTipML=ENU=Specifies the city of the vendor who shipped the items.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Buy-from City";
                Importance=Additional;
                Editable=FALSE }

    { 1110004;3;Field     ;
                CaptionML=[ENU=County;
                           PTG=Distrito];
                SourceExpr="Buy-from County";
                Editable=FALSE }

    { 91  ;3   ;Field     ;
                CaptionML=[ENU=Contact No.;
                           PTG=N� Contacto];
                ToolTipML=ENU=Specifies the number of the contact who you sent the purchase credit memo to.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Buy-from Contact No.";
                Importance=Additional;
                Editable=FALSE }

    { 59  ;2   ;Field     ;
                CaptionML=[ENU=Contact;
                           PTG=Contacto];
                ToolTipML=ENU=Specifies the name of the person to contact at the vendor who shipped the items.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Buy-from Contact";
                Editable=FALSE }

    { 12  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the date the credit memo was posted.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Posting Date";
                Importance=Promoted;
                Editable=FALSE }

    { 31  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the date on which the purchase document was created.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Document Date";
                Editable=FALSE }

    { 63  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the number of the credit memo that the posted credit memo was created from.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Pre-Assigned No.";
                Importance=Additional;
                Editable=FALSE }

    { 14  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the vendor's number for this credit memo.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Vendor Cr. Memo No.";
                Importance=Promoted;
                Editable=FALSE }

    { 7   ;2   ;Field     ;
                ToolTipML=ENU=Specifies the order address code used in the credit memo.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Order Address Code";
                Editable=FALSE }

    { 10  ;2   ;Field     ;
                ToolTipML=ENU=Specifies which purchaser is associated with the credit memo.;
                ApplicationArea=#Suite;
                SourceExpr="Purchaser Code";
                Importance=Additional;
                Editable=FALSE }

    { 80  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the code for the responsibility center that serves the vendor on this purchase document.;
                SourceExpr="Responsibility Center";
                Importance=Additional;
                Editable=FALSE }

    { 21  ;2   ;Field     ;
                ToolTipML=ENU=Specifies if the posted purchase invoice that relates to this purchase credit memo has been either corrected or canceled.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr=Cancelled;
                Importance=Additional;
                Style=Unfavorable;
                StyleExpr=Cancelled;
                OnDrillDown=BEGIN
                              ShowCorrectiveInvoice;
                            END;
                             }

    { 23  ;2   ;Field     ;
                ToolTipML=ENU=Specifies if the posted purchase invoice has been either corrected or canceled by this purchase credit memo .;
                ApplicationArea=#Basic,#Suite;
                SourceExpr=Corrective;
                Importance=Additional;
                Style=Unfavorable;
                StyleExpr=Corrective;
                OnDrillDown=BEGIN
                              ShowCancelledInvoice;
                            END;
                             }

    { 20  ;2   ;Field     ;
                ToolTipML=ENU=Specifies how many times the credit memo has been printed.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="No. Printed";
                Importance=Additional;
                Editable=FALSE }

    { 31022893;2;Field    ;
                SourceExpr="Reason Code" }

    { 44  ;1   ;Part      ;
                Name=PurchCrMemoLines;
                ApplicationArea=#Basic,#Suite;
                SubPageLink=Document No.=FIELD(No.);
                PagePartID=Page141 }

    { 1905885101;1;Group  ;
                CaptionML=[ENU=Invoice Details;
                           PTG=Detalhe da Fatura];
                GroupType=Group }

    { 75  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the currency code used to calculate the amounts on the credit memo.;
                ApplicationArea=#Suite;
                SourceExpr="Currency Code";
                Importance=Promoted;
                OnAssistEdit=BEGIN
                               ChangeExchangeRate.SetParameter("Currency Code","Currency Factor","Posting Date");
                               ChangeExchangeRate.EDITABLE(FALSE);
                               IF ChangeExchangeRate.RUNMODAL = ACTION::OK THEN BEGIN
                                 "Currency Factor" := ChangeExchangeRate.GetParameter;
                                 MODIFY;
                               END;
                               CLEAR(ChangeExchangeRate);
                             END;
                              }

    { 64  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the code for Shortcut Dimension 1.;
                ApplicationArea=#Suite;
                SourceExpr="Shortcut Dimension 1 Code";
                Importance=Additional;
                Editable=FALSE }

    { 66  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the code for Shortcut Dimension 2.;
                ApplicationArea=#Suite;
                SourceExpr="Shortcut Dimension 2 Code";
                Importance=Additional;
                Editable=FALSE }

    { 31022900;2;Field    ;
                SourceExpr="Payment Discount %";
                Editable=FALSE }

    { 68  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the code for the location used when you posted the credit memo.;
                SourceExpr="Location Code";
                Editable=FALSE }

    { 31022890;2;Field    ;
                SourceExpr="Vendor Posting Group";
                Importance=Additional;
                Visible=TRUE;
                Enabled=TRUE;
                Editable=FALSE }

    { 31022897;1;Group    ;
                CaptionML=[ENU=Payment;
                           PTG=Pagamento] }

    { 31022896;2;Field    ;
                SourceExpr="Vendor Bank Acc. Code";
                Editable=FALSE }

    { 31022895;2;Field    ;
                SourceExpr="Pay-at Code";
                Editable=FALSE }

    { 31022899;2;Field    ;
                ToolTipML=ENU=Specifies the type of the posted document that this document or journal line is applied to.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Applies-to Doc. Type";
                Editable=FALSE }

    { 31022898;2;Field    ;
                ToolTipML=ENU=Specifies the number of the posted document that this document or journal line is applied to.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Applies-to Doc. No.";
                Editable=FALSE }

    { 1906801201;1;Group  ;
                CaptionML=[ENU=Shipping and Payment;
                           PTG=Envio e Pagamento];
                GroupType=Group }

    { 19  ;2   ;Group     ;
                CaptionML=[ENU=Ship-to;
                           PTG=Envio-a];
                GroupType=Group }

    { 34  ;3   ;Field     ;
                CaptionML=[ENU=Name;
                           PTG=Nome];
                ToolTipML=ENU=Specifies the name of the company at the address to which the items in the purchase order were shipped.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Ship-to Name";
                Editable=FALSE }

    { 36  ;3   ;Field     ;
                CaptionML=[ENU=Address;
                           PTG=Endere�o];
                ToolTipML=ENU=Specifies the address that the items in the purchase order were shipped to.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Ship-to Address";
                Editable=FALSE }

    { 38  ;3   ;Field     ;
                CaptionML=[ENU=Address 2;
                           PTG=Endere�o 2];
                ToolTipML=ENU=Specifies additional address information.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Ship-to Address 2";
                Editable=FALSE }

    { 72  ;3   ;Field     ;
                CaptionML=[ENU=Post Code;
                           PTG=C�d.Postal];
                ToolTipML=ENU=Specifies the postal code.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Ship-to Post Code";
                Editable=FALSE }

    { 40  ;3   ;Field     ;
                CaptionML=[ENU=City;
                           PTG=Cidade];
                ToolTipML=ENU=Specifies the city the items in the purchase order were shipped to.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Ship-to City";
                Editable=FALSE }

    { 31022891;3;Field    ;
                CaptionML=[ENU=County;
                           PTG=Distrito];
                SourceExpr="Ship-to County";
                Editable=FALSE }

    { 42  ;3   ;Field     ;
                CaptionML=[ENU=Contact;
                           PTG=Contacto];
                ToolTipML=ENU=Specifies the name of a contact person at the address that the items were shipped to.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Ship-to Contact";
                Editable=FALSE }

    { 17  ;2   ;Group     ;
                CaptionML=[ENU=Pay-to;
                           PTG=Pagamento-a];
                GroupType=Group }

    { 22  ;3   ;Field     ;
                CaptionML=[ENU=Name;
                           PTG=Nome];
                ToolTipML=ENU=Specifies the name of the vendor that you received the credit memo from.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Pay-to Name";
                Importance=Promoted;
                Editable=FALSE }

    { 26  ;3   ;Field     ;
                CaptionML=[ENU=Address;
                           PTG=Endere�o];
                ToolTipML=ENU=Specifies the address of the vendor that you received the credit memo from.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Pay-to Address";
                Importance=Additional;
                Editable=FALSE }

    { 28  ;3   ;Field     ;
                CaptionML=[ENU=Address 2;
                           PTG=Endere�o 2];
                ToolTipML=ENU=Specifies additional address information.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Pay-to Address 2";
                Importance=Additional;
                Editable=FALSE }

    { 70  ;3   ;Field     ;
                CaptionML=[ENU=Post Code;
                           PTG=C�d. Postal];
                ToolTipML=ENU=Specifies the postal code.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Pay-to Post Code";
                Importance=Additional;
                Editable=FALSE }

    { 30  ;3   ;Field     ;
                CaptionML=[ENU=City;
                           PTG=Cidade];
                ToolTipML=ENU=Specifies the city of the vendor you received the credit memo from.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Pay-to City";
                Importance=Additional;
                Editable=FALSE }

    { 1110002;3;Field     ;
                CaptionML=[ENU=County;
                           PTG=Distrito];
                SourceExpr="Pay-to County";
                Editable=FALSE }

    { 93  ;3   ;Field     ;
                CaptionML=[ENU=Contact No.;
                           PTG=N� Contacto];
                ToolTipML=ENU=Specifies the number of the contact at the vendor who handles the credit memo.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Pay-to Contact No.";
                Importance=Additional;
                Editable=FALSE }

    { 32  ;3   ;Field     ;
                CaptionML=[ENU=Contact;
                           PTG=Contacto];
                ToolTipML=ENU=Specifies the name of the person you should contact at the vendor who you received the credit memo from.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Pay-to Contact";
                Editable=FALSE }

    { 1907468901;1;Group  ;
                CaptionML=[ENU=Foreign Trade;
                           PTG=Com�rcio Externo] }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 3   ;1   ;Part      ;
                Name=IncomingDocAttachFactBox;
                ApplicationArea=#Basic,#Suite;
                PagePartID=Page193;
                Visible=NOT IsOfficeAddin;
                PartType=Page;
                ShowFilter=No }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {
    VAR
      PurchCrMemoHeader@1000 : Record 124;
      ChangeExchangeRate@1001 : Page 511;
      HasIncomingDocument@1002 : Boolean;
      IsOfficeAddin@1003 : Boolean;

    BEGIN
    {
      V93.00#00018 -  Reten��o a Clientes e Fornecedores -  - 2016.06.29
    }
    END.
  }
}

