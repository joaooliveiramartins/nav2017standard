OBJECT Page 1628 Office No Vendor Dlg
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Create vendor record?;
               PTG=Criar registo de fornecedor?];
    InsertAllowed=No;
    DeleteAllowed=No;
    ModifyAllowed=No;
    SourceTable=Table5050;
  }
  CONTROLS
  {
    { 1   ;    ;Container ;
                Name=Dialog;
                ContainerType=ContentArea }

    { 5   ;1   ;Field     ;
                ApplicationArea=#All;
                SourceExpr=STRSUBSTNO(VendDialogLbl,Name);
                ShowCaption=No }

    { 2   ;1   ;Group     ;
                GroupType=Group }

    { 3   ;2   ;Field     ;
                Name=CreateVend;
                ToolTipML=ENU=Specifies a new vendor for the contact.;
                ApplicationArea=#All;
                SourceExpr=STRSUBSTNO(CreateVendLbl,Name);
                Editable=FALSE;
                OnDrillDown=BEGIN
                              CreateVendor;
                              CurrPage.CLOSE;
                            END;

                ShowCaption=No }

    { 4   ;2   ;Field     ;
                Name=ViewVendList;
                ToolTipML=ENU=Specifies a list of vendors that are available in your company.;
                ApplicationArea=#All;
                SourceExpr=ViewVendListLbl;
                Editable=FALSE;
                OnDrillDown=VAR
                              Vendor@1000 : Record 23;
                            BEGIN
                              PAGE.RUN(PAGE::"Vendor List",Vendor);
                            END;

                ShowCaption=No }

  }
  CODE
  {
    VAR
      VendDialogLbl@1001 : TextConst '@@@="%1 = Contact name";ENU=Cannot find an existing vendor that matches the contact %1. Do you want to create a new vendor based on this contact?;PTG=N�o foi poss�vel encontrar um fornecedor existente que corresponda ao contacto %1. Deseja criar um novo fornecedor baseado neste contacto?';
      CreateVendLbl@1002 : TextConst '@@@="%1 = Contact name";ENU=Create a vendor record for %1;PTG=Criar um registo de fornecedor para %1';
      ViewVendListLbl@1000 : TextConst 'ENU=View vendor list;PTG=Ver lista fornecedor';

    BEGIN
    END.
  }
}

