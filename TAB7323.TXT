OBJECT Table 7323 Posted Whse. Shipment Line
{
  OBJECT-PROPERTIES
  {
    Date=07/09/12;
    Time=12:00:00;
    Version List=NAVW17.00;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Posted Whse. Shipment Line;
               PTG=Linha Envio Armaz�m Reg.];
    LookupPageID=Page7362;
  }
  FIELDS
  {
    { 1   ;   ;No.                 ;Code20        ;CaptionML=[ENU=No.;
                                                              PTG=N�];
                                                   Editable=No }
    { 2   ;   ;Line No.            ;Integer       ;CaptionML=[ENU=Line No.;
                                                              PTG=N� Linha];
                                                   Editable=No }
    { 3   ;   ;Source Type         ;Integer       ;CaptionML=[ENU=Source Type;
                                                              PTG=Tipo Origem];
                                                   Editable=No }
    { 4   ;   ;Source Subtype      ;Option        ;CaptionML=[ENU=Source Subtype;
                                                              PTG=Subtipo Origem];
                                                   OptionCaptionML=[ENU=0,1,2,3,4,5,6,7,8,9,10;
                                                                    PTG=0,1,2,3,4,5,6,7,8,9,10];
                                                   OptionString=0,1,2,3,4,5,6,7,8,9,10;
                                                   Editable=No }
    { 6   ;   ;Source No.          ;Code20        ;CaptionML=[ENU=Source No.;
                                                              PTG=N� Origem];
                                                   Editable=No }
    { 7   ;   ;Source Line No.     ;Integer       ;CaptionML=[ENU=Source Line No.;
                                                              PTG=N� Linha Fonte];
                                                   Editable=No }
    { 9   ;   ;Source Document     ;Option        ;CaptionML=[ENU=Source Document;
                                                              PTG=Documento Origem];
                                                   OptionCaptionML=[ENU=,Sales Order,,,Sales Return Order,Purchase Order,,,Purchase Return Order,,Outbound Transfer,,,,,,,,Service Order;
                                                                    PTG=,Enc. Venda,,,Devolu��o Venda,Enc. Compra,,,Devolu��o Compra,Transf. Sa�da,,,,,,,,Ordem Servi�o];
                                                   OptionString=,Sales Order,,,Sales Return Order,Purchase Order,,,Purchase Return Order,,Outbound Transfer,,,,,,,,Service Order;
                                                   Editable=No }
    { 10  ;   ;Location Code       ;Code10        ;TableRelation=Location;
                                                   CaptionML=[ENU=Location Code;
                                                              PTG=C�d. Localiza��o];
                                                   Editable=No }
    { 11  ;   ;Shelf No.           ;Code10        ;CaptionML=[ENU=Shelf No.;
                                                              PTG=N� Prateleira] }
    { 12  ;   ;Bin Code            ;Code20        ;TableRelation=IF (Zone Code=FILTER('')) Bin.Code WHERE (Location Code=FIELD(Location Code))
                                                                 ELSE IF (Zone Code=FILTER(<>'')) Bin.Code WHERE (Location Code=FIELD(Location Code),
                                                                                                                  Zone Code=FIELD(Zone Code));
                                                   CaptionML=[ENU=Bin Code;
                                                              PTG=C�d. Posi��o] }
    { 13  ;   ;Zone Code           ;Code10        ;TableRelation=Zone.Code WHERE (Location Code=FIELD(Location Code));
                                                   CaptionML=[ENU=Zone Code;
                                                              PTG=C�d. Zona] }
    { 14  ;   ;Item No.            ;Code20        ;TableRelation=Item;
                                                   CaptionML=[ENU=Item No.;
                                                              PTG=N� Produto];
                                                   Editable=No }
    { 15  ;   ;Quantity            ;Decimal       ;CaptionML=[ENU=Quantity;
                                                              PTG=Quantidade];
                                                   DecimalPlaces=0:5;
                                                   Editable=No }
    { 16  ;   ;Qty. (Base)         ;Decimal       ;CaptionML=[ENU=Qty. (Base);
                                                              PTG=Quantidade (Base)];
                                                   DecimalPlaces=0:5;
                                                   Editable=No }
    { 29  ;   ;Unit of Measure Code;Code10        ;TableRelation="Item Unit of Measure".Code WHERE (Item No.=FIELD(Item No.));
                                                   CaptionML=[ENU=Unit of Measure Code;
                                                              PTG=C�d. Unidade Medida];
                                                   Editable=No }
    { 30  ;   ;Qty. per Unit of Measure;Decimal   ;InitValue=1;
                                                   CaptionML=[ENU=Qty. per Unit of Measure;
                                                              PTG=Qtd. por Unidade Medida];
                                                   DecimalPlaces=0:5;
                                                   Editable=No }
    { 31  ;   ;Variant Code        ;Code10        ;TableRelation="Item Variant".Code WHERE (Item No.=FIELD(Item No.));
                                                   CaptionML=[ENU=Variant Code;
                                                              PTG=C�d. Variante];
                                                   Editable=No }
    { 32  ;   ;Description         ;Text50        ;CaptionML=[ENU=Description;
                                                              PTG=Descri��o];
                                                   Editable=No }
    { 33  ;   ;Description 2       ;Text50        ;CaptionML=[ENU=Description 2;
                                                              PTG=Descri��o 2];
                                                   Editable=No }
    { 36  ;   ;Due Date            ;Date          ;CaptionML=[ENU=Due Date;
                                                              PTG=Data Vencimento] }
    { 39  ;   ;Destination Type    ;Option        ;CaptionML=[ENU=Destination Type;
                                                              PTG=Tipo Destino];
                                                   OptionCaptionML=[ENU=" ,Customer,Vendor,Location";
                                                                    PTG=" ,Cliente,Fornecedor,Localiza��o"];
                                                   OptionString=[ ,Customer,Vendor,Location];
                                                   Editable=No }
    { 40  ;   ;Destination No.     ;Code20        ;TableRelation=IF (Destination Type=CONST(Customer)) Customer.No.
                                                                 ELSE IF (Destination Type=CONST(Vendor)) Vendor.No.
                                                                 ELSE IF (Destination Type=CONST(Location)) Location.Code;
                                                   CaptionML=[ENU=Destination No.;
                                                              PTG=N� Destino];
                                                   Editable=No }
    { 44  ;   ;Shipping Advice     ;Option        ;CaptionML=[ENU=Shipping Advice;
                                                              PTG=Envio Parcial/Total];
                                                   OptionCaptionML=[ENU=Partial,Complete;
                                                                    PTG=Parcial,Completo];
                                                   OptionString=Partial,Complete;
                                                   Editable=No }
    { 45  ;   ;Shipment Date       ;Date          ;CaptionML=[ENU=Shipment Date;
                                                              PTG=Data Envio] }
    { 60  ;   ;Posted Source Document;Option      ;CaptionML=[ENU=Posted Source Document;
                                                              PTG=Documento Origem Registado];
                                                   OptionCaptionML=[ENU=" ,Posted Receipt,,Posted Return Receipt,,Posted Shipment,,Posted Return Shipment,,,Posted Transfer Shipment";
                                                                    PTG=" ,Guia Remessa Reg.,,Rece��o Devolu��o Reg.,,Envio Reg.,,Envio Devolu��o Reg.,,,Envio Transfer�ncia Reg."];
                                                   OptionString=[ ,Posted Receipt,,Posted Return Receipt,,Posted Shipment,,Posted Return Shipment,,,Posted Transfer Shipment] }
    { 61  ;   ;Posted Source No.   ;Code20        ;CaptionML=[ENU=Posted Source No.;
                                                              PTG=N� Origem Reg.] }
    { 62  ;   ;Posting Date        ;Date          ;CaptionML=[ENU=Posting Date;
                                                              PTG=Data Registo] }
    { 63  ;   ;Whse. Shipment No.  ;Code20        ;CaptionML=[ENU=Whse. Shipment No.;
                                                              PTG=N� Envio Armaz�m];
                                                   Editable=No }
    { 64  ;   ;Whse Shipment Line No.;Integer     ;CaptionML=[ENU=Whse Shipment Line No.;
                                                              PTG=N� Linha Envio Armaz�m];
                                                   Editable=No }
  }
  KEYS
  {
    {    ;No.,Line No.                            ;Clustered=Yes }
    {    ;Whse. Shipment No.,Whse Shipment Line No. }
    {    ;Posted Source No.,Posting Date           }
    {    ;Source Type,Source Subtype,Source No.,Source Line No. }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    END.
  }
}

