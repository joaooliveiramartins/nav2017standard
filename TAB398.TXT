OBJECT Table 398 XBRL Rollup Line
{
  OBJECT-PROPERTIES
  {
    Date=05/08/11;
    Time=12:00:00;
    Version List=NAVW16.00;
  }
  PROPERTIES
  {
    CaptionML=[ENU=XBRL Rollup Line;
               PTG=Linha Rollup XBRL];
  }
  FIELDS
  {
    { 1   ;   ;XBRL Taxonomy Name  ;Code20        ;TableRelation="XBRL Taxonomy";
                                                   CaptionML=[ENU=XBRL Taxonomy Name;
                                                              PTG=Nome Taxonomia XBRL] }
    { 2   ;   ;XBRL Taxonomy Line No.;Integer     ;TableRelation="XBRL Taxonomy Line"."Line No." WHERE (XBRL Taxonomy Name=FIELD(XBRL Taxonomy Name));
                                                   CaptionML=[ENU=XBRL Taxonomy Line No.;
                                                              PTG=N� Linha Taxonomia XBRL] }
    { 4   ;   ;From XBRL Taxonomy Line No.;Integer;TableRelation="XBRL Taxonomy Line"."Line No." WHERE (XBRL Taxonomy Name=FIELD(XBRL Taxonomy Name));
                                                   CaptionML=[ENU=From XBRL Taxonomy Line No.;
                                                              PTG=De N� Linha Taxonomia XBRL] }
    { 5   ;   ;From XBRL Taxonomy Line Name;Text250;
                                                   FieldClass=FlowField;
                                                   CalcFormula=Lookup("XBRL Taxonomy Line".Name WHERE (XBRL Taxonomy Name=FIELD(XBRL Taxonomy Name),
                                                                                                       Line No.=FIELD(From XBRL Taxonomy Line No.)));
                                                   CaptionML=[ENU=From XBRL Taxonomy Line Name;
                                                              PTG=De Nome Linha Taxonomia XBRL];
                                                   Editable=No }
    { 6   ;   ;From XBRL Taxonomy Line Label;Text250;
                                                   FieldClass=FlowField;
                                                   CalcFormula=Lookup("XBRL Taxonomy Label".Label WHERE (XBRL Taxonomy Name=FIELD(XBRL Taxonomy Name),
                                                                                                         XBRL Taxonomy Line No.=FIELD(From XBRL Taxonomy Line No.),
                                                                                                         XML Language Identifier=FIELD(Label Language Filter)));
                                                   CaptionML=[ENU=From XBRL Taxonomy Line Label;
                                                              PTG=De Etiqueta Linha Taxonomia XBRL];
                                                   Editable=No }
    { 8   ;   ;Weight              ;Decimal       ;CaptionML=[ENU=Weight;
                                                              PTG=Peso];
                                                   DecimalPlaces=0:0;
                                                   MinValue=-1;
                                                   MaxValue=1 }
    { 9   ;   ;Label Language Filter;Text10       ;FieldClass=FlowFilter;
                                                   CaptionML=[ENU=Label Language Filter;
                                                              PTG=Filtro Idioma Etiqueta] }
  }
  KEYS
  {
    {    ;XBRL Taxonomy Name,XBRL Taxonomy Line No.,From XBRL Taxonomy Line No.;
                                                   Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    END.
  }
}

