OBJECT Page 6655 Return Shipment Statistics
{
  OBJECT-PROPERTIES
  {
    Date=05/08/11;
    Time=12:00:00;
    Version List=NAVW16.00;
  }
  PROPERTIES
  {
    Editable=No;
    CaptionML=[ENU=Return Shipment Statistics;
               PTG=Estat�sticas Envio Devolu��o];
    LinksAllowed=No;
    SourceTable=Table6650;
    PageType=Card;
    OnAfterGetRecord=BEGIN
                       CLEARALL;

                       ReturnShptLine.SETRANGE("Document No.","No.");

                       IF ReturnShptLine.FIND('-') THEN
                         REPEAT
                           LineQty := LineQty + ReturnShptLine.Quantity;
                           TotalNetWeight := TotalNetWeight + (ReturnShptLine.Quantity * ReturnShptLine."Net Weight");
                           TotalGrossWeight := TotalGrossWeight + (ReturnShptLine.Quantity * ReturnShptLine."Gross Weight");
                           TotalVolume := TotalVolume + (ReturnShptLine.Quantity * ReturnShptLine."Unit Volume");
                           IF ReturnShptLine."Units per Parcel" > 0 THEN
                             TotalParcels := TotalParcels + ROUND(ReturnShptLine.Quantity / ReturnShptLine."Units per Parcel",1,'>');
                         UNTIL ReturnShptLine.NEXT = 0;
                     END;

  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 2   ;1   ;Group     ;
                CaptionML=[ENU=General;
                           PTG=Geral] }

    { 3   ;2   ;Field     ;
                CaptionML=[ENU=Quantity;
                           PTG=Quantidade];
                DecimalPlaces=0:5;
                SourceExpr=LineQty }

    { 13  ;2   ;Field     ;
                CaptionML=[ENU=Parcels;
                           PTG=Lotes];
                DecimalPlaces=0:5;
                SourceExpr=TotalParcels }

    { 7   ;2   ;Field     ;
                CaptionML=[ENU=Net Weight;
                           PTG=Peso L�quido];
                DecimalPlaces=0:5;
                SourceExpr=TotalNetWeight }

    { 10  ;2   ;Field     ;
                CaptionML=[ENU=Gross Weight;
                           PTG=Peso Bruto];
                DecimalPlaces=0:5;
                SourceExpr=TotalGrossWeight }

    { 17  ;2   ;Field     ;
                CaptionML=[ENU=Volume;
                           PTG=Volume];
                DecimalPlaces=0:5;
                SourceExpr=TotalVolume }

  }
  CODE
  {
    VAR
      ReturnShptLine@1000 : Record 6651;
      LineQty@1001 : Decimal;
      TotalNetWeight@1002 : Decimal;
      TotalGrossWeight@1003 : Decimal;
      TotalVolume@1004 : Decimal;
      TotalParcels@1005 : Decimal;

    BEGIN
    END.
  }
}

