OBJECT Table 911 Posted Assembly Line
{
  OBJECT-PROPERTIES
  {
    Date=15/09/15;
    Time=12:00:00;
    Version List=NAVW19.00;
  }
  PROPERTIES
  {
    OnDelete=VAR
               AssemblyCommentLine@1000 : Record 906;
             BEGIN
               AssemblyCommentLine.SETRANGE("Document Type",AssemblyCommentLine."Document Type"::"Posted Assembly");
               AssemblyCommentLine.SETRANGE("Document No.","Document No.");
               AssemblyCommentLine.SETRANGE("Document Line No.","Line No.");
               IF NOT AssemblyCommentLine.ISEMPTY THEN
                 AssemblyCommentLine.DELETEALL;
             END;

    CaptionML=[ENU=Posted Assembly Line;
               PTG=Linha Montagem Registada];
    LookupPageID=Page921;
    DrillDownPageID=Page921;
  }
  FIELDS
  {
    { 2   ;   ;Document No.        ;Code20        ;CaptionML=[ENU=Document No.;
                                                              PTG=N� Documento] }
    { 3   ;   ;Line No.            ;Integer       ;CaptionML=[ENU=Line No.;
                                                              PTG=N� Linha] }
    { 8   ;   ;Order No.           ;Code20        ;CaptionML=[ENU=Order No.;
                                                              PTG=N� Encomenda] }
    { 9   ;   ;Order Line No.      ;Integer       ;CaptionML=[ENU=Order Line No.;
                                                              PTG=N� Linha Encomenda] }
    { 10  ;   ;Type                ;Option        ;CaptionML=[ENU=Type;
                                                              PTG=Tipo];
                                                   OptionCaptionML=[ENU=" ,Item,Resource";
                                                                    PTG=" ,Produto,Recurso"];
                                                   OptionString=[ ,Item,Resource] }
    { 11  ;   ;No.                 ;Code20        ;TableRelation=IF (Type=CONST(Item)) Item
                                                                 ELSE IF (Type=CONST(Resource)) Resource;
                                                   CaptionML=[ENU=No.;
                                                              PTG=N�] }
    { 12  ;   ;Variant Code        ;Code10        ;TableRelation="Item Variant".Code WHERE (Item No.=FIELD(No.),
                                                                                            Code=FIELD(Variant Code));
                                                   CaptionML=[ENU=Variant Code;
                                                              PTG=C�d. Variante] }
    { 13  ;   ;Description         ;Text50        ;CaptionML=[ENU=Description;
                                                              PTG=Descri��o] }
    { 14  ;   ;Description 2       ;Text50        ;CaptionML=[ENU=Description 2;
                                                              PTG=Descri��o 2] }
    { 18  ;   ;Lead-Time Offset    ;DateFormula   ;CaptionML=[ENU=Lead-Time Offset;
                                                              PTG=Compensa��o Prazo Entrega] }
    { 19  ;   ;Resource Usage Type ;Option        ;CaptionML=[ENU=Resource Usage Type;
                                                              PTG=Tipo Utiliza��o Recurso];
                                                   OptionCaptionML=[ENU=" ,Direct,Fixed";
                                                                    PTG=" ,Direto,Fixo"];
                                                   OptionString=[ ,Direct,Fixed] }
    { 20  ;   ;Location Code       ;Code10        ;TableRelation=Location WHERE (Use As In-Transit=CONST(No));
                                                   CaptionML=[ENU=Location Code;
                                                              PTG=C�d. Localiza��o] }
    { 21  ;   ;Shortcut Dimension 1 Code;Code20   ;TableRelation="Dimension Value".Code WHERE (Global Dimension No.=CONST(1));
                                                   CaptionML=[ENU=Shortcut Dimension 1 Code;
                                                              PTG=C�d. Atalho Dimens�o 1];
                                                   CaptionClass='1,2,1' }
    { 22  ;   ;Shortcut Dimension 2 Code;Code20   ;TableRelation="Dimension Value".Code WHERE (Global Dimension No.=CONST(2));
                                                   CaptionML=[ENU=Shortcut Dimension 2 Code;
                                                              PTG=C�d. Atalho Dimens�o 2];
                                                   CaptionClass='1,2,2' }
    { 23  ;   ;Bin Code            ;Code20        ;AccessByPermission=TableData 5771=R;
                                                   CaptionML=[ENU=Bin Code;
                                                              PTG=C�d. Posi��o] }
    { 25  ;   ;Position            ;Code10        ;CaptionML=[ENU=Position;
                                                              PTG=Posi��o] }
    { 26  ;   ;Position 2          ;Code10        ;CaptionML=[ENU=Position 2;
                                                              PTG=Posi��o 2] }
    { 27  ;   ;Position 3          ;Code10        ;CaptionML=[ENU=Position 3;
                                                              PTG=Posi��o 3] }
    { 39  ;   ;Item Shpt. Entry No.;Integer       ;CaptionML=[ENU=Item Shpt. Entry No.;
                                                              PTG=N� Mov. Produto Assoc.] }
    { 40  ;   ;Quantity            ;Decimal       ;CaptionML=[ENU=Quantity;
                                                              PTG=Quantidade];
                                                   DecimalPlaces=0:5;
                                                   MinValue=0 }
    { 41  ;   ;Quantity (Base)     ;Decimal       ;CaptionML=[ENU=Quantity (Base);
                                                              PTG=Quantidade (Base)];
                                                   DecimalPlaces=0:5 }
    { 52  ;   ;Due Date            ;Date          ;CaptionML=[ENU=Due Date;
                                                              PTG=Data Vencimento] }
    { 60  ;   ;Quantity per        ;Decimal       ;CaptionML=[ENU=Quantity per;
                                                              PTG=Quantidade por];
                                                   DecimalPlaces=0:5 }
    { 61  ;   ;Qty. per Unit of Measure;Decimal   ;CaptionML=[ENU=Qty. per Unit of Measure;
                                                              PTG=Qtd. por Unidade Medida];
                                                   DecimalPlaces=0:5;
                                                   Editable=No }
    { 62  ;   ;Inventory Posting Group;Code10     ;TableRelation="Inventory Posting Group";
                                                   CaptionML=[ENU=Inventory Posting Group;
                                                              PTG=Gr. Contabil�stico Invent�rio] }
    { 63  ;   ;Gen. Prod. Posting Group;Code10    ;TableRelation="Gen. Product Posting Group";
                                                   CaptionML=[ENU=Gen. Prod. Posting Group;
                                                              PTG=Gr. Contabil�stico Produto] }
    { 65  ;   ;Unit Cost           ;Decimal       ;CaptionML=[ENU=Unit Cost;
                                                              PTG=Custo Unit�rio];
                                                   AutoFormatType=2 }
    { 67  ;   ;Cost Amount         ;Decimal       ;CaptionML=[ENU=Cost Amount;
                                                              PTG=Valor Custo];
                                                   Editable=No;
                                                   AutoFormatType=1 }
    { 80  ;   ;Unit of Measure Code;Code10        ;TableRelation=IF (Type=CONST(Item)) "Item Unit of Measure".Code WHERE (Item No.=FIELD(No.))
                                                                 ELSE IF (Type=CONST(Resource)) "Resource Unit of Measure".Code WHERE (Resource No.=FIELD(No.));
                                                   CaptionML=[ENU=Unit of Measure Code;
                                                              PTG=C�d. Unidade Medida] }
    { 480 ;   ;Dimension Set ID    ;Integer       ;TableRelation="Dimension Set Entry";
                                                   OnLookup=BEGIN
                                                              ShowDimensions;
                                                            END;

                                                   CaptionML=[ENU=Dimension Set ID;
                                                              PTG=ID Combina��o Dimens�o];
                                                   Editable=No }
  }
  KEYS
  {
    {    ;Document No.,Line No.                   ;Clustered=Yes }
    {    ;Order No.,Order Line No.                 }
    {    ;Type,No.                                 }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    PROCEDURE ShowDimensions@30();
    VAR
      DimMgt@1000 : Codeunit 408;
    BEGIN
      DimMgt.ShowDimensionSet("Dimension Set ID",STRSUBSTNO('%1 %2',TABLECAPTION,"Document No."));
    END;

    PROCEDURE ShowItemTrackingLines@1();
    VAR
      ItemTrackingDocMgt@1000 : Codeunit 6503;
    BEGIN
      ItemTrackingDocMgt.ShowItemTrackingForShptRcptLine(DATABASE::"Posted Assembly Line",0,"Document No.",'',0,"Line No.");
    END;

    PROCEDURE GetAssemblyLinesForDocument@2(VAR TempPostedAssemblyLine@1000 : TEMPORARY Record 911;ValueEntryDocType@1001 : Option;DocNo@1002 : Code[20];DocLineNo@1003 : Integer);
    VAR
      ValueEntry@1008 : Record 5802;
      ItemLedgerEntry@1007 : Record 32;
      PostedAsmHeader@1006 : Record 910;
      PostedAsmLine@1005 : Record 911;
      SalesShipmentLine@1004 : Record 111;
    BEGIN
      TempPostedAssemblyLine.RESET;
      TempPostedAssemblyLine.DELETEALL;
      ValueEntry.SETRANGE("Document Type",ValueEntryDocType);
      ValueEntry.SETRANGE("Document No.",DocNo);
      ValueEntry.SETRANGE("Document Line No.",DocLineNo);
      IF NOT ValueEntry.FINDSET THEN
        EXIT;
      REPEAT
        IF ItemLedgerEntry.GET(ValueEntry."Item Ledger Entry No.") THEN
          IF ItemLedgerEntry."Document Type" = ItemLedgerEntry."Document Type"::"Sales Shipment" THEN BEGIN
            SalesShipmentLine.GET(ItemLedgerEntry."Document No.",ItemLedgerEntry."Document Line No.");
            IF SalesShipmentLine.AsmToShipmentExists(PostedAsmHeader) THEN BEGIN
              PostedAsmLine.SETRANGE("Document No.",PostedAsmHeader."No.");
              IF PostedAsmLine.FINDSET THEN
                REPEAT
                  TempPostedAssemblyLine.SETRANGE(Type,PostedAsmLine.Type);
                  TempPostedAssemblyLine.SETRANGE("No.",PostedAsmLine."No.");
                  TempPostedAssemblyLine.SETRANGE("Variant Code",PostedAsmLine."Variant Code");
                  TempPostedAssemblyLine.SETRANGE(Description,PostedAsmLine.Description);
                  TempPostedAssemblyLine.SETRANGE("Unit of Measure Code",PostedAsmLine."Unit of Measure Code");
                  IF TempPostedAssemblyLine.FINDFIRST THEN BEGIN
                    TempPostedAssemblyLine.Quantity += PostedAsmLine.Quantity;
                    TempPostedAssemblyLine.MODIFY;
                  END ELSE BEGIN
                    TempPostedAssemblyLine := PostedAsmLine;
                    TempPostedAssemblyLine.INSERT;
                  END;
                UNTIL PostedAsmLine.NEXT = 0;
            END;
          END;
      UNTIL ValueEntry.NEXT = 0;
      TempPostedAssemblyLine.RESET;
    END;

    BEGIN
    END.
  }
}

