OBJECT Page 99000793 Family Line List
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0;
  }
  PROPERTIES
  {
    Editable=No;
    CaptionML=[ENU=Family Line List;
               PTG=Lista Linhas Fam�lia];
    SourceTable=Table99000774;
    PageType=List;
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 4   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the line number of the product family line.;
                           PTG=""];
                SourceExpr="Line No.";
                Visible=FALSE }

    { 15  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies which items belong to a family.;
                           PTG=""];
                SourceExpr="Item No." }

    { 2   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies a description for the product family line.;
                           PTG=""];
                SourceExpr=Description }

    { 6   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies an extended description if there is not enough space in the Description field.;
                           PTG=""];
                SourceExpr="Description 2";
                Visible=FALSE }

    { 8   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the unit of measure the family line refers to.;
                           PTG=""];
                SourceExpr="Unit of Measure Code" }

    { 10  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the quantity for the item in this family line.;
                           PTG=""];
                SourceExpr=Quantity }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {

    BEGIN
    END.
  }
}

