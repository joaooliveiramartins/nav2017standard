OBJECT Page 9086 Service Hist. Bill-to FactBox
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Customer Service History - Bill-to Customer;
               PTG=Hist�rico Servi�o do Cliente - Fatura-a Cliente];
    SourceTable=Table18;
    PageType=CardPart;
    OnOpenPage=BEGIN
                 RegularFastTabVisible := CURRENTCLIENTTYPE = CLIENTTYPE::Windows;
               END;

    OnFindRecord=BEGIN
                   NoOfQuotes := 0;
                   NoOfOrders := 0;
                   NoOfInvoices := 0;
                   NoOfCreditMemos := 0;
                   NoOfPostedShipments := 0;
                   NoOfPostedInvoices := 0;
                   NoOfPostedCreditMemos := 0;

                   IF FIND(Which) THEN BEGIN
                     FILTERGROUP(4);
                     SETFILTER("No.",GetBillToCustomerNo);
                     FILTERGROUP(0);
                   END;

                   EXIT(FIND(Which));
                 END;

    OnAfterGetRecord=BEGIN
                       CalcNoOfBillRecords;
                     END;

  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 10  ;1   ;Field     ;
                CaptionML=[ENU=Customer No.;
                           PTG=N� Cliente];
                ToolTipML=[ENU=Specifies the number of the customer. The field is either filled automatically from a defined number series, or you enter the number manually because you have enabled manual number entry in the number-series setup.;
                           PTG=""];
                ApplicationArea=#All;
                SourceExpr="No.";
                OnDrillDown=BEGIN
                              ShowDetails;
                            END;
                             }

    { 1   ;1   ;Group     ;
                Visible=RegularFastTabVisible;
                GroupType=Group }

    { 2   ;2   ;Field     ;
                DrillDown=Yes;
                CaptionML=[ENU=Quotes;
                           PTG=Propostas];
                SourceExpr=NoOfQuotes;
                Editable=TRUE;
                OnDrillDown=VAR
                              ServiceHeader@1000 : Record 5900;
                            BEGIN
                              ServiceHeader.SETRANGE("Bill-to Customer No.","No.");
                              PAGE.RUN(PAGE::"Service Quotes",ServiceHeader);
                            END;
                             }

    { 4   ;2   ;Field     ;
                DrillDown=Yes;
                CaptionML=[ENU=Orders;
                           PTG=Encomendas];
                SourceExpr=NoOfOrders;
                Editable=TRUE;
                OnDrillDown=VAR
                              ServiceHeader@1000 : Record 5900;
                            BEGIN
                              ServiceHeader.SETRANGE("Bill-to Customer No.","No.");
                              PAGE.RUN(PAGE::"Service Orders",ServiceHeader);
                            END;
                             }

    { 11  ;2   ;Field     ;
                DrillDown=Yes;
                CaptionML=[ENU=Invoices;
                           PTG=Faturas];
                SourceExpr=NoOfInvoices;
                Editable=TRUE;
                OnDrillDown=VAR
                              ServiceHeader@1000 : Record 5900;
                            BEGIN
                              ServiceHeader.SETRANGE("Bill-to Customer No.","No.");
                              PAGE.RUN(PAGE::"Service Invoices",ServiceHeader);
                            END;
                             }

    { 13  ;2   ;Field     ;
                DrillDown=Yes;
                CaptionML=[ENU=Credit Memos;
                           PTG=Notas Cr�dito];
                SourceExpr=NoOfCreditMemos;
                Editable=TRUE;
                OnDrillDown=VAR
                              ServiceHeader@1000 : Record 5900;
                            BEGIN
                              ServiceHeader.SETRANGE("Bill-to Customer No.","No.");
                              PAGE.RUN(PAGE::"Service Credit Memos",ServiceHeader);
                            END;
                             }

    { 15  ;2   ;Field     ;
                DrillDown=Yes;
                CaptionML=[ENU=Pstd. Shipments;
                           PTG=Envios Registados];
                SourceExpr=NoOfPostedShipments;
                Editable=TRUE;
                OnDrillDown=VAR
                              ServiceShipmentHdr@1000 : Record 5990;
                            BEGIN
                              ServiceShipmentHdr.SETRANGE("Bill-to Customer No.","No.");
                              PAGE.RUN(PAGE::"Posted Service Shipments",ServiceShipmentHdr);
                            END;
                             }

    { 17  ;2   ;Field     ;
                DrillDown=Yes;
                CaptionML=[ENU=Pstd. Invoices;
                           PTG=Faturas Registadas];
                SourceExpr=NoOfPostedInvoices;
                Editable=TRUE;
                OnDrillDown=VAR
                              ServiceInvoiceHdr@1000 : Record 5992;
                            BEGIN
                              ServiceInvoiceHdr.SETRANGE("Bill-to Customer No.","No.");
                              PAGE.RUN(PAGE::"Posted Service Invoices",ServiceInvoiceHdr);
                            END;
                             }

    { 19  ;2   ;Field     ;
                DrillDown=Yes;
                CaptionML=[ENU=Pstd. Credit Memos;
                           PTG=Notas Cr�dito Registadas];
                SourceExpr=NoOfPostedCreditMemos;
                Editable=TRUE;
                OnDrillDown=VAR
                              ServiceCrMemoHdr@1000 : Record 5994;
                            BEGIN
                              ServiceCrMemoHdr.SETRANGE("Bill-to Customer No.","No.");
                              PAGE.RUN(PAGE::"Posted Service Credit Memos",ServiceCrMemoHdr);
                            END;
                             }

    { 14  ;1   ;Group     ;
                Visible=NOT RegularFastTabVisible;
                GroupType=CueGroup }

    { 12  ;2   ;Field     ;
                Name=NoOfQuotesTile;
                DrillDown=Yes;
                CaptionML=[ENU=Quotes;
                           PTG=Propostas];
                SourceExpr=NoOfQuotes;
                Editable=TRUE;
                OnDrillDown=VAR
                              ServiceHeader@1000 : Record 5900;
                            BEGIN
                              ServiceHeader.SETRANGE("Bill-to Customer No.","No.");
                              PAGE.RUN(PAGE::"Service Quotes",ServiceHeader);
                            END;
                             }

    { 9   ;2   ;Field     ;
                Name=NoOfOrdersTile;
                DrillDown=Yes;
                CaptionML=[ENU=Orders;
                           PTG=Encomendas];
                SourceExpr=NoOfOrders;
                Editable=TRUE;
                OnDrillDown=VAR
                              ServiceHeader@1000 : Record 5900;
                            BEGIN
                              ServiceHeader.SETRANGE("Bill-to Customer No.","No.");
                              PAGE.RUN(PAGE::"Service Orders",ServiceHeader);
                            END;
                             }

    { 8   ;2   ;Field     ;
                Name=NoOfInvoicesTile;
                DrillDown=Yes;
                CaptionML=[ENU=Invoices;
                           PTG=Faturas];
                SourceExpr=NoOfInvoices;
                Editable=TRUE;
                OnDrillDown=VAR
                              ServiceHeader@1000 : Record 5900;
                            BEGIN
                              ServiceHeader.SETRANGE("Bill-to Customer No.","No.");
                              PAGE.RUN(PAGE::"Service Invoices",ServiceHeader);
                            END;
                             }

    { 7   ;2   ;Field     ;
                Name=NoOfCreditMemosTile;
                DrillDown=Yes;
                CaptionML=[ENU=Credit Memos;
                           PTG=Notas de Cr�dito];
                SourceExpr=NoOfCreditMemos;
                Editable=TRUE;
                OnDrillDown=VAR
                              ServiceHeader@1000 : Record 5900;
                            BEGIN
                              ServiceHeader.SETRANGE("Bill-to Customer No.","No.");
                              PAGE.RUN(PAGE::"Service Credit Memos",ServiceHeader);
                            END;
                             }

    { 6   ;2   ;Field     ;
                Name=NoOfPostedShipmentsTile;
                DrillDown=Yes;
                CaptionML=[ENU=Pstd. Shipments;
                           PTG=Envios Registados];
                SourceExpr=NoOfPostedShipments;
                Editable=TRUE;
                OnDrillDown=VAR
                              ServiceShipmentHdr@1000 : Record 5990;
                            BEGIN
                              ServiceShipmentHdr.SETRANGE("Bill-to Customer No.","No.");
                              PAGE.RUN(PAGE::"Posted Service Shipments",ServiceShipmentHdr);
                            END;
                             }

    { 5   ;2   ;Field     ;
                Name=NoOfPostedInvoicesTile;
                DrillDown=Yes;
                CaptionML=[ENU=Pstd. Invoices;
                           PTG=Faturas Registadas];
                SourceExpr=NoOfPostedInvoices;
                Editable=TRUE;
                OnDrillDown=VAR
                              ServiceInvoiceHdr@1000 : Record 5992;
                            BEGIN
                              ServiceInvoiceHdr.SETRANGE("Bill-to Customer No.","No.");
                              PAGE.RUN(PAGE::"Posted Service Invoices",ServiceInvoiceHdr);
                            END;
                             }

    { 3   ;2   ;Field     ;
                Name=NoOfPostedCreditMemosTile;
                DrillDown=Yes;
                CaptionML=[ENU=Pstd. Credit Memos;
                           PTG=Notas de Cr�dito Registadas];
                SourceExpr=NoOfPostedCreditMemos;
                Editable=TRUE;
                OnDrillDown=VAR
                              ServiceCrMemoHdr@1000 : Record 5994;
                            BEGIN
                              ServiceCrMemoHdr.SETRANGE("Bill-to Customer No.","No.");
                              PAGE.RUN(PAGE::"Posted Service Credit Memos",ServiceCrMemoHdr);
                            END;
                             }

  }
  CODE
  {
    VAR
      RegularFastTabVisible@1007 : Boolean;
      NoOfQuotes@1006 : Integer;
      NoOfOrders@1005 : Integer;
      NoOfInvoices@1004 : Integer;
      NoOfCreditMemos@1003 : Integer;
      NoOfPostedShipments@1002 : Integer;
      NoOfPostedInvoices@1001 : Integer;
      NoOfPostedCreditMemos@1000 : Integer;

    LOCAL PROCEDURE ShowDetails@1102601000();
    BEGIN
      PAGE.RUN(PAGE::"Customer Card",Rec);
    END;

    LOCAL PROCEDURE CalcNoOfBillRecords@3();
    VAR
      ServHeader@1000 : Record 5900;
      ServShptHeader@1003 : Record 5990;
      ServInvHeader@1001 : Record 5992;
      ServCrMemoHeader@1002 : Record 5994;
    BEGIN
      ServHeader.RESET;
      ServHeader.SETRANGE("Document Type",ServHeader."Document Type"::Quote);
      ServHeader.SETRANGE("Bill-to Customer No.","No.");
      NoOfQuotes := ServHeader.COUNT;

      ServHeader.RESET;
      ServHeader.SETRANGE("Document Type",ServHeader."Document Type"::Order);
      ServHeader.SETRANGE("Bill-to Customer No.","No.");
      NoOfOrders := ServHeader.COUNT;

      ServHeader.RESET;
      ServHeader.SETRANGE("Document Type",ServHeader."Document Type"::Invoice);
      ServHeader.SETRANGE("Bill-to Customer No.","No.");
      NoOfInvoices := ServHeader.COUNT;

      ServHeader.RESET;
      ServHeader.SETRANGE("Document Type",ServHeader."Document Type"::"Credit Memo");
      ServHeader.SETRANGE("Bill-to Customer No.","No.");
      NoOfCreditMemos := ServHeader.COUNT;

      ServShptHeader.RESET;
      ServShptHeader.SETRANGE("Bill-to Customer No.","No.");
      NoOfPostedShipments := ServShptHeader.COUNT;

      ServInvHeader.RESET;
      ServInvHeader.SETRANGE("Bill-to Customer No.","No.");
      NoOfPostedInvoices := ServInvHeader.COUNT;

      ServCrMemoHeader.RESET;
      ServCrMemoHeader.SETRANGE("Bill-to Customer No.","No.");
      NoOfPostedCreditMemos := ServCrMemoHeader.COUNT;
    END;

    BEGIN
    END.
  }
}

