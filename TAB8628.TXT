OBJECT Table 8628 Config. Field Mapping
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Config. Field Mapping;
               PTG=Configura��o Mapeamento de Campos];
  }
  FIELDS
  {
    { 1   ;   ;Package Code        ;Code20        ;TableRelation="Config. Package";
                                                   CaptionML=[ENU=Package Code;
                                                              PTG=C�d. Package];
                                                   NotBlank=Yes }
    { 2   ;   ;Table ID            ;Integer       ;TableRelation=AllObjWithCaption."Object ID" WHERE (Object Type=CONST(Table));
                                                   CaptionML=[ENU=Table ID;
                                                              PTG=ID Tabela];
                                                   NotBlank=Yes }
    { 3   ;   ;Field ID            ;Integer       ;CaptionML=[ENU=Field ID;
                                                              PTG=ID Campo];
                                                   NotBlank=Yes }
    { 4   ;   ;Field Name          ;Text30        ;CaptionML=[ENU=Field Name;
                                                              PTG=Nome Campo] }
    { 5   ;   ;Old Value           ;Text250       ;CaptionML=[ENU=Old Value;
                                                              PTG=Valor Antigo] }
    { 6   ;   ;New Value           ;Text250       ;CaptionML=[ENU=New Value;
                                                              PTG=Novo Valor] }
  }
  KEYS
  {
    {    ;Package Code,Table ID,Field ID,Old Value;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    END.
  }
}

