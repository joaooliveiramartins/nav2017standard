OBJECT Page 5136 Vendor Link
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Vendor Link;
               PTG=Link Fornecedor];
    InsertAllowed=No;
    DeleteAllowed=No;
    LinksAllowed=No;
    SourceTable=Table5054;
    PageType=Card;
    OnQueryClosePage=BEGIN
                       IF ("No." <> '') AND (CloseAction = ACTION::LookupOK) THEN BEGIN
                         TESTFIELD("No.");
                         ContBusRel := Rec;
                         ContBusRel.INSERT(TRUE);
                         CASE CurrMasterFields OF
                           CurrMasterFields::Contact:
                             BEGIN
                               Cont.GET(ContBusRel."Contact No.");
                               UpdateCustVendBank.UpdateVendor(Cont,ContBusRel);
                             END;
                           CurrMasterFields::Vendor:
                             BEGIN
                               Vend.GET(ContBusRel."No.");
                               UpdateContFromVend.OnModify(Vend);
                             END;
                         END;
                       END;
                     END;

  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                CaptionML=[ENU=General;
                           PTG=Geral] }

    { 2   ;2   ;Field     ;
                CaptionML=[ENU=Vendor No.;
                           PTG=N� Fornecedor];
                ToolTipML=ENU=Specifies the number assigned to the contact in the Customer, Vendor, or Bank Account table. This field is only valid for contacts recorded as customer, vendor or bank accounts.;
                ApplicationArea=#RelationshipMgmt;
                SourceExpr="No." }

    { 5   ;2   ;Field     ;
                CaptionML=[ENU=Current Master Fields;
                           PTG=Campos Base Atuais];
                ToolTipML=ENU=Specifies which fields should be used to prioritize in case there is conflicting information in fields common to the contact card and the bank account card.;
                OptionCaptionML=[ENU=Contact,Vendor;
                                 PTG=Contacto,Fornecedor];
                ApplicationArea=#RelationshipMgmt;
                SourceExpr=CurrMasterFields }

  }
  CODE
  {
    VAR
      ContBusRel@1000 : Record 5054;
      Cont@1001 : Record 5050;
      Vend@1002 : Record 23;
      UpdateCustVendBank@1003 : Codeunit 5055;
      UpdateContFromVend@1004 : Codeunit 5057;
      CurrMasterFields@1005 : 'Contact,Vendor';

    BEGIN
    END.
  }
}

