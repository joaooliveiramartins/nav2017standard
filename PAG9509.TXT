OBJECT Page 9509 Debugger Break Rules
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Debugger Break Rules;
               PTG="Regras Paragem Debugger "];
    PageType=StandardDialog;
  }
  CONTROLS
  {
    { 1   ;    ;Container ;
                Name=Content;
                ContainerType=ContentArea }

    { 2   ;1   ;Group     ;
                CaptionML=[ENU=General;
                           PTG=Geral];
                GroupType=Group }

    { 3   ;2   ;Field     ;
                CaptionML=[ENU=Break On Error;
                           PTG=Parar no Erro];
                ApplicationArea=#All;
                SourceExpr=BreakOnError }

    { 6   ;2   ;Field     ;
                CaptionML=[ENU=Break On Record Changes;
                           PTG=Altera��es Parar no Registo];
                ApplicationArea=#All;
                SourceExpr=BreakOnRecordChanges }

    { 4   ;2   ;Field     ;
                CaptionML=[ENU=Skip Codeunit 1;
                           PTG=Ignorar Codeunit 1];
                ApplicationArea=#All;
                SourceExpr=SkipCodeunit1 }

  }
  CODE
  {
    VAR
      BreakOnError@1000 : Boolean;
      BreakOnRecordChanges@1001 : Boolean;
      SkipCodeunit1@1002 : Boolean;

    PROCEDURE SetBreakOnError@51(Value@1000 : Boolean);
    BEGIN
      BreakOnError := Value;
    END;

    PROCEDURE GetBreakOnError@1() : Boolean;
    BEGIN
      EXIT(BreakOnError);
    END;

    PROCEDURE SetBreakOnRecordChanges@52(Value@1000 : Boolean);
    BEGIN
      BreakOnRecordChanges := Value;
    END;

    PROCEDURE GetBreakOnRecordChanges@2() : Boolean;
    BEGIN
      EXIT(BreakOnRecordChanges);
    END;

    PROCEDURE SetSkipCodeunit1@3(Value@1000 : Boolean);
    BEGIN
      SkipCodeunit1 := Value;
    END;

    PROCEDURE GetSkipCodeunit1@4() : Boolean;
    BEGIN
      EXIT(SkipCodeunit1);
    END;

    BEGIN
    END.
  }
}

