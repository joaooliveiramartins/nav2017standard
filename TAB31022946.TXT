OBJECT Table 31022946 Cartera Report Selections
{
  OBJECT-PROPERTIES
  {
    Date=01/02/15;
    Time=13:00:00;
    Version List=NAVPTSS81.00;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Cartera Report Selections;
               PTG=Selec�es Mapa Carteira];
  }
  FIELDS
  {
    { 1   ;   ;Usage               ;Option        ;CaptionML=[ENU=Usage;
                                                              PTG=Utiliza��o];
                                                   OptionCaptionML=[ENU=Bill Group,Posted Bill Group,Closed Bill Group,Bill,Bill Group - Test,Payment Order,Posted Payment Order,Payment Order - Test,Closed Payment Order;
                                                                    PTG=Remessa,Remessa Reg.,Remessa Fechada,T�tulo,Remessa Teste,Ordem Pagto.,Ordem Pagto. Regis.,Ordem Pagto. Teste,Ordem Pagto. Fechada];
                                                   OptionString=Bill Group,Posted Bill Group,Closed Bill Group,Bill,Bill Group - Test,Payment Order,Posted Payment Order,Payment Order - Test,Closed Payment Order }
    { 2   ;   ;Sequence            ;Code10        ;CaptionML=[ENU=Sequence;
                                                              PTG=Sequ�ncia];
                                                   Numeric=Yes }
    { 3   ;   ;Report ID           ;Integer       ;TableRelation=Object.ID WHERE (Type=CONST(Report));
                                                   OnValidate=BEGIN
                                                                CALCFIELDS("Report Name");
                                                              END;

                                                   CaptionML=[ENU=Report ID;
                                                              PTG=N� Mapa] }
    { 4   ;   ;Report Name         ;Text30        ;FieldClass=FlowField;
                                                   CalcFormula=Lookup(Object.Name WHERE (Type=CONST(Report),
                                                                                         ID=FIELD(Report ID)));
                                                   CaptionML=[ENU=Report Name;
                                                              PTG=Nome Mapa];
                                                   Editable=No }
  }
  KEYS
  {
    {    ;Usage,Sequence                          ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {
    VAR
      CarteraReportSelection2@1110000 : Record 31022946;

    PROCEDURE NewRecord@1();
    BEGIN
      CarteraReportSelection2.SETRANGE(Usage,Usage);
      IF CarteraReportSelection2.FINDLAST AND (CarteraReportSelection2.Sequence <> '') THEN
        Sequence := INCSTR(CarteraReportSelection2.Sequence)
      ELSE
        Sequence := '1';
    END;

    BEGIN
    END.
  }
}

