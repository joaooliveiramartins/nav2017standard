OBJECT Page 99000920 Registered Absences
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Registered Absences;
               PTG=Aus�ncias Registadas];
    SourceTable=Table99000848;
    DelayedInsert=Yes;
    PageType=List;
    ActionList=ACTIONS
    {
      { 1900000004;0 ;ActionContainer;
                      ActionContainerType=ActionItems }
      { 1900173204;1 ;Action    ;
                      CaptionML=[ENU=Implement Registered Absence;
                                 PTG=Implementar Aus�ncia Registada];
                      RunObject=Report 99003801;
                      Promoted=Yes;
                      Image=ImplementRegAbsence;
                      PromotedCategory=Process }
      { 1904735304;1 ;Action    ;
                      CaptionML=[ENU=Reg. Abs. (from Machine Ctr.);
                                 PTG=Registar Aus�ncia (do Centro M�quina)];
                      RunObject=Report 99003800;
                      Promoted=Yes;
                      Image=CalendarMachine;
                      PromotedCategory=Process }
      { 1901636904;1 ;Action    ;
                      CaptionML=[ENU=Reg. Abs. (from Work Ctr.);
                                 PTG=Registar Aus�ncia (do Centro Trabalho)];
                      RunObject=Report 99003805;
                      Promoted=Yes;
                      Image=CalendarWorkcenter;
                      PromotedCategory=Process }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 2   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies if the absence entry is related to a machine center or a work center.;
                           PTG=Especifica se o movimento de aus�ncia est  relacionado com um centro m quina ou com um centro de trabalho.];
                SourceExpr="Capacity Type" }

    { 4   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the number of the machine center or work center, depending on the entry in the Capacity Type field.;
                           PTG=Especifica o n�mero do centro m quina ou do centro de trabalho, dependendo do movimentos no campo Tipo de Capacidade.];
                SourceExpr="No." }

    { 6   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the date of the absence. If the absence covers several days, there will be an entry line for each day.;
                           PTG=Especifica a data da aus�ncia. Se a aus�ncia for de v rios dias, vai existir um linha de movimento para cada dia.];
                SourceExpr=Date }

    { 16  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies a short description of the reason for the absence.;
                           PTG=Especifica uma descri��o reduzida da raz�o da aus�ncia.];
                SourceExpr=Description }

    { 12  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the date and the starting time, which are combined in a format called "starting date-time".;
                           PTG=Especifica a data e hora iniciais, que est�o combinadas no formato "data-hora inicial".];
                SourceExpr="Starting Date-Time" }

    { 8   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the starting time of the absence, such as the time the employee normally starts to work or the time the machine starts to operate.;
                           PTG=Especifica a hora inicial da aus�ncia, tal como a hora em que o empregado come�a a trabalhar normalmente, ou a hora em que a m quina come�a a operar.];
                SourceExpr="Starting Time";
                Visible=FALSE }

    { 21  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the date and the ending time, which are combined in a format called "ending date-time".;
                           PTG=Especifica a data e hora finais, que est�o combinadas no formato "data-hora final".];
                SourceExpr="Ending Date-Time";
                Visible=FALSE }

    { 10  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the ending time of day of the absence, such as the time the employee normally leaves, or the time the machine stops operating.;
                           PTG=Especifica a hora final do dia da aus�ncia, tal como a hora em que o empregado sai normalmente, ou a hora em que a m quina p ra de operar.];
                SourceExpr="Ending Time" }

    { 14  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the amount of capacity, which cannot be used during the absence period.;
                           PTG=Especifica o valor da capacidade, que n�o pode ser usado durante o per�do de aus�ncia.];
                SourceExpr=Capacity }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {

    BEGIN
    END.
  }
}

