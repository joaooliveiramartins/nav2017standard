OBJECT Page 9323 Simulated Production Orders
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0;
  }
  PROPERTIES
  {
    Editable=No;
    CaptionML=[ENU=Simulated Production Orders;
               PTG=Ordens Produ��o Simuladas];
    SourceTable=Table5405;
    SourceTableView=WHERE(Status=CONST(Simulated));
    PageType=List;
    CardPageID=Simulated Production Order;
    ActionList=ACTIONS
    {
      { 1900000003;0 ;ActionContainer;
                      ActionContainerType=RelatedInformation }
      { 10      ;1   ;ActionGroup;
                      CaptionML=[ENU=Pro&d. Order;
                                 PTG=Ordem Pro&du��p];
                      Image=Order }
      { 12      ;2   ;ActionGroup;
                      CaptionML=[ENU=E&ntries;
                                 PTG=Movime&ntos];
                      Image=Entries }
      { 13      ;3   ;Action    ;
                      ShortCutKey=Ctrl+F7;
                      CaptionML=[ENU=Item Ledger E&ntries;
                                 PTG=&Movs. Produto];
                      RunObject=Page 38;
                      RunPageView=SORTING(Order Type,Order No.);
                      RunPageLink=Order Type=CONST(Production),
                                  Order No.=FIELD(No.);
                      Image=ItemLedger }
      { 27      ;3   ;Action    ;
                      CaptionML=[ENU=Capacity Ledger Entries;
                                 PTG=Movs. Capacidade];
                      RunObject=Page 5832;
                      RunPageView=SORTING(Order Type,Order No.);
                      RunPageLink=Order Type=CONST(Production),
                                  Order No.=FIELD(No.);
                      Image=CapacityLedger }
      { 28      ;3   ;Action    ;
                      CaptionML=[ENU=Value Entries;
                                 PTG=Movs. Valor];
                      RunObject=Page 5802;
                      RunPageView=SORTING(Order Type,Order No.);
                      RunPageLink=Order Type=CONST(Production),
                                  Order No.=FIELD(No.);
                      Image=ValueLedger }
      { 33      ;3   ;Action    ;
                      CaptionML=[ENU=&Warehouse Entries;
                                 PTG=Movs. &Armaz�m];
                      RunObject=Page 7318;
                      RunPageView=SORTING(Source Type,Source Subtype,Source No.);
                      RunPageLink=Source Type=FILTER(83|5407),
                                  Source Subtype=FILTER(3|4|5),
                                  Source No.=FIELD(No.);
                      Image=BinLedger }
      { 29      ;2   ;Action    ;
                      CaptionML=[ENU=Co&mments;
                                 PTG=Co&ment�rios];
                      RunObject=Page 99000838;
                      RunPageLink=Status=FIELD(Status),
                                  Prod. Order No.=FIELD(No.);
                      Image=ViewComments }
      { 30      ;2   ;Action    ;
                      AccessByPermission=TableData 348=R;
                      ShortCutKey=Shift+Ctrl+D;
                      CaptionML=[ENU=Dimensions;
                                 PTG=Dimens�es];
                      ToolTipML=[ENU=View or edit dimensions, such as area, project, or department, that you can assign to sales and purchase documents to distribute costs and analyze transaction history.;
                                 PTG=""];
                      Image=Dimensions;
                      OnAction=BEGIN
                                 ShowDocDim;
                                 CurrPage.SAVERECORD;
                               END;
                                }
      { 31      ;2   ;Separator  }
      { 32      ;2   ;Action    ;
                      ShortCutKey=F7;
                      CaptionML=[ENU=Statistics;
                                 PTG=Estat�sticas];
                      RunObject=Page 99000816;
                      RunPageLink=Status=FIELD(Status),
                                  No.=FIELD(No.),
                                  Date Filter=FIELD(Date Filter);
                      Promoted=Yes;
                      Image=Statistics;
                      PromotedCategory=Process }
      { 1900000004;0 ;ActionContainer;
                      ActionContainerType=ActionItems }
      { 150     ;1   ;ActionGroup;
                      CaptionML=[ENU=F&unctions;
                                 PTG=F&un��es];
                      Image=Action }
      { 152     ;2   ;Action    ;
                      CaptionML=[ENU=Change &Status;
                                 PTG=Alterar &Estado];
                      RunObject=Codeunit 5407;
                      Promoted=Yes;
                      Image=ChangeStatus;
                      PromotedCategory=Process }
      { 151     ;2   ;Action    ;
                      Ellipsis=Yes;
                      CaptionML=[ENU=&Update Unit Cost;
                                 PTG=&Atualizar Custo Unit�rio];
                      Promoted=Yes;
                      Image=UpdateUnitCost;
                      PromotedCategory=Process;
                      OnAction=VAR
                                 ProdOrder@1001 : Record 5405;
                               BEGIN
                                 ProdOrder.SETRANGE(Status,Status);
                                 ProdOrder.SETRANGE("No.","No.");

                                 REPORT.RUNMODAL(REPORT::"Update Unit Cost",TRUE,TRUE,ProdOrder);
                               END;
                                }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 2   ;2   ;Field     ;
                Lookup=No;
                ToolTipML=[ENU=Specifies the number of the production order.;
                           PTG=""];
                SourceExpr="No." }

    { 4   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the description of the production order.;
                           PTG=""];
                SourceExpr=Description }

    { 6   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the source number of the production order.;
                           PTG=""];
                SourceExpr="Source No." }

    { 8   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the routing number used for this production order.;
                           PTG=""];
                SourceExpr="Routing No." }

    { 45  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies how many units of the item or the family to produce (production quantity).;
                           PTG=""];
                SourceExpr=Quantity }

    { 49  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the code for the dimension associated with the production order.;
                           PTG=""];
                SourceExpr="Shortcut Dimension 1 Code";
                Visible=FALSE }

    { 51  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the code for the dimension associated with the production order.;
                           PTG=""];
                SourceExpr="Shortcut Dimension 2 Code";
                Visible=FALSE }

    { 53  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the location code to which you want to post the finished product from this production order.;
                           PTG=""];
                SourceExpr="Location Code";
                Visible=FALSE }

    { 14  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the starting time of the production order.;
                           PTG=""];
                SourceExpr="Starting Time";
                Visible=FALSE }

    { 16  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the starting date of the production order.;
                           PTG=""];
                SourceExpr="Starting Date" }

    { 18  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the ending time of the production order.;
                           PTG=""];
                SourceExpr="Ending Time";
                Visible=FALSE }

    { 20  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the ending date of the production order.;
                           PTG=""];
                SourceExpr="Ending Date" }

    { 22  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the due date of the production order.;
                           PTG=""];
                SourceExpr="Due Date" }

    { 34  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the ID of the user who is responsible for the document.;
                           PTG=""];
                SourceExpr="Assigned User ID" }

    { 41  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the actual finishing date of a finished production order.;
                           PTG=""];
                SourceExpr="Finished Date";
                Visible=FALSE }

    { 43  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the status of the production order.;
                           PTG=""];
                SourceExpr=Status }

    { 65  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the search description.;
                           PTG=""];
                SourceExpr="Search Description" }

    { 1102601000;2;Field  ;
                ToolTipML=[ENU=Specifies when the production order card was last modified.;
                           PTG=""];
                SourceExpr="Last Date Modified";
                Visible=FALSE }

    { 1102601002;2;Field  ;
                ToolTipML=[ENU=Specifies a bin to which you want to post the finished items.;
                           PTG=""];
                SourceExpr="Bin Code";
                Visible=FALSE }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                Visible=TRUE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {

    BEGIN
    END.
  }
}

