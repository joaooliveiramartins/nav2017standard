OBJECT Page 374 Check Ledger Entries
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0;
  }
  PROPERTIES
  {
    Editable=No;
    CaptionML=[ENU=Check Ledger Entries;
               PTG=Movs. Cheque];
    InsertAllowed=No;
    DeleteAllowed=No;
    SourceTable=Table272;
    SourceTableView=SORTING(Bank Account No.,Check Date)
                    ORDER(Descending);
    DataCaptionFields=Bank Account No.;
    PageType=List;
    OnOpenPage=BEGIN
                 IF FINDFIRST THEN;
               END;

    ActionList=ACTIONS
    {
      { 1900000003;0 ;ActionContainer;
                      ActionContainerType=RelatedInformation }
      { 34      ;1   ;ActionGroup;
                      CaptionML=[ENU=Chec&k;
                                 PTG=C&heque];
                      Image=Check }
      { 36      ;2   ;Action    ;
                      CaptionML=[ENU=Void Check;
                                 PTG=Anular Cheque];
                      ApplicationArea=#Basic,#Suite;
                      Image=VoidCheck;
                      OnAction=VAR
                                 CheckManagement@1001 : Codeunit 367;
                               BEGIN
                                 CheckManagement.FinancialVoidCheck(Rec);
                               END;
                                }
      { 1900000004;0 ;ActionContainer;
                      ActionContainerType=ActionItems }
      { 33      ;1   ;Action    ;
                      CaptionML=[ENU=&Navigate;
                                 PTG=&Navegar];
                      ApplicationArea=#Basic,#Suite;
                      Promoted=Yes;
                      Image=Navigate;
                      PromotedCategory=Process;
                      OnAction=BEGIN
                                 Navigate.SetDoc("Posting Date","Document No.");
                                 Navigate.RUN;
                               END;
                                }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 29  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the check date if a check is printed.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Check Date" }

    { 4   ;2   ;Field     ;
                ToolTipML=ENU=Specifies the check number if a check is printed.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Check No." }

    { 8   ;2   ;Field     ;
                ToolTipML=ENU=Specifies the number of the bank account used for the check ledger entry.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Bank Account No." }

    { 12  ;2   ;Field     ;
                ToolTipML=ENU=Specifies a printing description for the check ledger entry.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr=Description }

    { 10  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the amount on the check ledger entry.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr=Amount }

    { 16  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the type of balancing account used in the entry.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Bal. Account Type";
                Visible=FALSE }

    { 18  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the number of the balancing account used in the entry.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Bal. Account No.";
                Visible=FALSE }

    { 14  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the printing (and posting) status of the check ledger entry.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Entry Status" }

    { 27  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the status of the entry before you changed it.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Original Entry Status";
                Visible=FALSE }

    { 6   ;2   ;Field     ;
                ToolTipML=ENU=Specifies the payment type that applies to the entry.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Bank Payment Type";
                Visible=FALSE }

    { 2   ;2   ;Field     ;
                ToolTipML=ENU=Specifies the posting date of the check ledger entry.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Posting Date";
                Visible=FALSE }

    { 23  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the document type linked to the check ledger entry. For example, Payment.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Document Type";
                Visible=FALSE }

    { 25  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the document number on the check ledger entry.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Document No.";
                Visible=FALSE }

    { 31  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the entry number assigned the check ledger entry.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Entry No." }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {
    VAR
      Navigate@1002 : Page 344;

    BEGIN
    END.
  }
}

