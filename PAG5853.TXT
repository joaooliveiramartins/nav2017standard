OBJECT Page 5853 Get Pst.Doc-RtrnRcptLn Subform
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0;
  }
  PROPERTIES
  {
    Editable=No;
    CaptionML=[ENU=Lines;
               PTG=Linhas];
    LinksAllowed=No;
    SourceTable=Table6661;
    PageType=ListPart;
    OnAfterGetRecord=BEGIN
                       DocumentNoHideValue := FALSE;
                       DocumentNoOnFormat;
                     END;

    ActionList=ACTIONS
    {
      { 1900000004;0 ;ActionContainer;
                      ActionContainerType=ActionItems }
      { 1907935204;1 ;ActionGroup;
                      CaptionML=[ENU=&Line;
                                 PTG=&Linha];
                      Image=Line }
      { 1901742204;2 ;Action    ;
                      ShortCutKey=Shift+F7;
                      CaptionML=[ENU=Show Document;
                                 PTG=Mostrar Documento];
                      ToolTipML=[ENU=Open the document that the selected line exists on.;
                                 PTG=""];
                      Image=View;
                      OnAction=BEGIN
                                 ShowDocument;
                               END;
                                }
      { 1903866904;2 ;Action    ;
                      AccessByPermission=TableData 348=R;
                      ShortCutKey=Shift+Ctrl+D;
                      CaptionML=[ENU=Dimensions;
                                 PTG=Dimens�es];
                      ToolTipML=[ENU=View or edit dimensions, such as area, project, or department, that you can assign to sales and purchase documents to distribute costs and analyze transaction history.;
                                 PTG=""];
                      Image=Dimensions;
                      OnAction=BEGIN
                                 ShowDimensions;
                               END;
                                }
      { 1901313304;2 ;Action    ;
                      ShortCutKey=Shift+Ctrl+I;
                      CaptionML=[ENU=Item &Tracking Lines;
                                 PTG=Linhas Ras&treio Produto];
                      ToolTipML=[ENU=View or edit serial numbers and lot numbers that are assigned to the item on the document or journal line.;
                                 PTG=""];
                      Image=ItemTrackingLines;
                      OnAction=BEGIN
                                 ItemTrackingLines;
                               END;
                                }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 2   ;2   ;Field     ;
                Lookup=No;
                ToolTipML=[ENU=Specifies the number of the return receipt.;
                           PTG=""];
                SourceExpr="Document No.";
                HideValue=DocumentNoHideValue;
                StyleExpr='Strong' }

    { 40  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the date on which the item(s) on the sales return are received.;
                           PTG=""];
                SourceExpr="Shipment Date" }

    { 50  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the number of the customer you sent the original invoice to.;
                           PTG=""];
                SourceExpr="Bill-to Customer No.";
                Visible=FALSE }

    { 4   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the number of the customer who returned the items to you.;
                           PTG=""];
                SourceExpr="Sell-to Customer No.";
                Visible=FALSE }

    { 6   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the line type.;
                           PTG=""];
                SourceExpr=Type }

    { 8   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the number of the return receipt line.;
                           PTG=""];
                SourceExpr="No." }

    { 28  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the cross-reference number related to the item.;
                           PTG=""];
                SourceExpr="Cross-Reference No.";
                Visible=FALSE }

    { 10  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the variant number of the returned items.;
                           PTG=""];
                SourceExpr="Variant Code";
                Visible=FALSE }

    { 43  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies that this item is a nonstock item.;
                           PTG=""];
                SourceExpr=Nonstock;
                Visible=FALSE }

    { 12  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies either the name of, or the description of, the item, general ledger account, or item charge.;
                           PTG=""];
                SourceExpr=Description }

    { 53  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies a code that explains why the item is returned.;
                           PTG=""];
                SourceExpr="Return Reason Code";
                Visible=FALSE }

    { 31  ;2   ;Field     ;
                Lookup=No;
                DrillDown=No;
                ToolTipML=[ENU=Specifies the currency code for the amount on this line.;
                           PTG=""];
                SourceExpr="Currency Code";
                Visible=FALSE }

    { 18  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the location in which the return receipt line was registered.;
                           PTG=""];
                SourceExpr="Location Code";
                Visible=FALSE }

    { 20  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the code of the bin from which the items were sold.;
                           PTG=""];
                SourceExpr="Bin Code";
                Visible=FALSE }

    { 14  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the dimension value code for the dimension that has been chosen as Global Dimension 1.;
                           PTG=""];
                SourceExpr="Shortcut Dimension 1 Code";
                Visible=FALSE }

    { 16  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the dimension value code for the dimension that has been chosen as Global Dimension 2.;
                           PTG=""];
                SourceExpr="Shortcut Dimension 2 Code";
                Visible=FALSE }

    { 24  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the unit of measure code for the items sold.;
                           PTG=""];
                SourceExpr="Unit of Measure Code" }

    { 22  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the number of units of the item, general ledger account, or item charge specified on the line.;
                           PTG=""];
                SourceExpr=Quantity }

    { 36  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the quantity from the line that has been posted as received but that has not yet been posted as invoiced.;
                           PTG=""];
                SourceExpr="Return Qty. Rcd. Not Invd." }

    { 26  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the unit of measure used for the item (bottle or piece, for example).;
                           PTG=""];
                SourceExpr="Unit of Measure";
                Visible=FALSE }

    { 47  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies your cost in local currency of one unit of the item.;
                           PTG=""];
                SourceExpr="Unit Cost (LCY)";
                Visible=FALSE }

    { 34  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the job number corresponding to the sales invoice or credit memo.;
                           PTG=""];
                SourceExpr="Job No.";
                Visible=FALSE }

    { 60  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the number of the blanket order that the return receipt originates from.;
                           PTG=""];
                SourceExpr="Blanket Order No.";
                Visible=FALSE }

    { 62  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the number of the blanket order line that the sales credit memo line originates from.;
                           PTG=""];
                SourceExpr="Blanket Order Line No.";
                Visible=FALSE }

    { 64  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the number of the item ledger entry that the return receipt line is applied from.;
                           PTG=""];
                SourceExpr="Appl.-from Item Entry";
                Visible=FALSE }

    { 32  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the number of the item ledger entry that the items were applied to when the return receipt was posted.;
                           PTG=""];
                SourceExpr="Appl.-to Item Entry";
                Visible=FALSE }

  }
  CODE
  {
    VAR
      ReturnRcptLine@1001 : Record 6661;
      TempReturnRcptLine@1002 : TEMPORARY Record 6661;
      DocumentNoHideValue@19020538 : Boolean INDATASET;

    LOCAL PROCEDURE IsFirstDocLine@2() : Boolean;
    BEGIN
      TempReturnRcptLine.RESET;
      TempReturnRcptLine.COPYFILTERS(Rec);
      TempReturnRcptLine.SETRANGE("Document No.","Document No.");
      IF NOT TempReturnRcptLine.FINDFIRST THEN BEGIN
        ReturnRcptLine.COPYFILTERS(Rec);
        ReturnRcptLine.SETRANGE("Document No.","Document No.");
        IF NOT ReturnRcptLine.FINDFIRST THEN
          EXIT(FALSE);
        TempReturnRcptLine := ReturnRcptLine;
        TempReturnRcptLine.INSERT;
      END;

      EXIT("Line No." = TempReturnRcptLine."Line No.");
    END;

    PROCEDURE GetSelectedLine@1(VAR FromReturnRcptLine@1000 : Record 6661);
    BEGIN
      FromReturnRcptLine.COPY(Rec);
      CurrPage.SETSELECTIONFILTER(FromReturnRcptLine);
    END;

    LOCAL PROCEDURE ShowDocument@7();
    VAR
      ReturnRcptHeader@1000 : Record 6660;
    BEGIN
      IF NOT ReturnRcptHeader.GET("Document No.") THEN
        EXIT;
      PAGE.RUN(PAGE::"Posted Return Receipt",ReturnRcptHeader);
    END;

    LOCAL PROCEDURE ItemTrackingLines@12();
    VAR
      FromReturnRcptLine@1000 : Record 6661;
    BEGIN
      GetSelectedLine(FromReturnRcptLine);
      FromReturnRcptLine.ShowItemTrackingLines;
    END;

    LOCAL PROCEDURE DocumentNoOnFormat@19001080();
    BEGIN
      IF NOT IsFirstDocLine THEN
        DocumentNoHideValue := TRUE;
    END;

    BEGIN
    END.
  }
}

