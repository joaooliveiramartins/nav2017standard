OBJECT Table 2000000074 Profile Metadata
{
  OBJECT-PROPERTIES
  {
    Date=13/04/17;
    Time=14:36:26;
    Version List=NAVW110.0;
  }
  PROPERTIES
  {
    DataPerCompany=No;
    CaptionML=[ENU=Profile Metadata;
               PTG=Metadados Perfil];
  }
  FIELDS
  {
    { 3   ;   ;Profile ID          ;Code30        ;TableRelation=Profile."Profile ID";
                                                   CaptionML=[ENU=Profile ID;
                                                              PTG=ID Perfil] }
    { 6   ;   ;Page ID             ;Integer       ;TableRelation=AllObjWithCaption."Object ID" WHERE (Object Type=CONST(Page));
                                                   CaptionML=[ENU=Page ID;
                                                              PTG=ID Page] }
    { 9   ;   ;Description         ;Text250       ;FieldClass=FlowField;
                                                   CalcFormula=Lookup(AllObjWithCaption."Object Caption" WHERE (Object Type=CONST(Page),
                                                                                                                Object ID=FIELD(Page ID)));
                                                   CaptionML=[ENU=Description;
                                                              PTG=Descri��o] }
    { 12  ;   ;Date                ;Date          ;CaptionML=[ENU=Date;
                                                              PTG=Data] }
    { 15  ;   ;Time                ;Time          ;CaptionML=[ENU=Time;
                                                              PTG=Hora] }
    { 18  ;   ;Personalization ID  ;Code40        ;CaptionML=[ENU=Personalization ID;
                                                              PTG=ID Personaliza��o] }
    { 21  ;   ;Page Metadata Delta ;BLOB          ;CaptionML=[ENU=Page Metadata Delta;
                                                              PTG=Delta Metadados Page] }
  }
  KEYS
  {
    {    ;Profile ID,Page ID,Personalization ID   ;Clustered=Yes }
    {    ;Date                                     }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    END.
  }
}

