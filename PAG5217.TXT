OBJECT Page 5217 Employment Contracts
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Employment Contracts;
               PTG=Contratos Trabalho];
    SourceTable=Table5211;
    PageType=List;
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 2   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies a code for the employment contract.;
                           PTG=""];
                SourceExpr=Code }

    { 4   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies a description for the employment contract.;
                           PTG=""];
                SourceExpr=Description }

    { 9   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the number of contracts associated with the entry.;
                           PTG=""];
                SourceExpr="No. of Contracts" }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {

    BEGIN
    END.
  }
}

