OBJECT Page 99000795 Prod. Order BOM Cmt. Sheet
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Comment Sheet;
               PTG=Folha Coment rios];
    MultipleNewLines=Yes;
    LinksAllowed=No;
    SourceTable=Table99000776;
    DataCaptionExpr=Caption;
    PageType=List;
    AutoSplitKey=Yes;
    OnNewRecord=BEGIN
                  SetUpNewLine;
                END;

  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 2   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the date when the comment was created.;
                           PTG=""];
                SourceExpr=Date }

    { 4   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the actual comment.;
                           PTG=""];
                SourceExpr=Comment }

    { 6   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies a code for the comments.;
                           PTG=""];
                SourceExpr=Code;
                Visible=FALSE }

  }
  CODE
  {

    BEGIN
    END.
  }
}

