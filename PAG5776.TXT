OBJECT Page 5776 Warehouse Comment Sheet
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Comment Sheet;
               PTG=Folha Coment rios];
    MultipleNewLines=Yes;
    LinksAllowed=No;
    SourceTable=Table5770;
    DataCaptionExpr=FormCaption;
    DelayedInsert=Yes;
    PageType=List;
    AutoSplitKey=Yes;
    OnNewRecord=BEGIN
                  SetUpNewLine;
                END;

  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 2   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the date when the comment was created.;
                           PTG=""];
                SourceExpr=Date }

    { 4   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the comment.;
                           PTG=""];
                SourceExpr=Comment }

    { 6   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the code for the comment.;
                           PTG=""];
                SourceExpr=Code;
                Visible=FALSE }

  }
  CODE
  {

    BEGIN
    END.
  }
}

