OBJECT Table 6721 Booking Mgr. Setup
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Booking Mgr. Setup;
               PTG=Reserva Config. Mgr.];
  }
  FIELDS
  {
    { 1   ;   ;Primary Key         ;Code10        ;CaptionML=[ENU=Primary Key;
                                                              PTG=Chave Prim ria] }
    { 2   ;   ;Booking Mgr. Codeunit;Integer      ;InitValue=6722;
                                                   CaptionML=[ENU=Booking Mgr. Codeunit;
                                                              PTG=Reserva Codeunit Mgr.] }
  }
  KEYS
  {
    {    ;Primary Key                             ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    END.
  }
}

