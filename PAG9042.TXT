OBJECT Page 9042 Team Member Activities
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Self-Service;
               PTG=Self-Service];
    SourceTable=Table9042;
    PageType=CardPart;
    RefreshOnActivate=Yes;
    ShowFilter=No;
    OnOpenPage=VAR
                 RoleCenterNotificationMgt@1000 : Codeunit 1430;
               BEGIN
                 RESET;
                 IF NOT GET THEN BEGIN
                   INIT;
                   INSERT;
                 END;

                 SETRANGE("User ID Filter",USERID);

                 ShowOpenTimeSheets := TRUE;
                 ShowRequestToApprove := TRUE;
                 RoleCenterNotificationMgt.ShowNotifications;
               END;

    OnAfterGetRecord=BEGIN
                       SetActivityGroupVisibility;
                     END;

    ActionList=ACTIONS
    {
      { 11      ;    ;ActionContainer;
                      ActionContainerType=ActionItems }
      { 10      ;1   ;ActionGroup;
                      Name=Show/Hide Activities;
                      CaptionML=[ENU=Show/Hide Activities;
                                 PTG=Mostrar/Ocultar Atividades];
                      Image=Answers }
      { 9       ;2   ;Action    ;
                      Name=Time Sheets;
                      CaptionML=[ENU=Show/Hide Time Sheets;
                                 PTG=Mostrar/Ocultar Folhas de Tempo];
                      ToolTipML=[ENU=View open time sheets.;
                                 PTG=""];
                      ApplicationArea=#Basic,#Suite;
                      RunObject=Codeunit 1340;
                      Image=Timesheet }
      { 13      ;2   ;Action    ;
                      Name=Pending Time Sheets;
                      CaptionML=[ENU=Show/Hide Pending Time Sheets;
                                 PTG=Mostrar/Esconder Folhas de Tempo];
                      ToolTipML=[ENU=View submitted, rejected, and approved time sheets.;
                                 PTG=""];
                      ApplicationArea=#Basic,#Suite;
                      RunObject=Codeunit 1342;
                      Image=Timesheet }
      { 8       ;2   ;Action    ;
                      Name=Approvals;
                      CaptionML=[ENU=Show/Hide Approvals;
                                 PTG=Mostrar/Esconder Aprova��es];
                      ToolTipML=[ENU=Specifies the number of approval requests that require your approval.;
                                 PTG=""];
                      ApplicationArea=#Basic,#Suite;
                      RunObject=Codeunit 1341;
                      Image=Approve }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 7   ;1   ;Group     ;
                CaptionML=[ENU=Time Sheets;
                           PTG=Folhas de Tempo];
                Visible=ShowOpenTimeSheets;
                GroupType=CueGroup }

    { 1   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the number of time sheets that are currently assigned to you and not submitted for approval.;
                           PTG=""];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Open Time Sheets";
                DrillDownPageID=Time Sheet List }

    { 12  ;1   ;Group     ;
                CaptionML=[ENU=Pending Time Sheets;
                           PTG=Folhas de Tempo Pendentes];
                Visible=ShowPendingTimeSheets;
                GroupType=CueGroup }

    { 2   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the number of time sheets that you have submitted for approval but are not yet approved.;
                           PTG=""];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Submitted Time Sheets";
                DrillDownPageID=Time Sheet List }

    { 3   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the number of time sheets that you submitted for approval but were rejected.;
                           PTG=""];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Rejected Time Sheets";
                DrillDownPageID=Time Sheet List }

    { 5   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the number of time sheets that have been approved.;
                           PTG=""];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Approved Time Sheets";
                DrillDownPageID=Time Sheet List }

    { 4   ;1   ;Group     ;
                CaptionML=[ENU=Approvals;
                           PTG=Aprova��es];
                Visible=ShowRequestToApprove;
                GroupType=CueGroup }

    { 6   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies request for certain documents, cards, or journal lines that you must approve for other users before they can proceed.;
                           PTG=""];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Requests to Approve";
                DrillDownPageID=Requests to Approve }

  }
  CODE
  {
    VAR
      ShowOpenTimeSheets@1000 : Boolean;
      ShowRequestToApprove@1001 : Boolean;
      ShowPendingTimeSheets@1002 : Boolean;

    LOCAL PROCEDURE SetActivityGroupVisibility@12();
    VAR
      TeamMemberActivitiesMgt@1000 : Codeunit 1340;
      ReqToApprActivitiesMgt@1001 : Codeunit 1341;
      TimeSheetActivitesMgt@1002 : Codeunit 1342;
    BEGIN
      ShowOpenTimeSheets := TeamMemberActivitiesMgt.IsActivitiesVisible;
      ShowPendingTimeSheets := TimeSheetActivitesMgt.IsActivitiesVisible;
      ShowRequestToApprove := ReqToApprActivitiesMgt.IsActivitiesVisible;
    END;

    BEGIN
    END.
  }
}

