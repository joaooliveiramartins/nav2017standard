OBJECT Table 957 Time Sheet Cmt. Line Archive
{
  OBJECT-PROPERTIES
  {
    Date=07/09/12;
    Time=12:00:00;
    Version List=NAVW17.00;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Time Sheet Cmt. Line Archive;
               PTG=Hist. Linha Coment. Folha Horas];
  }
  FIELDS
  {
    { 1   ;   ;No.                 ;Code20        ;CaptionML=[ENU=No.;
                                                              PTG=N�] }
    { 2   ;   ;Time Sheet Line No. ;Integer       ;CaptionML=[ENU=Time Sheet Line No.;
                                                              PTG=N� Linha Folha Horas] }
    { 3   ;   ;Line No.            ;Integer       ;CaptionML=[ENU=Line No.;
                                                              PTG=N� Linha] }
    { 4   ;   ;Date                ;Date          ;CaptionML=[ENU=Date;
                                                              PTG=Data] }
    { 5   ;   ;Code                ;Code10        ;CaptionML=[ENU=Code;
                                                              PTG=C�digo] }
    { 6   ;   ;Comment             ;Text80        ;CaptionML=[ENU=Comment;
                                                              PTG=Coment�rio] }
  }
  KEYS
  {
    {    ;No.,Time Sheet Line No.,Line No.        ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    END.
  }
}

