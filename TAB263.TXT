OBJECT Table 263 Intrastat Jnl. Line
{
  OBJECT-PROPERTIES
  {
    Date=21/12/16;
    Time=13:00:00;
    Version List=NAVW19.00,NAVPTSS81.00;
  }
  PROPERTIES
  {
    OnInsert=BEGIN
               IntraJnlTemplate.GET("Journal Template Name");
               IntrastatJnlBatch.GET("Journal Template Name","Journal Batch Name");
             END;

    OnModify=BEGIN
               IntrastatJnlBatch.GET("Journal Template Name","Journal Batch Name");
               IntrastatJnlBatch.TESTFIELD(Reported,FALSE);
             END;

    OnRename=BEGIN
               IntrastatJnlBatch.GET(xRec."Journal Template Name",xRec."Journal Batch Name");
               IntrastatJnlBatch.TESTFIELD(Reported,FALSE);
             END;

    CaptionML=[ENU=Intrastat Jnl. Line;
               PTG=Linha Di�rio Intrastat];
  }
  FIELDS
  {
    { 1   ;   ;Journal Template Name;Code10       ;TableRelation="Intrastat Jnl. Template";
                                                   CaptionML=[ENU=Journal Template Name;
                                                              PTG=Nome Livro Di�rio] }
    { 2   ;   ;Journal Batch Name  ;Code10        ;TableRelation="Intrastat Jnl. Batch".Name WHERE (Journal Template Name=FIELD(Journal Template Name));
                                                   CaptionML=[ENU=Journal Batch Name;
                                                              PTG=Nome Sec��o Di�rio] }
    { 3   ;   ;Line No.            ;Integer       ;CaptionML=[ENU=Line No.;
                                                              PTG=N� Linha] }
    { 4   ;   ;Type                ;Option        ;CaptionML=[ENU=Type;
                                                              PTG=Tipo];
                                                   OptionCaptionML=[ENU=Receipt,Shipment;
                                                                    PTG=Introdu��o,Expedi��o];
                                                   OptionString=Receipt,Shipment }
    { 5   ;   ;Date                ;Date          ;CaptionML=[ENU=Date;
                                                              PTG=Data] }
    { 6   ;   ;Tariff No.          ;Code20        ;TableRelation="Tariff Number";
                                                   OnValidate=BEGIN
                                                                TESTFIELD("Item No.",'');
                                                                GetItemDescription;
                                                              END;

                                                   CaptionML=[ENU=Tariff No.;
                                                              PTG=C�d. Aduaneiro];
                                                   NotBlank=Yes }
    { 7   ;   ;Item Description    ;Text50        ;CaptionML=[ENU=Item Description;
                                                              PTG=Descri��o Produto] }
    { 8   ;   ;Country/Region Code ;Code10        ;TableRelation=Country/Region;
                                                   CaptionML=[ENU=Country/Region Code;
                                                              PTG=C�digo Pa�s/Regi�o] }
    { 9   ;   ;Transaction Type    ;Code10        ;TableRelation="Transaction Type";
                                                   CaptionML=[ENU=Transaction Type;
                                                              PTG=Natureza Transa��o] }
    { 10  ;   ;Transport Method    ;Code10        ;TableRelation="Transport Method";
                                                   CaptionML=[ENU=Transport Method;
                                                              PTG=M�todo Transporte] }
    { 11  ;   ;Source Type         ;Option        ;CaptionML=[ENU=Source Type;
                                                              PTG=Tipo Origem];
                                                   OptionCaptionML=[ENU=,Item entry,Job entry,,,,,,FA Entry;
                                                                    PTG=,Mov. produto,Mov. Projeto,,,,,Mov. Imob.];
                                                   OptionString=,Item entry,Job entry,,,,,,FA Entry;
                                                   BlankZero=Yes;
                                                   Description=soft }
    { 12  ;   ;Source Entry No.    ;Integer       ;TableRelation=IF (Source Type=CONST(Item entry)) "Item Ledger Entry"
                                                                 ELSE IF (Source Type=CONST(Job entry)) "Job Ledger Entry";
                                                   CaptionML=[ENU=Source Entry No.;
                                                              PTG=N� Mov. Origem];
                                                   Editable=No }
    { 13  ;   ;Net Weight          ;Decimal       ;OnValidate=BEGIN
                                                                IF Quantity <> 0 THEN
                                                                  "Total Weight" := ROUND("Net Weight" * Quantity,0.00001)
                                                                ELSE
                                                                  "Total Weight" := 0;
                                                              END;

                                                   CaptionML=[ENU=Net Weight;
                                                              PTG=Peso L�quido];
                                                   DecimalPlaces=2:5 }
    { 14  ;   ;Amount              ;Decimal       ;OnValidate=BEGIN
                                                                IF "Cost Regulation %" <> 0 THEN
                                                                  VALIDATE("Cost Regulation %")
                                                                ELSE
                                                                  "Statistical Value" := Amount + "Indirect Cost";
                                                              END;

                                                   CaptionML=[ENU=Amount;
                                                              PTG=Valor];
                                                   DecimalPlaces=0:0 }
    { 15  ;   ;Quantity            ;Decimal       ;OnValidate=BEGIN
                                                                IF (Quantity <> 0) AND Item.GET("Item No.") THEN
                                                                  VALIDATE("Net Weight",Item."Net Weight")
                                                                ELSE
                                                                  VALIDATE("Net Weight",0);
                                                              END;

                                                   CaptionML=[ENU=Quantity;
                                                              PTG=Quantidade];
                                                   DecimalPlaces=0:0 }
    { 16  ;   ;Cost Regulation %   ;Decimal       ;OnValidate=BEGIN
                                                                "Indirect Cost" := ROUND(Amount * "Cost Regulation %" / 100,1);
                                                                "Statistical Value" := ROUND(Amount + "Indirect Cost",1);
                                                              END;

                                                   CaptionML=[ENU=Cost Regulation %;
                                                              PTG=% Custo Territ�rio Nac.];
                                                   DecimalPlaces=2:2;
                                                   MinValue=-100;
                                                   MaxValue=100 }
    { 17  ;   ;Indirect Cost       ;Decimal       ;OnValidate=BEGIN
                                                                "Cost Regulation %" := 0;
                                                                "Statistical Value" := Amount + "Indirect Cost";
                                                              END;

                                                   CaptionML=[ENU=Indirect Cost;
                                                              PTG=Custo Indireto];
                                                   DecimalPlaces=0:0 }
    { 18  ;   ;Statistical Value   ;Decimal       ;CaptionML=[ENU=Statistical Value;
                                                              PTG=Valor Estat�stico];
                                                   DecimalPlaces=0:0;
                                                   Editable=No }
    { 19  ;   ;Document No.        ;Code20        ;CaptionML=[ENU=Document No.;
                                                              PTG=N� Documento] }
    { 20  ;   ;Item No.            ;Code20        ;TableRelation=Item;
                                                   OnValidate=BEGIN
                                                                TESTFIELD("Source Type",0);

                                                                IF "Item No." = '' THEN
                                                                  CLEAR(Item)
                                                                ELSE BEGIN
                                                                  Item.GET("Item No.");
                                                                  Item.TESTFIELD("Tariff No.");
                                                                END;

                                                                Name := Item.Description;
                                                                "Tariff No." := Item."Tariff No.";
                                                                "Country/Region of Origin Code" := Item."Country/Region of Origin Code";
                                                                GetItemDescription;
                                                              END;

                                                   CaptionML=[ENU=Item No.;
                                                              PTG=N� Produto] }
    { 21  ;   ;Name                ;Text50        ;CaptionML=[ENU=Name;
                                                              PTG=Nome] }
    { 22  ;   ;Total Weight        ;Decimal       ;CaptionML=[ENU=Total Weight;
                                                              PTG=Peso Total];
                                                   DecimalPlaces=0:0;
                                                   Editable=No }
    { 23  ;   ;Supplementary Units ;Boolean       ;CaptionML=[ENU=Supplementary Units;
                                                              PTG=Unidades Suplementares];
                                                   Editable=No }
    { 24  ;   ;Internal Ref. No.   ;Text10        ;CaptionML=[ENU=Internal Ref. No.;
                                                              PTG=N� Ref. Interno];
                                                   Editable=No }
    { 25  ;   ;Country/Region of Origin Code;Code10;
                                                   TableRelation=Country/Region;
                                                   CaptionML=[ENU=Country/Region of Origin Code;
                                                              PTG=C�d. Pa�s/Regi�o Origem] }
    { 26  ;   ;Entry/Exit Point    ;Code10        ;TableRelation="Entry/Exit Point";
                                                   CaptionML=[ENU=Entry/Exit Point;
                                                              PTG=Porto/Aeroporto] }
    { 27  ;   ;Area                ;Code10        ;TableRelation=Area;
                                                   CaptionML=[ENU=Area;
                                                              PTG=�rea] }
    { 28  ;   ;Transaction Specification;Code10   ;TableRelation="Transaction Specification";
                                                   CaptionML=[ENU=Transaction Specification;
                                                              PTG=Especifica��o Transa��o] }
    { 31022890;;Place of Receipt   ;Text25        ;CaptionML=[ENU=Place of Receipt;
                                                              PTG=Lugar de Entrega] }
    { 31022891;;Shipment Method Code #1;Code10    ;TableRelation="Shipment Method";
                                                   CaptionML=[ENU=Shipment Method Code #1;
                                                              PTG=C�d. Condi��es Envio #1] }
    { 31022892;;Shipment Method Code #2;Option    ;InitValue=1-Internal;
                                                   CaptionML=[ENU=Shipment Method Code #2;
                                                              PTG=C�d. Condi��es Envio #2];
                                                   OptionCaptionML=[ENU=" ,1-Internal,2-Another EU Country/Region,3-Others (outside EU)";
                                                                    PTG=" ,1-Interno,2-Outro Pa�s/Regi�o UE,3-Outros (fora da UE)"];
                                                   OptionString=[ ,1-Internal,2-Another EU Country/Region,3-Others (outside EU)] }
    { 31022893;;Statistical System ;Option        ;CaptionML=[ENU=Statistical System;
                                                              PTG=Regime Estat�stico];
                                                   OptionCaptionML=[ENU=" ,1-Final Destination,2-Temporary Destination,3-Temporary Destination+Transformation,4-Return,5-Return+Transformation";
                                                                    PTG=" ,1-Destino Final,2-Destino Tempor�rio,3-Destino Tempor�rio+Transforma��o,4-Devolu��o,5-Devolu��o+Transforma��o"];
                                                   OptionString=[ ,1-Final Destination,2-Temporary Destination,3-Temporary Destination+Transformation,4-Return,5-Return+Transformation] }
  }
  KEYS
  {
    {    ;Journal Template Name,Journal Batch Name,Line No.;
                                                   SumIndexFields=Statistical Value;
                                                   MaintainSIFTIndex=No;
                                                   Clustered=Yes }
    {    ;Source Type,Source Entry No.             }
    {    ;Type,Country/Region Code,Tariff No.,Transaction Type,Transport Method }
    {    ;Internal Ref. No.                        }
  }
  FIELDGROUPS
  {
  }
  CODE
  {
    VAR
      IntraJnlTemplate@1000 : Record 261;
      IntrastatJnlBatch@1001 : Record 262;
      Item@1002 : Record 27;
      TariffNumber@1003 : Record 260;

    LOCAL PROCEDURE GetItemDescription@1();
    BEGIN
      IF "Tariff No." <> '' THEN BEGIN
        TariffNumber.GET("Tariff No.");
        "Item Description" := TariffNumber.Description;
        "Supplementary Units" := TariffNumber."Supplementary Units";
      END ELSE
        "Item Description" := '';
    END;

    PROCEDURE IsOpenedFromBatch@42() : Boolean;
    VAR
      IntrastatJnlBatch@1002 : Record 262;
      TemplateFilter@1001 : Text;
      BatchFilter@1000 : Text;
    BEGIN
      BatchFilter := GETFILTER("Journal Batch Name");
      IF BatchFilter <> '' THEN BEGIN
        TemplateFilter := GETFILTER("Journal Template Name");
        IF TemplateFilter <> '' THEN
          IntrastatJnlBatch.SETFILTER("Journal Template Name",TemplateFilter);
        IntrastatJnlBatch.SETFILTER(Name,BatchFilter);
        IntrastatJnlBatch.FINDFIRST;
      END;

      EXIT((("Journal Batch Name" <> '') AND ("Journal Template Name" = '')) OR (BatchFilter <> ''));
    END;

    BEGIN
    END.
  }
}

