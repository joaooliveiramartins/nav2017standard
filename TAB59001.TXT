OBJECT Table 59001 Object Documentation
{
  OBJECT-PROPERTIES
  {
    Date=18/10/14;
    Time=17:33:37;
    Modified=Yes;
    Version List=DTT.JL;
  }
  PROPERTIES
  {
    DataPerCompany=No;
    OnInsert=VAR
               ObjectComment@1101970000 : Record 59001;
             BEGIN
               ObjectComment.RESET;
               ObjectComment.SETRANGE(Type,Type);
               ObjectComment.SETRANGE(ID,ID);
               IF ObjectComment.FINDLAST THEN
                 "Line No." := ObjectComment."Line No." + 1
               ELSE
                 "Line No." := 1;
             END;

    CaptionML=[ENU=Object Documentation;
               PTG=Documenta��o Objectos];
  }
  FIELDS
  {
    { 1   ;   ;Type                ;Option        ;CaptionML=ENU=Type;
                                                   OptionCaptionML=ENU=TableData,Table,Form,Report,Dataport,Codeunit,XMLport,MenuSuite,Page;
                                                   OptionString=TableData,Table,Form,Report,Dataport,Codeunit,XMLport,MenuSuite,Page;
                                                   Editable=No }
    { 2   ;   ;ID                  ;Integer       ;CaptionML=ENU=ID;
                                                   Editable=No }
    { 3   ;   ;Name                ;Text50        ;CaptionML=ENU=Name;
                                                   Editable=No }
    { 4   ;   ;Line No.            ;Integer       ;CaptionML=ENU=Line No.;
                                                   Editable=No }
    { 5   ;   ;Comment             ;Text250       ;CaptionML=ENU=Comment;
                                                   Editable=No }
    { 6   ;   ;Indentation         ;Integer        }
  }
  KEYS
  {
    {    ;Type,ID,Line No.                        ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {
    VAR
      Text001@1101970000 : TextConst 'ENU=Object Name #1############\;PTG=Nome Objecto #1############\';
      Text002@1101970001 : TextConst 'ENU=Object No.  #2############\;PTG=Objecto N�  #2############\';
      Text003@1101970002 : TextConst 'ENU=@3@@@@@@@@@@@@@@@@@@@@;PTG=@3@@@@@@@@@@@@@@@@@@@@';

    PROCEDURE UpdateComments@1101970000(VAR pObject@1101970005 : Record 2000000001);
    VAR
      ObjectComment@1101970001 : Record 59001;
      Window@1101970002 : Dialog;
      TotalObjects@1101970003 : Integer;
      Counter@1101970004 : Integer;
    BEGIN
      Window.OPEN(Text001 + Text002 + Text003);
      //TotalObjects := pObject.COUNTAPPROX;
      TotalObjects := pObject.COUNT;
      Counter := 0;
      IF pObject.FINDSET THEN REPEAT
        IF pObject.Type IN [pObject.Type::Table..pObject.Type::Page] THEN BEGIN
          Window.UPDATE(1,pObject.Name);
          Window.UPDATE(2,pObject.ID);
          Window.UPDATE(3,ROUND(Counter / TotalObjects * 10000, 1, '='));
          ObjectComment.SETRANGE(Type,pObject.Type);
          ObjectComment.SETRANGE(ID,pObject.ID);
          ObjectComment.DELETEALL;
          ProcessObject(pObject);
          Counter += 1;
        END;
      UNTIL pObject.NEXT = 0;
      Window.CLOSE;
    END;

    PROCEDURE ProcessObject@1000000000(pObject@1000000000 : Record 2000000001);
    VAR
      InStr@1101970000 : InStream;
    BEGIN
      pObject.CALCFIELDS("BLOB Reference");
      pObject."BLOB Reference".CREATEINSTREAM(InStr);
      ParseDocTrigger(pObject.ID,pObject.Type,InStr);
    END;

    PROCEDURE ParseDocTrigger@1000000003(pNo@1000000011 : Integer;pType@1000000012 : 'TableData,Table,Form,Report,Dataport,Codeunit,XMLport,MenuSuite,Page';pInStr@1101970000 : InStream);
    VAR
      DWORD@1000000006 : Text[4];
      DocLine@1000000004 : Text[1000];
      CodeFound@1000000009 : Boolean;
      IntType@1000000017 : Boolean;
      TextType@1000000018 : Boolean;
      TextChar@1000000019 : Text[1];
      i@1000000000 : Integer;
      NotText@1000000001 : Boolean;
      NotSkip@1101970001 : Boolean;
    BEGIN
      NotText := TRUE;
      IntType := FALSE;
      TextType := FALSE;
      DocLine := '';

      IF NOT MoveToDocBlock(pInStr,DWORD) THEN
        EXIT;

      WHILE TRUE DO
        BEGIN
          IF NotSkip THEN BEGIN
            IF NOT GetNextDWORD(DWORD,pInStr) THEN
              EXIT;
          END ELSE
            NotSkip := TRUE;
          CodeFound := FALSE;
          IF (DWORD[2] = 101) THEN
            CASE DWORD[1] OF
              23:
                IF IntType OR TextType THEN
                  BEGIN
                    InsertDocLine(pNo,pType,DocLine);
                    DocLine := '';
                    InsertDocLine(pNo,pType,DocLine);
                    EXIT;
                  END;
              24:
                BEGIN
                  InsertDocLine(pNo,pType,DocLine);
                  DocLine := '';
                  CodeFound := TRUE;
                  NotText := TRUE;
                END;
              115:
                BEGIN
                  IntType := FALSE;
                  TextType := TRUE;
                  CodeFound := TRUE;
                  NotText := FALSE;
                END;
              116:
                IF NotText THEN
                  BEGIN
                    IntType := TRUE;
                    TextType := FALSE;
                    CodeFound := TRUE;
                    NotText := FALSE;
                  END;
            END;
          IF NOT CodeFound THEN
            FOR i := 1 TO 4 DO
              CASE DWORD[i] OF
                0,1:
                  BEGIN
                    IF (IntType AND (DocLine <> '') AND NOT CodeFound) THEN BEGIN
                      InsertDocLine(pNo,pType,DocLine);
                      DocLine := '';
                    END;
                    IF (DWORD[i] = 0) THEN
                      CodeFound := TRUE;
                  END ELSE BEGIN
                    IF (((IntType AND (DWORD[i] IN ['0'..'9'])) OR TextType) AND NOT CodeFound) THEN BEGIN
                      TextChar[1] := DWORD[i];
                      IF TextChar[1] < ' ' THEN
                        TextChar[1] := ' ';
                      DocLine := DocLine + TextChar;
                    END;
                  END;
              END;
        END;
    END;

    PROCEDURE MoveToDocBlock@1000000005(pInStr@1101970000 : InStream;VAR LastDWORD@1101970012 : Text[4]) : Boolean;
    VAR
      DWORD@1000000002 : Text[4];
      Found@1000000000 : Boolean;
    BEGIN
      Found := FALSE;
      WHILE TRUE DO
        BEGIN
          IF NOT Found THEN
            IF NOT GetNextDWORD(DWORD,pInStr) THEN
              EXIT(FALSE);

          IF ((DWORD[3] = 0) AND (DWORD[4] = 0)) OR
           ((DWORD[1] = 255) AND (DWORD[2] = 255) AND (DWORD[3] = 255) AND (DWORD[4] = 255))
          THEN BEGIN
            IF NOT GetNextDWORD(DWORD,pInStr) THEN
              EXIT(FALSE);
            IF ((DWORD[1] = 69) AND (DWORD[2] = 101)) THEN BEGIN
              IF NOT GetNextDWORD(DWORD,pInStr) THEN
                EXIT(FALSE);
              IF ((DWORD[1] = 12) AND (DWORD[2] = 0) AND (DWORD[3] = 0) AND (DWORD[4] = 0)) THEN BEGIN
                IF NOT GetNextDWORD(DWORD,pInStr) THEN
                  EXIT(FALSE);
                IF ((DWORD[1] = 23) AND (DWORD[2] = 101)) THEN BEGIN
                  IF NOT GetNextDWORD(DWORD,pInStr) THEN
                    EXIT(FALSE);
                  IF ((DWORD[1] = 123) AND (DWORD[2] = 101)) THEN BEGIN
                    IF NOT GetNextDWORD(DWORD,pInStr) THEN
                      EXIT(FALSE);
                    IF ((DWORD[1] = 0) AND (DWORD[2] = 0) AND (DWORD[3] = 0) AND (DWORD[4] = 0)) THEN   BEGIN
                      IF NOT GetNextDWORD(DWORD,pInStr) THEN
                        EXIT(FALSE);
                      IF NOT ((DWORD[1] = 111) AND (DWORD[2] = 101) AND (DWORD[3] = 16) AND (DWORD[4] = 0)) THEN BEGIN
                        LastDWORD := DWORD;
                        EXIT(TRUE);
                      END;
                    END;
                  END;
                END;
              END;
            END ELSE BEGIN
              IF ((DWORD[2] = 0) AND (DWORD[3] = 0) AND (DWORD[4] = 0)) OR
               ((DWORD[1] = 255) AND (DWORD[2] = 255) AND (DWORD[3] = 255) AND (DWORD[4] = 255))
              THEN
                Found := TRUE;
            END;
          END ELSE
            Found := FALSE;
        END;
    END;

    PROCEDURE GetNextDWORD@1000000009(VAR DWORD@1000000001 : Text[4];pInStr@1101970000 : InStream) : Boolean;
    VAR
      i@1000000005 : Integer;
      Chr@1000000003 : Char;
      IsEnd@1000000002 : Boolean;
      Int@1101970001 : Integer;
      DWORD_Ch@1101970002 : ARRAY [4] OF Char;
    BEGIN
      IsEnd := FALSE;
      FOR i := 1 TO 4 DO
        BEGIN
          IsEnd := pInStr.READ(Chr) = 0;
          IF ISSERVICETIER THEN
            DWORD_Ch[i] := Chr
          ELSE
            DWORD[i] := Chr;
        END;
      IF ISSERVICETIER THEN
        DWORD := FORMAT(DWORD_Ch[1])+ FORMAT(DWORD_Ch[2])+ FORMAT(DWORD_Ch[3])+ FORMAT(DWORD_Ch[4]);
      EXIT(NOT IsEnd);
    END;

    PROCEDURE InsertDocLine@1000000002(pNo@1000000001 : Integer;pType@1000000000 : 'TableData,Table,Form,Report,Dataport,Codeunit,XMLport,MenuSuite,Page';pComment@1000000003 : Text[250]);
    VAR
      ObjectComment@1101970001 : Record 59001;
      Object@1101970000 : Record 2000000001;
    BEGIN
      IF pComment <> '' THEN BEGIN
        Object.GET(pType,'',pNo);
        ObjectComment.INIT;
        ObjectComment.Type := pType;
        ObjectComment.ID := pNo;
        ObjectComment.Name := Object.Name;
        ObjectComment.Comment := pComment;
        ObjectComment.Indentation := 1;
        ObjectComment.INSERT(TRUE);
      END;
    END;

    BEGIN
    END.
  }
}

