OBJECT Table 351 Dimension Value Combination
{
  OBJECT-PROPERTIES
  {
    Date=05/08/11;
    Time=12:00:00;
    Version List=NAVW16.00;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Dimension Value Combination;
               PTG=Combina��o Valor Dimens�o];
  }
  FIELDS
  {
    { 1   ;   ;Dimension 1 Code    ;Code20        ;TableRelation=Dimension.Code;
                                                   CaptionML=[ENU=Dimension 1 Code;
                                                              PTG=C�d. Dimens�o 1];
                                                   NotBlank=Yes }
    { 2   ;   ;Dimension 1 Value Code;Code20      ;TableRelation="Dimension Value".Code WHERE (Dimension Code=FIELD(Dimension 1 Code));
                                                   CaptionML=[ENU=Dimension 1 Value Code;
                                                              PTG=C�d. Valor Dimens�o 1];
                                                   NotBlank=Yes }
    { 3   ;   ;Dimension 2 Code    ;Code20        ;TableRelation=Dimension.Code;
                                                   CaptionML=[ENU=Dimension 2 Code;
                                                              PTG=C�d. Dimens�o 2];
                                                   NotBlank=Yes }
    { 4   ;   ;Dimension 2 Value Code;Code20      ;TableRelation="Dimension Value".Code WHERE (Dimension Code=FIELD(Dimension 2 Code));
                                                   CaptionML=[ENU=Dimension 2 Value Code;
                                                              PTG=C�d. Valor Dimens�o 2];
                                                   NotBlank=Yes }
  }
  KEYS
  {
    {    ;Dimension 1 Code,Dimension 1 Value Code,Dimension 2 Code,Dimension 2 Value Code;
                                                   Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    END.
  }
}

