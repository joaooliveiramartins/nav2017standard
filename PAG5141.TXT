OBJECT Page 5141 Saved Segment Criteria List
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0;
  }
  PROPERTIES
  {
    Editable=No;
    CaptionML=[ENU=Saved Segment Criteria List;
               PTG=Lista Crit�rio Segmento Guardado];
    SourceTable=Table5098;
    PageType=List;
    CardPageID=Saved Segment Criteria Card;
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 2   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the code of the saved segment criteria.;
                           PTG=""];
                ApplicationArea=#All;
                SourceExpr=Code }

    { 4   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the description of the saved segment criteria.;
                           PTG=""];
                ApplicationArea=#All;
                SourceExpr=Description }

    { 6   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the ID of the user who saved the segment criteria.;
                           PTG=""];
                SourceExpr="User ID";
                Visible=FALSE }

    { 8   ;2   ;Field     ;
                DrillDown=No;
                ToolTipML=[ENU=Specifies the number of actions that make up the segment criteria.;
                           PTG=""];
                SourceExpr="No. of Actions";
                Visible=FALSE }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {

    BEGIN
    END.
  }
}

