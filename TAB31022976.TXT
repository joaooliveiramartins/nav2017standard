OBJECT Table 31022976 Income Type
{
  OBJECT-PROPERTIES
  {
    Date=10/10/16;
    Time=13:00:00;
    Version List=NAVPTSS93.00;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Income Type;
               PTG=Categoria Rendimento];
    LookupPageID=Page31023085;
    DrillDownPageID=Page31023085;
  }
  FIELDS
  {
    { 1   ;   ;Code                ;Code20        ;CaptionML=[ENU=Code;
                                                              PTG=C�digo] }
    { 2   ;   ;Description         ;Text60        ;CaptionML=[ENU=Description;
                                                              PTG=Descri��o] }
  }
  KEYS
  {
    {    ;Code                                    ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    {
      V93.00#00018 - Reten��o a Clientes e Fornecedores -  - 2016.06.29
    }
    END.
  }
}

