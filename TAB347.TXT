OBJECT Table 347 Close Income Statement Buffer
{
  OBJECT-PROPERTIES
  {
    Date=05/08/11;
    Time=12:00:00;
    Version List=NAVW16.00;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Close Income Statement Buffer;
               PTG=Mem. Int. lan�amento regul.];
  }
  FIELDS
  {
    { 1   ;   ;Closing Date        ;Date          ;CaptionML=[ENU=Closing Date;
                                                              PTG=Data Encerramento] }
    { 2   ;   ;G/L Account No.     ;Code20        ;TableRelation="G/L Account";
                                                   CaptionML=[ENU=G/L Account No.;
                                                              PTG=N� Conta C/G] }
  }
  KEYS
  {
    {    ;Closing Date,G/L Account No.            ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    END.
  }
}

