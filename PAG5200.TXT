OBJECT Page 5200 Employee Card
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Employee Card;
               PTG=Ficha Empregado];
    SourceTable=Table5200;
    PageType=Card;
    ActionList=ACTIONS
    {
      { 1900000003;0 ;ActionContainer;
                      ActionContainerType=RelatedInformation }
      { 19      ;1   ;ActionGroup;
                      CaptionML=[ENU=E&mployee;
                                 PTG=Em&pregado];
                      Image=Employee }
      { 81      ;2   ;Action    ;
                      CaptionML=[ENU=Co&mments;
                                 PTG=Co&ment�rios];
                      RunObject=Page 5222;
                      RunPageLink=Table Name=CONST(Employee),
                                  No.=FIELD(No.);
                      Image=ViewComments }
      { 184     ;2   ;Action    ;
                      ShortCutKey=Shift+Ctrl+D;
                      CaptionML=[ENU=Dimensions;
                                 PTG=Dimens�es];
                      ToolTipML=[ENU=View or edit dimensions, such as area, project, or department, that you can assign to sales and purchase documents to distribute costs and analyze transaction history.;
                                 PTG=""];
                      RunObject=Page 540;
                      RunPageLink=Table ID=CONST(5200),
                                  No.=FIELD(No.);
                      Image=Dimensions }
      { 76      ;2   ;Action    ;
                      CaptionML=[ENU=&Picture;
                                 PTG=Ima&gem];
                      RunObject=Page 5202;
                      RunPageLink=No.=FIELD(No.);
                      Image=Picture }
      { 75      ;2   ;Action    ;
                      Name=AlternativeAddresses;
                      CaptionML=[ENU=&Alternative Addresses;
                                 PTG=E&ndere�o Alternativo];
                      RunObject=Page 5204;
                      RunPageLink=Employee No.=FIELD(No.);
                      Image=Addresses }
      { 83      ;2   ;Action    ;
                      CaptionML=[ENU=&Relatives;
                                 PTG=F&amiliares];
                      RunObject=Page 5209;
                      RunPageLink=Employee No.=FIELD(No.);
                      Image=Relatives }
      { 84      ;2   ;Action    ;
                      CaptionML=[ENU=Mi&sc. Article Information;
                                 PTG=Informa��o Artigo&s Div.];
                      RunObject=Page 5219;
                      RunPageLink=Employee No.=FIELD(No.);
                      Image=Filed }
      { 88      ;2   ;Action    ;
                      CaptionML=[ENU=&Confidential Information;
                                 PTG=In&forma��o Confidencial];
                      RunObject=Page 5221;
                      RunPageLink=Employee No.=FIELD(No.);
                      Image=Lock }
      { 41      ;2   ;Action    ;
                      CaptionML=[ENU=Q&ualifications;
                                 PTG=&Qualifica��es];
                      RunObject=Page 5206;
                      RunPageLink=Employee No.=FIELD(No.);
                      Image=Certificate }
      { 87      ;2   ;Action    ;
                      CaptionML=[ENU=A&bsences;
                                 PTG=Au&s�ncias];
                      RunObject=Page 5211;
                      RunPageLink=Employee No.=FIELD(No.);
                      Image=Absence }
      { 23      ;2   ;Separator  }
      { 95      ;2   ;Action    ;
                      CaptionML=[ENU=Absences by Ca&tegories;
                                 PTG=A&us�ncias p/ Categorias];
                      RunObject=Page 5226;
                      RunPageLink=No.=FIELD(No.),
                                  Employee No. Filter=FIELD(No.);
                      Image=AbsenceCategory }
      { 70      ;2   ;Action    ;
                      CaptionML=[ENU=Misc. Articles &Overview;
                                 PTG=&Vista Artigos Diversos];
                      RunObject=Page 5228;
                      Image=FiledOverview }
      { 71      ;2   ;Action    ;
                      CaptionML=[ENU=Co&nfidential Info. Overview;
                                 PTG=Vista Info. Confi&dencial];
                      RunObject=Page 5229;
                      Image=ConfidentialOverview }
      { 61      ;2   ;Separator  }
      { 62      ;2   ;Action    ;
                      CaptionML=[ENU=Online Map;
                                 PTG=Online Map];
                      Image=Map;
                      OnAction=BEGIN
                                 DisplayMap;
                               END;
                                }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                CaptionML=[ENU=General;
                           PTG=Geral] }

    { 2   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies a number for the employee.;
                           PTG=""];
                SourceExpr="No.";
                Importance=Promoted;
                OnAssistEdit=BEGIN
                               IF AssistEdit(xRec) THEN
                                 CurrPage.UPDATE;
                             END;
                              }

    { 12  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the employee's job title.;
                           PTG=""];
                SourceExpr="Job Title";
                Importance=Promoted }

    { 4   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the employee's first name.;
                           PTG=""];
                SourceExpr="First Name";
                Importance=Promoted }

    { 8   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the employee's last name.;
                           PTG=""];
                SourceExpr="Last Name" }

    { 6   ;2   ;Field     ;
                CaptionML=[ENU=Middle Name/Initials;
                           PTG=Nome Meio/Iniciais];
                ToolTipML=[ENU=Specifies the employee's middle name.;
                           PTG=""];
                SourceExpr="Middle Name" }

    { 10  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the employee's initials.;
                           PTG=""];
                SourceExpr=Initials }

    { 14  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the employee's address.;
                           PTG=""];
                SourceExpr=Address }

    { 16  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies another line of the address.;
                           PTG=""];
                SourceExpr="Address 2" }

    { 20  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the postal code of the address.;
                           PTG=""];
                SourceExpr="Post Code" }

    { 18  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the city of the address.;
                           PTG=""];
                SourceExpr=City }

    { 1110002;2;Field     ;
                SourceExpr=County }

    { 82  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the country/region code.;
                           PTG=""];
                SourceExpr="Country/Region Code" }

    { 30  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies a search name for the employee.;
                           PTG=""];
                SourceExpr="Search Name" }

    { 11  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the employee's gender.;
                           PTG=""];
                SourceExpr=Gender }

    { 38  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the last day this entry was modified.;
                           PTG=""];
                SourceExpr="Last Date Modified";
                Importance=Promoted }

    { 1902768601;1;Group  ;
                CaptionML=[ENU=Communication;
                           PTG=Comunica��o] }

    { 72  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the employee's telephone extension.;
                           PTG=""];
                SourceExpr=Extension;
                Importance=Promoted }

    { 26  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the employee's mobile telephone number.;
                           PTG=""];
                SourceExpr="Mobile Phone No.";
                Importance=Promoted }

    { 93  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the employee's pager number.;
                           PTG=""];
                SourceExpr=Pager }

    { 74  ;2   ;Field     ;
                Name=Phone No.2;
                ToolTipML=[ENU=Specifies the employee's telephone number.;
                           PTG=""];
                SourceExpr="Phone No." }

    { 28  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the employee's email address.;
                           PTG=""];
                SourceExpr="E-Mail";
                Importance=Promoted }

    { 48  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the employee's email address at the company.;
                           PTG=""];
                SourceExpr="Company E-Mail" }

    { 32  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies a code for an alternate address.;
                           PTG=""];
                SourceExpr="Alt. Address Code" }

    { 34  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the starting date when the alternate address is valid.;
                           PTG=""];
                SourceExpr="Alt. Address Start Date" }

    { 36  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the last day when the alternate address is valid.;
                           PTG=""];
                SourceExpr="Alt. Address End Date" }

    { 1900121501;1;Group  ;
                CaptionML=[ENU=Administration;
                           PTG=Administra��o] }

    { 46  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the date when the employee began to work for the company.;
                           PTG=""];
                SourceExpr="Employment Date";
                Importance=Promoted }

    { 50  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the employment status of the employee.;
                           PTG=""];
                SourceExpr=Status;
                Importance=Promoted }

    { 52  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the date when the employee became inactive, due to disability or maternity leave, for example.;
                           PTG=""];
                SourceExpr="Inactive Date" }

    { 54  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies a code for the cause of inactivity by the employee.;
                           PTG=""];
                SourceExpr="Cause of Inactivity Code" }

    { 56  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the date when the employee was terminated, due to retirement or dismissal, for example.;
                           PTG=""];
                SourceExpr="Termination Date" }

    { 58  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies a termination code for the employee who has been terminated.;
                           PTG=""];
                SourceExpr="Grounds for Term. Code" }

    { 42  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the employment contract code for the employee.;
                           PTG=""];
                SourceExpr="Emplymt. Contract Code" }

    { 44  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies a statistics group code to assign to the employee for statistical purposes.;
                           PTG=""];
                SourceExpr="Statistics Group Code" }

    { 64  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies a resource number for the employee, if the employee is a resource in Resources Planning.;
                           PTG=""];
                SourceExpr="Resource No." }

    { 77  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies a salesperson or purchaser code for the employee, if the employee is a salesperson or purchaser in the company.;
                           PTG=""];
                SourceExpr="Salespers./Purch. Code" }

    { 1901160401;1;Group  ;
                CaptionML=[ENU=Personal;
                           PTG=Pessoal] }

    { 66  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the employee's date of birth.;
                           PTG=""];
                SourceExpr="Birth Date";
                Importance=Promoted }

    { 68  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the Social Security number of the employee.;
                           PTG=""];
                SourceExpr="Social Security No.";
                Importance=Promoted }

    { 89  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the employee's labor union membership code.;
                           PTG=""];
                SourceExpr="Union Code" }

    { 91  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the employee's labor union membership number.;
                           PTG=""];
                SourceExpr="Union Membership No." }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 3   ;1   ;Part      ;
                ApplicationArea=#Basic,#Suite;
                SubPageLink=No.=FIELD(No.);
                PagePartID=Page5202;
                PartType=Page }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                Visible=TRUE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {

    BEGIN
    END.
  }
}

