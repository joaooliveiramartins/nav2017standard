OBJECT Page 5077 Create Interaction
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Create Interaction;
               PTG=Criar Intera��o];
    InsertAllowed=No;
    DeleteAllowed=No;
    LinksAllowed=No;
    SourceTable=Table5077;
    DataCaptionExpr=Caption;
    PageType=Card;
    ShowFilter=No;
    OnInit=BEGIN
             SalespersonCodeEditable := TRUE;
             OpportunityDescriptionEditable := TRUE;
             CampaignDescriptionEditable := TRUE;
             IsOnMobile := CURRENTCLIENTTYPE = CLIENTTYPE::Phone;
           END;

    OnOpenPage=BEGIN
                 CampaignDescriptionEditable := FALSE;
                 OpportunityDescriptionEditable := FALSE;
                 IsContactEditable := (GETFILTER("Contact No.") = '') AND (GETFILTER("Contact Company No.") = '');
                 UpdateUIFlags;

                 IF SalesPurchPerson.GET(GETFILTER("Salesperson Code")) THEN
                   SalespersonCodeEditable := FALSE;

                 AttachmentReload;

                 IsFinished := FALSE;
                 CurrPage.UPDATE(FALSE);
               END;

    OnQueryClosePage=BEGIN
                       IF IsFinished THEN
                         EXIT;

                       FinishWizard(CloseAction IN [ACTION::OK,ACTION::LookupOK]);
                     END;

    ActionList=ACTIONS
    {
      { 8       ;0   ;ActionContainer;
                      ActionContainerType=ActionItems }
      { 9       ;1   ;Action    ;
                      Name=Preview;
                      CaptionML=[ENU=Preview;
                                 PTG=Pr�-Visualizar];
                      ToolTipML=ENU=Test the setup of the interaction.;
                      ApplicationArea=#RelationshipMgmt;
                      Promoted=Yes;
                      Visible=HTMLAttachment;
                      Enabled=HTMLAttachment;
                      PromotedIsBig=Yes;
                      Image=PreviewChecks;
                      PromotedCategory=Process;
                      OnAction=BEGIN
                                 PreviewHTMLContent;
                               END;
                                }
      { 10      ;1   ;Action    ;
                      Name=Finish;
                      ApplicationArea=#RelationshipMgmt;
                      Promoted=Yes;
                      Visible=IsOnMobile;
                      Enabled=IsMainInfoSet;
                      InFooterBar=Yes;
                      PromotedIsBig=Yes;
                      Image=Approve;
                      PromotedCategory=Process;
                      OnAction=BEGIN
                                 FinishWizard(TRUE);
                                 IsFinished := TRUE;
                                 CurrPage.CLOSE;
                               END;
                                }
      { 7       ;0   ;ActionContainer;
                      ActionContainerType=RelatedInformation }
      { 6       ;1   ;Action    ;
                      CaptionML=[ENU=Co&mments;
                                 PTG=Co&ment�rios];
                      ToolTipML=ENU=View or add comments.;
                      Image=ViewComments;
                      OnAction=BEGIN
                                 ShowComment;
                               END;
                                }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                Name=General;
                CaptionML=[ENU=General;
                           PTG=Geral];
                GroupType=Group }

    { 23  ;2   ;Field     ;
                Lookup=No;
                CaptionML=[ENU=Contact;
                           PTG=Contacto];
                ToolTipML=ENU=Specifies the contact that you are interacting with.;
                ApplicationArea=#RelationshipMgmt;
                SourceExpr="Wizard Contact Name";
                Editable=IsContactEditable;
                OnValidate=VAR
                             Contact@1000 : Record 5050;
                             FilterWithoutQuotes@1001 : Text;
                           BEGIN
                             "Wizard Contact Name" := DELCHR("Wizard Contact Name",'<>');
                             IF "Wizard Contact Name" = "Contact Name" THEN
                               EXIT;
                             IF "Wizard Contact Name" = '' THEN
                               CLEAR(Contact)
                             ELSE BEGIN
                               FilterWithoutQuotes := CONVERTSTR("Wizard Contact Name",'''','?');
                               Contact.SETFILTER(Name,'''@*' + FilterWithoutQuotes + '*''');
                               IF NOT Contact.FINDFIRST THEN
                                 CLEAR(Contact);
                             END;
                             SetContactNo(Contact)
                           END;

                OnAssistEdit=VAR
                               Contact@1000 : Record 5050;
                             BEGIN
                               IF IsContactEditable THEN BEGIN
                                 IF Contact.GET("Contact No.") THEN;
                                 IF PAGE.RUNMODAL(0,Contact) = ACTION::LookupOK THEN
                                   SetContactNo(Contact);
                               END;
                             END;

                ShowMandatory=TRUE }

    { 12  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the type of the interaction.;
                ApplicationArea=#RelationshipMgmt;
                NotBlank=Yes;
                SourceExpr="Interaction Template Code";
                Importance=Promoted;
                OnValidate=BEGIN
                             UpdateUIFlags;

                             IF Campaign.GET("Campaign No.") THEN
                               "Campaign Description" := Campaign.Description;

                             IF "Attachment No." <> xRec."Attachment No." THEN
                               AttachmentReload;
                           END;

                ShowMandatory=TRUE }

    { 27  ;2   ;Field     ;
                CaptionML=[ENU=Description;
                           PTG=Descri��o];
                ToolTipML=ENU=Specifies what the interaction is about.;
                ApplicationArea=#RelationshipMgmt;
                NotBlank=Yes;
                SourceExpr=Description;
                Importance=Promoted;
                ShowMandatory=TRUE }

    { 22  ;2   ;Field     ;
                CaptionML=[ENU=Salesperson;
                           PTG=Vendedor];
                ToolTipML=ENU=Specifies the salesperson who is responsible for this interaction.;
                ApplicationArea=#Suite,#RelationshipMgmt;
                SourceExpr="Salesperson Code";
                Editable=SalespersonCodeEditable;
                ShowMandatory=TRUE }

    { 2   ;2   ;Field     ;
                ToolTipML=ENU=Specifies the language that you will use in this interaction.;
                ApplicationArea=#RelationshipMgmt;
                SourceExpr="Language Code";
                Enabled=IsMainInfoSet;
                OnValidate=BEGIN
                             IF "Attachment No." <> xRec."Attachment No." THEN
                               AttachmentReload;
                           END;

                OnLookup=BEGIN
                           LanguageCodeOnLookup;
                           IF "Attachment No." <> xRec."Attachment No." THEN
                             AttachmentReload;
                         END;
                          }

    { 4   ;1   ;Group     ;
                Name=Content;
                CaptionML=[ENU=Content;
                           PTG=Conte�do];
                Visible=HTMLAttachment;
                GroupType=Group }

    { 5   ;2   ;Field     ;
                Name=HTMLContentBodyText;
                ApplicationArea=#RelationshipMgmt;
                SourceExpr=HTMLContentBodyText;
                MultiLine=Yes;
                OnValidate=BEGIN
                             UpdateContentBodyTextInCustomLayoutAttachment(HTMLContentBodyText);
                           END;

                ShowCaption=No }

    { 3   ;1   ;Group     ;
                Name=InteractionDetails;
                CaptionML=[ENU=Interaction Details;
                           PTG=Detalhes Intera��o];
                Enabled=IsMainInfoSet;
                GroupType=Group }

    { 45  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the type of correspondence that you use for this interaction: email, fax, or a printed letter.;
                ApplicationArea=#RelationshipMgmt;
                SourceExpr="Correspondence Type";
                Importance=Additional;
                Enabled=IsMainInfoSet;
                OnValidate=BEGIN
                             ValidateCorrespondenceType;
                           END;
                            }

    { 50  ;2   ;Field     ;
                CaptionML=[ENU=Date of Interaction;
                           PTG=Data da Intera��o];
                ToolTipML=ENU=Specifies the date when the interaction took place.;
                ApplicationArea=#RelationshipMgmt;
                SourceExpr=Date;
                Importance=Additional;
                Enabled=IsMainInfoSet }

    { 52  ;2   ;Field     ;
                ToolTipML=ENU=Specifies�the time when the interaction took place;
                ApplicationArea=#RelationshipMgmt;
                SourceExpr="Time of Interaction";
                Importance=Additional;
                Enabled=IsMainInfoSet }

    { 30  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the direction of the interaction, inbound or outbound.;
                ApplicationArea=#RelationshipMgmt;
                SourceExpr="Information Flow";
                Importance=Additional;
                Enabled=IsMainInfoSet }

    { 32  ;2   ;Field     ;
                ToolTipML=[ENU="Specifies if the interaction was initiated by your company or by one of your contacts. The Us option indicates that your company was the initiator; the Them option indicates that a contact was the initiator."];
                ApplicationArea=#RelationshipMgmt;
                SourceExpr="Initiated By";
                Importance=Additional;
                Enabled=IsMainInfoSet }

    { 34  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the evaluation of the interaction involving the contact in the segment.;
                ApplicationArea=#RelationshipMgmt;
                SourceExpr=Evaluation;
                Importance=Additional;
                Enabled=IsMainInfoSet }

    { 36  ;2   ;Field     ;
                CaptionML=[ENU=Was Successful;
                           PTG=Foi Efetuado com Sucesso];
                ToolTipML=ENU=Specifies if the interaction was successful. Clear this check box to indicate that the interaction was not a success.;
                ApplicationArea=#RelationshipMgmt;
                SourceExpr="Interaction Successful";
                Importance=Additional;
                Enabled=IsMainInfoSet }

    { 13  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the cost of the interaction with the contact that this segment line applies to.;
                ApplicationArea=#RelationshipMgmt;
                SourceExpr="Cost (LCY)";
                Importance=Additional;
                Enabled=IsMainInfoSet }

    { 11  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the duration of the interaction with the contact.;
                ApplicationArea=#RelationshipMgmt;
                SourceExpr="Duration (Min.)";
                Importance=Additional;
                Enabled=IsMainInfoSet }

    { 20  ;2   ;Field     ;
                Lookup=No;
                CaptionML=[ENU=Campaign;
                           PTG=Campanha];
                ToolTipML=ENU=Specifies the campaign that is related to the segment. The description is copied from the campaign card.;
                SourceExpr="Campaign Description";
                TableRelation=Campaign;
                Importance=Promoted;
                Enabled=IsMainInfoSet;
                Editable=CampaignDescriptionEditable;
                OnAssistEdit=VAR
                               Campaign@1102601000 : Record 5071;
                             BEGIN
                               IF GETFILTER("Campaign No.") = '' THEN BEGIN
                                 IF Campaign.GET("Campaign No.") THEN ;
                                 IF PAGE.RUNMODAL(0,Campaign) = ACTION::LookupOK THEN BEGIN
                                   VALIDATE("Campaign No.",Campaign."No.");
                                   "Campaign Description" := Campaign.Description;
                                 END;
                               END;
                             END;
                              }

    { 51  ;2   ;Field     ;
                CaptionML=[ENU=Contact is Targeted;
                           PTG=Contacto � Objetivo];
                ToolTipML=ENU=Specifies that the segment involved in this interaction is the target of a campaign. This is used to measure the response rate of a campaign.;
                SourceExpr="Campaign Target";
                Importance=Additional;
                Enabled=IsMainInfoSet }

    { 53  ;2   ;Field     ;
                CaptionML=[ENU=Campaign Response;
                           PTG=Resposta Campanha];
                ToolTipML=ENU=Specifies that the interaction created for the segment is the response to a campaign. For example, coupons that are sent as a response to a campaign.;
                SourceExpr="Campaign Response";
                Importance=Additional;
                Enabled=IsMainInfoSet }

    { 54  ;2   ;Field     ;
                Lookup=No;
                CaptionML=[ENU=Opportunity;
                           PTG=Oportunidade];
                ToolTipML=ENU=Specifies a description of the opportunity that is related to the segment. The description is copied from the opportunity card.;
                ApplicationArea=#RelationshipMgmt;
                SourceExpr="Opportunity Description";
                TableRelation=Opportunity;
                Importance=Promoted;
                Enabled=IsMainInfoSet;
                Editable=OpportunityDescriptionEditable;
                OnAssistEdit=VAR
                               Opportunity@1000 : Record 5092;
                             BEGIN
                               FilterContactCompanyOpportunities(Opportunity);
                               IF PAGE.RUNMODAL(0,Opportunity) = ACTION::LookupOK THEN BEGIN
                                 VALIDATE("Opportunity No.",Opportunity."No.");
                                 "Opportunity Description" := Opportunity.Description;
                               END;
                             END;
                              }

  }
  CODE
  {
    VAR
      SalesPurchPerson@1007 : Record 13;
      Campaign@1008 : Record 5071;
      Todo@1022 : Record 5080;
      HTMLContentBodyText@1011 : Text;
      CampaignDescriptionEditable@19061248 : Boolean INDATASET;
      OpportunityDescriptionEditable@19023234 : Boolean INDATASET;
      SalespersonCodeEditable@19071610 : Boolean INDATASET;
      IsMainInfoSet@1002 : Boolean;
      HTMLAttachment@1010 : Boolean;
      UntitledTxt@1004 : TextConst 'ENU=untitled;PTG=sem t�tulo';
      IsOnMobile@1005 : Boolean;
      IsFinished@1012 : Boolean;
      IsContactEditable@1000 : Boolean;

    LOCAL PROCEDURE Caption@1() : Text[260];
    VAR
      Contact@1001 : Record 5050;
      CaptionStr@1000 : Text[260];
    BEGIN
      IF Contact.GET(GETFILTER("Contact Company No.")) THEN
        CaptionStr := COPYSTR(Contact."No." + ' ' + Contact.Name,1,MAXSTRLEN(CaptionStr));
      IF Contact.GET(GETFILTER("Contact No.")) THEN
        CaptionStr := COPYSTR(CaptionStr + ' ' + Contact."No." + ' ' + Contact.Name,1,MAXSTRLEN(CaptionStr));
      IF SalesPurchPerson.GET(GETFILTER("Salesperson Code")) THEN
        CaptionStr := COPYSTR(CaptionStr + ' ' + SalesPurchPerson.Code + ' ' + SalesPurchPerson.Name,1,MAXSTRLEN(CaptionStr));
      IF Campaign.GET(GETFILTER("Campaign No.")) THEN
        CaptionStr := COPYSTR(CaptionStr + ' ' + Campaign."No." + ' ' + Campaign.Description,1,MAXSTRLEN(CaptionStr));
      IF Todo.GET(GETFILTER("To-do No.")) THEN
        CaptionStr := COPYSTR(CaptionStr + ' ' + Todo."No." + ' ' + Todo.Description,1,MAXSTRLEN(CaptionStr));

      IF CaptionStr = '' THEN
        CaptionStr := UntitledTxt;

      EXIT(CaptionStr);
    END;

    LOCAL PROCEDURE UpdateUIFlags@3();
    BEGIN
      IsMainInfoSet := "Interaction Template Code" <> '';
    END;

    LOCAL PROCEDURE AttachmentReload@4();
    BEGIN
      LoadAttachment;
      HTMLAttachment := IsHTMLAttachment;
      IF HTMLAttachment THEN
        HTMLContentBodyText := LoadContentBodyTextFromCustomLayoutAttachment;
    END;

    LOCAL PROCEDURE SetContactNo@6(Contact@1000 : Record 5050);
    BEGIN
      VALIDATE("Contact No.",Contact."No.");
      "Wizard Contact Name" := Contact.Name;
    END;

    BEGIN
    END.
  }
}

