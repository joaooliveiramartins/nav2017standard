OBJECT Table 5053 Business Relation
{
  OBJECT-PROPERTIES
  {
    Date=07/09/12;
    Time=12:00:00;
    Version List=NAVW17.00;
  }
  PROPERTIES
  {
    DataCaptionFields=Code,Description;
    CaptionML=[ENU=Business Relation;
               PTG=Rela��es Comerciais];
    LookupPageID=Page5060;
  }
  FIELDS
  {
    { 1   ;   ;Code                ;Code10        ;CaptionML=[ENU=Code;
                                                              PTG=C�digo];
                                                   NotBlank=Yes }
    { 2   ;   ;Description         ;Text50        ;CaptionML=[ENU=Description;
                                                              PTG=Descri��o] }
    { 3   ;   ;No. of Contacts     ;Integer       ;FieldClass=FlowField;
                                                   CalcFormula=Count("Contact Business Relation" WHERE (Business Relation Code=FIELD(Code)));
                                                   CaptionML=[ENU=No. of Contacts;
                                                              PTG=N� de Contactos];
                                                   Editable=No }
  }
  KEYS
  {
    {    ;Code                                    ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    END.
  }
}

