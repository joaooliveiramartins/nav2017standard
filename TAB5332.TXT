OBJECT Table 5332 Coupling Record Buffer
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Coupling Record Buffer;
               PTG=Mem�ria Interna Registo Acupulado];
  }
  FIELDS
  {
    { 1   ;   ;NAV Name            ;Text250       ;CaptionML=[ENU=NAV Name;
                                                              PTG=Nome NAV] }
    { 2   ;   ;CRM Name            ;Text250       ;OnValidate=VAR
                                                                CRMIntegrationRecord@1001 : Record 5331;
                                                              BEGIN
                                                                IF FindCRMRecordByName("CRM Name") THEN BEGIN
                                                                  IF "Saved CRM ID" <> "CRM ID" THEN
                                                                    CRMIntegrationRecord.AssertRecordIDCanBeCoupled("NAV Record ID","CRM ID");
                                                                  "CRM Name" := CalcCRMName;
                                                                END ELSE
                                                                  ERROR(STRSUBSTNO(NoSuchCRMRecordErr,"CRM Name"));
                                                              END;

                                                   OnLookup=VAR
                                                              CRMIntegrationRecord@1001 : Record 5331;
                                                            BEGIN
                                                              IF LookupCRMTables.Lookup("CRM Table ID","Saved CRM ID","CRM ID") THEN BEGIN
                                                                IF "Saved CRM ID" <> "CRM ID" THEN
                                                                  CRMIntegrationRecord.AssertRecordIDCanBeCoupled("NAV Record ID","CRM ID");
                                                                "CRM Name" := CalcCRMName;
                                                              END;
                                                            END;

                                                   CaptionML=[ENU=CRM Name;
                                                              PTG=Nome CRM] }
    { 3   ;   ;NAV Table ID        ;Integer       ;OnValidate=VAR
                                                                IntegrationTableMapping@1000 : Record 5335;
                                                              BEGIN
                                                                IntegrationTableMapping.SETRANGE("Table ID","NAV Table ID");
                                                                IF IntegrationTableMapping.FINDFIRST THEN
                                                                  "CRM Table Name" := IntegrationTableMapping.Name
                                                                ELSE
                                                                  "CRM Table Name" := '';
                                                              END;

                                                   CaptionML=[ENU=NAV Table ID;
                                                              PTG=ID Tabela NAV] }
    { 4   ;   ;CRM Table ID        ;Integer       ;CaptionML=[ENU=CRM Table ID;
                                                              PTG=ID Tabela CRM] }
    { 5   ;   ;Sync Action         ;Option        ;CaptionML=[ENU=Sync Action;
                                                              PTG=A��o Sincroniza��o];
                                                   OptionCaptionML=[ENU=Do Not Synchronize,To Integration Table,From Integration Table;
                                                                    PTG="N�o Sincronizar,Para Tabela Integra��o,Da Tabela "];
                                                   OptionString=Do Not Synchronize,To Integration Table,From Integration Table }
    { 8   ;   ;NAV Record ID       ;RecordID      ;CaptionML=[ENU=NAV Record ID;
                                                              PTG=ID Registo NAV] }
    { 9   ;   ;CRM ID              ;GUID          ;OnValidate=BEGIN
                                                                "CRM Name" := CalcCRMName;
                                                              END;

                                                   CaptionML=[ENU=CRM ID;
                                                              PTG=ID CRM] }
    { 10  ;   ;Create New          ;Boolean       ;OnValidate=VAR
                                                                NullGUID@1000 : GUID;
                                                              BEGIN
                                                                IF "Create New" THEN BEGIN
                                                                  "Saved Sync Action" := "Sync Action";
                                                                  "Saved CRM ID" := "CRM ID";
                                                                  VALIDATE("Sync Action","Sync Action"::"To Integration Table");
                                                                  CLEAR(NullGUID);
                                                                  VALIDATE("CRM ID",NullGUID);
                                                                END ELSE BEGIN
                                                                  VALIDATE("Sync Action","Saved Sync Action");
                                                                  VALIDATE("CRM ID","Saved CRM ID");
                                                                END;
                                                              END;

                                                   CaptionML=[ENU=Create New;
                                                              PTG=Criar Novo] }
    { 11  ;   ;Saved Sync Action   ;Option        ;CaptionML=[ENU=Saved Sync Action;
                                                              PTG=A��o Guardada de Sincroniza��o];
                                                   OptionCaptionML=[ENU=Do Not Synchronize,To Integration Table,From Integration Table;
                                                                    PTG=N�o Sincronizar,Para Tabela Integra��o,Da Tabela Integra��o];
                                                   OptionString=Do Not Synchronize,To Integration Table,From Integration Table }
    { 12  ;   ;Saved CRM ID        ;GUID          ;CaptionML=[ENU=Saved CRM ID;
                                                              PTG=ID Guardado CRM] }
    { 13  ;   ;CRM Table Name      ;Code20        ;CaptionML=[ENU=CRM Table Name;
                                                              PTG=Nome Tabela CRM];
                                                   Editable=No }
  }
  KEYS
  {
    {    ;NAV Name                                ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {
    VAR
      InitialSynchDisabledErr@1001 : TextConst 'ENU=No initial synchronization direction was specified because initial synchronization was disabled.;PTG=N�o foi especificada a dire��o de sincroniza��o inicial porque a sincroniza��o inicial estava desativada.';
      NoSuchCRMRecordErr@1000 : TextConst '@@@="%1 = The record name entered by the user";ENU=A record with the name %1 does not exist in Dynamics CRM.;PTG="Um registo com o nome %1 n�o existe no Dynamics CRM "';
      CRMSetupDefaults@1002 : Codeunit 5334;
      LookupCRMTables@1003 : Codeunit 5332;

    PROCEDURE Initialize@1(NAVRecordID@1000 : RecordID);
    VAR
      IntegrationTableMapping@1004 : Record 5335;
      RecordRef@1003 : RecordRef;
    BEGIN
      RecordRef := NAVRecordID.GETRECORD;
      RecordRef.FIND;

      INIT;
      VALIDATE("NAV Table ID",NAVRecordID.TABLENO);
      "NAV Record ID" := NAVRecordID;
      "NAV Name" := NameValue(RecordRef);
      "CRM Table ID" := CRMSetupDefaults.GetCRMTableNo("NAV Table ID");
      IF CRMSetupDefaults.GetDefaultDirection("NAV Table ID") = IntegrationTableMapping.Direction::FromIntegrationTable THEN
        VALIDATE("Sync Action","Sync Action"::"From Integration Table")
      ELSE
        VALIDATE("Sync Action","Sync Action"::"To Integration Table");
      IF FindCRMId THEN BEGIN
        "CRM Name" := CalcCRMName;
        VALIDATE("Sync Action","Sync Action"::"Do Not Synchronize");
        "Saved CRM ID" := "CRM ID";
      END
    END;

    LOCAL PROCEDURE FindCRMId@8() : Boolean;
    VAR
      CRMIntegrationRecord@1002 : Record 5331;
    BEGIN
      EXIT(CRMIntegrationRecord.FindIDFromRecordID("NAV Record ID","CRM ID"))
    END;

    LOCAL PROCEDURE FindCRMRecordByName@7(VAR CRMName@1004 : Text[250]) : Boolean;
    VAR
      RecordRef@1002 : RecordRef;
      FieldRef@1001 : FieldRef;
      Found@1003 : Boolean;
    BEGIN
      RecordRef.OPEN("CRM Table ID");
      FieldRef := RecordRef.FIELD(CRMSetupDefaults.GetNameFieldNo("CRM Table ID"));
      FieldRef.SETRANGE(CRMName);
      IF RecordRef.FINDFIRST THEN
        Found := TRUE
      ELSE BEGIN
        RecordRef.CURRENTKEYINDEX(2); // "Name" key should be the second key in a CRM table
        FieldRef := RecordRef.FIELD(CRMSetupDefaults.GetNameFieldNo("CRM Table ID"));
        FieldRef.SETFILTER("CRM Name" + '*');
        IF RecordRef.FINDFIRST THEN
          Found := TRUE;
      END;
      IF Found THEN BEGIN
        CRMName := NameValue(RecordRef);
        "CRM ID" := PrimaryKeyValue(RecordRef);
      END;
      RecordRef.CLOSE;
      EXIT(Found);
    END;

    LOCAL PROCEDURE CalcCRMName@9() : Text[250];
    VAR
      RecordRef@1003 : RecordRef;
      CRMName@1000 : Text[250];
    BEGIN
      RecordRef.OPEN("CRM Table ID");
      FindCRMRecRefByPK(RecordRef,"CRM ID");
      CRMName := NameValue(RecordRef);
      RecordRef.CLOSE;
      EXIT(CRMName);
    END;

    PROCEDURE GetInitialSynchronizationDirection@13() : Integer;
    VAR
      IntegrationTableMapping@1000 : Record 5335;
    BEGIN
      IF "Sync Action" = "Sync Action"::"Do Not Synchronize" THEN
        ERROR(InitialSynchDisabledErr);

      IF "Sync Action" = "Sync Action"::"To Integration Table" THEN
        EXIT(IntegrationTableMapping.Direction::ToIntegrationTable);

      EXIT(IntegrationTableMapping.Direction::FromIntegrationTable);
    END;

    PROCEDURE GetPerformInitialSynchronization@6() : Boolean;
    BEGIN
      EXIT("Sync Action" <> "Sync Action"::"Do Not Synchronize");
    END;

    LOCAL PROCEDURE NameValue@10(RecordRef@1000 : RecordRef) : Text[250];
    VAR
      FieldRef@1001 : FieldRef;
    BEGIN
      FieldRef := RecordRef.FIELD(CRMSetupDefaults.GetNameFieldNo(RecordRef.NUMBER));
      EXIT(COPYSTR(FORMAT(FieldRef.VALUE),1,MAXSTRLEN("CRM Name")));
    END;

    LOCAL PROCEDURE PrimaryKeyValue@12(RecordRef@1000 : RecordRef) : GUID;
    VAR
      FieldRef@1002 : FieldRef;
      PrimaryKeyRef@1001 : KeyRef;
    BEGIN
      PrimaryKeyRef := RecordRef.KEYINDEX(1);
      FieldRef := PrimaryKeyRef.FIELDINDEX(1);
      EXIT(FieldRef.VALUE);
    END;

    LOCAL PROCEDURE FindCRMRecRefByPK@5(VAR RecordRef@1000 : RecordRef;CRMId@1001 : GUID) : Boolean;
    VAR
      FieldRef@1002 : FieldRef;
      PrimaryKeyRef@1003 : KeyRef;
    BEGIN
      PrimaryKeyRef := RecordRef.KEYINDEX(1);
      FieldRef := PrimaryKeyRef.FIELDINDEX(1);
      FieldRef.SETRANGE(CRMId);
      EXIT(RecordRef.FINDFIRST);
    END;

    BEGIN
    END.
  }
}

