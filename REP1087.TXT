OBJECT Report 1087 Change Job Dates
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Change Job Dates;
               PTG=Alterar Datas Projeto];
    ProcessingOnly=Yes;
    OnPreReport=BEGIN
                  ScheduleLine := FALSE;
                  ContractLine := FALSE;
                  IF Linetype = Linetype::Budget THEN
                    ScheduleLine := TRUE;
                  IF Linetype = Linetype::Billable THEN
                    ContractLine := TRUE;
                  IF Linetype = Linetype::"Budget+Billable" THEN BEGIN
                    ScheduleLine := TRUE;
                    ContractLine := TRUE;
                  END;

                  ScheduleLine2 := FALSE;
                  ContractLine2 := FALSE;
                  IF Linetype2 = Linetype2::Budget THEN
                    ScheduleLine2 := TRUE;
                  IF Linetype2 = Linetype2::Billable THEN
                    ContractLine2 := TRUE;
                  IF Linetype2 = Linetype2::"Budget+Billable" THEN BEGIN
                    ScheduleLine2 := TRUE;
                    ContractLine2 := TRUE;
                  END;
                  IF (Linetype = Linetype::" ") AND (Linetype2 = Linetype2::" ") THEN
                    ERROR(Text000);
                  IF NOT ChangePlanningDate AND NOT ChangeCurrencyDate THEN
                    ERROR(Text000);
                  IF ChangeCurrencyDate AND (Linetype = Linetype::" ") THEN
                    ERROR(Text001);
                  IF ChangePlanningDate AND (Linetype2 = Linetype2::" ") THEN
                    ERROR(Text002);
                END;

  }
  DATASET
  {
    { 2969;    ;DataItem;                    ;
               DataItemTable=Table1001;
               DataItemTableView=SORTING(Job No.,Job Task No.);
               OnAfterGetRecord=BEGIN
                                  CLEAR(CalculateBatches);
                                  IF ChangePlanningDate THEN
                                    IF Linetype2 <> Linetype2::" " THEN
                                      CalculateBatches.ChangePlanningDates(
                                        "Job Task",ScheduleLine2,ContractLine2,PeriodLength2,FixedDate2,StartingDate2,EndingDate2);
                                  CLEAR(CalculateBatches);
                                  IF ChangeCurrencyDate THEN
                                    IF Linetype <> Linetype::" " THEN
                                      CalculateBatches.ChangeCurrencyDates(
                                        "Job Task",ScheduleLine,ContractLine,
                                        PeriodLength,FixedDate,StartingDate,EndingDate);
                                END;

               OnPostDataItem=BEGIN
                                CalculateBatches.ChangeDatesEnd;
                              END;

               ReqFilterFields=Job No.,Job Task No. }

  }
  REQUESTPAGE
  {
    PROPERTIES
    {
      SaveValues=Yes;
    }
    CONTROLS
    {
      { 1900000001;0;Container;
                  ContainerType=ContentArea }

      { 1900000002;1;Group  ;
                  CaptionML=[ENU=Options;
                             PTG=Op��es] }

      { 11  ;2   ;Group     ;
                  CaptionML=[ENU=Currency Date;
                             PTG=Data Divisa] }

      { 13  ;3   ;Field     ;
                  Name=ChangeCurrencyDate;
                  CaptionML=[ENU=Change Currency Date;
                             PTG=Alterar Data Divisa];
                  ToolTipML=[ENU=Specifies that currencies will be updated on the jobs that are included in the batch job.;
                             PTG=Especifica que as divisas v�o ser atualizadas para os projetos que esta�o inclu�dos na fila de tarefas.];
                  ApplicationArea=#Jobs;
                  SourceExpr=ChangeCurrencyDate }

      { 1   ;3   ;Field     ;
                  Name=ChangeDateExpressionCurrency;
                  CaptionML=[ENU=Change Date Expression;
                             PTG=Alterar Express�o da Data];
                  ToolTipML=[ENU=Specifies how the dates on the entries that are copied will be changed by using a date formula.;
                             PTG=Especifica como as datas dos movimentos que s�o copiados, ser�o alteradas usando uma f�rmula de data.];
                  ApplicationArea=#Jobs;
                  DateFormula=Yes;
                  BlankZero=Yes;
                  SourceExpr=PeriodLength;
                  OnValidate=BEGIN
                               FixedDate := 0D;
                             END;
                              }

      { 8   ;3   ;Field     ;
                  Name=FixedDateCurrency;
                  CaptionML=[ENU=Fixed Date;
                             PTG=Data Fixada];
                  ToolTipML=[ENU=Specifies a date that the currency date on all planning lines will be moved to.;
                             PTG=Especifica uma data para a qual a data da divisa nas linhas de planeamento ser� alterada.];
                  ApplicationArea=#Jobs;
                  SourceExpr=FixedDate;
                  OnValidate=BEGIN
                               CLEAR(PeriodLength);
                             END;
                              }

      { 3   ;3   ;Field     ;
                  Name=IncludeLineTypeCurrency;
                  CaptionML=[ENU=Include Line type;
                             PTG=Incluir tipo Linha];
                  ToolTipML=[ENU=Specifies the job planning line type you want to change the currency date for.;
                             PTG=Especifica o tipo de linha de planeamento de projeto para o qual deseja alterar a data da divisa.];
                  OptionCaptionML=[ENU=" ,Budget,Billable,Budget+Billable";
                                   PTG=" ,Or�amentado,Fatur�vel,Or�amentado+Fatur�vel"];
                  ApplicationArea=#Jobs;
                  SourceExpr=Linetype }

      { 15  ;3   ;Field     ;
                  Name=IncludeCurrDateFrom;
                  CaptionML=[ENU=Include Curr. Date From;
                             PTG=Incluir Data Divisa De];
                  ToolTipML=[ENU=Specifies the starting date of the period for which you want currency dates to be moved. Only planning lines with a currency date on or after this date are included.;
                             PTG=Especifica a data inicial do per�odo para o qual deseja que as datas da divisa sejam movidas. Apenas linhas com uma data de divisa igual ou posterior a esta data ser�o inclu�das.];
                  ApplicationArea=#Jobs;
                  SourceExpr=StartingDate }

      { 22  ;3   ;Field     ;
                  Name=IncludeCurrDateTo;
                  CaptionML=[ENU=Include Curr. Date To;
                             PTG=Incluir Data Divisa A];
                  ToolTipML=[ENU=Specifies the ending date of the period for which you want currency dates to be moved. Only planning lines with a currency date on or before this date are included.;
                             PTG=Especifica a data inicial do per�odo para o qual deseja que as datas da divisa sejam movidas. Apenas linhas com uma data de divisa igual ou anterior a esta data ser�o inclu�das.];
                  ApplicationArea=#Jobs;
                  SourceExpr=EndingDate }

      { 7   ;2   ;Group     ;
                  CaptionML=[ENU=Planning Date;
                             PTG=Data Planeamento] }

      { 27  ;3   ;Field     ;
                  Name=ChangePlanningDate;
                  CaptionML=[ENU=Change Planning Date;
                             PTG=Alterar Data Planeamento];
                  ToolTipML=[ENU=Specifies that planning dates will be changed on the jobs that are included in the batch job.;
                             PTG=Especifica que as datas de planeamento ser�o alteradas nos projetos que est�o inclu�dos na fila de tarefas.];
                  ApplicationArea=#Jobs;
                  SourceExpr=ChangePlanningDate }

      { 10  ;3   ;Field     ;
                  Name=ChangeDateExpressionPlanning;
                  CaptionML=[ENU=Change Date Expression;
                             PTG=Alterar Express�o da Data];
                  ToolTipML=[ENU=Specifies how the dates on the entries that are copied will be changed by using a date formula.;
                             PTG=especifica com oas datas dos movimentos que s�o copiados, v�o ser alteradas usando uma f�rmua de data.];
                  ApplicationArea=#Jobs;
                  DateFormula=Yes;
                  BlankZero=Yes;
                  SourceExpr=PeriodLength2;
                  OnValidate=BEGIN
                               FixedDate2 := 0D;
                             END;
                              }

      { 12  ;3   ;Field     ;
                  Name=FixedDatePlanning;
                  CaptionML=[ENU=Fixed Date;
                             PTG=Data Fixada];
                  ToolTipML=[ENU=Specifies a date that the planning date on all planning lines will be moved to.;
                             PTG=Especifica uma data para qual a data de planeamento em todas as linhas de planeamento, ser� movida.];
                  ApplicationArea=#Jobs;
                  SourceExpr=FixedDate2;
                  OnValidate=BEGIN
                               CLEAR(PeriodLength2);
                             END;
                              }

      { 17  ;3   ;Field     ;
                  Name=IncludeLineTypePlanning;
                  CaptionML=[ENU=Include Line type;
                             PTG=Incluir tipo Linha];
                  ToolTipML=[ENU=Specifies the job planning line type you want to change the planning date for.;
                             PTG=Especifica o tipo de linha de planeamento de projeto para o qual deseja alterar a data de planeamento.];
                  OptionCaptionML=[ENU=" ,Budget,Billable,Budget+Billable";
                                   PTG=" ,Or�amentado,Fatur�vel,Or�amentado+Fatur�vel"];
                  ApplicationArea=#Jobs;
                  SourceExpr=Linetype2 }

      { 20  ;3   ;Field     ;
                  Name=IncludePlanDateFrom;
                  CaptionML=[ENU=Include Plan. Date From;
                             PTG=Incluir Data Plan. De];
                  ToolTipML=[ENU=Specifies the starting date of the period for which you want a Planning Date to be moved. Only planning lines with a Planning Date on or after this date are included.;
                             PTG=Especifica a data inicial do per�odo para  o qual deseja que a Data de Pleneamento  seja movida. Apenas linhas de planeamento com Data Planeamento igual ou posterior a esta data ser�o inclu�das.];
                  ApplicationArea=#Jobs;
                  SourceExpr=StartingDate2 }

      { 21  ;3   ;Field     ;
                  Name=IncludePlanDateTo;
                  CaptionML=[ENU=Include Plan. Date To;
                             PTG=Incluir Data Plan. A];
                  ToolTipML=[ENU=Specifies the ending date of the period for which you want a Planning Date to be moved. Only planning lines with a Planning Date on or before this date are included.;
                             PTG=Especifica a data final do per�odo para  o qual deseja que a Data de Pleneamento  seja movida. Apenas linhas de planeamento com Data Planeamento igual ou anterior a esta data ser�o inclu�das.];
                  ApplicationArea=#Jobs;
                  SourceExpr=EndingDate2 }

    }
  }
  LABELS
  {
  }
  CODE
  {
    VAR
      CalculateBatches@1007 : Codeunit 1005;
      PeriodLength@1010 : DateFormula;
      PeriodLength2@1014 : DateFormula;
      ScheduleLine@1008 : Boolean;
      ContractLine@1009 : Boolean;
      ScheduleLine2@1011 : Boolean;
      ContractLine2@1006 : Boolean;
      Linetype@1012 : ' ,Budget,Billable,Budget+Billable';
      Linetype2@1004 : ' ,Budget,Billable,Budget+Billable';
      FixedDate@1001 : Date;
      FixedDate2@1000 : Date;
      StartingDate@1005 : Date;
      EndingDate@1013 : Date;
      StartingDate2@1003 : Date;
      EndingDate2@1002 : Date;
      Text000@1019 : TextConst 'ENU=There is nothing to change.;PTG=N�o existe nada para alterar.';
      ChangePlanningDate@1020 : Boolean;
      ChangeCurrencyDate@1021 : Boolean;
      Text001@1022 : TextConst 'ENU=You must specify a Line Type for changing the currency date.;PTG=Tem de especificar um Tipo de Linha para alterar a data da divisa.';
      Text002@1023 : TextConst 'ENU=You must specify a Line Type for changing the planning date.;PTG=Tem de especificar um Tipo de Linha para alterar a data de planeamento.';

    BEGIN
    END.
  }
  RDLDATA
  {
  }
}

