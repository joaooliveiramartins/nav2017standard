OBJECT Page 5180 Sales Archive Comment Sheet
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0;
  }
  PROPERTIES
  {
    Editable=No;
    CaptionML=[ENU=Comment Sheet;
               PTG=Folha Coment rios];
    SourceTable=Table5126;
    PageType=List;
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 2   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the version number of the archived document.;
                           PTG=""];
                SourceExpr=Date }

    { 4   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the document line number of the quote or order to which the comment applies.;
                           PTG=""];
                SourceExpr=Code;
                Visible=FALSE }

    { 6   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the line number for the comment.;
                           PTG=""];
                SourceExpr=Comment }

  }
  CODE
  {

    BEGIN
    END.
  }
}

