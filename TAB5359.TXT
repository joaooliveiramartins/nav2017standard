OBJECT Table 5359 CRM Team
{
  OBJECT-PROPERTIES
  {
    Date=24/11/16;
    Time=12:00:00;
    Version List=NAVW110.0.00.14199;
  }
  PROPERTIES
  {
    TableType=CRM;
    ExternalName=team;
    CaptionML=[ENU=CRM Team;
               PTG=Equipa];
    Description=Collection of system users that routinely collaborate. Teams can be used to simplify record sharing and provide team members with common access to organization data when team members belong to different Business Units.;
  }
  FIELDS
  {
    { 1   ;   ;TeamId              ;GUID          ;ExternalName=teamid;
                                                   ExternalType=Uniqueidentifier;
                                                   ExternalAccess=Insert;
                                                   CaptionML=[ENU=Team;
                                                              PTG=Equipa];
                                                   Description=Unique identifier for the team. }
    { 2   ;   ;OrganizationId      ;GUID          ;ExternalName=organizationid;
                                                   ExternalType=Uniqueidentifier;
                                                   ExternalAccess=Read;
                                                   CaptionML=[ENU="Organization ";
                                                              PTG=Organiza��o];
                                                   Description=Unique identifier of the organization associated with the team. }
    { 3   ;   ;BusinessUnitId      ;GUID          ;TableRelation="CRM Businessunit".BusinessUnitId;
                                                   ExternalName=businessunitid;
                                                   ExternalType=Lookup;
                                                   CaptionML=[ENU=Business Unit;
                                                              PTG=Unidade Neg�cio];
                                                   Description=Unique identifier of the business unit with which the team is associated. }
    { 4   ;   ;Name                ;Text160       ;ExternalName=name;
                                                   ExternalType=String;
                                                   CaptionML=[ENU=Team Name;
                                                              PTG=Nome Equipa];
                                                   Description=Name of the team. }
    { 5   ;   ;Description         ;BLOB          ;ExternalName=description;
                                                   ExternalType=Memo;
                                                   CaptionML=[ENU=Description;
                                                              PTG=Descri��o];
                                                   Description=Description of the team.;
                                                   SubType=Memo }
    { 6   ;   ;EMailAddress        ;Text100       ;ExtendedDatatype=E-Mail;
                                                   ExternalName=emailaddress;
                                                   ExternalType=String;
                                                   CaptionML=[ENU=Email;
                                                              PTG=E-Mail];
                                                   Description=Email address for the team. }
    { 7   ;   ;CreatedOn           ;DateTime      ;ExternalName=createdon;
                                                   ExternalType=DateTime;
                                                   ExternalAccess=Read;
                                                   CaptionML=[ENU=Created On;
                                                              PTG=Criado Em];
                                                   Description=Date and time when the team was created. }
    { 8   ;   ;ModifiedOn          ;DateTime      ;ExternalName=modifiedon;
                                                   ExternalType=DateTime;
                                                   ExternalAccess=Read;
                                                   CaptionML=[ENU=Modified On;
                                                              PTG=Modificado Em];
                                                   Description=Date and time when the team was last modified. }
    { 9   ;   ;CreatedBy           ;GUID          ;TableRelation="CRM Systemuser".SystemUserId;
                                                   ExternalName=createdby;
                                                   ExternalType=Lookup;
                                                   ExternalAccess=Read;
                                                   CaptionML=[ENU=Created By;
                                                              PTG=Criado Por];
                                                   Description=Unique identifier of the user who created the team. }
    { 10  ;   ;ModifiedBy          ;GUID          ;TableRelation="CRM Systemuser".SystemUserId;
                                                   ExternalName=modifiedby;
                                                   ExternalType=Lookup;
                                                   ExternalAccess=Read;
                                                   CaptionML=[ENU=Modified By;
                                                              PTG=Modificado Por];
                                                   Description=Unique identifier of the user who last modified the team. }
    { 11  ;   ;VersionNumber       ;BigInteger    ;ExternalName=versionnumber;
                                                   ExternalType=BigInt;
                                                   ExternalAccess=Read;
                                                   CaptionML=[ENU=Version number;
                                                              PTG=N�mero Vers�o];
                                                   Description=Version number of the team. }
    { 12  ;   ;CreatedByName       ;Text200       ;FieldClass=FlowField;
                                                   CalcFormula=Lookup("CRM Systemuser".FullName WHERE (SystemUserId=FIELD(CreatedBy)));
                                                   ExternalName=createdbyname;
                                                   ExternalType=String;
                                                   ExternalAccess=Read;
                                                   CaptionML=[ENU=CreatedByName;
                                                              PTG=N�mero Vers�o] }
    { 13  ;   ;ModifiedByName      ;Text200       ;FieldClass=FlowField;
                                                   CalcFormula=Lookup("CRM Systemuser".FullName WHERE (SystemUserId=FIELD(ModifiedBy)));
                                                   ExternalName=modifiedbyname;
                                                   ExternalType=String;
                                                   ExternalAccess=Read;
                                                   CaptionML=[ENU=ModifiedByName;
                                                              PTG=ModificadoPorNome] }
    { 14  ;   ;BusinessUnitIdName  ;Text160       ;FieldClass=FlowField;
                                                   CalcFormula=Lookup("CRM Businessunit".Name WHERE (BusinessUnitId=FIELD(BusinessUnitId)));
                                                   ExternalName=businessunitidname;
                                                   ExternalType=String;
                                                   ExternalAccess=Read;
                                                   CaptionML=[ENU=BusinessUnitIdName;
                                                              PTG=Nome ID Unidade Neg�cio] }
    { 15  ;   ;ImportSequenceNumber;Integer       ;ExternalName=importsequencenumber;
                                                   ExternalType=Integer;
                                                   ExternalAccess=Insert;
                                                   CaptionML=[ENU=Import Sequence Number;
                                                              PTG=Importar N�mero Sequ�ncia];
                                                   Description=Unique identifier of the data import or data migration that created this record. }
    { 16  ;   ;OverriddenCreatedOn ;Date          ;ExternalName=overriddencreatedon;
                                                   ExternalType=DateTime;
                                                   ExternalAccess=Insert;
                                                   CaptionML=[ENU=Record Created On;
                                                              PTG=Registo Criado Em];
                                                   Description=Date and time that the record was migrated. }
    { 17  ;   ;AdministratorId     ;GUID          ;TableRelation="CRM Systemuser".SystemUserId;
                                                   ExternalName=administratorid;
                                                   ExternalType=Lookup;
                                                   CaptionML=[ENU=Administrator;
                                                              PTG=Administrador];
                                                   Description=Unique identifier of the user primary responsible for the team. }
    { 18  ;   ;IsDefault           ;Boolean       ;ExternalName=isdefault;
                                                   ExternalType=Boolean;
                                                   ExternalAccess=Read;
                                                   CaptionML=[ENU=Is Default;
                                                              PTG=� Pr�-Definido];
                                                   Description=Information about whether the team is a default business unit team. }
    { 19  ;   ;AdministratorIdName ;Text200       ;FieldClass=FlowField;
                                                   CalcFormula=Lookup("CRM Systemuser".FullName WHERE (SystemUserId=FIELD(AdministratorId)));
                                                   ExternalName=administratoridname;
                                                   ExternalType=String;
                                                   ExternalAccess=Read;
                                                   CaptionML=[ENU=AdministratorIdName;
                                                              PTG=NomeIDAdminstrador] }
    { 20  ;   ;YomiName            ;Text160       ;ExternalName=yominame;
                                                   ExternalType=String;
                                                   CaptionML=[ENU=Yomi Name;
                                                              PTG=Nome Yomi];
                                                   Description=Pronunciation of the full name of the team, written in phonetic hiragana or katakana characters. }
    { 21  ;   ;CreatedOnBehalfBy   ;GUID          ;TableRelation="CRM Systemuser".SystemUserId;
                                                   ExternalName=createdonbehalfby;
                                                   ExternalType=Lookup;
                                                   ExternalAccess=Read;
                                                   CaptionML=[ENU=Created By (Delegate);
                                                              PTG=Criado Por (Delegado)];
                                                   Description=Unique identifier of the delegate user who created the team. }
    { 22  ;   ;CreatedOnBehalfByName;Text200      ;FieldClass=FlowField;
                                                   CalcFormula=Lookup("CRM Systemuser".FullName WHERE (SystemUserId=FIELD(CreatedOnBehalfBy)));
                                                   ExternalName=createdonbehalfbyname;
                                                   ExternalType=String;
                                                   ExternalAccess=Read;
                                                   CaptionML=[ENU=CreatedOnBehalfByName;
                                                              PTG=CriadoEmNomePorNome] }
    { 23  ;   ;ModifiedOnBehalfBy  ;GUID          ;TableRelation="CRM Systemuser".SystemUserId;
                                                   ExternalName=modifiedonbehalfby;
                                                   ExternalType=Lookup;
                                                   ExternalAccess=Read;
                                                   CaptionML=[ENU=Modified By (Delegate);
                                                              PTG=Modificado Por (Delegado)];
                                                   Description=Unique identifier of the delegate user who last modified the team. }
    { 24  ;   ;ModifiedOnBehalfByName;Text200     ;FieldClass=FlowField;
                                                   CalcFormula=Lookup("CRM Systemuser".FullName WHERE (SystemUserId=FIELD(ModifiedOnBehalfBy)));
                                                   ExternalName=modifiedonbehalfbyname;
                                                   ExternalType=String;
                                                   ExternalAccess=Read;
                                                   CaptionML=[ENU=ModifiedOnBehalfByName;
                                                              PTG=ModificadoEmNomePorNome] }
    { 25  ;   ;TraversedPath       ;Text250       ;ExternalName=traversedpath;
                                                   ExternalType=String;
                                                   CaptionML=[ENU=Traversed Path;
                                                              PTG=Endere�o Cruzado];
                                                   Description=For internal use only. }
    { 26  ;   ;TransactionCurrencyId;GUID         ;TableRelation="CRM Transactioncurrency".TransactionCurrencyId;
                                                   ExternalName=transactioncurrencyid;
                                                   ExternalType=Lookup;
                                                   CaptionML=[ENU=Currency;
                                                              PTG=Divisa];
                                                   Description=Unique identifier of the currency associated with the team. }
    { 27  ;   ;TransactionCurrencyIdName;Text100  ;FieldClass=FlowField;
                                                   CalcFormula=Lookup("CRM Transactioncurrency".CurrencyName WHERE (TransactionCurrencyId=FIELD(TransactionCurrencyId)));
                                                   ExternalName=transactioncurrencyidname;
                                                   ExternalType=String;
                                                   ExternalAccess=Read;
                                                   CaptionML=[ENU=TransactionCurrencyIdName;
                                                              PTG=NomeIDDivisaTransa��o] }
    { 28  ;   ;ExchangeRate        ;Decimal       ;ExternalName=exchangerate;
                                                   ExternalType=Decimal;
                                                   ExternalAccess=Read;
                                                   CaptionML=[ENU=Exchange Rate;
                                                              PTG=Taxa C�mbio];
                                                   Description=Exchange rate for the currency associated with the team with respect to the base currency. }
    { 29  ;   ;TeamType            ;Option        ;InitValue=Owner;
                                                   ExternalName=teamtype;
                                                   ExternalType=Picklist;
                                                   ExternalAccess=Insert;
                                                   OptionOrdinalValues=[0;1];
                                                   CaptionML=[ENU=Team Type;
                                                              PTG=Tipo Equipa];
                                                   OptionCaptionML=[ENU=Owner,Access;
                                                                    PTG=Propriet�rio,Acesso];
                                                   OptionString=Owner,Access;
                                                   Description=Select the team type. }
    { 30  ;   ;RegardingObjectId   ;GUID          ;TableRelation=IF (RegardingObjectTypeCode=CONST(opportunity)) "CRM Opportunity".OpportunityId;
                                                   ExternalName=regardingobjectid;
                                                   ExternalType=Lookup;
                                                   ExternalAccess=Insert;
                                                   CaptionML=[ENU=Regarding Object Id;
                                                              PTG=ID Objeto Relativo];
                                                   Description=Choose the record that the team relates to. }
    { 31  ;   ;SystemManaged       ;Boolean       ;ExternalName=systemmanaged;
                                                   ExternalType=Boolean;
                                                   ExternalAccess=Read;
                                                   CaptionML=[ENU=Is System Managed;
                                                              PTG=� Sistema Controlado];
                                                   Description=Select whether the team will be managed by the system. }
    { 32  ;   ;RegardingObjectTypeCode;Option     ;ExternalName=regardingobjecttypecode;
                                                   ExternalType=EntityName;
                                                   ExternalAccess=Insert;
                                                   CaptionML=[ENU=Regarding Object Type;
                                                              PTG=Tipo Objeto Relativo];
                                                   OptionCaptionML=[ENU=" ,opportunity";
                                                                    PTG=" ,oportunidade"];
                                                   OptionString=[ ,opportunity];
                                                   Description=Type of the associated record for team - used for system managed access teams only. }
    { 33  ;   ;StageId             ;GUID          ;ExternalName=stageid;
                                                   ExternalType=Uniqueidentifier;
                                                   CaptionML=[ENU=Process Stage;
                                                              PTG=Etapa Processo];
                                                   Description=Shows the ID of the stage. }
    { 34  ;   ;ProcessId           ;GUID          ;ExternalName=processid;
                                                   ExternalType=Uniqueidentifier;
                                                   CaptionML=[ENU=Process;
                                                              PTG=Processo];
                                                   Description=Shows the ID of the process. }
  }
  KEYS
  {
    {    ;TeamId                                  ;Clustered=Yes }
    {    ;Name                                     }
  }
  FIELDGROUPS
  {
    { 1   ;DropDown            ;Name                                     }
  }
  CODE
  {

    BEGIN
    {
      Dynamics CRM Version: 7.1.0.2040
    }
    END.
  }
}

