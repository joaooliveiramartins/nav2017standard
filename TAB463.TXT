OBJECT Table 463 Shipment Method Translation
{
  OBJECT-PROPERTIES
  {
    Date=05/08/11;
    Time=12:00:00;
    Version List=NAVW16.00;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Shipment Method Translation;
               PTG=Tradu��o M�todo Envio];
  }
  FIELDS
  {
    { 1   ;   ;Shipment Method     ;Code10        ;TableRelation="Shipment Method";
                                                   CaptionML=[ENU=Shipment Method;
                                                              PTG=Condi��es Envio] }
    { 2   ;   ;Language Code       ;Code10        ;TableRelation=Language;
                                                   CaptionML=[ENU=Language Code;
                                                              PTG=C�d. Idioma] }
    { 3   ;   ;Description         ;Text50        ;CaptionML=[ENU=Description;
                                                              PTG=Descri��o] }
  }
  KEYS
  {
    {    ;Shipment Method,Language Code           ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    END.
  }
}

