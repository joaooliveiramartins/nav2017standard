OBJECT Report 9802 Copy Permission Set
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Copy Permission Set;
               PTG=Copiar Conjunto Permiss�es];
    ProcessingOnly=Yes;
  }
  DATASET
  {
    { 1   ;    ;DataItem;PermissionSet       ;
               DataItemTable=Table2000000004;
               DataItemTableView=SORTING(Role ID);
               OnAfterGetRecord=VAR
                                  NewPermissionSet@1000 : Record 2000000004;
                                  Permission@1001 : Record 2000000005;
                                  NewPermission@1002 : Record 2000000005;
                                BEGIN
                                  NewPermissionSet.INIT;
                                  NewPermissionSet.VALIDATE("Role ID",NewRoleId);
                                  NewPermissionSet.TESTFIELD("Role ID");
                                  NewPermissionSet.VALIDATE(Name,Name);
                                  NewPermissionSet.INSERT;

                                  Permission.SETRANGE("Role ID","Role ID");
                                  IF Permission.FINDSET THEN
                                    REPEAT
                                      NewPermission.INIT;
                                      NewPermission.COPY(Permission);
                                      NewPermission."Role ID" := NewRoleId;
                                      NewPermission.INSERT;
                                    UNTIL Permission.NEXT = 0;
                                END;
                                 }

  }
  REQUESTPAGE
  {
    PROPERTIES
    {
    }
    CONTROLS
    {
      { 3   ;0   ;Container ;
                  ContainerType=ContentArea }

      { 1   ;1   ;Group     ;
                  CaptionML=[ENU=Options;
                             PTG=Op��es];
                  GroupType=Group }

      { 2   ;2   ;Field     ;
                  Name=NewRoleId;
                  CaptionML=[ENU=New Permission Set;
                             PTG=Novo Conjunto de Permiss�es];
                  ToolTipML=[ENU=Specifies the name of the new permission set after copying.;
                             PTG=Especifica o nome do novo conjnto de permiss�o depois da c�pia.];
                  ApplicationArea=#Basic,#Suite;
                  NotBlank=Yes;
                  SourceExpr=NewRoleId }

    }
  }
  LABELS
  {
  }
  CODE
  {
    VAR
      NewRoleId@1000 : Code[20];

    PROCEDURE GetRoleId@3() : Code[20];
    BEGIN
      EXIT(NewRoleId);
    END;

    BEGIN
    END.
  }
  RDLDATA
  {
  }
}

