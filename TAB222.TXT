OBJECT Table 222 Ship-to Address
{
  OBJECT-PROPERTIES
  {
    Date=16/06/17;
    Time=18:43:19;
    Modified=Yes;
    Version List=NAVW110.0;
  }
  PROPERTIES
  {
    DataCaptionFields=Customer No.,Name,Code;
    OnInsert=BEGIN
               Cust.GET("Customer No.");
               Name := Cust.Name;
             END;

    OnModify=BEGIN
               "Last Date Modified" := TODAY;
             END;

    OnRename=BEGIN
               "Last Date Modified" := TODAY;
             END;

    CaptionML=[ENU=Ship-to Address;
               PTG=Envio-a Endere�o];
    LookupPageID=Page301;
  }
  FIELDS
  {
    { 1   ;   ;Customer No.        ;Code20        ;TableRelation=Customer;
                                                   CaptionML=[ENU=Customer No.;
                                                              PTG=N� Cliente];
                                                   NotBlank=Yes }
    { 2   ;   ;Code                ;Code10        ;CaptionML=[ENU=Code;
                                                              PTG=C�digo];
                                                   NotBlank=Yes }
    { 3   ;   ;Name                ;Text60        ;CaptionML=[ENU=Name;
                                                              PTG=Nome] }
    { 4   ;   ;Name 2              ;Text50        ;CaptionML=[ENU=Name 2;
                                                              PTG=Nome 2] }
    { 5   ;   ;Address             ;Text60        ;CaptionML=[ENU=Address;
                                                              PTG=Endere�o] }
    { 6   ;   ;Address 2           ;Text60        ;CaptionML=[ENU=Address 2;
                                                              PTG=Endere�o 2] }
    { 7   ;   ;City                ;Text30        ;TableRelation=IF (Country/Region Code=CONST()) "Post Code".City
                                                                 ELSE IF (Country/Region Code=FILTER(<>'')) "Post Code".City WHERE (Country/Region Code=FIELD(Country/Region Code));
                                                   OnValidate=BEGIN
                                                                PostCode.ValidateCity(City,"Post Code",County,"Country/Region Code",(CurrFieldNo <> 0) AND GUIALLOWED);
                                                              END;

                                                   ValidateTableRelation=No;
                                                   TestTableRelation=No;
                                                   CaptionML=[ENU=City;
                                                              PTG=Cidade] }
    { 8   ;   ;Contact             ;Text50        ;CaptionML=[ENU=Contact;
                                                              PTG=Contacto] }
    { 9   ;   ;Phone No.           ;Text30        ;ExtendedDatatype=Phone No.;
                                                   CaptionML=[ENU=Phone No.;
                                                              PTG=Telefone] }
    { 10  ;   ;Telex No.           ;Text30        ;CaptionML=[ENU=Telex No.;
                                                              PTG=N� Telex] }
    { 30  ;   ;Shipment Method Code;Code10        ;TableRelation="Shipment Method";
                                                   CaptionML=[ENU=Shipment Method Code;
                                                              PTG=C�d. Condi��es Envio] }
    { 31  ;   ;Shipping Agent Code ;Code10        ;TableRelation="Shipping Agent";
                                                   OnValidate=BEGIN
                                                                IF "Shipping Agent Code" <> xRec."Shipping Agent Code" THEN
                                                                  VALIDATE("Shipping Agent Service Code",'');
                                                              END;

                                                   AccessByPermission=TableData 5790=R;
                                                   CaptionML=[ENU=Shipping Agent Code;
                                                              PTG=C�d. Transportador] }
    { 32  ;   ;Place of Export     ;Code20        ;CaptionML=[ENU=Place of Export;
                                                              PTG=Lugar de Sa�da] }
    { 35  ;   ;Country/Region Code ;Code10        ;TableRelation=Country/Region;
                                                   CaptionML=[ENU=Country/Region Code;
                                                              PTG=C�d. Pa�s/Regi�o] }
    { 54  ;   ;Last Date Modified  ;Date          ;CaptionML=[ENU=Last Date Modified;
                                                              PTG=Data �ltima Modif.];
                                                   Editable=No }
    { 83  ;   ;Location Code       ;Code10        ;TableRelation=Location WHERE (Use As In-Transit=CONST(No));
                                                   CaptionML=[ENU=Location Code;
                                                              PTG=C�d. Localiza��o] }
    { 84  ;   ;Fax No.             ;Text30        ;CaptionML=[ENU=Fax No.;
                                                              PTG=N� Fax] }
    { 85  ;   ;Telex Answer Back   ;Text20        ;CaptionML=[ENU=Telex Answer Back;
                                                              PTG=N� Telex Resposta] }
    { 91  ;   ;Post Code           ;Code20        ;TableRelation=IF (Country/Region Code=CONST()) "Post Code"
                                                                 ELSE IF (Country/Region Code=FILTER(<>'')) "Post Code" WHERE (Country/Region Code=FIELD(Country/Region Code));
                                                   OnValidate=BEGIN
                                                                PostCode.ValidatePostCode(City,"Post Code",County,"Country/Region Code",(CurrFieldNo <> 0) AND GUIALLOWED);
                                                              END;

                                                   ValidateTableRelation=No;
                                                   TestTableRelation=No;
                                                   CaptionML=[ENU=Post Code;
                                                              PTG=C�d. Postal] }
    { 92  ;   ;County              ;Text30        ;CaptionML=[ENU=County;
                                                              PTG=Distrito] }
    { 102 ;   ;E-Mail              ;Text80        ;ExtendedDatatype=E-Mail;
                                                   CaptionML=[ENU=Email;
                                                              PTG=Email] }
    { 103 ;   ;Home Page           ;Text80        ;ExtendedDatatype=URL;
                                                   CaptionML=[ENU=Home Page;
                                                              PTG=Home Page] }
    { 108 ;   ;Tax Area Code       ;Code20        ;TableRelation="Tax Area";
                                                   CaptionML=[ENU=Tax Area Code;
                                                              PTG=C�d. �rea Imposto] }
    { 109 ;   ;Tax Liable          ;Boolean       ;CaptionML=[ENU=Tax Liable;
                                                              PTG=Sujeito a Imposto] }
    { 5792;   ;Shipping Agent Service Code;Code10 ;TableRelation="Shipping Agent Services".Code WHERE (Shipping Agent Code=FIELD(Shipping Agent Code));
                                                   CaptionML=[ENU=Shipping Agent Service Code;
                                                              PTG=C�d. Servi�o Transportador] }
    { 5900;   ;Service Zone Code   ;Code10        ;TableRelation="Service Zone";
                                                   CaptionML=[ENU=Service Zone Code;
                                                              PTG=C�d. Zona Servi�o] }
  }
  KEYS
  {
    {    ;Customer No.,Code                       ;Clustered=Yes }
  }
  FIELDGROUPS
  {
    { 1   ;DropDown            ;Code,Name,Address,City,Post Code         }
  }
  CODE
  {
    VAR
      Text000@1000 : TextConst 'ENU=untitled;PTG=SemT�tulo';
      Cust@1001 : Record 18;
      PostCode@1002 : Record 225;
      Text001@1003 : TextConst 'ENU=Before you can use Online Map, you must fill in the Online Map Setup window.\See Setting Up Online Map in Help.;PTG=Para utilizar o Online Map, tem de preencher a janela Configura��o Online Map.\Consulte Configurar o Online Map na Ajuda.';

    PROCEDURE Caption@1() : Text[130];
    BEGIN
      IF "Customer No." = '' THEN
        EXIT(Text000);
      Cust.GET("Customer No.");
      EXIT(STRSUBSTNO('%1 %2 %3 %4',Cust."No.",Cust.Name,Code,Name));
    END;

    PROCEDURE DisplayMap@8();
    VAR
      MapPoint@1001 : Record 800;
      MapMgt@1000 : Codeunit 802;
    BEGIN
      IF MapPoint.FINDFIRST THEN
        MapMgt.MakeSelection(DATABASE::"Ship-to Address",GETPOSITION)
      ELSE
        MESSAGE(Text001);
    END;

    PROCEDURE GetFilterCustNo@2() : Code[20];
    BEGIN
      IF GETFILTER("Customer No.") <> '' THEN
        IF GETRANGEMIN("Customer No.") = GETRANGEMAX("Customer No.") THEN
          EXIT(GETRANGEMAX("Customer No."));
    END;

    BEGIN
    {
      SGG10.00 PFILIPE 01-09-06 Alterados os tamanhos dos campos "Name", "Address", "Address 2" para 60
    }
    END.
  }
}

