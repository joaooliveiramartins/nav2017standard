OBJECT Table 99000854 Inventory Profile Track Buffer
{
  OBJECT-PROPERTIES
  {
    Date=05/08/11;
    Time=12:00:00;
    Version List=NAVW16.00;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Inventory Profile Track Buffer;
               PTG=Mem. Interm. Rastreio Perfil Invent�rio];
  }
  FIELDS
  {
    { 1   ;   ;Line No.            ;Integer       ;CaptionML=[ENU=Line No.;
                                                              PTG=N� Linha] }
    { 2   ;   ;Priority            ;Integer       ;CaptionML=[ENU=Priority;
                                                              PTG=Prioridade] }
    { 3   ;   ;Demand Line No.     ;Integer       ;CaptionML=[ENU=Demand Line No.;
                                                              PTG=N� Linha de Procura] }
    { 4   ;   ;Sequence No.        ;Integer       ;CaptionML=[ENU=Sequence No.;
                                                              PTG=N� Sequ�ncia] }
    { 21  ;   ;Source Type         ;Integer       ;CaptionML=[ENU=Source Type;
                                                              PTG=Tipo Origem] }
    { 23  ;   ;Source ID           ;Code20        ;CaptionML=[ENU=Source ID;
                                                              PTG=ID Origem] }
    { 72  ;   ;Quantity Tracked    ;Decimal       ;CaptionML=[ENU=Quantity Tracked;
                                                              PTG=Quantidade Rastreada] }
    { 73  ;   ;Surplus Type        ;Option        ;CaptionML=[ENU=Surplus Type;
                                                              PTG=Tipo Excedente];
                                                   OptionCaptionML=[ENU=None,Forecast,BlanketOrder,SafetyStock,ReorderPoint,MaxInventory,FixedOrderQty,MaxOrder,MinOrder,OrderMultiple,DampenerQty,PlanningFlexibility,Undefined,EmergencyOrder;
                                                                    PTG=Nenhum,Previs�o,EncomendaAberta,Invent�rioSeguran�a,PontoEncomenda,Invent�rioM�x,QtdEncomendaFixa,M�xEncomenda,M�nEncomenda,M�ltiplasEnc,QtdAmortecedor,FlexibilidadePlaneamento,N�oDefinido,EncomendaEmerg�ncia];
                                                   OptionString=None,Forecast,BlanketOrder,SafetyStock,ReorderPoint,MaxInventory,FixedOrderQty,MaxOrder,MinOrder,OrderMultiple,DampenerQty,PlanningFlexibility,Undefined,EmergencyOrder }
    { 75  ;   ;Warning Level       ;Option        ;CaptionML=[ENU=Warning Level;
                                                              PTG=N�vel Aviso];
                                                   OptionCaptionML=[ENU=,Emergency,Exception,Attention;
                                                                    PTG=,Emerg�ncia,Excep��o,Aten��o];
                                                   OptionString=,Emergency,Exception,Attention }
  }
  KEYS
  {
    {    ;Line No.,Priority,Demand Line No.,Sequence No.;
                                                   SumIndexFields=Quantity Tracked;
                                                   MaintainSIFTIndex=No;
                                                   Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    END.
  }
}

