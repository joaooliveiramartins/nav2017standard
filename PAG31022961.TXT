OBJECT Page 31022961 Closed Bill Groups List
{
  OBJECT-PROPERTIES
  {
    Date=21/12/16;
    Time=13:00:00;
    Version List=NAVPTSS10.00;
  }
  PROPERTIES
  {
    Editable=No;
    CaptionML=[ENU=Closed Bill Groups List;
               PTG=Lista Remessas Fechadas];
    SourceTable=Table31022940;
    PageType=List;
    CardPageID=Closed Bill Groups;
    ActionList=ACTIONS
    {
      { 1900000003;0 ;ActionContainer;
                      ActionContainerType=RelatedInformation }
      { 14      ;1   ;ActionGroup;
                      CaptionML=[ENU=&Bill Group;
                                 PTG=&Remessa];
                      Image=VoucherGroup }
      { 16      ;2   ;Action    ;
                      CaptionML=[ENU=Co&mments;
                                 PTG=Co&ment rios];
                      ApplicationArea=#Basic,#Suite;
                      RunObject=Page 31022969;
                      RunPageLink=BG/PO No.=FIELD(No.),
                                  Type=FILTER(Receivable);
                      Image=ViewComments }
      { 17      ;2   ;Separator  }
      { 18      ;2   ;Action    ;
                      CaptionML=[ENU=Analysis;
                                 PTG=An lise];
                      ApplicationArea=#Basic,#Suite;
                      RunObject=Page 31022966;
                      RunPageLink=No.=FIELD(No.),
                                  Global Dimension 1 Filter=FIELD(Global Dimension 1 Filter),
                                  Global Dimension 2 Filter=FIELD(Global Dimension 2 Filter);
                      Image=Report }
      { 19      ;2   ;Separator  }
      { 20      ;2   ;Action    ;
                      Ellipsis=Yes;
                      CaptionML=[ENU=Listing;
                                 PTG=Listagem];
                      ApplicationArea=#Basic,#Suite;
                      Promoted=Yes;
                      Image=List;
                      PromotedCategory=Report;
                      OnAction=BEGIN
                                 IF FIND THEN BEGIN
                                   ClosedBillGr.COPY(Rec);
                                   ClosedBillGr.SETRECFILTER;
                                   REPORT.RUN(REPORT::"Closed Bill Group Listing",TRUE,FALSE,ClosedBillGr);
                                 END;
                               END;
                                }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 2   ;2   ;Field     ;
                SourceExpr="No." }

    { 6   ;2   ;Field     ;
                SourceExpr="Bank Account No." }

    { 4   ;2   ;Field     ;
                SourceExpr="Bank Account Name" }

    { 8   ;2   ;Field     ;
                SourceExpr="Currency Code" }

    { 9   ;2   ;Field     ;
                SourceExpr="Amount Grouped" }

    { 12  ;2   ;Field     ;
                SourceExpr="Amount Grouped (LCY)" }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1907056207;1;Part   ;
                SubPageLink=No.=FIELD(No.);
                PagePartID=Page31023021;
                Visible=TRUE;
                PartType=Page }

    { 1907056307;1;Part   ;
                SubPageLink=No.=FIELD(No.);
                PagePartID=Page31023022;
                Visible=FALSE;
                PartType=Page }

    { 1905767507;1;Part   ;
                Visible=TRUE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {
    VAR
      ClosedBillGr@1110000 : Record 31022940;

    BEGIN
    END.
  }
}

