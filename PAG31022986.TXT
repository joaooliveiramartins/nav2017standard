OBJECT Page 31022986 Report Selection - Cartera
{
  OBJECT-PROPERTIES
  {
    Date=01/02/15;
    Time=13:00:00;
    Version List=NAVPTSS81.00;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Report Selection - Cartera;
               PTG=Sele��o Mapa - Carteira];
    SaveValues=Yes;
    SourceTable=Table31022946;
    PageType=Worksheet;
    OnOpenPage=BEGIN
                 SetUsageFilter;
               END;

    OnAfterGetRecord=BEGIN
                       ObjTransl.TranslateObject(ObjTransl."Object Type"::Report,"Report ID");
                     END;

    OnNewRecord=BEGIN
                  NewRecord;
                END;

  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 11  ;1   ;Field     ;
                CaptionML=[ENU=Usage;
                           PTG=Utiliza��o];
                SourceExpr=ReportUsage2;
                OnValidate=BEGIN
                             SetUsageFilter;
                             ReportUsage2OnAfterValidate;
                           END;
                            }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 2   ;2   ;Field     ;
                SourceExpr=Sequence }

    { 9   ;2   ;Field     ;
                SourceExpr="Report ID";
                LookupPageID=Objects }

    { 4   ;2   ;Field     ;
                DrillDown=No;
                SourceExpr="Report Name" }

  }
  CODE
  {
    VAR
      ReportUsage2@1110000 : 'Bill Group,Posted Bill Group,Closed Bill Group,Bill,Bill Group - Test,Payment Order,Posted Payment Order,Payment Order - Test,Closed Payment Order';
      ObjTransl@1110001 : Record 377;

    LOCAL PROCEDURE SetUsageFilter@1();
    BEGIN
      FILTERGROUP(2);
      CASE ReportUsage2 OF
        ReportUsage2::"Bill Group":
          SETRANGE(Usage,Usage::"Bill Group");
        ReportUsage2::"Posted Bill Group":
          SETRANGE(Usage,Usage::"Posted Bill Group");
        ReportUsage2::"Closed Bill Group":
          SETRANGE(Usage,Usage::"Closed Bill Group");
        ReportUsage2::Bill:
          SETRANGE(Usage,Usage::Bill);
        ReportUsage2::"Bill Group - Test":
          SETRANGE(Usage,Usage::"Bill Group - Test");
        ReportUsage2::"Payment Order":
          SETRANGE(Usage,Usage::"Payment Order");
        ReportUsage2::"Posted Payment Order":
          SETRANGE(Usage,Usage::"Posted Payment Order");
        ReportUsage2::"Payment Order - Test":
          SETRANGE(Usage,Usage::"Payment Order - Test");
        ReportUsage2::"Closed Payment Order":
          SETRANGE(Usage,Usage::"Closed Payment Order");
      END;
      FILTERGROUP(0);
    END;

    LOCAL PROCEDURE ReportUsage2OnAfterValidate@19038799();
    BEGIN
      CurrPage.UPDATE;
    END;

    BEGIN
    END.
  }
}

