OBJECT Table 7600 Base Calendar
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0;
  }
  PROPERTIES
  {
    DataCaptionFields=Code,Name;
    OnDelete=VAR
               CustomizedCalendarChange@1000 : Record 7602;
             BEGIN
               CustomizedCalendarChange.SETRANGE("Base Calendar Code",Code);
               IF NOT CustomizedCalendarChange.ISEMPTY THEN
                 ERROR(Text001,Code);

               BaseCalendarLine.RESET;
               BaseCalendarLine.SETRANGE("Base Calendar Code",Code);
               BaseCalendarLine.DELETEALL;
             END;

    CaptionML=[ENU=Base Calendar;
               PTG=Calend�rio Base];
    LookupPageID=Page7601;
  }
  FIELDS
  {
    { 1   ;   ;Code                ;Code10        ;CaptionML=[ENU=Code;
                                                              PTG=C�digo];
                                                   NotBlank=Yes }
    { 2   ;   ;Name                ;Text30        ;CaptionML=[ENU=Name;
                                                              PTG=Nome] }
    { 3   ;   ;Customized Changes Exist;Boolean   ;FieldClass=FlowField;
                                                   CalcFormula=Exist("Customized Calendar Change" WHERE (Base Calendar Code=FIELD(Code)));
                                                   CaptionML=[ENU=Customized Changes Exist;
                                                              PTG=Existem Altera��es Person.];
                                                   Editable=No }
  }
  KEYS
  {
    {    ;Code                                    ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {
    VAR
      BaseCalendarLine@1001 : Record 7601;
      Text001@1002 : TextConst 'ENU="You cannot delete this record. Customized calendar changes exist for calendar code=<%1>.";PTG="N�o pode eliminar este registo. Existem altera��es calend�rio personalizado para o c�d. calend�rio=<%1>."';

    BEGIN
    END.
  }
}

