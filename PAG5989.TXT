OBJECT Page 5989 Service Item Log
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0;
  }
  PROPERTIES
  {
    Editable=No;
    CaptionML=[ENU=Service Item Log;
               PTG=Altera��es Produto Servi�o];
    SourceTable=Table5942;
    DataCaptionExpr=GetCaptionHeader;
    SourceTableView=ORDER(Descending);
    PageType=List;
    OnInit=BEGIN
             ServiceItemNoVisible := TRUE;
           END;

    ActionList=ACTIONS
    {
      { 1900000004;0 ;ActionContainer;
                      ActionContainerType=ActionItems }
      { 16      ;1   ;ActionGroup;
                      CaptionML=[ENU=F&unctions;
                                 PTG=F&un��es];
                      Image=Action }
      { 20      ;2   ;Action    ;
                      Ellipsis=Yes;
                      CaptionML=[ENU=Delete Service Item Log;
                                 PTG=Eliminar Reg. Produto Servi�o];
                      Image=Delete;
                      OnAction=BEGIN
                                 REPORT.RUNMODAL(REPORT::"Delete Service Item Log",TRUE,FALSE,Rec);
                                 CurrPage.UPDATE;
                               END;
                                }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 2   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the number of the event associated with the service item.;
                           PTG=""];
                SourceExpr="Service Item No.";
                Visible=ServiceItemNoVisible;
                Editable=FALSE }

    { 14  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the number assigned to this entry.;
                           PTG=""];
                SourceExpr="Entry No.";
                Visible=FALSE }

    { 18  ;2   ;Field     ;
                CaptionML=[ENU=Description;
                           PTG=Descri��o];
                ToolTipML=[ENU=Specifies the description of the event regarding service item that has taken place.;
                           PTG=""];
                SourceExpr=ServLogMgt.ServItemEventDescription("Event No.");
                Editable=FALSE }

    { 4   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the value of the field modified after the event takes place.;
                           PTG=""];
                SourceExpr=After }

    { 6   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the previous value of the field, modified after the event takes place.;
                           PTG=""];
                SourceExpr=Before }

    { 22  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the document type of the service item associated with the event, such as contract, order, or quote.;
                           PTG=""];
                SourceExpr="Document Type" }

    { 17  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the document number of the event associated with the service item.;
                           PTG=""];
                SourceExpr="Document No." }

    { 8   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the date of the event.;
                           PTG=""];
                SourceExpr="Change Date" }

    { 10  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the time of the event.;
                           PTG=""];
                SourceExpr="Change Time" }

    { 12  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the user ID linked to this entry.;
                           PTG=""];
                SourceExpr="User ID" }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {
    VAR
      ServLogMgt@1000 : Codeunit 5906;
      ServiceItemNoVisible@19053939 : Boolean INDATASET;

    LOCAL PROCEDURE GetCaptionHeader@1() : Text[250];
    VAR
      ServItem@1000 : Record 5940;
    BEGIN
      IF GETFILTER("Service Item No.") <> '' THEN BEGIN
        ServiceItemNoVisible := FALSE;
        IF ServItem.GET("Service Item No.") THEN
          EXIT("Service Item No." + ' ' + ServItem.Description);

        EXIT("Service Item No.");
      END;

      ServiceItemNoVisible := TRUE;
      EXIT('');
    END;

    BEGIN
    END.
  }
}

