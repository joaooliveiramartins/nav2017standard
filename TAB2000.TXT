OBJECT Table 2000 Time Series Buffer
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Time Series Buffer;
               PTG="Mem�ria Interna S�ries Tempo "];
  }
  FIELDS
  {
    { 1   ;   ;Group ID            ;Code50        ;CaptionML=[ENU=Group ID;
                                                              PTG=ID Grupo] }
    { 2   ;   ;Period No.          ;Integer       ;CaptionML=[ENU=Period No.;
                                                              PTG=N� Per�odo] }
    { 3   ;   ;Period Start Date   ;Date          ;CaptionML=[ENU=Period Start Date;
                                                              PTG=Data In�cio Per�odo] }
    { 4   ;   ;Value               ;Decimal       ;CaptionML=[ENU=Value;
                                                              PTG=Valor] }
  }
  KEYS
  {
    {    ;Group ID,Period No.                     ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    END.
  }
}

