OBJECT Table 2000000197 Token Cache
{
  OBJECT-PROPERTIES
  {
    Date=13/04/17;
    Time=14:36:29;
    Version List=NAVW110.0;
  }
  PROPERTIES
  {
    DataPerCompany=No;
    CaptionML=[ENU=Token Cache;
               PTG=Cache Token];
  }
  FIELDS
  {
    { 1   ;   ;User Security ID    ;GUID          ;TableRelation=User."User Security ID";
                                                   TestTableRelation=No;
                                                   CaptionML=[ENU=User Security ID;
                                                              PTG=ID Seguran�a Utilizador] }
    { 2   ;   ;User Unique ID      ;GUID          ;CaptionML=[ENU=User Unique ID;
                                                              PTG=ID �nico Utilizador] }
    { 3   ;   ;Tenant ID           ;GUID          ;CaptionML=[ENU=Tenant ID;
                                                              PTG=ID Tenant] }
    { 4   ;   ;Cache Write Time    ;DateTime      ;CaptionML=[ENU=Cache Write Time;
                                                              PTG=Hora de Escrita Cache] }
    { 5   ;   ;Cache Data          ;BLOB          ;CaptionML=[ENU=Cache Data;
                                                              PTG=Dados Cache] }
  }
  KEYS
  {
    {    ;User Security ID                        ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    END.
  }
}

