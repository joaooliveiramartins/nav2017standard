OBJECT Page 5138 Duplicate Search String Setup
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Duplicate Search String Setup;
               PTG=Conf. Texto Busca Duplicados];
    SourceTable=Table5095;
    PageType=List;
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 2   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the name of the field on which the search string is based. There are eight options: Name, Name 2, Address, Address 2, Post Code, City, Phone No., and VAT Registration No.;
                           PTG=""];
                ApplicationArea=#RelationshipMgmt;
                SourceExpr=Field }

    { 4   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the part of the field to use to generate the search string. There are two options: First and Last.;
                           PTG=""];
                ApplicationArea=#RelationshipMgmt;
                SourceExpr="Part of Field" }

    { 6   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies how many characters the search string will contain. You can enter a number from 2 to 10. The program automatically enters 5 as a default value.;
                           PTG=""];
                ApplicationArea=#RelationshipMgmt;
                SourceExpr=Length }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {

    BEGIN
    END.
  }
}

