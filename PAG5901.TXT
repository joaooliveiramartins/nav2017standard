OBJECT Page 5901 Service List
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0;
  }
  PROPERTIES
  {
    Editable=No;
    CaptionML=[ENU=Service List;
               PTG=Lista Servi�o];
    SourceTable=Table5900;
    DataCaptionFields=Document Type,No.;
    PageType=List;
    OnInit=BEGIN
             ResponseTimeVisible := TRUE;
             ResponseDateVisible := TRUE;
           END;

    OnOpenPage=BEGIN
                 IF "Document Type" = "Document Type"::Order THEN BEGIN
                   ResponseDateVisible := TRUE;
                   ResponseTimeVisible := TRUE;
                 END ELSE BEGIN
                   ResponseDateVisible := FALSE;
                   ResponseTimeVisible := FALSE;
                 END;

                 CopyCustomerFilter;
               END;

    ActionList=ACTIONS
    {
      { 1900000003;0 ;ActionContainer;
                      ActionContainerType=RelatedInformation }
      { 8       ;1   ;ActionGroup;
                      CaptionML=[ENU=&Line;
                                 PTG=&Linha];
                      Image=Line }
      { 29      ;2   ;Action    ;
                      ShortCutKey=Shift+F7;
                      CaptionML=[ENU=Card;
                                 PTG=Ficha];
                      Image=EditLines;
                      OnAction=VAR
                                 PageManagement@1000 : Codeunit 700;
                               BEGIN
                                 PageManagement.PageRun(Rec);
                               END;
                                }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 14  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the service order status, which reflects the repair or maintenance status of all service items on the service order.;
                           PTG=""];
                SourceExpr=Status }

    { 27  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the type of the service document on the line.;
                           PTG=""];
                SourceExpr="Document Type" }

    { 2   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the number of the service document you are creating.;
                           PTG=""];
                SourceExpr="No." }

    { 4   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the date when the service order was created.;
                           PTG=""];
                SourceExpr="Order Date" }

    { 6   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the time when the service order was created.;
                           PTG=""];
                SourceExpr="Order Time" }

    { 10  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the number of the customer who owns the items in the service document.;
                           PTG=""];
                SourceExpr="Customer No." }

    { 25  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the code for the customer' s address where you will ship the service.;
                           PTG=""];
                SourceExpr="Ship-to Code" }

    { 12  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the name of the customer to whom the items on the document will be shipped.;
                           PTG=""];
                SourceExpr=Name }

    { 9   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the code of the location (for example, warehouse or distribution center) of the items specified on the service item lines.;
                           PTG=""];
                SourceExpr="Location Code" }

    { 16  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the estimated date when work on the order should start, that is, when the service order status changes from Pending, to In Process.;
                           PTG=""];
                SourceExpr="Response Date";
                Visible=ResponseDateVisible }

    { 18  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the estimated time when work on the order starts, that is, when the service order status changes from Pending, to In Process.;
                           PTG=""];
                SourceExpr="Response Time";
                Visible=ResponseTimeVisible }

    { 23  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the priority of the service order.;
                           PTG=""];
                SourceExpr=Priority }

    { 121 ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the dimension value code for the dimension chosen as Global Dimension 1.;
                           PTG=""];
                SourceExpr="Shortcut Dimension 1 Code";
                Visible=FALSE }

    { 119 ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the dimension value code for the dimension chosen as Global Dimension 2.;
                           PTG=""];
                SourceExpr="Shortcut Dimension 2 Code";
                Visible=FALSE }

    { 31  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the ID of the user who is responsible for the document.;
                           PTG=""];
                SourceExpr="Assigned User ID" }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {
    VAR
      ResponseDateVisible@19066074 : Boolean INDATASET;
      ResponseTimeVisible@19074545 : Boolean INDATASET;

    BEGIN
    END.
  }
}

