OBJECT Report 292 Copy Sales Document
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0,NAVPTSS83.00;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Copy Sales Document;
               PTG=Copiar Documento Venda];
    ProcessingOnly=Yes;
    OnPreReport=BEGIN
                  SalesSetup.GET;
                  CopyDocMgt.SetProperties(
                    IncludeHeader,RecalculateLines,FALSE,FALSE,FALSE,SalesSetup."Exact Cost Reversing Mandatory",FALSE);

                  //soft,sn
                  CASE DocType OF
                    DocType::"Debit Memo":
                      BEGIN
                        IsDebitMemo := TRUE;
                        IsPostedDebitMemo := FALSE;
                        DocType := DocType::Invoice;
                      END;
                    DocType::"Posted Debit Memo":
                      BEGIN
                        IsDebitMemo := FALSE;
                        IsPostedDebitMemo := TRUE;
                        DocType := DocType::"Posted Invoice";
                      END;
                    ELSE BEGIN
                      IsDebitMemo := FALSE;
                      IsPostedDebitMemo := FALSE;
                    END;
                  END;
                  //soft,en

                  CopyDocMgt.CopySalesDoc(DocType,DocNo,SalesHeader);

                  //soft,sn
                  IF IsDebitMemo THEN
                    DocType := DocType::"Debit Memo";

                  IF IsPostedDebitMemo THEN
                    DocType := DocType::"Posted Debit Memo";
                  //soft,en
                END;

  }
  DATASET
  {
  }
  REQUESTPAGE
  {
    PROPERTIES
    {
      SaveValues=Yes;
      OnOpenPage=BEGIN
                   IF DocNo <> '' THEN BEGIN
                     CASE DocType OF
                       DocType::Quote:
                         IF FromSalesHeader.GET(FromSalesHeader."Document Type"::Quote,DocNo) THEN;
                       DocType::"Blanket Order":
                         IF FromSalesHeader.GET(FromSalesHeader."Document Type"::"Blanket Order",DocNo) THEN;
                       DocType::Order:
                         IF FromSalesHeader.GET(FromSalesHeader."Document Type"::Order,DocNo) THEN;
                       DocType::Invoice:
                         IF FromSalesHeader.GET(FromSalesHeader."Document Type"::Invoice,DocNo) THEN;
                       DocType::"Return Order":
                         IF FromSalesHeader.GET(FromSalesHeader."Document Type"::"Return Order",DocNo) THEN;
                       DocType::"Credit Memo":
                         IF FromSalesHeader.GET(FromSalesHeader."Document Type"::"Credit Memo",DocNo) THEN;
                       DocType::"Posted Shipment":
                         IF FromSalesShptHeader.GET(DocNo) THEN
                           FromSalesHeader.TRANSFERFIELDS(FromSalesShptHeader);
                       DocType::"Posted Invoice":
                         IF FromSalesInvHeader.GET(DocNo) THEN
                           FromSalesHeader.TRANSFERFIELDS(FromSalesInvHeader);
                       DocType::"Posted Return Receipt":
                         IF FromReturnRcptHeader.GET(DocNo) THEN
                           FromSalesHeader.TRANSFERFIELDS(FromReturnRcptHeader);
                       DocType::"Posted Credit Memo":
                         IF FromSalesCrMemoHeader.GET(DocNo) THEN
                           FromSalesHeader.TRANSFERFIELDS(FromSalesCrMemoHeader);
                       //soft,sn
                       DocType::"Debit Memo":
                         BEGIN
                           FromSalesHeader.RESET;
                           FromSalesHeader.SETRANGE("Document Type",FromSalesHeader."Document Type"::Invoice);
                           FromSalesHeader.SETRANGE("Debit Memo",TRUE);
                           IF FromSalesHeader.FINDFIRST THEN;
                         END;
                       DocType::"Posted Debit Memo":
                         BEGIN
                           FromSalesInvHeader.RESET;
                           IF FromSalesInvHeader.FINDFIRST THEN;
                           FromSalesHeader.TRANSFERFIELDS(FromSalesInvHeader);
                         END;
                       //soft,en
                     END;
                     IF FromSalesHeader."No." = '' THEN
                       DocNo := '';
                   END;
                   ValidateDocNo;
                 END;

    }
    CONTROLS
    {
      { 1900000001;0;Container;
                  ContainerType=ContentArea }

      { 1900000002;1;Group  ;
                  CaptionML=[ENU=Options;
                             PTG=Op��es] }

      { 3   ;2   ;Field     ;
                  Name=DocumentType;
                  CaptionML=[ENU=Document Type;
                             PTG=Tipo Documento];
                  ToolTipML=ENU=Specifies the type of document that is processed by the report or batch job.;
                  OptionCaptionML=[ENU=Quote,Blanket Order,Order,Invoice,Return Order,Credit Memo,Posted Shipment,Posted Invoice,Posted Return Receipt,Posted Credit Memo,Debit Memo,Posted Debit Memo;
                                   PTG=Proposta,Enc. Aberta,Encomenda,Fatura,Devolu��o,Nota Cr�dito,Envio Registado,Fatura Registada,Rec. Devol. Reg.,Nota Cr�dito Reg.,Nota D�bito,Nota D�bito Reg.];
                  ApplicationArea=#Suite;
                  SourceExpr=DocType;
                  OnValidate=BEGIN
                               DocNo := '';
                               ValidateDocNo;
                             END;
                              }

      { 8   ;2   ;Field     ;
                  Name=DocumentNo;
                  CaptionML=[ENU=Document No.;
                             PTG=N.� Documento];
                  ToolTipML=ENU=Specifies the number of the document that is processed by the report or batch job.;
                  ApplicationArea=#Suite;
                  SourceExpr=DocNo;
                  OnValidate=BEGIN
                               ValidateDocNo;
                             END;

                  OnLookup=BEGIN
                             LookupDocNo;
                           END;
                            }

      { 5   ;2   ;Field     ;
                  Name=SellToCustNo;
                  CaptionML=[ENU=Sell-to Customer No.;
                             PTG=Venda-a N.� Cliente];
                  ToolTipML=ENU=Specifies the sell-to customer number that will appear on the new sales document.;
                  ApplicationArea=#Suite;
                  SourceExpr=FromSalesHeader."Sell-to Customer No.";
                  Editable=FALSE }

      { 7   ;2   ;Field     ;
                  Name=SellToCustName;
                  CaptionML=[ENU=Sell-to Customer Name;
                             PTG=Venda-a Nome Cliente];
                  ToolTipML=ENU=Specifies the sell-to customer name that will appear on the new sales document.;
                  ApplicationArea=#Suite;
                  SourceExpr=FromSalesHeader."Sell-to Customer Name";
                  Editable=FALSE }

      { 1   ;2   ;Field     ;
                  Name=IncludeHeader_Options;
                  CaptionML=[ENU=Include Header;
                             PTG=Incluir Cabe�alho];
                  ToolTipML=ENU=Specifies if you also want to copy the information from the document header. When you copy quotes, if the posting date field of the new document is empty, the work date is used as the posting date of the new document.;
                  ApplicationArea=#Suite;
                  SourceExpr=IncludeHeader;
                  OnValidate=BEGIN
                               ValidateIncludeHeader;
                             END;
                              }

      { 4   ;2   ;Field     ;
                  Name=RecalculateLines;
                  CaptionML=[ENU=Recalculate Lines;
                             PTG=Recalcular Linhas];
                  ToolTipML=ENU=Specifies that lines are recalculate and inserted on the sales document you are creating. The batch job retains the item numbers and item quantities but recalculates the amounts on the lines based on the customer information on the new document header. In this way, the batch job accounts for item prices and discounts that are specifically linked to the customer on the new header.;
                  ApplicationArea=#Suite;
                  SourceExpr=RecalculateLines;
                  OnValidate=BEGIN
                               IF (DocType = DocType::"Posted Shipment") OR (DocType = DocType::"Posted Return Receipt") THEN
                                 RecalculateLines := TRUE;
                             END;
                              }

    }
  }
  LABELS
  {
  }
  CODE
  {
    VAR
      SalesHeader@1007 : Record 36;
      FromSalesHeader@1010 : Record 36;
      FromSalesShptHeader@1012 : Record 110;
      FromSalesInvHeader@1014 : Record 112;
      FromReturnRcptHeader@1016 : Record 6660;
      FromSalesCrMemoHeader@1018 : Record 114;
      SalesSetup@1003 : Record 311;
      CopyDocMgt@1008 : Codeunit 6620;
      DocType@1026 : 'Quote,Blanket Order,Order,Invoice,Return Order,Credit Memo,Posted Shipment,Posted Invoice,Posted Return Receipt,Posted Credit Memo,Debit Memo,Posted Debit Memo';
      DocNo@1027 : Code[20];
      IncludeHeader@1028 : Boolean;
      RecalculateLines@1029 : Boolean;
      Text000@1000 : TextConst 'ENU=The price information may not be reversed correctly, if you copy a %1. If possible copy a %2 instead or use %3 functionality.;PTG=As informa��es do pre�o poder�o n�o ser revertidas corretamente se copiar %1. Se poss�vel, em alternativa, copie %2 ou utilize a funcionalidade %3.';
      Text001@1001 : TextConst 'ENU=Undo Shipment;PTG=Anular Envio';
      Text002@1002 : TextConst 'ENU=Undo Return Receipt;PTG=Anular Rece��o Devolu��o';
      Text003@1004 : TextConst 'ENU=Quote,Blanket Order,Order,Invoice,Return Order,Credit Memo,Posted Shipment,Posted Invoice,Posted Return Receipt,Posted Credit Memo;PTG=Proposta,Enc. Aberta,Encomenda,Fatura,Devolu��o,Nota Cr�dito,G. Remessa Reg.,Fatura Registada,Rec. Devol. Reg.,Nota Cr�dito Reg.';
      IsDebitMemo@1005 : Boolean;
      IsPostedDebitMemo@1006 : Boolean;

    PROCEDURE SetSalesHeader@2(VAR NewSalesHeader@1000 : Record 36);
    BEGIN
      NewSalesHeader.TESTFIELD("No.");
      SalesHeader := NewSalesHeader;
    END;

    LOCAL PROCEDURE ValidateDocNo@10();
    VAR
      DocType2@1000 : 'Quote,Blanket Order,Order,Invoice,Return Order,Credit Memo,Posted Shipment,Posted Invoice,Posted Return Receipt,Posted Credit Memo,Debit Memo,Posted Debit Memo';
    BEGIN
      IF DocNo = '' THEN
        FromSalesHeader.INIT
      ELSE
        IF FromSalesHeader."No." = '' THEN BEGIN
          FromSalesHeader.INIT;
          CASE DocType OF
            DocType::Quote,
            DocType::"Blanket Order",
            DocType::Order,
            DocType::Invoice,
            DocType::"Return Order",
            DocType::"Credit Memo",
            DocType::"Debit Memo": //soft,n
              FromSalesHeader.GET(CopyDocMgt.SalesHeaderDocType(DocType),DocNo);
            DocType::"Posted Shipment":
              BEGIN
                FromSalesShptHeader.GET(DocNo);
                FromSalesHeader.TRANSFERFIELDS(FromSalesShptHeader);
                IF SalesHeader."Document Type" IN
                   [SalesHeader."Document Type"::"Return Order",SalesHeader."Document Type"::"Credit Memo"]
                THEN BEGIN
                  DocType2 := DocType2::"Posted Invoice";
                  MESSAGE(Text000,SELECTSTR(1 + DocType,Text003),SELECTSTR(1 + DocType2,Text003),Text001);
                END;
              END;
            DocType::"Posted Invoice":
              BEGIN
                FromSalesInvHeader.GET(DocNo);
                IF NOT FromSalesHeader."Debit Memo" THEN //soft,n
                FromSalesHeader.TRANSFERFIELDS(FromSalesInvHeader);
              END;
            DocType::"Posted Return Receipt":
              BEGIN
                FromReturnRcptHeader.GET(DocNo);
                FromSalesHeader.TRANSFERFIELDS(FromReturnRcptHeader);
                IF SalesHeader."Document Type" IN
                   [SalesHeader."Document Type"::Order,SalesHeader."Document Type"::Invoice]
                THEN BEGIN
                  DocType2 := DocType2::"Posted Credit Memo";
                  MESSAGE(Text000,SELECTSTR(1 + DocType,Text003),SELECTSTR(1 + DocType2,Text003),Text002);
                END;
              END;
            DocType::"Posted Credit Memo":
              BEGIN
                FromSalesCrMemoHeader.GET(DocNo);
                FromSalesHeader.TRANSFERFIELDS(FromSalesCrMemoHeader);
              END;

            //soft,sn
            DocType::"Posted Debit Memo":
              BEGIN
                FromSalesInvHeader.GET(DocNo);
                FromSalesHeader.TRANSFERFIELDS(FromSalesInvHeader);
              END;
            //soft,en

          END;
        END;
      FromSalesHeader."No." := '';

      IncludeHeader :=
        (DocType IN [DocType::"Posted Invoice",DocType::"Posted Credit Memo"]) AND
        ((DocType = DocType::"Posted Credit Memo") <>
         (SalesHeader."Document Type" IN
          [SalesHeader."Document Type"::"Return Order",SalesHeader."Document Type"::"Credit Memo"])) AND
        (SalesHeader."Bill-to Customer No." IN [FromSalesHeader."Bill-to Customer No.",'']);
      ValidateIncludeHeader;
    END;

    LOCAL PROCEDURE LookupDocNo@3();
    BEGIN
      CASE DocType OF
        DocType::Quote,
        DocType::"Blanket Order",
        DocType::Order,
        DocType::Invoice,
        DocType::"Return Order",
        DocType::"Credit Memo":
          BEGIN
            FromSalesHeader.FILTERGROUP := 0;
            FromSalesHeader.SETRANGE("Document Type",CopyDocMgt.SalesHeaderDocType(DocType));
            FromSalesHeader.SETRANGE("Debit Memo",FALSE); //soft,n
            IF SalesHeader."Document Type" = CopyDocMgt.SalesHeaderDocType(DocType) THEN
              FromSalesHeader.SETFILTER("No.",'<>%1',SalesHeader."No.");
            FromSalesHeader.FILTERGROUP := 2;
            FromSalesHeader."Document Type" := CopyDocMgt.SalesHeaderDocType(DocType);
            FromSalesHeader."No." := DocNo;
            IF (DocNo = '') AND (SalesHeader."Sell-to Customer No." <> '') THEN
              IF FromSalesHeader.SETCURRENTKEY("Document Type","Sell-to Customer No.") THEN BEGIN
                FromSalesHeader."Sell-to Customer No." := SalesHeader."Sell-to Customer No.";
                IF FromSalesHeader.FIND('=><') THEN;
              END;
            IF PAGE.RUNMODAL(0,FromSalesHeader) = ACTION::LookupOK THEN
              DocNo := FromSalesHeader."No.";
          END;
        //soft,sn
        DocType::"Debit Memo":
          BEGIN
            FromSalesHeader.RESET;
            FromSalesHeader.FILTERGROUP := 0;
            FromSalesHeader.SETRANGE("Document Type",SalesHeader."Document Type"::Invoice);
            FromSalesHeader.SETRANGE("Debit Memo",TRUE);
            IF SalesHeader."Document Type" = CopyDocMgt.SalesHeaderDocType(SalesHeader."Document Type"::Invoice) THEN
              FromSalesHeader.SETFILTER("No.",'<>%1',SalesHeader."No.");
            FromSalesHeader.FILTERGROUP := 2;
            FromSalesHeader."Document Type" := CopyDocMgt.SalesHeaderDocType(DocType);
            FromSalesHeader."No." := DocNo;
            IF (DocNo = '') AND (SalesHeader."Sell-to Customer No." <> '') THEN
              IF FromSalesHeader.SETCURRENTKEY("Document Type","Sell-to Customer No.") THEN BEGIN
                FromSalesHeader."Sell-to Customer No." := SalesHeader."Sell-to Customer No.";
                IF FromSalesHeader.FIND('=><') THEN;
              END;
            IF PAGE.RUNMODAL(PAGE::"Sales Debit Memo List",FromSalesHeader) = ACTION::LookupOK THEN
              DocNo := FromSalesHeader."No.";
          END;
        //soft,en

        DocType::"Posted Shipment":
          BEGIN
            FromSalesShptHeader."No." := DocNo;
            IF (DocNo = '') AND (SalesHeader."Sell-to Customer No." <> '') THEN
              IF FromSalesShptHeader.SETCURRENTKEY("Sell-to Customer No.") THEN BEGIN
                FromSalesShptHeader."Sell-to Customer No." := SalesHeader."Sell-to Customer No.";
                IF FromSalesShptHeader.FIND('=><') THEN;
              END;
            IF PAGE.RUNMODAL(0,FromSalesShptHeader) = ACTION::LookupOK THEN
              DocNo := FromSalesShptHeader."No.";
          END;
        DocType::"Posted Invoice":
          BEGIN
            FromSalesInvHeader.SETRANGE("Debit Memo",FALSE); //soft,n
            FromSalesInvHeader."No." := DocNo;
            IF (DocNo = '') AND (SalesHeader."Sell-to Customer No." <> '') THEN
              IF FromSalesInvHeader.SETCURRENTKEY("Sell-to Customer No.") THEN BEGIN
                FromSalesInvHeader."Sell-to Customer No." := SalesHeader."Sell-to Customer No.";
                IF FromSalesInvHeader.FIND('=><') THEN;
              END;
            FromSalesInvHeader.FILTERGROUP(2);
            FromSalesInvHeader.SETRANGE("Prepayment Invoice",FALSE);
            FromSalesInvHeader.FILTERGROUP(0);
            IF PAGE.RUNMODAL(0,FromSalesInvHeader) = ACTION::LookupOK THEN
              DocNo := FromSalesInvHeader."No.";
          END;
        //soft,sn
        DocType::"Posted Debit Memo":
          BEGIN
            FromSalesInvHeader.SETRANGE("Debit Memo",TRUE);
            FromSalesInvHeader."No." := DocNo;
            IF (DocNo = '') AND (SalesHeader."Sell-to Customer No." <> '') THEN
              IF FromSalesInvHeader.SETCURRENTKEY("Sell-to Customer No.") THEN BEGIN
                FromSalesInvHeader."Sell-to Customer No." := SalesHeader."Sell-to Customer No.";
                IF FromSalesInvHeader.FIND('=><') THEN;
              END;
            IF PAGE.RUNMODAL(PAGE::"Posted Sales Debit Memos",FromSalesInvHeader) = ACTION::LookupOK THEN
              DocNo := FromSalesInvHeader."No.";
          END;
        //soft,en

        DocType::"Posted Return Receipt":
          BEGIN
            FromReturnRcptHeader."No." := DocNo;
            IF (DocNo = '') AND (SalesHeader."Sell-to Customer No." <> '') THEN
              IF FromReturnRcptHeader.SETCURRENTKEY("Sell-to Customer No.") THEN BEGIN
                FromReturnRcptHeader."Sell-to Customer No." := SalesHeader."Sell-to Customer No.";
                IF FromReturnRcptHeader.FIND('=><') THEN;
              END;
            IF PAGE.RUNMODAL(0,FromReturnRcptHeader) = ACTION::LookupOK THEN
              DocNo := FromReturnRcptHeader."No.";
          END;
        DocType::"Posted Credit Memo":
          BEGIN
            FromSalesCrMemoHeader."No." := DocNo;
            IF (DocNo = '') AND (SalesHeader."Sell-to Customer No." <> '') THEN
              IF FromSalesCrMemoHeader.SETCURRENTKEY("Sell-to Customer No.") THEN BEGIN
                FromSalesCrMemoHeader."Sell-to Customer No." := SalesHeader."Sell-to Customer No.";
                IF FromSalesCrMemoHeader.FIND('=><') THEN;
              END;
            FromSalesCrMemoHeader.FILTERGROUP(2);
            FromSalesCrMemoHeader.SETRANGE("Prepayment Credit Memo",FALSE);
            FromSalesCrMemoHeader.FILTERGROUP(0);
            IF PAGE.RUNMODAL(0,FromSalesCrMemoHeader) = ACTION::LookupOK THEN
              DocNo := FromSalesCrMemoHeader."No.";
          END;
      END;
      ValidateDocNo;
    END;

    LOCAL PROCEDURE ValidateIncludeHeader@4();
    BEGIN
      RecalculateLines :=
        (DocType IN [DocType::"Posted Shipment",DocType::"Posted Return Receipt"]) OR NOT IncludeHeader;
    END;

    PROCEDURE InitializeRequest@1(NewDocType@1001 : Option;NewDocNo@1000 : Code[20];NewIncludeHeader@1002 : Boolean;NewRecalcLines@1003 : Boolean);
    BEGIN
      DocType := NewDocType;
      DocNo := NewDocNo;
      IncludeHeader := NewIncludeHeader;
      RecalculateLines := NewRecalcLines;
    END;

    BEGIN
    END.
  }
  RDLDATA
  {
  }
}

