OBJECT Table 31022948 Vendor Pmt. Address
{
  OBJECT-PROPERTIES
  {
    Date=01/02/15;
    Time=13:00:00;
    Version List=NAVPTSS81.00;
  }
  PROPERTIES
  {
    OnModify=BEGIN
               "Last Date Modified" := TODAY;
             END;

    CaptionML=[ENU=Vendor Pmt. Address;
               PTG=Endere�o Pagamento Forn.];
    LookupPageID=Page31022984;
  }
  FIELDS
  {
    { 1   ;   ;Vendor No.          ;Code20        ;TableRelation=Vendor;
                                                   CaptionML=[ENU=Vendor No.;
                                                              PTG=N� Fornecedor] }
    { 2   ;   ;Code                ;Code10        ;CaptionML=[ENU=Code;
                                                              PTG=C�digo];
                                                   NotBlank=Yes }
    { 3   ;   ;Name                ;Text50        ;CaptionML=[ENU=Name;
                                                              PTG=Nome] }
    { 4   ;   ;Name 2              ;Text50        ;CaptionML=[ENU=Name 2;
                                                              PTG=Nome 2] }
    { 5   ;   ;Address             ;Text50        ;CaptionML=[ENU=Address;
                                                              PTG=Endere�o] }
    { 6   ;   ;Address 2           ;Text50        ;CaptionML=[ENU=Address 2;
                                                              PTG=Endere�o 2] }
    { 7   ;   ;City                ;Text30        ;OnValidate=BEGIN
                                                                PostCode.ValidateCity(City,"Post Code",County,"Country/Region Code",TRUE);
                                                              END;

                                                   CaptionML=[ENU=City;
                                                              PTG=Cidade] }
    { 8   ;   ;Post Code           ;Code20        ;TableRelation="Post Code";
                                                   OnValidate=BEGIN
                                                                PostCode.ValidatePostCode(City,"Post Code",County,"Country/Region Code",TRUE);
                                                              END;

                                                   ValidateTableRelation=No;
                                                   TestTableRelation=No;
                                                   CaptionML=[ENU=Post Code;
                                                              PTG=C�d. Postal] }
    { 9   ;   ;County              ;Text30        ;CaptionML=[ENU=County;
                                                              PTG=Distrito] }
    { 10  ;   ;Contact             ;Text30        ;CaptionML=[ENU=Contact;
                                                              PTG=Contacto] }
    { 11  ;   ;Country/Region Code ;Code10        ;TableRelation=Country/Region;
                                                   CaptionML=[ENU=Country/Region Code;
                                                              PTG=C�digo Pa�s/Regi�o] }
    { 12  ;   ;Phone No.           ;Text30        ;ExtendedDatatype=Phone No.;
                                                   CaptionML=[ENU=Phone No.;
                                                              PTG=Telefone] }
    { 13  ;   ;Fax No.             ;Text30        ;CaptionML=[ENU=Fax No.;
                                                              PTG=N� Fax] }
    { 14  ;   ;Telex No.           ;Text30        ;CaptionML=[ENU=Telex No.;
                                                              PTG=N� Telex] }
    { 15  ;   ;Last Date Modified  ;Date          ;CaptionML=[ENU=Last Date Modified;
                                                              PTG=Data �ltima Modif.];
                                                   Editable=No }
    { 16  ;   ;Telex Answer Back   ;Text20        ;CaptionML=[ENU=Telex Answer Back;
                                                              PTG=N� Telex Resposta] }
    { 17  ;   ;E-Mail              ;Text80        ;ExtendedDatatype=E-Mail;
                                                   CaptionML=[ENU=E-Mail;
                                                              PTG=E-Mail] }
    { 18  ;   ;Home Page           ;Text80        ;ExtendedDatatype=URL;
                                                   CaptionML=[ENU=Home Page;
                                                              PTG=Home Page] }
  }
  KEYS
  {
    {    ;Vendor No.,Code                         ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {
    VAR
      Text31022890@1000 : TextConst 'ENU=untitled;PTG=SemT�tulo';
      PostCode@1110000 : Record 225;

    PROCEDURE Caption@1() : Text[100];
    VAR
      Vend@1110000 : Record 23;
    BEGIN
      IF "Vendor No." = '' THEN
        EXIT(Text31022890);
      Vend.GET("Vendor No.");
      EXIT(STRSUBSTNO('%1 %2 %3 %4',Vend."No.",Vend.Name,Code,Name));
    END;

    BEGIN
    END.
  }
}

