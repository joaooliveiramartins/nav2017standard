OBJECT Page 31022954 Bill Groups
{
  OBJECT-PROPERTIES
  {
    Date=12/04/17;
    Time=14:48:44;
    Modified=Yes;
    Version List=NAVPTSS10.00;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Bill Groups;
               PTG=Remessas];
    SourceTable=Table31022938;
    DataCaptionExpr=Caption;
    PageType=Document;
    RefreshOnActivate=Yes;
    ActionList=ACTIONS
    {
      { 1900000003;0 ;ActionContainer;
                      ActionContainerType=RelatedInformation }
      { 40      ;1   ;ActionGroup;
                      Name=Bil Group;
                      CaptionML=[ENU=Bill &Group;
                                 PTG=&Remessa] }
      { 48      ;2   ;Action    ;
                      Name=Comments;
                      CaptionML=[ENU=Co&mments;
                                 PTG=Co&ment rios];
                      RunObject=Page 31022969;
                      RunPageLink=Type=FILTER(Receivable),
                                  BG/PO No.=FIELD(No.);
                      Image=ViewComments }
      { 18      ;2   ;Separator  }
      { 47      ;2   ;Action    ;
                      Name=Analysis;
                      CaptionML=[ENU=Analysis;
                                 PTG=An lise];
                      ApplicationArea=#Basic,#Suite;
                      RunObject=Page 31022964;
                      RunPageLink=No.=FIELD(No.),
                                  Due Date Filter=FIELD(Due Date Filter),
                                  Global Dimension 1 Filter=FIELD(Global Dimension 1 Filter),
                                  Global Dimension 2 Filter=FIELD(Global Dimension 2 Filter),
                                  Category Filter=FIELD(Category Filter);
                      Image=Report }
      { 33      ;2   ;Separator  }
      { 34      ;2   ;Action    ;
                      Name=Listing;
                      Ellipsis=Yes;
                      CaptionML=[ENU=Listing;
                                 PTG=Listagem];
                      ApplicationArea=#Basic,#Suite;
                      Promoted=Yes;
                      Image=List;
                      PromotedCategory=Report;
                      OnAction=BEGIN
                                 CurrPage.SETSELECTIONFILTER(BillGr);
                                 BillGr.PrintRecords(TRUE);
                               END;
                                }
      { 1900000004;0 ;ActionContainer;
                      ActionContainerType=ActionItems }
      { 42      ;1   ;ActionGroup;
                      Name=Posting;
                      CaptionML=[ENU=P&osting;
                                 PTG=&Registo] }
      { 29      ;2   ;Action    ;
                      Name=TestReport;
                      Ellipsis=Yes;
                      CaptionML=[ENU=Test Report;
                                 PTG=Verificar...];
                      ApplicationArea=#Basic,#Suite;
                      Image=TestReport;
                      OnAction=BEGIN
                                 IF NOT FIND THEN
                                   EXIT;
                                 BillGr.RESET;
                                 BillGr := Rec;
                                 BillGr.SETRECFILTER;
                                 REPORT.RUN(REPORT::"Bill Group - Test",TRUE,FALSE,BillGr);
                               END;
                                }
      { 54      ;2   ;Action    ;
                      Name=Post;
                      ShortCutKey=F9;
                      Ellipsis=Yes;
                      CaptionML=[ENU=P&ost;
                                 PTG=&Registar];
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=Post;
                      PromotedCategory=Process;
                      OnAction=BEGIN
                                 IF FIND THEN
                                   PostBGPO.ReceivablePostOnly(Rec);
                               END;
                                }
      { 55      ;2   ;Action    ;
                      Name=PostandPrint;
                      ShortCutKey=Shift+F9;
                      Ellipsis=Yes;
                      CaptionML=[ENU=Post and &Print;
                                 PTG=Registar e &Imprimir];
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=PostPrint;
                      PromotedCategory=Process;
                      OnAction=BEGIN
                                 IF FIND THEN
                                   PostBGPO.ReceivablePostAndPrint(Rec);
                               END;
                                }
      { 31022890;2   ;Action    ;
                      Name=ExportToFile;
                      CaptionML=[ENU=Export Bill Group to File;
                                 PTG=Exportar Remessa para Ficheiro];
                      ApplicationArea=#Basic,#Suite;
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=ExportFile;
                      PromotedCategory=Process;
                      OnAction=BEGIN
                                 ExportToFile;
                               END;
                                }
      { 1907413404;1 ;Action    ;
                      Name=BillGroupsMaturity;
                      CaptionML=[ENU=Bill Groups Maturity;
                                 PTG=Vencimento Remessas];
                      ApplicationArea=#Basic,#Suite;
                      RunObject=Page 31022973;
                      RunPageLink=No.=FIELD(No.),
                                  Global Dimension 1 Filter=FIELD(Global Dimension 1 Filter),
                                  Global Dimension 2 Filter=FIELD(Global Dimension 2 Filter),
                                  Category Filter=FIELD(Category Filter);
                      Promoted=Yes;
                      Image=DocumentsMaturity;
                      PromotedCategory=Process }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                CaptionML=[ENU=General;
                           PTG=Geral] }

    { 2   ;2   ;Field     ;
                SourceExpr="No.";
                OnAssistEdit=BEGIN
                               IF AssistEdit(xRec) THEN
                                 CurrPage.UPDATE;
                             END;
                              }

    { 12  ;2   ;Field     ;
                SourceExpr="Bank Account No.";
                OnLookup=BEGIN
                           BankSel.SetCurrBillGr(Rec);
                           IF ACTION::LookupOK = BankSel.RUNMODAL THEN BEGIN
                             BankSel.GETRECORD(BankAcc);
                             "Dealing Type" := BankSel.IsForDiscount;
                             CLEAR(BankSel);
                             VALIDATE("Bank Account No.",BankAcc."No.");
                           END ELSE
                             CLEAR(BankSel);
                         END;
                          }

    { 4   ;2   ;Field     ;
                SourceExpr="Bank Account Name" }

    { 6   ;2   ;Field     ;
                SourceExpr="Dealing Type";
                Importance=Promoted }

    { 26  ;2   ;Field     ;
                SourceExpr=Factoring }

    { 8   ;2   ;Field     ;
                SourceExpr="Posting Date";
                Importance=Promoted }

    { 17  ;2   ;Field     ;
                SourceExpr="Currency Code" }

    { 14  ;2   ;Field     ;
                SourceExpr=Amount }

    { 20  ;2   ;Field     ;
                SourceExpr="Amount (LCY)";
                Importance=Promoted }

    { 36  ;1   ;Part      ;
                Name=Docs;
                CaptionML=[ENU=Docs. in BG;
                           PTG=Documenros Remessa];
                SubPageView=SORTING(Type,Collection Agent,Bill Gr./Pmt. Order No.,Currency Code,Accepted,Due Date,Place);
                SubPageLink=Type=CONST(Receivable),
                            Collection Agent=CONST(Bank),
                            Bill Gr./Pmt. Order No.=FIELD(No.);
                PagePartID=Page31022949;
                PartType=Page }

    { 1904361101;1;Group  ;
                CaptionML=[ENU=Auditing;
                           PTG=Auditoria] }

    { 11  ;2   ;Field     ;
                SourceExpr="Reason Code" }

    { 21  ;2   ;Field     ;
                SourceExpr="No. Printed";
                Editable=FALSE }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1905767507;1;Part   ;
                Visible=TRUE;
                PartType=System;
                SystemPartID=Notes }

    { 1907055907;1;Part   ;
                SubPageLink=No.=FIELD(No.);
                PagePartID=Page31023018;
                Visible=TRUE;
                PartType=Page }

    { 1901839707;1;Part   ;
                SubPageLink=No.=FIELD(Bank Account No.);
                PagePartID=Page31023030;
                Visible=TRUE;
                PartType=Page }

  }
  CODE
  {
    VAR
      BankAcc@1110000 : Record 270;
      BillGr@1110001 : Record 31022938;
      PostBGPO@1110003 : Codeunit 31022903;
      BankSel@1110002 : Page 31022962;

    BEGIN
    END.
  }
}

