OBJECT Page 1873 Item Availability Check Det.
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Details;
               PTG=Detalhes];
    InsertAllowed=No;
    DeleteAllowed=No;
    ModifyAllowed=No;
    SourceTable=Table27;
    PageType=CardPart;
    ShowFilter=No;
  }
  CONTROLS
  {
    { 1   ;0   ;Container ;
                ContainerType=ContentArea }

    { 14  ;1   ;Field     ;
                Name=No.;
                ToolTipML=ENU=Specifies the number of the item.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="No.";
                Editable=FALSE }

    { 13  ;1   ;Field     ;
                Name=Description;
                ToolTipML=ENU=Specifies a description of the item.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr=Description;
                Editable=FALSE }

    { 11  ;1   ;Field     ;
                Name=GrossReq;
                CaptionML=[ENU=Gross Requirement;
                           PTG=Necessidade Bruta];
                ToolTipML=ENU=Specifies dependent demand plus independent demand. Dependent demand comes from production order components of all statuses, assembly order components, and planning lines. Independent demand comes from sales orders, transfer orders, service orders, job tasks, and production forecasts.;
                ApplicationArea=#All;
                DecimalPlaces=0:5;
                SourceExpr=GrossReq;
                Visible=GrossReq <> 0;
                Editable=FALSE }

    { 10  ;1   ;Field     ;
                Name=ReservedReq;
                CaptionML=[ENU=Reserved Requirement;
                           PTG=Necessidade Reservada];
                ToolTipML=[ENU=Specifies reservation quantities on demand records.;
                           PTG=Especifica quantidades reservadas em registos de venda.];
                ApplicationArea=#All;
                DecimalPlaces=0:5;
                SourceExpr=ReservedReq;
                Visible=ReservedReq <> 0;
                Editable=False }

    { 9   ;1   ;Field     ;
                Name=SchedRcpt;
                CaptionML=[ENU=Scheduled Receipt;
                           PTG=Rece��o Programada];
                ToolTipML=ENU=Specifies how many units of the assembly component are inbound on purchase orders, transfer orders, assembly orders, firm planned production orders, and released production orders.;
                ApplicationArea=#All;
                DecimalPlaces=0:5;
                SourceExpr=SchedRcpt;
                Visible=SchedRcpt <> 0;
                Editable=FALSE }

    { 8   ;1   ;Field     ;
                Name=ReservedRcpt;
                CaptionML=[ENU=Reserved Receipt;
                           PTG=Rece��o Reservada];
                ToolTipML=[ENU=Specifies reservation quantities on supply records.;
                           PTG=Especifica quantidades reservadas em registos de compra.];
                ApplicationArea=#All;
                DecimalPlaces=0:5;
                SourceExpr=ReservedRcpt;
                Visible=ReservedRcpt <> 0;
                Editable=False }

    { 7   ;1   ;Field     ;
                Name=CurrentQuantity;
                CaptionML=[ENU=Current Quantity;
                           PTG=quantidade Atual];
                ToolTipML=[ENU=Specifies the quantity on the document for which the availability is checked.;
                           PTG=Especifica a quantidade no documento, para o qual � verificada a disponibilidade];
                ApplicationArea=#All;
                DecimalPlaces=0:5;
                SourceExpr=CurrentQuantity;
                Visible=CurrentQuantity <> 0;
                Editable=FALSE }

    { 6   ;1   ;Field     ;
                Name=CurrentReservedQty;
                CaptionML=[ENU=Current Reserved Quantity;
                           PTG=Quantidade Reservada Atual];
                ToolTipML=[ENU=Specifies the quantity of the item on the document that is currently reserved.;
                           PTG=Especifica a quantidade do produto no documento que esta atualmente reservada.];
                ApplicationArea=#All;
                DecimalPlaces=0:5;
                SourceExpr=CurrentReservedQty;
                Visible=CurrentReservedQty <> 0;
                Editable=FALSE }

    { 4   ;1   ;Field     ;
                Name=EarliestAvailable;
                CaptionML=[ENU=Earliest Availability Date;
                           PTG=Pr�xima Data Disponibilidade];
                ToolTipML=ENU=Specifies the arrival date of an inbound supply that can cover the needed quantity on a date later than the due date. Note that if the inbound supply only covers parts of the needed quantity, it is not considered available and the field will not contain a date.;
                ApplicationArea=#Basic,#Suite;
                DecimalPlaces=0:5;
                SourceExpr=EarliestAvailDate;
                Editable=FALSE }

    { 3   ;1   ;Field     ;
                Name=SubsituteExists;
                ToolTipML=[ENU=Specifies that a substitute exists for this item.;
                           PTG=Especifica que existe um substituto para este produto.];
                SourceExpr="Substitutes Exist";
                Editable=FALSE }

    { 2   ;1   ;Field     ;
                Name=UnitOfMeasureCode;
                Lookup=No;
                CaptionML=[ENU=Unit of Measure Code;
                           PTG=C�d. Unidade Medida];
                ToolTipML=[ENU=Specifies the unit of measure that the availability figures are shown in.;
                           PTG=Especifica a unidade de medida na qual os dados de disponibiidade s�o apresentados.];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=UnitOfMeasureCode;
                Editable=FALSE }

  }
  CODE
  {
    VAR
      UnitOfMeasureCode@1009 : Code[20];
      GrossReq@1007 : Decimal;
      SchedRcpt@1006 : Decimal;
      ReservedReq@1005 : Decimal;
      ReservedRcpt@1004 : Decimal;
      CurrentQuantity@1003 : Decimal;
      CurrentReservedQty@1002 : Decimal;
      EarliestAvailDate@1000 : Date;

    PROCEDURE SetUnitOfMeasureCode@2(Value@1000 : Code[20]);
    BEGIN
      UnitOfMeasureCode := Value;
    END;

    PROCEDURE SetGrossReq@5(Value@1000 : Decimal);
    BEGIN
      GrossReq := Value;
    END;

    PROCEDURE SetReservedRcpt@6(Value@1000 : Decimal);
    BEGIN
      ReservedRcpt := Value;
    END;

    PROCEDURE SetReservedReq@7(Value@1000 : Decimal);
    BEGIN
      ReservedReq := Value;
    END;

    PROCEDURE SetSchedRcpt@8(Value@1000 : Decimal);
    BEGIN
      SchedRcpt := Value;
    END;

    PROCEDURE SetCurrentQuantity@9(Value@1000 : Decimal);
    BEGIN
      CurrentQuantity := Value;
    END;

    PROCEDURE SetCurrentReservedQty@10(Value@1000 : Decimal);
    BEGIN
      CurrentReservedQty := Value;
    END;

    PROCEDURE SetEarliestAvailDate@12(Value@1000 : Date);
    BEGIN
      EarliestAvailDate := Value;
    END;

    BEGIN
    END.
  }
}

