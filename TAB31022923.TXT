OBJECT Table 31022923 Cash-Flow
{
  OBJECT-PROPERTIES
  {
    Date=21/12/16;
    Time=13:00:00;
    Version List=NAVPTSS10.00;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Cash-Flow;
               PTG=Fluxo Caixa];
    LookupPageID=Page31022935;
  }
  FIELDS
  {
    { 1   ;   ;No.                 ;Code10        ;CaptionML=[ENU=No.;
                                                              PTG=N�] }
    { 2   ;   ;Description         ;Text100       ;CaptionML=[ENU=Description;
                                                              PTG=Descri��o] }
    { 3   ;   ;Total               ;Boolean       ;CaptionML=[ENU=Total;
                                                              PTG=Total];
                                                   Editable=No }
    { 4   ;   ;Degree 1            ;Code10        ;TableRelation="Cash-Flow Code - Degree 1".No.;
                                                   CaptionML=[ENU=Degree 1;
                                                              PTG=Grau 1] }
    { 5   ;   ;Degree 2            ;Code10        ;TableRelation="Cash-Flow Code - Degree 2".No.;
                                                   OnValidate=BEGIN
                                                                CashFlowGr2.GET("Degree 2");
                                                                "Degree 1" := CashFlowGr2."Degree 1";
                                                              END;

                                                   CaptionML=[ENU=Degree 2;
                                                              PTG=Grau 2] }
    { 6   ;   ;Date filter         ;Date          ;FieldClass=FlowFilter;
                                                   CaptionML=[ENU=Date filter;
                                                              PTG=Filtro Data] }
    { 7   ;   ;Amount              ;Decimal       ;FieldClass=FlowField;
                                                   CalcFormula=Sum("G/L Entry".Amount WHERE (Posting Date=FIELD(Date filter),
                                                                                             Acc: cash-flow code=FIELD(No.)));
                                                   CaptionML=[ENU=Amount;
                                                              PTG=Valor];
                                                   Editable=No }
    { 8   ;   ;Degree 1 Description;Text100       ;FieldClass=FlowField;
                                                   CalcFormula=Lookup("Cash-Flow Code - Degree 1".Description WHERE (No.=FIELD(Degree 1)));
                                                   CaptionML=[ENU=Degree 1 Description;
                                                              PTG=Descri��o Grau 1];
                                                   Editable=No }
    { 9   ;   ;Degree 2 Description;Text100       ;FieldClass=FlowField;
                                                   CalcFormula=Lookup("Cash-Flow Code - Degree 2".Description WHERE (No.=FIELD(Degree 2)));
                                                   CaptionML=[ENU=Degree 2 Description;
                                                              PTG=Descri��o Grau 2];
                                                   Editable=No }
  }
  KEYS
  {
    {    ;No.                                     ;Clustered=Yes }
    {    ;Total                                    }
    {    ;Degree 1                                 }
    {    ;Degree 2                                 }
  }
  FIELDGROUPS
  {
    { 1   ;DropDown            ;No.,Description,Degree 2,Degree 1        }
  }
  CODE
  {
    VAR
      CashFlowGr2@1110000 : Record 31022924;

    BEGIN
    END.
  }
}

