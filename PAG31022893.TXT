OBJECT Page 31022893 FA Legal Doc. Description List
{
  OBJECT-PROPERTIES
  {
    Date=01/02/15;
    Time=13:00:00;
    Version List=NAVPTSS81.00;
  }
  PROPERTIES
  {
    CaptionML=[ENU=FA Legal Doc. Description List;
               PTG=Lista Descri��o Doc. Legal Imob.];
    SourceTable=Table31022892;
    PageType=List;
    RefreshOnActivate=Yes;
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 2   ;2   ;Field     ;
                SourceExpr="Serial No." }

    { 4   ;2   ;Field     ;
                SourceExpr=Description }

    { 6   ;2   ;Field     ;
                SourceExpr="Legal Table" }

    { 8   ;2   ;Field     ;
                SourceExpr="Legal Division" }

    { 10  ;2   ;Field     ;
                SourceExpr="Legal Group" }

    { 12  ;2   ;Field     ;
                SourceExpr="Legal Activity" }

  }
  CODE
  {

    BEGIN
    END.
  }
}

