OBJECT Page 5611 Depreciation Book List
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0;
  }
  PROPERTIES
  {
    Editable=No;
    CaptionML=[ENU=Depreciation Book List;
               PTG=Lista Livro Amortiza��o];
    SourceTable=Table5611;
    PageType=List;
    CardPageID=Depreciation Book Card;
    ActionList=ACTIONS
    {
      { 1900000003;0 ;ActionContainer;
                      ActionContainerType=RelatedInformation }
      { 9       ;1   ;ActionGroup;
                      CaptionML=[ENU=&Depr. Book;
                                 PTG=&Livro Amort.];
                      Image=DepreciationsBooks }
      { 14      ;2   ;Action    ;
                      CaptionML=[ENU=FA Posting Type Setup;
                                 PTG=Config Tipo Registo Imob.];
                      ToolTipML=[ENU=Set up how to handle the write-down, appreciation, custom 1, and custom 2 posting types that you use when posting to fixed assets.;
                                 PTG=""];
                      ApplicationArea=#FixedAssets;
                      RunObject=Page 5608;
                      RunPageLink=Depreciation Book Code=FIELD(Code);
                      Image=Setup }
      { 15      ;2   ;Action    ;
                      CaptionML=[ENU=FA &Journal Setup;
                                 PTG=Con&fig. Di�rio Imob.];
                      ToolTipML=[ENU=Set up the FA general ledger journal, the FA journal, and the insurance journal templates and batches to use when duplicating depreciation entries and acquisition-cost entries and when calculating depreciation or indexing fixed assets.;
                                 PTG=""];
                      ApplicationArea=#FixedAssets;
                      RunObject=Page 5609;
                      RunPageLink=Depreciation Book Code=FIELD(Code);
                      Image=JournalSetup }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 2   ;2   ;Field     ;
                ToolTipML=ENU=Specifies a code that identifies the depreciation book.;
                ApplicationArea=#FixedAssets;
                SourceExpr=Code }

    { 4   ;2   ;Field     ;
                ToolTipML=ENU=Specifies the purpose of the depreciation book.;
                ApplicationArea=#FixedAssets;
                SourceExpr=Description }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {

    BEGIN
    END.
  }
}

