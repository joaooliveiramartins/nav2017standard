OBJECT Page 99000802 Capacity Units of Measure
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Capacity Units of Measure;
               PTG=Unidadr Medida Capacidade];
    SourceTable=Table99000780;
    PageType=List;
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 2   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the unit code.;
                           PTG=""];
                SourceExpr=Code }

    { 6   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the type of unit of measure.;
                           PTG=""];
                SourceExpr=Type }

    { 4   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the description of the unit of measure.;
                           PTG=""];
                SourceExpr=Description }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {

    BEGIN
    END.
  }
}

