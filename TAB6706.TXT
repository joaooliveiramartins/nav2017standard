OBJECT Table 6706 Booking Service Mapping
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Booking Service Mapping;
               PTG=Mapeamento de Servi�o de Reserva];
  }
  FIELDS
  {
    { 1   ;   ;Service ID          ;Text50        ;CaptionML=[ENU=Service ID;
                                                              PTG=ID Servi�o] }
    { 2   ;   ;Item No.            ;Code20        ;CaptionML=[ENU=Item No.;
                                                              PTG=N� Produto] }
    { 3   ;   ;Booking Mailbox     ;Text80        ;CaptionML=[ENU=Booking Mailbox;
                                                              PTG=Caixa de Correio de Reserva] }
  }
  KEYS
  {
    {    ;Service ID                              ;Clustered=Yes }
    {    ;Booking Mailbox,Item No.                 }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    PROCEDURE Map@1(ItemNo@1000 : Code[20];ServiceID@1001 : Text[50];Mailbox@1002 : Text[80]);
    BEGIN
      INIT;
      "Item No." := ItemNo;
      "Service ID" := ServiceID;
      "Booking Mailbox" := Mailbox;
      IF NOT INSERT(TRUE) THEN
        MODIFY(TRUE);
    END;

    BEGIN
    END.
  }
}

