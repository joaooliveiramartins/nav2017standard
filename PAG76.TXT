OBJECT Page 76 Resource Card
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0,NAVPTSS81.00;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Resource Card;
               PTG=Ficha Recurso];
    SourceTable=Table156;
    PageType=Card;
    RefreshOnActivate=Yes;
    OnOpenPage=VAR
                 CRMIntegrationManagement@1001 : Codeunit 5330;
               BEGIN
                 CRMIntegrationEnabled := CRMIntegrationManagement.IsCRMIntegrationEnabled;
               END;

    OnAfterGetCurrRecord=VAR
                           CRMCouplingManagement@1001 : Codeunit 5331;
                         BEGIN
                           CRMIsCoupledToRecord := CRMIntegrationEnabled AND CRMCouplingManagement.IsRecordCoupledToCRM(RECORDID);
                         END;

    ActionList=ACTIONS
    {
      { 1900000003;0 ;ActionContainer;
                      ActionContainerType=RelatedInformation }
      { 56      ;1   ;ActionGroup;
                      CaptionML=[ENU=&Resource;
                                 PTG=&Recurso];
                      Image=Resource }
      { 58      ;2   ;Action    ;
                      ShortCutKey=F7;
                      CaptionML=[ENU=Statistics;
                                 PTG=Estat�sticas];
                      ToolTipML=ENU=View statistical information, such as the value of posted entries, for the record.;
                      ApplicationArea=#Jobs;
                      RunObject=Page 223;
                      RunPageLink=No.=FIELD(No.),
                                  Date Filter=FIELD(Date Filter),
                                  Unit of Measure Filter=FIELD(Unit of Measure Filter),
                                  Chargeable Filter=FIELD(Chargeable Filter);
                      Promoted=Yes;
                      Image=Statistics;
                      PromotedCategory=Process }
      { 84      ;2   ;Action    ;
                      ShortCutKey=Shift+Ctrl+D;
                      CaptionML=[ENU=Dimensions;
                                 PTG=Dimens�es];
                      ToolTipML=ENU=View or edits dimensions, such as area, project, or department, that you can assign to sales and purchase documents to distribute costs and analyze transaction history.;
                      ApplicationArea=#Jobs;
                      RunObject=Page 540;
                      RunPageLink=Table ID=CONST(156),
                                  No.=FIELD(No.);
                      Image=Dimensions }
      { 73      ;2   ;Action    ;
                      CaptionML=[ENU=&Picture;
                                 PTG=Ima&gem];
                      ToolTipML=ENU=View or add a picture of the resource, or for example, the company's logo.;
                      ApplicationArea=#Jobs;
                      RunObject=Page 407;
                      RunPageLink=No.=FIELD(No.);
                      Image=Picture }
      { 70      ;2   ;Action    ;
                      CaptionML=[ENU=E&xtended Texts;
                                 PTG=Te&xtos Adicionais];
                      ToolTipML=ENU=View the extended description that is set up.;
                      ApplicationArea=#Suite;
                      RunObject=Page 391;
                      RunPageView=SORTING(Table Name,No.,Language Code,All Language Codes,Starting Date,Ending Date);
                      RunPageLink=Table Name=CONST(Resource),
                                  No.=FIELD(No.);
                      Image=Text }
      { 77      ;2   ;Action    ;
                      CaptionML=[ENU=Units of Measure;
                                 PTG=Unidades de Medida];
                      ToolTipML=ENU=View or edit the units of measure that are set up for the resource.;
                      ApplicationArea=#Jobs;
                      RunObject=Page 210;
                      RunPageLink=Resource No.=FIELD(No.);
                      Image=UnitOfMeasure }
      { 36      ;2   ;Action    ;
                      CaptionML=[ENU=S&kills;
                                 PTG=Compet�&ncias];
                      ToolTipML=ENU=View the assignment of skills to the resource. You can use skill codes to allocate skilled resources to service items or items that need special skills for servicing.;
                      ApplicationArea=#Jobs;
                      RunObject=Page 6019;
                      RunPageLink=Type=CONST(Resource),
                                  No.=FIELD(No.);
                      Image=Skills }
      { 34      ;2   ;Separator ;
                      CaptionML=[ENU="";
                                 PTG=""] }
      { 37      ;2   ;Action    ;
                      CaptionML=[ENU=Resource L&ocations;
                                 PTG=L&ocaliza��es Recurso];
                      RunObject=Page 6015;
                      RunPageView=SORTING(Resource No.);
                      RunPageLink=Resource No.=FIELD(No.);
                      Image=Resource }
      { 59      ;2   ;Action    ;
                      CaptionML=[ENU=Co&mments;
                                 PTG=Co&ment�rios];
                      RunObject=Page 124;
                      RunPageLink=Table Name=CONST(Resource),
                                  No.=FIELD(No.);
                      Image=ViewComments }
      { 80      ;2   ;Action    ;
                      CaptionML=[ENU=Online Map;
                                 PTG=Online Map];
                      ToolTipML=ENU=View the address on an online map.;
                      ApplicationArea=#Jobs;
                      Image=Map;
                      OnAction=BEGIN
                                 DisplayMap;
                               END;
                                }
      { 69      ;2   ;Separator  }
      { 33      ;1   ;ActionGroup;
                      Name=ActionGroupCRM;
                      CaptionML=[ENU=Dynamics CRM;
                                 PTG=Dynamics CRM];
                      Visible=CRMIntegrationEnabled }
      { 31      ;2   ;Action    ;
                      Name=CRMGoToProduct;
                      CaptionML=[ENU=Product;
                                 PTG=Produto];
                      ToolTipML=[ENU=Open the coupled Microsoft Dynamics CRM product.;
                                 PTG=Abrir produto do Microsoft Dynamics CRM, acoplado.];
                      Image=CoupledItem;
                      OnAction=VAR
                                 CRMIntegrationManagement@1000 : Codeunit 5330;
                               BEGIN
                                 CRMIntegrationManagement.ShowCRMEntityFromRecordID(RECORDID);
                               END;
                                }
      { 29      ;2   ;Action    ;
                      Name=CRMSynchronizeNow;
                      AccessByPermission=TableData 5331=IM;
                      CaptionML=[ENU=Synchronize Now;
                                 PTG=Sincronizar Agora];
                      ToolTipML=[ENU=Send updated data to Microsoft Dynamics CRM.;
                                 PTG=Enviar dados atualizados para o Microsoft Dynamics CRM.];
                      Image=Refresh;
                      OnAction=VAR
                                 CRMIntegrationManagement@1001 : Codeunit 5330;
                               BEGIN
                                 CRMIntegrationManagement.UpdateOneNow(RECORDID);
                               END;
                                }
      { 27      ;2   ;ActionGroup;
                      Name=Coupling;
                      CaptionML=[@@@=Coupling is a noun;
                                 ENU=Coupling;
                                 PTG=Acoplamento];
                      ToolTipML=[ENU=Create, change, or delete a coupling between the Microsoft Dynamics NAV record and a Microsoft Dynamics CRM record.;
                                 PTG=Criar, modificar, ou eliminar o acoplamento entre o registo do Microsoft Dynamics NAV e um registo do Microsoft dynamics CRM.];
                      Image=LinkAccount }
      { 25      ;3   ;Action    ;
                      Name=ManageCRMCoupling;
                      AccessByPermission=TableData 5331=IM;
                      CaptionML=[ENU=Set Up Coupling;
                                 PTG=Configurar Acoplamento];
                      ToolTipML=[ENU=Create or modify the coupling to a Microsoft Dynamics CRM product.;
                                 PTG=Criar ou modificar o acoplamento a um produto do Microsoft Dynamics CRM.];
                      ApplicationArea=#Suite;
                      Image=LinkAccount;
                      OnAction=VAR
                                 CRMIntegrationManagement@1000 : Codeunit 5330;
                               BEGIN
                                 CRMIntegrationManagement.DefineCoupling(RECORDID);
                               END;
                                }
      { 22      ;3   ;Action    ;
                      Name=DeleteCRMCoupling;
                      AccessByPermission=TableData 5331=IM;
                      CaptionML=[ENU=Delete Coupling;
                                 PTG=Eliminar Acoplamento];
                      ToolTipML=[ENU=Delete the coupling to a Microsoft Dynamics CRM product.;
                                 PTG=Eliminar acoplamento a um produto do microsoft Dynamics CRM.];
                      ApplicationArea=#Suite;
                      Enabled=CRMIsCoupledToRecord;
                      Image=UnLinkAccount;
                      OnAction=VAR
                                 CRMCouplingManagement@1000 : Codeunit 5331;
                               BEGIN
                                 CRMCouplingManagement.RemoveCoupling(RECORDID);
                               END;
                                }
      { 51      ;1   ;ActionGroup;
                      CaptionML=[ENU=&Prices;
                                 PTG=&Pre�os];
                      Image=Price }
      { 61      ;2   ;Action    ;
                      CaptionML=[ENU=Costs;
                                 PTG=Custos];
                      ToolTipML=ENU=View or change detailed information about costs for the resource.;
                      ApplicationArea=#Jobs;
                      RunObject=Page 203;
                      RunPageLink=Type=CONST(Resource),
                                  Code=FIELD(No.);
                      Promoted=Yes;
                      Image=ResourceCosts;
                      PromotedCategory=Process }
      { 62      ;2   ;Action    ;
                      CaptionML=[ENU=Prices;
                                 PTG=Pre�os];
                      ToolTipML=ENU=View or edit prices for the resource.;
                      ApplicationArea=#Jobs;
                      RunObject=Page 204;
                      RunPageLink=Type=CONST(Resource),
                                  Code=FIELD(No.);
                      Promoted=Yes;
                      Image=Price;
                      PromotedCategory=Process }
      { 50      ;1   ;ActionGroup;
                      CaptionML=[ENU=Plan&ning;
                                 PTG=Pla&neamento];
                      Image=Planning }
      { 63      ;2   ;Action    ;
                      CaptionML=[ENU=Resource &Capacity;
                                 PTG=&Capacidade Recurso];
                      ToolTipML=ENU=View this job's resource capacity.;
                      ApplicationArea=#Jobs;
                      RunObject=Page 213;
                      RunPageOnRec=Yes;
                      Image=Capacity }
      { 64      ;2   ;Action    ;
                      CaptionML=[ENU=Resource &Allocated per Job;
                                 PTG=&Aloca��o Recurso por Projeto];
                      ToolTipML=ENU=View this job's resource allocation.;
                      ApplicationArea=#Jobs;
                      RunObject=Page 221;
                      RunPageLink=Resource Filter=FIELD(No.);
                      Image=ViewJob }
      { 76      ;2   ;Action    ;
                      CaptionML=[ENU=Resource Allocated per Service &Order;
                                 PTG=Aloca��o Recurso por &Ordem Servi�o];
                      ToolTipML=ENU=View the service order allocations of the resource.;
                      ApplicationArea=#Jobs;
                      RunObject=Page 6008;
                      RunPageLink=Resource Filter=FIELD(No.);
                      Image=ViewServiceOrder }
      { 66      ;2   ;Action    ;
                      CaptionML=[ENU=Resource A&vailability;
                                 PTG=Dis&ponibilidade Recurso];
                      ToolTipML=ENU=View a summary of resource capacities, the quantity of resource hours allocated to jobs on order, the quantity allocated to service orders, the capacity assigned to jobs on quote, and the resource availability.;
                      ApplicationArea=#Jobs;
                      RunObject=Page 225;
                      RunPageLink=No.=FIELD(No.),
                                  Base Unit of Measure=FIELD(Base Unit of Measure),
                                  Chargeable Filter=FIELD(Chargeable Filter);
                      Image=Calendar }
      { 19      ;1   ;ActionGroup;
                      CaptionML=[ENU=Service;
                                 PTG=Servi�o];
                      Image=ServiceZone }
      { 35      ;2   ;Action    ;
                      CaptionML=[ENU=Service &Zones;
                                 PTG=&Zonas Servi�o];
                      ToolTipML=ENU=View the different service zones that you can assign to customers and resources. When you allocate a resource to a service task that is to be performed at the customer site, you can select a resource that is located in the same service zone as the customer.;
                      ApplicationArea=#Jobs;
                      RunObject=Page 6021;
                      RunPageLink=Resource No.=FIELD(No.);
                      Image=ServiceZone }
      { 17      ;1   ;ActionGroup;
                      CaptionML=[ENU=History;
                                 PTG=Hist�rico];
                      Image=History }
      { 60      ;2   ;Action    ;
                      ShortCutKey=Ctrl+F7;
                      CaptionML=[ENU=Ledger E&ntries;
                                 PTG=Movime&ntos];
                      ToolTipML=ENU=View the history of transactions that have been posted for the selected record.;
                      ApplicationArea=#Jobs;
                      RunObject=Page 202;
                      RunPageView=SORTING(Resource No.)
                                  ORDER(Descending);
                      RunPageLink=Resource No.=FIELD(No.);
                      Promoted=Yes;
                      Image=ResourceLedger;
                      PromotedCategory=Process }
      { 1900000006;0 ;ActionContainer;
                      ActionContainerType=Reports }
      { 1901205806;1 ;Action    ;
                      CaptionML=[ENU=Resource Statistics;
                                 PTG=Estat�sticas Recurso];
                      ToolTipML=ENU=View detailed, historical information for the resource.;
                      ApplicationArea=#Jobs;
                      RunObject=Report 1105;
                      Promoted=Yes;
                      Image=Report;
                      PromotedCategory=Report }
      { 1907688806;1 ;Action    ;
                      CaptionML=[ENU=Resource Usage;
                                 PTG=Consumo de Recurso];
                      ToolTipML=ENU=View the resource utilization that has taken place. The report includes the resource capacity, quantity of usage, and the remaining balance.;
                      ApplicationArea=#Jobs;
                      RunObject=Report 1106;
                      Promoted=Yes;
                      Image=Report;
                      PromotedCategory=Report }
      { 1907042906;1 ;Action    ;
                      CaptionML=[ENU=Resource - Cost Breakdown;
                                 PTG=Recurso - An�lise Custos];
                      ToolTipML=ENU=View the direct unit costs and the total direct costs for each resource. Only usage postings are considered in this report. Resource usage can be posted in the resource journal or the job journal.;
                      ApplicationArea=#Jobs;
                      RunObject=Report 1107;
                      Promoted=Yes;
                      Image=Report;
                      PromotedCategory=Report }
      { 15      ;0   ;ActionContainer;
                      ActionContainerType=ActionItems }
      { 13      ;1   ;ActionGroup;
                      CaptionML=[ENU=F&unctions;
                                 PTG=F&un��es];
                      Image=Action }
      { 9       ;2   ;Action    ;
                      Name=CreateTimeSheets;
                      Ellipsis=Yes;
                      CaptionML=[ENU=Create Time Sheets;
                                 PTG=Criar Folha Horas];
                      ToolTipML=ENU=Create new time sheets for the resource.;
                      ApplicationArea=#Jobs;
                      Image=NewTimesheet;
                      OnAction=BEGIN
                                 CreateTimeSheets;
                               END;
                                }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                CaptionML=[ENU=General;
                           PTG=Geral] }

    { 2   ;2   ;Field     ;
                ToolTipML=ENU=Specifies a number for the resource.;
                ApplicationArea=#Jobs;
                SourceExpr="No.";
                Importance=Promoted;
                OnAssistEdit=BEGIN
                               IF AssistEdit(xRec) THEN
                                 CurrPage.UPDATE;
                             END;
                              }

    { 6   ;2   ;Field     ;
                ToolTipML=ENU=Specifies a description of the resource.;
                ApplicationArea=#Jobs;
                SourceExpr=Name }

    { 4   ;2   ;Field     ;
                ToolTipML=ENU=Specifies whether the resource is a person or a machine.;
                ApplicationArea=#Jobs;
                SourceExpr=Type;
                Importance=Promoted }

    { 20  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the base unit used to measure the resource, such as hour, piece, or kilometer.;
                ApplicationArea=#Jobs;
                SourceExpr="Base Unit of Measure";
                Importance=Promoted }

    { 26  ;2   ;Field     ;
                ToolTipML=ENU=Specifies an additional name for the resource for searching purposes.;
                ApplicationArea=#Jobs;
                SourceExpr="Search Name" }

    { 30  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the resource group that this resource is assigned to.;
                ApplicationArea=#Jobs;
                SourceExpr="Resource Group No.";
                Importance=Promoted }

    { 24  ;2   ;Field     ;
                ToolTipML=ENU=Specifies that the resource is blocked for posting.;
                ApplicationArea=#Jobs;
                SourceExpr=Blocked }

    { 28  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the date of the most recent change of information in the Resource Card window.;
                ApplicationArea=#Jobs;
                SourceExpr="Last Date Modified" }

    { 3   ;2   ;Field     ;
                ToolTipML=ENU=Specifies if a resource uses a time sheet to record time allocated to various tasks.;
                ApplicationArea=#Jobs;
                SourceExpr="Use Time Sheet" }

    { 5   ;2   ;Field     ;
                ToolTipML=ENU=Specifies the name of the owner of the time sheet.;
                ApplicationArea=#Jobs;
                SourceExpr="Time Sheet Owner User ID" }

    { 7   ;2   ;Field     ;
                ToolTipML=ENU=Specifies the ID of the approver of the time sheet.;
                ApplicationArea=#Jobs;
                SourceExpr="Time Sheet Approver User ID" }

    { 1905885101;1;Group  ;
                CaptionML=[ENU=Invoicing;
                           PTG=Fatura��o] }

    { 38  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the direct cost of the resource per unit of measurement.;
                ApplicationArea=#Jobs;
                SourceExpr="Direct Unit Cost" }

    { 40  ;2   ;Field     ;
                ToolTipML=ENU=Specifies an additional percentage to cover benefits or administrative costs.;
                ApplicationArea=#Jobs;
                SourceExpr="Indirect Cost %" }

    { 42  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the cost of one unit of the resource.;
                ApplicationArea=#Jobs;
                SourceExpr="Unit Cost" }

    { 46  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the relationship between the Unit Cost, Unit Price, and Profit Percentage fields associated with this resource.;
                ApplicationArea=#Jobs;
                SourceExpr="Price/Profit Calculation" }

    { 44  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the percentage that is calculated based on the calculation option that you select in the Price/Profit Calculation field.;
                ApplicationArea=#Jobs;
                SourceExpr="Profit %" }

    { 48  ;2   ;Field     ;
                ToolTipML=ENU=Specifies either an amount or value that is calculated based on the calculation option you select in the Price/Profit Calculation field.;
                ApplicationArea=#Jobs;
                SourceExpr="Unit Price" }

    { 32  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the resource's general product posting group. It links business transactions made for this resource with the general ledger to account for the value of trade with the resource.;
                ApplicationArea=#Jobs;
                SourceExpr="Gen. Prod. Posting Group";
                Importance=Promoted }

    { 74  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the VAT product posting group to which this resource belongs.;
                ApplicationArea=#Jobs;
                SourceExpr="VAT Prod. Posting Group";
                Importance=Promoted }

    { 21  ;2   ;Field     ;
                CaptionML=[ENU=Default Deferral Template;
                           PTG=Livro Diferimentos Padr�o];
                ToolTipML=ENU=Specifies the default template that governs how to defer revenues and expenses to the periods when they occurred.;
                ApplicationArea=#Jobs;
                SourceExpr="Default Deferral Template Code" }

    { 71  ;2   ;Field     ;
                ToolTipML=ENU=Specifies that an Extended Text Header will be added on sales or purchase documents for this resource.;
                ApplicationArea=#Jobs;
                SourceExpr="Automatic Ext. Texts" }

    { 78  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the intercompany g/l account number in your partner's company that the amount for this resource is posted to.;
                ApplicationArea=#Jobs;
                SourceExpr="IC Partner Purch. G/L Acc. No." }

    { 1904603601;1;Group  ;
                CaptionML=[ENU=Personal Data;
                           PTG=Dados Pessoais] }

    { 14  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the person's job title.;
                ApplicationArea=#Jobs;
                SourceExpr="Job Title" }

    { 8   ;2   ;Field     ;
                ToolTipML=ENU=Specifies the address or location of the resource, if applicable.;
                ApplicationArea=#Jobs;
                SourceExpr=Address }

    { 23  ;2   ;Field     ;
                ToolTipML=ENU=Specifies an additional address or location of the resource, if applicable.;
                ApplicationArea=#Jobs;
                SourceExpr="Address 2" }

    { 11  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the postal code of the resource's address.;
                ApplicationArea=#Jobs;
                SourceExpr="Post Code" }

    { 10  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the city of the resource's address.;
                ApplicationArea=#Jobs;
                SourceExpr=City }

    { 1110000;2;Field     ;
                SourceExpr=County }

    { 12  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the person's social security number or the machine's serial number.;
                ApplicationArea=#Jobs;
                SourceExpr="Social Security No." }

    { 16  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the training, education, or certification level of the person.;
                ApplicationArea=#Jobs;
                SourceExpr=Education }

    { 54  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the contract class for the person.;
                ApplicationArea=#Jobs;
                SourceExpr="Contract Class" }

    { 18  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the date when the person began working for you or the date when the machine was placed in service.;
                ApplicationArea=#Jobs;
                SourceExpr="Employment Date" }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 39  ;1   ;Part      ;
                ApplicationArea=#Jobs;
                SubPageLink=No.=FIELD(No.);
                PagePartID=Page407;
                PartType=Page }

    { 1906609707;1;Part   ;
                ApplicationArea=#Jobs;
                SubPageLink=No.=FIELD(No.),
                            Unit of Measure Filter=FIELD(Unit of Measure Filter),
                            Chargeable Filter=FIELD(Chargeable Filter),
                            Service Zone Filter=FIELD(Service Zone Filter);
                PagePartID=Page9107;
                Visible=TRUE;
                PartType=Page }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                Visible=TRUE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {
    VAR
      CRMIntegrationEnabled@1001 : Boolean;
      CRMIsCoupledToRecord@1000 : Boolean;

    BEGIN
    END.
  }
}

