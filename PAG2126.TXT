OBJECT Page 2126 O365 Email CC Listpart
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0;
  }
  PROPERTIES
  {
    CaptionML=[ENU=CC;
               PTG=CC];
    SourceTable=Table2118;
    SourceTableView=WHERE(RecipientType=CONST(CC));
    PageType=ListPart;
    OnNewRecord=BEGIN
                  RecipientType := RecipientType::CC;
                END;

  }
  CONTROLS
  {
    { 1   ;0   ;Container ;
                ContainerType=ContentArea }

    { 2   ;1   ;Group     ;
                Name=Group;
                GroupType=Repeater }

    { 3   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the CC  recipient address on all new invoices;
                           PTG=Especifica o endere�o de destinat�rio CC];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=Email }

  }
  CODE
  {

    BEGIN
    END.
  }
}

