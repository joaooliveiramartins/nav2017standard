OBJECT Page 591 Payment Tolerance Warning
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Payment Tolerance Warning;
               PTG=Aviso Toler�ncia Pagamento];
    PageType=ConfirmationDialog;
    InstructionalTextML=[ENU=An action is requested regarding the Payment Tolerance Warning.;
                         PTG=Uma a��o � necess�ria relativamente ao Aviso Toler�ncia Pagto.];
    RefreshOnActivate=Yes;
    OnOpenPage=BEGIN
                 Posting := Posting::"Remaining Amount";

                 IF BalanceAmount = 0 THEN
                   BalanceAmount := Amount + AppliedAmount;
               END;

    OnQueryClosePage=BEGIN
                       IF CloseAction = ACTION::No THEN
                         NoOnPush;
                       IF CloseAction = ACTION::Yes THEN
                         YesOnPush;
                     END;

  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 3   ;1   ;Field     ;
                CaptionML=[ENU=Regarding the Balance amount, do you want to:;
                           PTG=Quanto ao valor do Saldo, pretende:];
                OptionCaptionML=[ENU=,Post the Balance as Payment Tolerance?,Leave a Remaining Amount?;
                                 PTG=,Registar o Saldo como Toler�ncia Pagamento?,Deixar um Valor Pendente?];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=Posting }

    { 2   ;1   ;Group     ;
                CaptionML=[ENU=Details;
                           PTG=Detalhes];
                InstructionalTextML=[ENU=Posting this application will create an outstanding balance. You can close all entries by posting the balance as a payment tolerance amount.;
                                     PTG=O registo desta liquida��o ir� criar um saldo pendente. Pode fechar todos os movimentos registando o saldo como um valor de toler�ncia de pagamento.] }

    { 7   ;2   ;Field     ;
                CaptionML=[ENU=Posting Date;
                           PTG=Data Registo];
                ToolTipML=ENU=Specifies the posting date of the document to be paid.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr=PostingDate;
                Editable=FALSE }

    { 19  ;2   ;Field     ;
                CaptionML=[ENU=No;
                           PTG=N�];
                ToolTipML=ENU=Specifies the number of the record that the payment tolerance warning refers to.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr=CustVendNo;
                Editable=FALSE }

    { 20  ;2   ;Field     ;
                CaptionML=[ENU=Document No.;
                           PTG=N� Documento];
                ToolTipML=ENU=Specifies the document that the payment is for.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr=DocNo;
                Editable=FALSE }

    { 13  ;2   ;Field     ;
                CaptionML=[ENU=Currency Code;
                           PTG=C�d. Divisa];
                ToolTipML=ENU=Specifies the code for the currency that amounts are shown in.;
                ApplicationArea=#Suite;
                SourceExpr=CurrencyCode;
                Editable=FALSE }

    { 8   ;2   ;Field     ;
                CaptionML=[ENU=Amount;
                           PTG=Valor];
                ToolTipML=ENU=Specifies the amount that the payment tolerance warning refers to.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr=Amount;
                Editable=FALSE }

    { 15  ;2   ;Field     ;
                CaptionML=[ENU=Applied Amount;
                           PTG=Valor Liquidado];
                ToolTipML=ENU=Specifies the applied amount that the payment tolerance warning refers to.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr=AppliedAmount;
                Editable=FALSE }

    { 17  ;2   ;Field     ;
                CaptionML=[ENU=Balance;
                           PTG=Saldo];
                ToolTipML=ENU=Specifies the payment amount that the customer owes for completed sales.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr=BalanceAmount;
                Editable=FALSE }

  }
  CODE
  {
    VAR
      PostingDate@1002 : Date;
      CustVendNo@1023 : Code[20];
      DocNo@1003 : Code[20];
      CurrencyCode@1001 : Code[10];
      Amount@1006 : Decimal;
      AppliedAmount@1011 : Decimal;
      BalanceAmount@1009 : Decimal;
      Posting@1015 : ' ,Payment Tolerance Accounts,Remaining Amount';
      NewPostingAction@1005 : Integer;

    PROCEDURE SetValues@1(ShowPostingDate@1000 : Date;ShowCustVendNo@1001 : Code[20];ShowDocNo@1003 : Code[20];ShowCurrencyCode@1002 : Code[10];ShowAmount@1004 : Decimal;ShowAppliedAmount@1005 : Decimal;ShowBalance@1007 : Decimal);
    BEGIN
      PostingDate := ShowPostingDate;
      CustVendNo := ShowCustVendNo;
      DocNo := ShowDocNo;
      CurrencyCode := ShowCurrencyCode;
      Amount := ShowAmount;
      AppliedAmount := ShowAppliedAmount;
      BalanceAmount := ShowBalance;
    END;

    PROCEDURE GetValues@2(VAR PostingAction@1001 : Integer);
    BEGIN
      PostingAction := NewPostingAction
    END;

    LOCAL PROCEDURE YesOnPush@19065578();
    BEGIN
      IF Posting = Posting::"Payment Tolerance Accounts" THEN
        NewPostingAction := 1
      ELSE
        IF Posting = Posting::"Remaining Amount" THEN
          NewPostingAction := 2;
    END;

    LOCAL PROCEDURE NoOnPush@19040112();
    BEGIN
      NewPostingAction := 3;
    END;

    PROCEDURE InitializeOption@3(OptionValue@1000 : Integer);
    BEGIN
      NewPostingAction := OptionValue;
    END;

    BEGIN
    END.
  }
}

