OBJECT Table 432 Handled IC Outbox Purch. Hdr
{
  OBJECT-PROPERTIES
  {
    Date=09/09/14;
    Time=12:00:00;
    Version List=NAVW18.00;
  }
  PROPERTIES
  {
    OnDelete=VAR
               HndlICOutboxPurchLine@1002 : Record 433;
               DimMgt@1000 : Codeunit 408;
             BEGIN
               HndlICOutboxPurchLine.SETRANGE("IC Partner Code","IC Partner Code");
               HndlICOutboxPurchLine.SETRANGE("IC Transaction No.","IC Transaction No.");
               HndlICOutboxPurchLine.SETRANGE("Transaction Source","Transaction Source");
               IF HndlICOutboxPurchLine.FINDFIRST THEN
                 HndlICOutboxPurchLine.DELETEALL(TRUE);
               DimMgt.DeleteICDocDim(
                 DATABASE::"Handled IC Outbox Purch. Hdr","IC Transaction No.","IC Partner Code","Transaction Source",0);
             END;

    CaptionML=[ENU=Handled IC Outbox Purch. Hdr;
               PTG=Cabe�alho Compra Envio IE Processada];
  }
  FIELDS
  {
    { 1   ;   ;Document Type       ;Option        ;CaptionML=[ENU=Document Type;
                                                              PTG=Tipo Documento];
                                                   OptionCaptionML=[ENU=" ,Order,Invoice,Credit Memo,,Return Order";
                                                                    PTG=" ,Encomenda,Fatura,Nota Cr�dito,,Devolu��o"];
                                                   OptionString=[ ,Order,Invoice,Credit Memo,,Return Order];
                                                   Editable=No }
    { 2   ;   ;Buy-from Vendor No. ;Code20        ;TableRelation=Vendor;
                                                   CaptionML=[ENU=Buy-from Vendor No.;
                                                              PTG="Compra-a N� Forn. "];
                                                   Editable=No }
    { 3   ;   ;No.                 ;Code20        ;CaptionML=[ENU=No.;
                                                              PTG=N�];
                                                   Editable=No }
    { 4   ;   ;Pay-to Vendor No.   ;Code20        ;TableRelation=Vendor;
                                                   CaptionML=[ENU=Pay-to Vendor No.;
                                                              PTG=Pagto.-a N� Fornecedor];
                                                   Editable=No }
    { 11  ;   ;Your Reference      ;Text35        ;CaptionML=[ENU=Your Reference;
                                                              PTG=Refer�ncia] }
    { 13  ;   ;Ship-to Name        ;Text50        ;CaptionML=[ENU=Ship-to Name;
                                                              PTG=Envio-a Nome];
                                                   Editable=No }
    { 15  ;   ;Ship-to Address     ;Text50        ;CaptionML=[ENU=Ship-to Address;
                                                              PTG=Envio-a Endere�o];
                                                   Editable=No }
    { 16  ;   ;Ship-to Address 2   ;Text50        ;CaptionML=[ENU=Ship-to Address 2;
                                                              PTG=Envio-a Endere�o 2];
                                                   Editable=No }
    { 17  ;   ;Ship-to City        ;Text30        ;CaptionML=[ENU=Ship-to City;
                                                              PTG=Envio-a Cidade];
                                                   Editable=No }
    { 20  ;   ;Posting Date        ;Date          ;CaptionML=[ENU=Posting Date;
                                                              PTG=Data Registo];
                                                   Editable=No }
    { 21  ;   ;Expected Receipt Date;Date         ;CaptionML=[ENU=Expected Receipt Date;
                                                              PTG=Data Rece��o Esperada];
                                                   Editable=No }
    { 24  ;   ;Due Date            ;Date          ;CaptionML=[ENU=Due Date;
                                                              PTG=Data Vencimento];
                                                   Editable=No }
    { 25  ;   ;Payment Discount %  ;Decimal       ;CaptionML=[ENU=Payment Discount %;
                                                              PTG=% Desconto P.P.];
                                                   Editable=No }
    { 26  ;   ;Pmt. Discount Date  ;Date          ;CaptionML=[ENU=Pmt. Discount Date;
                                                              PTG=Data Desconto P.P.];
                                                   Editable=No }
    { 32  ;   ;Currency Code       ;Code10        ;TableRelation=Currency;
                                                   TestTableRelation=No;
                                                   CaptionML=[ENU=Currency Code;
                                                              PTG=C�d. Divisa];
                                                   Editable=No }
    { 35  ;   ;Prices Including VAT;Boolean       ;CaptionML=[ENU=Prices Including VAT;
                                                              PTG="Pre�os Incluindo IVA "] }
    { 68  ;   ;Vendor Invoice No.  ;Code35        ;CaptionML=[ENU=Vendor Invoice No.;
                                                              PTG=N� Fatura Fornecedor];
                                                   Editable=No }
    { 69  ;   ;Vendor Cr. Memo No. ;Code35        ;CaptionML=[ENU=Vendor Cr. Memo No.;
                                                              PTG=N� Nota Cr�dito Fornecedor];
                                                   Editable=No }
    { 72  ;   ;Sell-to Customer No.;Code20        ;CaptionML=[ENU=Sell-to Customer No.;
                                                              PTG=Venda-a N� Cliente];
                                                   Editable=No }
    { 91  ;   ;Ship-to Post Code   ;Code20        ;CaptionML=[ENU=Ship-to Post Code;
                                                              PTG=Envio-a C�d.Postal];
                                                   Editable=No }
    { 99  ;   ;Document Date       ;Date          ;CaptionML=[ENU=Document Date;
                                                              PTG=Data Documento];
                                                   Editable=No }
    { 125 ;   ;IC Partner Code     ;Code20        ;TableRelation="IC Partner";
                                                   CaptionML=[ENU=IC Partner Code;
                                                              PTG=C�d. Parceiro IE];
                                                   Editable=No }
    { 201 ;   ;IC Transaction No.  ;Integer       ;CaptionML=[ENU=IC Transaction No.;
                                                              PTG=N� Transa��o IE];
                                                   Editable=No }
    { 202 ;   ;Transaction Source  ;Option        ;CaptionML=[ENU=Transaction Source;
                                                              PTG=Origem Transa��o];
                                                   OptionCaptionML=[ENU=Rejected by Current Company,Created by Current Company;
                                                                    PTG=Rejeitado por Empresa Atual,Criado por Empresa Atual];
                                                   OptionString=Rejected by Current Company,Created by Current Company;
                                                   Editable=No }
    { 5790;   ;Requested Receipt Date;Date        ;CaptionML=[ENU=Requested Receipt Date;
                                                              PTG=Data Rece��o Requerida];
                                                   Editable=No }
    { 5791;   ;Promised Receipt Date;Date         ;CaptionML=[ENU=Promised Receipt Date;
                                                              PTG=Data Rece��o Prometida];
                                                   Editable=No }
  }
  KEYS
  {
    {    ;IC Transaction No.,IC Partner Code,Transaction Source;
                                                   Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    END.
  }
}

