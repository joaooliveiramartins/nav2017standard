OBJECT Page 31023016 VAT Annex Setup
{
  OBJECT-PROPERTIES
  {
    Date=01/02/15;
    Time=13:00:00;
    Version List=NAVPTSS81.00;
  }
  PROPERTIES
  {
    CaptionML=[ENU=VAT Annex Setup;
               PTG=Configura��o Anexos IVA];
    SourceTable=Table31022959;
    PageType=List;
  }
  CONTROLS
  {
    { 1000000000;0;Container;
                ContainerType=ContentArea }

    { 1000000001;1;Group  ;
                Name=Group;
                GroupType=Repeater }

    { 1000000002;2;Field  ;
                ApplicationArea=#All;
                SourceExpr=Annex }

    { 1000000003;2;Field  ;
                ApplicationArea=#All;
                SourceExpr="Record Type" }

    { 1000000004;2;Field  ;
                ApplicationArea=#All;
                SourceExpr=Frame }

    { 1000000005;2;Field  ;
                ApplicationArea=#All;
                SourceExpr=Article }

    { 1000000006;2;Field  ;
                ApplicationArea=#All;
                SourceExpr="No." }

    { 1000000007;2;Field  ;
                ApplicationArea=#All;
                SourceExpr=SubSection }

    { 1000000008;2;Field  ;
                ApplicationArea=#All;
                SourceExpr="Reason Code" }

    { 1000000009;2;Field  ;
                ApplicationArea=#All;
                SourceExpr="Tax Authority Code" }

    { 1000000010;2;Field  ;
                ApplicationArea=#All;
                SourceExpr="Group By Doc. No." }

    { 1000000011;2;Field  ;
                ApplicationArea=#All;
                SourceExpr="Group by Reason Code" }

  }
  CODE
  {

    BEGIN
    END.
  }
}

