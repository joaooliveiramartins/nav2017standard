OBJECT Page 303 Vendor Entry Statistics
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0,NAVPTSS93.00;
  }
  PROPERTIES
  {
    Editable=No;
    CaptionML=[ENU=Vendor Entry Statistics;
               PTG=Estat�sticas Mov. Fornecedor];
    LinksAllowed=No;
    SourceTable=Table23;
    PageType=Card;
    OnAfterGetRecord=BEGIN
                       CLEARALL;

                       FOR j := 1 TO 6 DO BEGIN
                         VendLedgEntry[j].SETCURRENTKEY("Document Type","Vendor No.","Posting Date");
                         VendLedgEntry[j].SETRANGE("Document Type",j); // Payment,Invoice,Credit Memo,Finance Charge Memo,Reminder,Refund
                         VendLedgEntry[j].SETRANGE("Vendor No.","No.");
                         IF VendLedgEntry[j].FINDLAST THEN
                           VendLedgEntry[j].CALCFIELDS(Amount,"Remaining Amount");
                       END;

                       //soft,sn
                       jBill := 12;   // Bill position on Document Type= 12
                       VendLedgEntry[jBill].SETCURRENTKEY("Document Type","Vendor No.","Posting Date");
                       VendLedgEntry[jBill].SETRANGE("Document Type",jBill);
                       VendLedgEntry[jBill].SETRANGE("Vendor No.","No.");
                       IF VendLedgEntry[jBill].FINDLAST THEN
                         VendLedgEntry[jBill].CALCFIELDS(Amount,"Remaining Amount");
                       //soft,en

                       VendLedgEntry2.SETCURRENTKEY("Vendor No.",Open);
                       VendLedgEntry2.SETRANGE("Vendor No.","No.");
                       VendLedgEntry2.SETRANGE(Open,TRUE);
                       IF VendLedgEntry2.FIND('+') THEN
                         REPEAT
                           j := VendLedgEntry2."Document Type";
                           IF j > 0 THEN BEGIN
                             VendLedgEntry2.CALCFIELDS("Remaining Amt. (LCY)");
                             TotalRemainAmountLCY[j] := TotalRemainAmountLCY[j] + VendLedgEntry2."Remaining Amt. (LCY)";
                           END;
                         UNTIL VendLedgEntry2.NEXT(-1) = 0;
                       VendLedgEntry2.RESET;

                       DateFilterCalc.CreateAccountingPeriodFilter(VendDateFilter[1],VendDateName[1],WORKDATE,0);
                       DateFilterCalc.CreateFiscalYearFilter(VendDateFilter[2],VendDateName[2],WORKDATE,0);
                       DateFilterCalc.CreateFiscalYearFilter(VendDateFilter[3],VendDateName[3],WORKDATE,-1);

                       FOR i := 1 TO 3 DO BEGIN // Period,This Year,Last Year
                         VendLedgEntry2.SETCURRENTKEY("Vendor No.","Posting Date");
                         VendLedgEntry2.SETRANGE("Vendor No.","No.");
                         VendLedgEntry2.SETFILTER("Posting Date",VendDateFilter[i]);
                         IF VendLedgEntry2.FIND('+') THEN
                           REPEAT
                             j := VendLedgEntry2."Document Type";
                             IF j > 0 THEN
                               NoOfDoc[i][j] := NoOfDoc[i][j] + 1;

                             VendLedgEntry2.CALCFIELDS(Amount);
                             TotalPaymentDiscLCY[i] := TotalPaymentDiscLCY[i] + VendLedgEntry2."Pmt. Disc. Rcd.(LCY)";
                             IF (VendLedgEntry2."Document Type" = VendLedgEntry2."Document Type"::Invoice) AND
                                (NOT VendLedgEntry2.Open) AND
                                (VendLedgEntry2.Amount <> 0)
                             THEN BEGIN
                               VendLedgEntry2.CALCFIELDS("Amount (LCY)");
                               PaymentDiscMissedLCY[i] :=
                                 PaymentDiscMissedLCY[i] +
                                 (VendLedgEntry2."Original Pmt. Disc. Possible" * (VendLedgEntry2."Amount (LCY)" / VendLedgEntry2.Amount)) -
                                 VendLedgEntry2."Pmt. Disc. Rcd.(LCY)";
                             END;
                           UNTIL VendLedgEntry2.NEXT(-1) = 0;
                       END;
                     END;

  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                CaptionML=[ENU=Last Documents;
                           PTG=�ltimos Documentos] }

    { 1903895301;2;Group  ;
                GroupType=FixedLayout }

    { 1900724601;3;Group  ;
                CaptionML=[ENU=Date;
                           PTG=Data] }

    { 7   ;4   ;Field     ;
                CaptionML=[ENU=Payment;
                           PTG=Pagamento];
                ToolTipML=ENU=Specifies the amount that relates to payments.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr=VendLedgEntry[1]."Posting Date" }

    { 13  ;4   ;Field     ;
                CaptionML=[ENU=Invoice;
                           PTG=Fatura];
                ToolTipML=ENU=Specifies the amount that relates to invoices.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr=VendLedgEntry[2]."Posting Date" }

    { 19  ;4   ;Field     ;
                CaptionML=[ENU=Credit Memo;
                           PTG=Nota Cr�dito];
                ToolTipML=ENU=Specifies the amount that relates to credit memos.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr=VendLedgEntry[3]."Posting Date" }

    { 25  ;4   ;Field     ;
                CaptionML=[ENU=Reminder;
                           PTG=Carta Aviso];
                ToolTipML=ENU=Specifies the amount that relates to reminders.;
                SourceExpr=VendLedgEntry[5]."Posting Date" }

    { 65  ;4   ;Field     ;
                CaptionML=[ENU=Finance Charge Memo;
                           PTG=Nota Juros];
                ToolTipML=ENU=Specifies the amount that relates to finance charge memos.;
                SourceExpr=VendLedgEntry[4]."Posting Date" }

    { 79  ;4   ;Field     ;
                CaptionML=[ENU=Refund;
                           PTG=Reembolso];
                ToolTipML=ENU=Specifies the amount that relates to refunds.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr=VendLedgEntry[6]."Posting Date" }

    { 1110106;4;Field     ;
                CaptionML=[ENU=Bill;
                           PTG=T�tulo];
                SourceExpr=VendLedgEntry[jBill]."Posting Date" }

    { 1900724501;3;Group  ;
                CaptionML=[ENU=Document No.;
                           PTG=N� Documento] }

    { 8   ;4   ;Field     ;
                CaptionML=[ENU=Document No.;
                           PTG=N� Documento];
                ToolTipML=ENU=Specifies the number of the document that the statistic is based on.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr=VendLedgEntry[1]."Document No." }

    { 14  ;4   ;Field     ;
                CaptionML=[ENU=Document No.;
                           PTG=N� Documento];
                ToolTipML=ENU=Specifies the number of the document that the statistic is based on.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr=VendLedgEntry[2]."Document No." }

    { 20  ;4   ;Field     ;
                CaptionML=[ENU=Document No.;
                           PTG=N� Documento];
                ToolTipML=ENU=Specifies the number of the document that the statistic is based on.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr=VendLedgEntry[3]."Document No." }

    { 26  ;4   ;Field     ;
                CaptionML=[ENU=Document No.;
                           PTG=N� Documento];
                ToolTipML=ENU=Specifies the number of the document that the statistic is based on.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr=VendLedgEntry[5]."Document No." }

    { 66  ;4   ;Field     ;
                CaptionML=[ENU=Document No.;
                           PTG=N� Documento];
                ToolTipML=ENU=Specifies the number of the document that the statistic is based on.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr=VendLedgEntry[4]."Document No." }

    { 78  ;4   ;Field     ;
                CaptionML=[ENU=Document No.;
                           PTG=N� Documento];
                ToolTipML=ENU=Specifies the number of the document that the statistic is based on.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr=VendLedgEntry[6]."Document No." }

    { 1110107;4;Field     ;
                CaptionML=[ENU=Document No.;
                           PTG=N� Documento];
                SourceExpr=VendLedgEntry[jBill]."Document No." }

    { 1900725501;3;Group  ;
                CaptionML=[ENU=Currency Code;
                           PTG=C�d. Divisa] }

    { 15  ;4   ;Field     ;
                CaptionML=[ENU=Currency Code;
                           PTG=C�d. Divisa];
                ToolTipML=ENU=Specifies the code for the currency that amounts are shown in.;
                ApplicationArea=#Suite;
                SourceExpr=VendLedgEntry[1]."Currency Code" }

    { 21  ;4   ;Field     ;
                CaptionML=[ENU=Currency Code;
                           PTG=C�d. Divisa];
                ToolTipML=ENU=Specifies the code for the currency that amounts are shown in.;
                ApplicationArea=#Suite;
                SourceExpr=VendLedgEntry[2]."Currency Code" }

    { 27  ;4   ;Field     ;
                CaptionML=[ENU=Currency Code;
                           PTG=C�d. Divisa];
                ToolTipML=ENU=Specifies the code for the currency that amounts are shown in.;
                ApplicationArea=#Suite;
                SourceExpr=VendLedgEntry[3]."Currency Code" }

    { 67  ;4   ;Field     ;
                CaptionML=[ENU=Currency Code;
                           PTG=C�d. Divisa];
                ToolTipML=ENU=Specifies the code for the currency that amounts are shown in.;
                ApplicationArea=#Suite;
                SourceExpr=VendLedgEntry[5]."Currency Code" }

    { 76  ;4   ;Field     ;
                CaptionML=[ENU=Currency Code;
                           PTG=C�d. Divisa];
                ToolTipML=ENU=Specifies the code for the currency that amounts are shown in.;
                ApplicationArea=#Suite;
                SourceExpr=VendLedgEntry[4]."Currency Code" }

    { 77  ;4   ;Field     ;
                CaptionML=[ENU=Currency Code;
                           PTG=C�d. Divisa];
                ToolTipML=ENU=Specifies the code for the currency that amounts are shown in.;
                ApplicationArea=#Suite;
                SourceExpr=VendLedgEntry[6]."Currency Code" }

    { 1110108;4;Field     ;
                CaptionML=[ENU=Currency Code;
                           PTG=C�d. Divisa];
                SourceExpr=VendLedgEntry[jBill]."Currency Code" }

    { 1900724301;3;Group  ;
                CaptionML=[ENU=Amount;
                           PTG=Valor] }

    { 10  ;4   ;Field     ;
                CaptionML=[ENU=Amount;
                           PTG=Valor];
                ToolTipML=ENU=Specifies the net amount of all the lines in the vendor entry.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr=VendLedgEntry[1].Amount;
                AutoFormatType=1;
                AutoFormatExpr=VendLedgEntry[1]."Currency Code" }

    { 16  ;4   ;Field     ;
                Name=Amount;
                CaptionML=[ENU=Amount;
                           PTG=Valor];
                ToolTipML=ENU=Specifies the net amount of all the lines in the vendor entry.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr=-VendLedgEntry[2].Amount;
                AutoFormatType=1;
                AutoFormatExpr=VendLedgEntry[2]."Currency Code" }

    { 22  ;4   ;Field     ;
                CaptionML=[ENU=Amount;
                           PTG=Valor];
                ToolTipML=ENU=Specifies the net amount of all the lines in the vendor entry.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr=VendLedgEntry[3].Amount;
                AutoFormatType=1;
                AutoFormatExpr=VendLedgEntry[3]."Currency Code" }

    { 28  ;4   ;Field     ;
                CaptionML=[ENU=Amount;
                           PTG=Valor];
                ToolTipML=ENU=Specifies the net amount of all the lines in the vendor entry.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr=-VendLedgEntry[5].Amount;
                AutoFormatType=1;
                AutoFormatExpr=VendLedgEntry[5]."Currency Code" }

    { 68  ;4   ;Field     ;
                CaptionML=[ENU=Amount;
                           PTG=Valor];
                ToolTipML=ENU=Specifies the net amount of all the lines in the vendor entry.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr=-VendLedgEntry[4].Amount;
                AutoFormatType=1;
                AutoFormatExpr=VendLedgEntry[4]."Currency Code" }

    { 4   ;4   ;Field     ;
                CaptionML=[ENU=Amount;
                           PTG=Valor];
                ToolTipML=ENU=Specifies the net amount of all the lines in the vendor entry.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr=VendLedgEntry[6].Amount;
                AutoFormatType=1;
                AutoFormatExpr=VendLedgEntry[6]."Currency Code" }

    { 1110109;4;Field     ;
                CaptionML=[ENU=Amount;
                           PTG=Valor];
                SourceExpr=-VendLedgEntry[jBill].Amount;
                AutoFormatType=1;
                AutoFormatExpr=VendLedgEntry[12]."Currency Code" }

    { 1900724201;3;Group  ;
                CaptionML=[ENU=Remaining Amount;
                           PTG=Valor Pendente] }

    { 11  ;4   ;Field     ;
                CaptionML=[ENU=Remaining Amount;
                           PTG=Valor Pendente];
                ToolTipML=ENU=Specifies the net remaining amount of all the lines in the vendor entry.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr=VendLedgEntry[1]."Remaining Amount";
                AutoFormatType=1;
                AutoFormatExpr=VendLedgEntry[1]."Currency Code" }

    { 17  ;4   ;Field     ;
                CaptionML=[ENU=Remaining Amount;
                           PTG=Valor Pendente];
                ToolTipML=ENU=Specifies the net remaining amount of all the lines in the vendor entry.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr=-VendLedgEntry[2]."Remaining Amount";
                AutoFormatType=1;
                AutoFormatExpr=VendLedgEntry[2]."Currency Code" }

    { 23  ;4   ;Field     ;
                CaptionML=[ENU=Remaining Amount;
                           PTG=Valor Pendente];
                ToolTipML=ENU=Specifies the net remaining amount of all the lines in the vendor entry.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr=VendLedgEntry[3]."Remaining Amount";
                AutoFormatType=1;
                AutoFormatExpr=VendLedgEntry[3]."Currency Code" }

    { 29  ;4   ;Field     ;
                CaptionML=[ENU=Remaining Amount;
                           PTG=Valor Pendente];
                ToolTipML=ENU=Specifies the net remaining amount of all the lines in the vendor entry.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr=-VendLedgEntry[5]."Remaining Amount";
                AutoFormatType=1;
                AutoFormatExpr=VendLedgEntry[5]."Currency Code" }

    { 69  ;4   ;Field     ;
                CaptionML=[ENU=Remaining Amount;
                           PTG=Valor Pendente];
                ToolTipML=ENU=Specifies the net remaining amount of all the lines in the vendor entry.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr=-VendLedgEntry[4]."Remaining Amount";
                AutoFormatType=1;
                AutoFormatExpr=VendLedgEntry[4]."Currency Code" }

    { 75  ;4   ;Field     ;
                CaptionML=[ENU=Remaining Amount;
                           PTG=Valor Pendente];
                ToolTipML=ENU=Specifies the net remaining amount of all the lines in the vendor entry.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr=VendLedgEntry[6]."Remaining Amount";
                AutoFormatType=1;
                AutoFormatExpr=VendLedgEntry[6]."Currency Code" }

    { 1110110;4;Field     ;
                CaptionML=[ENU=Remaining Amount;
                           PTG=Valor Pendente];
                SourceExpr=-VendLedgEntry[jBill]."Remaining Amount";
                AutoFormatType=1;
                AutoFormatExpr=VendLedgEntry[12]."Currency Code" }

    { 1907032201;1;Group  ;
                CaptionML=[ENU=No. of Documents;
                           PTG=N� de Documentos] }

    { 1904230801;2;Group  ;
                GroupType=FixedLayout }

    { 1901314301;3;Group  ;
                CaptionML=[ENU=This Period;
                           PTG=Este Per�odo] }

    { 32  ;4   ;Field     ;
                ApplicationArea=#Basic,#Suite;
                SourceExpr=VendDateName[1];
                ShowCaption=No }

    { 36  ;4   ;Field     ;
                CaptionML=[ENU=Payments;
                           PTG=Pagamentos];
                ToolTipML=ENU=Specifies the amount that relates to payments.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr=NoOfDoc[1][1] }

    { 41  ;4   ;Field     ;
                CaptionML=[ENU=Invoices;
                           PTG=Faturas];
                ToolTipML=ENU=Specifies the amount that relates to invoices.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr=NoOfDoc[1][2] }

    { 46  ;4   ;Field     ;
                CaptionML=[ENU=Credit Memos;
                           PTG=Notas Cr�dito];
                ToolTipML=ENU=Specifies the amount that relates to credit memos.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr=NoOfDoc[1][3] }

    { 70  ;4   ;Field     ;
                CaptionML=[ENU=Reminder;
                           PTG=Carta Aviso];
                ToolTipML=ENU=Specifies the amount that relates to reminders.;
                SourceExpr=NoOfDoc[1][5] }

    { 51  ;4   ;Field     ;
                CaptionML=[ENU=Finance Charge Memos;
                           PTG=Notas Juros];
                ToolTipML=ENU=Specifies the amount that relates to finance charge memos.;
                SourceExpr=NoOfDoc[1][4] }

    { 84  ;4   ;Field     ;
                CaptionML=[ENU=Refund;
                           PTG=Reembolso];
                ToolTipML=ENU=Specifies the amount that relates to refunds.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr=NoOfDoc[1][6] }

    { 1110101;4;Field     ;
                CaptionML=[ENU=Bills;
                           PTG=T�tulos];
                SourceExpr=NoOfDoc[1][jBill] }

    { 56  ;4   ;Field     ;
                CaptionML=[ENU=Pmt. Disc. Received (LCY);
                           PTG=Desc. P.P. Recebido (DL)];
                ToolTipML=ENU=Specifies the total amount that the vendor has granted as payment discount.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr=-TotalPaymentDiscLCY[1];
                AutoFormatType=1 }

    { 60  ;4   ;Field     ;
                CaptionML=[ENU=Pmt. Disc. Missed (LCY);
                           PTG=Desc. P.P. Perdido (DL)];
                ToolTipML=ENU=Specifies the total amount that the vendor granted as payment discount but you missed.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr=-PaymentDiscMissedLCY[1];
                AutoFormatType=1 }

    { 1900206101;3;Group  ;
                CaptionML=[ENU=This Year;
                           PTG=Este Ano] }

    { 86  ;4   ;Field     ;
                SourceExpr=Text000;
                Visible=FALSE }

    { 37  ;4   ;Field     ;
                CaptionML=[ENU=Payments;
                           PTG=Pagamentos];
                ToolTipML=ENU=Specifies the amount that relates to payments.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr=NoOfDoc[2][1] }

    { 42  ;4   ;Field     ;
                CaptionML=[ENU=Invoices;
                           PTG=Faturas];
                ToolTipML=ENU=Specifies the amount that relates to invoices.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr=NoOfDoc[2][2] }

    { 47  ;4   ;Field     ;
                CaptionML=[ENU=Credit Memos;
                           PTG=Notas Cr�dito];
                ToolTipML=ENU=Specifies the amount that relates to credit memos.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr=NoOfDoc[2][3] }

    { 71  ;4   ;Field     ;
                CaptionML=[ENU=Reminder;
                           PTG=Carta Aviso];
                ToolTipML=ENU=Specifies the amount that relates to reminders.;
                SourceExpr=NoOfDoc[2][5] }

    { 52  ;4   ;Field     ;
                CaptionML=[ENU=Finance Charge Memos;
                           PTG=Notas Juros];
                ToolTipML=ENU=Specifies the amount that relates to finance charge memos.;
                SourceExpr=NoOfDoc[2][4] }

    { 83  ;4   ;Field     ;
                CaptionML=[ENU=Refund;
                           PTG=Reembolso];
                ToolTipML=ENU=Specifies the amount that relates to refunds.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr=NoOfDoc[2][6] }

    { 1110102;4;Field     ;
                CaptionML=[ENU=Bills;
                           PTG=T�tulos];
                SourceExpr=NoOfDoc[2][jBill] }

    { 57  ;4   ;Field     ;
                CaptionML=[ENU=Pmt. Disc. Received (LCY);
                           PTG=Desc. P.P. Recebido (DL)];
                ToolTipML=ENU=Specifies the total amount that the vendor has granted as payment discount.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr=-TotalPaymentDiscLCY[2];
                AutoFormatType=1 }

    { 61  ;4   ;Field     ;
                CaptionML=[ENU=Pmt. Disc. Missed (LCY);
                           PTG=Desc. P.P. Perdido (DL)];
                ToolTipML=ENU=Specifies the total amount that the vendor granted as payment discount but you missed.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr=-PaymentDiscMissedLCY[2];
                AutoFormatType=1 }

    { 1901652501;3;Group  ;
                CaptionML=[ENU=Last Year;
                           PTG=Ano Anterior] }

    { 87  ;4   ;Field     ;
                SourceExpr=Text000;
                Visible=FALSE }

    { 38  ;4   ;Field     ;
                CaptionML=[ENU=Payments;
                           PTG=Pagamentos];
                ToolTipML=ENU=Specifies the amount that relates to payments.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr=NoOfDoc[3][1] }

    { 43  ;4   ;Field     ;
                CaptionML=[ENU=Invoices;
                           PTG=Faturas];
                ToolTipML=ENU=Specifies the amount that relates to invoices.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr=NoOfDoc[3][2] }

    { 48  ;4   ;Field     ;
                CaptionML=[ENU=Credit Memos;
                           PTG=Notas Cr�dito];
                ToolTipML=ENU=Specifies the amount that relates to credit memos.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr=NoOfDoc[3][3] }

    { 72  ;4   ;Field     ;
                CaptionML=[ENU=Reminder;
                           PTG=Carta Aviso];
                ToolTipML=ENU=Specifies the amount that relates to reminders.;
                SourceExpr=NoOfDoc[3][5] }

    { 53  ;4   ;Field     ;
                CaptionML=[ENU=Finance Charge Memos;
                           PTG=Notas Juros];
                ToolTipML=ENU=Specifies the amount that relates to finance charge memos.;
                SourceExpr=NoOfDoc[3][4] }

    { 81  ;4   ;Field     ;
                CaptionML=[ENU=Refund;
                           PTG=Reembolso];
                ToolTipML=ENU=Specifies the amount that relates to refunds.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr=NoOfDoc[3][6] }

    { 1110103;4;Field     ;
                CaptionML=[ENU=Bills;
                           PTG=T�tulos];
                SourceExpr=NoOfDoc[3][jBill] }

    { 58  ;4   ;Field     ;
                CaptionML=[ENU=Pmt. Disc. Received (LCY);
                           PTG=Desc. P.P. Recebido (DL)];
                ToolTipML=ENU=Specifies the total amount that the vendor has granted as payment discount.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr=-TotalPaymentDiscLCY[3];
                AutoFormatType=1 }

    { 62  ;4   ;Field     ;
                CaptionML=[ENU=Pmt. Disc. Missed (LCY);
                           PTG=Desc. P.P. Perdido (DL)];
                ToolTipML=ENU=Specifies the total amount that the vendor granted as payment discount but you missed.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr=-PaymentDiscMissedLCY[3];
                AutoFormatType=1 }

    { 1903098901;3;Group  ;
                CaptionML=[ENU=Remaining Amt. (LCY);
                           PTG=Valor Pendente (DL)] }

    { 88  ;4   ;Field     ;
                SourceExpr=Text000;
                Visible=FALSE }

    { 39  ;4   ;Field     ;
                CaptionML=[ENU=Remaining Amt. (LCY);
                           PTG=Valor Pendente (DL)];
                ToolTipML=ENU=Specifies the amount that remains to be paid.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr=TotalRemainAmountLCY[1];
                AutoFormatType=1 }

    { 44  ;4   ;Field     ;
                CaptionML=[ENU=Remaining Amt. (LCY);
                           PTG=Valor Pendente (DL)];
                ToolTipML=ENU=Specifies the amount that remains to be paid.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr=-TotalRemainAmountLCY[2];
                AutoFormatType=1 }

    { 49  ;4   ;Field     ;
                CaptionML=[ENU=Remaining Amt. (LCY);
                           PTG=Valor Pendente (DL)];
                ToolTipML=ENU=Specifies the amount that remains to be paid.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr=TotalRemainAmountLCY[3];
                AutoFormatType=1 }

    { 73  ;4   ;Field     ;
                CaptionML=[ENU=Remaining Amt. (LCY);
                           PTG=Valor Pendente (DL)];
                ToolTipML=ENU=Specifies the amount that remains to be paid.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr=-TotalRemainAmountLCY[5];
                AutoFormatType=1 }

    { 54  ;4   ;Field     ;
                CaptionML=[ENU=Remaining Amt. (LCY);
                           PTG=Valor Pendente (DL)];
                ToolTipML=ENU=Specifies the amount that remains to be paid.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr=-TotalRemainAmountLCY[4];
                AutoFormatType=1 }

    { 82  ;4   ;Field     ;
                CaptionML=[ENU=Remaining Amt. (LCY);
                           PTG=Valor Pendente (DL)];
                ToolTipML=ENU=Specifies the amount that remains to be paid.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr=TotalRemainAmountLCY[6];
                AutoFormatType=1 }

    { 1110104;4;Field     ;
                CaptionML=[ENU=Remaining Amt. (LCY);
                           PTG=Valor Pendente (DL)];
                SourceExpr=-TotalRemainAmountLCY[jBill];
                AutoFormatType=1 }

    { 89  ;4   ;Field     ;
                SourceExpr=Text000;
                Visible=FALSE }

    { 90  ;4   ;Field     ;
                SourceExpr=Text000;
                Visible=FALSE }

  }
  CODE
  {
    VAR
      VendLedgEntry@1002 : ARRAY [12] OF Record 25;
      VendLedgEntry2@1000 : Record 25;
      DateFilterCalc@1001 : Codeunit 358;
      VendDateFilter@1003 : ARRAY [3] OF Text[30];
      VendDateName@1004 : ARRAY [3] OF Text[30];
      TotalRemainAmountLCY@1005 : ARRAY [12] OF Decimal;
      NoOfDoc@1006 : ARRAY [3,12] OF Integer;
      TotalPaymentDiscLCY@1007 : ARRAY [3] OF Decimal;
      PaymentDiscMissedLCY@1008 : ARRAY [3] OF Decimal;
      i@1009 : Integer;
      j@1010 : Integer;
      Text000@1011 : TextConst 'ENU=Placeholder;PTG=Marcador de posi��o';
      "//--soft-global--//"@1012 : Integer;
      jBill@1013 : Integer;

    BEGIN
    {
      V93.00#00037 - Ficha de Fornecedor - Estat�sticas Movs - MID - 2016.08.08
    }
    END.
  }
}

