OBJECT Codeunit 271 Res. Jnl.-Post
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0;
  }
  PROPERTIES
  {
    TableNo=207;
    OnRun=BEGIN
            ResJnlLine.COPY(Rec);
            Code;
            COPY(ResJnlLine);
          END;

  }
  CODE
  {
    VAR
      Text000@1000 : TextConst 'ENU=cannot be filtered when posting recurring journals;PTG=n�o pode conter um filtro quando regista-se um di�rio peri�dico';
      Text001@1001 : TextConst 'ENU=Do you want to post the journal lines?;PTG=Quer registar as linhas de di�rio?';
      Text002@1002 : TextConst 'ENU=There is nothing to post.;PTG=N�o h� nada para registar.';
      Text003@1003 : TextConst 'ENU=The journal lines were successfully posted.;PTG=As linhas de di�rio foram registadas com sucesso.';
      Text004@1004 : TextConst 'ENU="The journal lines were successfully posted. ";PTG="As linhas de di�rio foram registadas com sucesso. "';
      Text005@1005 : TextConst 'ENU=You are now in the %1 journal.;PTG=Est� agora no di�rio %1.';
      ResJnlTemplate@1006 : Record 206;
      ResJnlLine@1007 : Record 207;
      TempJnlBatchName@1009 : Code[10];

    LOCAL PROCEDURE Code@1();
    BEGIN
      WITH ResJnlLine DO BEGIN
        ResJnlTemplate.GET("Journal Template Name");
        ResJnlTemplate.TESTFIELD("Force Posting Report",FALSE);
        IF ResJnlTemplate.Recurring AND (GETFILTER("Posting Date") <> '') THEN
          FIELDERROR("Posting Date",Text000);

        IF NOT CONFIRM(Text001) THEN
          EXIT;

        TempJnlBatchName := "Journal Batch Name";

        CODEUNIT.RUN(CODEUNIT::"Res. Jnl.-Post Batch",ResJnlLine);

        IF "Line No." = 0 THEN
          MESSAGE(Text002)
        ELSE
          IF TempJnlBatchName = "Journal Batch Name" THEN
            MESSAGE(Text003)
          ELSE
            MESSAGE(
              Text004 +
              Text005,
              "Journal Batch Name");

        IF NOT FIND('=><') OR (TempJnlBatchName <> "Journal Batch Name") THEN BEGIN
          RESET;
          FILTERGROUP(2);
          SETRANGE("Journal Template Name","Journal Template Name");
          SETRANGE("Journal Batch Name","Journal Batch Name");
          FILTERGROUP(0);
          "Line No." := 1;
        END;
      END;
    END;

    BEGIN
    END.
  }
}

