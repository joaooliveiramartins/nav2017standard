OBJECT Page 195 Acc. Sched. KPI Web Srv. Setup
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Account Schedule KPI Web Service Setup;
               PTG=Config. Web Service Esquema Contas KPI];
    InsertAllowed=No;
    DeleteAllowed=No;
    SourceTable=Table135;
    PageType=Worksheet;
    OnOpenPage=BEGIN
                 RESET;
                 IF NOT GET THEN BEGIN
                   INIT;
                   INSERT;
                 END;
               END;

    ActionList=ACTIONS
    {
      { 2       ;    ;ActionContainer;
                      ActionContainerType=ActionItems }
      { 8       ;1   ;Action    ;
                      Name=PublishWebService;
                      CaptionML=[ENU=Publish Web Service;
                                 PTG=Publicar Web Service];
                      ToolTipML=ENU=Publish the account schedule as a web service. The Published field is set to Yes.;
                      ApplicationArea=#Basic,#Suite;
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=Add;
                      PromotedCategory=Process;
                      PromotedOnly=Yes;
                      OnAction=BEGIN
                                 PublishWebService;
                               END;
                                }
      { 9       ;1   ;Action    ;
                      Name=DeleteWebService;
                      CaptionML=[ENU=Remove Web Service;
                                 PTG=Remover Web Service];
                      ToolTipML=ENU=Unpublish the account schedule web service. The Published field is set to No.;
                      ApplicationArea=#Basic,#Suite;
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=Delete;
                      PromotedCategory=Process;
                      PromotedOnly=Yes;
                      OnAction=BEGIN
                                 DeleteWebService;
                               END;
                                }
      { 10      ;0   ;ActionContainer;
                      ActionContainerType=RelatedInformation }
      { 15      ;1   ;Action    ;
                      Name=KPIData;
                      CaptionML=[ENU=Acc. Sched. KPI Web Service;
                                 PTG=Web Service Esquema Contas KPI];
                      ToolTipML=ENU=View the data that is published as a web service based on the account schedules that you have set up in this window.;
                      ApplicationArea=#Basic,#Suite;
                      RunObject=Page 197;
                      Image=List }
      { 11      ;1   ;Action    ;
                      Name=WebServices;
                      CaptionML=[ENU=Web Services;
                                 PTG=Web Services];
                      ToolTipML=ENU=Opens the Web Services window so you can see all available web services.;
                      ApplicationArea=#Basic,#Suite;
                      RunObject=Page 810;
                      Image=Web }
    }
  }
  CONTROLS
  {
    { 1   ;0   ;Container ;
                ContainerType=ContentArea }

    { 14  ;1   ;Group     ;
                CaptionML=[ENU=General;
                           PTG=Geral];
                GroupType=Group }

    { 6   ;2   ;Field     ;
                ToolTipML=ENU=Specifies the period that the account-schedule KPI web service is based on.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr=Period }

    { 5   ;2   ;Field     ;
                ToolTipML=ENU=Specifies which time interval the account-schedule KPI is shown in.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="View By" }

    { 12  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the name of the general ledger budget that provides budgeted values to the account-schedule KPI web service.;
                ApplicationArea=#Suite;
                SourceExpr="G/L Budget Name" }

    { 16  ;2   ;Field     ;
                ToolTipML=ENU=Specifies at what point in time forecasted values are shown on the account-schedule KPI graphic. The forecasted values are retrieved from the selected general ledger budget.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Forecasted Values Start" }

    { 7   ;2   ;Field     ;
                ToolTipML=ENU=Specifies the name of the account-schedule KPI web service. This name will be shown under the displayed account-schedule KPI.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Web Service Name" }

    { 13  ;2   ;Field     ;
                ToolTipML=ENU=Specifies if the account-schedule KPI web service has been published. Published web services are listed in the Web Services window.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr=Published }

    { 3   ;2   ;Field     ;
                CaptionML=[ENU=Last Locked Posting Date;
                           PTG=�ltima Data Registo Bloqueada];
                ToolTipML=ENU=Specifies the last date that posting was locked and actual transaction values were not supplied to the account-schedule KPI.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr=GetLastClosedAccDate }

    { 17  ;2   ;Field     ;
                CaptionML=[ENU=Last Changed Budget Date;
                           PTG=Data Ultima Altera��o Or�amento];
                ToolTipML=ENU=Specifies when the general ledger budget for this account-schedule KPI was last modified.;
                ApplicationArea=#Suite;
                SourceExpr=GetLastBudgetChangedDate }

    { 4   ;1   ;Part      ;
                CaptionML=[ENU=Account Schedules;
                           PTG=Esquemas de Contas];
                ApplicationArea=#Basic,#Suite;
                PagePartID=Page196;
                PartType=Page;
                ShowFilter=No }

  }
  CODE
  {

    BEGIN
    END.
  }
}

