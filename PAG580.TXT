OBJECT Page 580 Dimension Translations
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Dimension Translations;
               PTG=Tradu��o Dimens�es];
    SourceTable=Table388;
    DataCaptionFields=Code;
    PageType=List;
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 10  ;2   ;Field     ;
                ToolTipML=ENU=Specifies a language code.;
                ApplicationArea=#Suite;
                SourceExpr="Language ID";
                LookupPageID=Windows Languages }

    { 4   ;2   ;Field     ;
                DrillDown=No;
                ToolTipML=ENU=Specifies the name of the language.;
                ApplicationArea=#Suite;
                SourceExpr="Language Name";
                Editable=FALSE }

    { 2   ;2   ;Field     ;
                ToolTipML=ENU=Specifies the name of the dimension code.;
                ApplicationArea=#Suite;
                SourceExpr=Name }

    { 6   ;2   ;Field     ;
                ToolTipML=ENU=Specifies the name of the dimension code as you want it to appear as a field name after the Language ID code is selected.;
                ApplicationArea=#Suite;
                SourceExpr="Code Caption" }

    { 8   ;2   ;Field     ;
                ToolTipML=ENU=Specifies the dimension filter caption.;
                ApplicationArea=#Suite;
                SourceExpr="Filter Caption" }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {

    BEGIN
    END.
  }
}

