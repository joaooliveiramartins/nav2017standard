OBJECT Table 31022897 Aprec./Depr. Factors
{
  OBJECT-PROPERTIES
  {
    Date=01/02/15;
    Time=13:00:00;
    Version List=NAVPTSS81.00;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Aprec./Depr. Factors;
               PTG=Factores Aprec./Amort.];
  }
  FIELDS
  {
    { 1   ;   ;Type                ;Option        ;CaptionML=[ENU=Type;
                                                              PTG=Tipo];
                                                   OptionCaptionML=[ENU=Apreciation,Depreciation;
                                                                    PTG=Aprecia��o,Amortiza��o];
                                                   OptionString=Apreciation,Depreciation }
    { 2   ;   ;Reference Year      ;Integer       ;CaptionML=[ENU=Reference Year;
                                                              PTG=Ano Refer�ncia] }
    { 3   ;   ;Acquire Year        ;Integer       ;CaptionML=[ENU=Acquire Year;
                                                              PTG=Ano Aquisi��o] }
    { 4   ;   ;Factor              ;Decimal       ;CaptionML=[ENU=Factor;
                                                              PTG=Factor] }
  }
  KEYS
  {
    {    ;Type,Reference Year,Acquire Year        ;Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    END.
  }
}

