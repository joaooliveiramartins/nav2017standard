OBJECT Page 1009 Job WIP G/L Entries
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0;
  }
  PROPERTIES
  {
    Editable=No;
    CaptionML=[ENU=Job WIP G/L Entries;
               PTG=Movs. C/G Trab. Curso de Projeto];
    SourceTable=Table1005;
    DataCaptionFields=Job No.;
    PageType=List;
    ActionList=ACTIONS
    {
      { 1900000003;0 ;ActionContainer;
                      ActionContainerType=RelatedInformation }
      { 52      ;1   ;ActionGroup;
                      CaptionML=[ENU=Ent&ry;
                                 PTG=M&ovimento];
                      Image=Entry }
      { 9       ;2   ;Action    ;
                      Name=<Action57>;
                      CaptionML=[ENU=WIP Totals;
                                 PTG=Totais Trab. Curso];
                      ToolTipML=ENU=View the job's WIP totals.;
                      ApplicationArea=#Jobs;
                      RunObject=Page 1028;
                      RunPageLink=Entry No.=FIELD(Job WIP Total Entry No.);
                      Promoted=Yes;
                      Image=EntriesList;
                      PromotedCategory=Process }
      { 53      ;2   ;Action    ;
                      AccessByPermission=TableData 348=R;
                      ShortCutKey=Shift+Ctrl+D;
                      CaptionML=[ENU=Dimensions;
                                 PTG=Dimens�es];
                      ToolTipML=ENU=View or edits dimensions, such as area, project, or department, that you can assign to sales and purchase documents to distribute costs and analyze transaction history.;
                      ApplicationArea=#Jobs;
                      Image=Dimensions;
                      OnAction=BEGIN
                                 ShowDimensions;
                               END;
                                }
      { 1900000004;0 ;ActionContainer;
                      ActionContainerType=ActionItems }
      { 37      ;1   ;Action    ;
                      CaptionML=[ENU=&Navigate;
                                 PTG=&Navegar];
                      ToolTipML=ENU=Find all entries and documents that exist for the document number and posting date on the selected entry or document.;
                      ApplicationArea=#Jobs;
                      Promoted=Yes;
                      Image=Navigate;
                      PromotedCategory=Process;
                      OnAction=BEGIN
                                 Navigate.SetDoc("Posting Date","Document No.");
                                 Navigate.RUN;
                               END;
                                }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 28  ;2   ;Field     ;
                ToolTipML=ENU=Specifies whether the entry has been reversed. If the check box is selected, the entry has been reversed from the G/L.;
                ApplicationArea=#Jobs;
                SourceExpr=Reversed }

    { 10  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the posting date you entered in the Posting Date field, on the Options FastTab, in the Job Post WIP to G/L batch job.;
                ApplicationArea=#Jobs;
                SourceExpr="Posting Date" }

    { 24  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the posting date you entered in the Posting Date field, on the Options FastTab, in the Job Calculate WIP batch job.;
                ApplicationArea=#Jobs;
                SourceExpr="WIP Posting Date" }

    { 6   ;2   ;Field     ;
                ToolTipML=ENU=Specifies the document number you entered in the Document No. field on the Options FastTab in the Job Post WIP to G/L batch job.;
                ApplicationArea=#Jobs;
                SourceExpr="Document No." }

    { 4   ;2   ;Field     ;
                ToolTipML=ENU=Specifies the number of the job that this WIP general ledger entry is related to.;
                ApplicationArea=#Jobs;
                SourceExpr="Job No." }

    { 15  ;2   ;Field     ;
                ToolTipML=ENU=Specifies whether a job is complete. This check box is selected if the Job WIP G/L Entry was created for a Job with a Completed status.;
                ApplicationArea=#Jobs;
                SourceExpr="Job Complete" }

    { 7   ;2   ;Field     ;
                ToolTipML=ENU=Specifies the entry number from the associated job WIP total.;
                ApplicationArea=#Jobs;
                SourceExpr="Job WIP Total Entry No." }

    { 8   ;2   ;Field     ;
                ToolTipML=ENU=Specifies the general ledger account number to which the WIP, on this entry, is posted.;
                ApplicationArea=#Jobs;
                SourceExpr="G/L Account No." }

    { 18  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the general ledger balancing account number that WIP on this entry was posted to.;
                ApplicationArea=#Jobs;
                SourceExpr="G/L Bal. Account No." }

    { 32  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the reverse date. If the WIP on this entry is reversed, you can see the date of the reversal in the Reverse Date field.;
                ApplicationArea=#Jobs;
                SourceExpr="Reverse Date" }

    { 50  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the WIP method that was specified for the job when you ran the Job Calculate WIP batch job.;
                ApplicationArea=#Jobs;
                SourceExpr="WIP Method Used" }

    { 13  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the WIP posting method used in the context of the general ledger. The information in this field comes from the setting you have specified on the job card.;
                ApplicationArea=#Jobs;
                SourceExpr="WIP Posting Method Used" }

    { 16  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the WIP type for this entry.;
                ApplicationArea=#Jobs;
                SourceExpr=Type }

    { 12  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the WIP amount that was posted in the general ledger for this entry.;
                ApplicationArea=#Jobs;
                SourceExpr="WIP Entry Amount" }

    { 14  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the posting group related to this entry.;
                ApplicationArea=#Jobs;
                SourceExpr="Job Posting Group" }

    { 30  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the transaction number assigned to all the entries involved in the same transaction.;
                ApplicationArea=#Jobs;
                SourceExpr="WIP Transaction No." }

    { 11  ;2   ;Field     ;
                ToolTipML=ENU=Specifies whether the entry has been part of a reverse transaction (correction) made by the reverse function.;
                ApplicationArea=#Jobs;
                SourceExpr=Reverse }

    { 5   ;2   ;Field     ;
                ToolTipML=ENU=Specifies the dimension value code that the job G/L entry is linked to. You cannot change the code because the entry has been posted.;
                ApplicationArea=#Jobs;
                SourceExpr="Global Dimension 1 Code" }

    { 3   ;2   ;Field     ;
                ToolTipML=ENU=Specifies the dimension value code that the job G/L entry is linked to. You cannot change the code because the entry has been posted.;
                ApplicationArea=#Jobs;
                SourceExpr="Global Dimension 2 Code" }

    { 26  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the G/L Entry No. to which this entry is linked.;
                ApplicationArea=#Jobs;
                SourceExpr="G/L Entry No." }

    { 2   ;2   ;Field     ;
                ToolTipML=ENU=Specifies the number of the WIP general ledger entry.;
                ApplicationArea=#Jobs;
                SourceExpr="Entry No." }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {
    VAR
      Navigate@1000 : Page 344;

    BEGIN
    END.
  }
}

