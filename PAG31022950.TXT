OBJECT Page 31022950 Docs. in Posted BG Subform
{
  OBJECT-PROPERTIES
  {
    Date=11/04/17;
    Time=12:22:04;
    Modified=Yes;
    Version List=NAVPT10.00#00015;
  }
  PROPERTIES
  {
    Permissions=TableData 31022936=m;
    CaptionML=[ENU=Docs. in Posted BG Subform;
               PTG=Subform Documentos Remessa Reg.];
    InsertAllowed=No;
    DeleteAllowed=No;
    SourceTable=Table31022936;
    SourceTableView=WHERE(Type=CONST(Receivable));
    PageType=ListPart;
    OnModifyRecord=BEGIN
                     CODEUNIT.RUN(CODEUNIT::"Posted Cartera Doc.- Edit",Rec);
                     EXIT(FALSE);
                   END;

    ActionList=ACTIONS
    {
      { 1900000004;0 ;ActionContainer;
                      ActionContainerType=ActionItems }
      { 1907380404;1 ;ActionGroup;
                      CaptionML=[ENU=&Documents.;
                                 PTG=&Documentos] }
      { 1906734104;2 ;Action    ;
                      Name=Dimensions;
                      ShortCutKey=Shift+Ctrl+D;
                      CaptionML=[ENU=Dime&nsions;
                                 PTG=Dime&ns�es];
                      ApplicationArea=#Basic,#Suite;
                      Image=Dimensions;
                      OnAction=BEGIN
                                 ShowDimension;
                               END;
                                }
      { 1902759804;2 ;Action    ;
                      Name=Categorize;
                      Ellipsis=Yes;
                      CaptionML=[ENU=Categorize;
                                 PTG=Classificar];
                      ApplicationArea=#Basic,#Suite;
                      Image=Category;
                      OnAction=BEGIN
                                 Categorize;
                               END;
                                }
      { 1900206204;2 ;Action    ;
                      Name=Decategorize;
                      CaptionML=[ENU=Decategorize;
                                 PTG=Desclassificar];
                      ApplicationArea=#Basic,#Suite;
                      Image=UndoCategory;
                      OnAction=BEGIN
                                 Decategorize;
                               END;
                                }
      { 1900295704;2 ;ActionGroup;
                      CaptionML=[ENU=Settle;
                                 PTG=Liquidar];
                      Image=SettleOpenTransactions }
      { 1901652204;3 ;Action    ;
                      Name=Total Settlement;
                      Ellipsis=Yes;
                      CaptionML=[ENU=Total Settlement;
                                 PTG=Liquida��o &Total];
                      ApplicationArea=#Basic,#Suite;
                      OnAction=BEGIN
                                 Settle;
                               END;
                                }
      { 1903098604;3 ;Action    ;
                      Name=Partial Settlement;
                      Ellipsis=Yes;
                      CaptionML=[ENU=P&artial Settlement;
                                 PTG=Liquida��o &Parcial];
                      ApplicationArea=#Basic,#Suite;
                      OnAction=BEGIN
                                 PartialSettle;
                               END;
                                }
      { 1901742104;2 ;Action    ;
                      Name=Reject;
                      Ellipsis=Yes;
                      CaptionML=[ENU=Reject;
                                 PTG=Devolver];
                      ApplicationArea=#Basic,#Suite;
                      Image=Reject;
                      OnAction=BEGIN
                                 Reject;
                               END;
                                }
      { 1903866804;2 ;Action    ;
                      Name=Redraw;
                      Ellipsis=Yes;
                      CaptionML=[ENU=Redraw;
                                 PTG=Reformar];
                      ApplicationArea=#Basic,#Suite;
                      Image=RefreshVoucher;
                      OnAction=BEGIN
                                 Redraw;
                               END;
                                }
      { 1901991504;2 ;Action    ;
                      Name=Print;
                      Ellipsis=Yes;
                      CaptionML=[ENU=Print;
                                 PTG=Imprimir];
                      ApplicationArea=#Basic,#Suite;
                      Image=Print;
                      OnAction=BEGIN
                                 PrintDoc;
                               END;
                                }
      { 31022890;2   ;Action    ;
                      Name=Navigate;
                      CaptionML=[ENU=&Navigate;
                                 PTG=&Navegar];
                      Image=Navigate;
                      OnAction=BEGIN
                                 Navigate;
                               END;
                                }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 2   ;2   ;Field     ;
                SourceExpr="Posting Date";
                Visible=FALSE;
                Editable=FALSE }

    { 40  ;2   ;Field     ;
                SourceExpr="Document Type" }

    { 4   ;2   ;Field     ;
                SourceExpr="Due Date";
                Editable=FALSE }

    { 24  ;2   ;Field     ;
                SourceExpr=Status;
                Editable=FALSE }

    { 34  ;2   ;Field     ;
                SourceExpr="Honored/Rejtd. at Date";
                Visible=FALSE;
                Editable=FALSE }

    { 20  ;2   ;Field     ;
                SourceExpr="Payment Method Code";
                Editable=FALSE }

    { 6   ;2   ;Field     ;
                SourceExpr="Document No.";
                Editable=FALSE }

    { 8   ;2   ;Field     ;
                SourceExpr="No.";
                Editable=FALSE }

    { 10  ;2   ;Field     ;
                SourceExpr=Description;
                Editable=FALSE }

    { 30  ;2   ;Field     ;
                SourceExpr="Original Amount";
                Visible=FALSE;
                Editable=FALSE }

    { 32  ;2   ;Field     ;
                SourceExpr="Original Amt. (LCY)";
                Visible=FALSE;
                Editable=FALSE }

    { 22  ;2   ;Field     ;
                SourceExpr="Amount for Collection";
                Editable=FALSE }

    { 36  ;2   ;Field     ;
                SourceExpr="Amt. for Collection (LCY)";
                Visible=FALSE;
                Editable=FALSE }

    { 12  ;2   ;Field     ;
                SourceExpr="Remaining Amount";
                Editable=FALSE }

    { 38  ;2   ;Field     ;
                SourceExpr="Remaining Amt. (LCY)";
                Visible=FALSE;
                Editable=FALSE }

    { 28  ;2   ;Field     ;
                SourceExpr=Redrawn;
                Editable=FALSE }

    { 14  ;2   ;Field     ;
                SourceExpr=Place;
                Visible=FALSE;
                Editable=FALSE }

    { 16  ;2   ;Field     ;
                SourceExpr="Category Code" }

    { 18  ;2   ;Field     ;
                SourceExpr="Account No.";
                Visible=FALSE;
                Editable=FALSE }

    { 26  ;2   ;Field     ;
                SourceExpr="Entry No.";
                Editable=FALSE }

  }
  CODE
  {
    VAR
      Text31022890@1000 : TextConst 'ENU=No documents have been found that can be settled. \;PTG=N�o existem documentos a liquidar. \';
      Text31022891@1001 : TextConst 'ENU=Please check that at least one open document was selected.;PTG=Confira que selecionou no m�nimo um documento pendente.';
      Text31022892@1002 : TextConst 'ENU=No documents have been found that can be rejected. \;PTG=N�o existem documentos a devolver. \';
      Text31022893@1003 : TextConst 'ENU=Only invoices in Bill Groups marked as %1 Risked can be rejected.;PTG=S� podem ser devolvidas as faturas em remessas marcadas com risco %1.';
      Text31022894@1004 : TextConst 'ENU=No documents have been found that can be redrawn. \;PTG=N�o existem documentos que possam ser reformados. \';
      Text31022895@1005 : TextConst 'ENU=Please check that at least one rejected or honored document was selected.;PTG=Confira que selecionou no m�nimo um documento pago ou devolvido.';
      Text31022896@1006 : TextConst 'ENU=Only bills can be redrawn.;PTG=S� podem ser reformados t�tulos.';
      Text31022897@1007 : TextConst 'ENU=Please check that one open document was selected.;PTG=Confira que foi selecionado um documento pendente.';
      Text31022898@1008 : TextConst 'ENU=Only one open document can be selected;PTG=S� pode ser selecionado um documento pendente';
      PostedDoc@1110000 : Record 31022936;
      CustLedgEntry@1110001 : Record 21;
      SalesInvHeader@1110003 : Record 112;
      CarteraManagement@1110004 : Codeunit 31022901;

    PROCEDURE Categorize@6();
    BEGIN
      CurrPage.SETSELECTIONFILTER(PostedDoc);
      CarteraManagement.CategorizePostedDocs(PostedDoc);
    END;

    PROCEDURE Decategorize@7();
    BEGIN
      CurrPage.SETSELECTIONFILTER(PostedDoc);
      CarteraManagement.DecategorizePostedDocs(PostedDoc);
    END;

    PROCEDURE Settle@2();
    BEGIN
      CurrPage.SETSELECTIONFILTER(PostedDoc);
      IF PostedDoc.ISEMPTY THEN
        EXIT;

      PostedDoc.SETRANGE(Status,PostedDoc.Status::Open);
      IF PostedDoc.ISEMPTY THEN
        ERROR(Text31022890 + Text31022891);

      REPORT.RUNMODAL(REPORT::"Settle Docs. in Post. Bill Gr.",TRUE,FALSE,PostedDoc);
      CurrPage.UPDATE(FALSE);
    END;

    PROCEDURE Reject@3();
    VAR
      PostedBillGr@1110000 : Record 31022939;
    BEGIN
      CurrPage.SETSELECTIONFILTER(PostedDoc);

      IF PostedDoc.ISEMPTY THEN
        EXIT;

      PostedDoc.SETRANGE(Status,PostedDoc.Status::Open);
      IF PostedDoc.ISEMPTY THEN
        ERROR(Text31022892 + Text31022891);

      PostedDoc.FINDSET;

      IF PostedDoc.Factoring <> PostedDoc.Factoring::" " THEN BEGIN
        PostedBillGr.GET(PostedDoc."Bill Gr./Pmt. Order No.");
        IF PostedBillGr.Factoring = PostedBillGr.Factoring::Unrisked THEN
          ERROR(Text31022893,PostedBillGr.FIELDCAPTION(Factoring));
      END;

      CustLedgEntry.RESET;
      REPEAT
        CustLedgEntry.GET(PostedDoc."Entry No.");
        CustLedgEntry.MARK(TRUE);
      UNTIL PostedDoc.NEXT = 0;

      CustLedgEntry.MARKEDONLY(TRUE);
      REPORT.RUNMODAL(REPORT::"Reject Docs.",TRUE,FALSE,CustLedgEntry);
      CurrPage.UPDATE(FALSE);
    END;

    PROCEDURE Redraw@5();
    BEGIN
      CurrPage.SETSELECTIONFILTER(PostedDoc);
      IF PostedDoc.ISEMPTY THEN
        EXIT;

      PostedDoc.SETFILTER(Status,'<>%1',PostedDoc.Status::Open);
      IF PostedDoc.ISEMPTY THEN
        ERROR(Text31022894 + Text31022895);

      PostedDoc.SETFILTER("Document Type",'<>%1',PostedDoc."Document Type"::Bill);
      IF NOT PostedDoc.ISEMPTY THEN
        ERROR(Text31022896);

      CustLedgEntry.RESET;
      PostedDoc.SETRANGE("Document Type");
      PostedDoc.FINDSET;
      REPEAT
        CustLedgEntry.GET(PostedDoc."Entry No.");
        CustLedgEntry.MARK(TRUE);
      UNTIL PostedDoc.NEXT = 0;

      CustLedgEntry.MARKEDONLY(TRUE);
      REPORT.RUNMODAL(REPORT::"Redraw Receivable Bills",TRUE,FALSE,CustLedgEntry);
      CurrPage.UPDATE(FALSE);
    END;

    PROCEDURE PrintDoc@4();
    BEGIN
      CurrPage.SETSELECTIONFILTER(PostedDoc);
      IF PostedDoc.ISEMPTY THEN
        EXIT;

      PostedDoc.FINDSET;

      IF PostedDoc."Document Type" = PostedDoc."Document Type"::Bill THEN BEGIN
        CustLedgEntry.RESET;
        REPEAT
          CustLedgEntry.GET(PostedDoc."Entry No.");
          CustLedgEntry.MARK(TRUE);
        UNTIL PostedDoc.NEXT = 0;

        CustLedgEntry.MARKEDONLY(TRUE);
        CurrPage.UPDATE(FALSE);
        REPORT.RUNMODAL(REPORT::"Receivable Bill",TRUE,FALSE,CustLedgEntry);
      END ELSE BEGIN
        SalesInvHeader.RESET;
        REPEAT
          SalesInvHeader.GET(PostedDoc."Document No.");
          SalesInvHeader.MARK(TRUE);
        UNTIL PostedDoc.NEXT = 0;
        SalesInvHeader.MARKEDONLY(TRUE);
        CurrPage.UPDATE(FALSE);
        REPORT.RUNMODAL(REPORT::"Sales - Invoice",TRUE,FALSE,SalesInvHeader);
      END;
    END;

    PROCEDURE Navigate@1();
    BEGIN
      CarteraManagement.NavigatePostedDoc(Rec);
    END;

    PROCEDURE PartialSettle@8();
    VAR
      PartialSettlePayable@1110000 : Report 31022981;
      PartialSettleReceivable@1110001 : Report 31022980;
      CustLedgEntry2@1110002 : Record 21;
      VendLedgEntry2@1110003 : Record 25;
    BEGIN
      CurrPage.SETSELECTIONFILTER(PostedDoc);
      IF PostedDoc.ISEMPTY THEN
        EXIT;

      PostedDoc.SETRANGE(Status,PostedDoc.Status::Open);
      IF PostedDoc.ISEMPTY THEN
        ERROR(Text31022890 + Text31022897);

      IF PostedDoc.COUNT > 1 THEN
        ERROR(Text31022898);

      PostedDoc.FINDFIRST;

      CLEAR(PartialSettleReceivable);
      CustLedgEntry2.GET(PostedDoc."Entry No.");
      IF (WORKDATE <= CustLedgEntry2."Pmt. Discount Date") AND
         (PostedDoc."Document Type" = PostedDoc."Document Type"::Invoice) THEN
        PartialSettleReceivable.SetInitValue(PostedDoc."Remaining Amount" - CustLedgEntry2."Remaining Pmt. Disc. Possible",
          PostedDoc."Currency Code",PostedDoc."Entry No.")
      ELSE
        PartialSettleReceivable.SetInitValue(PostedDoc."Remaining Amount",
          PostedDoc."Currency Code",PostedDoc."Entry No.");
      PartialSettleReceivable.SETTABLEVIEW(PostedDoc);
      PartialSettleReceivable.RUNMODAL;
      CurrPage.UPDATE(FALSE);
    END;

    PROCEDURE ShowDimension@19020062();
    VAR
      Status@1110001 : 'Open,Posted,Closed';
      DimMgmt@1000000000 : Codeunit 408;
    BEGIN
      DimMgmt.ShowDimensionSet("Dimension Set ID",STRSUBSTNO('%1 %2 %3',"Document Type","Document No.",Description));
    END;

    BEGIN
    {
      20170306 V10.00#00015 : Subform BG/PO
    }
    END.
  }
}

