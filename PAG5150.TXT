OBJECT Page 5150 Contact Segment List
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0;
  }
  PROPERTIES
  {
    Editable=No;
    CaptionML=[ENU=Contact Segment List;
               PTG=Lista Segmento Contacto];
    SourceTable=Table5077;
    DataCaptionExpr=GetCaption;
    PageType=List;
    OnAfterGetCurrRecord=BEGIN
                           CALCFIELDS("Contact Name");
                         END;

    ActionList=ACTIONS
    {
      { 1900000003;0 ;ActionContainer;
                      ActionContainerType=RelatedInformation }
      { 11      ;1   ;ActionGroup;
                      CaptionML=[ENU=&Segment;
                                 PTG=&Segmento];
                      Image=Segment }
      { 12      ;2   ;Action    ;
                      ShortCutKey=Shift+F7;
                      CaptionML=[ENU=&Card;
                                 PTG=Fi&cha];
                      ToolTipML=ENU=View detailed information about the contact segment.;
                      ApplicationArea=#RelationshipMgmt;
                      RunObject=Page 5091;
                      RunPageLink=No.=FIELD(Segment No.);
                      Image=EditLines }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 2   ;2   ;Field     ;
                ToolTipML=ENU=Specifies the number of the segment to which this segment line belongs.;
                ApplicationArea=#All;
                SourceExpr="Segment No." }

    { 4   ;2   ;Field     ;
                ToolTipML=ENU=Specifies the description of the segment line.;
                ApplicationArea=#All;
                SourceExpr=Description }

    { 6   ;2   ;Field     ;
                ToolTipML=ENU=Specifies the date the segment line was created.;
                ApplicationArea=#RelationshipMgmt;
                SourceExpr=Date }

    { 13  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the number of the contact to which this segment line applies.;
                ApplicationArea=#RelationshipMgmt;
                SourceExpr="Contact No." }

    { 3   ;2   ;Field     ;
                DrillDown=No;
                CaptionML=[ENU=Contact Name;
                           PTG=Nome Contacto];
                ToolTipML=ENU=Specifies the name of the contact to which the segment line applies. The program automatically fills in this field when you fill in the Contact No. field on the line.;
                ApplicationArea=#RelationshipMgmt;
                SourceExpr="Contact Name" }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {

    LOCAL PROCEDURE GetCaption@1() Result : Text;
    VAR
      Contact@1001 : Record 5050;
    BEGIN
      IF Contact.GET(GETFILTER("Contact Company No.")) THEN
        Result := STRSUBSTNO('%1 %2',Contact."No.",Contact.Name);

      IF Contact.GET(GETFILTER("Contact No.")) THEN
        Result := STRSUBSTNO('%1 %2 %3',Result,Contact."No.",Contact.Name);

      IF CURRENTCLIENTTYPE = CLIENTTYPE::Phone THEN
        Result := STRSUBSTNO('%1 %2',CurrPage.CAPTION,Result);
    END;

    BEGIN
    END.
  }
}

