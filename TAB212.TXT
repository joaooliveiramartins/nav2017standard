OBJECT Table 212 Job Posting Buffer
{
  OBJECT-PROPERTIES
  {
    Date=15/09/15;
    Time=12:00:00;
    Version List=NAVW19.00;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Job Posting Buffer;
               PTG=Mem. Int. Projeto];
  }
  FIELDS
  {
    { 1   ;   ;Job No.             ;Code20        ;TableRelation=Job;
                                                   CaptionML=[ENU=Job No.;
                                                              PTG=N� Projeto] }
    { 2   ;   ;Entry Type          ;Option        ;CaptionML=[ENU=Entry Type;
                                                              PTG=Tipo Movimento];
                                                   OptionCaptionML=[ENU=Usage,Sale;
                                                                    PTG=Consumo,Venda];
                                                   OptionString=Usage,Sale }
    { 3   ;   ;Posting Group Type  ;Option        ;CaptionML=[ENU=Posting Group Type;
                                                              PTG=Tipo Gr. Contabil�stico];
                                                   OptionCaptionML=[ENU=Resource,Item,G/L Account;
                                                                    PTG=Recurso,Produto,Conta C/G];
                                                   OptionString=Resource,Item,G/L Account }
    { 4   ;   ;No.                 ;Code20        ;CaptionML=[ENU=No.;
                                                              PTG=N�] }
    { 5   ;   ;Posting Group       ;Code10        ;TableRelation="Job Posting Group";
                                                   CaptionML=[ENU=Posting Group;
                                                              PTG=Gr. Contabil�stico] }
    { 6   ;   ;Global Dimension 1 Code;Code20     ;TableRelation="Dimension Value".Code WHERE (Global Dimension No.=CONST(1));
                                                   CaptionML=[ENU=Global Dimension 1 Code;
                                                              PTG=C�d. Dimens�o 1 Global];
                                                   CaptionClass='1,1,1' }
    { 7   ;   ;Global Dimension 2 Code;Code20     ;TableRelation="Dimension Value".Code WHERE (Global Dimension No.=CONST(2));
                                                   CaptionML=[ENU=Global Dimension 2 Code;
                                                              PTG=C�d. Dimens�o 2 Global];
                                                   CaptionClass='1,1,2' }
    { 8   ;   ;Unit of Measure Code;Code10        ;CaptionML=[ENU=Unit of Measure Code;
                                                              PTG=C�d. Unidade Medida] }
    { 9   ;   ;Work Type Code      ;Code10        ;TableRelation="Work Type";
                                                   CaptionML=[ENU=Work Type Code;
                                                              PTG=C�d. Tipo Trabalho] }
    { 20  ;   ;Amount              ;Decimal       ;CaptionML=[ENU=Amount;
                                                              PTG=Valor];
                                                   AutoFormatType=1 }
    { 21  ;   ;Quantity            ;Decimal       ;CaptionML=[ENU=Quantity;
                                                              PTG=Quantidade];
                                                   DecimalPlaces=0:5 }
    { 22  ;   ;Total Cost          ;Decimal       ;CaptionML=[ENU=Total Cost;
                                                              PTG=Custo Total];
                                                   AutoFormatType=1 }
    { 23  ;   ;Total Price         ;Decimal       ;CaptionML=[ENU=Total Price;
                                                              PTG=Pre�o Total];
                                                   AutoFormatType=1 }
    { 24  ;   ;Applies-to ID       ;Code50        ;CaptionML=[ENU=Applies-to ID;
                                                              PTG=Liq. por ID] }
    { 25  ;   ;Gen. Bus. Posting Group;Code10     ;TableRelation="Gen. Business Posting Group";
                                                   CaptionML=[ENU=Gen. Bus. Posting Group;
                                                              PTG=Gr. Contabil�stico Neg�cio] }
    { 26  ;   ;Gen. Prod. Posting Group;Code10    ;TableRelation="Gen. Product Posting Group";
                                                   CaptionML=[ENU=Gen. Prod. Posting Group;
                                                              PTG=Gr. Contabil�stico Produto] }
    { 27  ;   ;Additional-Currency Amount;Decimal ;CaptionML=[ENU=Additional-Currency Amount;
                                                              PTG=Valor Divisa-Adicional];
                                                   AutoFormatType=1 }
    { 28  ;   ;Dimension Entry No. ;Integer       ;CaptionML=[ENU=Dimension Entry No.;
                                                              PTG=N� Mov. Dimens�o] }
    { 29  ;   ;Variant Code        ;Code10        ;CaptionML=[ENU=Variant Code;
                                                              PTG=C�d. Variante] }
    { 480 ;   ;Dimension Set ID    ;Integer       ;TableRelation="Dimension Set Entry";
                                                   CaptionML=[ENU=Dimension Set ID;
                                                              PTG=ID Combina��o Dimens�o];
                                                   Editable=No }
  }
  KEYS
  {
    {    ;Job No.,Entry Type,Posting Group Type,No.,Variant Code,Posting Group,Gen. Bus. Posting Group,Gen. Prod. Posting Group,Unit of Measure Code,Work Type Code,Dimension Entry No.;
                                                   Clustered=Yes }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    END.
  }
}

