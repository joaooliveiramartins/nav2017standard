OBJECT Page 8619 Config. Template Subform
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Lines;
               PTG=Linhas];
    LinksAllowed=No;
    SourceTable=Table8619;
    PageType=ListPart;
    AutoSplitKey=Yes;
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 2   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the type of data in the data template.;
                           PTG=""];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=Type }

    { 4   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the name of the field in the data template.;
                           PTG=""];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Field Name" }

    { 14  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the caption of the field on which the data template is based. The caption comes from the Caption property of the field.;
                           PTG=""];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Field Caption";
                Editable=FALSE }

    { 12  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the code for the data template.;
                           PTG=""];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Template Code" }

    { 6   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the default value with reference to the data template line.;
                           PTG=""];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Default Value" }

    { 3   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies that the relationship between the table fields should not be checked. This can useful when you want to specify a value for a field that is not yet available. For example, you may want to specify a value for a payment term that is not available in the table on which you are basing you configuration. You can specify that value, select the Skip Relation Check box, and then continue to apply data without error.;
                           PTG=""];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Skip Relation Check" }

    { 8   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies whether it is required that there be data in the field in the data template. By default, the check box is selected to make a value mandatory. You can clear the check box.;
                           PTG=""];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=Mandatory }

    { 10  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies a url address. Use this field to provide a url address to a location that Specifies additional information about the field in the data template. For example, you could provide the address that Specifies information on setup considerations that the solution implementer should consider.;
                           PTG=""];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=Reference }

  }
  CODE
  {

    BEGIN
    END.
  }
}

