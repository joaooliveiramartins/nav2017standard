OBJECT Table 5203 Employee Qualification
{
  OBJECT-PROPERTIES
  {
    Date=07/09/12;
    Time=12:00:00;
    Version List=NAVW17.00;
  }
  PROPERTIES
  {
    DataCaptionFields=Employee No.;
    OnInsert=BEGIN
               Employee.GET("Employee No.");
               "Employee Status" := Employee.Status;
             END;

    OnDelete=BEGIN
               IF Comment THEN
                 ERROR(Text000);
             END;

    CaptionML=[ENU=Employee Qualification;
               PTG=Qualifica��o empregado];
    LookupPageID=Page5206;
    DrillDownPageID=Page5207;
  }
  FIELDS
  {
    { 1   ;   ;Employee No.        ;Code20        ;TableRelation=Employee;
                                                   CaptionML=[ENU=Employee No.;
                                                              PTG=N� Empregado];
                                                   NotBlank=Yes }
    { 2   ;   ;Line No.            ;Integer       ;CaptionML=[ENU=Line No.;
                                                              PTG=N� Linha] }
    { 3   ;   ;Qualification Code  ;Code10        ;TableRelation=Qualification;
                                                   OnValidate=BEGIN
                                                                Qualification.GET("Qualification Code");
                                                                Description := Qualification.Description;
                                                              END;

                                                   CaptionML=[ENU=Qualification Code;
                                                              PTG=C�d. Qualifica��o] }
    { 4   ;   ;From Date           ;Date          ;CaptionML=[ENU=From Date;
                                                              PTG=Da Data] }
    { 5   ;   ;To Date             ;Date          ;CaptionML=[ENU=To Date;
                                                              PTG=At� Data] }
    { 6   ;   ;Type                ;Option        ;CaptionML=[ENU=Type;
                                                              PTG=Tipo];
                                                   OptionCaptionML=[ENU=" ,Internal,External,Previous Position";
                                                                    PTG=" ,Interna,Externa,Posto Anterior"];
                                                   OptionString=[ ,Internal,External,Previous Position] }
    { 7   ;   ;Description         ;Text50        ;CaptionML=[ENU=Description;
                                                              PTG=Descri��o] }
    { 8   ;   ;Institution/Company ;Text50        ;CaptionML=[ENU=Institution/Company;
                                                              PTG=Institui��o/Companhia] }
    { 9   ;   ;Cost                ;Decimal       ;CaptionML=[ENU=Cost;
                                                              PTG=Custo];
                                                   AutoFormatType=1 }
    { 10  ;   ;Course Grade        ;Text50        ;CaptionML=[ENU=Course Grade;
                                                              PTG=Titula��o] }
    { 11  ;   ;Employee Status     ;Option        ;CaptionML=[ENU=Employee Status;
                                                              PTG=Estado empregado];
                                                   OptionCaptionML=[ENU=Active,Inactive,Terminated;
                                                                    PTG=Ativo,Inativo,Terminado];
                                                   OptionString=Active,Inactive,Terminated;
                                                   Editable=No }
    { 12  ;   ;Comment             ;Boolean       ;FieldClass=FlowField;
                                                   CalcFormula=Exist("Human Resource Comment Line" WHERE (Table Name=CONST(Employee Qualification),
                                                                                                          No.=FIELD(Employee No.),
                                                                                                          Table Line No.=FIELD(Line No.)));
                                                   CaptionML=[ENU=Comment;
                                                              PTG=Coment�rio];
                                                   Editable=No }
    { 13  ;   ;Expiration Date     ;Date          ;CaptionML=[ENU=Expiration Date;
                                                              PTG=Data Expira��o] }
  }
  KEYS
  {
    {    ;Employee No.,Line No.                   ;Clustered=Yes }
    {    ;Qualification Code                       }
  }
  FIELDGROUPS
  {
  }
  CODE
  {
    VAR
      Text000@1000 : TextConst 'ENU=You cannot delete employee qualification information if there are comments associated with it.;PTG=N�o pode eliminar a qualifica��o se tem coment�rios associados.';
      Qualification@1001 : Record 5202;
      Employee@1002 : Record 5200;

    BEGIN
    END.
  }
}

