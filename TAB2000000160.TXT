OBJECT Table 2000000160 NAV App
{
  OBJECT-PROPERTIES
  {
    Date=13/04/17;
    Time=14:36:27;
    Version List=NAVW110.0;
  }
  PROPERTIES
  {
    DataPerCompany=No;
    CaptionML=[ENU=NAV App;
               PTG=Nav App];
  }
  FIELDS
  {
    { 1   ;   ;Package ID          ;GUID          ;CaptionML=[ENU=Package ID;
                                                              PTG=ID Pacote] }
    { 2   ;   ;ID                  ;GUID          ;CaptionML=[ENU=ID;
                                                              PTG=ID] }
    { 3   ;   ;Name                ;Text250       ;CaptionML=[ENU=Name;
                                                              PTG=Nome] }
    { 4   ;   ;Publisher           ;Text250       ;CaptionML=[ENU=Publisher;
                                                              PTG=Publicador] }
    { 5   ;   ;Version Major       ;Integer       ;CaptionML=[ENU=Version Major;
                                                              PTG=Vers�o Major] }
    { 6   ;   ;Version Minor       ;Integer       ;CaptionML=[ENU=Version Minor;
                                                              PTG=Vers�o Minor] }
    { 7   ;   ;Version Build       ;Integer       ;CaptionML=[ENU=Version Build;
                                                              PTG=Vers�o Build] }
    { 8   ;   ;Version Revision    ;Integer       ;CaptionML=[ENU=Version Revision;
                                                              PTG=Vers�o Revis�o] }
    { 9   ;   ;Compatibility Major ;Integer       ;CaptionML=[ENU=Compatibility Major;
                                                              PTG=Compatibilidade Major] }
    { 10  ;   ;Compatibility Minor ;Integer       ;CaptionML=[ENU=Compatibility Minor;
                                                              PTG=Compatibilidade Minor] }
    { 11  ;   ;Compatibility Build ;Integer       ;CaptionML=[ENU=Compatibility Build;
                                                              PTG=Compatibilidade Build] }
    { 12  ;   ;Compatibility Revision;Integer     ;CaptionML=[ENU=Compatibility Revision;
                                                              PTG=Compatibilidade Revis�o] }
    { 13  ;   ;Brief               ;Text250       ;CaptionML=[ENU=Brief;
                                                              PTG=Resumo] }
    { 14  ;   ;Description         ;BLOB          ;CaptionML=[ENU=Description;
                                                              PTG=Descri��o] }
    { 15  ;   ;Privacy Statement   ;Text250       ;CaptionML=[ENU=Privacy Statement;
                                                              PTG=Declara��o de Privacidade] }
    { 16  ;   ;EULA                ;Text250       ;CaptionML=[ENU=EULA;
                                                              PTG=EULA] }
    { 17  ;   ;Url                 ;Text250       ;CaptionML=[ENU=Url;
                                                              PTG=Url] }
    { 18  ;   ;Help                ;Text250       ;CaptionML=[ENU=Help;
                                                              PTG=Ajuda] }
    { 19  ;   ;Logo                ;Media         ;CaptionML=[ENU=Logo;
                                                              PTG=Log�tipo] }
    { 20  ;   ;Screenshots         ;MediaSet      ;CaptionML=[ENU=Screenshots;
                                                              PTG=Capturas de Ecr�] }
    { 21  ;   ;Blob                ;BLOB          ;CaptionML=[ENU=Blob;
                                                              PTG=Blob] }
    { 22  ;   ;responseUrl         ;Text250       ;FieldClass=FlowField;
                                                   CaptionML=[ENU=responseUrl;
                                                              PTG=responseUrl] }
    { 23  ;   ;requestId           ;Text250       ;FieldClass=FlowField;
                                                   CaptionML=[ENU=requestId;
                                                              PTG=requestId] }
    { 24  ;   ;Installed           ;Boolean       ;FieldClass=FlowField;
                                                   CalcFormula=Exist("NAV App Installed App" WHERE (Package ID=FIELD(Package ID)));
                                                   CaptionML=[ENU=Installed;
                                                              PTG=Installed] }
  }
  KEYS
  {
    {    ;Package ID                              ;Clustered=Yes }
    {    ;Name                                     }
    {    ;Publisher                                }
    {    ;Name,Version Major,Version Minor,Version Build,Version Revision }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    END.
  }
}

