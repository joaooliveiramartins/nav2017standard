OBJECT Page 5201 Employee List
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0;
  }
  PROPERTIES
  {
    Editable=No;
    CaptionML=[ENU=Employee List;
               PTG=Lista Empregado];
    SourceTable=Table5200;
    PageType=List;
    CardPageID=Employee Card;
    ActionList=ACTIONS
    {
      { 1900000003;0 ;ActionContainer;
                      ActionContainerType=RelatedInformation }
      { 33      ;1   ;ActionGroup;
                      CaptionML=[ENU=E&mployee;
                                 PTG=Em&pregado];
                      Image=Employee }
      { 43      ;2   ;Action    ;
                      CaptionML=[ENU=Co&mments;
                                 PTG=Co&ment�rios];
                      RunObject=Page 5222;
                      RunPageLink=Table Name=CONST(Employee),
                                  No.=FIELD(No.);
                      Image=ViewComments }
      { 20      ;2   ;ActionGroup;
                      CaptionML=[ENU=Dimensions;
                                 PTG=Dimens�es];
                      Image=Dimensions }
      { 184     ;3   ;Action    ;
                      ShortCutKey=Shift+Ctrl+D;
                      CaptionML=[ENU=Dimensions-Single;
                                 PTG=Dimens�es-Singular];
                      ToolTipML=[ENU=View or edit the single set of dimensions that are set up for the selected record.;
                                 PTG=""];
                      RunObject=Page 540;
                      RunPageLink=Table ID=CONST(5200),
                                  No.=FIELD(No.);
                      Image=Dimensions }
      { 19      ;3   ;Action    ;
                      AccessByPermission=TableData 348=R;
                      CaptionML=[ENU=Dimensions-&Multiple;
                                 PTG=Dimens�es-&M�ltipla];
                      ToolTipML=[ENU=View or edit dimensions for a group of records. You can assign dimension codes to transactions to distribute costs and analyze historical information.;
                                 PTG=""];
                      Image=DimensionSets;
                      OnAction=VAR
                                 Employee@1001 : Record 5200;
                                 DefaultDimMultiple@1002 : Page 542;
                               BEGIN
                                 CurrPage.SETSELECTIONFILTER(Employee);
                                 DefaultDimMultiple.SetMultiEmployee(Employee);
                                 DefaultDimMultiple.RUNMODAL;
                               END;
                                }
      { 44      ;2   ;Action    ;
                      CaptionML=[ENU=&Picture;
                                 PTG=Ima&gem];
                      RunObject=Page 5202;
                      RunPageLink=No.=FIELD(No.);
                      Image=Picture }
      { 45      ;2   ;Action    ;
                      Name=AlternativeAddresses;
                      CaptionML=[ENU=&Alternative Addresses;
                                 PTG=E&ndere�o Alternativo];
                      RunObject=Page 5204;
                      RunPageLink=Employee No.=FIELD(No.);
                      Image=Addresses }
      { 46      ;2   ;Action    ;
                      CaptionML=[ENU=&Relatives;
                                 PTG=F&amiliares];
                      RunObject=Page 5209;
                      RunPageLink=Employee No.=FIELD(No.);
                      Image=Relatives }
      { 47      ;2   ;Action    ;
                      CaptionML=[ENU=Mi&sc. Article Information;
                                 PTG=Informa��o Artigo&s Div.];
                      RunObject=Page 5219;
                      RunPageLink=Employee No.=FIELD(No.);
                      Image=Filed }
      { 48      ;2   ;Action    ;
                      CaptionML=[ENU=Co&nfidential Information;
                                 PTG=&Informa��o confidencial];
                      RunObject=Page 5221;
                      RunPageLink=Employee No.=FIELD(No.);
                      Image=Lock }
      { 49      ;2   ;Action    ;
                      CaptionML=[ENU=Q&ualifications;
                                 PTG=&Qualifica��es];
                      RunObject=Page 5206;
                      RunPageLink=Employee No.=FIELD(No.);
                      Image=Certificate }
      { 50      ;2   ;Action    ;
                      CaptionML=[ENU=A&bsences;
                                 PTG=Au&s�ncias];
                      RunObject=Page 5211;
                      RunPageLink=Employee No.=FIELD(No.);
                      Image=Absence }
      { 51      ;2   ;Separator  }
      { 54      ;2   ;Action    ;
                      CaptionML=[ENU=Absences by Ca&tegories;
                                 PTG=A&us�ncias p/ Categorias];
                      RunObject=Page 5226;
                      RunPageLink=No.=FIELD(No.),
                                  Employee No. Filter=FIELD(No.);
                      Image=AbsenceCategory }
      { 55      ;2   ;Action    ;
                      CaptionML=[ENU=Misc. Articles &Overview;
                                 PTG=&Vista Artigos Diversos];
                      RunObject=Page 5228;
                      Image=FiledOverview }
      { 56      ;2   ;Action    ;
                      CaptionML=[ENU=Con&fidential Info. Overview;
                                 PTG=Vista Info. Con&fidencial];
                      RunObject=Page 5229;
                      Image=ConfidentialOverview }
      { 57      ;    ;ActionContainer;
                      ActionContainerType=ActionItems }
      { 58      ;1   ;Action    ;
                      CaptionML=[ENU=Absence Registration;
                                 PTG=Registo Aus�ncia];
                      RunObject=Page 5212;
                      Promoted=Yes;
                      PromotedIsBig=Yes;
                      Image=Absence;
                      PromotedCategory=Process }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 2   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies a number for the employee.;
                           PTG=""];
                SourceExpr="No." }

    { 17  ;2   ;Field     ;
                Name=FullName;
                CaptionML=[ENU=Full Name;
                           PTG=Nome completo];
                ToolTipML=[ENU=Specifies the full name of the employee.;
                           PTG=""];
                SourceExpr=FullName }

    { 4   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the employee's first name.;
                           PTG=""];
                SourceExpr="First Name";
                Visible=FALSE }

    { 6   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the employee's middle name.;
                           PTG=""];
                SourceExpr="Middle Name";
                Visible=FALSE }

    { 8   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the employee's last name.;
                           PTG=""];
                SourceExpr="Last Name";
                Visible=FALSE }

    { 10  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the employee's initials.;
                           PTG=""];
                SourceExpr=Initials;
                Visible=FALSE }

    { 12  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the employee's job title.;
                           PTG=""];
                SourceExpr="Job Title" }

    { 22  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the postal code of the address.;
                           PTG=""];
                SourceExpr="Post Code";
                Visible=FALSE }

    { 84  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the country/region code.;
                           PTG=""];
                SourceExpr="Country/Region Code";
                Visible=FALSE }

    { 80  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the employee's telephone extension.;
                           PTG=""];
                SourceExpr=Extension }

    { 26  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the employee's telephone number.;
                           PTG=""];
                SourceExpr="Phone No.";
                Visible=FALSE }

    { 28  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the employee's mobile telephone number.;
                           PTG=""];
                SourceExpr="Mobile Phone No.";
                Visible=FALSE }

    { 30  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the employee's email address.;
                           PTG=""];
                SourceExpr="E-Mail";
                Visible=FALSE }

    { 52  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies a statistics group code to assign to the employee for statistical purposes.;
                           PTG=""];
                SourceExpr="Statistics Group Code";
                Visible=FALSE }

    { 72  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies a resource number for the employee, if the employee is a resource in Resources Planning.;
                           PTG=""];
                SourceExpr="Resource No.";
                Visible=FALSE }

    { 14  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies a search name for the employee.;
                           PTG=""];
                SourceExpr="Search Name" }

    { 16  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies if a comment has been entered for this entry.;
                           PTG=""];
                SourceExpr=Comment }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                Visible=TRUE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {

    BEGIN
    END.
  }
}

