OBJECT Table 99000846 Planning Buffer
{
  OBJECT-PROPERTIES
  {
    Date=07/09/12;
    Time=12:00:00;
    Version List=NAVW17.00;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Planning Buffer;
               PTG=Mem. Interna Planeamento];
  }
  FIELDS
  {
    { 1   ;   ;Buffer No.          ;Integer       ;CaptionML=[ENU=Buffer No.;
                                                              PTG=N� Mem. Int.] }
    { 2   ;   ;Date                ;Date          ;CaptionML=[ENU=Date;
                                                              PTG=Data] }
    { 3   ;   ;Document Type       ;Option        ;CaptionML=[ENU=Document Type;
                                                              PTG=Tipo Documento];
                                                   OptionCaptionML=[ENU=Requisition Line,Planned Prod. Order Comp.,Firm Planned Prod. Order Comp.,Released Prod. Order Comp.,Planning Comp.,Sales Order,Planned Prod. Order,Planning Line,Req. Worksheet Line,Firm Planned Prod. Order,Released Prod. Order,Purchase Order,Quantity at Inventory,Service Order,Transfer,Job Order,Assembly Order,Assembly Order Line,Production Forecast-Sales,Production Forecast-Component;
                                                                    PTG=Linha Requisi��o,Comp. Enc. Prod. Planeada,Comp. Enc. Prod. Planeada Firme,Comp. Enc. Prod. Lan�ada,Comp. Planeamento,Encomenda Venda,Enc. Prod. Planeada,Linha Planeamento,Linha Folha Req.,Enc. Prod. Planeada Firma,Enc.Prod. Lan�ada,Encome];
                                                   OptionString=Requisition Line,Planned Prod. Order Comp.,Firm Planned Prod. Order Comp.,Released Prod. Order Comp.,Planning Comp.,Sales Order,Planned Prod. Order,Planning Line,Req. Worksheet Line,Firm Planned Prod. Order,Released Prod. Order,Purchase Order,Quantity at Inventory,Service Order,Transfer,Job Order,Assembly Order,Assembly Order Line,Production Forecast-Sales,Production Forecast-Component }
    { 4   ;   ;Document No.        ;Code20        ;CaptionML=[ENU=Document No.;
                                                              PTG=N� Documento] }
    { 6   ;   ;Item No.            ;Code20        ;TableRelation=Item;
                                                   CaptionML=[ENU=Item No.;
                                                              PTG=N� Produto] }
    { 7   ;   ;Description         ;Text50        ;CaptionML=[ENU=Description;
                                                              PTG=Descri��o] }
    { 8   ;   ;Gross Requirement   ;Decimal       ;CaptionML=[ENU=Gross Requirement;
                                                              PTG=Necessidades Brutas];
                                                   DecimalPlaces=0:5 }
    { 10  ;   ;Planned Receipts    ;Decimal       ;CaptionML=[ENU=Planned Receipts;
                                                              PTG=Rece��es Planeadas];
                                                   DecimalPlaces=0:5 }
    { 11  ;   ;Scheduled Receipts  ;Decimal       ;CaptionML=[ENU=Scheduled Receipts;
                                                              PTG=Rece��es Programadas];
                                                   DecimalPlaces=0:5 }
  }
  KEYS
  {
    {    ;Buffer No.                              ;Clustered=Yes }
    {    ;Item No.,Date                            }
  }
  FIELDGROUPS
  {
  }
  CODE
  {

    BEGIN
    END.
  }
}

