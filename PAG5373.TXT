OBJECT Page 5373 CRM Synch. Job Queue
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0;
  }
  PROPERTIES
  {
    Editable=No;
    CaptionML=[ENU=Microsoft Dynamics CRM Synch. Job Queue;
               PTG=Sinc. Fila Tarefas Microsoft Dynamics CRM];
    SourceTable=Table472;
    SourceTableView=SORTING(Priority,Last Ready State);
    PageType=List;
    RefreshOnActivate=Yes;
    OnOpenPage=BEGIN
                 SETRANGE(Status,Status::Error);
                 SETRANGE("Object ID to Run",CODEUNIT::"Integration Synch. Job Runner");
               END;

    OnAfterGetRecord=BEGIN
                       StatusIsError := Status = Status::Error;
                     END;

    ActionList=ACTIONS
    {
      { 6       ;    ;ActionContainer;
                      CaptionML=[ENU=Actions;
                                 PTG=A��es];
                      ActionContainerType=ActionItems }
      { 14      ;1   ;Action    ;
                      Name=EditJob;
                      ShortCutKey=Return;
                      CaptionML=[ENU=Edit Job;
                                 PTG=Editar Tarefa];
                      ToolTipML=ENU=Change the settings for the job queue entry.;
                      ApplicationArea=#Suite;
                      RunObject=Page 673;
                      RunPageOnRec=Yes;
                      Image=Edit }
    }
  }
  CONTROLS
  {
    { 1   ;0   ;Container ;
                ContainerType=ContentArea }

    { 2   ;1   ;Group     ;
                Name=Group;
                GroupType=Repeater }

    { 3   ;2   ;Field     ;
                CaptionML=[ENU=Date;
                           PTG=Data];
                ToolTipML=ENU=Specifies the date and time when the Microsoft Dynamics CRM synchronization job was set to Ready and sent to the job queue.;
                ApplicationArea=#Suite;
                SourceExpr="Last Ready State" }

    { 11  ;2   ;Field     ;
                ToolTipML=ENU=Specifies the latest error message that was received from the job queue entry. You can view the error message if the Status field is set to Error. The field can contain up to 250 characters.;
                ApplicationArea=#Suite;
                SourceExpr="Error Message";
                Style=Attention;
                StyleExpr=StatusIsError }

  }
  CODE
  {
    VAR
      StatusIsError@1000 : Boolean INDATASET;

    BEGIN
    END.
  }
}

