OBJECT Report 9001 Copy User Group
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Copy User Group;
               PTG=Copiar Grupo de Utilizadores];
    ProcessingOnly=Yes;
  }
  DATASET
  {
    { 1   ;    ;DataItem;                    ;
               DataItemTable=Table9000;
               DataItemTableView=SORTING(Code);
               OnAfterGetRecord=VAR
                                  NewUserGroup@1000 : Record 9000;
                                  UserGroupPermissionSet@1001 : Record 9003;
                                  NewUserGroupPermissionSet@1002 : Record 9003;
                                BEGIN
                                  NewUserGroup.INIT;
                                  NewUserGroup.Code := NewUserGroupCode;
                                  NewUserGroup.Name := Name;
                                  NewUserGroup.INSERT;
                                  UserGroupPermissionSet.SETRANGE("User Group Code",Code);
                                  IF UserGroupPermissionSet.FINDSET THEN
                                    REPEAT
                                      NewUserGroupPermissionSet := UserGroupPermissionSet;
                                      NewUserGroupPermissionSet."User Group Code" := NewUserGroup.Code;
                                      NewUserGroupPermissionSet.INSERT;
                                    UNTIL UserGroupPermissionSet.NEXT = 0;
                                END;
                                 }

  }
  REQUESTPAGE
  {
    PROPERTIES
    {
    }
    CONTROLS
    {
      { 4   ;0   ;Container ;
                  ContainerType=ContentArea }

      { 2   ;1   ;Group     ;
                  CaptionML=[ENU=Options;
                             PTG=Op��es] }

      { 1   ;2   ;Field     ;
                  Name=NewUserGroupCode;
                  CaptionML=[ENU=New User Group Code;
                             PTG=Novo C�digo de Grupo de Utilizadores];
                  ToolTipML=ENU=Specifies the code of the user group that result from the copying.;
                  ApplicationArea=#Basic,#Suite;
                  NotBlank=Yes;
                  SourceExpr=NewUserGroupCode }

    }
  }
  LABELS
  {
  }
  CODE
  {
    VAR
      NewUserGroupCode@1000 : Code[20];

    BEGIN
    END.
  }
  RDLDATA
  {
  }
}

