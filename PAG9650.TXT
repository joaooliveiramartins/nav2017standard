OBJECT Page 9650 Custom Report Layouts
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Custom Report Layouts;
               PTG=Layout Mapas Personalizados];
    InsertAllowed=No;
    SourceTable=Table9650;
    SourceTableView=SORTING(Report ID,Company Name,Type);
    PageType=List;
    OnOpenPage=VAR
                 FileMgt@1000 : Codeunit 419;
               BEGIN
                 IsWindowsClient := FileMgt.IsWindowsClient;
                 PageName := CurrPage.CAPTION;
                 CurrPage.CAPTION := GetPageCaption;
               END;

    OnClosePage=VAR
                  ReportLayoutSelection@1000 : Record 9651;
                BEGIN
                  ReportLayoutSelection.SetTempLayoutSelected('');
                END;

    OnAfterGetRecord=BEGIN
                       CanEdit := IsWindowsClient;
                     END;

    OnAfterGetCurrRecord=VAR
                           ReportLayoutSelection@1000 : Record 9651;
                         BEGIN
                           CurrPage.CAPTION := GetPageCaption;
                           ReportLayoutSelection.SetTempLayoutSelected('');
                           IsNotBuiltIn := NOT "Built-In";
                         END;

    ActionList=ACTIONS
    {
      { 14      ;    ;ActionContainer;
                      ActionContainerType=NewDocumentItems }
      { 25      ;1   ;Action    ;
                      Name=NewLayout;
                      Ellipsis=Yes;
                      CaptionML=[ENU=New;
                                 PTG=Novo];
                      ToolTipML=[ENU=Create a new built-in layout for reports.;
                                 PTG=""];
                      ApplicationArea=#Basic,#Suite;
                      Promoted=Yes;
                      Image=NewDocument;
                      OnAction=BEGIN
                                 InsertBuiltInLayout;
                               END;
                                }
      { 15      ;1   ;Action    ;
                      Name=CopyRec;
                      CaptionML=[ENU=Copy;
                                 PTG=Copiar];
                      ToolTipML=[ENU=Make a copy of a built-in layout for reports.;
                                 PTG=""];
                      ApplicationArea=#Basic,#Suite;
                      Promoted=Yes;
                      Image=CopyDocument;
                      OnAction=BEGIN
                                 CopyRecord;
                               END;
                                }
      { 17      ;0   ;ActionContainer;
                      ActionContainerType=ActionItems }
      { 23      ;1   ;Action    ;
                      Name=ExportWordXMLPart;
                      CaptionML=[ENU=Export Word XML Part;
                                 PTG=Exportar Word XML Part];
                      ToolTipML=[ENU=Export to a Word XML file.;
                                 PTG=""];
                      ApplicationArea=#Basic,#Suite;
                      Image=Export;
                      OnAction=BEGIN
                                 ExportSchema('',TRUE);
                               END;
                                }
      { 16      ;1   ;Action    ;
                      Name=ImportLayout;
                      CaptionML=[ENU=Import Layout;
                                 PTG=Importar Layout];
                      ToolTipML=[ENU=Import a Word file.;
                                 PTG=""];
                      ApplicationArea=#Basic,#Suite;
                      Promoted=Yes;
                      Image=Import;
                      PromotedCategory=Process;
                      OnAction=BEGIN
                                 ImportLayout('');
                               END;
                                }
      { 19      ;1   ;Action    ;
                      Name=ExportLayout;
                      CaptionML=[ENU=Export Layout;
                                 PTG=Exportar Layout];
                      ToolTipML=[ENU=Export a Word file.;
                                 PTG=""];
                      ApplicationArea=#Basic,#Suite;
                      Promoted=Yes;
                      Image=Export;
                      PromotedCategory=Process;
                      OnAction=BEGIN
                                 ExportLayout('',TRUE);
                               END;
                                }
      { 24      ;1   ;Action    ;
                      Name=EditLayout;
                      CaptionML=[ENU=Edit Layout;
                                 PTG=Editar Layout];
                      ToolTipML=[ENU=Edit the report layout in Word, make changes to the file, and close Word to continue.;
                                 PTG=""];
                      ApplicationArea=#Basic,#Suite;
                      Promoted=Yes;
                      Visible=CanEdit;
                      Image=EditReminder;
                      PromotedCategory=Process;
                      OnAction=BEGIN
                                 EditLayout;
                               END;
                                }
      { 20      ;1   ;Action    ;
                      Name=UpdateWordLayout;
                      CaptionML=[ENU=Update Layout;
                                 PTG=Atualizar Layout];
                      ToolTipML=[ENU=Update specific report layouts or all custom report layouts that might be affected by dataset changes.;
                                 PTG=""];
                      ApplicationArea=#Basic,#Suite;
                      Image=UpdateXML;
                      OnAction=BEGIN
                                 IF CanBeModified THEN
                                   IF UpdateLayout(FALSE,FALSE) THEN
                                     MESSAGE(UpdateSuccesMsg,FORMAT(Type))
                                   ELSE
                                     MESSAGE(UpdateNotRequiredMsg,FORMAT(Type));
                               END;
                                }
      { 13      ;    ;ActionContainer;
                      ActionContainerType=Reports }
      { 21      ;1   ;Action    ;
                      Name=RunReport;
                      CaptionML=[ENU=Run Report;
                                 PTG=Executar Mapa];
                      ToolTipML=[ENU=Run a test report.;
                                 PTG=""];
                      ApplicationArea=#Basic,#Suite;
                      Promoted=Yes;
                      Image=Report;
                      PromotedCategory=Report;
                      OnAction=BEGIN
                                 RunCustomReport;
                               END;
                                }
    }
  }
  CONTROLS
  {
    { 1   ;0   ;Container ;
                ContainerType=ContentArea }

    { 2   ;1   ;Group     ;
                Name=Group;
                GroupType=Repeater }

    { 6   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the Code. This field is intended only for internal use.;
                           PTG=""];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=Code;
                Visible=FALSE;
                Editable=IsNotBuiltIn }

    { 3   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the ID of the report that uses the report layout.;
                           PTG=""];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Report ID";
                Enabled=FALSE }

    { 4   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the name of the report that the report layout applies to.;
                           PTG=""];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Report Name";
                Enabled=FALSE }

    { 18  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies a description of the report layout.;
                           PTG=""];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=Description }

    { 5   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the Microsoft Dynamics NAV company that the report layout applies to. You to create report layouts that can only be used on reports when they are run for a specific to a company. If the field is blank, then the layout will be available for use in all companies.;
                           PTG=""];
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Company Name" }

    { 7   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the file type of the report layout. The following table includes the types that are available:;
                           PTG=""];
                ApplicationArea=#Basic,#Suite;
                SourceExpr=Type;
                Editable=FALSE }

    { 8   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the date and time of the last change to the report layout entry.;
                           PTG=""];
                SourceExpr="Last Modified";
                Visible=FALSE }

    { 9   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the Microsoft Dynamics NAV user who made the last change to the report layout entry.;
                           PTG=""];
                SourceExpr="Last Modified by User";
                Visible=FALSE }

    { 10  ;0   ;Container ;
                ContainerType=FactBoxArea }

    { 11  ;1   ;Part      ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=Notes }

    { 12  ;1   ;Part      ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

  }
  CODE
  {
    VAR
      IsWindowsClient@1001 : Boolean;
      CanEdit@1002 : Boolean;
      UpdateSuccesMsg@1004 : TextConst 'ENU=The %1 layout has been updated to use the current report design.;PTG=O layout  %1  foi atualizado para usar o desenho do mapa atual.';
      UpdateNotRequiredMsg@1005 : TextConst 'ENU=The %1 layout is up-to-date. No further updates are required.;PTG=O layout %1 est� atualizado. N�o ser�o necess�rias mais atualiza��es.';
      PageName@1000 : Text;
      CaptionTxt@1003 : TextConst '@@@={Locked};ENU=%1 - %2 %3;PTG=%1 - %2 %3';
      IsNotBuiltIn@1006 : Boolean;

    LOCAL PROCEDURE GetPageCaption@2() : Text;
    VAR
      AllObjWithCaption@1000 : Record 2000000058;
      FilterText@1001 : Text;
      ReportID@1002 : Integer;
    BEGIN
      IF "Report ID" <> 0 THEN
        EXIT(STRSUBSTNO(CaptionTxt,PageName,"Report ID","Report Name"));
      FILTERGROUP(4);
      FilterText := GETFILTER("Report ID");
      FILTERGROUP(0);
      IF EVALUATE(ReportID,FilterText) THEN
        IF AllObjWithCaption.GET(AllObjWithCaption."Object Type"::Report,ReportID) THEN
          EXIT(STRSUBSTNO(CaptionTxt,PageName,ReportID,AllObjWithCaption."Object Caption"));
      EXIT(PageName);
    END;

    BEGIN
    END.
  }
}

