OBJECT Page 9330 Inventory Movements
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0;
  }
  PROPERTIES
  {
    Editable=No;
    CaptionML=[ENU=Inventory Movements;
               PTG=Movs. Invent rio];
    SourceTable=Table5766;
    SourceTableView=WHERE(Type=CONST(Invt. Movement));
    PageType=List;
    CardPageID=Inventory Movement;
    OnOpenPage=BEGIN
                 ErrorIfUserIsNotWhseEmployee;
               END;

    OnFindRecord=BEGIN
                   EXIT(FindFirstAllowedRec(Which));
                 END;

    OnNextRecord=BEGIN
                   EXIT(FindNextAllowedRec(Steps));
                 END;

    ActionList=ACTIONS
    {
      { 1900000003;0 ;ActionContainer;
                      ActionContainerType=RelatedInformation }
      { 17      ;1   ;ActionGroup;
                      CaptionML=[ENU=&Movement;
                                 PTG=&Movimento];
                      Image=CreateMovement }
      { 31      ;2   ;Action    ;
                      CaptionML=[ENU=Co&mments;
                                 PTG=Co&ment rios];
                      RunObject=Page 5776;
                      RunPageLink=Table Name=CONST(Whse. Activity Header),
                                  Type=FIELD(Type),
                                  No.=FIELD(No.);
                      Image=ViewComments }
      { 32      ;2   ;Action    ;
                      CaptionML=[ENU=&Registered Invt. Movement;
                                 PTG=Movs. Invent rio &Registados];
                      RunObject=Page 7384;
                      RunPageView=SORTING(Invt. Movement No.);
                      RunPageLink=Invt. Movement No.=FIELD(No.);
                      Image=RegisteredDocs }
      { 33      ;2   ;Action    ;
                      CaptionML=[ENU=Source Document;
                                 PTG=Documento Origem];
                      Image=Order;
                      OnAction=VAR
                                 WMSMgt@1000 : Codeunit 7302;
                               BEGIN
                                 WMSMgt.ShowSourceDocCard("Source Type","Source Subtype","Source No.");
                               END;
                                }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 2   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the number of the warehouse header.;
                           PTG=""];
                SourceExpr="No." }

    { 25  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the type of document to which the line relates, including sales order, purchase order, or transfer order.;
                           PTG=""];
                SourceExpr="Source Document" }

    { 27  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the number of the source document from which the activity originated.;
                           PTG=""];
                SourceExpr="Source No." }

    { 15  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the type of activity, such as Put-away, that the warehouse performs on the lines that are attached to the header.;
                           PTG=""];
                SourceExpr=Type;
                Visible=FALSE }

    { 4   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the code for the location where the warehouse activity takes place.;
                           PTG=""];
                SourceExpr="Location Code" }

    { 8   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies information about the type of destination, such as customer or vendor, associated with the warehouse activity.;
                           PTG=""];
                SourceExpr="Destination Type" }

    { 10  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the number or the code of the customer or vendor that the line is linked to.;
                           PTG=""];
                SourceExpr="Destination No." }

    { 23  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the external document number for the source document to which the warehouse activity is related.;
                           PTG=""];
                SourceExpr="External Document No." }

    { 19  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the ID of the user who is responsible for the document.;
                           PTG=""];
                SourceExpr="Assigned User ID";
                Visible=FALSE }

    { 21  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the number of lines in the warehouse activity document.;
                           PTG=""];
                SourceExpr="No. of Lines" }

    { 6   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the method by which the lines are sorted on the warehouse header, such as Item or Document.;
                           PTG=""];
                SourceExpr="Sorting Method";
                Visible=FALSE }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                Visible=TRUE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {

    BEGIN
    END.
  }
}

