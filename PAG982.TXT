OBJECT Page 982 Payment Registration Setup
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Payment Registration Setup;
               PTG=Configura��o Registo Pagamentos];
    InsertAllowed=No;
    DeleteAllowed=No;
    SourceTable=Table980;
    DataCaptionExpr=PageCaption;
    PageType=StandardDialog;
    OnOpenPage=BEGIN
                 IF NOT GET(USERID) THEN BEGIN
                   IF GET THEN;

                   "User ID" := USERID;
                   INSERT;
                 END;

                 PageCaption := '';
               END;

    OnQueryClosePage=BEGIN
                       IF CloseAction = ACTION::LookupOK THEN
                         EXIT(ValidateMandatoryFields(TRUE));
                     END;

  }
  CONTROLS
  {
    { 1   ;    ;Container ;
                ContainerType=ContentArea }

    { 2   ;1   ;Group     ;
                CaptionML=[ENU=General;
                           PTG=Geral];
                GroupType=Group }

    { 3   ;2   ;Field     ;
                ToolTipML=ENU=Specifies the name of the journal template that the Payment Registration window is based on.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Journal Template Name" }

    { 4   ;2   ;Field     ;
                ToolTipML=ENU=Specifies the name of the journal batch that the Payment Registration window is based on.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Journal Batch Name" }

    { 5   ;2   ;Field     ;
                CaptionML=[ENU=Balancing Account Type;
                           PTG=Tipo de Contrapartida];
                ToolTipML=ENU=Specifies the type of account that is used as the balancing account for payments. The field is filled according to the selection in the Journal Batch Name field.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Bal. Account Type" }

    { 6   ;2   ;Field     ;
                CaptionML=[ENU=Balancing Account;
                           PTG=Contrapartida];
                ToolTipML=ENU=Specifies the account number that is used as the balancing account for payments.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Bal. Account No." }

    { 7   ;2   ;Field     ;
                CaptionML=[ENU=Use this Account as Default;
                           PTG=Utilize esta conta por defeito];
                ToolTipML=ENU=Specifies if the Date Received and the Amount Received fields are automatically filled when you select the Payment Made check box.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Use this Account as Def." }

    { 8   ;2   ;Field     ;
                CaptionML=[ENU=Automatically Fill Date Received;
                           PTG=Preenche automaticamente a Data de Rece��o];
                ToolTipML=ENU=Specifies if the account in the Bal. Account No. field is used for all payments.;
                ApplicationArea=#Basic,#Suite;
                SourceExpr="Auto Fill Date Received" }

  }
  CODE
  {
    VAR
      PageCaption@1000 : Text[10];

    BEGIN
    END.
  }
}

