OBJECT Report 296 Batch Post Sales Orders
{
  OBJECT-PROPERTIES
  {
    Date=12/04/17;
    Time=15:13:24;
    Modified=Yes;
    Version List=NAVW19.00,NAVPTSS71.10;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Batch Post Sales Orders;
               PTG=Reg. Lotes Encomendas Venda];
    ProcessingOnly=Yes;
  }
  DATASET
  {
    { 6640;    ;DataItem;                    ;
               DataItemTable=Table36;
               DataItemTableView=SORTING(Document Type,No.)
                                 WHERE(Document Type=CONST(Order));
               ReqFilterHeadingML=[ENU=Sales Order;
                                   PTG=Encomenda de Venda];
               OnPreDataItem=BEGIN
                               IF ReplacePostingDate AND (PostingDateReq = 0D) THEN
                                 ERROR(Text000);

                               CounterTotal := COUNT;
                               Window.OPEN(Text001);
                             END;

               OnAfterGetRecord=VAR
                                  ApprovalsMgmt@1000 : Codeunit 1535;
                                BEGIN
                                  IF ApprovalsMgmt.IsSalesApprovalsWorkflowEnabled("Sales Header") OR (Status = Status::"Pending Approval") THEN
                                    CurrReport.SKIP;

                                  IF CalcInvDisc THEN
                                    CalculateInvoiceDiscount;

                                  Counter := Counter + 1;
                                  Window.UPDATE(1,"No.");
                                  Window.UPDATE(2,ROUND(Counter / CounterTotal * 10000,1));

                                  IF NOT IsApprovedForPostingBatch THEN
                                    CurrReport.SKIP;

                                  BatchConfirmUpdateDeferralDate(BatchConfirm,ReplacePostingDate,PostingDateReq);

                                  Ship := ShipReq;
                                  Invoice := InvReq;
                                  CLEAR(SalesPost);
                                  SalesPost.SetPostingDate(ReplacePostingDate,ReplaceDocumentDate,PostingDateReq);

                                  SalesPost.ATSkipMessages(TRUE); //soft,n

                                  IF SalesPost.RUN("Sales Header") THEN BEGIN
                                    CounterOK := CounterOK + 1;
                                    IF MARKEDONLY THEN
                                      MARK(FALSE);
                                  END;

                                  CounterSourceDocSentOk := CounterSourceDocSentOk + SalesPost.GetSuccessCounter; //soft,n
                                  {<<<<<<<}
                                END;

               OnPostDataItem=BEGIN
                                Window.CLOSE;
                                MESSAGE(Text002,CounterOK,CounterTotal);

                                MESSAGE(Text31022890,CounterSourceDocSentOk,CounterTotal); //soft,n
                              END;

               ReqFilterFields=No.,Status }

  }
  REQUESTPAGE
  {
    PROPERTIES
    {
      SaveValues=Yes;
      OnOpenPage=BEGIN
                   SalesSetup.GET;
                   CalcInvDisc := SalesSetup."Calc. Inv. Discount";
                   ReplacePostingDate := FALSE;
                   ReplaceDocumentDate := FALSE;
                 END;

    }
    CONTROLS
    {
      { 1900000001;0;Container;
                  ContainerType=ContentArea }

      { 1900000002;1;Group  ;
                  CaptionML=[ENU=Options;
                             PTG=Op��es] }

      { 1   ;2   ;Field     ;
                  Name=Ship;
                  CaptionML=[ENU=Ship;
                             PTG=Envio];
                  SourceExpr=ShipReq }

      { 2   ;2   ;Field     ;
                  Name=Invoice;
                  CaptionML=[ENU=Invoice;
                             PTG=Fatura];
                  SourceExpr=InvReq }

      { 3   ;2   ;Field     ;
                  Name=PostingDate;
                  CaptionML=[ENU=Posting Date;
                             PTG=Data Registo];
                  SourceExpr=PostingDateReq }

      { 4   ;2   ;Field     ;
                  Name=ReplacePostingDate;
                  CaptionML=[ENU=Replace Posting Date;
                             PTG=Substituir Data Reg.];
                  ToolTipML=ENU=Specifies if the new posting date will be applied.;
                  SourceExpr=ReplacePostingDate;
                  OnValidate=BEGIN
                               IF ReplacePostingDate THEN
                                 MESSAGE(Text003);
                             END;
                              }

      { 5   ;2   ;Field     ;
                  CaptionML=[ENU=Replace Document Date;
                             PTG=Substituir Data Doc.];
                  SourceExpr=ReplaceDocumentDate }

      { 7   ;2   ;Field     ;
                  CaptionML=[ENU=Calc. Inv. Discount;
                             PTG=Calc. Desconto Fatu.];
                  SourceExpr=CalcInvDisc;
                  OnValidate=BEGIN
                               SalesSetup.GET;
                               SalesSetup.TESTFIELD("Calc. Inv. Discount",FALSE);
                             END;
                              }

    }
  }
  LABELS
  {
  }
  CODE
  {
    VAR
      Text000@1000 : TextConst 'ENU=Enter the posting date.;PTG=Introduza a data registo.';
      Text001@1001 : TextConst 'ENU=Posting orders  #1########## @2@@@@@@@@@@@@@;PTG=Reg. encomendas #1########## @2@@@@@@@@@@@@@';
      Text002@1002 : TextConst 'ENU=%1 orders out of a total of %2 have now been posted.;PTG=Foram registadas %1 encomendas de um total de %2.';
      Text003@1003 : TextConst 'ENU=The exchange rate associated with the new posting date on the sales header will not apply to the sales lines.;PTG=A taxa de c�mbio associada � nova data de registo do cabe�alho de venda n�o ser� aplicada �s linhas de venda.';
      SalesLine@1005 : Record 37;
      SalesSetup@1006 : Record 311;
      SalesCalcDisc@1007 : Codeunit 60;
      SalesPost@1008 : Codeunit 80;
      Window@1009 : Dialog;
      ShipReq@1010 : Boolean;
      InvReq@1011 : Boolean;
      PostingDateReq@1012 : Date;
      CounterTotal@1013 : Integer;
      Counter@1014 : Integer;
      CounterOK@1015 : Integer;
      ReplacePostingDate@1016 : Boolean;
      ReplaceDocumentDate@1017 : Boolean;
      CalcInvDisc@1018 : Boolean;
      BatchConfirm@1004 : ' ,Skip,Update';
      "//--soft-global--//"@90000000 : Integer;
      CounterSourceDocSentOk@1000000001 : Integer;
      "//--soft-text--//"@90000001 : TextConst;
      Text31022890@1000000000 : TextConst 'ENU=Number of source documents successfully sent to AT: %1 out of a total of %2.;PTG=N� de documentos origem enviados � AT com sucesso: %1 de um total de %2.';

    LOCAL PROCEDURE CalculateInvoiceDiscount@1();
    BEGIN
      SalesLine.RESET;
      SalesLine.SETRANGE("Document Type","Sales Header"."Document Type");
      SalesLine.SETRANGE("Document No.","Sales Header"."No.");
      IF SalesLine.FINDFIRST THEN
        IF SalesCalcDisc.RUN(SalesLine) THEN BEGIN
          "Sales Header".GET("Sales Header"."Document Type","Sales Header"."No.");
          COMMIT;
        END;
    END;

    PROCEDURE InitializeRequest@2(ShipParam@1000 : Boolean;InvoiceParam@1001 : Boolean;PostingDateParam@1002 : Date;ReplacePostingDateParam@1003 : Boolean;ReplaceDocumentDateParam@1004 : Boolean;CalcInvDiscParam@1005 : Boolean);
    BEGIN
      ShipReq := ShipParam;
      InvReq := InvoiceParam;
      PostingDateReq := PostingDateParam;
      ReplacePostingDate := ReplacePostingDateParam;
      ReplaceDocumentDate := ReplaceDocumentDateParam;
      CalcInvDisc := CalcInvDiscParam;
    END;

    BEGIN
    END.
  }
  RDLDATA
  {
  }
}

