OBJECT Page 9315 Inventory Put-aways
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0;
  }
  PROPERTIES
  {
    Editable=No;
    CaptionML=[ENU=Inventory Put-aways;
               PTG=Arruma��es Invent�rio];
    SourceTable=Table5766;
    SourceTableView=WHERE(Type=CONST(Invt. Put-away));
    PageType=List;
    CardPageID=Inventory Put-away;
    OnOpenPage=BEGIN
                 ErrorIfUserIsNotWhseEmployee;
               END;

    OnFindRecord=BEGIN
                   EXIT(FindFirstAllowedRec(Which));
                 END;

    OnNextRecord=BEGIN
                   EXIT(FindNextAllowedRec(Steps));
                 END;

    ActionList=ACTIONS
    {
      { 1900000003;0 ;ActionContainer;
                      ActionContainerType=RelatedInformation }
      { 1102601000;1 ;ActionGroup;
                      CaptionML=[ENU=Put-&away;
                                 PTG=&Arrumar];
                      Image=CreatePutAway }
      { 1102601002;2 ;Action    ;
                      CaptionML=[ENU=Co&mments;
                                 PTG=Co&ment�rios];
                      RunObject=Page 5776;
                      RunPageLink=Table Name=CONST(Whse. Activity Header),
                                  Type=FIELD(Type),
                                  No.=FIELD(No.);
                      Image=ViewComments }
      { 1102601003;2 ;Action    ;
                      CaptionML=[ENU=Posted Put-aways;
                                 PTG=Arrumar Registados];
                      RunObject=Page 7394;
                      RunPageView=SORTING(Invt. Put-away No.);
                      RunPageLink=Invt. Put-away No.=FIELD(No.);
                      Image=PostedPutAway }
      { 1102601004;2 ;Action    ;
                      CaptionML=[ENU=Source Document;
                                 PTG=Documento Origem];
                      Image=Order;
                      OnAction=VAR
                                 WMSMgt@1000 : Codeunit 7302;
                               BEGIN
                                 WMSMgt.ShowSourceDocCard("Source Type","Source Subtype","Source No.");
                               END;
                                }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 2   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the number of the warehouse header.;
                           PTG=""];
                SourceExpr="No." }

    { 25  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the type of document to which the line relates, including sales order, purchase order, or transfer order.;
                           PTG=""];
                SourceExpr="Source Document" }

    { 27  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the number of the source document from which the activity originated.;
                           PTG=""];
                SourceExpr="Source No." }

    { 15  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the type of activity, such as Put-away, that the warehouse performs on the lines that are attached to the header.;
                           PTG=""];
                SourceExpr=Type;
                Visible=FALSE }

    { 4   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the code for the location where the warehouse activity takes place.;
                           PTG=""];
                SourceExpr="Location Code" }

    { 8   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies information about the type of destination, such as customer or vendor, associated with the warehouse activity.;
                           PTG=""];
                SourceExpr="Destination Type" }

    { 10  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the number or the code of the customer or vendor that the line is linked to.;
                           PTG=""];
                SourceExpr="Destination No." }

    { 23  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the external document number for the source document to which the warehouse activity is related.;
                           PTG=""];
                SourceExpr="External Document No." }

    { 19  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the ID of the user who is responsible for the document.;
                           PTG=""];
                SourceExpr="Assigned User ID";
                Visible=FALSE }

    { 21  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the number of lines in the warehouse activity document.;
                           PTG=""];
                SourceExpr="No. of Lines" }

    { 6   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the method by which the lines are sorted on the warehouse header, such as Item or Document.;
                           PTG=""];
                SourceExpr="Sorting Method";
                Visible=FALSE }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                Visible=TRUE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {

    BEGIN
    END.
  }
}

