OBJECT Page 1508 Workflow Categories
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0;
  }
  PROPERTIES
  {
    CaptionML=[ENU=Workflow Categories;
               PTG=Categorias Workflow];
    SourceTable=Table1508;
    PageType=List;
  }
  CONTROLS
  {
    { 1   ;0   ;Container ;
                ContainerType=ContentArea }

    { 2   ;1   ;Group     ;
                Name=Group;
                GroupType=Repeater }

    { 3   ;2   ;Field     ;
                ToolTipML=ENU=Specifies the code for the workflow category.;
                ApplicationArea=#Suite;
                SourceExpr=Code }

    { 4   ;2   ;Field     ;
                ToolTipML=ENU=Specifies the workflow category.;
                ApplicationArea=#Suite;
                SourceExpr=Description }

  }
  CODE
  {

    BEGIN
    END.
  }
}

