OBJECT Page 5204 Alternative Address List
{
  OBJECT-PROPERTIES
  {
    Date=25/10/16;
    Time=12:00:00;
    Version List=NAVW110.0;
  }
  PROPERTIES
  {
    Editable=No;
    CaptionML=[ENU=Alternative Address List;
               PTG=Lista endere�os alternativo];
    SourceTable=Table5201;
    DataCaptionFields=Employee No.;
    PageType=List;
    CardPageID=Alternative Address Card;
    ActionList=ACTIONS
    {
      { 1900000003;0 ;ActionContainer;
                      ActionContainerType=RelatedInformation }
      { 28      ;1   ;ActionGroup;
                      CaptionML=[ENU=&Address;
                                 PTG=E&ndere�o];
                      Image=Addresses }
      { 27      ;2   ;Action    ;
                      CaptionML=[ENU=Co&mments;
                                 PTG=Co&ment�rios];
                      RunObject=Page 5222;
                      RunPageLink=Table Name=CONST(Alternative Address),
                                  No.=FIELD(Employee No.),
                                  Alternative Address Code=FIELD(Code);
                      Image=ViewComments }
    }
  }
  CONTROLS
  {
    { 1900000001;0;Container;
                ContainerType=ContentArea }

    { 1   ;1   ;Group     ;
                GroupType=Repeater }

    { 2   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies a code for the employee's alternate address.;
                           PTG=""];
                SourceExpr=Code }

    { 4   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the employee's last name.;
                           PTG=""];
                SourceExpr=Name }

    { 6   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the employee's first name, or an alternate name.;
                           PTG=""];
                SourceExpr="Name 2";
                Visible=FALSE }

    { 8   ;2   ;Field     ;
                ToolTipML=[ENU=Specifies an alternate address for the employee.;
                           PTG=""];
                SourceExpr=Address }

    { 10  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies another line of an alternate address for the employee.;
                           PTG=""];
                SourceExpr="Address 2";
                Visible=FALSE }

    { 12  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the city of the alternate address.;
                           PTG=""];
                SourceExpr=City;
                Visible=FALSE }

    { 14  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the postal code of the alternate address.;
                           PTG=""];
                SourceExpr="Post Code";
                Visible=FALSE }

    { 16  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the county of the employee's alternate address.;
                           PTG=""];
                SourceExpr=County;
                Visible=FALSE }

    { 18  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the employee's telephone number at the alternate address.;
                           PTG=""];
                SourceExpr="Phone No." }

    { 20  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the employee's fax number at the alternate address.;
                           PTG=""];
                SourceExpr="Fax No.";
                Visible=FALSE }

    { 22  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies the employee's alternate email address.;
                           PTG=""];
                SourceExpr="E-Mail";
                Visible=FALSE }

    { 30  ;2   ;Field     ;
                ToolTipML=[ENU=Specifies if a comment was entered for this entry.;
                           PTG=""];
                SourceExpr=Comment }

    { 1900000007;0;Container;
                ContainerType=FactBoxArea }

    { 1900383207;1;Part   ;
                Visible=FALSE;
                PartType=System;
                SystemPartID=RecordLinks }

    { 1905767507;1;Part   ;
                Visible=TRUE;
                PartType=System;
                SystemPartID=Notes }

  }
  CODE
  {

    BEGIN
    END.
  }
}

